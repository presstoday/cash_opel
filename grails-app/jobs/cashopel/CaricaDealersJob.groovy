package cashopel

import cashopel.polizze.Polizza
import cashopel.polizze.ZoneTarif
import cashopel.utenti.Dealer
import cashopel.utenti.Log
import excel.reader.ExcelReader
import grails.transaction.Transactional
import org.apache.poi.ss.util.CellReference
import org.apache.poi.xssf.usermodel.XSSFWorkbook

@Transactional
class CaricaDealersJob {
    def mailService
    def DLWebService
    static triggers = {

        //cron name: "AssocTelaiDataFinLeas", cronExpression: "0 32 9 * * ?"
        /*if(Environment.current == Environment.PRODUCTION) {
            cron name: "GenCertFinLeasJob", cronExpression: "0 00 19 * * ?"
        }*/

    }

    def group = "CaricaDealer"
    def description = "job per caricare Dealers"
    def execute(context) {
        try {
            def fileAssocia = context.mergedJobDataMap.get('myInputStream')
            caricaDealer(fileAssocia)

        } catch (Exception e) {
            log.error e.message
        }
    }

    def caricaDealer(def myInputStream){
        def riassunto = [], errore=[]
        boolean attivo=false
        def percVenditore
        def logg
        def rispostaMail = "Riassunto caricamento dealers :\r\n"
        def fileNameRisposta = "riassunto_Caricamento_Dealers_${new Date().format("yyyyMMddHms")}_.txt"
        def polizza
        try {
            def wb = ExcelReader.readXlsx(myInputStream) {
                sheet (0) {
                    rows(from: 1) {
                        def cellaCodice = cell("C")?.value
                        def cellaRagioneSociale = cell("F")?.value
                        def cellPartitaIva = cell("V")?.value
                        def cellaSalesSpecialist = cell("J")?.value
                        def cellaDealerType = cell("M")?.value
                        def cellapercVenditore =cell("N")?.value
                        def cellTelefono = cell("P")?.value
                        def cellFax = cell("Q")?.value?:""
                        def cellmailD= cell("O")?.value
                        def cellmailSales= cell("K")?.value
                        def cellIndirizzo=cell("S")?.value
                        def cellCap=cell("T")?.value
                        def cellLocalita=cell("U")?.value
                        def cellProvincia=cell("W")?.value
                        def cellCodIassicur=cell("Y")?.value
                       /* def cellZonaCampDealer=cell("Z")?.value
                        def cellCodProdDealer=cell("AA")?.value*/
                        if(cellaCodice.toString().trim()!='null' && cellaCodice.toString().trim()!='' && cellaCodice.toString().trim()!=null ){
                           /* println "que es esto--> ${cell("C")?.value}"
                            println "F que es esto--> ${cell("F")?.value}"
                            println "V que es esto--> ${cell("V")?.value}"
                            println "J que es esto--> ${cell("J")?.value}"
                            println "M que es esto--> ${cell("M")?.value}"
                            println "N que es esto--> ${cell("N")?.value}"
                            println "P que es esto--> ${cell("P")?.value}"
                            println "Q que es esto--> ${cell("Q")?.value}"
                            println "O que es esto--> ${cell("O")?.value}"
                            println "K que es esto--> ${cell("K")?.value}"
                            println "S que es esto--> ${cell("S")?.value}"
                            println "T que es esto--> ${cell("T")?.value}"
                            println "U que es esto--> ${cell("U")?.value}"
                            println "W que es esto--> ${cell("W")?.value}"
                            println "Y que es esto--> ${cell("Y")?.value}"
                            println "Z que es esto--> ${cell("Z")?.value}"
                            println "AA que es esto--> ${cell("AA")?.value}"*/

                            if(!(cellaDealerType.toString().contains("NO FINLINK")) && !(cellaDealerType.toString().contains("NO IVASS"))){
                                attivo=true
                                if(cellapercVenditore==null){
                                    percVenditore=0.0
                                }else{
                                    if(cellapercVenditore.toString().trim().toLowerCase().contains("si"))
                                    {
                                        percVenditore=0.3333333
                                    }else if(cellapercVenditore.toString().trim().toLowerCase().contains("no"))
                                    {
                                        percVenditore=0.0
                                    }
                                }
                            }else{
                                attivo=false
                                percVenditore=0.0
                            }
                            def codice=cellaCodice.toString().toString().trim()
                            if(codice.contains(".")){
                                /*def punto=codice.indexOf(".")
                                codice=codice.substring(0,punto).replaceAll("[^0-9]", "")*/
                                codice=Double.valueOf(codice).longValue().toString()
                            }
                            if (codice.toString().matches("[0].*")) {
                                //codice=codice.toString().replaceFirst("[^1-9]", "")
                                codice=codice.toString().replaceFirst("^0+(?!\\\$)", "")
                            }
                            def partitaIva = cellPartitaIva.toString().trim()
                            if(partitaIva.trim()!='null' && partitaIva.trim()!='' && partitaIva.trim()!=null ){
                                if(!partitaIva.toString().matches("^[a-z|A-Z]{6}[0-9]{2}[A-Z|a-z][0-9]{2}[A-Z|a-z][0-9]{3}[A-Z|a-z]\$") && !partitaIva.toString().matches("^[a-z|A-Z]{2}[0-9]{5}") && !partitaIva.matches("^0+\\d*(?:\\.\\d+)?\$")){
                                    partitaIva=Double.valueOf(partitaIva).longValue().toString()
                                }
                            }
                            if(!partitaIva.matches("[0-9]{11}")){
                                partitaIva=partitaIva.padLeft(11,"0")
                            }

                            def cap = cellCap.toString().trim()
                            if(cap.contains(".")){
                                def puntocap=cap.indexOf(".")
                                cap=cap.substring(0,puntocap).replaceAll("[^0-9]", "")
                            }
                            if (!cap.matches("[0-9]{5}")) {
                                cap=cap.padLeft(5,"0")
                            }
                            def salesSpecialist= cellaSalesSpecialist
                            def ragioneSociale = cellaRagioneSociale
                            def telefono =cellTelefono.toString().trim()
                            if(telefono.trim()!='null'){
                                if(telefono.toString().contains(".")){
                                    int zero=telefono.toString().indexOf(".")
                                    telefono=telefono.toString().substring(0,zero)
                                }

                            }else{telefono=''}
                            def fax = cellFax.toString().trim()
                            if(fax.trim()!='null' && fax.trim()!=''){
                                if(fax.toString().contains(".")){
                                    int zero=fax.toString().indexOf(".")
                                    fax=fax.toString().substring(0,zero)
                                }
                            }else{
                                fax=''
                            }
                            def emaild=cellmailD
                            def emailsales=cellmailSales
                            def codIassicur=cellCodIassicur
                            /*def zonaDealer = cellZonaCampDealer.toString().trim()
                            if(zonaDealer.contains(".")){
                                def punto=zonaDealer.indexOf(".")
                                zonaDealer=zonaDealer.substring(0,punto)
                            }*/
                            def codDL="0"
                            def zonadl=0

                            if(ZoneTarif.findByProvincia(cellProvincia)){
                                def zonaprov=ZoneTarif.findByProvincia(cellProvincia)
                                logg = new Log(parametri: "zona prov ${zonaprov.zona}", operazione: "caricamento dealers", pagina: "Polizza CASH")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                if(zonaprov && zonaprov.zona in ["1","2","3"]){
                                    codDL="GM1"
                                    zonadl=1
                                }else if(zonaprov && zonaprov.zona in ["4","5","6"]){
                                    codDL="GM2"
                                    zonadl=2
                                }else if(zonaprov && zonaprov.zona in ["7","8"]){
                                    codDL="GM3"
                                    zonadl=3
                                }
                                logg = new Log(parametri: "zona e codice ${zonadl}--${codDL}", operazione: "caricamento dealers", pagina: "Polizza CASH")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }

                            def dealerold= Dealer.findByCodice(codice)
                            if(dealerold){
                                logg = new Log(parametri: "Dealer trovato ${codice} -- ${ragioneSociale}", operazione: "aggiornamento dealers", pagina: "Polizza CASH")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                logg = new Log(parametri: "parametri Dealer trovato: ${codice} --rag soc.: ${ragioneSociale}, p.iva: ${partitaIva}, indirizzo: ${cellIndirizzo}, " +
                                        "localita: ${cellLocalita}, cap: ${cap}, provincia: ${cellProvincia}, dealer type: ${cellaDealerType}, telefono: ${telefono}, " +
                                        "fax: ${fax}, email: ${emaild}, emailsales: ${emailsales}, salesspecialist: ${salesSpecialist}, percvenditore: ${percVenditore}, " +
                                        "codiceiassicur: ${codIassicur}, cod campagna: ${codDL}, zonacampagna: ${zonadl}", operazione: "aggiornamento dealers", pagina: "Polizza CASH")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                dealerold.codice= codice
                                dealerold.ragioneSocialeD= ragioneSociale.toString().trim()
                                dealerold.pivaD= partitaIva
                                dealerold.indirizzoD= cellIndirizzo.toString().trim()
                                dealerold.localitaD= cellLocalita.toString().trim()
                                dealerold.capD= cap.toString().trim()
                                dealerold.provinciaD= cellProvincia.toString().trim()
                                dealerold.dealerType= cellaDealerType.toString().trim()
                                dealerold.telefonoD= telefono.toString().trim()
                                dealerold.faxD= fax.toString().trim()
                                dealerold.emailD= emaild.toString().trim()
                                dealerold.emailSales= emailsales.toString().trim()
                                dealerold.salesSpecialist= salesSpecialist.toString().trim()
                                dealerold.percVenditore= percVenditore
                                dealerold.attivo= attivo.toString().trim()
                                dealerold.codiceIASSICUR= codIassicur.toString().trim()
                                dealerold.codProdCampagna= codDL
                                dealerold.zonaCampagna= zonadl
                                if(dealerold.save(flush: true)) {
                                    logg = new Log(parametri: "Dealer aggiornato correttamente ${codice} -- ${ragioneSociale}", operazione: "aggiornamento dealers", pagina: "Polizza CASH")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    riassunto.add( "Dealer aggiornato correttamente ${codice} -- ${ragioneSociale}")
                                }
                                else{
                                    logg = new Log(parametri: "Errore creazione Dealer:${codice} -- ${ragioneSociale} ${dealerold.errors}", operazione: "aggiornamento dealers", pagina: "Polizza CASH")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    errore.add( "Errore creazione Dealer:${codice} -- ${ragioneSociale} ${dealerold.errors}")
                                }
                            }else{
                                logg = new Log(parametri: "creo nuovo dealer  ${codice} -- ${ragioneSociale}", operazione: "creazione dealer", pagina: "Polizza CASH")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                def dealer = new Dealer(
                                        codice: codice,
                                        ragioneSocialeD: ragioneSociale.toString().trim(),
                                        pivaD: partitaIva,
                                        indirizzoD: cellIndirizzo.toString().trim(),
                                        localitaD: cellLocalita.toString().trim(),
                                        capD: cap.toString().trim(),
                                        provinciaD: cellProvincia.toString().trim(),
                                        dealerType: cellaDealerType.toString().trim(),
                                        telefonoD: telefono.toString().trim(),
                                        faxD: fax.toString().trim(),
                                        emailD: emaild.toString().trim(),
                                        emailSales: emailsales.toString().trim(),
                                        salesSpecialist: salesSpecialist.toString().trim(),
                                        percVenditore: percVenditore,
                                        attivo: attivo.toString().trim(),
                                        codiceIASSICUR: codIassicur.toString().trim(),
                                        codProdCampagna: codDL,
                                        zonaCampagna: zonadl
                                )
                                if(dealer.save(flush: true)){
                                    logg = new Log(parametri: "Dealer creato correttamente ${codice} -- ${ragioneSociale}", operazione: "creazione dealers", pagina: "Polizza CASH")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    riassunto.add( "Dealer creato correttamente ${codice} -- ${ragioneSociale}")
                                }
                                else {
                                    logg = new Log(parametri: "Errore creazione Dealer: ${codice} -- ${ragioneSociale} ${dealer.errors}", operazione: "creazione dealers", pagina: "Polizza CASH")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    errore.add( "Errore creazione Dealer ${codice} -- ${ragioneSociale}: ${dealer.errors}")
                                }
                            }
                        }else{
                            logg = new Log(parametri: "riga vuota", operazione: "aggiornamento dealers", pagina: "Polizza CASH")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }


                    }
                }

            }

        }catch (e){
            logg = new Log(parametri: "c'e' un problema con il file, ${e.toString()}", operazione: "aggiornamento dealer tramite xls", pagina: "liste")
            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            errore.add( "c'e' un problema con il file, ${e.toString()}")
        }
        if (riassunto.size() > 0) {
            riassunto.collect { commento ->
                rispostaMail = rispostaMail + " " + commento + "\r\n"
            }/*.grep().join("\n")*/
        }
        if(errore.size()>0){
            errore.collect { commento ->
                rispostaMail = rispostaMail + " " + commento + "\r\n"
            }/*.grep().join("\n")*/

        }
        if(errore.size()>0 && !(riassunto.size()>0)){
            /***MI INVIO UNA MAIL con il riepilogo degli errori */
            def inviaMailCaricamento = mailService.invioMailDealerCaricati(rispostaMail, fileNameRisposta)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto degli aggiornamenti \u00E8 stata inviata", operazione: "invio riassunto dealer aggiornati/creati", pagina: "Polizza CASH")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail con il riassunto degli aggiornamenti dei dealers", operazione: "invio riassunto dealer aggiornati/creati", pagina: "Polizza CASH")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
        }else {
            /***MI INVIO UNA MAIL con il riepilogo degli errori*/
            def inviaMailCaricamento = mailService.invioMailDealerCaricati(rispostaMail, fileNameRisposta)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto del caricamento degli aggiornamenti \u00E8 stata inviata", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza CASH")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail la mail con il riassunto degli aggiornamenti dei dealers", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza CASH")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
        }
    }
}
