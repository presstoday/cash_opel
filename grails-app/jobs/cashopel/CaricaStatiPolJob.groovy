package cashopel

import cashopel.polizze.Documenti
import cashopel.polizze.Polizza
import cashopel.polizze.PolizzaRCA
import cashopel.polizze.StatoPolizza
import cashopel.polizze.TipoDoc
import cashopel.polizze.Tracciato
import cashopel.polizze.TracciatoCompagnia
import cashopel.polizze.TracciatoIASSRCA
import cashopel.utenti.Log
import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.JSch
import grails.transaction.Transactional
import grails.util.Environment

import java.util.regex.Pattern
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@Transactional
class CaricaStatiPolJob {
    def ftpService
    def mailService
    def tracciatiService
    static triggers = {

        if(Environment.current == Environment.PRODUCTION) {
            cron name: "CaricaStatiPolJob", cronExpression: "0 00 7 * * ?"
        }else{
            /**todo commentare questo cuando andiamo in prod*/
            //cron name: "CaricaStatiPolJob", cronExpression: "0 00 15 * * ?"
        }
    }

    def group = "CaricaStatiPolJob"
    def description = "job per scaricare dal FTP DL il file di polizze gia accettate"
    def execute() {
        try {
            ScaricaFileStatiPol()

        } catch (Exception e) {
            log.error e.message
        }
    }

    def ScaricaFileStatiPol() {
        def logg
        def response =[]
        Pattern pattern = ~/(\w+)/
        def fileReader = null
        def reader = null
        //Delimiter used in CSV file
        def delimiter = ";"
        println "è partito il job"
        def inserimento, fileNameRisposta, risposta, rispostaFinale,rispostaMail,filename
        fileNameRisposta = "risposta_aggiornamento_polizze_rca_${new Date().format("yyyyMMdd")}.txt"
        try {
            rispostaMail="Riassunto inserimento:\r\n"
            def username,password,host,port

            if(Environment.current == Environment.PRODUCTION) {
                username='SftpUserOPEL'
                password='Op.Fpbs.2016!'
                host='sftp.prontosinistro.it'
                port = 22
            }else{
                host = "5.249.141.51"
                port = 22
                username = "flussi-nais"
                password = "XYZrrr655"
            }


            def jsch = new JSch()
            def session = jsch.getSession(username, host, port)
            def properties = new Properties()
            properties.put("StrictHostKeyChecking", "no")
            session.config = properties
            session.userName = username
            session.password = password
            session.connect()
            def channel = session.openChannel("sftp") as ChannelSftp
            channel.connect()
            logg =new Log(parametri: "Connected to $host:$port", operazione: "LETTURA FILE CSV polizza", pagina: "SCARICA FILE STATO POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "Connected to $host:$port"
            logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: "LETTURA FILE CSV polizza", pagina: "SCARICA FILE STATO POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "Current sftp directory: ${channel.pwd()}"
            if(Environment.current == Environment.PRODUCTION ){
                channel.cd("ESITI-FLOTTE")
            }else{
                channel.cd("RCA-OPEL/test_esiti")
            }
            logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: " LETTURA FILE CSV polizza", pagina: "SCARICA FILE STATO POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "Current sftp directory: ${channel.pwd()}"
            Vector<ChannelSftp.LsEntry> list =  channel.ls("*.csv")
            if(list.size()>0){
                logg =new Log(parametri: "sono stati trovati:${list.size()}", operazione: " LETTURA FILE CSV polizza", pagina: "SCARICA FILE STATO POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "sono stati trovati:${list.size()}"
                for(ChannelSftp.LsEntry entry : list) {
                    def line = ""
                    boolean errori=false
                    logg =new Log(parametri: "downloaded: ${entry.filename}", operazione: " LETTURA FILE CSV polizza", pagina: "SCARICA FILE STATO POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println "downloaded: ${entry.filename}"
                    InputStream stream =  channel.get(entry.filename)
                    //createPolizzeCSV(stream)
                    filename="${entry.filename}"
                    //filenameorig = filename.substring(0, filename.lastIndexOf("."))
                    response.add(noPratica:"", messaggio: "\r\n\n riassunto del file che e' stato letto: ${filename}\n\n")
                    reader = new InputStreamReader(stream)
                    fileReader = new BufferedReader(reader)
                    fileReader.readLine()
                    while ((line = fileReader.readLine()) != null)
                    {
                        //println "${line}"
                        logg =new Log(parametri: " file--> ${filename} riga di file letta: ${line}", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        def tokens = []
                        tokens = line.split(delimiter, -1)
                        def dataricez = tokens[0].toString().trim()
                        def targa  = tokens[1].toString().trim()
                        def tipomovimento  = tokens[2].toString().trim()
                        def status = tokens[3].toString().trim()
                        def motivo = tokens[4].toString()

                        if(targa && tipomovimento=="A"){
                            def polizzaRCA=PolizzaRCA.findByTarga(targa.toString())
                            if(polizzaRCA){
                                if(status.contains("OK")){
                                    if(polizzaRCA.stato!=StatoPolizza.POLIZZA){
                                        polizzaRCA.stato=StatoPolizza.POLIZZA
                                        /**genero i tracciati iassicur **/
                                        def istracciato=TracciatoIASSRCA.findByPolizzaRCA(polizzaRCA)
                                        if(istracciato){
                                            istracciato.delete(flush: true)
                                        }
                                        def tracciatoIassicur= tracciatiService.generaTracciatoIAssicurRCA(polizzaRCA)
                                        logg =new Log(parametri: "${tracciatoIassicur}", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        if(!tracciatoIassicur.save(flush: true)){
                                            logg =new Log(parametri: "Si e' verificato un errore durante la creazione del tracciato IAssicur ${tracciatoIassicur.errors} ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }else{
                                            if(polizzaRCA.save(flush:true)) {
                                                logg =new Log(parametri: " polizza ${polizzaRCA.noPolizza} aggiornata allo stato POLIZZA", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                response.add(noPratica:"la polizza -->${polizzaRCA.noPolizza}", messaggio:" con targa ${targa} e' stata aggiornata correttamente")
                                                /**cerco se ha una polizza abbinata CVT**/
                                                def polizzacvt=Polizza.findByNoPolizza(polizzaRCA.noPolizza)
                                                if(polizzacvt){
                                                    if(polizzacvt.tracciatoId!=null){
                                                       // println "entro qui dove il tracciato esiste"
                                                        def tracciatoId= polizzacvt.tracciatoId
                                                        def tracciatoIASS=Tracciato.get(tracciatoId)
                                                        tracciatoIASS.dataCaricamento=new Date()
                                                        if(!tracciatoIASS.save(flush: true)){
                                                            // response.add(noPratica:"${polizzaRCA.noPolizza}", errore:"Si e' verificato un errore durante l'aggiornamento della POLIZZA CVT ${polizzacvt.noPolizza}--> ${polizzacvt.errors} ")
                                                            logg =new Log(parametri: "Si e' verificato un errore durante l'aggiornamento del tracciato essistente polizza CVT ${polizzacvt.noPolizza}--> ${tracciatoPol.errors} ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        }else{
                                                            def tracciatoIassicurCVT= tracciatiService.generaTracciatoIAssicur(polizzacvt)
                                                            if(!tracciatoIassicurCVT.save(flush: true)){
                                                                response.add(noPratica:"${polizzaRCA.noPolizza}", errore:"Si e' verificato un errore durante la creazione del tracciato IAssicur PER POLIZZA CVT ${polizzacvt.noPolizza}-->  ${tracciatoIassicurCVT.errors} ")
                                                                logg =new Log(parametri: "Si e' verificato un errore durante la creazione del tracciato IAssicur PER POLIZZA CVT ${polizzacvt.noPolizza}-->  ${tracciatoIassicurCVT.errors} ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            } else{
                                                                polizzacvt.tracciato=tracciatoIassicurCVT
                                                                /*println " trac polizza ${tracciatoIassicurCVT.polizzaId}"
                                                                println " trac id ${tracciatoIassicurCVT.id}"
                                                                println "polizza trac ${polizzacvt.tracciatoId}"*/
                                                                if(!polizzacvt.save(flush: true)){
                                                                    response.add(noPratica:"${polizzaRCA.noPolizza}", errore:"Si e' verificato un errore durante l'aggiornamento del tracciato iassicur  della POLIZZA CVT ${polizzacvt.noPolizza}--> ${polizzacvt.errors} ")
                                                                    logg =new Log(parametri: "Si e' verificato un errore durante l'aggiornamento della POLIZZA CVT ${polizzacvt.noPolizza}--> ${polizzacvt.errors} ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }else{
                                                                    //println "polizza tracdopod save ${polizzacvt.tracciatoId}"
                                                                    if(polizzacvt.tracciatoCompagniaId!=null){
                                                                        def tracciatocomp= polizzacvt.tracciatoCompagniaId
                                                                        def tracciatoComp=TracciatoCompagnia.get(tracciatocomp)
                                                                        tracciatoComp.dataCaricamento=new Date()
                                                                        if(!tracciatoComp.save(flush: true)) {
                                                                            logg =new Log(parametri: "Si e' verificato un errore durante l'aggiornamento del tracciato per DL essistente polizza CVT ${polizzacvt.noPolizza}--> ${tracciatoPol.errors} ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        }else{
                                                                            def tracciatoCompagnia= tracciatiService.generaTracciatoCompagnia(polizzacvt)
                                                                            if(!tracciatoCompagnia.save(flush: true)){
                                                                                response.add(noPratica:"per la polizza ${polizzaRCA.noPolizza}", errore:"si e' verificato un errore durante la creazione del tracciato DL PER POLIZZA CVT abbinata ${polizzacvt.noPolizza}--> ${tracciatoCompagnia.errors}")
                                                                                logg =new Log(parametri: "si e' verificato un errore durante la creazione del tracciato DL PER POLIZZA CVT abbinata ${polizzacvt.noPolizza}--> ${tracciatoCompagnia.errors} ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            } else{
                                                                                polizzacvt.tracciatoCompagnia=tracciatoCompagnia
                                                                                if(!polizzacvt.save(flush: true)){
                                                                                    response.add(noPratica:"${polizzaRCA.noPolizza}", errore:"Si e' verificato un errore durante l'aggiornamento del tracciato DL  per la POLIZZA CVT ${polizzacvt.noPolizza}--> ${polizzacvt.errors} ")
                                                                                    logg =new Log(parametri: "Si e' verificato un errore durante l'aggiornamento della POLIZZA CVT ${polizzacvt.noPolizza}--> ${polizzacvt.errors} ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                }else{
                                                                                    response.add(noPratica:"la polizza -->${polizzaRCA.noPolizza}", messaggio: "ha una POLIZZA CVT ${polizzacvt.noPolizza} che si e' aggiornata correttamente")
                                                                                    logg =new Log(parametri: "la polizza -->${polizzaRCA.noPolizza} ha una POLIZZA CVT ${polizzacvt.noPolizza} che si e' aggiornata correttamente", operazione: "aggiornamento polizza CVT da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                }
                                                                            }
                                                                        }
                                                                    }
                                                                }
                                                            }
                                                        }
                                                    }else{
                                                       // println "invece entro qui dove non esiste un tracciato"
                                                        def tracciatoIassicurCVT= tracciatiService.generaTracciatoIAssicur(polizzacvt)
                                                        polizzacvt.tracciato=tracciatoIassicurCVT
                                                        if(!tracciatoIassicurCVT.save(flush: true)){
                                                            response.add(noPratica:"${polizzaRCA.noPolizza}", errore:"Si e' verificato un errore durante la creazione del tracciato IAssicur PER POLIZZA CVT ${polizzacvt.noPolizza}-->  ${tracciatoIassicurCVT.errors} ")
                                                            logg =new Log(parametri: "Si e' verificato un errore durante la creazione del tracciato IAssicur PER POLIZZA CVT ${polizzacvt.noPolizza}-->  ${tracciatoIassicurCVT.errors} ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        } else{
                                                            if(!polizzacvt.save(flush: true)){
                                                                response.add(noPratica:"${polizzaRCA.noPolizza}", errore:"Si e' verificato un errore durante l'assegnazione del tracciato iassicur  della POLIZZA CVT ${polizzacvt.noPolizza}--> ${polizzacvt.errors} ")
                                                                logg =new Log(parametri: "Si e' verificato un errore durante l'aggiornamento della POLIZZA CVT ${polizzacvt.noPolizza}--> ${polizzacvt.errors} ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else{
                                                                if(polizzacvt.tracciatoCompagniaId!=null){
                                                                    def tracciatocomp= polizzacvt.tracciatoCompagniaId
                                                                    def tracciatoComp=TracciatoCompagnia.get(tracciatocomp)
                                                                    tracciatoComp.dataCaricamento=new Date()
                                                                    if(!tracciatoComp.save(flush: true)) {
                                                                        logg =new Log(parametri: "Si e' verificato un errore durante l'aggiornamento del tracciato per DL essistente polizza CVT ${polizzacvt.noPolizza}--> ${tracciatoPol.errors} ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    }else{
                                                                        def tracciatoCompagnia= tracciatiService.generaTracciatoCompagnia(polizzacvt)
                                                                        if(!tracciatoCompagnia.save(flush: true)){
                                                                            response.add(noPratica:"per la polizza ${polizzaRCA.noPolizza}", errore:"si e' verificato un errore durante la creazione del tracciato DL PER POLIZZA CVT abbinata ${polizzacvt.noPolizza}--> ${tracciatoCompagnia.errors}")
                                                                            logg =new Log(parametri: "si e' verificato un errore durante la creazione del tracciato DL PER POLIZZA CVT abbinata ${polizzacvt.noPolizza}--> ${tracciatoCompagnia.errors} ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        } else{
                                                                            polizzacvt.tracciatoCompagnia=tracciatoCompagnia
                                                                            if(!polizzacvt.save(flush: true)){
                                                                                response.add(noPratica:"${polizzaRCA.noPolizza}", errore:"Si e' verificato un errore durante l'aggiornamento del tracciato DL  per la POLIZZA CVT ${polizzacvt.noPolizza}--> ${polizzacvt.errors} ")
                                                                                logg =new Log(parametri: "Si e' verificato un errore durante l'aggiornamento della POLIZZA CVT ${polizzacvt.noPolizza}--> ${polizzacvt.errors} ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            }else{
                                                                                response.add(noPratica:"la polizza -->${polizzaRCA.noPolizza}", messaggio: "ha una POLIZZA CVT ${polizzacvt.noPolizza} che si e' aggiornata correttamente")
                                                                                logg =new Log(parametri: "la polizza -->${polizzaRCA.noPolizza} ha una POLIZZA CVT ${polizzacvt.noPolizza} che si e' aggiornata correttamente", operazione: "aggiornamento polizza CVT da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            }
                                                                        }
                                                                    }
                                                                }else{
                                                                    def tracciatoCompagnia= tracciatiService.generaTracciatoCompagnia(polizzacvt)
                                                                    if(!tracciatoCompagnia.save(flush: true)){
                                                                        response.add(noPratica:"per la polizza ${polizzaRCA.noPolizza}", errore:"si e' verificato un errore durante la creazione del tracciato DL PER POLIZZA CVT abbinata ${polizzacvt.noPolizza}--> ${tracciatoCompagnia.errors}")
                                                                        logg =new Log(parametri: "si e' verificato un errore durante la creazione del tracciato DL PER POLIZZA CVT abbinata ${polizzacvt.noPolizza}--> ${tracciatoCompagnia.errors} ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    } else{
                                                                        polizzacvt.tracciatoCompagnia=tracciatoCompagnia
                                                                        if(!polizzacvt.save(flush: true)){
                                                                            response.add(noPratica:"${polizzaRCA.noPolizza}", errore:"Si e' verificato un errore durante la creazione del tracciato DL  per la POLIZZA CVT ${polizzacvt.noPolizza}--> ${polizzacvt.errors} ")
                                                                            logg =new Log(parametri: "Si e' verificato un errore durante l'aggiornamento della POLIZZA CVT ${polizzacvt.noPolizza}--> ${polizzacvt.errors} ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        }else{
                                                                            response.add(noPratica:"la polizza -->${polizzaRCA.noPolizza}", messaggio: "ha una POLIZZA CVT ${polizzacvt.noPolizza} che si e' aggiornata correttamente")
                                                                            logg =new Log(parametri: "la polizza -->${polizzaRCA.noPolizza} ha una POLIZZA CVT ${polizzacvt.noPolizza} che si e' aggiornata correttamente", operazione: "aggiornamento polizza CVT da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        }
                                                                    }
                                                                }
                                                            }

                                                        }
                                                    }

                                                }else{
                                                    if(polizzaRCA.save(flush:true)) {
                                                        logg =new Log(parametri: " polizza ${polizzaRCA.noPolizza} aggiornata allo stato POLIZZA", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        response.add(noPratica:"la polizza -->${polizzaRCA.noPolizza}", messaggio:" con targa ${targa} e' stata aggiornata correttamente")
                                                    }else{
                                                        response.add(noPratica:"${polizzaRCA.noPolizza}", errore:"Errore aggiornamento polizza: ${polizzaRCA.errors}")
                                                        logg =new Log(parametri: "noPratica:${polizzaRCA.noPolizza}, errore:Errore aggiornamento polizza: ${polizzaRCA.errors}", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    }
                                                }


                                            }else{
                                                response.add(noPratica:"${polizzaRCA.noPolizza}", errore:"Errore aggiornamento polizza: ${polizzaRCA.errors}")
                                                logg =new Log(parametri: "noPratica:${polizzaRCA.noPolizza}, errore:Errore aggiornamento polizza: ${polizzaRCA.errors}", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            }



                                        }

                                    }else{
                                        response.add(noPratica:"La polizza --> ${polizzaRCA.noPolizza}", errore:" con targa--> ${targa} e' gia' nello stato POLIZZA  ")
                                        logg =new Log(parametri: "noPratica: La polizza-->${polizzaRCA.noPolizza}, con targa--> ${targa}  e' gia' nello stato POLIZZA ", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }

                                }else{
                                    response.add(noPratica:"${polizzaRCA.noPolizza}", errore:"per questa polizza lo status e': $status")
                                    logg =new Log(parametri: "noPratica: per questa polizza${polizzaRCA.noPolizza}, errore: lo status e': $status", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }
                            }else {
                                response.add(noPratica:"", errore:"non e' stata trovata nessuna polizza con questa targa: ${targa} controllare")
                                logg =new Log(parametri: "errore:non e' stata trovata nessuna polizza con questa targa: ${targa} controllare", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }
                        }else{
                            if(targa && tipomovimento!="A"){
                                response.add(noPratica:"", errore:"per la targa ${targa} trovata, il tipo movimento--> ${tipomovimento} e' diverso di A")
                                logg =new Log(parametri: "errore: per la targa ${targa} trovata, il tipo movimento--> ${tipomovimento}  e' diverso di A", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }else if (!targa){
                                response.add(noPratica:"", errore:"non e' stata trovata una targa compilata nel file controllare")
                                logg =new Log(parametri: "errore:non e' stata trovata nessuna polizza con questa targa: ${targa} controllare", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO POLIZZE JOB")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }
                        }
                    }
                    /***todo rimuovere il commento**/
                    channel.rename("/ESITI-FLOTTE/"+entry.filename,"/ESITI-FLOTTE/"+entry.filename+".old")
                    println "renamed: ${entry.filename}"
                    logg =new Log(parametri: "renamed: ${entry.filename}", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO  POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }
                response.collect{ commento ->
                    if (commento.messaggio){
                        rispostaMail =rispostaMail +" "+ commento.noPratica +" "+ commento.messaggio+"\r\n"
                    }
                    if (commento.errore){
                        rispostaMail =rispostaMail+" "+ commento.noPratica +" "+ commento.errore+"\r\n"
                    }
                }.grep().join("\n")
                def inviaMailCaricamento=mailService.invioMailAggiornamentoPolizzeRCA(rispostaMail, fileNameRisposta)
                if(inviaMailCaricamento.contains("queued")||inviaMailCaricamento.contains("sent") ||inviaMailCaricamento.contains("scheduled") ){
                    println "la mail con il riassunto degli aggiornamenti \u00E8 stata inviata"
                    logg = new Log(parametri: "la mail con il riassunto degli aggiornamenti \u00E8 stata inviata", operazione: " aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO  POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }else{
                    logg =new Log(parametri: "Non \u00E8 stato possibile inviare la mail con il riassunto degli aggiornamenti", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO  POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println "Non \u00E8 stato possibile inviare la mail con il riassunto degli aggiornamenti"
                }

                logg =new Log(parametri: "riassunto_${new Date().format("yyyyMMdd")}", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO  POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Current sftp directory: ${channel.pwd()}"
                logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO  POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }else {
                logg =new Log(parametri: "non ci sono file da scaricare", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO  POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                rispostaMail =rispostaMail+" non ci sono file da scaricare in data ${new Date().format("dd/MM/yyyy")}"
                def inviaMailCaricamento=mailService.invioMailAggiornamentoPolizzeRCA(rispostaMail, fileNameRisposta)
                if(inviaMailCaricamento.contains("queued")||inviaMailCaricamento.contains("sent") ||inviaMailCaricamento.contains("scheduled") ){
                    println "la mail con il riassunto degli aggiornamenti \u00E8 stata inviata"
                    logg = new Log(parametri: "la mail con il riassunto degli aggiornamenti \u00E8 stata inviata", operazione: " aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO  POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }else{
                    logg =new Log(parametri: "Non \u00E8 stato possibile inviare la mail con il riassunto degli aggiornamenti", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO  POLIZZE JOB")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println "Non \u00E8 stato possibile inviare la mail con il riassunto degli aggiornamenti"
                }
            }
            channel.disconnect()
            session.disconnect()
        } catch(e) {
            rispostaMail =rispostaMail+" c'\u00E8 un problema con la lettura dei file, errore--> ${e.toString()} \r\n conttatare il supporto"
            def inviaMailCaricamento=mailService.invioMailAggiornamentoPolizzeRCA(rispostaMail, fileNameRisposta)
            if(inviaMailCaricamento.contains("queued")||inviaMailCaricamento.contains("sent") ||inviaMailCaricamento.contains("scheduled") ){
                println "la mail con il riassunto degli aggiornamenti \u00E8 stata inviata"
                logg = new Log(parametri: "la mail con il riassunto degli aggiornamenti \u00E8 stata inviata", operazione: " aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO  POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }else{
                logg =new Log(parametri: "Non \u00E8 stato possibile inviare la mail con il riassunto degli aggiornamenti", operazione: "aggiornamento polizza da file excel DL", pagina: "SCARICA FILE STATO  POLIZZE JOB")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Non \u00E8 stato possibile inviare la mail con il riassunto degli aggiornamenti"
            }
            logg =new Log(parametri: "c'\u00E8 un problema con il file, --> ${e.toString()} ", operazione: " caricamento file stati polizza", pagina: "SCARICA FILE STATO  POLIZZE JOB")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "c'\u00E8 un problema con il file, --> ${e.toString()} "
            //response.add(codice:null, errore:"c'\u00E8 un problema con il file,  verificare che non ci siano righe con valori nulli sono nel csv")
        }

    }


}
