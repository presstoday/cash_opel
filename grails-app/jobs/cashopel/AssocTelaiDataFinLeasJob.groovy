package cashopel

import cashopel.polizze.Polizza
import cashopel.utenti.Log
import excel.reader.ExcelReader
import grails.transaction.Transactional


@Transactional
class AssocTelaiDataFinLeasJob {
    def mailService
    def DLWebService
    static triggers = {

        //cron name: "AssocTelaiDataFinLeas", cronExpression: "0 32 9 * * ?"
        /*if(Environment.current == Environment.PRODUCTION) {
            cron name: "GenCertFinLeasJob", cronExpression: "0 00 19 * * ?"
        }*/

    }

    def group = "AssocTelaiDataFinLeas"
    def description = "job per associare telai e data tariffa polizze FIN/LEASING"
    def execute(context) {
        try {
            def fileAssocia = context.mergedJobDataMap.get('myInputStream')
            associaTelaioDataTar(fileAssocia)

        } catch (Exception e) {
            log.error e.message
        }
    }

    def associaTelaioDataTar(def myInputStream){
        def riassunto = [], errore=[]
        def logg
        def rispostaMail = "Riassunto caricamento polizze FIN LEASING :\r\n"
        def fileNameRisposta = "riassunto_Caricamento_FinLeasing_${new Date().format("yyyyMMddHms")}_.txt"
        def polizza
        try {
            def wb = ExcelReader.readXlsx(myInputStream) {
                sheet (0) {
                    rows(from: 1) {
                        def rispostaWebservice
                        def cellaTelaio = cell("B")?.value?:""
                        def cellaDataTar = cell("A")?.value?:""
                        if((cellaDataTar.toString().trim().equals("null") || cellaDataTar.toString().trim()=='')){
                            logg =new Log(parametri: "la data della tariffa non e' stata inserita nella cella", operazione: "associazione dataTariffa-telaio polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            errore.add("la data della tariffa non e' stata inserita nella cella,")
                        }else {
                            if ((cellaTelaio.toString().trim().equals("null") || cellaTelaio.toString().trim() == '')) {
                                logg = new Log(parametri: "il numero di telaio non e' stato inserito nella cella", operazione: "aassociazione dataTariffa-telaio polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                errore.add("il numero di telaio non e' stato inserito nella cella,")
                            } else {
                                logg = new Log(parametri: "numero di telaio inserito ${cellaTelaio}", operazione: "associazione dataTariffa-telaio polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                polizza = Polizza.findByTelaio(cellaTelaio.toString().trim())
                                if (!polizza) {
                                    logg = new Log(parametri: "non esiste una polizza associata a questo telaio ${cellaTelaio}", operazione: "associazione dataTariffa-telaio polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    errore.add("non esiste una polizza associata a questo telaio ${cellaTelaio},")
                                } else {
                                    rispostaWebservice=DLWebService.chiamataWSFIN(polizza,cellaDataTar)
                                    logg = new Log(parametri: "risposta chiamata WS-->: ${rispostaWebservice}", operazione: "chiamata WS", pagina: "POLIZZE FIN/LEASING")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    if(rispostaWebservice.risposta==true){
                                        riassunto.add( "polizza caricata nel portale--> ${polizza.noPolizza} ")
                                    }else{
                                        errore.add( "errore chiamata al web service per la polizza--> ${polizza.noPolizza} -->errore : ${rispostaWebservice.errore}")
                                        logg = new Log(parametri: "errore chiamata al web service per la polizza--> ${polizza.noPolizza} --> ${rispostaWebservice.errore}", operazione: "associazione dataTariffa-telaio polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }
                                }
                            }
                        }
                    }
                }
            }


        }catch (e){
            logg = new Log(parametri: "c'e' un problema con il file, ${e.toString()}", operazione: "associazione dataTariffa-telaio polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "c'e' un problema con il file, ${e.toString()}"
            errore.add( "c'e' un problema con il file, ${e.toString()}")
        }
        if (riassunto.size() > 0) {
            riassunto.collect { commento ->
                rispostaMail = rispostaMail + " " + commento + "\r\n"
            }.grep().join("\n")
        }
        if(errore.size()>0){
            errore.collect { commento ->
                rispostaMail = rispostaMail + " " + commento + "\r\n"
            }.grep().join("\n")

        }
        if(errore.size()>0 && !(riassunto.size()>0)){
            /***INVIO LA MAIL A mach1 con il riepilogo dei certificati generati e i campioni dei certificati*/
            def inviaMailCaricamento = mailService.invioMailCaricamentoPolizzaFinLeas(rispostaMail, fileNameRisposta)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto dello stato del caricamento delle polizze \u00E8 stata inviata", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail con il riassunto dello stato del caricamento delle polizze", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Non e' stato possibile inviare la mail con il riassunto dello stato del caricamento delle polizze \n ${inviaMailCaricamento}"
            }
        }else {
            /***INVIO LA MAIL A mach1 con il riepilogo dei certificati generati e i campioni dei certificati*/
            def inviaMailCaricamento = mailService.invioMailCaricamentoPolizzaFinLeas(rispostaMail, fileNameRisposta)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto del caricamento delle polizze \u00E8 stata inviata", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail la mail con il riassunto del caricamento delle polizze", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Non e' stato possibile inviare la mail la mail con il riassunto del caricamento delle polizze \n ${inviaMailCaricamento}"
            }
        }
    }
}
