package cashopel

import cashopel.polizze.Polizza
import cashopel.utenti.Log
import excel.reader.ExcelReader
import grails.transaction.Transactional
import org.apache.camel.Pattern


@Transactional
class CorrezDataFinLeasJob {
    def mailService
    def DLWebService
    static triggers = {

        //cron name: "AssocTelaiDataFinLeas", cronExpression: "0 32 9 * * ?"
        /*if(Environment.current == Environment.PRODUCTION) {
            cron name: "GenCertFinLeasJob", cronExpression: "0 00 19 * * ?"
        }*/

    }

    def group = "CorrezDataFinLeas"
    def description = "job per correggere valori polizze FIN/LEASING"
    def execute(context) {
        try {
            def fileAssocia = context.mergedJobDataMap.get('myInputStream')
            correzdataFinLeas(fileAssocia)

        } catch (Exception e) {
            log.error e.message
        }
    }

    def correzdataFinLeas(def myInputStream){

        def logg
        def praticheNuove=[], errorePratica=[]
        def delimiter = ";"
        def listError =""
        def listPratiche=""
        def reader = null
        def fileReader = null
        def totale=0
        def totaleErr=0
        reader = new InputStreamReader(myInputStream)
        fileReader = new BufferedReader(reader)
        fileReader.readLine()
        def risposta =[]

        //try {
            def line = ""
            while ((line = fileReader.readLine()) != null) {
                def tokens = []
                tokens = line.split(delimiter)
                def cellnoPratica = tokens[1].toString().trim().replaceAll ("'", "")
                def cellaPremioImpo= tokens[56].toString().trim().replaceAll ("'", "")
                def cellaPremioLordo= tokens[57].toString().trim().replaceAll ("'", "")
                def cellaProvvDealer= tokens[58].toString().trim().replaceAll ("'", "")
                def cellaProvvVenditore= tokens[59].toString().trim().replaceAll ("'", "")
                def cellaProvvGMFI= tokens[60].toString().trim().replaceAll ("'", "")

                if(cellnoPratica.toString().equals("null")){
                    cellnoPratica=""
                }else{
                    cellnoPratica=cellnoPratica.toString()
                    cellnoPratica=cellnoPratica.toString().replaceFirst ("^0*", "")
                    def indexslash=cellnoPratica.indexOf("/")
                    if(indexslash>0){
                        cellnoPratica=cellnoPratica.toString().substring(0, indexslash)
                    }
                    if(cellnoPratica.toString().contains(".")){
                        def punto=cellnoPratica.toString().indexOf(".")
                        cellnoPratica=cellnoPratica.toString().substring(0,punto).replaceAll("[^0-9]", "")
                    }
                }

                    def premioImponibile
                    if(cellaPremioImpo.toString().trim()!='null' && cellaPremioImpo.toString().trim().length()>0){
                        premioImponibile=cellaPremioImpo.toString().trim().replaceAll("\\.","").replaceAll(",","\\.")
                        //premioImponibile=cellaPremioImpo.toString().trim().replaceAll(",","\\.")
                        premioImponibile = new BigDecimal(premioImponibile)
                    }else{
                        premioImponibile=0.0
                    }
                    def premioLordo
                    if(cellaPremioLordo.toString().trim()!='null' && cellaPremioLordo.toString().trim().length()>0){
                        premioLordo=cellaPremioLordo.toString().trim().replaceAll("\\.","").replaceAll(",","\\.")
                        // premioLordo=cellaPremioLordo.toString().trim().replaceAll(",","\\.")
                        premioLordo = new BigDecimal(premioLordo)
                    }else{
                        premioLordo=0.0
                    }
                    def provvDealer
                    if(cellaProvvDealer.toString().trim()!='null' && cellaProvvDealer.toString().trim().length()>0){
                        provvDealer=cellaProvvDealer.toString().trim().replaceAll("\\.","").replaceAll(",","\\.")
                        //provvDealer=cellaProvvDealer.toString().trim().replaceAll(",","\\.")
                        provvDealer = new BigDecimal(provvDealer)
                    }else{
                        provvDealer=0.0
                    }
                    def provvGmfi
                    if(cellaProvvGMFI.toString().trim()!='null' && cellaProvvGMFI.toString().trim().length()>0){
                        provvGmfi=cellaProvvGMFI.toString().trim().replaceAll("\\.","").replaceAll(",",".")
                        //provvGmfi=cellaProvvGMFI.toString().trim().replaceAll(",",".")
                        provvGmfi = new BigDecimal(provvGmfi)
                    }else{
                        provvGmfi=0.0
                    }
                    def provvVendi
                    if(cellaProvvVenditore.toString().trim()!='' && cellaProvvVenditore.toString().trim().length()>0 && cellaProvvVenditore.toString().trim()!='0'){
                        provvVendi=cellaProvvVenditore.toString().trim().replaceAll("\\.","").replaceAll(",","\\.")
                        //provvVendi=cellaProvvVenditore.toString().trim().replaceAll(",","\\.")
                        provvVendi=new BigDecimal(provvVendi)
                    }else{
                        provvVendi=0.0
                    }
                    def polizzaesistente= Polizza.findByNoPolizzaIlike(cellnoPratica)
                    if (polizzaesistente){
                        logg =new Log(parametri: "parametri aggiornati per la polizza ${cellnoPratica}-->premioImponibile: ${premioImponibile}, premioLordo :${premioLordo}, provvDealer:${provvDealer}, provvGmfi:${provvGmfi}, provvVendi:${provvVendi}  ", operazione: "correzione polizze finnaziate tramite csv", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        polizzaesistente.premioImponibile=premioImponibile
                        polizzaesistente.premioLordo=premioLordo
                        polizzaesistente.provvDealer=provvDealer
                        polizzaesistente.provvGmfi=provvGmfi
                        polizzaesistente.provvVenditore=provvVendi

                        if(polizzaesistente.save(flush:true)) {
                            logg =new Log(parametri: "la polizza viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            praticheNuove.add("polizza caricata correttamente ${polizzaesistente.noPolizza},")
                            totale++
                        }
                        else{
                            logg =new Log(parametri: "Errore caricamento polizza: ${polizzaesistente.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            risposta.add(noPratica:cellnoPratica, errore:"Errore caricamento polizza: ${polizzaesistente.errors}")
                            errorePratica.add(" pratica-->${cellnoPratica} Errore caricamento polizza: ${polizzaesistente.errors},")
                            totaleErr++
                        }
                    }

                }

        /*}catch (e){
            logg =new Log(parametri: "c\u00E8 un problema con il caricamento del file ${e.toString()}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            risposta.add("c\u00E8  un problema con il caricamento del file")
            errorePratica.add("c\u00E8  un problema con il caricamento del file ")
        }*/


                if(errorePratica.size()>0){
                    for (String s : errorePratica)
                    {
                        listError += s + "\r\n\r\n"
                    }
                    //println(listError)
                }

                if(praticheNuove.size()>0){
                    for (String s : praticheNuove)
                    {
                        listPratiche += s + "\r\n\r\n"
                    }
                    //println(listPratiche)
                }





    }
}
