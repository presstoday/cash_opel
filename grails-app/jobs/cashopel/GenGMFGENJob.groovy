package cashopel

import cashopel.utenti.Log
import grails.transaction.Transactional

import java.text.NumberFormat

@Transactional
class GenGMFGENJob {
    def mailService
    def IAssicurWebService
    def GmfgenService
    def ftpService
    static triggers = {

        //cron name: "GenCertFinLeas", cronExpression: "0 32 9 * * ?"
        /*if(Environment.current == Environment.PRODUCTION) {
            cron name: "GenCertFinLeasJob", cronExpression: "0 00 19 * * ?"
        }*/

    }

    def group = "GenGMFGEN"
    def description = "job per generare GMFGEN"
    def execute(context) {
        try {
            def mese = context.mergedJobDataMap.get('mese')
            def anno = context.mergedJobDataMap.get('anno')
            genGMFGEN(mese, anno)

        } catch (Exception e) {
            log.error e.message
        }
    }

    def genGMFGEN(mese, anno) {
        def logg
        try {
           // println mese
            //println anno
            def resultwebS = IAssicurWebService.getGMFGENFINLEACASHRINN(mese, anno)
            def polizzeWS = resultwebS.polizze
            logg =new Log(parametri: "numero di polizze trovate su IAssciur nel periodo selezionato ${polizzeWS.size()} ", operazione: "generaFileGMFGEN", pagina: "LISTA POLIZZE CASH")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def  fmt_IT = NumberFormat.getNumberInstance(Locale.ITALIAN)
            //fmt_IT.setParseBigDecimal(true)
            fmt_IT.setMaximumFractionDigits(2)
            if (polizzeWS) {
                def filegmfgen=GmfgenService.fileGMFGEN(polizzeWS,mese)
                /*response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                response.addHeader "Content-disposition", "inline; filename=GMFEN_${new Date().format("ddMMyyyy")}.xlsx"
                response.outputStream << filegmfgen*/
            }else{
                /***inviare mail avvisando che non c'erano***/
                logg =new Log(parametri: "non ci sono polizze generate nel mese/anno selezionato", operazione: "generazione GMFGEN", pagina: "LISTA POLIZZE CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                def inviaMailCaricamento = mailService.invioMailGMFGENMach1(false)
                if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                    logg = new Log(parametri: "la mail con il riassunto della creazione del report GMFGEN \u00E8 stata inviata", operazione: "invio GMFGEN", pagina: "LISTA POLIZZE CASH")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                } else {
                    logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail di certificati falliti", operazione: "invio riassunto certificati Fin/Leasing generati", pagina: "LISTA POLIZZE CASH")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println "Non e' stato possibile inviare la mail di certificati falliti \n ${inviaMailCaricamento}"

                }
            }
        } catch(e) {
            logg =new Log(parametri: "errore try cash ${e.toString()}", operazione: "generazione GMFGEN", pagina: "LISTA POLIZZE CASH")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
    }
}
