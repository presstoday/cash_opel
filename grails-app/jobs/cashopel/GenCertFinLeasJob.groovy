package cashopel

import cashopel.polizze.Documenti
import cashopel.polizze.Polizza
import cashopel.polizze.StatoPolizza
import cashopel.polizze.TipoDoc
import cashopel.utenti.Log
import grails.transaction.Transactional
import grails.util.Environment

import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@Transactional
class GenCertFinLeasJob {
    def mailService
    def IAssicurWebService
    def generaCertService
    def ftpService
    static triggers = {

        //cron name: "GenCertFinLeas", cronExpression: "0 32 9 * * ?"
        /*if(Environment.current == Environment.PRODUCTION) {
            cron name: "GenCertFinLeasJob", cronExpression: "0 00 19 * * ?"
        }*/

    }

    def group = "GenCertFinLeas"
    def description = "job per generare i certificati FIN/LEASING"
    def execute(context) {
        try {
            def dataGeneraCert = context.mergedJobDataMap.get('dataGeneraCert')
            println "datagencert--> $dataGeneraCert"
                    CertFINLeas(dataGeneraCert)

        } catch (Exception e) {
            log.error e.message
        }
    }

    def CertFINLeas(dataGeneraCert) {
        def logg
        try {
            generaCertService.generaCertificato(dataGeneraCert)

        } catch(e) {
            logg =new Log(parametri: "errore try cash ${e.toString()}", operazione: "generazione certificati FIN_LEASING", pagina: "JOB GENERA FIN_LEASING")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
    }
}
