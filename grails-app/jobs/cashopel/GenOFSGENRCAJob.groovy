package cashopel

import cashopel.utenti.Log
import grails.transaction.Transactional

import java.text.NumberFormat

@Transactional
class GenOFSGENRCAJob {
    def mailService
    def IAssicurWebService
    def GmfgenService
    def ftpService
    static triggers = {

        //cron name: "GenCertFinLeas", cronExpression: "0 32 9 * * ?"
        /*if(Environment.current == Environment.PRODUCTION) {
            cron name: "GenCertFinLeasJob", cronExpression: "0 00 19 * * ?"
        }*/

    }

    def group = "GenOFSGEN"
    def description = "job per generare OFSGEN"
    def execute(context) {
        try {
            def mese = context.mergedJobDataMap.get('mese')
            def anno = context.mergedJobDataMap.get('anno')
            genOFSGEN(mese, anno)

        } catch (Exception e) {
            log.error e.message
        }
    }

    def genOFSGEN(mese, anno) {
        def logg
        try {
           // println mese
            //println anno
            def resultwebS = IAssicurWebService.getOFS(mese, anno)
            def polizzeWS = resultwebS.polizze
            logg =new Log(parametri: "numero di polizze trovate su IAssciur nel periodo selezionato ${polizzeWS.size()} ", operazione: "generaFileOFSGEN", pagina: "LISTA POLIZZE RCA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def  fmt_IT = NumberFormat.getNumberInstance(Locale.ITALIAN)
            //fmt_IT.setParseBigDecimal(true)
            fmt_IT.setMaximumFractionDigits(2)
            if (polizzeWS) {
                def filegmfgen=GmfgenService.fileOFSGEN(polizzeWS,mese)
                if(!filegmfgen){
                    logg =new Log(parametri: "il report non e' stato generato", operazione: "generazione OFSGEN", pagina: "LISTA POLIZZE RCA")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    def inviaMailCaricamento = mailService.invioMailOFSGENMach1(false)
                    if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                        logg = new Log(parametri: "la mail con il riassunto della creazione del report OFSGEN \u00E8 stata inviata", operazione: "invio OFSGEN", pagina: "LISTA POLIZZE RCA")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    } else {
                        logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail di certificati falliti", operazione: "invio riassunto certificati Fin/Leasing generati", pagina: "LISTA POLIZZE RCA")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        println "Non e' stato possibile inviare la mail di certificati falliti \n ${inviaMailCaricamento}"

                    }
                }
                /*response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                response.addHeader "Content-disposition", "inline; filename=GMFEN_${new Date().format("ddMMyyyy")}.xlsx"
                response.outputStream << fileOFSGEN*/
            }else{
                /***inviare mail avvisando che non c'erano***/
                logg =new Log(parametri: "non ci sono polizze generate nel mese/anno selezionato", operazione: "generazione OFSGEN", pagina: "LISTA POLIZZE RCA")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                def inviaMailCaricamento = mailService.invioMailOFSGENMach1(false)
                if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                    logg = new Log(parametri: "la mail con il riassunto della creazione del report OFSGEN \u00E8 stata inviata", operazione: "invio OFSGEN", pagina: "LISTA POLIZZE RCA")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                } else {
                    logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail di certificati falliti", operazione: "invio riassunto certificati Fin/Leasing generati", pagina: "LISTA POLIZZE RCA")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println "Non e' stato possibile inviare la mail di certificati falliti \n ${inviaMailCaricamento}"

                }
            }
        } catch(e) {
            logg =new Log(parametri: "errore try cash ${e.toString()}", operazione: "generazione OFSGEN", pagina: "LISTA POLIZZE RCA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
    }
}
