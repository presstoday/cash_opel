package cashopel

import cashopel.polizze.DocumentiSalesSpecialist
import cashopel.polizze.Polizza
import cashopel.polizze.TipoDoc
import cashopel.polizze.TipoDocSS
import cashopel.utenti.Log
import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.JSch
import grails.transaction.Transactional
import grails.util.Environment

import java.util.zip.ZipOutputStream

@Transactional
class ElencoMensileJob {
    def mailService
    static triggers = {

        if(Environment.current == Environment.PRODUCTION) {
           // cron name: "ElencoMensilePolizze", cronExpression: "0 30 12 1 * ?"
        }

    }

    def group = "ElencoMensilePolizze"
    def description = "job per inviare elenco mensile delle polizze"
    def execute() {
        try {
            invioMensilePolizze()

        } catch (Exception e) {
            log.error e.message
        }
    }

    def invioMensilePolizze() {
         def logg
        try {
            def response =[]
            def rispostaMail
            def dataodierna = new Date()
            dataodierna=dataodierna.clearTime()
            def streamRiassunti = new ByteArrayOutputStream()
            def zipRiassunti = new ZipOutputStream(streamRiassunti)
            rispostaMail="Riassunto invio mensile polizze:\r\n"
            def c = Calendar.getInstance()
            c.add(Calendar.MONTH, -1)
            def cal = Calendar.getInstance()
            cal.setTime(dataodierna)
            cal.add(Calendar.MONTH,-1)
            cal.set(Calendar.DAY_OF_MONTH,cal.getActualMinimum(Calendar.DAY_OF_MONTH))
            def firstDayOfTheMonth = cal.getTime()
            cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DAY_OF_MONTH))
            def lastDayOfTheMonth = cal.getTime()
            def fileNameRisposta = "risposta_.txt"
            def documenti = /*SqlLogger.log {*/DocumentiSalesSpecialist.createCriteria().list() {
                isNull ("dataCaricamento")
                eq "tipo",TipoDocSS.ELENCO_MENSILE
            }
            if (documenti.size() > 0) {
                documenti.each { documento ->
                    def invioMailMensile=mailService.invioMailMensile(documento)
                    println invioMailMensile
                    if(invioMailMensile.contains("queued")||invioMailMensile.contains("sent") ||invioMailMensile.contains("scheduled") ){
                        logg =new Log(parametri: "la mail di invio mensile \u00E8 stata inviata ai destinatari segnalati", operazione: "invio mensile polizze generate portale", pagina: "JOB ELENCO MENSILE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        println "la mail di invio mensile \u00E8 stata inviata ai destinatari segnalati"
                        def dataOdierna=new Date()
                        dataOdierna=dataOdierna.clearTime()
                        documento.dataCaricamento=dataOdierna
                        if(!documento.save(flush:true)){
                            logg =new Log(parametri: "la data caricamento del documento appena inviato non \u00E8 stata impostata ${documento.errors}", operazione: "invio mensile polizze generate portale", pagina: "JOB ELENCO MENSILE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            println "la data caricamento del documento appena inviato non \u00E8 stata impostata ${documento.errors}"
                        }else{
                            def host,port,username,password
                            host = "5.249.141.51"
                            port = 22
                            username = "flussi-nais"
                            password = "XYZrrr655"
                            def jsch = new JSch()
                            def session = jsch.getSession(username, host, port)
                            def properties = new Properties()
                            properties.put("StrictHostKeyChecking", "no")
                            session.config = properties
                            session.userName = username
                            session.password = password
                            session.connect()
                            def channel = session.openChannel("sftp") as ChannelSftp
                            channel.connect()
                            logg =new Log(parametri: "Connected to $host:$port", operazione: "invio mensile polizze generate portale", pagina: "JOB ELENCO MENSILE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            println "Connected to $host:$port"
                            //channel.cd("flussi-bmw")
                            println "Current sftp directory: ${channel.pwd()}"

                            channel.cd("..")
                            channel.cd("/home/flussi-nais/FLUSSI cashOpel/ESTRAZIONI")
                            channel.put(new ByteArrayInputStream(documento.fileContent), "${documento.fileName}")
                            println "creazione estrazioni ${documento.fileName}"
                            println "Current sftp directory: ${channel.pwd()}"
                            channel.disconnect()
                            session.disconnect()
                        }
                    }else{
                        println invioMailMensile
                        response.add("Errore invio mail  polizza: ${invioMailMensile}")
                    }
                }

            }else{
                println "Non ci sono polizze generate nel mese precedente"
            }


            /*response.collect{ commento ->
                if (commento?.messaggio){
                    rispostaMail =rispostaMail +" "+ commento.noDealer +" "+ commento.messaggio+"\r\n"
                }
            }.grep().join("\n")
            zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
            zipRiassunti.write(rispostaMail.bytes)*/
        } catch(e) { e.printStackTrace() }

    }


}
