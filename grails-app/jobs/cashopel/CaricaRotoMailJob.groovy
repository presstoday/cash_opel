package cashopel

import au.com.bytecode.opencsv.CSVWriter
import cashopel.polizze.Documenti
import cashopel.utenti.Log
import grails.transaction.Transactional
import grails.util.Environment

import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@Transactional
class CaricaRotoMailJob {
    def ftpService
    static triggers = {

        if(Environment.current == Environment.PRODUCTION) {
            cron name: "CaricaRotoMailJobPolizze", cronExpression: "0 30 21 1 * ?"
        }

    }

    def group = "CaricaRotoMailJobPolizze"
    def description = "job per caricare nel FTP certificati Mail le polizze"
    def execute() {
        try {
            CaricaRotoMail()

        } catch (Exception e) {
            log.error e.message
        }
    }

    def CaricaRotoMail() {
        def logg
        try {
            def fileNameCert, fileNameCSV
            def streamCSV = new ByteArrayOutputStream()
            def streamT = new ByteArrayOutputStream()
            def streamZIP = new ByteArrayOutputStream()
            def zipRiassunti = new ZipOutputStream(streamZIP)
            def inputStream
            def documentiInseriti=[], errorePratica=[]
            logg =new Log(parametri: "inizio a cercare i file da inviare al ftp per certificati posta", operazione: "carica roto mail", pagina: "JOB upload CERTIFICATI POSTA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def certificati=Documenti.createCriteria().list(){
                eq "caricatoRotoMail", false

            }
            if (certificati.size() > 0) {
                logg =new Log(parametri: "sono stati trovati  ${certificati.size()} da spedire per posta", operazione: "carica roto mail", pagina: "JOB upload CERTIFICATI POSTA")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                def writer = new StringWriter()
                // make a CSV writer that writes to a string
                fileNameCSV="indice${new Date().format("yyyyMMddHHmmss")}.csv"
                char puntoe=';'
                def w = new CSVWriter(writer, puntoe)
                w.writeNext((String[]) ['NomePDF', 'TotPag', 'Ragsoc', 'Indirizzo', 'CAP', 'Localit\u00E1','Prov','PagCCP','PagCO','Canale'])
                String[] line
                certificati.each { certificato ->
                    zipRiassunti.putNextEntry(new ZipEntry(certificato.fileName))
                    zipRiassunti.write(certificato.fileContent)

                    line = ["${certificato.fileName}","4","${certificato.cliente}","${certificato.indirizzo}","${certificato.cap}","${certificato.localita}","${certificato.provincia}","","","CASH_OPEL"]
                    w.writeNext(line)

                    certificato.caricatoRotoMail=true
                    if(certificato.save(flush:true)){
                        logg =new Log(parametri: "${certificato.fileName} aggiornato correttamente", operazione: "carica CERTFICATI mail", pagina: "JOB upload CERTIFICATI POSTA")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        documentiInseriti.add(" ${certificato.fileName} aggiornato correttamente")
                    }else{
                        logg =new Log(parametri: "Errore aggiornamento documento ${certificato.errors}", operazione: "carica CERTFICATI mail", pagina: "JOB upload CERTIFICATI POSTA")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        errorePratica.add(" Errore aggiornamento documento  ${certificato.errors}")
                    }
                }
                writer.close()
                streamCSV.write(writer.toString().getBytes())
                streamCSV.close()
                zipRiassunti.putNextEntry(new ZipEntry(fileNameCSV))
                zipRiassunti.write(streamCSV.toByteArray())
                zipRiassunti.close()
                streamZIP.close()
                inputStream=new ByteArrayInputStream(streamZIP.toByteArray())
                def fileTNome="machPisani${new Date().format("yyyyMMddHHmmss")}.T"
                def fileTCont=new ByteArrayInputStream(streamT.toByteArray())
                ftpService.uploadFileRotoDef("machPisani${new Date().format("yyyyMMddHHmmss")}.zip", inputStream,fileTNome,fileTCont )

            }else{
                logg =new Log(parametri: "Non ci sono polizze generate nel mese", operazione: "carica CERTFICATI mail", pagina: "JOB upload CERTIFICATI POSTA")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
            /*risposta.collect{ commento ->
                if (commento?.messaggio){
                    rispostaMail =rispostaMail +" "+ commento.noDealer +" "+ commento.messaggio+"\r\n"
                }
            }.grep().join("\n")
            zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
            zipRiassunti.write(rispostaMail.bytes)*/
        } catch(e) { println e.toString() }

    }


}
