package cashopel

import cashopel.polizze.Polizza
import cashopel.utenti.Log
import grails.transaction.Transactional
import grails.util.Environment

import java.util.zip.ZipOutputStream

@Transactional
class ElencoMensileMACH1Job {
    def mailService
    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
            cron name: "ElencoMensilePolizzeMACH1", cronExpression: "0 15 9 1 * ?"
        }
    }

    def group = "ElencoMensilePolizzeMACH1"
    def description = "job per inviare elenco mensile delle polizze a MACH1"
    def execute() {
        try {
            invioMensilePolizzeMACH1()

        } catch (Exception e) {
            log.error e.message
        }
    }

    def invioMensilePolizzeMACH1() {
        def logg
        try {
            logg =new Log(parametri: "inizio a preparare il file mensile", operazione: "invio mensile MACH1 delle polizze generate portale", pagina: "JOB ELENCO MENSILE MACH1")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "inizio a preparare il file mensile"
            def response =[]
            def rispostaMail
            def dataodierna = new Date()
            dataodierna=dataodierna.clearTime()
            def streamRiassunti = new ByteArrayOutputStream()
            def zipRiassunti = new ZipOutputStream(streamRiassunti)
            rispostaMail="Riassunto invio mensile polizze:\r\n"
            def fileNameRisposta = "risposta_.txt"
            def c = Calendar.getInstance()
            c.add(Calendar.MONTH, -1)
            def cal = Calendar.getInstance()
            cal.setTime(dataodierna)
            cal.add(Calendar.MONTH,-1)
            cal.set(Calendar.DAY_OF_MONTH,cal.getActualMinimum(Calendar.DAY_OF_MONTH))
            def firstDayOfTheMonth = cal.getTime()
            cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DAY_OF_MONTH))
            def lastDayOfTheMonth = cal.getTime()
            def polizze = /*SqlLogger.log {*/Polizza.createCriteria().list() {
                ge "dataInserimento", firstDayOfTheMonth
                le "dataInserimento", lastDayOfTheMonth
                isNotNull "tracciato"
                isNotNull "tracciatoCompagnia"
            }
            if (polizze.size() > 0) {
                def invioMailMensile=mailService.invioMailMensileMACH1()
                logg =new Log(parametri: "risultato dell0invio ${invioMailMensile}", operazione: "invio mensile MACH1 delle polizze generate portale", pagina: "JOB ELENCO MENSILE MACH1")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(invioMailMensile.contains("queued")||invioMailMensile.contains("sent") ||invioMailMensile.contains("scheduled") ){
                    logg =new Log(parametri: "la mail di invio mensile polizze \u00E8 stata inviata a MACH1 per controllo", operazione: "invio mensile MACH1 delle polizze generate portale", pagina: "JOB ELENCO MENSILE MACH1")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    //response.add(noDealer:"${dealer.ragioneSocialeD}", messaggio:"la mail di presa in carico per il dealer:${dealer.ragioneSocialeD}  \u00E8 stata inviata")
                }else{
                    logg =new Log(parametri: "Errore invio mail  polizza: ${invioMailMensile}", operazione: "invio mensile MACH1 delle polizze generate portale", pagina: "JOB ELENCO MENSILE MACH1")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    response.add("Errore invio mail  polizza: ${invioMailMensile}")
                }
            }else{
                logg =new Log(parametri: "Non ci sono polizze generate in questo mese", operazione: "invio mensile MACH1 delle polizze generate portale", pagina: "JOB ELENCO MENSILE MACH1")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
            /*response.collect{ commento ->
                if (commento?.messaggio){
                    rispostaMail =rispostaMail +" "+ commento.noDealer +" "+ commento.messaggio+"\r\n"
                }
            }.grep().join("\n")
            zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
            zipRiassunti.write(rispostaMail.bytes)*/
        } catch(e) {
            logg =new Log(parametri: "errore try cash ${e.toString()}", operazione: "invio mensile MACH1 delle polizze generate portale", pagina: "JOB ELENCO MENSILE MACH1")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
    }


}
