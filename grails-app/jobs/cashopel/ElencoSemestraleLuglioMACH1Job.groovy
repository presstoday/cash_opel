package cashopel

import cashopel.polizze.Polizza
import cashopel.utenti.Log
import grails.transaction.Transactional
import grails.util.Environment

import java.util.zip.ZipOutputStream

@Transactional
class ElencoSemestraleLuglioMACH1Job {
    def mailService
    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
            cron name: "ElencoSemestraleLuglioPolizzeMACH1", cronExpression: "0 45 9 1 7 ? *"
        }
    }

    def group = "ElencoSemestraleLuglioPolizzeMACH1"
    def description = "job per inviare elenco semestrale luglio delle polizze a MACH1"
    def execute() {
        try {
            invioSemestraleLuglioPolizzeMACH1()

        } catch (Exception e) {
            log.error e.message
        }
    }

    def invioSemestraleLuglioPolizzeMACH1() {
        def logg
        try {
            logg =new Log(parametri: "inizio a preparare il file semestrale Luglio", operazione: "invio semestrale Luglio MACH1 delle polizze generate portale", pagina: "JOB ELENCO SEMESTRALE LUGLIO MACH1")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "inizio a preparare il file semestrale Luglio"
            def response =[]
            def rispostaMail
            def streamRiassunti = new ByteArrayOutputStream()
            def zipRiassunti = new ZipOutputStream(streamRiassunti)
            rispostaMail="Riassunto invio semestrale luglio polizze:\r\n"
            def fileNameRisposta = "risposta_.txt"
            int year = Calendar.getInstance().get(Calendar.YEAR)
            def inizio = "${year}0101"
            def fine = "${year}0630"
            Date data_inizio = Date.parse( 'yyyyMMdd', inizio)
            Date data_fine = Date.parse( 'yyyyMMdd', fine)
            def polizze = /*SqlLogger.log {*/Polizza.createCriteria().list() {
                ge "dataInserimento", data_inizio
                le "dataInserimento", data_fine
                isNotNull "tracciato"
                isNotNull "tracciatoCompagnia"

            }
            if (polizze.size() > 0) {
                def invioMailSemestrale=mailService.invioMailSemestraleLuglioMACH1()
                logg =new Log(parametri: "risposta invioMailsemestraleMach1 ${invioMailSemestrale}", operazione: "invio semestrale Luglio MACH1 delle polizze generate portale", pagina: "JOB ELENCO SEMESTRALE LUGLIO MACH1")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(invioMailSemestrale.contains("queued")||invioMailSemestrale.contains("sent") ||invioMailSemestrale.contains("scheduled") ){
                    logg =new Log(parametri: "la mail di invio semestrale polizze \u00E8 stata inviata a MACH1 per controllo", operazione: "invio semestrale Luglio MACH1 delle polizze generate portale", pagina: "JOB ELENCO SEMESTRALE LUGLIO MACH1")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    //response.add(noDealer:"${dealer.ragioneSocialeD}", messaggio:"la mail di presa in carico per il dealer:${dealer.ragioneSocialeD}  \u00E8 stata inviata")
                }else{
                    logg =new Log(parametri: "Errore invio mail  polizza: ${invioMailSemestrale}", operazione: "invio semestrale Luglio MACH1 delle polizze generate portale", pagina: "JOB ELENCO SEMESTRALE LUGLIO MACH1")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    response.add("Errore invio mail  polizza: ${invioMailSemestrale}")
                }
            }else{
                logg =new Log(parametri: "Non ci sono polizze generate in questo semestre", operazione: "invio semestrale Luglio MACH1 delle polizze generate portale", pagina: "JOB ELENCO SEMESTRALE LUGLIO MACH1")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
            /*response.collect{ commento ->
                if (commento?.messaggio){
                    rispostaMail =rispostaMail +" "+ commento.noDealer +" "+ commento.messaggio+"\r\n"
                }
            }.grep().join("\n")
            zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
            zipRiassunti.write(rispostaMail.bytes)*/
        } catch(e) {
            logg =new Log(parametri: "errore try cash ${e.toString()}", operazione: "invio semestrale Luglio MACH1 delle polizze generate portale", pagina: "JOB ELENCO SEMESTRALE LUGLIO MACH1")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
    }


}
