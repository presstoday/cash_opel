package cashopel

import cashopel.polizze.Polizza
import cashopel.utenti.Log
import grails.transaction.Transactional
import grails.util.Environment
import java.util.zip.ZipOutputStream

@Transactional
class ElencoSettimanaleMACH1Job {
    def mailService
    static triggers = {

        if(Environment.current == Environment.PRODUCTION) {
            cron name: "ElencoSettimanalePolizzeMACH1", cronExpression: "0 00 9 ? * MON"
        }

    }

    def group = "ElencoSettimanalePolizzeMACH1"
    def description = "job per inviare elenco settimanale delle polizze a MACH1"
    def execute() {
        try {
            invioSettimanalePolizzeMACH1()

        } catch (Exception e) {
            log.error e.message
        }
    }

    def invioSettimanalePolizzeMACH1() {
        def logg
        try {
            logg =new Log(parametri: "inizio a preparare il file settimanale", operazione: "invio settimanale MACH1 delle polizze generate portale", pagina: "JOB ELENCO SETTIMANALE MACH1")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def response =[]
            def rispostaMail
            def dataodierna = new Date()
            dataodierna=dataodierna.clearTime()
            def streamRiassunti = new ByteArrayOutputStream()
            def zipRiassunti = new ZipOutputStream(streamRiassunti)
            rispostaMail="Riassunto invio settimanale polizze:\r\n"
            def fileNameRisposta = "risposta_.txt"
            def polizze = /*SqlLogger.log {*/Polizza.createCriteria().list() {
                ge "dataInserimento", dataodierna-7
                le "dataInserimento", dataodierna-3
                isNotNull "tracciato"
                isNotNull "tracciatoCompagnia"

            }
            if (polizze.size() > 0) {
                def invioMailSettimanale=mailService.invioMailSettimanaleMACH1()
                logg =new Log(parametri: "risultato invio mail settimanale ${invioMailSettimanale}", operazione: "invio settimanale MACH1 delle polizze generate portale", pagina: "JOB ELENCO SETTIMANALE MACH1")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(invioMailSettimanale.contains("queued")||invioMailSettimanale.contains("sent") ||invioMailSettimanale.contains("scheduled") ){
                    logg =new Log(parametri: "la mail di invio settimanale polizze \u00E8 stata inviata a MACH1 per controllo", operazione: "invio settimanale MACH1 delle polizze generate portale", pagina: "JOB ELENCO SETTIMANALE MACH1")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }else{
                    logg =new Log(parametri: "Errore invio mail  polizza: ${invioMailSettimanale}", operazione: "invio settimanale MACH1 delle polizze generate portale", pagina: "JOB ELENCO SETTIMANALE MACH1")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    response.add("Errore invio mail  polizza: ${invioMailSettimanale}")
                }
            }else{
                logg =new Log(parametri: "Non ci sono polizze generate in settimana", operazione: "invio settimanale MACH1 delle polizze generate portale", pagina: "JOB ELENCO SETTIMANALE MACH1")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
            /*response.collect{ commento ->
                if (commento?.messaggio){
                    rispostaMail =rispostaMail +" "+ commento.noDealer +" "+ commento.messaggio+"\r\n"
                }
            }.grep().join("\n")
            zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
            zipRiassunti.write(rispostaMail.bytes)*/
        } catch(e) {
            logg =new Log(parametri: "errore try cash ${e.toString()}", operazione: "invio settimanale MACH1 delle polizze generate portale", pagina: "JOB ELENCO SETTIMANALE MACH1")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
    }


}
