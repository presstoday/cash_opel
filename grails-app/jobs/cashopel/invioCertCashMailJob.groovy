package cashopel

import au.com.bytecode.opencsv.CSVWriter
import cashopel.polizze.Documenti
import cashopel.polizze.Polizza
import cashopel.polizze.StatoPolizza
import cashopel.polizze.TipoDoc
import cashopel.utenti.Log
import com.itextpdf.text.pdf.security.MakeXmlSignature
import grails.transaction.Transactional
import grails.util.Environment

import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@Transactional
class InvioCertCashMailJob {
    def mailService
    def generaCertService
    static triggers = {

        if(Environment.current == Environment.PRODUCTION) {
           //cron name: "InvioCertCashMail", cronExpression: "0 45 11 * * ?"
        }

    }
    def group = "InvioCertCashMail"
    def description = "job per invio tramite mail i certificati polizze CASH/FIN_LEASING"
    def execute(context) {
        try {
            def cash = context.mergedJobDataMap.get('cash')
            println "cash--> $cash"
            def isCash = Boolean.valueOf(cash)
            InvioCertificatiCashMail(isCash)

        } catch (Exception e) {
            log.error e.message
        }
    }
    def InvioCertificatiCashMail(boolean cash){
        def logg
        try {
            logg =new Log(parametri: "lancio il servizio", operazione: "invio certificati polizze CASH_FIN_LEASING", pagina: "JOB INVIO CERTIFICATI CASH_FIN_LEASING AI DEALERS_VENDITORI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            generaCertService.inviaMailDefinitivo(cash)
        } catch(e) {
            logg =new Log(parametri: "errore try cash ${e.toString()}", operazione: "invio certificati polizze CASH_FIN_LEASING", pagina: "JOB INVIO CERTIFICATI CASH_FIN_LEASING AI DEALERS_VENDITORI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
    }
}
