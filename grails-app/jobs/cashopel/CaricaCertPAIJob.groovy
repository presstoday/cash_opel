package cashopel

import cashopel.polizze.Documenti
import cashopel.polizze.TipoDoc
import cashopel.utenti.Log
import grails.transaction.Transactional
import grails.util.Environment

import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@Transactional
class CaricaCertPAIJob {
    def ftpService
    def mailService
    static triggers = {

        if(Environment.current == Environment.PRODUCTION) {
            cron name: "CaricaCertPAIJob", cronExpression: "0 00 21 * * ?"
        }

    }

    def group = "CaricaCertPAIJob"
    def description = "job per caricare nel FTP certificati Mail le polizze PAI"
    def execute() {
        try {
            CaricaCertPAI()

        } catch (Exception e) {
            log.error e.message
        }
    }

    def CaricaCertPAI() {
        def logg
        try {
            def fileNameCert
            def streamZIP = new ByteArrayOutputStream()
            def zipCertPAI = new ZipOutputStream(streamZIP)
            def inputStream
            def documentiInseriti=[], errorePratica=[]
            logg =new Log(parametri: "inizio a cercare i file da inviare al ftp per certificati Axa posta", operazione: "carica CERT PAI", pagina: "JOB upload CERTIFICATI PAI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def certificati=Documenti.createCriteria().list(){
                eq "caricatoRotoMail", false
                eq "tipo", TipoDoc.PAIPAG
            }
            if (certificati.size() > 0) {
                logg =new Log(parametri: "sono stati trovati  ${certificati.size()} da caricare", operazione: "carica PAI PAGAMENTO", pagina: "JOB upload CERTIFICATI PAI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                certificati.each { certificato ->
                    /**implementare solo questa parte dopo i primi test*/
                    def fileTNome=certificato.fileName
                    def contentByte=certificato.fileContent
                    def fileTCont= new ByteArrayInputStream(contentByte)
                    ftpService.uploadFileCertAxa(fileTNome, fileTCont)

                    zipCertPAI.putNextEntry(new ZipEntry(certificato.fileName))
                    zipCertPAI.write(certificato.fileContent)

                    certificato.caricatoRotoMail=true
                    if(certificato.save(flush:true)){
                        logg =new Log(parametri: "${certificato.fileName} aggiornato correttamente", operazione: "INVIA CERTFICATI pai mail", pagina: "JOB upload CERTIFICATI PAI")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        documentiInseriti.add(" ${certificato.fileName} aggiornato correttamente")
                    }else{
                        logg =new Log(parametri: "Errore aggiornamento documento ${certificato.errors}", operazione: "invia CERTFICATI pai mail", pagina: "JOB upload CERTIFICATI PAI")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        errorePratica.add(" Errore aggiornamento documento  ${certificato.errors}")
                    }
                }
                zipCertPAI.close()
                streamZIP.close()
                fileNameCert="POLIZZE_PAI_PAGAMENTO_${new Date().format("yyyyMMdd")}"
                def inviaMailCaricamento=mailService.invioMailCertificatiPAICaricati(streamZIP, fileNameCert)
                if(inviaMailCaricamento.contains("queued")||inviaMailCaricamento.contains("sent") ||inviaMailCaricamento.contains("scheduled") ){
                    logg =new Log(parametri: "la mail con i certificati pai  \u00E8 stata inviata", operazione: "invio certificati polizze pai", pagina: "JOB INVIO CERTIFICATI pai")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }else{
                    logg =new Log(parametri: "Non \u00E8 stato possibile inviare la mail dei certificati", operazione: "invio certificati polizze pai", pagina: "JOB INVIO CERTIFICATI pai")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }

            }else{
                logg =new Log(parametri: "Non ci sono polizze generate nel mese", operazione: "invia CERTFICATI pai mail", pagina: "JOB upload CERTIFICATI pai")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
            /*risposta.collect{ commento ->
                if (commento?.messaggio){
                    rispostaMail =rispostaMail +" "+ commento.noDealer +" "+ commento.messaggio+"\r\n"
                }
            }.grep().join("\n")
            zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
            zipRiassunti.write(rispostaMail.bytes)*/
        } catch(e) { println e.toString() }

    }


}
