package cashopel

import cashopel.polizze.PolizzaPaiPag
import cashopel.polizze.StatoPolizza
import cashopel.polizze.TipoCliente
import cashopel.polizze.TipoPolizza
import cashopel.utenti.Dealer
import cashopel.utenti.Log
import excel.reader.ExcelReader
import grails.transaction.Transactional
import groovy.time.TimeCategory

import java.util.regex.Pattern

@Transactional
class CaricaCSVPaipagJob {
    def mailService
    def caricamentiService
    def tracciatiService
    def DLWebService
    static triggers = {

        //cron name: "AssocTelaiDataFinLeas", cronExpression: "0 32 9 * * ?"
        /*if(Environment.current == Environment.PRODUCTION) {
            cron name: "GenCertFinLeasJob", cronExpression: "0 00 19 * * ?"
        }*/

    }

    def group = "CaricaCSVPaipag"
    def description = "job per caricare pratiche csv polizze pai pagamento "
    def execute(context) {
        try {
            def fileAssocia = context.mergedJobDataMap.get('myInputStream')
            caricapratichecsv(fileAssocia)

        } catch (Exception e) {
            log.error e.message
        }
    }

    def caricapratichecsv(def myInputStream){
        def logg
        def listFlussiPAI=""
        def listErrori=""
        def totaleErr=0
        def totale=0
        def erroPolizza=0
        def rispostaMail = "Riassunto caricamento polizze:\r\n"
        def fileNameRisposta = "riassunto_Caricamento_Polizze_${new Date().format("yyyyMMddHms")}_.txt"
        def flussiPAI=[], errorePratica=[]
        def filename=""
        //try {
        def risposta =[]
        Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$)/
        Pattern pattern = ~/(\w+)/
        String regex = "<(\\S+)[^>]+?mso-[^>]*>.*?</\\1>";
        def polizza
        def wb = ExcelReader.readXlsx(myInputStream) {
            sheet (0) {
                def j=1
                rows(from: 3) {
                    boolean sup75=false
                    def tipoClienteDef=TipoCliente.M
                    def cellnoPratica = cell("C")?.value?:""
                    def dataDecorrenza=""
                    if((cellnoPratica.toString().equals("null") || cellnoPratica.toString()=='')){
                        logg =new Log(parametri: "il numero di polizza non e' stato inserito nella cella-- riga ${j}", operazione: "caricamento polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        errorePratica.add("il numero di polizza non e' stato inserito nella cella -- riga ${j} del file excel,")
                    }else{
                        //cellaNoPolizza=cellaNoPolizza.toString().replaceFirst ("^0*", "")
                        if(cellnoPratica.toString().contains(".")){
                            def punto=cellnoPratica.toString().indexOf(".")
                            //cellaNoPolizza=Double.valueOf(cellaNoPolizza).longValue().toString()
                            cellnoPratica=cellnoPratica.toString().substring(0,punto).replaceAll("[^0-9]", "")
                        }
                        logg =new Log(parametri: "numero di polizza inserito ${cellnoPratica}", operazione: "caricamento polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                        polizza=PolizzaPaiPag.findByNoPolizza(cellnoPratica)
                        if(polizza){
                            logg = new Log(parametri: "la polizza con numero ${cellnoPratica} e' gia' stata caricata", operazione: "caricamento polizze PAI pagamento tramite xlsx", pagina: "POLIZZE FINANZIATE")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            errorePratica.add("la polizza con numero ${cellnoPratica}  e' gia' stata caricata,")
                        }else{
                            boolean denominazione=false
                            boolean coobligato=false
                            boolean cliente=false
                            boolean nonTrovato=false
                            def dealer
                            def cognomeDef,nomeDef,provinciaDef,localitaDef,capDef,telDef,cellDef,cfDef,indirizzoDef,emailDef
                            def celladatadecorrenza = cell("D")?.value?:""
                            def cellaDealer =cell("E")?.value?:""
                            def cellaBeneficiario=cell("BG")?.value?:""
                            def cellaDenominazione= cell("AE")?.value?:""
                            def cellaCoobligatoCognome= cell("S")?.value?:""
                            def cellaCoobligatoNome= cell("T")?.value?:""
                            def cellaCognome= cell("G")?.value?:""
                            def cellaNome= cell("H")?.value?:""
                            if((celladatadecorrenza.toString().equals("null") || celladatadecorrenza.toString()=='')) {
                                logg = new Log(parametri: "la data di decorrenza non e' stata inserita", operazione: "caricamento polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                errorePratica.add("la data di decorrenza non e' stata inserita,")
                                dataDecorrenza=null
                            }else{
                                //println "entro qui ${celladatadecorrenza} \n"
                                if(celladatadecorrenza instanceof Date){
                                    dataDecorrenza=celladatadecorrenza
                                }else {dataDecorrenza=Date.parse("dd/MM/yyyy",celladatadecorrenza)}

                            }
                            if(cellaDealer.toString().equals("null") || cellaDealer.toString().equals("")){
                                cellaDealer=""
                            }else{
                                cellaDealer=cellaDealer.toString().replaceAll ("'", "")
                                cellaDealer=cellaDealer.toString().replaceFirst ("^0*", "")
                                if(cellaDealer.toString().contains(".")){
                                    cellaDealer=Double.valueOf(cellaDealer).longValue().toString()
                                    //cellaDealer=cellaDealer.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                }
                                dealer=Dealer.findByCodice(cellaDealer.toString())
                            }
                            if(cellaBeneficiario.toString()!=''){
                                cellaBeneficiario=cellaBeneficiario.toUpperCase().trim()
                                //println "benficiario ${cellaBeneficiario}"
                                logg =new Log(parametri: "beneficiario-->${cellaBeneficiario}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                if(cellaDenominazione) {
                                    cellaDenominazione=cellaDenominazione.toUpperCase().trim()
                                    //println "cellaDenominazione ${cellaDenominazione}"
                                    if (cellaDenominazione.contains(cellaBeneficiario)) {
                                        logg =new Log(parametri: "denominazione-->${cellaDenominazione}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        denominazione = true

                                    } else {
                                        denominazione = false
                                    }
                                }
                                if(cellaCoobligatoNome){
                                    cellaCoobligatoNome=cellaCoobligatoNome.toUpperCase().trim()
                                    cellaCoobligatoCognome=cellaCoobligatoCognome.toUpperCase().trim()
                                    //def nomeCooC= cellaCoobligatoCognome+" "+cellaCoobligatoNome
                                    def nomeCooC= cellaCoobligatoNome+" "+cellaCoobligatoCognome
                                    //println "nomeCooC ${nomeCooC}"
                                    if(nomeCooC.trim().contains(cellaBeneficiario)){
                                        logg =new Log(parametri: "nome Coobligato-->${nomeCooC}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        coobligato=true
                                    }else{
                                        coobligato = false
                                    }
                                }
                                if(cellaNome){
                                    cellaNome=cellaNome.toUpperCase().trim()
                                    cellaCognome=cellaCognome.toUpperCase().trim()
                                    //def nomeCom=cellaCognome+" "+cellaNome
                                    def nomeCom=cellaNome+" "+cellaCognome
                                    //println "nomeCom ${nomeCom}"
                                    if(nomeCom.trim().contains(cellaBeneficiario)){
                                        logg =new Log(parametri: "nome Cliente-->${nomeCom}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        cliente=true
                                    }else{
                                        cliente = false
                                    }
                                }
                                if(coobligato){
                                    logg =new Log(parametri: "prendo i dati del Coobligato-->${cellaCoobligatoNome}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    def cellaCoobligatoPROV=cell("Z")?.value?:""
                                    if(cellaCoobligatoPROV.toString().contains("\'")) cellaCoobligatoPROV = cellaCoobligatoPROV.replaceAll("\'","")
                                    def cellaCoobligatoLOC=cell("X")?.value?:""
                                    if(cellaCoobligatoLOC.toString().contains("\'")) cellaCoobligatoLOC = cellaCoobligatoLOC.replaceAll("\'","")
                                    def cellaCoobligatoCAP=cell("AA")?.value?:""
                                    if(cellaCoobligatoCAP.toString().contains("\'")) cellaCoobligatoCAP = cellaCoobligatoCAP.replaceAll("\'","")
                                    def cellaCoobligatoTEL=cell("AB")?.value?:""
                                    if(cellaCoobligatoTEL.toString().contains("\'")) cellaCoobligatoTEL = cellaCoobligatoTEL.replaceAll("\'","")
                                    def cellaCoobligatoCEL=cell("AC")?.value?:""
                                    if(cellaCoobligatoCEL.toString().contains("\'")) cellaCoobligatoCEL = cellaCoobligatoCEL.replaceAll("\'","")
                                    def cellaCoobligatoCF=cell("V")?.value?:""
                                    if(cellaCoobligatoCF.toString().contains("\'")){
                                        cellaCoobligatoCF = cellaCoobligatoCF.replaceAll("\'","")
                                    }
                                    def cellaCoobligatoIND=cell("Y")?.value?:""
                                    if(cellaCoobligatoIND.toString().contains("\'")) cellaCoobligatoIND = cellaCoobligatoIND.replaceAll("\'","")
                                    def cellaCoobligatoemail=cell("AD")?.value?:""
                                    if(cellaCoobligatoIND.toString().contains("\'")) cellaCoobligatoIND = cellaCoobligatoIND.replaceAll("\'","")
                                    cognomeDef=cellaCoobligatoCognome.toString().trim().toUpperCase()
                                    nomeDef=cellaCoobligatoNome.toString().trim().toUpperCase()
                                    provinciaDef=cellaCoobligatoPROV.toString().trim().toUpperCase()
                                    localitaDef=cellaCoobligatoLOC.toString().trim().toUpperCase()
                                    capDef=cellaCoobligatoCAP.toString().trim().toUpperCase()
                                    telDef=cellaCoobligatoTEL.toString().trim().toUpperCase()
                                    cellDef=cellaCoobligatoCEL.toString().trim().toUpperCase()
                                    if(cellaCoobligatoCF !='null'){
                                        cfDef=cellaCoobligatoCF.toString().trim().toUpperCase()
                                        if(cfDef){
                                            def annoCF=cfDef.substring(6,8)
                                            if(Integer.parseInt(annoCF) && Integer.parseInt(annoCF)<=40){
                                                sup75=true
                                            }
                                            def sessC=cfDef.substring(9,11)
                                            if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                                tipoClienteDef=TipoCliente.F
                                            }
                                        }
                                        /*if(!cfDef.toString().matches("^[a-z|A-Z]{6}[0-9]{2}[A-Z|a-z][0-9]{2}[A-Z|a-z][0-9]{3}[A-Z|a-z]\$") && !cfDef.matches("^0+\\d*(?:\\.\\d+)?\$")){
                                            cfDef=Double.valueOf(cfDef).longValue().toString()
                                        }
                                        if(!cfDef.matches("[0-9]{11}")){
                                        cfDef=cfDef.padLeft(11,"0")
                                        }
                                        */
                                    }
                                    if(cellDef.toString().contains(".")){
                                        def punto=cellDef.toString().indexOf(".")
                                        cellDef=cellDef.toString().replaceAll("[^0-9]", "")
                                    }
                                    if(telDef.toString().contains(".")){
                                        def punto=telDef.toString().indexOf(".")
                                        telDef=telDef.toString().replaceAll("[^0-9]", "")
                                    }
                                    indirizzoDef=cellaCoobligatoIND.toString().trim().toUpperCase()

                                    if(cellaCoobligatoemail.toString().trim().size()>4){
                                        emailDef=cellaCoobligatoemail.toString().trim()
                                    }

                                }else if(denominazione){
                                    logg =new Log(parametri: "prendo i dati della denominazione -->${cellaDenominazione}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                                    def cellaProvinciaBusiness =cell("AK")?.value?:""
                                    if(cellaProvinciaBusiness.toString().contains("\'")) cellaProvinciaBusiness = cellaProvinciaBusiness.replaceAll("\'","")
                                    def cellaLocalitaBusiness =cell("X")?.value?:""
                                    if(cellaLocalitaBusiness.toString().contains("\'")) cellaLocalitaBusiness = cellaLocalitaBusiness.replaceAll("\'","")
                                    def cellaCapBusiness =cell("AL")?.value?:""
                                    if(cellaCapBusiness.toString().contains("\'")) cellaCapBusiness = cellaCapBusiness.replaceAll("\'","")
                                    def cellaTelefonoBusiness =cell("AM")?.value?:""
                                    if(cellaTelefonoBusiness.toString().contains("\'")) cellaTelefonoBusiness = cellaTelefonoBusiness.replaceAll("\'","")
                                    def cellaCellulareBusiness =cell("AN")?.value?:""
                                    if(cellaCellulareBusiness.toString().contains("\'")) cellaCellulareBusiness = cellaCellulareBusiness.replaceAll("\'","")
                                    def cellaPIvaBusiness =cell("AH")?.value?:""
                                    if(cellaPIvaBusiness.toString().contains("\'")) cellaPIvaBusiness = cellaPIvaBusiness.replaceAll("\'","")
                                    def cellaIndirizzoBusiness =cell("AJ")?.value?:""
                                    if(cellaIndirizzoBusiness.toString().contains("\'")) cellaIndirizzoBusiness = cellaIndirizzoBusiness.replaceAll("\'","")
                                    def cellaEmailBusiness =cell("AO")?.value?:""
                                    if(cellaEmailBusiness.toString().contains("\'")) cellaEmailBusiness = cellaEmailBusiness.replaceAll("\'","")
                                    cognomeDef=cellaDenominazione.toString().trim().toUpperCase()
                                    nomeDef=""

                                    provinciaDef=cellaProvinciaBusiness.toString().trim().toUpperCase()
                                    localitaDef=cellaLocalitaBusiness.toString().trim().toUpperCase()
                                    capDef=cellaCapBusiness.toString().trim()
                                    telDef=cellaTelefonoBusiness.toString().trim()
                                    cellDef=cellaCellulareBusiness.toString().trim()
                                    if(cellDef.toString().contains(".")){
                                        def punto=cellDef.toString().indexOf(".")
                                        //cellaNoPolizza=Double.valueOf(cellaNoPolizza).longValue().toString()
                                        //cellDef=cellDef.toString().replaceAll("[^0-9]", "")
                                    }
                                    if(telDef.toString().contains(".")){
                                        def punto=telDef.toString().indexOf(".")
                                        //cellaNoPolizza=Double.valueOf(cellaNoPolizza).longValue().toString()
                                        // telDef=telDef.toString().replaceAll("[^0-9]", "")
                                    }
                                    if(cellaPIvaBusiness !='null'){
                                        cfDef=cellaPIvaBusiness.toString().trim().toUpperCase()
                                        /*if(!cfDef.toString().matches("^[a-z|A-Z]{6}[0-9]{2}[A-Z|a-z][0-9]{2}[A-Z|a-z][0-9]{3}[A-Z|a-z]\$") && !cfDef.matches("^0+\\d*(?:\\.\\d+)?\$")){
                                            cfDef=Double.valueOf(cfDef).longValue().toString()
                                        }
                                        if(!cfDef.matches("[0-9]{11}")){
                                        cfDef=cfDef.padLeft(11,"0")
                                        }
                                        */
                                    }

                                    indirizzoDef=cellaIndirizzoBusiness.toString().trim().toUpperCase()
                                    tipoClienteDef=TipoCliente.DITTA_INDIVIDUALE
                                    if(cellaEmailBusiness.toString().trim().size()>4){
                                        emailDef=cellaEmailBusiness.toString().trim()
                                    }
                                }else if(cliente){
                                    logg =new Log(parametri: "prendo i dati del cliente -->${cellaNome}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    def cellaProvincia=cell("N")?.value?:""
                                    if(cellaProvincia.toString().contains("\'")) cellaProvincia = cellaProvincia.replaceAll("\'","")
                                    def cellaLocalita=cell("L")?.value?:""
                                    if(cellaLocalita.toString().contains("\'")) cellaLocalita = cellaLocalita.replaceAll("\'","")
                                    def cellaCap=cell("O")?.value?:""
                                    if(cellaCap.toString().contains("\'")) cellaCap = cellaCap.replaceAll("\'","")
                                    def cellaTel=cell("P")?.value?:""
                                    if(cellaTel.toString().contains("\'")) cellaTel = cellaTel.replaceAll("\'","")
                                    def cellaCel=cell("Q")?.value?:""
                                    if(cellaCel.toString().contains("\'")) cellaCel = cellaCel.replaceAll("\'","")
                                    def cellaCF=cell("J")?.value?:""
                                    def cellaIndirizzo=cell("M")?.value?:""
                                    if(cellaIndirizzo.toString().contains("\'")) cellaIndirizzo = cellaIndirizzo.replaceAll("\'","")
                                    def cellaEmail=cell("R")?.value?:""
                                    if(cellaEmail.toString().contains("\'")) cellaEmail = cellaEmail.replaceAll("\'","")
                                    cognomeDef=cellaCognome.toString().trim().toUpperCase()
                                    nomeDef=cellaNome.toString().trim().toUpperCase()
                                    provinciaDef=cellaProvincia.toString().trim().toUpperCase()
                                    localitaDef=cellaLocalita.toString().trim().toUpperCase()
                                    capDef=cellaCap.toString().trim()
                                    telDef=cellaTel.toString().trim()
                                    cellDef=cellaCel.toString().trim()
                                    if(cellDef.toString().contains(".")){
                                        //def punto=cellDef.toString().indexOf(".")
                                        cellDef=cellDef.toString().replaceAll("[^0-9]", "")
                                    }
                                    if(telDef.toString().contains(".")){
                                        def punto=telDef.toString().indexOf(".")
                                        telDef=telDef.toString().replaceAll("[^0-9]", "")
                                    }
                                    if(cellaCF !='null'){
                                        cfDef=cellaCF.toString().trim().toUpperCase()
                                        if(cfDef){
                                            def annoCF=cfDef.substring(6,8)
                                            if(Integer.parseInt(annoCF) && Integer.parseInt(annoCF)<=40){
                                                sup75=true
                                            }
                                            def sessC=cfDef.substring(9,11)
                                            if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                                tipoClienteDef=TipoCliente.F
                                            }
                                        }
                                        /*if(!cfDef.toString().matches("^[a-z|A-Z]{6}[0-9]{2}[A-Z|a-z][0-9]{2}[A-Z|a-z][0-9]{3}[A-Z|a-z]\$") && !cfDef.matches("^0+\\d*(?:\\.\\d+)?\$")){
                                            cfDef=Double.valueOf(cfDef).longValue().toString()
                                        }

                                        if(!cellaCF.matches("[0-9]{11}")){
                                        cfDef=cfDef.padLeft(11,"0")
                                        }
                                        */
                                    }

                                    indirizzoDef=cellaIndirizzo.toString().trim().toUpperCase()
                                    if(cellaEmail.toString().trim().size()>4){
                                        emailDef=cellaEmail.toString().trim()
                                    }

                                }else{
                                    nonTrovato=true
                                }

                                if(nonTrovato){
                                    logg =new Log(parametri: "il beneficiario ${cellaBeneficiario} non appare nelle colonne dei dati coobligato/cliente/business, controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    errorePratica.add("per la pratica-->${cellnoPratica} il beneficiario non appare nelle colonne dei dati coobligato/cliente/business controllare file,")
                                    erroPolizza++
                                }else{
                                    def cellaNomeDealer=cell("F")?.value?:null
                                    if(cellaNomeDealer.toString().contains("\'")) cellaNomeDealer = cellaNomeDealer.replaceAll("\'","")
                                    def cellaProdotto=cell("AZ")?.value?:null
                                    if(cellaProdotto.toString().contains("\'")) cellaProdotto = cellaProdotto.replaceAll("\'","")
                                    def cellaDataImmatr=cell("AQ")?.value?:null
                                    if(cellaDataImmatr.toString().contains("\'")) cellaDataImmatr = cellaDataImmatr.replaceAll("\'","")
                                    def cellaDurata=cell("BA")?.value?:null
                                    //println "cell(\"AZ\") ${cellaDurata}"
                                    if(cellaDurata.toString().contains("\'")) cellaDurata = cellaDurata.replaceAll("\'","")
                                    def cellaMarca=cell("AR")?.value?:null
                                    if(cellaMarca.toString().contains("\'")) cellaMarca = cellaMarca.replaceAll("\'","")
                                    def cellaModello=cell("AS")?.value?:null
                                    if(cellaModello.toString().contains("\'")) cellaModello = cellaModello.replaceAll("\'","")
                                    def cellaBeneFinanziato=cell("AP")?.value?:null
                                    if(cellaBeneFinanziato.toString().contains("\'")) cellaBeneFinanziato = cellaBeneFinanziato.replaceAll("\'","")
                                    def cellapremioImponibile=cell("BB")?.value?:null
                                    if(cellapremioImponibile.toString().contains("\'")) cellapremioImponibile = cellapremioImponibile.replaceAll("\'","")
                                    /*def cellapremioLordo=cell("BB")?.value?:null*/
                                    def cellaprovvVendi=cell("BE")?.value?:null
                                    if(cellaprovvVendi.toString().contains("\'")) cellaprovvVendi = cellaprovvVendi.replaceAll("\'","")
                                    def cellaprovvDealer=cell("BD")?.value?:null
                                    if(cellaprovvDealer.toString().contains("\'")) cellaprovvDealer = cellaprovvDealer.replaceAll("\'","")
                                    def cellaprovvGmfi=cell("BF")?.value?:null
                                    if(cellaprovvGmfi.toString().contains("\'")) cellaprovvGmfi = cellaprovvGmfi.replaceAll("\'","")
                                    def cellaTelaio
                                    if(cell("AV")?.value !=null){
                                        cellaTelaio=cell("AV")?.value
                                        if(cellaTelaio.toString().contains("\'")) cellaTelaio = cellaTelaio.replaceAll("\'","")
                                    }
                                    def cellaTarga
                                    if(cell("AW")?.value !=null){
                                        cellaTarga=cell("AW")?.value
                                        if( cellaTarga.toString().contains("\'")) cellaTarga = cellaTarga.replaceAll("\'","")
                                    }
                                    def cellaValoreAssi=cell("AX")?.value?:null
                                    if(cellaValoreAssi.toString().contains("\'")) cellaValoreAssi = cellaValoreAssi.replaceAll("\'","")
                                    def cellaPrezzo=cell("AX")?.value?:null
                                    if(cellaPrezzo.toString().contains("\'")) cellaPrezzo = cellaPrezzo.replaceAll("\'","")
                                    def cellaVenditore=cell("BH")?.value?:null
                                    if(cellaVenditore.toString().contains("\'")) cellaVenditore = cellaVenditore.replaceAll("\'","")
                                    def cellaTipoPol=cell("BK")?.value?:null
                                    if(cellaTipoPol.toString().contains("\'")) cellaTipoPol = cellaVenditore.replaceAll("\'","")
                                    def leasing=false
                                    def onStar=false
                                    def dataInserimento=new Date()
                                    dataInserimento=dataInserimento.clearTime()
                                    def dataScadenza
                                    def durata=null
                                    def premioImponibile=0.0
                                    def premioImponibileFoglio=0.0
                                    def provvDealerFoglio=0.0
                                    def provvVendFoglio=0.0
                                    def provvGMFFoglio=0.0
                                    def premioLordo=0.0
                                    def provvVendi=0.0
                                    def provvDealer=0.0
                                    def provvGmfi=0.0
                                    def premioFisso=126.83
                                    def imposte=0.0
                                    if(cellaTipoPol.toString().trim().toLowerCase().contains("lease")){leasing=true}
                                    if((cellaDurata.toString().trim()!=null && cellaDurata.toString().trim()!='') && cellaDurata.toString().trim().length()>0){
                                        if(cellaDurata.toString().contains(".")){
                                            def punto=cellaDurata.toString().indexOf(".")
                                            cellaDurata=cellaDurata.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                        }
                                        durata=Integer.parseInt(cellaDurata.toString().trim())
                                        premioImponibile=premioFisso*durata
                                        premioImponibile=Math.round(premioImponibile * 100) / 100
                                        premioLordo = premioImponibile * (1.025)
                                        premioLordo=Math.round(premioLordo * 100) / 100
                                        provvGmfi=premioImponibile*0.38
                                        provvGmfi=Math.round(provvGmfi * 100) / 100
                                        if(dealer.percVenditore>0){
                                            provvDealer= (premioImponibile*0.1867)
                                            provvDealer=Math.round(provvDealer * 100) / 100
                                            provvVendi= (premioImponibile*0.0933)
                                            provvVendi=Math.round(provvVendi * 100) / 100
                                        }else{
                                            provvDealer= (premioImponibile*0.28)
                                            provvDealer=Math.round(provvDealer * 100) / 100
                                            provvVendi= (premioImponibile*0.0)
                                            provvVendi=Math.round(provvVendi * 100) / 100
                                        }
                                        if(cellaprovvDealer !=null && cellaprovvDealer !=''){
                                            if(cellaprovvDealer instanceof BigDecimal){
                                                cellaprovvDealer=cellaprovvDealer.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                                provvDealerFoglio = new BigDecimal(cellaprovvDealer)
                                            }else if(cellaprovvDealer instanceof Double){
                                                provvDealerFoglio = BigDecimal.valueOf(cellaprovvDealer)
                                                provvDealerFoglio=Math.round(provvDealerFoglio * 100) / 100
                                            }
                                            else {provvDealerFoglio=0.0}
                                            def diffProvvD=Math.abs(Math.round((provvDealer-provvDealerFoglio)*100)/100)
                                            if(diffProvvD>=0.05 && diffProvvD!=""){
                                                logg =new Log(parametri: "per la polizza ${cellnoPratica}  la provv. Dealer inserita nel foglio ${provvDealerFoglio} non corrisponde con la provv. Dealer calcolata : ${provvDealer} ", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                errorePratica.add("per la polizza ${cellnoPratica}  la provv. Dealer inserita nel foglio ${provvDealerFoglio} non corrisponde con la provv. Dealer calcolata : ${provvDealer},")
                                                erroPolizza++
                                            }
                                        }else{
                                            logg =new Log(parametri: "per la polizza ${cellnoPratica} il valore della provv. Dealer non e' stata inserita controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            errorePratica.add("per la polizza ${cellnoPratica} il valore della provv. Dealer non e' stata inserita controllare file,")
                                            erroPolizza++
                                            provvDealerFoglio=0.0
                                        }
                                        if(cellaprovvGmfi !=null && cellaprovvGmfi !=''){
                                            if(cellaprovvGmfi instanceof BigDecimal){
                                                cellaprovvGmfi=cellaprovvGmfi.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                                provvGMFFoglio = new BigDecimal(cellaprovvGmfi)
                                            }else if(cellaprovvGmfi instanceof Double){
                                                provvGMFFoglio = BigDecimal.valueOf(cellaprovvGmfi)
                                                provvGMFFoglio=Math.round(provvGMFFoglio * 100) / 100
                                            }else {provvGMFFoglio=0.0}
                                            def diffProvvGMF=Math.abs(Math.round((provvGmfi-provvGMFFoglio)*100)/100)
                                            if(diffProvvGMF>=0.05 && diffProvvGMF!=""){
                                                logg =new Log(parametri: "per la polizza ${cellnoPratica}  la provv. GMF inserita nel foglio ${provvGMFFoglio} non corrisponde con la provv. GMF calcolata : ${provvGmfi} ", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                errorePratica.add("per la polizza ${cellnoPratica}  la provv. GMF inserita nel foglio ${provvGMFFoglio} non corrisponde con la provv. GMF calcolata : ${provvGmfi},")
                                                erroPolizza++
                                            }

                                        }else{
                                            logg =new Log(parametri: "per la polizza ${cellnoPratica} il valore della provv. GMF non e' stata inserita controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            errorePratica.add("per la polizza ${cellnoPratica} il valore della provv. GMF non e' stata inserita controllare file,")
                                            erroPolizza++
                                            provvGMFFoglio=0.0
                                        }
                                        if(cellaprovvVendi !=null && cellaprovvVendi !=''){
                                            if(cellaprovvVendi instanceof BigDecimal){
                                                cellaprovvVendi=cellaprovvVendi.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                                provvVendFoglio = new BigDecimal(cellaprovvVendi)
                                            }else if(cellaprovvVendi instanceof Double){
                                                provvVendFoglio = BigDecimal.valueOf(cellaprovvVendi)
                                                provvVendFoglio=Math.round(provvVendFoglio * 100) / 100
                                            }else {provvVendFoglio=0.0}
                                            def diffProvvVend=Math.abs(Math.round((provvVendi-provvVendFoglio)*100)/100)
                                            if(diffProvvVend>=0.05 && diffProvvVend!=""){
                                                logg =new Log(parametri: "per la polizza ${cellnoPratica}  la provv. venditore inserita nel foglio ${provvVendFoglio} non corrisponde con la provv. venditore calcolata : ${provvVendi} ", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                errorePratica.add("per la polizza ${cellnoPratica}  la provv. venditore inserita nel foglio ${provvVendFoglio} non corrisponde con la provv. venditore calcolata : ${provvVendi},")
                                                erroPolizza++
                                            }

                                        }else{
                                            logg =new Log(parametri: "per la polizza ${cellnoPratica} il valore della provv. venditore non e' stata inserita controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            errorePratica.add("per la polizza ${cellnoPratica} il valore della provv. venditore non e' stata inserita controllare file,")
                                            erroPolizza++
                                            provvVendFoglio=0.0
                                        }
                                        imposte=premioImponibile *0.025
                                        if( dataDecorrenza!=null){
                                            if(durata==1){
                                                dataScadenza= use(TimeCategory) {dataDecorrenza +12.months}
                                            }else if( durata==2){
                                                dataScadenza= use(TimeCategory) {dataDecorrenza +24.months}
                                            }else if(durata==3){
                                                dataScadenza=use(TimeCategory) {dataDecorrenza +36.months}
                                            }else if(durata==4){
                                                dataScadenza= use(TimeCategory) {dataDecorrenza +48.months}
                                            }else if(durata==5){
                                                dataScadenza=use(TimeCategory) {dataDecorrenza +60.months}
                                            }else{
                                                dataScadenza=null
                                            }
                                        }else{
                                            logg =new Log(parametri: "la data decorrenza non e' stata inserita pertanto non e' possibile mettere una data scadenza, controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            errorePratica.add("la data decorrenza non e' stata inserita pertanto non e' possibile mettere una data scadenza controllare file,")
                                            erroPolizza++
                                            dataScadenza=null
                                        }
                                    }else{
                                        logg =new Log(parametri: "la durata non e' stata inserita, controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        errorePratica.add("la durata non e' stata inserita controllare file,")
                                        erroPolizza++

                                    }
                                    def coperturaR
                                    if(cellaProdotto.toString().trim()!=null && cellaProdotto.toString().trim()!=''){
                                        coperturaR=cellaProdotto

                                    }
                                    def dataImmatricolazione=null
                                    if(!(cellaDataImmatr.toString().trim().equals("null") || cellaDataImmatr.toString().trim()!='')){
                                        def newDate = Date.parse( 'MM/yyyy', cellaDataImmatr )
                                        dataImmatricolazione = newDate
                                    }else{
                                        dataImmatricolazione = new Date()
                                    }
                                    if(cellaMarca.toString().equals("null") && cellaMarca.toString().trim()!=''){
                                        cellaMarca=null;
                                    }else{
                                        cellaMarca=cellaMarca.toString().trim().toUpperCase()
                                    }
                                    if(cellaModello.toString().equals("null")&& cellaModello.toString().equals("")){
                                        cellaModello=null;
                                    }else{
                                        cellaModello=cellaModello.toString().trim().toUpperCase()
                                    }
                                    def nuovo=false
                                    if(cellaBeneFinanziato.toString().trim()!=null && cellaBeneFinanziato.toString().trim()!=''){
                                        if(cellaBeneFinanziato.toString().trim().toLowerCase().contains("seminuove")){
                                            nuovo=false
                                        }else{
                                            nuovo=true
                                        }
                                    }
                                    if(cellapremioImponibile !=null && cellapremioImponibile !=''){
                                        if(cellapremioImponibile instanceof BigDecimal){
                                            cellapremioImponibile=cellapremioImponibile.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                            premioImponibileFoglio = new BigDecimal(cellapremioImponibile)
                                        }else if(cellapremioImponibile instanceof Double){
                                            premioImponibileFoglio = BigDecimal.valueOf(cellapremioImponibile)
                                            premioImponibileFoglio=Math.round(premioImponibileFoglio * 100) / 100
                                        }else {premioImponibileFoglio=0.0}
                                        def diffPremioImpo=Math.abs(Math.round((premioImponibile-premioImponibileFoglio)*100)/100)
                                        if(diffPremioImpo>=0.05 && diffPremioImpo!=""){
                                            logg =new Log(parametri: "per la polizza ${cellnoPratica}  il premio imponibile inserito nel foglio ${premioImponibileFoglio} non corrisponde con il premio (imponibile: ${premioFisso} * la durata: ${durata}) -> ${premioImponibile}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            errorePratica.add("per la polizza ${cellnoPratica} il premio imponibile inserito nel foglio ${premioImponibileFoglio} non corrisponde con il premio (imponibile: ${premioFisso} * la durata: ${durata})--> ${premioImponibile},")
                                            erroPolizza++
                                        }

                                    }else{
                                        logg =new Log(parametri: "per la polizza ${cellnoPratica} il valore del premio imponibile non e' stato inserito controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        errorePratica.add("per la polizza ${cellnoPratica} il valore del premio imponibile non e' stato inserito controllare file,")
                                        erroPolizza++
                                        premioImponibileFoglio=0.0
                                    }

                                    def valoreAssicurato
                                    if((cellaValoreAssi.toString().trim()!=null && cellaValoreAssi.toString().trim()!='' )&& cellaValoreAssi.toString().length()>0){
                                        cellaValoreAssi=cellaValoreAssi.toString().trim()
                                        def counter = 0
                                        for( int i=0; i<cellaValoreAssi.length(); i++ ) {
                                            if( cellaValoreAssi.charAt(i) == '.' ) {
                                                counter++
                                            }
                                        }
                                        if(counter==1){
                                            def punto=cellaValoreAssi.indexOf(".")
                                            valoreAssicurato=cellaValoreAssi.substring(0,punto).replaceAll("[^0-9]", "")
                                        }else if(counter>1){
                                            def sdopto=cellaValoreAssi.indexOf(".", cellaValoreAssi.indexOf(".") + 1)
                                            valoreAssicurato=cellaValoreAssi.substring(0,sdopto).replaceAll("[^0-9]", "")
                                        }
                                        valoreAssicurato = new BigDecimal(valoreAssicurato)
                                    }else{
                                        valoreAssicurato=0.0
                                    }
                                    def prezzoVendita
                                    if((cellaPrezzo.toString().trim()!=null || cellaPrezzo.toString().trim()!='' ) && cellaPrezzo.toString().length()>0){
                                        prezzoVendita=cellaPrezzo.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                        //prezzoVendita=prezzoVendita.toString().replaceAll(",","\\.")
                                        prezzoVendita = new BigDecimal(prezzoVendita)
                                    }else{
                                        prezzoVendita=0.0
                                    }
                                    if(cellaVenditore.toString()!=null || cellaVenditore.toString()!=''){
                                        cellaVenditore=cellaVenditore.toString().trim()
                                    }else{
                                        cellaVenditore=""
                                    }
                                    if(dealer && cellnoPratica && cellaTelaio && cellaBeneficiario.toString()!='' ){
                                        if (!sup75){
                                            def polizzaPAI = PolizzaPaiPag.findOrCreateWhere(
                                                    annullata:false,
                                                    cap: capDef.toString().trim().padLeft(5,"0"),
                                                    cellulare:  cellDef.toString().trim(),
                                                    cognome:  cognomeDef.toString().trim(),
                                                    coperturaRichiesta: coperturaR,
                                                    dataDecorrenza:dataDecorrenza,
                                                    dataImmatricolazione: dataImmatricolazione,
                                                    dataInserimento: dataInserimento,
                                                    dataScadenza: dataScadenza,
                                                    dealer: dealer,
                                                    durata:durata,
                                                    email: emailDef.toString().trim(),
                                                    indirizzo: indirizzoDef.toString().trim().toUpperCase(),
                                                    localita: localitaDef.toString().trim().toUpperCase(),
                                                    marca: cellaMarca.toString().trim(),
                                                    modello: cellaModello.toString().trim(),
                                                    noPolizza: cellnoPratica.toString().trim(),
                                                    nome: nomeDef.toString().trim().toUpperCase(),
                                                    nuovo:nuovo,
                                                    onStar: onStar,
                                                    partitaIva: cfDef.toString().trim().toUpperCase(),
                                                    premioImponibile:premioImponibile,
                                                    premioLordo:premioLordo,
                                                    provincia: provinciaDef.toString().trim().toUpperCase(),
                                                    provvDealer:provvDealer,
                                                    provvGmfi:provvGmfi,
                                                    provvVenditore:provvVendi,
                                                    stato:StatoPolizza.POLIZZA,
                                                    tipoCliente:tipoClienteDef,
                                                    tipoPolizza:TipoPolizza.PAIPAGAMENTO,
                                                    step:"0",
                                                    telaio:cellaTelaio.toString().trim(),
                                                    //targa:cellaTarga?:cellaTarga.toString().trim(),
                                                    telefono:telDef.toString().trim(),
                                                    valoreAssicurato:valoreAssicurato,
                                                    valoreAssicuratoconiva:prezzoVendita,
                                                    certificatoMail:false,
                                                    codiceZonaTerritoriale: "1",
                                                    rappel:0.0,
                                                    codOperazione:"0",
                                                    dSlip:"S",
                                                    venditore:cellaVenditore?cellaVenditore.toString().trim().toUpperCase():"",
                                                    imposte:imposte
                                            )
                                            if(cellaTarga){
                                                polizzaPAI.targa=cellaTarga?:cellaTarga.toString().trim()
                                            }
                                            if(polizzaPAI.save(flush:true)) {
                                                logg =new Log(parametri: "la polizza ${cellnoPratica} viene salvata nel DB", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                flussiPAI.add("pratica-->${cellnoPratica} salvata nel DB,")
                                                def tracciatoPolizza= tracciatiService.generaTracciatoPAIPAgamento(polizzaPAI,leasing)
                                                if(!tracciatoPolizza.save(flush: true)){
                                                    logg =new Log(parametri: "errore creazione tracciato PAI", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    risposta.add(noPratica:cellnoPratica, errore:"Errore creazione tracciato PAI: ${tracciatoPolizza.errors}")
                                                    errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPolizza.errors},")
                                                    erroPolizza++
                                                }else{
                                                    flussiPAI.add("per la pratica-->${cellnoPratica} tracciato PAI PAGAMENTO generato,")
                                                    totale++
                                                    logg =new Log(parametri: "per la pratica-->${cellnoPratica} tracciato PAI PAGAMENTO generato", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                }
                                            }
                                            else{
                                                logg =new Log(parametri: "Errore creazione polizza:${cellnoPratica}--> ${polizzaPAI.errors}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                risposta.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: ${polizzaPAI.errors}")
                                                errorePratica.add(" pratica-->${cellnoPratica} Errore creazione polizza: ${polizzaPAI.errors},")
                                                erroPolizza++
                                            }
                                        }else{
                                            errorePratica.add("il beneficiario della pratica-->${cellnoPratica}  supera la eta' di 75 anni,")
                                            erroPolizza++
                                        }

                                    }else if(!dealer){
                                        logg =new Log(parametri: "Errore creazione polizza:${cellnoPratica} il dealer non e' presente", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        risposta.add("Errore creazione polizza:${cellnoPratica} il dealer non e' presente")
                                        errorePratica.add("Errore creazione polizza:${cellnoPratica} il dealer ${cellaDealer}- ${cellaNomeDealer} non e' presente,")
                                        erroPolizza++
                                    }else if(!cellnoPratica){
                                        logg =new Log(parametri: "Errore creazione polizza: non e' presente un numero di pratica riga ${j}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        risposta.add("Errore creazione polizza: non e' presente un numero di pratica riga ${j},")
                                        errorePratica.add("Errore creazione polizza: non e' presente un numero di polizza riga ${j},")
                                        erroPolizza++
                                    }else if(!cellaTelaio){
                                        logg =new Log(parametri: "Errore creazione polizza:${cellnoPratica} non e' presente un numero di telaio", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        risposta.add("Errore creazione polizza:${cellnoPratica} non e' presente un numero di telaio,")
                                        errorePratica.add("Errore creazione polizza:${cellnoPratica} non e' presente un numero di telaio,")
                                        erroPolizza++
                                    }
                                }
                            }else{
                                logg =new Log(parametri: "per la polizza ${cellnoPratica}-->i dati del beneficiaro non sono stati inseriti", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                risposta.add("per la polizza ${cellnoPratica}-->i dati del beneficiaro non sono stati inseriti,")
                                errorePratica.add("per la polizza ${cellnoPratica}-->i dati del beneficiaro non sono stati inseriti,")
                                erroPolizza++
                            }
                        }
                    }
                    j++
                    if(erroPolizza>0){ totaleErr++}
                }
            }
        }

        /*}catch (e){
            logg = new Log(parametri: "c'e' un problema con il file, ${e.toString()}", operazione: "associazione dataTariffa-telaio polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "c'e' un problema con il file, ${e.toString()}"
            errorePratica.add( "c'e' un problema con il file, ${e.toString()}")
        }*/
        if(errorePratica.size()>0){
            for (String s : errorePratica)
            {
                listErrori += s + "\r\n\r\n"
            }
        }
        if(flussiPAI.size()>0){
            for (String s : flussiPAI)
            {
                listFlussiPAI += s + "\r\n\r\n"
            }
        }
        if(flussiPAI.size()>0){
            rispostaMail=listFlussiPAI+="totale flussi generati ${totale}"
        }
        if(errorePratica.size()>0){

            rispostaMail=listErrori+="totale flussi non generati ${totaleErr}"
        }
        if(errorePratica.size()>0 || (flussiPAI.size()>0)){
            def inviaMailCaricamento = mailService.invioMailCaricamentoPolizzePaiPag(rispostaMail, fileNameRisposta)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto dello stato del caricamento delle polizze \u00E8 stata inviata", operazione: "invio riassunto polizze PAI Pagamento caricate", pagina: "Polizza PAI Pagamento")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail con il riassunto dello stato del caricamento delle polizze", operazione: "invio riassunto polizze PAI Pagamento caricate", pagina: "Polizza PAI Pagamento")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Non e' stato possibile inviare la mail con il riassunto dello stato del caricamento delle polizze \n ${inviaMailCaricamento}"
            }
        }
    }

}
