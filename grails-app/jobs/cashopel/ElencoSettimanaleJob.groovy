package cashopel

import cashopel.polizze.DocumentiSalesSpecialist
import cashopel.polizze.Polizza
import cashopel.polizze.TipoDoc
import cashopel.polizze.TipoDocSS
import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.JSch
import grails.transaction.Transactional
import grails.util.Environment

import java.util.zip.ZipOutputStream

@Transactional
class ElencoSettimanaleJob {
    def mailService
    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
           // cron name: "ElencoSettimanalePolizze", cronExpression: "0 00 13 ? * MON"
        }

    }

    def group = "ElencoSettimanalePolizze"
    def description = "job per inviare elenco settimanale delle polizze"
    def execute() {
        try {
            invioSettimanalePolizze()

        } catch (Exception e) {
            log.error e.message
        }
    }

    def invioSettimanalePolizze() {
        try {
            def response =[]
            def rispostaMail
            def dataodierna = new Date()
            dataodierna=dataodierna.clearTime()
            def streamRiassunti = new ByteArrayOutputStream()
            def zipRiassunti = new ZipOutputStream(streamRiassunti)
            rispostaMail="Riassunto invio settimanale polizze:\r\n"
            def fileNameRisposta = "risposta_.txt"
            def documenti = /*SqlLogger.log {*/DocumentiSalesSpecialist.createCriteria().list() {
                isNull ("dataCaricamento")
                eq "tipo",TipoDocSS.ELENCO_SETTIMANALE
            }
            if (documenti.size() > 0) {
                documenti.each { documento ->
                    def invioMailSettimanale=mailService.invioMailSettimanale(documento)
                    println invioMailSettimanale
                    if(invioMailSettimanale.contains("queued")||invioMailSettimanale.contains("sent") ||invioMailSettimanale.contains("scheduled") ){
                        println "la mail di invio settimanale polizze \u00E8 stata inviata ai destinatari segnalati"
                        def dataOdierna=new Date()
                        dataOdierna=dataOdierna.clearTime()
                        documento.dataCaricamento=dataOdierna
                        if(!documento.save(flush:true)){
                            println "la data caricamento del documento appena inviato non \u00E8 stata impostata ${documento.errors}"
                        }else{
                            def host,port,username,password
                            host = "5.249.141.51"
                            port = 22
                            username = "flussi-nais"
                            password = "XYZrrr655"
                            def jsch = new JSch()
                            def session = jsch.getSession(username, host, port)
                            def properties = new Properties()
                            properties.put("StrictHostKeyChecking", "no")
                            session.config = properties
                            session.userName = username
                            session.password = password
                            session.connect()
                            def channel = session.openChannel("sftp") as ChannelSftp
                            channel.connect()
                            println "Connected to $host:$port"
                            //channel.cd("flussi-bmw")
                            println "Current sftp directory: ${channel.pwd()}"
                            channel.cd("..")
                            channel.cd("/home/flussi-nais/FLUSSI cashOpel/ESTRAZIONI")
                            channel.put(new ByteArrayInputStream(documento.fileContent), "${documento.fileName}")
                            println "creazione estrazioni ${documento.fileName}"
                            println "Current sftp directory: ${channel.pwd()}"
                            channel.disconnect()
                            session.disconnect()
                        }
                    }else{
                        println invioMailSettimanale
                        response.add("Errore invio mail  polizza: ${invioMailSettimanale}")
                    }
                }
            }else{
                println "Non ci sono polizze generate in settimana"
            }
            /*response.collect{ commento ->
                if (commento?.messaggio){
                    rispostaMail =rispostaMail +" "+ commento.noDealer +" "+ commento.messaggio+"\r\n"
                }
            }.grep().join("\n")
            zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
            zipRiassunti.write(rispostaMail.bytes)*/
        } catch(e) { println "${e.toString()}" }
    }


}
