package cashopel

import au.com.bytecode.opencsv.CSVWriter
import cashopel.polizze.Documenti
import cashopel.polizze.TipoDoc
import cashopel.utenti.Log
import grails.transaction.Transactional
import grails.util.Environment

import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@Transactional
class CaricasuIAssicurJob {
    def ftpService
    static triggers = {

        if(Environment.current == Environment.PRODUCTION) {
            cron name: "CaricasuIAssicur", cronExpression: "0 30 20 * * ?"
        }

    }

    def group = "CaricasuIAssicur"
    def description = "job per caricare nel FTP di IASSICUR i certificati"
    def execute() {
        try {
            CaricasuIAssicur()

        } catch (Exception e) {
            log.error e.message
        }
    }

    def CaricasuIAssicur() {
        def logg
        try {
            def inputStream
            def documentiInseriti=[], errorePratica=[]

            def certificati=Documenti.createCriteria().list(){
                eq "caricatosuIassicur", false
                //ne "tipo", TipoDoc.PAIPAG
            }
            if (certificati.size() > 0) {
                logg =new Log(parametri: "Sono stati trovati ${certificati.size()}", operazione: "carica IASSICUR ", pagina: "JOB CARICA IASSICUR")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                certificati.each { certificato ->
                    def name="${certificato.codIAssicur}.${certificato.fileName}"
                    inputStream=certificato.fileContent
                     ftpService.uploadFile(certificato.fileContent, name)
                    //certificato.fileName=name
                    certificato.caricatosuIassicur=true
                    if(certificato.save(flush:true)){
                        println" ${certificato.fileName} aggiornato correttamente"
                        logg =new Log(parametri: "${certificato.fileName} aggiornato correttamente", operazione: "carica IASSICUR ", pagina: "JOB CARICA IASSICUR")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        documentiInseriti.add(" ${certificato.fileName} aggiornato correttamente")
                    }else{
                        println certificato.errors
                        println "Errore aggiornamento documento ${certificato.errors}"
                        logg =new Log(parametri: "Errore aggiornamento documento ${certificato.fileName}--> ${certificato.errors}", operazione: "carica IASSICUR ", pagina: "JOB CARICA IASSICUR")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        errorePratica.add(" Errore aggiornamento documento  ${certificato.errors}")
                    }
                }

            }else{
                logg =new Log(parametri: "Non ci sono polizze generate in giornata", operazione: "carica IASSICUR ", pagina: "JOB CARICA IASSICUR")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
            /*risposta.collect{ commento ->
                if (commento?.messaggio){
                    rispostaMail =rispostaMail +" "+ commento.noDealer +" "+ commento.messaggio+"\r\n"
                }
            }.grep().join("\n")
            zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
            zipRiassunti.write(rispostaMail.bytes)*/
        } catch(e) { println  e.toString() }

    }


}
