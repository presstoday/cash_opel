package cashopel

import cashopel.polizze.CodProdotti
import cashopel.polizze.Polizza
import cashopel.polizze.StatoPolizza
import cashopel.polizze.TipoCliente
import cashopel.polizze.TipoPolizza
import cashopel.polizze.ZoneTarif
import cashopel.utenti.Dealer
import cashopel.utenti.Log
import excel.reader.ExcelReader
import grails.transaction.Transactional
import groovy.time.TimeCategory

import java.util.regex.Pattern


@Transactional
class CaricaCSVFinLeasJob {
    def mailService
    def tracciatiService
    def DLWebService
    static triggers = {

        //cron name: "AssocTelaiDataFinLeas", cronExpression: "0 32 9 * * ?"
        /*if(Environment.current == Environment.PRODUCTION) {
            cron name: "GenCertFinLeasJob", cronExpression: "0 00 19 * * ?"
        }*/

    }

    def group = "CaricaCSVFinLeas"
    def description = "job per caricare pratiche csv polizze FIN/LEASING"
    def execute(context) {
        try {
            def fileAssocia = context.mergedJobDataMap.get('myInputStream')
            caricapratichecsv(fileAssocia)

        } catch (Exception e) {
            log.error e.message
        }
    }

    def caricapratichecsv(def myInputStream){
        def speciale=false
        def praticheNuove=[], errorePratica=[], flussiPAI=[]
        def dealerMancante=[]
        def filename=""
        def delimiter = ";"
        def fileReader = null
        def reader = null
        def erroreWS=""
        def dataPAIFin=new Date().parse("yyyyMMdd","20160930")
        def dataPAIIni=new Date().parse("yyyyMMdd","20160701")
        def listError =""
        def listPratiche=""
        def listFlussiPAI=""
        def listDeleted=""
        def totale=0
        def totaleErr=0
        def logg
        boolean isPai=false
        def rispostaMail = "Riassunto caricamento polizze FIN LEASING :\r\n"
        def fileNameRisposta = "riassunto_Caricamento_FinLeasing_${new Date().format("yyyyMMddHms")}_.txt"
        //def polizza
        def risposta =[]
        def polizzeDeleted =[]
        Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$)/
        Pattern pattern = ~/(\w+)/
        String regex = "<(\\S+)[^>]+?mso-[^>]*>.*?</\\1>"
        def x_value=[]
        def dealer
        def dealers=[]
        reader = new InputStreamReader(myInputStream)
        fileReader = new BufferedReader(reader)
        fileReader.readLine()
        try {
            def line = ""
            while ((line = fileReader.readLine()) != null) {
                def rispostaWebservice
                boolean sup75 = false
                boolean isDeleted = false
                def tokens = []
                tokens = line.split(delimiter)
                def cellType = tokens[0].toString().trim().replaceAll("'", "")
                def cellnoPratica = tokens[1].toString().trim().replaceAll("'", "")
                def cellDataDecorrenza = tokens[2].toString().trim().replaceAll("'", "")
                def cellaDealer = tokens[3].toString().trim().replaceAll("'", "")
                def cellaNomeDealer = tokens[4].toString().trim().replaceAll("'", "")
                def cellaCognome = tokens[5].toString().trim().replaceAll("'", "")
                def cellaNome = tokens[6].toString().trim().replaceAll("'", "")
                def cellaBeneficiario = tokens[61].toString().trim().replaceAll("'", "")
                def cellaCF = tokens[7].toString().trim().replaceAll("'", "")
                def cellaTel = tokens[8].toString().trim().replaceAll("'", "")
                def cellaCel = tokens[9].toString().trim().replaceAll("'", "")
                def cellaLocalita = tokens[10].toString().trim().replaceAll("'", "")
                def cellaIndirizzo = tokens[11].toString().trim().replaceAll("'", "")
                def cellaProvincia = tokens[12].toString().trim().replaceAll("'", "")
                def cellaCap = tokens[13].toString().trim().replaceAll("'", "")
                def cellaPIva = tokens[15].toString().trim().replaceAll("'", "")
                def cellaEmail = tokens[16].toString().trim().replaceAll("'", "")
                def cellaCoobligatoCognome = tokens[17].toString().trim().replaceAll("'", "")
                def cellaCoobligatoNome = tokens[18].toString().trim().replaceAll("'", "")
                def cellaCoobligatoCF = tokens[19].toString().trim().replaceAll("'", "")
                def cellaCoobligatoTEL = tokens[20].toString().trim().replaceAll("'", "")
                def cellaCoobligatoCEL = tokens[21].toString().trim().replaceAll("'", "")
                def cellaCoobligatoLOC = tokens[22].toString().trim().replaceAll("'", "")
                def cellaCoobligatoIND = tokens[23].toString().trim().replaceAll("'", "")
                def cellaCoobligatoPROV = tokens[24].toString().trim().replaceAll("'", "")
                def cellaCoobligatoCAP = tokens[25].toString().trim().replaceAll("'", "")
                def cellaCoobligatoemail = tokens[28].toString().trim().replaceAll("'", "")
                def cellaDenominazione = tokens[29].toString().trim().replaceAll("'", "")
                def cellaTelefonoBusiness = tokens[31].toString().trim().replaceAll("'", "")
                def cellaIndirizzoBusiness = tokens[32].toString().trim().replaceAll("'", "")
                def cellaCellulareBusiness = tokens[33].toString().trim().replaceAll("'", "")
                def cellaProvinciaBusiness = tokens[34].toString().trim().replaceAll("'", "")
                def cellaLocalitaBusiness = tokens[22].toString().trim().replaceAll("'", "")
                def cellaCapBusiness = tokens[35].toString().trim().replaceAll("'", "")
                def cellaPIvaBusiness = tokens[36].toString().trim().replaceAll("'", "")
                def cellaCFBusiness = tokens[37].toString().trim().replaceAll("'", "")
                def cellaEmailBusiness = tokens[38].toString().trim().replaceAll("'", "")
                def cellaBeneFinanziato = tokens[39].toString().trim().replaceAll("'", "")
                def cellaDataImmatr = tokens[40].toString().trim().replaceAll("'", "")
                def cellaMarca = tokens[41].toString().trim().replaceAll("'", "")
                def cellaModello = tokens[42].toString().trim().replaceAll("'", "")
                def cellaVersione = tokens[43].toString().trim().replaceAll("'", "")
                def cellaTelaio = tokens[45].toString().trim().replaceAll("'", "")
                def cellaTarga = tokens[46].toString().trim().replaceAll("'", "")
                def cellaPrezzo = tokens[47].toString().trim().replaceAll("'", "")
                def cellaStep = tokens[49].toString().trim().replaceAll("'", "")
                def cellaProdotto = tokens[51].toString().trim().replaceAll("'", "")
                def cellaDurata = tokens[52].toString().trim().replaceAll("'", "")
                def cellaValoreAssi = tokens[53].toString().trim().replaceAll("'", "")
                def cellaPacchetto = tokens[54].toString().trim().replaceAll("'", "")
                def cellaPremioImpo = tokens[56].toString().trim().replaceAll("'", "")
                def cellaPremioLordo = tokens[57].toString().trim().replaceAll("'", "")
                def cellaProvvDealer = tokens[58].toString().trim().replaceAll("'", "")
                def cellaProvvVenditore = tokens[59].toString().trim().replaceAll("'", "")
                def cellaProvvGMFI = tokens[60].toString().trim().replaceAll("'", "")
                def cellaCertificato = tokens[62].toString().trim().replaceAll("'", "")
                def cellaVenditore = tokens[65].toString().trim().replaceAll("'", "")
                def cellatipoPolizza = tokens[68].toString().trim().replaceAll("'", "")
                def cellacodCampagna = tokens[69].toString().trim().replaceAll("'", "")
                def dataDecorrenza = null
                if (cellDataDecorrenza.toString().trim() != 'null' && cellDataDecorrenza.toString().length() > 0) {
                    def newDate = Date.parse('yyyyMMdd', cellDataDecorrenza)
                    dataDecorrenza = newDate
                    if ((dataDecorrenza) && (dataDecorrenza >= dataPAIIni && dataDecorrenza <= dataPAIFin)) {
                        isPai = true
                    } else {
                        isPai = false
                    }
                }
                if (cellnoPratica.toString().equals("null")) {
                    cellnoPratica = ""
                } else {
                    cellnoPratica = cellnoPratica.toString()
                    cellnoPratica = cellnoPratica.toString().replaceFirst("^0*", "")
                    def indexslash = cellnoPratica.indexOf("/")
                    if (indexslash > 0) {
                        cellnoPratica = cellnoPratica.toString().substring(0, indexslash)
                    }
                    if (cellnoPratica.toString().contains(".")) {
                        def punto = cellnoPratica.toString().indexOf(".")
                        cellnoPratica = cellnoPratica.toString().substring(0, punto).replaceAll("[^0-9]", "")
                    }
                }
                def codOperazione = "0"
                if (cellType.toString().trim() != 'null') {
                    if (cellType.toString().trim().equalsIgnoreCase("NEW")) {
                        codOperazione = "0"
                    } else if (cellType.toString().trim().equalsIgnoreCase("DELETED")) {
                        isDeleted = true
                    } else if (cellType.toString().trim().equalsIgnoreCase("CLOSED")) {
                        codOperazione = "0"
                    } else {
                        codOperazione = "S"
                    }
                }
                if (isDeleted && cellnoPratica != "") {
                    polizzeDeleted.add("${cellnoPratica},")
                    logg = new Log(parametri: "Errore creazione polizza: la polizza e' tipo DELETED", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    risposta.add("Errore creazione polizza: la polizza ${cellnoPratica} e' tipo DELETED,")
                    errorePratica.add("Errore creazione polizza: la polizza ${cellnoPratica} e' tipo DELETED,")
                    totaleErr++
                } else if (!cellnoPratica) {
                    logg = new Log(parametri: "Errore creazione polizza: non \u00E8 presente un numero di pratica", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    risposta.add("Errore creazione polizza: non \u00E8 presente un numero di pratica,")
                    errorePratica.add("Errore creazione polizza: non \u00E8 presente un numero di polizza,")
                    totaleErr++
                } else {
                    if (cellaNome.toString().equals("null")) {
                        cellaNome = ""
                    } else {
                        cellaNome = cellaNome.toString().trim()
                    }
                    if (cellaCognome.toString().equals("null")) {
                        cellaCognome = ""
                    } else {
                        cellaCognome = cellaCognome.toString().trim()
                    }
                    if (cellaCap.toString().equals("null")) {
                        cellaCap = ""
                    } else {
                        cellaCap = cellaCap.toString().replaceAll("'", "")
                        if (cellaCap.toString().contains(".")) {
                            def punto = cellaCap.toString().indexOf(".")
                            cellaCap = cellaCap.toString().substring(0, punto)
                        }
                    }
                    if (cellaDealer.toString().equals("null")) {
                        cellaDealer = ""
                    } else {
                        cellaDealer = cellaDealer.toString().replaceAll("'", "")
                        cellaDealer = cellaDealer.toString().replaceFirst("^0*", "")
                        if (cellaDealer.toString().contains(".")) {
                            cellaDealer = Double.valueOf(cellaDealer).longValue().toString()
                            //cellaDealer=cellaDealer.toString().substring(0,punto).replaceAll("[^0-9]", "")
                        }
                        dealer = Dealer.findByCodice(cellaDealer.toString())
                    }
                    if (cellaCF.toString().equals("null")) {
                        cellaCF = cellaPIva.toString().replaceAll("[^a-z,A-Z,0-9]", "")
                    } else {
                        cellaCF = cellaCF.toString().replaceAll("[^a-z,A-Z,0-9]", "")
                    }
                    if (cellaMarca.toString().equals("null")) {
                        cellaMarca = null
                    } else {
                        cellaMarca = cellaMarca.toString().trim().toUpperCase()
                    }
                    if (cellaModello.toString().equals("null")) {
                        cellaModello = null
                    } else {
                        cellaModello = cellaModello.toString().trim().toUpperCase()
                    }
                    if (cellaTel.toString().trim() != 'null') {
                        cellaTel = cellaTel.toString().trim()
                        if (cellaTel.contains(".")) {
                            int zero = cellaTel.toString().indexOf(".")
                            cellaTel = cellaTel.toString().substring(0, zero)
                        }
                    } else {
                        cellaTel = null
                    }
                    if (cellaTelefonoBusiness.toString().trim() != 'null') {
                        cellaTelefonoBusiness = cellaTelefonoBusiness.toString().trim()
                        if (cellaTelefonoBusiness.contains(".")) {
                            int zero = cellaTelefonoBusiness.toString().indexOf(".")
                            cellaTelefonoBusiness = cellaTelefonoBusiness.toString().substring(0, zero)
                        }

                    } else {
                        cellaTelefonoBusiness = null
                    }
                    if (cellaCel.toString().trim() != 'null') {
                        cellaCel = cellaCel.toString().trim().replaceAll("[^0-9]+", "")
                        if (cellaCel.toString().contains(".")) {
                            int zero = cellaCel.toString().indexOf(".")
                            cellaCel = cellaCel.toString().substring(0, zero)
                        }
                    } else {
                        cellaCel = null
                    }
                    if (cellaCellulareBusiness.toString().trim() != 'null') {
                        cellaCellulareBusiness = cellaCellulareBusiness.toString().trim().replaceAll("[^0-9]+", "")
                        if (cellaCellulareBusiness.toString().contains(".")) {
                            int zero = cellaCellulareBusiness.toString().indexOf(".")
                            cellaCellulareBusiness = cellaCellulareBusiness.toString().substring(0, zero)
                        }
                    } else {
                        cellaCellulareBusiness = null
                    }
                    def coperturaR
                    if (cellaProdotto.toString().trim() != 'null' && cellaPacchetto.toString().trim().length() > 0) {
                        def pacchetto = ""
                        def indexdue = cellaProdotto.toString().trim().indexOf("-")
                        pacchetto = cellaProdotto.toString().trim().substring(0, indexdue)
                        pacchetto = pacchetto.toString().trim()
                        if (pacchetto == "A") {
                            coperturaR = "SILVER"
                        } else if (pacchetto == "B") {
                            coperturaR = "GOLD"
                        } else if (pacchetto == "C") {
                            coperturaR = "PLATINUM"
                        } else if (pacchetto == "D") {
                            coperturaR = "PLATINUM COLLISIONE"
                        } else if (pacchetto == "E") {
                            coperturaR = "PLATINUM KASKO"
                        }
                    }
                    def nuovo = false
                    if (cellaBeneFinanziato.toString().trim() != 'null') {
                        if (cellaBeneFinanziato.toString().trim().equalsIgnoreCase("NEW")) {
                            nuovo = true
                        } else {
                            nuovo = false
                        }
                    }
                    def polizzaTipo = ""
                    if (cellatipoPolizza.toString().trim() != 'null') {
                        if (cellatipoPolizza.toString().trim().equalsIgnoreCase("RETAIL")) {
                            polizzaTipo = TipoPolizza.FINANZIATE
                        } else {
                            polizzaTipo = TipoPolizza.LEASING
                        }
                    }
                    def codCampagna = ""
                    if (cellacodCampagna.toString().trim() != 'null') {
                        if (cellacodCampagna.toString().trim().equalsIgnoreCase("0")) {
                            codCampagna = "0"
                        } else {
                            codCampagna = cellacodCampagna.toString().trim()
                        }
                    }
                    boolean onStar = false
                    boolean certificato = false
                    if (cellaCertificato.toString().trim() != '') {
                        if (cellaCertificato.toString().trim() != "N") {
                            certificato = true
                        } else {
                            certificato = false
                        }
                    }
                    if (cellaTelaio.toString().trim() != '') {
                        cellaTelaio = cellaTelaio.toString().trim().toUpperCase()
                    } else {
                        cellaTelaio = null
                    }
                    if (cellaTarga.toString().trim() != '') {
                        cellaTarga = cellaTarga.toString().trim().toUpperCase()
                    } else {
                        cellaTarga = null
                    }
                    if (cellaEmail.toString().trim() != '') {
                        cellaEmail = cellaEmail.toString().trim()
                    } else {
                        cellaEmail = null
                    }
                    def dataScadenza
                    def durata
                    if (cellaDurata.toString().trim() != 'null' && cellaDurata.toString().trim().length() > 0) {
                        if (cellaDurata.toString().contains(".")) {
                            def punto = cellaDurata.toString().indexOf(".")
                            cellaDurata = cellaDurata.toString().substring(0, punto).replaceAll("[^0-9]", "")
                        }
                        durata = Integer.parseInt(cellaDurata.toString().trim())
                    } else {
                        durata = null
                    }
                    if (durata != null && dataDecorrenza != null) {

                        if (durata == 12) {
                            dataScadenza = use(TimeCategory) { dataDecorrenza + 12.months }
                        } else if (durata == 18) {
                            dataScadenza = use(TimeCategory) { dataDecorrenza + 18.months }
                        } else if (durata == 24) {
                            dataScadenza = use(TimeCategory) { dataDecorrenza + 24.months }
                        } else if (durata == 30) {
                            dataScadenza = use(TimeCategory) { dataDecorrenza + 30.months }
                        } else if (durata == 36) {
                            dataScadenza = use(TimeCategory) { dataDecorrenza + 36.months }
                        } else if (durata == 42) {
                            dataScadenza = use(TimeCategory) { dataDecorrenza + 42.months }
                        } else if (durata == 48) {
                            dataScadenza = use(TimeCategory) { dataDecorrenza + 48.months }
                        } else if (durata == 54) {
                            dataScadenza = use(TimeCategory) { dataDecorrenza + 54.months }
                        } else if (durata == 60) {
                            dataScadenza = use(TimeCategory) { dataDecorrenza + 60.months }
                        } else if (durata == 66) {
                            dataScadenza = use(TimeCategory) { dataDecorrenza + 66.months }
                        } else if (durata == 72) {
                            dataScadenza = use(TimeCategory) { dataDecorrenza + 72.months }
                        } else {
                            dataScadenza = null
                            durata = null
                        }
                    }
                    def step, codStep
                    if (cellaStep.toString().trim() != 'null') {
                        step = cellaStep.toString().trim()
                        if (step.contains(".")) {
                            int zero = step.toString().indexOf(".")
                            step = step.toString().substring(0, zero).toLowerCase()
                        }
                        if (step == "1") {
                            codStep = "step 1"
                        } else if (step == "2") {
                            codStep = "step 2"
                        } else if (step == "3") {
                            codStep = "step 3"
                        }
                    } else {
                        codStep = null
                    }
                    def dataImmatricolazione = null
                    if (cellaDataImmatr.toString().trim() != 'null' && cellaDataImmatr.toString().length() > 0) {
                        def newDate = Date.parse('yyyyMMdd', cellaDataImmatr)
                        dataImmatricolazione = newDate
                    }
                    def valoreAssicurato
                    if (cellaValoreAssi.toString().trim() != 'null' && cellaValoreAssi.toString().length() > 0) {
                        valoreAssicurato = cellaValoreAssi.toString().replaceAll("\\.", "").replaceAll(",", "\\.")
                        //valoreAssicurato=cellaValoreAssi.toString().replaceAll(",","\\.")
                        valoreAssicurato = new BigDecimal(valoreAssicurato)
                    } else {
                        valoreAssicurato = 0.0
                    }
                    def prezzoVendita
                    if (cellaPrezzo.toString().trim() != 'null' && cellaPrezzo.toString().length() > 0) {
                        prezzoVendita = cellaPrezzo.toString().replaceAll("\\.", "").replaceAll(",", "\\.")
                        //prezzoVendita=prezzoVendita.toString().replaceAll(",","\\.")
                        prezzoVendita = new BigDecimal(prezzoVendita)
                    } else {
                        prezzoVendita = 0.0
                    }
                    def premioImponibile
                    if (cellaPremioImpo.toString().trim() != 'null' && cellaPremioImpo.toString().trim().length() > 0) {
                        premioImponibile = cellaPremioImpo.toString().trim().replaceAll("\\.", "").replaceAll(",", "\\.")
                        //premioImponibile=cellaPremioImpo.toString().trim().replaceAll(",","\\.")
                        premioImponibile = new BigDecimal(premioImponibile)
                    } else {
                        premioImponibile = 0.0
                    }
                    def premioLordo
                    if (cellaPremioLordo.toString().trim() != 'null' && cellaPremioLordo.toString().trim().length() > 0) {
                        premioLordo = cellaPremioLordo.toString().trim().replaceAll("\\.", "").replaceAll(",", "\\.")
                        // premioLordo=cellaPremioLordo.toString().trim().replaceAll(",","\\.")
                        premioLordo = new BigDecimal(premioLordo)
                    } else {
                        premioLordo = 0.0
                    }
                    def provvDealer
                    if (cellaProvvDealer.toString().trim() != 'null' && cellaProvvDealer.toString().trim().length() > 0) {
                        provvDealer = cellaProvvDealer.toString().trim().replaceAll("\\.", "").replaceAll(",", "\\.")
                        //provvDealer=cellaProvvDealer.toString().trim().replaceAll(",","\\.")
                        provvDealer = new BigDecimal(provvDealer)
                    } else {
                        provvDealer = 0.0
                    }
                    def provvGmfi
                    if (cellaProvvGMFI.toString().trim() != 'null' && cellaProvvGMFI.toString().trim().length() > 0) {
                        provvGmfi = cellaProvvGMFI.toString().trim().replaceAll("\\.", "").replaceAll(",", ".")
                        //provvGmfi=cellaProvvGMFI.toString().trim().replaceAll(",",".")
                        provvGmfi = new BigDecimal(provvGmfi)
                    } else {
                        provvGmfi = 0.0
                    }
                    def provvVendi
                    if (cellaProvvVenditore.toString().trim() != '' && cellaProvvVenditore.toString().trim().length() > 0 && cellaProvvVenditore.toString().trim() != '0') {
                        provvVendi = cellaProvvVenditore.toString().trim().replaceAll("\\.", "").replaceAll(",", "\\.")
                        //provvVendi=cellaProvvVenditore.toString().trim().replaceAll(",","\\.")
                        provvVendi = new BigDecimal(provvVendi)
                    } else {
                        provvVendi = 0.0
                    }
                    if (cellaVenditore.toString() != 'null') {
                        cellaVenditore = cellaVenditore.toString().trim()
                    } else {
                        cellaVenditore = ""
                    }
                    def dataInserimento = new Date()
                    def premioLordoSAntifurto = ""
                    def premioLordoAntifurto = ""
                    dataInserimento = dataInserimento.clearTime()
                    def parole = []
                    def nomeB = "", cognomeB = ""
                    def cognomeDef, nomeDef, provinciaDef, localitaDef, capDef, telDef, cellDef, cfDef, indirizzoDef, tipoClienteDef, annoCF
                    def emailDef = ""
                    boolean denominazione = false
                    boolean coobligato = false
                    boolean cliente = false
                    boolean nonTrovato = false
                    if (cellaBeneficiario.toString() != '') {
                        cellaBeneficiario = cellaBeneficiario.toUpperCase()
                        logg = new Log(parametri: "beneficiario-->${cellaBeneficiario}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        if (cellaDenominazione) {
                            cellaDenominazione = cellaDenominazione.toUpperCase().trim()
                            if (cellaDenominazione.contains(cellaBeneficiario)) {
                                logg = new Log(parametri: "denominazione-->${cellaDenominazione}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                denominazione = true

                            } else {
                                denominazione = false
                            }
                        }
                        if (cellaCoobligatoNome) {
                            cellaCoobligatoNome = cellaCoobligatoNome.toUpperCase().trim()
                            cellaCoobligatoCognome = cellaCoobligatoCognome.toUpperCase().trim()
                            def nomeCooC = cellaCoobligatoNome + " " + cellaCoobligatoCognome
                            if (nomeCooC.contains(cellaBeneficiario)) {
                                logg = new Log(parametri: "nome Coobligato-->${nomeCooC}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                coobligato = true
                            } else {
                                coobligato = false
                            }
                        }
                        if (cellaNome) {
                            cellaNome = cellaNome.toUpperCase().trim()
                            cellaCognome = cellaCognome.toUpperCase().trim()
                            def nomeCom = cellaNome + " " + cellaCognome

                            if (nomeCom.contains(cellaBeneficiario)) {
                                logg = new Log(parametri: "nome Cliente-->${nomeCom} cella benificiario ${cellaBeneficiario}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                cliente = true
                            } else {
                                cliente = false
                            }
                        }

                        if (coobligato) {
                            logg = new Log(parametri: "prendo i dati del Coobligato-->${cellaCoobligatoNome}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            cognomeDef = cellaCoobligatoCognome.toString().trim().toUpperCase()
                            nomeDef = cellaCoobligatoNome.toString().trim().toUpperCase()
                            provinciaDef = cellaCoobligatoPROV.toString().trim().toUpperCase()
                            localitaDef = cellaCoobligatoLOC.toString().trim().toUpperCase()
                            capDef = cellaCoobligatoCAP.toString().trim().toUpperCase()
                            telDef = cellaCoobligatoTEL.toString().trim().toUpperCase()
                            cellDef = cellaCoobligatoCEL.toString().trim().toUpperCase()
                            cfDef = cellaCoobligatoCF.toString().trim().toUpperCase()
                            //prendo i dati dell'anno dal codice fiscale
                            if (cfDef) {
                                annoCF = cfDef.substring(6, 8)
                                if (Integer.parseInt(annoCF) && Integer.parseInt(annoCF) <= 40) {
                                    sup75 = true
                                }
                                def sessC=cfDef.substring(9,11)
                                if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                    tipoClienteDef=TipoCliente.F
                                }
                            }
                            indirizzoDef = cellaCoobligatoIND.toString().trim().toUpperCase()
                            //tipoClienteDef = TipoCliente.M
                            if (cellaCoobligatoemail.toString().trim().size() > 4) {
                                emailDef = cellaCoobligatoemail.toString().trim()
                            }
                        } else if (denominazione) {
                            logg = new Log(parametri: "prendo i dati della denominazione -->${cellaDenominazione}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            cognomeDef = cellaDenominazione.toString().trim().toUpperCase()
                            nomeDef = ""
                            provinciaDef = cellaProvinciaBusiness.toString().trim().toUpperCase()
                            localitaDef = cellaLocalitaBusiness.toString().trim().toUpperCase()
                            capDef = cellaCapBusiness.toString().trim().toUpperCase()
                            telDef = cellaTelefonoBusiness.toString().trim().toUpperCase()
                            cellDef = cellaCellulareBusiness.toString().trim().toUpperCase()
                            if (cellaPIvaBusiness) {
                                cfDef = cellaPIvaBusiness.toString().trim().toUpperCase()
                                if (!cfDef.matches("[0-9]{11}")) {
                                    cfDef = cfDef.padLeft(11, "0")
                                }

                            }


                            indirizzoDef = cellaIndirizzoBusiness.toString().trim().toUpperCase()
                            tipoClienteDef = TipoCliente.DITTA_INDIVIDUALE
                            if (cellaEmailBusiness.toString().trim().size() > 4) {
                                emailDef = cellaEmailBusiness.toString().trim()
                            }
                        } else if (cliente) {
                            logg = new Log(parametri: "prendo i dati del cliente -->${cellaNome}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            cognomeDef = cellaCognome.toString().trim().toUpperCase()
                            nomeDef = cellaNome.toString().trim().toUpperCase()
                            provinciaDef = cellaProvincia.toString().trim().toUpperCase()
                            localitaDef = cellaLocalita.toString().trim().toUpperCase()
                            capDef = cellaCap.toString().trim().toUpperCase()
                            telDef = cellaTel.toString().trim().toUpperCase()
                            cellDef = cellaCel.toString().trim().toUpperCase()
                            cfDef = cellaCF.toString().trim().toUpperCase()
                            if (cfDef) {
                                annoCF = cfDef.substring(6, 8)
                                if (Integer.parseInt(annoCF) && Integer.parseInt(annoCF) <= 40) {
                                    sup75 = true
                                }
                                def sessC=cfDef.substring(9,11)
                                if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                    tipoClienteDef=TipoCliente.F
                                }
                            }
                            indirizzoDef = cellaIndirizzo.toString().trim().toUpperCase()
                            //tipoClienteDef = TipoCliente.M
                            if (cellaEmail.toString().trim().size() > 4) {
                                emailDef = cellaEmail.toString().trim()
                            }

                        } else {
                            nonTrovato = true
                        }
                    }
                    if (nonTrovato) {
                        logg = new Log(parametri: "il beneficiario non appare nelle colonne dei dati coobligato/cliente/business, controllare file", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        errorePratica.add("per la pratica-->${cellnoPratica} il beneficiario non appare nelle colonne dei dati coobligato/cliente/business, controllare file,")
                        totaleErr++
                    } else {
                        //verifica tot provv rispetto a step assegnato
                        def provincia = provinciaDef.toString().trim()
                        def calcoloImp, totProvvi, diffImpoProvv
                        totProvvi = provvDealer + provvVendi + provvGmfi
                        if ((provincia.trim() != "") && (provincia.trim() != null) && (provincia.trim() != 'null')) {
                            def zonaT = ZoneTarif.findByProvincia(provincia).zona
                            if (zonaT != "8") {
                                if (step == "1") {
                                    calcoloImp = premioImponibile * 0.30
                                } else if (step == "2") {
                                    calcoloImp = premioImponibile * 0.40
                                } else if (step == "3") {
                                    calcoloImp = premioImponibile * 0.50
                                }
                            } else {
                                if (step == "1") {
                                    calcoloImp = premioImponibile * 0.27
                                } else if (step == "2") {
                                    calcoloImp = premioImponibile * 0.37
                                } else if (step == "3") {
                                    calcoloImp = premioImponibile * 0.47
                                }
                            }
                            diffImpoProvv = Math.abs(Math.round((calcoloImp - totProvvi) * 100) / 100)
                            logg = new Log(parametri: "differenza imponibile provvigioni -> ${diffImpoProvv}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            if (diffImpoProvv <= 0.05 && diffImpoProvv != "") {
                                if (dealer && cellnoPratica && cellaTelaio && cellaBeneficiario.toString() != '') {
                                    def polizzaesistente = Polizza.findByNoPolizzaIlike(cellnoPratica)
                                    if (polizzaesistente) {
                                        if (codOperazione == "S" && !speciale) {
                                            if (polizzaesistente.telaio.equalsIgnoreCase(cellaTelaio)) {
                                                polizzaesistente.codOperazione = codOperazione
                                                if (polizzaesistente.save(flush: true)) {
                                                    def tracciatoPolizzaR = tracciatiService.generaTracciatoIAssicur(polizzaesistente)
                                                    if (!tracciatoPolizzaR.save(flush: true)) {
                                                        logg = new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        risposta.add(noPratica: cellnoPratica, errore: "Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors}")
                                                        errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors},")
                                                        totaleErr++
                                                    } else {
                                                        def tracciatoComPolizzaR = tracciatiService.generaTracciatoCompagnia(polizzaesistente)
                                                        if (!tracciatoComPolizzaR.save(flush: true)) {
                                                            logg = new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            risposta.add(noPratica: cellnoPratica, errore: "Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors}")
                                                            errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors},")
                                                            totaleErr++
                                                        } else {
                                                            if (polizzaesistente.tracciatoPAIId) {
                                                                def tracciatoPAIPolizzaR = tracciatiService.generaTracciatoPAI(polizzaesistente, speciale)
                                                                if (!tracciatoPAIPolizzaR.save(flush: true)) {
                                                                    logg = new Log(parametri: "Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    risposta.add(noPratica: cellnoPratica, errore: "Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors}")
                                                                    errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors},")
                                                                    totaleErr++
                                                                } else {
                                                                    if (polizzaesistente.targa != null && polizzaesistente.targa != '') {
                                                                        polizzaesistente.targa = polizzaesistente.targa + "_R"
                                                                    }
                                                                    if (polizzaesistente.telaio != null && polizzaesistente.telaio != '') {
                                                                        polizzaesistente.telaio = polizzaesistente.telaio + "_R"
                                                                    }
                                                                    if (polizzaesistente.save(flush: true)) {
                                                                        logg = new Log(parametri: "targa e telaio per la polizza ${polizzaesistente.noPolizza} aggiornati", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        praticheNuove.add("polizza stornata correttamente ${polizzaesistente.noPolizza},")
                                                                        totale++
                                                                    } else {
                                                                        logg = new Log(parametri: "non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        risposta.add(noPratica: cellnoPratica, errore: "non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}")
                                                                        errorePratica.add(" pratica-->${cellnoPratica} non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors},")
                                                                        totaleErr++
                                                                    }
                                                                }
                                                            } else {
                                                                if (polizzaesistente.targa != null && polizzaesistente.targa != '') {
                                                                    polizzaesistente.targa = polizzaesistente.targa + "_R"
                                                                }
                                                                if (polizzaesistente.telaio != null && polizzaesistente.telaio != '') {
                                                                    polizzaesistente.telaio = polizzaesistente.telaio + "_R"
                                                                }
                                                                if (polizzaesistente.save(flush: true)) {
                                                                    logg = new Log(parametri: "targa e telaio per la polizza ${polizzaesistente.noPolizza} aggiornati", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    praticheNuove.add("polizza stornata correttamente ${polizzaesistente.noPolizza},")
                                                                    totale++
                                                                } else {
                                                                    logg = new Log(parametri: "non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    risposta.add(noPratica: cellnoPratica, errore: "non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}")
                                                                    errorePratica.add(" pratica-->${cellnoPratica} non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors},")
                                                                    totaleErr++
                                                                }
                                                            }

                                                        }
                                                    }
                                                }
                                            } else {
                                                risposta.add(noPratica: cellnoPratica, errore: "il telaio associato a questa pratica non corrisponde a quello inserito nel DB")
                                                errorePratica.add("per questa pratica-->${cellnoPratica} il telaio associato non corrisponde a quello inserito nel DB,")
                                                totaleErr++
                                            }
                                        } else if (speciale && !sup75) {
                                            logg = new Log(parametri: "genero tracciato PAI", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            def tracciatoPAI = tracciatiService.generaTracciatoPAI(polizzaesistente, speciale)
                                            if (!tracciatoPAI.save(flush: true)) {
                                                logg = new Log(parametri: "Errore creazione tracciato PAI: ${tracciatoPAI.errors}", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                risposta.add(noPratica: cellnoPratica, errore: "Errore creazione tracciato PAI: ${tracciatoPAI.errors}")
                                                errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAI.errors},")
                                                totaleErr++
                                            } else {
                                                logg = new Log(parametri: "tracciato PAI generato correttamente per la polizza ${polizzaesistente.noPolizza}", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                flussiPAI.add("tracciato PAI generato correttamente per la polizza ${polizzaesistente.noPolizza},")
                                                totale++
                                            }
                                        }/*else if(sup75){
                                                    errorePratica.add("il beneficiario della pratica-->${cellnoPratica}  supera la eta' di 75 anni,")
                                                    totaleErr++
                                                }*/
                                        else {
                                            errorePratica.add("la pratica-->${cellnoPratica} \u00E8 gia' stata caricata nel portale,")
                                            totaleErr++
                                        }
                                    } else {
                                        if (codOperazione == "S") {
                                            risposta.add(noPratica: cellnoPratica, errore: "la polizza che si sta cercando di stornare, non esiste, verificare no polizza")
                                            errorePratica.add("la pratica-->${cellnoPratica} che si sta cercando di stornare non esiste. Verificare no polizza,")
                                            totaleErr++
                                        } else if (speciale) {
                                            risposta.add(noPratica: cellnoPratica, errore: "la polizza non esiste e non e' possibile generare un flusso PAI... verificare no polizza")
                                            errorePratica.add("la pratica-->${cellnoPratica} non esiste e non e' possibile generare un flusso PAI... verificare no polizza,")
                                            totaleErr++
                                        } else {
                                                /****hacer llamada al ws**/
                                            def pacchetto=CodProdotti.findByCoperturaAndStep(coperturaR, codStep).codPacchetto
                                            def percentualeVenditore=dealer.percVenditore
                                            if(percentualeVenditore.toPlainString()=="0.0000000"){
                                                percentualeVenditore=0.0
                                            }
                                            def dealerType=dealer.dealerType
                                            if((provincia.trim()!="") && (provincia.trim() !='null') && pacchetto!="" && durata!=null && localitaDef.trim()!="" && localitaDef.trim()!='NULL' && dealerType!="" && step!=""&& percentualeVenditore!=""&& valoreAssicurato!="" && valoreAssicurato > 0.0 ){
                                                def dataTarriffa='2017-04-21'
                                                def tipoOperazione
                                                if(codOperazione=="0"){
                                                    tipoOperazione="A"
                                                }else{
                                                    tipoOperazione=codOperazione
                                                }

                                                rispostaWebservice=DLWebService.chiamataWSFINTAR2(pacchetto,valoreAssicurato,provincia,localitaDef,durata,dealer.dealerType,step, dataTarriffa,tipoOperazione,percentualeVenditore,premioLordo, cellnoPratica,polizzaTipo)

                                                logg = new Log(parametri: "risposta chiamata WS-->: ${rispostaWebservice}", operazione: "chiamata WS", pagina: "POLIZZE FIN/LEASING")
                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                if(rispostaWebservice.risposta==true){
                                                    def polizza = Polizza.findOrCreateWhere(
                                                            annullata: false,
                                                            cap: capDef.toString().trim().padLeft(5, "0"),
                                                            cellulare: cellDef.toString().trim(),
                                                            cognome: cognomeDef.toString().trim(),
                                                            coperturaRichiesta: coperturaR,
                                                            dataDecorrenza: dataDecorrenza,
                                                            dataImmatricolazione: dataImmatricolazione,
                                                            dataInserimento: dataInserimento,
                                                            dataScadenza: dataScadenza,
                                                            dealer: dealer,
                                                            durata: durata,
                                                            email: emailDef.toString().trim(),
                                                            indirizzo: indirizzoDef.toString().trim().toUpperCase(),
                                                            localita: localitaDef.toString().trim().toUpperCase(),
                                                            marca: cellaMarca,
                                                            modello: cellaModello,
                                                            noPolizza: cellnoPratica,
                                                            nome: nomeDef.toString().trim().toUpperCase(),
                                                            nuovo: nuovo,
                                                            onStar: rispostaWebservice.polizza.onStar,
                                                            partitaIva: cfDef.toString().trim().toUpperCase(),
                                                            premioImponibile: premioImponibile,
                                                            premioLordo: premioLordo,
                                                            provincia: provinciaDef.toString().trim().toUpperCase(),
                                                            provvDealer: provvDealer,
                                                            provvGmfi: provvGmfi,
                                                            provvVenditore: provvVendi,
                                                            stato: StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE,
                                                            tipoCliente: tipoClienteDef,
                                                            tipoPolizza: polizzaTipo,
                                                            step: codStep,
                                                            telaio: cellaTelaio,
                                                            targa: cellaTarga,
                                                            telefono: telDef.toString().trim(),
                                                            valoreAssicurato: valoreAssicurato,
                                                            valoreAssicuratoconiva: prezzoVendita,
                                                            certificatoMail: certificato,
                                                            codiceZonaTerritoriale: rispostaWebservice.polizza.codiceZonaTerritoriale,
                                                            rappel: rispostaWebservice.polizza.rappel,
                                                            codOperazione: codOperazione,
                                                            dSlip: "S",
                                                            venditore: cellaVenditore ? cellaVenditore.toString().trim().toUpperCase() : "",
                                                            imposte: rispostaWebservice.polizza.imposte,
                                                            codCampagna: codCampagna,
                                                            tariffa: 2,
                                                            premioCliente: rispostaWebservice.polizza.premioCliente,
                                                            premioLordoCliente: rispostaWebservice.polizza.premioLordoCliente,
                                                            premioLordoTerzi: rispostaWebservice.polizza.premioLordoTerzi,
                                                            premioTerzi: rispostaWebservice.polizza.premioTerzi
                                                    )
                                                    if (polizza.save(flush: true)) {
                                                        logg = new Log(parametri: "la polizza viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                                                        if(!tracciatoPolizza.save(flush: true)){
                                                            logg =new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        }else{
                                                            polizza.tracciato=tracciatoPolizza
                                                            logg =new Log(parametri: "genero tracciato IAssicur", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            def tracciatoComPolizza= tracciatiService.generaTracciatoCompagnia(polizza)
                                                            if(!tracciatoComPolizza.save(flush: true)){
                                                                logg =new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            } else{
                                                                polizza.tracciatoCompagnia=tracciatoComPolizza
                                                                logg =new Log(parametri: "genero tracciato Compagnia", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                if(polizza.save(flush:true)) {
                                                                    logg =new Log(parametri: "polizza creata con i suoi tracciati correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    praticheNuove.add("polizza caricata correttamente ${polizza.noPolizza},")
                                                                    //totaleErr++
                                                                }else{
                                                                    println "Errore creazione polizza: ${polizza.errors}"
                                                                    //praticheNuove.add("Errore creazione polizza: ${polizza.errors}")
                                                                    errorePratica.add("Errore creazione polizza: ${polizza.errors}")
                                                                    logg =new Log(parametri: "Errore aggiornamento tracciati polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }
                                                            }
                                                        }

                                                    } else {
                                                        logg = new Log(parametri: "Errore caricamento polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        //risposta.add(noPratica: cellnoPratica, errore: "Errore caricamento polizza: ${polizza.errors}")
                                                        errorePratica.add(" pratica-->${cellnoPratica} Errore caricamento polizza: ${polizza.errors},")
                                                        totaleErr++
                                                    }

                                                }else{
                                                    if(rispostaWebservice.errore!=''|| rispostaWebservice.errore.size()>0 || rispostaWebservice.errore!=null ){
                                                        println "questa è la risposta--> ${rispostaWebservice.errore}"
                                                        errorePratica.add( "errore chiamata al web service per la polizza--> ${cellnoPratica} -->errore : ${rispostaWebservice.errore}")
                                                        logg = new Log(parametri: "errore chiamata al web service per la polizza--> ${cellnoPratica} --> ${rispostaWebservice.errore}", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    }

                                                }
                                            }


                                        }
                                    }
                                } else if (!dealer) {
                                    logg = new Log(parametri: "Errore caricamento polizza: il dealer non \u00E8 presente", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    //risposta.add("Errore caricamento polizza: il dealer non \u00E8 presente")
                                    errorePratica.add("Errore caricamento polizza: il dealer ${cellaDealer}- ${cellaNomeDealer} non \u00E8 presente,")
                                    totaleErr++
                                } else if (!cellnoPratica) {
                                    logg = new Log(parametri: "Errore caricamento polizza: non \u00E8 presente un numero di pratica", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    //risposta.add("Errore caricamento polizza: non \u00E8 presente un numero di pratica,")
                                    errorePratica.add("Errore caricamento polizza: non \u00E8 presente un numero di polizza,")
                                    totaleErr++
                                }
                            } else {
                                logg = new Log(parametri: "Errore caricamento polizza:${cellnoPratica} la somma delle provviggioni-> ${totProvvi} non corrisponde con la percentuale imponibile dello step assegnato-> ${calcoloImp}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                //risposta.add("Errore caricamento polizza:${cellnoPratica} la somma delle provviggioni-> ${totProvvi} non corrisponde con la percentuale imponibile dello step assegnato-> ${calcoloImp},")
                                errorePratica.add("Errore caricamento polizza:${cellnoPratica} la somma delle provviggioni-> ${totProvvi} non corrisponde con la percentuale imponibile dello step assegnato-> ${calcoloImp},")
                                totaleErr++
                            }
                        } else {
                            logg = new Log(parametri: "Errore caricamento polizza:${cellnoPratica} la provincia non \u00E8 stata dichiarata, controllare il file", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            //risposta.add("Errore caricamento polizza:${cellnoPratica} la provincia non \u00E8 stata dichiarata, controllare il file,")
                            errorePratica.add("Errore caricamento polizza:${cellnoPratica} la provincia non \u00E8 stata dichiarata, controllare il file,")
                            totaleErr++
                        }
                    }
                }
            }

        }catch (e){
            logg = new Log(parametri: "c'e' un problema con il file, ${e.toString()}", operazione: "associazione dataTariffa-telaio polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "c'e' un problema con il file, ${e.toString()}"
            errore.add( "c'e' un problema con il file, ${e.toString()}")
        }
        if (praticheNuove.size() > 0) {
            praticheNuove.collect { commento ->
                rispostaMail = rispostaMail + " " + commento + "\r\n"
            }.grep().join("\n")
        }
        if(errorePratica.size()>0){
            errorePratica.collect { commento ->
                rispostaMail = rispostaMail + " " + commento + "\r\n"
            }.grep().join("\n")

        }
        if(errorePratica.size()>0 || (praticheNuove.size()>0)){
            /***INVIO LA MAIL A mach1 con il riepilogo dei certificati generati e i campioni dei certificati*/
            def inviaMailCaricamento = mailService.invioMailCaricamentoPolizzaFinLeas(rispostaMail, fileNameRisposta)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto dello stato del caricamento delle polizze \u00E8 stata inviata", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail con il riassunto dello stato del caricamento delle polizze", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Non e' stato possibile inviare la mail con il riassunto dello stato del caricamento delle polizze \n ${inviaMailCaricamento}"
            }
        }/*else {
            def inviaMailCaricamento = mailService.invioMailCaricamentoPolizzaFinLeas(rispostaMail, fileNameRisposta)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto del caricamento delle polizze \u00E8 stata inviata", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail la mail con il riassunto del caricamento delle polizze", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Non e' stato possibile inviare la mail la mail con il riassunto del caricamento delle polizze \n ${inviaMailCaricamento}"
            }
        }*/
    }
}
