package cashopel

import au.com.bytecode.opencsv.CSVWriter
import cashopel.polizze.Documenti
import cashopel.polizze.Polizza
import cashopel.polizze.StatoPolizza
import cashopel.polizze.TipoDoc
import cashopel.utenti.Log
import grails.transaction.Transactional
import grails.util.Environment

import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@Transactional
class CertificatiIASSICURJob {
    def mailService
    def IAssicurWebService
    def ftpService
    static triggers = {

        if(Environment.current == Environment.PRODUCTION) {
            cron name: "CertificatiIASSICUR", cronExpression: "0 00 19 * * ?"
        }

    }

    def group = "CertificatiIASSICUR"
    def description = "job per generare i certificati da IASSICUR"
    def execute() {
        try {
           CertificatiIASSICUR()

        } catch (Exception e) {
            log.error e.message
        }
    }

    def CertificatiIASSICUR() {
        def logg
        try {
            def resultwebS=IAssicurWebService.getPolizzeCASH()
            def polizze = resultwebS.polizze
            def fileNameCert,fileContentCert, fileNameWelcome,fileContentWelcome, documentofinale, codPolizza
            def documentiInseriti=[], errore=[]
            def streamCertMach1 = new ByteArrayOutputStream()
            def zipCertificati = new ZipOutputStream(streamCertMach1)
            def streamallCert = new ByteArrayOutputStream()
            def zipAllCertificati = new ZipOutputStream(streamallCert)
            def contDocs=0
            def rispostaMail="Riassunto invio certificati polizze :\r\n"
            def fileNameAllCert = "CASH_${new Date().format("ddMMyyyyHms")}.zip"
            def fileNameRisposta = "risposta_Invio_MailCert_${new Date().format("yyyyMMddHms")}_.txt"
            boolean cash=true
            if(polizze){
                def totale=0
                polizze.each { polizza ->
                    def ramo= polizza.ramo
                    codPolizza=polizza.codice
                    codPolizza=codPolizza.toString().replaceAll("/","-")
                    def noPolizza=polizza.noPolizza
                    def documentiCaricati=Documenti.findByNoPolizza(noPolizza.toString().trim())
                    if(!documentiCaricati){
                        //per le polizze  cash
                        mailService.generaCertIASSPDF(polizza).each{fileName, fileContent ->
                            fileNameCert = fileName
                            fileContentCert = fileContent
                        }
                        mailService.generaWelcomeIASSPDF(polizza).each {fileName, fileContent ->
                            fileNameWelcome = fileName
                            fileContentWelcome = fileContent
                        }
                        //creo un unico pdf
                        def streams = []
                        def baos2 = new ByteArrayOutputStream(fileContentCert.length)
                        baos2.write(fileContentCert, 0, fileContentCert.length)
                        def baos1 = new ByteArrayOutputStream(fileContentWelcome.length);
                        baos1.write(fileContentWelcome, 0, fileContentWelcome.length)
                        streams += baos1
                        streams += baos2
                        documentofinale=mailService.mergePdf(streams).toByteArray()

                        //salvo il file nel BD
                        def documenti=new Documenti()
                        documenti.tipo=TipoDoc.CASH
                        documenti.inviatoMail=false
                        documenti.fileName="${fileNameCert}"
                        documenti.fileContent=documentofinale
                        documenti.noPolizza="${noPolizza}"
                        documenti.codIAssicur="${codPolizza}"
                        documenti.cliente="${polizza.cliente}"
                        documenti.indirizzo="${polizza.indirizzo}"
                        documenti.cap="${polizza.cap}"
                        documenti.localita="${polizza.localita}"
                        documenti.provincia="${polizza.provincia}"
                        if(documenti.save(flush:true)){
                            logg =new Log(parametri: "il documento apartenente alla polizza ${noPolizza.toString().trim()} e' stato generato", operazione: "generazione certificati CASH", pagina: "JOB CASH IAssicur")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            def polizzaPortale=Polizza.findByNoPolizza(noPolizza.toString().trim())
                            if(polizzaPortale && polizzaPortale.stato.toString().contains("polizza in attesa di attivazione")){
                                zipAllCertificati.putNextEntry(new ZipEntry(documenti.fileName))
                                zipAllCertificati.write(documenti.fileContent)
                                if(contDocs<=3){
                                    contDocs++
                                    zipCertificati.putNextEntry(new ZipEntry(documenti.fileName))
                                    zipCertificati.write(documenti.fileContent)
                                    logg =new Log(parametri: "il certificato della polizza ${polizzaPortale.noPolizza} e' stato generato e inviato per controllo a MACH1", operazione: "invio certificati polizze CASH", pagina: "JOB INVIO CERTIFICATI CASH")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }
                                logg =new Log(parametri: "il certificato della polizza ${polizzaPortale.noPolizza} e' stato generato e inviato per controllo a MACH1", operazione: "invio certificati polizze CASH", pagina: "JOB INVIO CERTIFICATI CASH")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                documentiInseriti.add("il certificato della polizza ${polizzaPortale.noPolizza} e' stato stato generato e inviato per controllo a MACH1")

                                polizzaPortale.stato=StatoPolizza.POLIZZA
                                if(!polizzaPortale.save(flush: true)){
                                    logg =new Log(parametri: "non e' stato possibile aggiornare lo stato della polizza ${polizzaPortale.noPolizza}", operazione: "generazione certificati CASH", pagina: "JOB CASH IAssicur")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }else{
                                    logg =new Log(parametri: "polizza ${polizzaPortale.noPolizza} aggiornata", operazione: "generazione certificati CASH", pagina: "JOB CASH IAssicur")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }
                            }
                            totale++
                            logg =new Log(parametri: "documento ${documenti.fileName} -> ${noPolizza.toString().trim()}caricato nel sistema", operazione: "generazione certificati CASH", pagina: "JOB CASH IAssicur")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            //println" ${documenti.fileName} salvato correttamente"
                            //documentiInseriti.add(" ${documenti.fileName} salvato correttamente")
                        }else{
                            logg =new Log(parametri: "Errore salvataggio documento per la polizza ${noPolizza.toString().trim()}--> ${documenti.errors}", operazione: "generazione certificati CASH", pagina: "JOB CASH IAssicur")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            errore.add(" Errore generazione certificato  ${documenti.errors}")
                        }
                    }else{
                        errore.add("il documento ${noPolizza.toString().trim()} esiste gia' nel sistema")
                        logg =new Log(parametri: "il documento per la  polizza  ${noPolizza.toString().trim()} esiste gia' nel sistema", operazione: "generazione certificati CASH", pagina: "JOB CASH IAssicur")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }
                zipCertificati.close()
                streamCertMach1.close()
                zipAllCertificati.close()
                streamallCert.close()
                if(totale>0){
                    logg =new Log(parametri: "sono stati generati  ${totale} certificati", operazione: "generazione certificati CASH", pagina: "JOB CASH IAssicur")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    documentiInseriti.add("sono stati generati  ${totale} certificati")
                }
                if (documentiInseriti.size()>0){
                    documentiInseriti.collect{ commento ->
                        rispostaMail =rispostaMail +" "+ commento+"\r\n"
                    }.grep().join("\n")
                    def inputzipCert=new ByteArrayInputStream(streamallCert.toByteArray())
                    ftpService.uploadFileCertMACH1(fileNameAllCert, inputzipCert)
                }
                if(errore.size()>0){
                    errore.collect { commento ->
                        rispostaMail = rispostaMail + " " + commento + "\r\n"
                    }.grep().join("\n")

                }
                if(errore.size()>0 && !(documentiInseriti.size()>0)){
                    /***INVIO LA MAIL A mach1 con il riepilogo dei certificati generati e i campioni dei certificati*/
                    def inviaMailCaricamento = mailService.invioMailCaricamentoMach1(rispostaMail, fileNameRisposta, true, null)
                    if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                        logg =new Log(parametri: "la mail con il riassunto dell'invio delle mail dei certificati e' stata inviata", operazione: "invio certificati polizze CASH", pagina: "JOB INVIO CERTIFICATI CASH")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    } else {
                        logg =new Log(parametri: "Non e' stato possibile inviare la mail di carimenti falliti \n ${inviaMailCaricamento}", operazione: "invio certificati polizze CASH", pagina: "JOB INVIO CERTIFICATI CASH")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }else {
                    /***INVIO LA MAIL A mach1 con il riepilogo dei certificati generati e i campioni dei certificati*/
                    def inviaMailCaricamento = mailService.invioMailCaricamentoMach1(rispostaMail, fileNameRisposta, true, streamCertMach1)
                    if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                        logg =new Log(parametri: "la mail con il riassunto dell'invio delle mail dei certificati  e' stata inviata", operazione: "invio certificati polizze CASH", pagina: "JOB INVIO CERTIFICATI CASH")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    } else {
                        logg =new Log(parametri: "Non e' stato possibile inviare la mail di carimenti falliti \n ${inviaMailCaricamento}", operazione: "invio certificati polizze CASH", pagina: "JOB INVIO CERTIFICATI CASH")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }

            }else{
                logg =new Log(parametri: "non ci sono polizze da scaricare", operazione: "generazione certificati CASH", pagina: "JOB CASH IAssicur")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }

        } catch(e) {
            logg =new Log(parametri: "errore try cash ${e.toString()}", operazione: "generazione certificati CASH", pagina: "JOB CASH IAssicur")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
    }


}
