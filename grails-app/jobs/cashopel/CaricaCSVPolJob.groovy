package cashopel

import cashopel.polizze.*
import cashopel.utenti.Dealer
import cashopel.utenti.Log
import grails.transaction.Transactional
import groovy.time.TimeCategory

import java.util.regex.Pattern

@Transactional
class CaricaCSVPolJob {
    def mailService
    def caricamentiService
    def tracciatiService
    def DLWebService
    static triggers = {

        //cron name: "AssocTelaiDataFinLeas", cronExpression: "0 32 9 * * ?"
        /*if(Environment.current == Environment.PRODUCTION) {
            cron name: "GenCertFinLeasJob", cronExpression: "0 00 19 * * ?"
        }*/

    }

    def group = "CaricaCSVPol"
    def description = "job per caricare pratiche csv polizze "
    def execute(context) {
        try {
            def fileAssocia = context.mergedJobDataMap.get('myInputStream')
            caricapratichecsv(fileAssocia)

        } catch (Exception e) {
            log.error e.message
        }
    }

    def caricapratichecsv(def myInputStream){
        def praticheNuove=[], errorePratica=[]
        def polizzeRCACVT =[:]
        def fileReader = null
        def reader = null
        def logg
        def delimiter = ";"
        def rispostaMail = "Riassunto caricamento polizze:\r\n"
        def fileNameRisposta = "riassunto_Caricamento_Polizze_${new Date().format("yyyyMMddHms")}_.txt"

        //try {
        polizzeRCACVT=[:].withDefault { [] }
            myInputStream.eachLine { String line, count ->
                if(count!=0){
                    def splittato = line.split(";")
                    def cellnoPratica, cellaProdotto,cellaPacchetto
                    cellnoPratica = splittato[1]
                    cellaProdotto= splittato[51]
                    if (cellaProdotto.toString().trim() != 'null' && cellnoPratica.toString().trim().length() > 0) {
                        def pacchetto = ""
                        def indexdue = cellaProdotto.toString().trim().indexOf("-")
                        pacchetto = cellaProdotto.toString().trim().substring(0, indexdue)
                        pacchetto = pacchetto.toString().trim()
                        polizzeRCACVT["${cellnoPratica}"]<<pacchetto

                    }

                }
            }

        /*println "polizze rca--> $polizzeRCA"
        println "polizze CVT--> $polizzeCVT"
        println "polizze CVT E RCA --> $polizzeRCACVT"*/
           caricamentiService.caricapratiche(myInputStream, polizzeRCACVT)

        /*}catch (e){
            logg = new Log(parametri: "c'e' un problema con il file, ${e.toString()}", operazione: "associazione dataTariffa-telaio polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "c'e' un problema con il file, ${e.toString()}"
            errorePratica.add( "c'e' un problema con il file, ${e.toString()}")
        }*/
        if (praticheNuove.size() > 0) {
            praticheNuove.collect { commento ->
                rispostaMail = rispostaMail + " " + commento + "\r\n"
            }.grep().join("\n")
        }
        if(errorePratica.size()>0){
            errorePratica.collect { commento ->
                rispostaMail = rispostaMail + " " + commento + "\r\n"
            }.grep().join("\n")

        }
        /*if(errorePratica.size()>0 || (praticheNuove.size()>0)){
            def inviaMailCaricamento = mailService.invioMailCaricamentoPolizzaFinLeas(rispostaMail, fileNameRisposta)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto dello stato del caricamento delle polizze \u00E8 stata inviata", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail con il riassunto dello stato del caricamento delle polizze", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Non e' stato possibile inviare la mail con il riassunto dello stato del caricamento delle polizze \n ${inviaMailCaricamento}"
            }
        }else {
            def inviaMailCaricamento = mailService.invioMailCaricamentoPolizzaFinLeas(rispostaMail, fileNameRisposta)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto del caricamento delle polizze \u00E8 stata inviata", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail la mail con il riassunto del caricamento delle polizze", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Non e' stato possibile inviare la mail la mail con il riassunto del caricamento delle polizze \n ${inviaMailCaricamento}"
            }
        }*/
    }

}
