package cashopel

import cashopel.polizze.Documenti
import cashopel.polizze.TipoDoc
import cashopel.utenti.Log
import grails.transaction.Transactional
import grails.util.Environment


@Transactional
class CertificatiRinnoviIASSICURJob {
    def mailService
    def IAssicurWebService
    static triggers = {

        if(Environment.current == Environment.PRODUCTION) {
            cron name: "CertificatiRinnoviIASSICUR", cronExpression: "0 30 19 * * ?"
        }

    }

    def group = "CertificatiRinnoviIASSICUR"
    def description = "job per generare i certificati rinnovi da IASSICUR"
    def execute() {
        try {
           CertificatiRinnoviIASSICUR()

        } catch (Exception e) {
            log.error e.message
        }
    }

    def CertificatiRinnoviIASSICUR() {
        def logg
        try {
            def resultwebS=IAssicurWebService.getPolizzeRinnovi()
            def polizze = resultwebS.polizze
            def fileNameCert,fileContentCert, fileNameWelcome,fileContentWelcome, documentofinale, codPolizza
            if(polizze){

                def totale=0
                polizze.each { polizza ->
                    def ramo= polizza.ramo
                    codPolizza=polizza.codice
                    codPolizza=codPolizza.toString().replaceAll("/","-")
                    //per le polizze rinnovi
                    def noPolizza=polizza.noPolizza
                    def documentiCaricati=Documenti.findByNoPolizza(noPolizza.toString().trim())
                    if(!documentiCaricati){
                        mailService.generaCertRinnIASSPDF(polizza).each{fileName, fileContent ->
                            fileNameCert = fileName
                            fileContentCert = fileContent
                        }
                        mailService.generaWelcomeIASSPDF(polizza).each {fileName, fileContent ->
                            fileNameWelcome = fileName
                            fileContentWelcome = fileContent
                        }
                        //creo un unico pdf
                        def streams = []
                        def baos2 = new ByteArrayOutputStream(fileContentCert.length)
                        baos2.write(fileContentCert, 0, fileContentCert.length)
                        def baos1 = new ByteArrayOutputStream(fileContentWelcome.length);
                        baos1.write(fileContentWelcome, 0, fileContentWelcome.length)
                        streams += baos1
                        streams += baos2
                        documentofinale=mailService.mergePdf(streams).toByteArray()
                        //salvo il file nel BD
                        def documentiRinnovi=new Documenti()
                        documentiRinnovi.tipo=TipoDoc.RINNOVI
                        documentiRinnovi.inviatoMail=true
                        documentiRinnovi.fileName="${fileNameCert}"
                        documentiRinnovi.fileContent=documentofinale
                        documentiRinnovi.noPolizza="${noPolizza}"
                        documentiRinnovi.codIAssicur="${codPolizza}"
                        documentiRinnovi.cliente="${polizza.cliente}"
                        documentiRinnovi.indirizzo="${polizza.indirizzo}"
                        documentiRinnovi.cap="${polizza.cap}"
                        documentiRinnovi.localita="${polizza.localita}"
                        documentiRinnovi.provincia="${polizza.provincia}"
                        if(documentiRinnovi.save(flush:true)){
                            logg =new Log(parametri: "documento ${documentiRinnovi.fileName} --> ${noPolizza.toString().trim()} caricato nel sistema", operazione: "generazione certificati Rinnovi", pagina: "JOB RINNOVI IAssicur")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            totale++
                        }else{
                            logg =new Log(parametri: "Errore salvataggio per la polizza ${noPolizza.toString().trim()} -- documento ${documentiRinnovi.errors}", operazione: "generazione certificati Rinnovi", pagina: "JOB RINNOVI IAssicur")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }
                    }else{
                        logg =new Log(parametri: "il documento ${noPolizza.toString().trim()} esiste gia' nel sistema", operazione: "generazione certificati Rinnovi", pagina: "JOB RINNOVI IAssicur")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }
                if(totale>0){
                    logg =new Log(parametri: "sono stati generati  ${totale} certificati", operazione: "generazione certificati Rinnovi", pagina: "JOB RINNOVI IAssicur")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }
            }else{
                logg =new Log(parametri: "non ci sono polizze da scaricare", operazione: "generazione certificati Rinnovi", pagina: "JOB RINNOVI IAssicur")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
        } catch(e) {
            logg =new Log(parametri: "errore try cash ${e.toString()}", operazione: "generazione certificati Rinnovi", pagina: "JOB RINNOVI IAssicur")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
    }


}
