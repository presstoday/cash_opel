package cashopel

import cashopel.polizze.Polizza
import cashopel.utenti.Log
import grails.transaction.Transactional
import grails.util.Environment

import java.util.zip.ZipOutputStream

@Transactional
class ElencoSemestraleGennaioMACH1Job {
    def mailService
    static triggers = {
        if(Environment.current == Environment.PRODUCTION) {
            cron name: "ElencoSemestraleGennaioPolizzeMACH1", cronExpression: "0 45 9 1 1 ? *"
        }
    }

    def group = "ElencoSemestraleGennaioPolizzeMACH1"
    def description = "job per inviare elenco semestrale gennaio delle polizze a MACH1"
    def execute() {
        try {
            invioSemestraleGennaioPolizzeMACH1()

        } catch (Exception e) {
            log.error e.message
        }
    }

    def invioSemestraleGennaioPolizzeMACH1() {
        def logg
        try {
            logg =new Log(parametri: "inizio a preparare il file semestrale Gennaio", operazione: "invio semestrale Gennagio MACH1 delle polizze generate portale", pagina: "JOB ELENCO SEMESTRALE GENNAIO MACH1")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "inizio a preparare il file semestrale Gennaio"
            def response =[]
            def rispostaMail
            def streamRiassunti = new ByteArrayOutputStream()
            def zipRiassunti = new ZipOutputStream(streamRiassunti)
            rispostaMail="Riassunto invio semestrale gennaio polizze:\r\n"
            def fileNameRisposta = "risposta_.txt"
            int year = Calendar.getInstance().get(Calendar.YEAR)
            year=year-1
            def inizio = "${year}0701"
            def fine = "${year}1231"
            Date data_inizio = Date.parse( 'yyyyMMdd', inizio)
            Date data_fine = Date.parse( 'yyyyMMdd', fine)
            def polizze = /*SqlLogger.log {*/Polizza.createCriteria().list() {
                ge "dataInserimento", data_inizio
                le "dataInserimento", data_fine
                isNotNull "tracciato"
                isNotNull "tracciatoCompagnia"
            }
            if (polizze.size() > 0) {
                def invioMailSemestrale=mailService.invioMailSemestraleGennaioMACH1()
                logg =new Log(parametri: "risultato inviomail Semestrale ${invioMailSemestrale}", operazione: "invio semestrale Gennagio MACH1 delle polizze generate portale", pagina: "JOB ELENCO SEMESTRALE GENNAIO MACH1")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(invioMailSemestrale.contains("queued")||invioMailSemestrale.contains("sent") ||invioMailSemestrale.contains("scheduled") ){
                    logg =new Log(parametri: "la mail di invio semestrale polizze \u00E8 stata inviata a MACH1 per controllo", operazione: "invio semestrale Gennagio MACH1 delle polizze generate portale", pagina: "JOB ELENCO SEMESTRALE GENNAIO MACH1")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }else{
                    logg =new Log(parametri: "Errore invio mail  polizza: ${invioMailSemestrale}", operazione: "invio semestrale Gennagio MACH1 delle polizze generate portale", pagina: "JOB ELENCO SEMESTRALE GENNAIO MACH1")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    response.add("Errore invio mail  polizza: ${invioMailSemestrale}")
                }
            }else{
                logg =new Log(parametri: "Non ci sono polizze generate in questo semestre", operazione: "invio semestrale Gennagio MACH1 delle polizze generate portale", pagina: "JOB ELENCO SEMESTRALE GENNAIO MACH1")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
            /*response.collect{ commento ->
                if (commento?.messaggio){
                    rispostaMail =rispostaMail +" "+ commento.noDealer +" "+ commento.messaggio+"\r\n"
                }
            }.grep().join("\n")
            zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
            zipRiassunti.write(rispostaMail.bytes)*/
        } catch(e) {
            logg =new Log(parametri: "errore try cash ${e.toString()}", operazione: "invio semestrale Gennagio MACH1 delle polizze generate portale", pagina: "JOB ELENCO SEMESTRALE GENNAIO MACH1")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
    }
}
