<html>
<head>
    <meta name="layout" content="main">
    <title>Login</title>

</head>
<body>
<div class="row animated slideInDown">
    <div class="col-xs-10 col-md-12">
        <div class="login-panel">
            <div class="row">
                <div class="col-md-11  col-xs-11 col-md-push-0 col-xs-push-1 text-center divLogo">
                    <asset:image src="loghi/opel_financial_services.png" height="80px" id="logo-mach1"/>
                </div>
                <g:renderFlash/>
                <f:form>
                    <div class="col-md-11  col-xs-11 col-md-push-3 col-xs-push-2 text-center" style="margin-bottom: 40px;">
                        <sec:authenticationError/>
                    </div>
                    <div class="col-md-6  col-xs-10 col-md-push-4 text-center">
                        <div class="col-md-10 col-xs-10">
                            <input type="text" align="center" class="form-control input-login" name="username"  id="username" required="true"  placeholder="utente" >
                        </div>

                        <div class="col-md-10 col-xs-10 ">
                            <input type="text" align="center" class="form-control input-login" name="password"  id="password" required="true"  placeholder="contrasegna" >
                        </div>
                        <div class="col-md-10 col-xs-10 col-md-pull-4">
                            <label for="remember-me" class="checkbox chk-login divRicordarmi" >
                                <input type="checkbox"  id="remember-me" name="remember-me" value="1" > Ricordami
                            </label>
                        </div>
                        <div class="col-md-8 col-xs-6 col-md-pull-2 divLogin">
                            <button class="btnLogin" type="submit"><b>Login</b></button>
                        </div>
                    </div>
                </f:form>
                <div class="col-md-10 col-xs-12">
                    <footer class="col-md-10 col-md-push-0 col-xs-10 col-xs-push-1" >
                        <div class="col-md-10 col-md-push-2 col-xs-10 col-xs-push-1">
                            <p class="text-center"><asset:image src="/loghi/logo_mach1_small.png" height="30px" style="margin-top:150px; margin-left:80px;" /></p>
                        </div>
                        <div  class="col-md-9 col-md-push-3 col-xs-12" style="margin-left: 30px;">
                            <p class="testofooter" align="center"> Mach1 s.r.l. - Via Vittor Pisani, 13/B  – 20124 Milano - TEL. 02 30465068 FAX 02 62694254 - Email: attivazioni.ofs@mach-1.it - CCIAA Milano - REA MI 1908726 - C.F.,  P. IVA e Reg. Imprese Milano 06680830962 - Capitale sociale 100.000 EURO - Registro Unico Intermediari  A000317603 - Banco Posta Sede di Milano, Piazza Cordusio, 1 - IBAN IT 54 C 07601 01600 0000 99701393</p>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>