<g:applyLayout name="main">
<html >
    <head>
        <title>Elenco</title>
    </head>
    <body>
    %{--<div class="con">
        <div id="header" class="navbar navbar-defaultRCA ">
            <div class="navbar-header">
                <p><asset:image src="/loghi/logo_mach1_small.png" height="30px" style="margin-top: 90px; margin-left: 147px;" /></p>
            </div>
            <div class="nav navbar-nav navbar-left" >
                <ul >
                    <span style="margin-bottom: 30px;"><a href="#" class="btn btn-ricerca" id="menu-toggle" onclick="toggleTable();"><i class="fa fa-bars"></i></a> </span>

                </ul>
            </div>
            <div class="nav navbar-nav navbar-right">
                <ul>
                    <span style="color: #fff; font-size: 13px; margin-right: 25px;">
                        Buongiorno, <sec:loggedInUser/> &nbsp;
                        <a href="#" style="color: #fff!important;">
                            <g:link uri="/logout"><i class="fa fa-sign-out"></i>&nbsp; Log out </g:link>
                        </a>
                    </span>
                </ul>

            </div>
            <br>
            <br>
            <br>
            <nav class="collapse navbar-collapse colore" style="margin-right:6px;border-radius: 1px; ">
                <ul class="nav navbar-nav" style="margin-left: 7px;">
                    <li><h2 class="testo-navBarTitolo">Elenco Polizze RCA</h2></li>
                </ul>
            </nav>
        </div>
    </div>--}%
            <div class="bs-component">
                <nav role="navigation" class="navbar navbar-default">
                    <div class="navbar-header">
                        <asset:image src="/loghi/opel_financial_services.png" style="height:50px; margin-top: 7px; margin-left: 10px;" />
                    </div>
                    <!-- Collection of nav links, forms, and other content for toggling -->
                    <div id="navbarCollapse" class="navbar-collapse">
                        <ul class="nav navbar-nav">
                            <li><h3 class="testo-navBarTitolo">RCA OPEL</h3></li>
                        </ul>
                        <ul class="nav navbar-nav navbar-right" style="font-size: 26px; margin-top: 7px;">
                            <sec:ifHasRole role="ADMIN">
                                <li class="dropdown">
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle">Menu polizze <b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li class="" id="polizzeFIN"><a href="${createLink(controller: "polizze", action: "listFinanziate")}" class="menu_poli">Polizze finanziate/leasing</a></li>
                                        <li class="" id="polizzeAnnulla"><a href="${createLink(controller: "polizze", action: "annullamenti")}" class="menu_poli">Annullamenti</a></li>
                                        <li class="" id="polizzePAI"><a href="${createLink(controller: "polizze", action: "listPaiPag")}" class="menu_poli">Polizze PAI pagamento</a></li>
                                    </ul>
                                </li>
                            </sec:ifHasRole>

                            <li>
                                <g:link uri="/logout"><p>Esci</p></g:link>
                            </li>
                        </ul>
                    </div>
                </nav>
            </div>

    <div class="row">
        <div class="col-md-2">

            <ul class="nav nav-pills nav-stacked" id="menu">
                %{--<%System.out.println "${request.getRequestURI().toString()}"%>--}%
                <g:if test="${request.getRequestURI().toString().contains("/polizzeRCA/listaRCA")}">
                    <li class="active">
                        <a href="${createLink( controller:"polizzeRCA", action: "listaRCA",)}" ><span class="fa-lg"><i class="fa fa-wrench fa-stack-3x "></i></span><b> POLIZZE RCA</b></a>
                    </li>
                    <li>
                        <a href="${createLink(controller:"polizzeRCA", action: "dacontattare",)}"><span class="fa-lg"><i class="fa fa-id-card-o fa-stack-3x"></i></span><b> DA CONTATTARE</b></a>
                    </li>
                   <li>
                        <a href="${createLink(controller:"polizzeRCA", action: "annullate",)}"><span class="fa-lg"><i class="fa fa-trash fa-stack-3x"></i></span><b> ANNULLATE</b></a>
                    </li>
                    <li>
                        <a href="${createLink(controller:"polizzeRCA", action: "ofsgen",)}"><span class="fa-lg"><i class="fa fa-table fa-stack-3x"></i></span><b> OFSGEN</b></a>
                    </li>
                </g:if>
                <g:elseif test="${request.getRequestURI().toString().contains("/polizzeRCA/dacontattare")}">
                    <li>
                        <a href="${createLink( controller:"polizzeRCA", action: "listaRCA",)}"><span class="fa-lg"><i class="fa fa-wrench fa-stack-3x "></i></span> <b> POLIZZE RCA</b></a>
                    </li>
                    <li class="active">
                        <a href="${createLink(controller:"polizzeRCA", action: "dacontattare",)}"><span class="fa-lg"><i class="fa fa-id-card-o fa-stack-3x"></i></span><b> DA CONTATTARE</b></a>
                    </li>
                    <li>
                        <a href="${createLink(controller:"polizzeRCA", action: "annullate",)}"><span class="fa-lg"><i class="fa fa-trash fa-stack-3x"></i></span><b> ANNULLATE</b></a>
                    </li>
                    <li>
                        <a href="${createLink(controller:"polizzeRCA", action: "ofsgen",)}"><span class="fa-lg"><i class="fa fa-table fa-stack-3x"></i></span><b> OFSGEN</b></a>
                    </li>
                </g:elseif>
                <g:elseif test="${request.getRequestURI().toString().contains("/polizzeRCA/annullate")}">
                    <li>
                        <a href="${createLink( controller:"polizzeRCA", action: "listaRCA",)}"><span class="fa-lg"><i class="fa fa-wrench fa-stack-3x "></i></span> <b> POLIZZE RCA</b></a>
                    </li>
                    <li >
                        <a href="${createLink(controller:"polizzeRCA", action: "dacontattare",)}"><span class="fa-lg"><i class="fa fa-id-card-o fa-stack-3x"></i></span><b> DA CONTATTARE</b></a>
                    </li>
                <li class="active">
                    <a href="${createLink(controller:"polizzeRCA", action: "annullate",)}"><span class="fa-lg"><i class="fa fa-trash fa-stack-3x"></i></span><b> ANNULLATE</b></a>
                </li>
                    <li>
                        <a href="${createLink(controller:"polizzeRCA", action: "ofsgen",)}"><span class="fa-lg"><i class="fa fa-table fa-stack-3x"></i></span><b> OFSGEN</b></a>
                    </li>
                </g:elseif>
                <g:elseif test="${request.getRequestURI().toString().contains("/polizzeRCA/ofsgen")}">
                    <li>
                        <a href="${createLink( controller:"polizzeRCA", action: "listaRCA",)}"><span class="fa-lg"><i class="fa fa-wrench fa-stack-3x "></i></span> <b> POLIZZE RCA</b></a>
                    </li>
                    <li >
                        <a href="${createLink(controller:"polizzeRCA", action: "dacontattare",)}"><span class="fa-lg"><i class="fa fa-id-card-o fa-stack-3x"></i></span><b> DA CONTATTARE</b></a>
                    </li>
                    <li>
                        <a href="${createLink(controller:"polizzeRCA", action: "annullate",)}"><span class="fa-lg"><i class="fa fa-trash fa-stack-3x"></i></span><b> ANNULLATE</b></a>
                    </li>
                    <li class="active">
                        <a href="${createLink(controller:"polizzeRCA", action: "ofsgen",)}"><span class="fa-lg"><i class="fa fa-table fa-stack-3x"></i></span><b> OFSGEN</b></a>
                    </li>
                </g:elseif>
                <g:else>
                    <li class="active">
                        <a href="${createLink( controller:"polizzeRCA", action: "listaRCA",)}"><span class="fa-lg"><i class="fa fa-wrench fa-stack-3x "></i></span><b> POLIZZE RCA</b></a>
                    </li>
                    <li>
                        <a href="${createLink(controller:"polizzeRCA", action: "dacontattare",)}"><span class="fa-lg"><i class="fa fa-id-card-o fa-stack-3x"></i></span><b> DA CONTATTARE</b></a>
                    </li>
                    <li>
                        <a href="${createLink(controller:"polizzeRCA", action: "annullate",)}"><span class="fa-lg"><i class="fa fa-trash fa-stack-3x"></i></span><b> ANNULLATE</b></a>
                    </li>
                    <li>
                        <a href="${createLink(controller:"polizzeRCA", action: "ofsgen",)}"><span class="fa-lg"><i class="fa fa-table fa-stack-3x"></i></span><b> OFSGEN</b></a>
                    </li>
                </g:else>

            </ul>
        </div><!-- /#sidebar-wrapper -->
    <!-- Page Content -->

        <div class="col-md-10">
                        <div class="container-fluid colore">
                            <div class="col-lg-12 ">

                                <g:layoutBody/>

                            </div>

                        </div>
               </div>
        <!-- /#page-content-wrapper -->
    </div>
    <script>

    </script>

    </body>
</html>
</g:applyLayout>