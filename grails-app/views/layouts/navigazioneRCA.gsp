<g:applyLayout name="main">

    <html>
    <head>
        <title><g:layoutTitle/></title>
        <g:layoutHead/>
        <style>
        .page-header h1 { display: inline-block; }
        </style>
    </head>
    <body>
    <div class="row">
        <div class="col-md-12 col-xs-12">
            <div>
                <div class="bs-component">
                    <nav role="navigation" class="navbar navbar-default">
                        <div class="navbar-header">
                            <asset:image src="/loghi/opel_financial_services.png" style="height:50px; margin-top: 7px; margin-left: 10px;" />
                        </div>
                        <!-- Collection of nav links, forms, and other content for toggling -->
                        <div id="navbarCollapse" class="navbar-collapse">
                            <ul class="nav navbar-nav">
                                <li><h3 class="testo-navBarTitolo">RCA OPEL</h3></li>
                            </ul>
                            <ul class="nav navbar-nav navbar-right" style="font-size: 26px; margin-top: 7px;">
                                <sec:ifHasRole role="ADMIN">
                                    <li class="dropdown">
                                        <a href="#" data-toggle="dropdown" class="dropdown-toggle">Menu polizze <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li class="" id="polizzeRCA"><a href="${createLink(controller: "polizze", action: "lista")}" class="menu_poli">Polizze cash</a></li>
                                            <li class="" id="polizzeFIN"><a href="${createLink(controller: "polizze", action: "listFinanziate")}" class="menu_poli">Polizze finanziate/leasing</a></li>
                                            <li class="" id="polizzeAnnulla"><a href="${createLink(controller: "polizze", action: "annullamenti")}" class="menu_poli">Annullamenti</a></li>
                                            <li class="" id="polizzePAI"><a href="${createLink(controller: "polizze", action: "listPaiPag")}" class="menu_poli">Polizze PAI pagamento</a></li>
                                        </ul>
                                    </li>
                                </sec:ifHasRole>
                                <li>
                                    <g:link uri="/logout"><p>Esci</p></g:link>
                                </li>
                            </ul>
                        </div>
                    </nav>
                </div>
            </div>
        </div>
    </div>
    <g:layoutBody/>
    </body>
    </html>
</g:applyLayout>