<!doctype html>
<html lang="en" class="no-js">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge"/>
    <title><g:layoutTitle default="Grails"/></title>
    <meta name="viewport" content="width=device-width, initial-scale=1"/>
    <link rel="shortcut icon" href="${assetPath(src: 'favicon.ico')}" type="image/x-icon">

    <%-- Styles --%>
    <asset:stylesheet src="cashopel.css"/>
    <asset:stylesheet src="bootstrap.css"/>
    %{--<link rel="stylesheet" href="//bootswatch.com/cerulean/bootstrap.css">--}%
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
   %{-- <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/css/bootstrap-select.min.css"/>--}%
    <link rel="stylesheet" href="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/css/bootstrap-datepicker3.min.css"/>

    <link href="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.css" rel="stylesheet" />
    <%-- Scripts --%>
    <script src="//code.jquery.com/jquery-1.11.3.min.js"></script>
    <script src="//code.jquery.com/jquery-migrate-1.2.1.min.js"></script>
    <script src="//code.jquery.com/jquery-1.10.2.js"></script>
    %{--<script src="//netdna.bootstrapcdn.com/bootstrap/3.0.0/js/bootstrap.js"></script>--}%
    <script src="//maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
    <asset:javascript src="jquery-2.2.0.min.js"/>
    <asset:javascript src="cashopel.js"/>
    <asset:javascript src="bootstrap.js"/>
    <script src="//cdnjs.cloudflare.com/ajax/libs/autosize.js/3.0.14/autosize.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/bootstrap-select.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-select/1.7.5/js/i18n/defaults-it_IT.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/js/bootstrap-datepicker.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.5.0/locales/bootstrap-datepicker.it.min.js"></script>
    <script src="//cdn.datatables.net/1.10.7/js/jquery.dataTables.min.js"></script>
    <script src="//cdn.datatables.net/plug-ins/1.10.7/integration/bootstrap/3/dataTables.bootstrap.js"></script>

    <%-- Autocomplete --%>
    <script src="//cdnjs.cloudflare.com/ajax/libs/typeahead.js/0.11.1/typeahead.bundle.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/moment.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/moment.js/2.19.1/locale/it.js"></script>
    <g:layoutHead/>
</head>
<body>
    <div class="container">
        <g:layoutBody/>
        <g:if test="${actionName == ("lista") || actionName==("nuovaPolizza") || actionName==("modificaPolizza") || actionName==("listFinanziate")}">
            <div class="col-md-12 col-md-offset-4">
                <p><asset:image src="/loghi/logo_mach1_small.png" height="30px" style="margin-top: 90px; margin-left: 147px;" /></p>
            </div>
            <div class="col-md-12 ">
                <p class="testofooter" align="center"><br>  Mach1 s.r.l. - Via Vittor Pisani, 13/B  – 20124 Milano - TEL. 02 30465068 FAX 02 62694254 - Email: attivazioni.ofs@mach-1.it - CCIAA Milano - REA MI 1908726 - C.F.,  P. IVA e Reg. Imprese Milano 06680830962 - Capitale sociale 100.000 EURO - Registro Unico Intermediari  A000317603 - Banco Posta Sede di Milano, Piazza Cordusio, 1 - IBAN IT 54 C 07601 01600 0000 99701393</p>
            </div>
        </g:if>
    </div>

<script type="text/javascript">
    $('[data-toggle="tooltip"]').tooltip({html: true});
</script>
</body>
</html>
