<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main_RCA"/>
    <title>Elenco polizze da contattare</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
%{--<div class="col-md-12 col-md-push-11"><asset:image src="/loghi/mansutti.png" height="30px" style="margin-bottom: 25px;" /></div>--}%
<div class="row">
    <div class="col-lg-12 col-xs-10"  style="margin-top: 15px;">
        <g:if test="${flash.error}"><div class="labelErrore marginesotto">${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
        <g:if test="${flash.errorDealerPiva}">
            <div class="labelErrore marginesotto">
                <p>DEALER NON CARICATO PERCHE' GIA' PRESENTI:</p>
                <g:each var="errore" in="${flash.errorDealerPiva}">
                        <p>${errore}</p>
                </g:each>
            </div>
        </g:if>
        <g:if test="${flash.errorDealer}">
            <div class="labelErrore marginesotto">
                <p>DEALER GIA' PRESENTI, MA CON P.IVA DIVERSA:</p>
                <g:each var="errore" in="${flash.errorDealer}">
                    <p>${errore}</p>
                </g:each>
            </div>
        </g:if>
        <g:if test="${flash.errorePratica}">
            <div class="labelErrore marginesotto">
                <p>Il caricamento delle pratiche ha riportato i seguenti errori:</p>
                %{--<g:each var="errore" in="${flash.errorePratica}">--}%
                    <p>${flash.errorePratica.toString().replaceAll("\\<.*?>","")}</p>
               %{-- </g:each>--}%
            </div>
        </g:if>
        <g:if test="${flash.message=="POLIZZA"}"><script type="text/javascript">swal({
            title: "Richiesta inviata!",
            type: "success",
            timer: "1800",
            showConfirmButton: false
        });</script></g:if>
        <g:elseif test="${flash.message=="Dealer aggiornato"}" ><script type="text/javascript">swal({
            title: "${flash.message}",
            type: "success",
            timer: "1800",
            showConfirmButton: false
        });</script></g:elseif>
        <g:elseif test="${flash.message=="DEALER AGGIORNATO"}">
            <script type="text/javascript">swal({
                title: "Il dealer è stato aggiornato!",
                type: "success",
                timer: "1800",
                showConfirmButton: false
            });</script>
        </g:elseif>
        <g:elseif test="${flash.messageDealer}">
            <div class="labelErrore marginesotto">
                <p>NUOVI DEALER CARICATI CORRETTAMENTE:</p>
                <g:each var="messageD" in="${flash.messageDealer}">
                    <p>${messageD}</p>
                </g:each>
            </div>
        </g:elseif>
        <g:elseif test="${flash.praticheNuove}">
            <div class="labelErrore marginesotto">
                <p>PRATICHE CORRETTAMENTE CARICATE:</p>
                %{-- <g:each var="messageP" in="${flash.praticheNuove}">--}%
                     <p>${flash.praticheNuove}</p>
                %{-- </g:each>--}%
             </div>
         </g:elseif>
        <div class="col-lg-12 col-xs-12">
             <f:form  id="listform" method="get">
                 <div class="col-md-4 col-xs-6  area-ricerca">
                     <div class="input-group input-ricerca">
                         <input type="text" class="form-controlC" placeholder="ricerca polizze RCA" name="search" value="${params.search}">
                         <span class="input-group-btn">
                             <button type="submit" id="button_search" class="btn btn-secondary btn-ricerca" onchange="window.location.href = window.location.href.substr(0,window.location.href.lastIndexOf('?'));"><i class="glyphicon glyphicon-search"></i></button>
                         </span>
                     </div>
                 </div>
                 <sec:ifHasRole role="ADMIN">
                 %{-- <div class="col-md-4 col-xs-6 ">
                     <a href="${createLink(action: "nuovaPolizza")}" class="btn btn-ricerca"><i class="fa fa-plus"></i> <b>Nuova polizza</b></a>
                 </div>
                <div class="col-md-3 col-xs-3">
                     <a href="#" id="new-pratica" class="btn  btn-small" title="Nuova pratica">Caricare pratica <i class="fa fa-upload"></i></a>
                 </div>--}%
                     %{--<div class="col-md-3 col-xs-3 col-md-push-1" >
                         <a href="#" id="new-dealer" class="btn  btn-small" title="Nuovo dealer">Caricare dealer <i class="fa fa-upload"></i></a>
                     </div>--}%
                     <div class="col-md-4 col-xs-3 col-md-push-2">
                         <a href="${createLink(controller: "polizzeRCA", action: "generaFilePolizzedachiamare")}" target="_blank" class="btn btn-ricerca" title="Estrazione polizze">Estrarre polizze <i class="fa fa-download"></i></a>
                     </div>
                    %{-- <div class="col-md-3 col-xs-3 col-md-push-2" >
                         <a href="${createLink(controller: "polizze", action: "scaricaFascicoloInformativo")}" target="_blank" class="btn  btn-small" title="Scaricare fascicolo informativo"><b>Fascicolo informativo</b> <i class="fa fa-download"></i></a>
                     </div>--}%
                </sec:ifHasRole>
                %{--<sec:ifHasNotRole role="ADMIN">
                <div class="col-md-3 col-xs-3 col-md-push-6 col-xs-pull-1" style="margin-bottom: 4px;">
                    <a href="${createLink(controller: "polizze", action: "scaricaFascicoloInformativo")}" target="_blank" class="btn  btn-small" title="Scaricare fascicolo informativo"><b>Fascicolo informativo</b> <i class="fa fa-download"></i></a>
                </div>
                </sec:ifHasNotRole>--}%
            </f:form>
        </div>
        <div class="table-responsive col-lg-12 col-xs-12" style="font-size: 11px !important;">
            <table class="table table-condensed" id="tabellaPolizza">
                <thead>
                <sec:ifHasNotRole role="DEALER">
                    <th class="text-left vcenter-column">DEALER</th>
                </sec:ifHasNotRole>
                <th class="text-justify"></th>
                <th class="text-left">STATO</th>
                <th class="text-left">NO. PRATICA</th>
                <th class="text-left">TIPO PRODOTTO</th>
                <th class="text-left">ASSICURATO</th>
                <th class="text-left">COD. FISCALE<br>PARTITA IVA</th>
                <th class="text-left">TARGA</th>
                <th class="text-left">TELAIO</th>
                <th class="text-left">CASUALE CONTATTO</th>
                <th class="text-left">CONTATTO</th>
                <th class="text-center">CONTATTATO</th>
                %{--<th class="text-left">CERTIFICATO<br>DI POLIZZA</th>
                <th class="text-center">DOCUMENTO<br>ACQUISIZIONE</th>--}%
                </tr>
                </thead>
                <tbody>
                <g:each var="polizza" in="${polizze}">
                    <input type="hidden" name="idPolizzasel" id="idPolizzasel" value="${polizza.id}"/>
                    <tr class="polizza">
                        <sec:ifHasNotRole role="DEALER">
                            <td class="text-left vcenter-column dealerNome" data-id="${polizza.dealer.id}"  id="dealerNome" name="dealerNome" value="${polizza.dealer.id}"><b>${polizza.dealer.ragioneSocialeD}</b><br>${polizza.dealer.indirizzoD}, ${polizza.dealer.provinciaD}</td>
                            <td class="text-left vcenter-column"><a href="${createLink(controller: polizzeRCA, action: "nuovaPolizza", id: polizza.id)}"  style="cursor: pointer; font-size: 1.25em;"><i class="fa fa-pencil" ></i></a></td>
                        </sec:ifHasNotRole>
                        <sec:ifHasRole role="DEALER">
                            <td class="text-left vcenter-column"><a href="${createLink(controller: polizzeRCA, action: "nuovaPolizza", id: polizza.id)}"  style="cursor: pointer; font-size: 1.25em;"><i class="fa fa-pencil" style="cursor: pointer; font-size: 1.25em;" onclick="return modalPolizza(${polizza.id});"></i></a></td>
                        </sec:ifHasRole>
                        <td class="text-left vcenter-column" id="statoPolizza"><g:if test="${polizza.stato.toString().toUpperCase()==cashopel.polizze.StatoPolizza.DA_CHIAMARE}">POSTICIPATA</g:if><g:elseif test="${polizza.stato.toString().toUpperCase()=="POLIZZA IN ATTESA DI ATTIVAZIONE"}">IN ATTESA DI ATTIVAZIONE</g:elseif><g:else>${polizza.stato.toString().toUpperCase()}</g:else> </td>
                        <td class="text-left vcenter-column">${polizza.noPolizza}</td>
                        <td class="text-left vcenter-column">${polizza.tipoPolizza.toString().toUpperCase()}</td>
                        <td class="text-left vcenter-column">${polizza.nome.toString().toUpperCase()} ${polizza.cognome.toString().toUpperCase()}</td>
                        <td class="text-left vcenter-column">${polizza.partitaIva.toString().toUpperCase()}</td>
                        <td class="text-left vcenter-column"><g:if test="${polizza.targa}">${polizza.targa.toString().toUpperCase()}</g:if><g:else></g:else></td>
                        <td class="text-left vcenter-column"><g:if test="${polizza.telaio}">${polizza.telaio.toString().toUpperCase()}</g:if><g:else></g:else></td>
                        <td class="text-left vcenter-column"><g:if test="${polizza.causaleContatto!=cashopel.polizze.CausaleContatto.ERRORE_ANIA}">DATO MANCANTE</g:if><g:else>ERRORE ANIA</g:else></td>
                        <td class="text-left vcenter-column"> ${polizza.contattare.toString().toUpperCase()}</td>
                        <td class="text-center vcenter-column">
                            <select class="iscontatta" name="contattato" id="${polizza.id}" >
                                <option value="" disabled selected>Seleziona una opzione</option>
                                <option <g:if test="${polizza.contattato=="S"}"> value="S" selected </g:if> >S</option>
                                <option <g:if test="${polizza.contattato=="N"}"> value="N" selected </g:if> >N</option>
                            </select>
                        </td>
                        %{--<td class="text-center vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()=="POLIZZA"}"><a class="bottoneDoc" href="${createLink(controller: "polizze", action: "scaricaCertificato", id: polizza.id)}" target="_blank"  title="Certificato polizza ${polizza.noPolizza}"><i class="fa fa-file-pdf-o" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></a></g:if></td>
                        <td class="text-center vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()!="PREVENTIVO" && polizza.tipoAcquisizione.toString().toUpperCase()!= "NUOVA POLIZZA" }"><g:link action="scaricaDocumento" id="${polizza.id}" class="bottoneDoc" data-toggle="tooltip" data-container="body" data-placement="bottom" title="documento acquisizione"><i class="fa fa-file-text" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></g:link></g:if><g:elseif test="${polizza.stato.toString().toUpperCase()!="PREVENTIVO" && polizza.tipoAcquisizione.toString().toUpperCase()== "NUOVA POLIZZA" }">NUOVA POLIZZA</g:elseif></td>--}%
                    </tr>
                </g:each>
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="18" class="text-center"><b>Sono state trovate ${polizze?.totalCount ?: 0} polizze</b></th>
                </tr>
                </tfoot>
            </table>
        </div>
        <ul class="pagination pull-right">
            <g:if test="${pages}">
                <li>
                    <link:listdacontt page="1" controller="${controllerName}">
                        <span aria-hidden="true">&laquo;</span>
                    </link:listdacontt>
                </li>
            </g:if>
            <g:each var="p" in="${pages}">
                <g:if test="${p == page}"><li class="active"></g:if><g:else><li></g:else>
                <g:if test="${params.search}"><link:listdacontt page="${p}" controller="${controllerName}" search="${params.search}">${p}</link:listdacontt></g:if>
                <g:else><link:listdacontt page="${p}" controller="${controllerName}">${p}</link:listdacontt></g:else>
                </li>
            </g:each>
            <g:if test="${pages}">
                <li>
                    <link:listdacontt page="${totalPages}" controller="${controllerName}">
                        <span aria-hidden="true">&raquo;</span>
                    </link:listdacontt>
                </li>
            </g:if>

        </ul>
    </div>

    <div class="modal" id="form-dealer">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center testo-navBarModal"><b>Caricamento Dealer</b></h3>
                    </div>
                    <div class="modal-body" style="background: white">
                        <form action="${createLink(action: "caricaDealer", params:[_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-idExcelDealer" type="file" name="excelDealer" class="file" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
    <div class="modal" id="form-pratica">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center testo-navBarModal"> <b>Caricamento Pratica</b></h3>
                    </div>
                    <div class="modal-body" style="background: white">
                        <form action="${createLink(action: "caricaPraticaCSV", params:[_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-idExcelPratica" type="file" name="excelPratica" class="file" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#new-dealer").click(function() { $("#form-dealer").modal("show"); });
    $("#new-pratica").click(function() { $("#form-pratica").modal("show"); });
    $(document).ready(function() {
        $(".chiamaWS").hide();
        $(".chiamastato").hide();
        window.setTimeout(function() {
            $(".alert").fadeTo(1500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 5000);
    });

    function attivaTab(tab){
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };
    function createCell(cell, text, style) {
        var txt = document.createTextNode(text); // create text node
        cell.appendChild(txt);                   // append DIV to the table cell
    }

    $('.iscontatta').change(function() {
        var idpol=$(this).attr("id");
        var seleop=$(this).val();
        var contattato="";
        if (seleop == 'S') {
            contattato = "S";

        }else{
            contattato = "N";
        }
        aggContattato(contattato, idpol);
    });
    function aggContattato(contattato,idpol) {
        //var idpol=$('#idPolizzasel').val();
            $.post("${createLink(controller: "polizzeRCA", action: "aggiornacontattato")}", {
                contattato: contattato,
                idpol: idpol,
                _csrf: "${request._csrf.token}"
            }, function (response) {
                var risposta = response.risposta;

                if (risposta == false) {
                    var errore = response.errore;
                    swal({
                        title: "Polizza no aggiornata!",
                        type: "error",
                        timer: "1800",
                        showConfirmButton: false
                    });
                    //location.reload();
                    setTimeout("location.reload(true);", 2500);
                } else {
                    swal({
                        title: "Polizza aggiornata!",
                        type: "success",
                        timer: "1800",
                        showConfirmButton: false
                    });
                    //location.reload();
                    setTimeout("location.reload(true);", 2500);
                }
            }, "json");
    }
</script>
</body>
</html>