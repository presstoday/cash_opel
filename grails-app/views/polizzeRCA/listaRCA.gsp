<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main_RCA"/>
    <title>Elenco polizze RCA</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
%{--<div class="col-md-12 col-md-push-11"><asset:image src="/loghi/mansutti.png" height="30px" style="margin-bottom: 25px;" /></div>--}%
<div class="row">
    <div class="col-lg-12 col-xs-10"  style="margin-top: 15px;">
        <g:if test="${flash.error}"><div class="labelErrore marginesotto">${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
        <g:if test="${flash.errorDealerPiva}">
            <div class="labelErrore marginesotto">
                <p>DEALER NON CARICATO PERCHE' GIA' PRESENTI:</p>
                <g:each var="errore" in="${flash.errorDealerPiva}">
                        <p>${errore}</p>
                </g:each>
            </div>
        </g:if>
        <g:if test="${flash.errorDealer}">
            <div class="labelErrore marginesotto">
                <p>DEALER GIA' PRESENTI, MA CON P.IVA DIVERSA:</p>
                <g:each var="errore" in="${flash.errorDealer}">
                    <p>${errore}</p>
                </g:each>
            </div>
        </g:if>
        <g:if test="${flash.errorePratica}">
            <div class="labelErrore marginesotto">
                <p>Il caricamento delle pratiche ha riportato i seguenti errori:</p>
                %{--<g:each var="errore" in="${flash.errorePratica}">--}%
                    <p>${flash.errorePratica.toString().replaceAll("\\<.*?>","")}</p>
               %{-- </g:each>--}%
            </div>
        </g:if>
        <g:if test="${flash.message=="Richiesta inviata"}"><script type="text/javascript">swal({
            title: "Richiesta inviata e flussi generati!",
            type: "success",
            timer: "1800",
            showConfirmButton: false
        });</script></g:if>
        <g:if test="${flash.message=="polizza Attivata"}"><script type="text/javascript">swal({
            title: "polizza Attivata!",
            type: "success",
            timer: "1800",
            showConfirmButton: false
        });</script></g:if>
        <g:if test="${flash.message=="polizza da chiamare"}"><script type="text/javascript">swal({
            title: "polizza da chiamare!",
            type: "success",
            timer: "1800",
            showConfirmButton: false
        });</script></g:if>
        <g:if test="${flash.message=="DA CHIAMARE"}"><script type="text/javascript">swal({
            title: "polizza aggiornata!",
            type: "success",
            timer: "1800",
            showConfirmButton: false
        });</script></g:if>
        <g:if test="${flash.message=="polizza annullata"}"><script type="text/javascript">swal({
            title: "Polizza annullata!",
            type: "success",
            timer: "1800",
            showConfirmButton: false
        });</script></g:if>
        <g:elseif test="${flash.message=="Dealer aggiornato"}" ><script type="text/javascript">swal({
            title: "${flash.message}",
            type: "success",
            timer: "1800",
            showConfirmButton: false
        });</script></g:elseif>
        <g:elseif test="${flash.message=="DEALER AGGIORNATO"}">
            <script type="text/javascript">swal({
                title: "Il dealer è stato aggiornato!",
                type: "success",
                timer: "1800",
                showConfirmButton: false
            });</script>
        </g:elseif>
        <g:elseif test="${flash.messageDealer}">
            <div class="labelErrore marginesotto">
                <p>NUOVI DEALER CARICATI CORRETTAMENTE:</p>
                <g:each var="messageD" in="${flash.messageDealer}">
                    <p>${messageD}</p>
                </g:each>
            </div>
        </g:elseif>
        <g:elseif test="${flash.praticheNuove}">
            <div class="labelErrore marginesotto">
                <p>PRATICHE CORRETTAMENTE CARICATE:</p>
                %{-- <g:each var="messageP" in="${flash.praticheNuove}">--}%
                     <p>${flash.praticheNuove}</p>
                %{-- </g:each>--}%
             </div>
         </g:elseif>
        <g:elseif test="${flash.message=="Richiesta inviata"}">
            <script type="text/javascript">swal({
                title: "Richiesta inviata!",
                type: "success",
                timer: "1800",
                showConfirmButton: false
            });</script>
        </g:elseif>
        <div class="col-lg-12 col-xs-12">
             <f:form  id="listform" method="get">
                 <div class="col-md-4 col-xs-6  area-ricerca">
                     <div class="input-group input-ricerca">
                         <input type="text" class="form-controlC" placeholder="ricerca polizze RCA" name="search" value="${params.search}">
                         <span class="input-group-btn">
                             <button type="submit" id="button_search" class="btn btn-secondary btn-ricerca" onchange="window.location.href = window.location.href.substr(0,window.location.href.lastIndexOf('?'));"><i class="glyphicon glyphicon-search"></i></button>
                         </span>
                     </div>
                 </div>
                 %{--<sec:ifHasRole role="ADMIN">--}%
                     <div class="col-md-4 col-xs-6 ">
                         <a href="${createLink(action: "nuovaPolizza")}" class="btn btn-ricerca"><i class="fa fa-plus"></i> <b>Nuova polizza</b></a>
                     </div>
                     %{--<div class="col-md-3 col-xs-3">
                         <a href="#" id="new-pratica" class="btn  btn-small" title="Nuova pratica">Caricare pratica <i class="fa fa-upload"></i></a>
                     </div>--}%
                     %{--<div class="col-md-3 col-xs-3 col-md-push-1" >
                         <a href="#" id="new-dealer" class="btn  btn-small" title="Nuovo dealer">Caricare dealer <i class="fa fa-upload"></i></a>
                     </div>--}%
                     <div class="col-md-4 col-xs-3 col-md-push-2">
                         <a href="${createLink(controller: "polizzeRCA", action: "generaFilePolizzeattivate")}" target="_blank" class="btn btn-ricerca" title="Estrazione polizze">Estrarre polizze <i class="fa fa-download"></i></a>
                     </div>
                %{--</sec:ifHasRole>--}%
                 <sec:ifHasRole role="ADMINPRESSTODAY">
                     <div class="col-md-3 col-xs-3  btnFinanziate">
                         <a href="${createLink(controller: "polizzeRCA", action: "elencoFlussiIassicur")}" target="" class="btn btn-ricerca" title="Estrazione flussi IASSICUR"><b>Elenco flussi IASSICUR</b> <i class="fa fa-download"></i></a>
                     </div>
                     <div class="col-md-3 col-xs-3  btnFinanziate">
                         <a href="${createLink(controller: "polizzeRCA", action: "generazioneFlussiDL")}" target="" class="btn btn-ricerca" title="Estrazione flussi DL"><b>Genera flussi DL</b> <i class="fa fa-download"></i></a>
                     </div>
                 </sec:ifHasRole>
            </f:form>
        </div>
        <div class="table-responsive col-lg-12 col-xs-12" style="font-size: 11px !important;">
            <table class="table table-condensed" id="tabellaPolizza">
                <thead>
                <sec:ifHasNotRole role="DEALER">
                    <th class="text-left vcenter-column">DEALER</th>
                </sec:ifHasNotRole>
                <th class="text-justify"></th>
                <th class="text-left">STATO</th>
                <th class="text-left">NO. PRATICA</th>
                <th class="text-left">TIPO PRODOTTO</th>
                <th class="text-left">ASSICURATO</th>
                <th class="text-left">COD. FISCALE<br>PARTITA IVA</th>
                <th class="text-left">TARGA</th>
                <th class="text-left">TELAIO</th>
                <th class="text-left">DATA DECORRENZA</th>
                %{--<th class="text-center">MODULO<br>ADESIONE</th>
                <th class="text-left">CERTIFICATO<br>DI POLIZZA</th>
                <th class="text-center">DOCUMENTO<br>ACQUISIZIONE</th>--}%
                </tr>
                </thead>
                <tbody>
                <g:each var="polizza" in="${polizze}">
                    <input type="hidden" name="idPolizzasel" id="idPolizzasel" value="${polizza.id}"/>
                    <tr class="polizza">
                        <sec:ifHasNotRole role="DEALER">
                            <td class="text-left vcenter-column dealerNome" data-id="${polizza.dealer.id}"  id="dealerNome" name="dealerNome" value="${polizza.dealer.id}"><b>${polizza.dealer.ragioneSocialeD}</b><br>${polizza.dealer.indirizzoD}, ${polizza.dealer.provinciaD}</td>
                            <td class="text-left vcenter-column"><a href="${createLink(controller: polizzeRCA, action: "nuovaPolizza", id: polizza.id)}"  style="cursor: pointer; font-size: 1.25em;"><i class="fa fa-pencil" ></i></a></td>
                        </sec:ifHasNotRole>
                        <sec:ifHasRole role="DEALER">
                            <td class="text-left vcenter-column"><a href="${createLink(controller: polizzeRCA, action: "nuovaPolizza", id: polizza.id)}"  style="cursor: pointer; font-size: 1.25em;"><i class="fa fa-pencil" style="cursor: pointer; font-size: 1.25em;" onclick="return modalPolizza(${polizza.id});"></i></a></td>
                        </sec:ifHasRole>
                        <td class="text-left vcenter-column" id="statoPolizza"><g:if test="${polizza.stato.toString().toUpperCase()==cashopel.polizze.StatoPolizza.DA_CHIAMARE}">POSTICIPATA</g:if><g:elseif test="${polizza.stato.toString().toUpperCase()=="POLIZZA IN ATTESA DI ATTIVAZIONE"}">IN ATTESA DI ATTIVAZIONE</g:elseif><g:else>${polizza.stato.toString().toUpperCase()}</g:else> </td>
                        <td class="text-left vcenter-column">${polizza.noPolizza}</td>
                        <td class="text-left vcenter-column">${polizza.tipoPolizza.toString().toUpperCase()}</td>
                        <td class="text-left vcenter-column">${polizza.nome.toString().toUpperCase()} ${polizza.cognome.toString().toUpperCase()}</td>
                        <td class="text-left vcenter-column">${polizza.partitaIva.toString().toUpperCase()}</td>
                        <td class="text-left vcenter-column"><g:if test="${polizza.targa}">${polizza.targa.toString().toUpperCase()}</g:if><g:else></g:else></td>
                        <td class="text-left vcenter-column"><g:if test="${polizza.telaio}">${polizza.telaio.toString().toUpperCase()}</g:if><g:else></g:else></td>
                        <td class="text-left vcenter-column">${polizza.dataDecorrenza.format("dd/MM/yyyy")}</td>
                        %{--<td class="text-center vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()!="PREVENTIVO" && polizza.adesione ==true }"><g:link action="scaricaModulo" id="${polizza.id}" class="bottoneDoc" data-toggle="tooltip" data-container="body" data-placement="bottom" title="Modulo adesione ${polizza.noPolizza}"><i class="fa fa-file-text" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></g:link></g:if></td>
                       <td class="text-center vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()=="POLIZZA"}"><a class="bottoneDoc" href="${createLink(controller: "polizze", action: "scaricaCertificato", id: polizza.id)}" target="_blank"  title="Certificato polizza ${polizza.noPolizza}"><i class="fa fa-file-pdf-o" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></a></g:if></td>
                       <td class="text-center vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()!="PREVENTIVO" && polizza.tipoAcquisizione.toString().toUpperCase()!= "NUOVA POLIZZA" }"><g:link action="scaricaDocumento" id="${polizza.id}" class="bottoneDoc" data-toggle="tooltip" data-container="body" data-placement="bottom" title="documento acquisizione"><i class="fa fa-file-text" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></g:link></g:if><g:elseif test="${polizza.stato.toString().toUpperCase()!="PREVENTIVO" && polizza.tipoAcquisizione.toString().toUpperCase()== "NUOVA POLIZZA" }">NUOVA POLIZZA</g:elseif></td>--}%
                    </tr>
                </g:each>
                </tbody>
                <tfoot>
                <tr>
                    <th colspan="18" class="text-center"><b>Sono state trovate ${polizze?.totalCount ?: 0} polizze</b></th>
                </tr>
                </tfoot>
            </table>
        </div>
        <ul class="pagination pull-right">
            <g:if test="${pages}">
                <li>
                    <link:listRCA page="1" controller="${controllerName}">
                        <span aria-hidden="true">&laquo;</span>
                    </link:listRCA>
                </li>
            </g:if>
            <g:each var="p" in="${pages}">
                <g:if test="${p == page}"><li class="active"></g:if><g:else><li></g:else>
                <g:if test="${params.search}"><link:listRCA page="${p}" controller="${controllerName}" search="${params.search}">${p}</link:listRCA></g:if>
                <g:else><link:listRCA page="${p}" controller="${controllerName}">${p}</link:listRCA></g:else>
                </li>
            </g:each>
            <g:if test="${pages}">
                <li>
                    <link:listRCA page="${totalPages}" controller="${controllerName}">
                        <span aria-hidden="true">&raquo;</span>
                    </link:listRCA>
                </li>
            </g:if>

        </ul>
    </div>

    <div class="modal" id="form-dealer">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center testo-navBarModal"><b>Caricamento Dealer</b></h3>
                    </div>
                    <div class="modal-body" style="background: white">
                        <form action="${createLink(action: "caricaDealer", params:[_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-idExcelDealer" type="file" name="excelDealer" class="file" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
    <div class="modal" id="form-pratica">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center testo-navBarModal"> <b>Caricamento Pratica</b></h3>
                    </div>
                    <div class="modal-body" style="background: white">
                        <form action="${createLink(action: "caricaPraticaCSV", params:[_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-idExcelPratica" type="file" name="excelPratica" class="file" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    $("#new-dealer").click(function() { $("#form-dealer").modal("show"); });
    $("#new-pratica").click(function() { $("#form-pratica").modal("show"); });
    $(document).ready(function() {
        $(".chiamaWS").hide();
        $(".chiamastato").hide();
        window.setTimeout(function() {
            $(".alert").fadeTo(1500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 5000);
    });
    var txtemailCliente="";
    var txttargaCliente="";
    var txttarga_1a_imatr="";
    var txtdataImmatricolazione="";

    function attivaTab(tab){
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };
    function modalPolizzaAdmin(x){
        $.post("${createLink(controller: "polizze", action: 'getPolizzaAdmin')}",{ idPolizza: x,  _csrf: "${request._csrf.token}"}, function(response) {
            if(response.risposta==true){
                $( ".alert" ).remove();
                $("#cognome").val(response.cognomeInt);
                $("#nome").val(response.nomeInt);
                $("#tipoCliente").val(response.tipoClienteInt);
                $("#noPolizza").val(response.noPolizza);
                $("#partitaIva").val(response.partitaIvaInt);
                $("#telefonoDef").val(response.telefonoInt);
                $("#indirizzo").val(response.indirizzoInt);
                $("#cap").val(response.capInt);
                $("#localita").val(response.localitaInt);
                $("#provincia").val(response.provinciaInt);
                $("#emailDef").val(response.emailInt);
                $("#targa").val(response.targa);
                $("#telaio").val(response.telaio);
                $("#valoreAssicurato").val(response.valoreAssicurato);
                $("#marca").val(response.marca);
                $("#dataImmatricolazione").datepicker('setDate',response.dataImmatricolazione);
                $("#dataDecorrenza").val(response.dataDecorrenza);
                $("#dataScadenza").val(response.dataScadenza);
                $("#durata").val(response.durata);
                $("#modello").val(response.modello);
                $("#versione").val(response.variante);
                $("#percVend").val(response.percVend);
                $("#premioLordo").val(response.premioLordo);
                $("#premioImponibile").val(response.premioImponibile);
                $("#tipoPolizza").val(response.tipoPolizza);
                $("#coperturaRichiesta").val(response.coperturaRichiesta);

                var stato=response.stato
                if(stato=="PREVENTIVO"){
                    document.getElementById("stato").selectedIndex = 0;
                }else if(stato=="RICHIESTA INVIATA"){
                    document.getElementById("stato").selectedIndex = 1;
                }else if(stato=="POLIZZA"){
                    document.getElementById("stato").selectedIndex = 2;
                }else if(stato=="ANNULLATA"){
                    document.getElementById("stato").selectedIndex = 3;
                }else if(stato=="DA CONTATTARE"){
                    document.getElementById("stato").selectedIndex = 4;
                }
                $("#ragionesociale").val(response.dealer);
                $("#piva").val(response.piva);
                $("#indirizzo").val(response.indirizzo);
                $("#cap").val(response.cap);
                $("#provincia").val(response.provincia);
                $("#comune").val(response.comune);
                $("#telefono").val(response.telefono);
                $("#cellulare").val(response.cellulare);
                /*var fax;
                if(response.fax=="null"){
                    fax='';
                }else{
                    fax=response.fax;
                }
                $("#fax").val(fax);*/
                $("#email").val(response.email);
                $("#idPolizza").val(x);

                //$("#bottonSalva").prop('disabled', false);
                //$("#bottonChiama").prop('disabled', true);
                $("#bottonSalva").prop('disabled', true);
                $("#modalPolizza").modal({
                    keyboard: true
                });
            }
        }, "json");
    }
    function modalPolizza(x){
        $.post("${createLink(controller: "polizze", action: 'getPolizza')}",{ idPolizza: x,  _csrf: "${request._csrf.token}"}, function(response) {
            if(response.risposta==true && response.stato=="PREVENTIVO"){
                $( ".alert" ).remove();
                $("#cognome").val(response.cognomeInt);
                $("#nome").val(response.nomeInt);
                $("#tipoCliente").val(response.tipoClienteInt);
                $("#noPolizza").val(response.noPolizza);
                $("#partitaIva").val(response.partitaIvaInt);
                $("#telefonoDef").val(response.telefonoInt);
                $("#indirizzo").val(response.indirizzoInt);
                $("#cap").val(response.capInt);
                $("#localita").val(response.localitaInt);
                $("#provincia").val(response.provinciaInt);
                $("#emailDef").val(response.emailInt);
                $("#targa").val(response.targa);
                $("#telaio").val(response.telaio);
                $("#marca").val(response.marca);
                $("#dataImmatricolazione").val(response.dataImmatricolazione);
                $("#valoreAssicurato").val(response.valoreAssicurato);
                $("#dataDecorrenza").val(response.dataDecorrenza);
                $("#dataScadenza").val(response.dataScadenza);
                $("#durata").val(response.durata);
                $("#modello").val(response.modello);
                $("#versione").val(response.variante);
                $("#percVend").val(response.percVend);
                $("#premioLordo").val(response.premioLordo);
                $("#premioImponibile").val(response.premioImponibile);
                $("#tipoPolizza").val(response.tipoPolizza);
                $("#coperturaRichiesta").val(response.coperturaRichiesta);
                var stato=response.stato
                if(stato=="PREVENTIVO"){
                    document.getElementById("stato").selectedIndex = 0;
                }else if(stato=="RICHIESTA INVIATA"){
                    document.getElementById("stato").selectedIndex = 1;
                }else if(stato=="POLIZZA"){
                    document.getElementById("stato").selectedIndex = 2;
                }else if(stato=="ANNULLATA"){
                    document.getElementById("stato").selectedIndex = 3;
                }else if(stato=="DA CONTATTARE"){
                    document.getElementById("stato").selectedIndex = 4;
                }
                $("#ragionesociale").val(response.dealer);
                $("#piva").val(response.piva);
                $("#indirizzo").val(response.indirizzo);
                $("#cap").val(response.cap);
                $("#provincia").val(response.provincia);
                $("#comune").val(response.comune);
                $("#telefono").val(response.telefono);
                $("#cellulare").val(response.cellulare);
                /*var fax;
                if(response.fax=="null"){
                    fax='';
                }else{
                    fax=response.fax;
                }
                $("#fax").val(fax);*/
                $("#email").val(response.email);
                $("#idPolizza").val(x);
                $('#input-idAcquisizione').fileinput('disable');
                $("#targaAcquisizione");
                //$("#bottonChiama").prop('disabled', true);
                $("#bottonSalva").prop('disabled', true);
                $("#modalPolizza").modal({
                    keyboard: true
                });
            }
        }, "json");
    }
    function createCell(cell, text, style) {
        var txt = document.createTextNode(text); // create text node
        cell.appendChild(txt);                   // append DIV to the table cell
    }
    function aggiornaPolizza(){
        var idPolizza=$("#idPolizza").val();
        var targa=$("#targa").val();
        var telaio=$("#telaio").val();
        var dataImmatricolazione=$("#dataImmatricolazione").val();
        var dataDecorrenza=$("#dataDecorrenza").val();
        var marchio=$("#marca").val();
        var versione=$("#modello").val();
        var dataDecorrenza=$("#dataDecorrenza").val();
        var telefonoInt=$("#telefono").val();
        var email=$("#email").val();
        var valoreAssicurato=$("#valoreAssicurato").val();
        var bottonChiama=$("#bottonChiama").val();
        $.post("${createLink(controller: "polizze", action: 'aggiornaPolizza')}",{ idPolizza: idPolizza, bottonchiama:bottonChiama, targa:targa, telaio:telaio,dataImmatricolazione:dataImmatricolazione, marchio:marchio, versione:versione, motoveicolo:motoveicolo,dataDecorrenza:dataDecorrenza, dataPosticipata:dataPosticipata,cv:cv,valoreAssicurato:valoreAssicurato,cellulare:cellulare,email:email, _csrf: "${request._csrf.token}"}, function(response) {
            if(response.risposta==true){
                $('#tabellaPolizza').load(window.location.href + ' #tabellaPolizza');
                swal({
                    title: "Polizza salvata!",
                    type: "success",
                    timer: "1800",
                    showConfirmButton: false
                });
            }else{
                swal({
                    title: "Errore salvataggio!",
                    text: ""+ response.errore+"",
                    type: "error",
                    showConfirmButton: true
                });
            }
        }, "json");
    }
    function aggiornaPolizzadaChiamare(){
        var idPolizza=$("#idPolizza").val();
        $.post("${createLink(controller: "polizze", action: 'aggiornaPolizzaDaChiamare')}",{ idPolizza: idPolizza, /*targa:targa, telaio:telaio,dataImmatricolazione:dataImmatricolazione, marchio:marchio, versione:versione, motoveicolo:motoveicolo,dataDecorrenza:dataDecorrenza, dataPosticipata:dataPosticipata,cv:cv,valoreAssicurato:valoreAssicurato,cellulare:cellulare,email:email,*/ _csrf: "${request._csrf.token}"}, function(response) {
            if(response.risposta==true){
                $('#modalPolizza').modal('toggle');
                swal({
                    title: "Polizza salvata!",
                    type: "success",
                    html: '<a href="#" onclick="swal.closeModal(); return false;">I want more Checkout</a>',
                    timer: "1000",
                    showConfirmButton: false
                });
                $('#tabellaPolizza').load(window.location.href + ' #tabellaPolizza');

            }else{
                swal({
                    title: "Errore salvataggio!",
                    text: ""+ response.errore+"",
                    type: "error",
                    showConfirmButton: true
                });
            }
        }, "json");
    }
    function annullaPolizza(){
        var idPolizza=$("#idPolizza").val();
        $.post("${createLink(controller: "polizze", action: 'annullaPolizza')}",{ idPolizza: idPolizza, _csrf: "${request._csrf.token}"}, function(response) {
            if(response.risposta==true){
                $('#tabellaPolizza').load(window.location.href + ' #tabellaPolizza');
                swal({
                    title: "Polizza aggiornata!",
                    type: "success",
                    timer: "1800",
                    showConfirmButton: false
                });
            }else{
                swal({
                    title: "Errore aggiornamento preventivo!",
                    text: ""+ response.errore+"",
                    type: "error",
                    showConfirmButton: true
                });
            }
        }, "json");
    }
    $(".windowPolizza").on("hidden.bs.modal", function(){
        $( ".alert" ).remove();
        $(this).find('form')[0].reset();
        if($("#dataImmatricolazione").val()!= ''){
            $("#dataImmatricolazione").data('datepicker').setDate(null);
        }
    });

    function verificatargaprima() {
        var valore= $("#targa_prima").val();
        var regtarga=/^[a-zA-Z]{2}[0-9]{3,4}[a-zA-Z]{2}$/;
        if(valore!='' && regtarga.test(valore)){ return true; }
        else  {
            if(!regtarga.test(valore) && valore!=''){
                txttarga_1a_imatr="il formato della targa è sbagliato";
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txttargaCliente);
            }
            else {
                txttarga_1a_imatr="compilare il campo targa prima immatr.";
            }
            return false;
        }
    }
    function verificaTarga() {
        var valore= $("#targa").val();
        var regtarga=/^[a-zA-Z]{2}[0-9]{3,4}[a-zA-Z]{2}$/;
        if(valore!='' && regtarga.test(valore)){ return true; }
        else  {
            if(!regtarga.test(valore) && valore!=''){
                txttargaCliente="il formato della targa è sbagliato";
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txttargaCliente);
            }
            else {
                txttargaCliente="compilare il campo targa";
            }
            return false;
        }
    }
    function verificaEmailContr() {
        var valore= $("#emailDef").val();
        var regemailDef=/^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$/;
        if(valore!='' && regemailDef.test(valore)){
            $( ".alert" ).remove();
            return true;
        }
        else{
            if(!regemailDef.test(valore) && valore!=''){ txtemailCliente="verificare il formato del campo email";}
            else {txttargaCliente="compilare il campo email";}
            return false;
        }
    }
    function monthDiff(d2) {
        var d1=new Date();
        var day1= d1,day2= d2;
        if(day1<day2){
            d1= day2;
            d2= day1;
        }
        var months= (d1.getFullYear()-d2.getFullYear())*12+(d1.getMonth()-d2.getMonth());
        if(d1.getDate()<d2.getDate()) --months;
        return months <= 0 ? 0 : months;
    }
    function verificaDataImmatricolazione(){
        var data1 = $("#dataImmatricolazione").val();
        var format1=new Date($("#dataImmatricolazione").datepicker("getDate"));
        var strDateTime =  format1.getDate() + "-" + (format1.getMonth()+1) + "-" + format1.getFullYear();
        if (data1 !=''){
            d1 = strDateTime;
            d2 = strDateTime;
            var mesi=monthDiff( format1);
            if(mesi>36){
                txtdataImmatricolazione="la data immatricolazione deve essere inferiore a 3 anni";
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtdataImmatricolazione);
                return false;
            }else{
                return true;
            }
        }
        else{return false;  }
    }
    function verificaTelaio() {
        var valore= $("#telaio").val();
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaValore() {
        var valore= $("#valoreAssicurato").val();
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaDecorrenza() {
        var valore= $("#dataDecorrenza").val();
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaTelefono() {
        var valore= $("#telefonoDef").val();
        if (valore!=''){ return true; }
        else { return false; }
    }
    $("#targa").on("change", function(event) {
        var tipoimmatr = false;
        if($("#tipoimmatr").is(":checked")){  tipoimmatr= true;}
        //alert(tipoimmatr);
        if(($.trim($("#targa").val()) != '' )&& verificaTarga() && !tipoimmatr && verificatargaprima()  /*&& ($.trim($("#emailDef").val()) != '' )  && verificaEmailContr() && verificaTelefono() && verificaDataImmatricolazione() && verificaTelaio()  && verificaValore() && verificaDecorrenza()*/){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $("#bottonSalva").prop('disabled', false);
        }
        else{
            if(!verificaTarga()){
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"labelErrore marginesotto\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txttargaCliente);
                $("#bottonSalva").prop('disabled', true);
            }else if(!tipoimmatr){
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"labelErrore marginesotto\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append("compilare targa prima immatricolazione");
                $("#bottonSalva").prop('disabled', true);
            }else if(tipoimmatr && verificaTarga()){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $("#bottonSalva").prop('disabled', false);
        }
            /*$("#avanti1").prop('disabled', true);*/

        }
    });
    $("#targa_prima").on("change", function(event) {
        var tipoimmatr = false;
        if($("#tipoimmatr").is(":checked")){  tipoimmatr= true;}
        //alert(tipoimmatr);
        if(($.trim($("#targa_prima").val()) != '' )&& verificatargaprima() && verificaTarga() && !tipoimmatr/*&& ($.trim($("#emailDef").val()) != '' )  && verificaEmailContr() && verificaTelefono() && verificaDataImmatricolazione() && verificaTelaio()  && verificaValore() && verificaDecorrenza()*/){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $("#bottonSalva").prop('disabled', false);
        }
        else{
            if(!verificaTarga()){
                $(".alert").remove();
                $(".testoAlert").remove();
                $(".datiPoli").before( "<div class=\"labelErrore marginesotto\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $(".testoAlert").append(txttargaCliente);
            }else if(!verificatargaprima){
                $(".alert").remove();
                $(".testoAlert").remove();
                $(".datiPoli").before( "<div class=\"labelErrore marginesotto\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $(".testoAlert").append("compilare targa prima immatricolazione");
            }
            /*$("#avanti1").prop('disabled', true);*/
            $("#bottonSalva").prop('disabled', true);
        }
    });
    $("#tipoimmatr").on("change", function(event) {
        var tipoimmatr = false;
        if($("#tipoimmatr").is(":checked")){  tipoimmatr= true;}
        if((!tipoimmatr)&& verificaTarga() && verificatargaprima() /*&& ($.trim($("#emailDef").val()) != '' )  && verificaEmailContr() && verificaTelefono() && verificaDataImmatricolazione() && verificaTelaio()  && verificaValore() && verificaDecorrenza()*/){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
           // $("#targa_prima").prop('disabled', false);
            $("#bottonSalva").prop('disabled', false);
        }
        else{
            if(tipoimmatr && verificaTarga()){
                $( ".testoAlert " ).remove();
                $("#bottonSalva").prop('disabled', false);
                //$("#targa_prima").prop('disabled', true);
            }else if(!tipoimmatr && !verificaTarga() && verificatargaprima()){
                $("#targa_prima").prop('disabled', false);
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"labelErrore marginesotto\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txttargaCliente);
                $("#bottonSalva").prop('disabled', true);
            }else if(!tipoimmatr && verificaTarga() && !verificatargaprima()){
                //$("#targa_prima").prop('disabled', false);
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"labelErrore marginesotto\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txttarga_1a_imatr);
                $("#bottonSalva").prop('disabled', true);
            }else if(tipoimmatr && !verificaTarga()){
                //$("#targa_prima").prop('disabled', true);
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"labelErrore marginesotto\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txttargaCliente);
                $("#bottonSalva").prop('disabled', true);
            }
            else{
                //$("#targa_prima").prop('disabled', false);
                $("#bottonSalva").prop('disabled', false);
            }
            /**/
        }
    });
    $("#emailDef").on("change", function(event) {
        if(($.trim($("#emailDef").val()) != '' )&& verificaEmailContr() && verificaTelefono() && verificaTarga() && verificaDataImmatricolazione() && verificaTelaio()  && verificaValore() && verificaDecorrenza() ){
            $( ".alert " ).remove();
            $("#avanti1").prop('disabled', false);
        }
        else{
            if(!verificaEmailContr()){
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtemailCliente);
            }
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#telaio").on("change", function(event) {
        if(($.trim($("#telaio").val()) != '' )  && verificaEmailContr() && verificaTelefono()  && verificaDataImmatricolazione() && verificaTarga()   && verificaDecorrenza()  && verificaValore()){
            $("#avanti1").prop('disabled', false);
        }else {
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#telefonoDef").on("change", function(event) {
        if(($.trim($("#telefonoDef").val()) != '' )  && verificaEmailContr() && verificaTelaio()  && verificaDataImmatricolazione() && verificaTarga()   && verificaDecorrenza()  && verificaValore()){
            $("#avanti1").prop('disabled', false);
        }else {
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#valoreAssicurato").on("change", function(event) {
        if(($.trim($("#valoreAssicurato").val()) != '' ) && verificaEmailContr() && verificaTelefono()&& verificaDataImmatricolazione() && verificaTarga() && verificaTelaio()  && verificaDecorrenza() ){
            $("#avanti1").prop('disabled', false);
        }else{
            $("#avanti1").prop('disabled', true);
        }
    });
    //autoveicolo
    $("#stato").on("change", function(event) {
        if(($.trim($("#stato").val()) != '' ) && ($.trim($("#stato").val()) =='da contattare') ){
            $(".chiamastato").show();
        }else{
            $(".chiamastato").hide();
        }
    });

    $("#dataImmatricolazione").on("changeDate", function(event) {
        var data1 = $("#dataImmatricolazione").val().split('-');
        if(data1 != ''){
            if(verificaDataImmatricolazione()){
                if(verificaTelaio() && verificaEmailContr() && verificaTelefono() && verificaTarga()  && verificaValore()  && verificaDecorrenza()){$("#avanti1").prop('disabled', false);}
                else{$("#avanti1").prop('disabled', true);}
            }else{
                $( ".alert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtdataImmatricolazione);
                $("#avanti1").prop('disabled', true);
            }

        }else{$("#bottonSalva").prop('disabled', true);}
    });

    $('#bottonSalva').click(function(e) {
        e.preventDefault();
        alert('uno');
        var tipoimmatr = false;
        if($("#tipoimmatr").is(":checked")){  tipoimmatr= true;}
        if(($.trim($("#targa").val()) != '' )&& verificaTarga() && verificatargaprima()){
            alert('due');
            $( ".labelErrore" ).remove();
            var bottone="salva";
            chiamataWS(bottone);
        }else if(($.trim($("#targa").val()) != '' )&& verificaTarga() && !verificatargaprima() && tipoimmatr){
            alert('entro qui 1');
            $( ".labelErrore" ).remove();
            var bottone="salva";
            chiamataWS(bottone);
        }
        else{
            alert('tre');
            swal({
                title: "controllare che la targa sia stata inserita!",
                type: "error",
                timer: "2000",
                showConfirmButton: false
            });
        }
    });
    $('#chiamataWS').click(function(e) {
        e.preventDefault();
        var bottone="chiamata";
        chiamataWS(bottone);
    });
    function chiamataWS(bottone){
        alert('chiamata2');
        var idPolizza=$("#idPolizza").val();
        var targa=$("#targa").val();
        var provincia=$("#provincia").val();
        var comune=$("#localita").val();
        var dataDecorrenza=$("#dataDecorrenza").val();
        var valoreAssicurato=$("#valoreAssicurato").val();
        var percVenditore=$("#percVend").val();
        var modello=$('#modello').val();
        var tipopolizza=$('#tipoPolizza').val();
        var prodotto=$('#coperturaRichiesta').val();
        var tipoVeicolo=$('#tveicolo').val();
        var cognome=$('#cognome').val();
        var nome=$('#nome').val();
        var partitaiva=$('#partitaIva').val();
        var stato=$('#stato').val();
        var telefono=$('#telefonoDef').val();
        var cellulare=$('#cellulare').val();
        var email=$('#emailDef').val();
        var indirizzo=$('#indirizzo').val();
        var comune=$('#comune').val();
        var cap=$('#cap').val();
        var sesso=$('#sesso').val();
        var provincia=$('#provincia').val();
        var bonus=$('#bonus').val();
        $.post("${createLink(controller: "polizze", action: "chiamataWS")}",{ bottone:bottone, stato:stato, idPolizza:idPolizza,percVenditore:percVenditore,valoreAssicurato:valoreAssicurato,
            provincia:provincia, comune:comune,dataDecorrenza:dataDecorrenza,modello:modello,targa:targa, tipopolizza:tipopolizza, prodotto:prodotto,tipoVeicolo:tipoVeicolo, cognome:cognome,
            nome:nome, partitaiva:partitaiva,  _csrf: "${request._csrf.token}"}, function(response) {
            var risposta=response.risposta;

            if(risposta==false){
                var errore=response.errore;
                var provvDealer=Math.round(response.provvDealer * 100) / 100
                var provvGmfi=Math.round(response.provvgmfi * 100) / 100
                $( ".labelErrore" ).remove();
                //$( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"><p>"+response.errore+"</p></div>" );

            }else{
                if(response.bottone){
                    var bottone=response.bottone;
                    if(bottone=="chiamata"){
                        $("#premioNettoWS").val(response.premioImponibile);
                        $("#premioLordoWS").val(response.premioLordo);
                        $(".chiamaWS").show();
                    }else{
                        swal({
                            title: "Polizza salvata!",
                            type: "success",
                            timer: "1800",
                            showConfirmButton: false
                        });
                        $('#modalPolizza').modal('hide');
                        //location.reload();
                        setTimeout("location.reload(true);",2500);
                    }
                }
            }
        }, "json");

    }
</script>
</body>
</html>