<%@ page import="cashopel.polizze.TipoVeicolo" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="navigazioneRCA" charset="UTF-8"/>
    <title>Nuova polizza</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
<div class="row">
    <div class="col-md-6" style="margin-bottom: 10px;">
        <g:if test="${pagina.toString().contains("RCA")}">
            <link:listRCA page="1" controller="polizzeRCA" attrs="[class: 'btn btn-annulla pull-left']">Torna elenco polizze</link:listRCA>
        </g:if>
        <g:elseif test="${pagina.toString().contains("DACONTTATARE")}">
            <link:listdacontt page="1" controller="polizzeRCA" attrs="[class: 'btn btn-annulla pull-left']">Torna elenco polizze</link:listdacontt>
        </g:elseif>
        <g:elseif test="${pagina.toString().contains("ANNULLATA")}">
            <link:listannullate page="1" controller="polizzeRCA" attrs="[class: 'btn btn-annulla pull-left']">Torna elenco polizze</link:listannullate>
        </g:elseif>
    </div>
    <div class="col-md-12 col-xs-10">
        <g:if test="${flash.error}"><div class="labelErrore marginesotto " > ${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
        <g:if test="${flash.message =="RICHIESTA_INVIATA"}">
            <script type="text/javascript">swal({
                title: "RICHIESTA INVIATA!",
                type: "success",
                timer: "3800",
                showConfirmButton: false
                });
            </script>
        </g:if>
        <div class="inner testoalert">  </div>
        <div class="datiPoli"></div>

        <f:form  id="polizzaRCAForm"   class="form-horizontal" enctype="multipart/form-data">
                <div class="tab-content">
                    <div id="sectionA" class="" style="background-color: #f7d900; padding: 15px;">
                        <input type="hidden" name="percVenditore" id="percVenditore" value="<g:if test="${dealerCensitiCodici}">${dealerCensitiCodici.percVenditore}</g:if><g:elseif test="${polizza?.dealer?.percVenditore}">${polizza?.dealer?.percVenditore}</g:elseif>" >
                        <input type="hidden" name="rappel" id="rappel" value="${polizza.rappel?:0.0}" >
                        <input type="hidden" name="imposte" id="imposte" value="${polizza.imposte?:0.0}" >
                        <input type="hidden" name="dacsv" id="dacsv" value="${polizza.dacsv}" >
                       <input type="hidden" name="provvDealer" id="provvDealer" value="${polizza.provvDealer?:0.0}" >
                        <input type="hidden" name="provvGmfi" id="provvGmfi" value="${polizza.provvGmfi?:0.0}" >
                        <input type="hidden" name="provvVenditore" id="provvVenditore" value="${polizza.provvVenditore?:0.0}" >
                        <input type="hidden" name="codiceZonaTerritoriale" id="codiceZonaTerritoriale" value="${polizza.codiceZonaTerritoriale?:0}" >
                        <input type="hidden" name="categoriaveicolo" id="categoriaveicolo" value="${polizza.categoriaveicolo?:null}" >
                        %{--<sec:ifHasRole role="ADMIN">
                            <div class="datiDealer" style="margin-top: 10px;">
                                <legend class="legenda"><span class="fa fa-users" style="margin-top: 20px;"></span> DATI DEALER</legend>
                                <div class="col-xs-6 col-md-6" >
                                    <label class="testoParagraph control-label">RAGIONE SOCIALE</label>
                                    <div id="dealer">
                                        <input id="dealer-input" type="text" align="center" name="dealerCensiti.ragioneSocialeD"  class="col-md-6 form-controlI typeahead"  value="<g:if test="${dealerCensiti}">${dealerCensiti.ragioneSocialeD}</g:if><g:else>${polizza.dealer.ragioneSocialeD}</g:else>"  placeholder="inserire la ragione sociale del dealer">
                                        <input type="hidden" name="dealer.id" id="id-dealer" value="<g:if test="${dealerCensitiCodici}">${dealerCensitiCodici.id}</g:if><g:else>${polizza.dealer.id}</g:else>" >
                                    </div>
                                </div>
                            </div>
                            <div class="row"></div>
                        </sec:ifHasRole>--}%
                        <input type="hidden" name="idPolizza" id="idPolizza" value="${polizza.id?:""}"/>
                        <input type="hidden" name="isCash" id="isCash" value="${polizza.isCash}"/>
                        <input type="hidden" name="percVend" id="percVend" value="<g:if test="${polizza.id}">${polizza?.dealer.percVenditore?:""}</g:if>"/>
                        <div class="col-xs-6 col-md-4 marginesopra marginesotto">
                            <label class="testoParagraph control-label ">TIPO PRODOTTO</label>
                            %{--<input class="form-controlI maiuscola" type="text" name="tipoPolizza" id="tipoPolizza"   value="${polizza.tipoPolizza?:""}" />--}%
                            <select class="form-controlI" name="tipoPolizza" id="tipoPolizza">
                                <option value="" disabled selected>Seleziona una opzione</option>
                                <option value="${cashopel.polizze.TipoPolizza.CASH}" <g:if test="${polizza.tipoPolizza==cashopel.polizze.TipoPolizza.CASH}">selected </g:if> >CASH</option>
                                <option value="${cashopel.polizze.TipoPolizza.FINANZIATE}" <g:if test="${polizza.tipoPolizza==cashopel.polizze.TipoPolizza.FINANZIATE}">selected </g:if> >FINANZIATE</option>
                                <option value="${cashopel.polizze.TipoPolizza.LEASING}" <g:if test="${polizza.tipoPolizza==cashopel.polizze.TipoPolizza.LEASING}">selected </g:if> >LEASING</option>
                            </select>
                        </div>
                        <div class="col-xs-6 col-md-4 marginesopra marginesotto">
                            <label class="testoParagraph control-label ">PRODOTTO</label>
                            <input class="form-controlI maiuscola" type="text" name="coperturaRichiesta" id="coperturaRichiesta" readonly value="FLEX RCA" />
                        </div>
                        <div class="col-xs-6 col-md-4 marginesopra marginesotto">
                            <label class="testoParagraph control-label">TIPO VEICOLO</label>
                            <select class="form-controlI" name="tipoVeicolo" id="tipoVeicolo"   >
                                <option value="" disabled selected>Seleziona una opzione</option>
                                <option value="${cashopel.polizze.TipoVeicolo.AUTOCARRO}" <g:if test="${polizza.tipoVeicolo==cashopel.polizze.TipoVeicolo.AUTOCARRO}">selected </g:if> >AUTOCARRO</option>
                                <option value="${cashopel.polizze.TipoVeicolo.AUTOVEICOLO}" <g:if test="${polizza.tipoVeicolo==cashopel.polizze.TipoVeicolo.AUTOVEICOLO}">selected</g:if>>AUTOVETTURA</option>
                            </select>
                        </div>
                        <legend class="legenda "><span class="fa fa-user"></span> DATI DEL CLIENTE</legend>
                        <div class="col-xs-6 col-md-4 ">
                            <label class="testoParagraph control-label ">COGNOME</label>
                            <input class="form-controlI maiuscola" type="text" name="cognome" id="cognome"    value="${polizza.cognome?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-4 ">
                            <label class="testoParagraph control-label ">NOME</label>
                            <input class="form-controlI maiuscola" type="text" name="nome" id="nome"   value="${polizza.nome?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-4 ">
                            <label class="testoParagraph control-label">C.FISCALE / P.IVA</label>
                            <input class="form-controlI maiuscola" type="text" name="partitaIva"  id="partitaIva"   value="${polizza.partitaIva?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-4">
                            <label class="testoParagraph control-label">TELEFONO</label>
                            <input class="form-controlI maiuscola" type="text" name="telefonoDef"  id="telefonoDef"  value="${polizza.telefono?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-4">
                            <label class="testoParagraph control-label">CELLULARE</label>
                            <input class="form-controlI maiuscola" type="text" name="cellulare"  id="cellulare"  value="${polizza.cellulare?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-4">
                            <label class="testoParagraph control-label">EMAIL</label>
                            <input class="form-controlI maiuscola" type="text" name="email"  id="email"   value="${polizza.email?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-5 marginesotto">
                            <label class="testoParagraph control-label">INDIRIZZO</label>
                            <input class="form-controlI maiuscola testoParagraph" type="text" name="indirizzo" id="indirizzo"  value="${polizza.indirizzo?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-5 marginesotto">
                            <label class="testoParagraph control-label">LOCALITA'</label>
                            <input class="form-controlI maiuscola" type="text" name="localita" id="localita" value="${polizza.localita?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-2 marginesotto">
                            <label class="testoParagraph control-label">CAP</label>
                            <input class="form-controlI maiuscola testoParagraph" type="text" name="cap" id="cap" value="${polizza.cap?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-2">
                            <label class="testoParagraph control-label">SESSO</label>
                            <select class="form-controlI" name="tipoCliente" id="tipoCliente"> <!--Supplement an id here instead of using 'name'-->
                                <option value="" disabled selected>Seleziona una opzione</option>
                                <option  <g:if test="${polizza.tipoCliente==cashopel.polizze.TipoCliente.F}">value="${cashopel.polizze.TipoCliente.F}" selected </g:if> >F</option>
                                <option  <g:if test="${polizza.tipoCliente==cashopel.polizze.TipoCliente.M}">value="${cashopel.polizze.TipoCliente.M}" selected </g:if> >M</option>
                            </select>
                        </div>
                        <div class="col-xs-6 col-md-2">
                            <label class="testoParagraph control-label">PROVINCIA</label>
                            <select class="form-controlI testoParagraph" name="provincia" id="provincia"   >
                                <option value="" disabled selected>Seleziona una opzione</option>
                                <g:each var="provincia" in="${province}">
                                    <option <g:if test="${polizza.provincia==provincia.value}"> value="${provincia.value}" selected </g:if> >${provincia.value}</option>
                                </g:each>
                            </select>
                        </div>
                        <div class="col-xs-6 col-md-4">
                            <label class="testoParagraph control-label">EMAIL DEALER</label>
                            <input class="form-controlI maiuscola" type="text" name="emailDealer"   id="emailDealer" value="${polizza?.emailDealer?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-2">
                            <label class="testoParagraph control-label">COD. DEALER</label>
                            <div id="codice">
                                <input id="codice-input" type="text" align="center" name="dealerCensitiCodici.codice"   class="col-md-6 form-controlI typeahead"  value="<g:if test="${dealerCensitiCodici}">${dealerCensitiCodici.codice}</g:if><g:else>${polizza?.dealer?.codice}</g:else>" placeholder="inserire il codice">
                                <input type="hidden" name="codice.id" id="id-dealer2" value="<g:if test="${dealerCensitiCodici}">${dealerCensitiCodici.id}</g:if><g:else>${polizza?.dealer?.id}</g:else>" >
                            </div>
                            %{--<input  type="text" name="codice" class="form-controlI testoParagraph"  id="codice" readonly="true" value="" />--}%
                        </div>
                        <div class="col-xs-6 col-md-4 marginesotto">
                            <label class="testoParagraph control-label">VENDITORE</label>
                            <input class="form-controlI maiuscola" type="text" name="venditore" id="venditore" value="${polizza?.venditore?:""}" />
                        </div>
                        <div class="row"></div>
                        <legend class="legenda"><span class="fa fa-car"></span> DATI DEL AUTOVEICOLO</legend>
                        <div class="col-xs-6 col-md-2" >
                            <label class="testoParagraph control-label">NO. PRATICA</label>
                            <input class="form-controlI maiuscola" type="text" name="noPolizza" id="noPolizza"  maxlength="7" size="7"  value="${polizza.noPolizza?:""}"/>
                        </div>
                        <div class="col-xs-6 col-md-2">
                            <label class="testoParagraph control-label">MARCA</label>
                            <input class="form-controlI maiuscola " type="text" name="marca"  id="marca"   value="${polizza.marca?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">MODELLO</label>
                            <input class="form-controlI maiuscola " type="text" name="modello" id="modello"   value="${polizza.modello?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-5">
                            <label class="testoParagraph control-label">ALLESTIMENTO</label>
                            <input class="form-controlI maiuscola" type="text" name="versione" id="versione"   value="${polizza.versione?:""}"  />
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">VALORE ASSIC. (&euro;)</label>
                            <input class="form-controlI" type="number" name="valoreAssicurato"   id="valoreAssicurato"   value="${polizza.valoreAssicurato?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">DATA IMMATR.</label>
                            <input class="form-controlI testoParagraph" type="text" name="dataImmatricolazione"   id="dataImmatricolazione" value="${polizza.dataImmatricolazione?.format("dd-MM-yyyy")?:""}"
                                   data-provide="datepicker"
                                   data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"  data-date-end-date="0d"
                                     data-date-today-highlight="true" data-date-toggle-active="false"
                                     data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"  />
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">DATA DECORRENZA</label>
                            <input class="form-controlI " type="text" name="dataDecorrenza"  id="dataDecorrenza"   value="${polizza.dataDecorrenza?.format("dd-MM-yyyy")?:""}" data-provide="datepicker"
                                   data-date-language="it" data-date-format="dd-mm-yyyy"
                                   data-date-today-btn="false" data-date-autoclose="true"  data-date-start-date="0d"
                                     data-date-today-highlight="true" data-date-toggle-active="false"
                                     data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"  />
                            %{--<f:input control-class="form" type="text" name="dataDecorrenza"  id="dataDecorrenza" value=""  required="false"/>--}%
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">DATA SCADENZA</label>
                            <input class="form-controlI maiuscola"  type="text" name="dataScadenza"   id="dataScadenza" value="${polizza.dataScadenza?.format("dd-MM-yyyy")?:""}" disabled />
                            %{--<input class="form-controlI " type="text" name="dataScadenza"  id="dataScadenza" value="${polizza.dataScadenza?.format("dd-MM-yyyy")?:""}" disabled data-provide="datepicker"
                                   data-date-language="it" data-date-format="dd-mm-yyyy"
                                   data-date-today-btn="false" data-date-autoclose="true" --}%%{-- data-date-end-date="0d"--}%%{--
                                     data-date-today-highlight="true" data-date-toggle-active="false"
                                     data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false" />--}%
                            %{-- <f:input control-class="form" type="text" name="dataScadenza"  id="dataScadenza" value=""  required="false"/>--}%
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">TELAIO</label>
                            <input class="form-controlI maiuscola " type="text" name="telaio"  id="telaio" value="${polizza.telaio?:""}"  />
                        </div>

                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">TARGA</label>
                            <input class="form-controlI maiuscola"  type="text" name="targa"  id="targa" value="${polizza.targa?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">DURATA</label>
                            <input class="form-controlI maiuscola"  type="number" name="durata"   id="durata" value="${polizza.durata?:12}" disabled="" />
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">CILINDRATA</label>
                            <input class="form-controlI"  type="number" name="cilindrata"  id="cilindrata"   value="${polizza.cilindrata?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">CAVALLI</label>
                            <input class="form-controlI"  type="number" name="cavalli" id="cavalli" value="${polizza.cavalli?:""}"  readonly/>
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">QUINTALI</label>
                            <input class="form-controlI"  type="number" name="quintali" id="quintali" value="${polizza.quintali?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">KW</label>
                            <input class="form-controlI"  type="number" name="kw"   id="kw" value="${polizza.kw?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">PROV. IMMATRICOLAZIONE</label>
                            <select class="form-controlI testoParagraph" name="provImmatricolazione" id="provImmatricolazione" >
                                <g:if test="${polizza.tipoPolizza!=cashopel.polizze.TipoPolizza.LEASING}">
                                    <option value="" disabled selected>Seleziona una opzione</option>
                                    <g:each var="provincia" in="${province}">
                                        <option <g:if test="${polizza.provincia==provincia.value}"> value="${provincia.value}" selected </g:if> >${provincia.value}</option>
                                    </g:each>
                                </g:if>
                                <g:else>
                                    <option value="RM" selected readonly="readonly">RM</option>
                                </g:else>
                            </select>
                        </div>
                        <div class="col-xs-6 col-md-3 marginesotto">
                            <label class="testoParagraph control-label">DEST. USO</label>
                            <input class="form-controlI"  name="destUso"   id="destUso" value="PROPRIO" readonly />
                        </div>
                        <div class="row"></div>
                        <legend class="legenda "><span class="fa fa-user"></span> SITUAZIONE ASSICURATIVA</legend>
                        <div class="col-md-5 col-xs-6">
                            <div class="col-md-6 col-xs-3">PRIMA IMMATRICOLAZIONE: <input type="checkbox" class="centered" name="primaImmatr"  value="${polizza.primaImmatr?:""}" <g:if test="${polizza.primaImmatr}"> checked </g:if> id="primaImmatr"   text="PRIMA IMMATRICOLAZIONE"/></div>
                        </div>
                        <div class="col-xs-6 col-md-2 col-md-pull-2">
                            <label class="testoParagraph control-label">AGEV. DECRETO BERSANI</label>
                            <select class="form-controlI" name="bonus" id="bonus"  > <!--Supplement an id here instead of using 'name'-->
                                <option value="" disabled selected>Seleziona una opzione</option>
                                <option <g:if test="${polizza.bonus=="S"}"> value="S" selected </g:if> >S</option>
                                <option <g:if test="${polizza.bonus=="N"}"> value="N" selected </g:if> >N</option>
                            </select>
                        </div>
                        <div class="col-xs-6 col-md-3 col-md-pull-1">
                            <label class="testoParagraph control-label">TARGA old</label>
                            <input class="form-controlI maiuscola"  type="text" name="targa1a" id="targa1a" value="${polizza.targa1a?:""}" />
                        </div>

                        <div class="row"></div>
                        <legend class="legenda"><span class="fa fa-eur"></span> PREMI</legend>
                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">PREMIO LORDO</label>
                            <input class="form-controlI"  type="number" name="premioLordo" readonly  id="premioLordo" value="${polizza.premioLordo?:""}" />
                        </div>
                            <div class="col-xs-6 col-md-4">
                                <a href="" %{--onclick="return chiamataWS();"--}% class="btn btn-avanti  bottoneWS" name="chiamataWS" id="chiamataWS"  title="chiamata web service"><b>CHIAMATA WEB SERVICE</b></a>

                            </div>
                       %{-- <div class="col-xs-6 col-md-3 col-md-pull-1 chiamaWS">
                            <br>
                            <label class="testoParagraph control-label" >PROVV. DEALER</label>
                            <input class="form-controlI"  type="number" name="provvDealer"  id="provvDealer" readonly value="${polizza.provvDealer?:""}"  />
                        </div>--}%
                        <div class="row"></div>
                        <div class="col-xs-6 col-md-3 marginesotto">
                            <label class="testoParagraph control-label">PREMIO NETTO</label>
                            <input class="form-controlI"  type="number" name="premioImponibile" id="premioImponibile" readonly value="${polizza.premioImponibile?:""}" />
                        </div>
                        <div class="col-xs-6 col-md-4 chiamaWS marginesotto">
                            <br>
                        </div>
                        %{--<div class="col-xs-6 col-md-3 col-md-pull-1 chiamaWS marginesotto">
                            <label class="testoParagraph control-label" >PROVV. GMFI</label>
                            <input class="form-controlI"  type="number" name="provvGmfi"   id="provvGmfi" readonly value="${polizza.provvGmfi?:""}"  />
                        </div>
                        <div class="col-xs-6 col-md-4 chiamaWS marginesotto">
                            <br>
                        </div>
                        <div class="col-xs-6 col-md-3 col-md-push-2 chiamaWS marginesotto">
                            <label class="testoParagraph control-label" >PROVV. VENDITORE</label>
                            <input class="form-controlI"  type="number" name="provvVenditore" readonly  id="provvVenditore" value="${polizza.provvVenditore?:""}"  />
                        </div>--}%
                        <div class="row"></div>
                        <legend class="legenda"><span class="fa fa-check-square"></span> ATTIVITA'</legend>
                           %{--<div class="col-xs-6 col-md-3">
                                <label class="testoParagraph control-label">STATO</label>
                                <select class="form-controlI" name="stato" id="stato"> <!--Supplement an id here instead of using 'name'-->
                                    <option value="" disabled selected>Seleziona se vuoi che diventi polizza </option>
                                    --}%%{--<option value="${cashopel.polizze.StatoPolizza.PREVENTIVO}" <g:if test="${polizza.stato==cashopel.polizze.StatoPolizza.PREVENTIVO}">selected </g:if>>PREVENTIVO</option>
                                    <option value="${cashopel.polizze.StatoPolizza.RICHIESTA_INVIATA}" <g:if test="${polizza.stato==cashopel.polizze.StatoPolizza.RICHIESTA_INVIATA}">selected </g:if>>RICHIESTA INVIATA</option>--}%%{--
                                    <option value="${cashopel.polizze.StatoPolizza.POLIZZA}" <g:if test="${polizza.stato==cashopel.polizze.StatoPolizza.POLIZZA}">selected </g:if>>POLIZZA</option>
                                    --}%%{--<option value="${cashopel.polizze.StatoPolizza.ANNULLATA}" <g:if test="${polizza.stato==cashopel.polizze.StatoPolizza.ANNULLATA}">selected </g:if>>ANNULLATA</option>
                                    <option value="${cashopel.polizze.StatoPolizza.DA_CHIAMARE}" <g:if test="${polizza.stato==cashopel.polizze.StatoPolizza.DA_CHIAMARE}">selected </g:if>>DA CHIAMARE</option>
                                    <option value="${cashopel.polizze.StatoPolizza.ANNULLATA_FURTO}" <g:if test="${polizza.stato==cashopel.polizze.StatoPolizza.ANNULLATA_FURTO}">selected </g:if>>ANNULLATA PER FURTO</option>--}%%{--
                                </select>
                            </div>--}%
                        <div class="row"></div>
                        <div class="col-md-5 col-xs-6" style="margin-top: 25px;">
                            <div class="col-md-6 col-xs-3">DA CHIAMARE: <input type="checkbox" class="centered" name="daChiamare"  value="${polizza.daChiamare?:""}" <g:if test="${polizza.daChiamare}"> checked </g:if> id="daChiamare"   text="DA CHIAMARE"/></div>
                        </div>
                        <div class="col-xs-6 col-md-3 chiamastato col-md-pull-2">
                            <label class="testoParagraph control-label" >CAUSALE CONTATTO</label>
                            <select class="form-controlI" name="causaleContatto" id="causaleContatto"> <!--Supplement an id here instead of using 'name'-->
                                <option value="" disabled selected>Seleziona una opzione</option>
                                <option value="${cashopel.polizze.CausaleContatto.DATO_MANCANTE}" <g:if test="${polizza.causaleContatto==cashopel.polizze.CausaleContatto.DATO_MANCANTE}">selected </g:if>>DATO MANCANTE</option>
                                <option value="${cashopel.polizze.CausaleContatto.ERRORE_ANIA}" <g:if test="${polizza.causaleContatto==cashopel.polizze.CausaleContatto.ERRORE_ANIA}">selected </g:if>>ERRORE ANIA</option>
                            </select>
                        </div>
                        <div class="col-xs-6 col-md-3 chiamastato col-md-pull-2">
                            <label class="testoParagraph control-label" >CONTATTARE</label>

                            <select class="form-controlI" name="contattare" id="contattare"> <!--Supplement an id here instead of using 'name'-->
                                <option value="" disabled selected>Seleziona una opzione</option>
                                <option value="${cashopel.polizze.Contattare.CLIENTE}" <g:if test="${polizza.contattare==cashopel.polizze.Contattare.CLIENTE}">selected </g:if>>CLIENTE</option>
                                <option value="${cashopel.polizze.Contattare.CONCESSIONARIO}" <g:if test="${polizza.contattare==cashopel.polizze.Contattare.CONCESSIONARIO}">selected </g:if>>CONCESSIONARIO</option>
                            </select>
                        </div>
                        <div class="row"></div>
                        <div class="col-xs-6 col-md-3">
                            <label class="testoParagraph control-label">DATA ANNULLAMENTO</label>
                            <input class="form-controlI " type="text" name="dataAnnullamento"  id="dataAnnullamento"   value="${polizza.dataAnnullamento?.format("dd-MM-yyyy")?:""}" data-provide="datepicker"
                                   data-date-language="it" data-date-format="dd-mm-yyyy"
                                   data-date-today-btn="false" data-date-autoclose="true" %{-- data-date-end-date="0d"--}%
                                   data-date-today-highlight="true" data-date-toggle-active="false"
                                   data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"  />
                            %{--<f:input control-class="form" type="text" name="dataDecorrenza"  id="dataDecorrenza" value=""  required="false"/>--}%
                        </div>
                        <div class="col-xs-6 col-md-3 annullata">
                            <label class="testoParagraph control-label" >MOTIVO ANNULLAMENTO</label>
                            <select class="form-controlI" name="motivoAnnulla" id="motivoAnnulla"> <!--Supplement an id here instead of using 'name'-->
                                <option value="" disabled selected>Seleziona una opzione</option>
                                <option value="${cashopel.polizze.MotivoAnnulla.ANNULLATA}" <g:if test="${polizza.motivoAnnulla==cashopel.polizze.MotivoAnnulla.ANNULLATA}">selected </g:if>>ANNULLATA</option>
                                <option value="${cashopel.polizze.MotivoAnnulla.ANNULLATA_PER_FURTO}" <g:if test="${polizza.motivoAnnulla==cashopel.polizze.MotivoAnnulla.ANNULLATA_PER_FURTO}">selected </g:if>>ANNULLATA PER FURTO</option>
                            </select>
                        </div>
                        <div class="row"></div>
                        <div class="row">
                            <div class="col-xs-6 col-md-12">
                                <button type="submit" id="bottonSalva" name="bottonSalva" class="btn-avanti btn pull-right">&nbsp;&nbsp;Salva polizza&nbsp;&nbsp;</button>
                            </div>
                        </div>
                        <div class="row"></div>
                    </div>

                </div>
        </f:form>
    </div>
</div>
<script type="text/javascript">

    $(document).ready(function() {
       // $(".chiamaWS").hide();
            //var dacsv=$("#dacsv").val();
           // alert(dacsv);
        var modello= $.trim($("#modello").val());
        if(modello.indexOf("ANTARA")>-1){
            $("#modello").val("ANTARA");
        }else if(modello.indexOf("ADAM")>-1){
            $("#modello").val("ADAM");
        }else if(modello.indexOf("ASTRA")>-1){
            $("#modello").val("ASTRA");
        }else if(modello.indexOf("CASCADA")>-1){
            $("#modello").val("CASCADA");
        }else if(modello.indexOf("COMBO")>-1){
            $("#modello").val("COMBO");
        }else if(modello.indexOf("CORSA")>-1){
            $("#modello").val("CORSA");
        }else if(modello.indexOf("CROSSLAND X")>-1){
            $("#modello").val("CROSSLAND X");
        }else if(modello.indexOf("INSIGNIA")>-1){
            $("#modello").val("INSIGNIA");
        }else if(modello.indexOf("KARL")>-1){
            $("#modello").val("KARL");
        }else if(modello.indexOf("MERIVA")>-1){
            $("#modello").val("MERIVA");
        }else if(modello.indexOf("MOKKA")>-1){
            $("#modello").val("MOKKA X");
        }else if(modello.indexOf("ZAFIRA")>-1){
            $("#modello").val("ZAFIRA");
        }else if(modello.indexOf("MOVANO")>-1){
            $("#modello").val("MOVANO");
        }else if(modello.indexOf("VIVARO")>-1){
            $("#modello").val("VIVARO");
        }else if(modello.indexOf("AGILA")>-1){
            $("#modello").val("AGILA");
        }else if(modello.indexOf("GRANDLAND X")>-1){
            $("#modello").val("GRANDLAND X");
        }

        if($("#dacsv").val()=='true'){
            $("#valoreAssicurato").prop('readonly', true);
            $(".bottoneWS").hide();
            $(".chiamaWS").hide();

        }else{
            $(".bottoneWS").show();
            $(".chiamaWS").show();


        }
        //var tipoveicolo=$('#tipoVeicolo').val();
        if($('#tipoVeicolo').val()=='AUTOCARRO'){
            $("#cavalli").val('');
            $("#cavalli").prop('disabled', true);
        }else if($('#tipoVeicolo').val()=='AUTOVEICOLO'){
            $("#quintali").val('');
            $("#quintali").prop('disabled', true);
        }

    /*if(verificaTipoProdotto() && verificaTipoVeicolo() && verificaCognome() && verificaNomeCliente() && verificapartitaIva() && verificaEmailCliente() && verificaIndirizzoCliente()
        && verificaLocalitaCliente() && verificaCapCliente() && verificaProvCliente() && verificaPratica() && verificaMarchio() && verificaModello() && verificaVersione() && verificaValore()
    && verificaDataImmatricolazione()&& verificaDataDecorrenza() && verificaTarga() && verificacilindrata() && verificakw() && verificaprovImma() && verificaPremioL() && verificaPremioN()){
            $("#bottonSalva").prop('disabled', false);
    }else{
        $("#bottonSalva").prop('disabled', true);
    }
*/
        if($("#daChiamare").is(":checked")){
            $(".chiamastato").show();
        }else{
            $(".chiamastato").hide();
        }
        var dataannulla=$("#dataAnnullamento").val()
        if(dataannulla !=''){
                $(".annullata").show();
        }

        window.setTimeout(function() {
            $(".alert").fadeTo(1500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 5000);
    });

    $('#dealer .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength:4,
            classNames: {
                menu: 'popover',
                dataset: 'popover-content'
            }
        }, {
            name: "elencodealers",
            display: "ragioneSocialeD",
            limit: 20,
            source: new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '${createLink(controller: "polizze", action: "dealerCensiti", params: [ragioneSocialeD: "nome"])}',
                    wildcard: 'nome'
                }
            }),
            templates:{
                header: Handlebars.compile( '<p class="testoTypeahead"><strong>Ragione sociale:</strong></p>'),
                suggestion: Handlebars.compile('<div>{{#if ragioneSocialeD}} {{ragioneSocialeD}} {{else}} <p class="testoTypeahead"><strong><i>il dealer inserito non si trova nell\'elenco di dealer disponibili</i></strong></p>{{/if}}</div>')
            }
        }
    ).on("typeahead:select", function(event, selected) {
        if(selected.id >0){
            //if(selected.username){
            $.post("${createLink( controller: "polizze", action: 'getDealer')}",{ nodealer: selected.id,  _csrf: "${request._csrf.token}"}, function(response) {
                var risposta=response.risposta;
                if(risposta==false){
                }else{
                    $("#codice-input").val(response.codice);
                    $("#percVenditore").val(response.percVenditore);
                    /*$("#pivaD").val(response.pivaD);
                    $("#indirizzoD").val(response.indirizzoD);
                    $("#provinciaD").val(response.provinciaD);
                    $("#capD").val(response.capD);
                    $("#localitaD").val(response.localitaD);
                    $("#telefonoD").val(response.telefonoD);
                    $("#faxD").val(response.faxD);
                    $("#emailD").val(response.emailD);*/
                    $("#dealerType").val(response.dealerType);
                }
            }, "json");
            //}
        }
        $("#id-dealer").val(selected.id);
        /*        $.post("%{--${createLink(action: 'datiDealer')}",{ id: selected.id, _csrf: "${request._csrf.token}--}%"}, function(response) {
         if(response.id>0){
         $("#salva").prop('readonly', true);
         }

         });*/
    }).on("blur", function(event) {
        var input = $(this);
        if(input.val() == "") {
            input.typeahead("val", "");
            $("#id-dealer").val(null);
            $.post("${createLink(controller: "polizze", action: 'datiDealerT')}",{ id: null, _csrf: "${request._csrf.token}"}, function(response) {});
        }
    });
    $('#codice .typeahead').typeahead({
            hint: true,
            highlight: true,
            minLength:3,
            classNames: {
                menu: 'popover',
                dataset: 'popover-content'
            }
        }, {
            name: "elencocodici",
            display: "codice",
            limit: 20,
            source: new Bloodhound({
                datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                queryTokenizer: Bloodhound.tokenizers.whitespace,
                remote: {
                    url: '${createLink(controller: "polizze", action: "dealerCensitiCodici", params: [codice: "codiceD"])}',
                    wildcard: 'codiceD'
                }
            }),
            templates:{
                header: Handlebars.compile( '<p class="testoTypeahead"><strong>Codice:</strong></p>'),
                suggestion: Handlebars.compile('<div>{{#if codice}} {{codice}} {{else}} <p class="testoTypeahead"><strong><i>il dealer inserito non si trova nell\'elenco di dealer disponibili</i></strong></p>{{/if}}</div>')
            }
        }
    ).on("typeahead:select", function(event, selected) {
        if(selected.id >0){
            //if(selected.username){
            $.post("${createLink( controller: "polizze", action: 'getDealerCodice')}",{ nodealer: selected.id,  _csrf: "${request._csrf.token}"}, function(response) {
                var risposta=response.risposta;
                if(risposta==false){
                }else{
                    $("#dealer-input").val(response.ragioneSocialeD);
                    $("#percVenditore").val(response.percVenditore);
                    /*$("#pivaD").val(response.pivaD);
                    $("#indirizzoD").val(response.indirizzoD);
                    $("#provinciaD").val(response.provinciaD);
                    $("#capD").val(response.capD);
                    $("#localitaD").val(response.localitaD);
                    $("#telefonoD").val(response.telefonoD);
                    $("#faxD").val(response.faxD);
                    $("#emailD").val(response.emailD);*/
                    $("#dealerType").val(response.dealerType);
                }
            }, "json");
            //}
        }
        $("#id-dealer2").val(selected.id);
        /*        $.post("%{--${createLink(action: 'datiDealer')}--}%",{ id: selected.id, _csrf: "%{--${request._csrf.token}--}%"}, function(response) {
         if(response.id>0){
         $("#salva").prop('readonly', true);
         }

         });*/
    }).on("blur", function(event) {
        var input = $(this);
        if(input.val() == "") {
            input.typeahead("val", "");
            $("#id-dealer2").val(null);
            $.post("${createLink(controller: "polizze", action: 'datiDealerTCodice')}",{ id: null, _csrf: "${request._csrf.token}"}, function(response) {});
        }
    });


    var txttipoprodotto="";
    var txtiddealer="";
    var txttipoveicolo="";
    var txtCognomeCliente="";
    var txtNomeCliente="";
    var txtpartitaIvaCliente="";
    var txtemailCliente="";
    var txtIndirizzoCliente="";
    var txtLocalitaCliente="";
    var txtCapCliente="";
    var txtprovcliente="";
    var txtemaildealer="";
    var txtnopolizza="";
    var txtmarca="";
    var txtmodello="";
    var txtversione="";
    var txtvalore="";
    var txtdataImm="";
    var txtdatadeco="";
    var txtdataannulla="";
    var txttarga="";
    var txtcilindrata="";
    var txtcavalli="";
    var txtquintali="";
    var txtkw="";
    var txtprovimma="";
    var txtstato="";
    var txttarga_1a_imatr="";
    var txtpremioL="";
    var txtPremioN="";
    var txtdataImmatsup8="";
    var txtbonusmalus="";
    var txtcausale="";
    var txtcontattare="";
    var txtmotivoannulla="";
    var txtprimaImma="";
    var txtvenditore="";

    function verificaTipoProdotto(){
        var valore= $.trim($("#tipoPolizza").val());
        if(valore!=''){
            var tipoPolizza=removeDiacritics(valore);
            $("#tipoPolizza").val(tipoPolizza);
            return true;
        }
        else{
            txttipoprodotto="inserire il tipo prodotto";
            return false;
        }
    }
    function verificaTipoVeicolo(){
        var valore= $.trim($("#tipoVeicolo").val());
        if(valore!=''){
            var tipoVeicolo=removeDiacritics(valore);
            $("#tipoVeicolo").val(tipoVeicolo);
            return true;
        }
        else{
            txttipoveicolo="inserire il tipo veicolo";
            return false;
        }
    }
    function verificaCognome(){
        var valore= $.trim($("#cognome").val());
        if(valore!=''){
            var cognomec=removeDiacritics(valore);
            $("#cognome").val(cognomec);
            return true;
        }
        else{
            txtCognomeCliente="inserire il cognome";
            return false;
        }
    }
    function verificaNomeCliente(){
        var valore= $.trim($("#nome").val());
        if(valore!=''){
            var nomec=removeDiacritics(valore);
            $("#nome").val(nomec);
            return true;
        }
        else{
            txtNomeCliente="inserire il nome";
            return false;
        }
    }
    function verificaVenditore(){
        var valore= $.trim($("#venditore").val());
        if(valore!=''){
            var nomec=removeDiacritics(valore);
            $("#venditore").val(nomec);
            return true;
        }
        else{
            txtvenditore="inserire il nome del venditore";
            return false;
        }
    }
    function verificapartitaIva(){
        var valore= $.trim($("#partitaIva").val());
        var regpIva= /^[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z]$/;
        var regCF=/^[0-9]{11}$/;
        if(valore!=''  && (regpIva.test(valore)|| (regCF.test(valore)))){ return true;}
        else{
            txtpartitaIvaCliente="il formato della partita iva codice fiscale e' sbagliato";
            return false;
        }
    }
    function verificaEmailCliente() {
        var valore= $("#email").val();
        var regemailDef=/^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$/;
        if(valore!='' && regemailDef.test(valore)){
           // $( ".alert" ).remove();
            return true;
        }
        else{
            if(!regemailDef.test(valore) && valore!=''){ txtemailCliente="verificare il formato del campo email";}
            else {txtemailCliente="compilare il campo email";}
            return false;
        }
    }
    function verificaIndirizzoCliente(){
        var valore= $.trim($("#indirizzo").val());
        if(valore!=''){
            var indirizzoc=removeDiacritics(valore);
            $("#indirizzo").val(indirizzoc);
            return true;
        }
        else{
            txtIndirizzoCliente="inserire il indirizzo";
            return false;
        }
    }
    function verificaLocalitaCliente(){
        var valore= $.trim($("#localita").val());
        if(valore!=''){
            var localitaC=removeDiacritics(valore);
            $("#localita").val(localitaC);
            return true;
        }
        else{
            txtLocalitaCliente="inserire la localita'";
            return false;
        }
    }
    function verificaCapCliente(){
        var valore= $.trim($("#cap").val());
        var regcap=/^[0-9]{5}$/;
        if(valore!='' && regcap.test(valore)){
            return true;
        }
        else{
            txtCapCliente="inserire il cap o verificare il formato che sia di 5 numeri";
            return false;
        }
    }
    function verificaProvCliente(){
        var valore= $.trim($("#provincia").val());
        if(valore!=''){
            return true;
        }
        else{
            txtprovcliente="inserire la provincia del cliente";
            return false;
        }
    }
    function verificaEmailDealer(){
        var valore= $.trim($("#emailDealer").val());
        if(valore!=''){
            return true;
        }
        else{
            txtemaildealer="inserire la mail del Dealer";
            return false;
        }
    }
    function verificaDealer(){
        var valore= $.trim($("#id-dealer2").val());
        if(valore!=''){
            return true;
        }
        else{
                txtiddealer="inserire codice dealer";
                return false;

        }
    }
    function verificaPratica() {
        var valore= $("#noPolizza").val();
        if(valore!=''){ return true; }
        else  {
            txtnopolizza="compilare il numero pratica";

            return false;
        }
    }
    function verificaMarchio() {
        var valore= $.trim($("#marca").val().toUpperCase());
        if (valore!=''){
            /*if(valore=='AUDI' || valore=='BENTLEY' || valore=='BMW' || valore=='FERRARI' || valore=='LAMBORGHINI' || valore=='LOTUS' || valore=='MERCEDES' || valore=='PORSCHE'|| valore=='ROLLS ROYCE'){
                txtmarca="QUESTA MARCA NON E' COMPRESSA NELLA TARIFFA";
                return false;
            }else{*/
                return true;
            //}
        }
        else {
            txtmarca="compilare il marchio vettura";
            return false;
        }
    }
    function verificaModello() {
        var valore= $.trim($("#modello").val());
        if (valore!=''){
            /*var modello= $.trim($("#modello").val());
            if(modello.indexOf("CORSA 5 SERIE")>-1){
                txtversione="Il modello "+modello+"  deve essere solo CORSA";
                return false;
            }else if(modello.indexOf("COMBO TOUR")>-1 || modello.indexOf("COMBO TOUR")>-1){
                txtversione="Il modello "+modello+"  deve essere solo COMBO";
                return false;
            }else if(modello.indexOf("AGILA 1 SERIE")>-1 || modello.indexOf("AGILA 2 SERIE")>-1 || modello.indexOf("AGILA 3 SERIE")>-1 || modello.indexOf("AGILA 4 SERIE")>-1 || modello.indexOf("AGILA 5 SERIE")>-1){
                txtversione="Il modello "+modello+"  deve essere solo AGILA";
                return false;
            }else if(modello.indexOf("MERIVA 2 S.")>-1){
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append("Il modello "+modello+" deve essere solo MERIVA").append("<br>");
            }else if(modello.indexOf("MOKKA")>-1){
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append("Il modello "+modello+" deve essere  MOKKA X").append("<br>");
            }else if(modello.indexOf("VIVARO 3 SERIE")>-1){
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append("Il modello "+modello+"  deve essere  VIVARO").append("<br>");
            }else if(modello.indexOf("GRANDLAND X 2")>-1){
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append("Il modello "+modello+"  deve essere  GRANDLAND X").append("<br>");
            }else if(modello.indexOf("INSIGNIA 2 SERIE")>-1){
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append("Il modello "+modello+"  deve essere  INSIGNIA").append("<br>");
            }else if(modello.indexOf("ZAFIRA 3 SERIE")>-1){
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append("Il modello "+modello+"  deve essere  ZAFIRA").append("<br>");
            }else if(modello.indexOf("MOVANO 4 SERIE")>-1){
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append("Il modello "+modello+"  deve essere  MOVANO").append("<br>");
            }*/
            return true;
        }
        else {
            txtmodello="compilare il modello";
            return false;
        }
    }
    function verificaVersione() {
        var valore= $.trim($("#versione").val());
        if (valore!=''){ return true; }
        else {
            txtversione="compilare l\'allestimento";
            return false;
        }
    }
    function verificaValore() {
        var valore= $("#valoreAssicurato").val();
        if (valore!=''){ return true; }
        else {
            txtvalore="compilare il valore assicurato";
            return false;
        }
    }
    function verificaDataImmatricolazione(){
        var data1 = $("#dataImmatricolazione").val();
        //var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        if (data1 !=''){ return true;  }
        else{
            txtdataImm="compilare la data Immatricolazione";
            return false;
        }
    }
    function verificaDataDecorrenza(){
        var data1 = $("#dataDecorrenza").val();
        //var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        if (data1 !=''){ return true;  }
        else{
            txtdatadeco="compilare la data decorrenza";
            return false;
        }
    }
    function verificaDataAnnullamento(){
        var data1 = $("#dataAnnullamento").val();
        //var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        if (data1 !=''){ return true;  }
        else{
            txtdataannulla="compilare la data annullamento";
            return false;
        }
    }
    function verificaTarga() {
        var valore= $("#targa").val();
        var regtarga=/^[a-zA-Z]{2}[0-9]{3,4}[a-zA-Z]{2}$/;
        if(valore!='' ){ return true; }
        else  {
            txttarga="compilare il campo targa";
            return false;
        }
    }
    function verificacilindrata() {
        var valore= $.trim($("#cilindrata").val());
        if (valore!=''){ return true; }
        else {
            txtcilindrata="compilare la cilindrata";
            return false;
        }
    }
    function verificavalli() {
        var valore= $.trim($("#cavalli").val());
        if (valore!=''){ return true; }
        else {
            txtcavalli="compilare  i cavalli";
            return false;
        }
    }
    function verificaquintali() {
        var valore= $.trim($("#quintali").val());
        if (valore!=''){ return true; }
        else {
            txtquintali="compilare  i quintali";
            return false;
        }
    }
    function verificakw() {
        var valore= $.trim($("#kw").val());
        if (valore!=''){ return true; }
        else {
            txtkw="compilare  i kw";
            return false;
        }
    }
    function verificaprovImma() {
        var valore= $.trim($("#provImmatricolazione").val());
        if (valore!=''){
            return true;
        }
        else {
            txtprovimma="compilare la provincia immatricolazione";
            return false;
        }
    }
    function verificaPremioL() {
        var valore= $("#premioLordo").val();
        if (valore!=''){ return true; }
        else {
            txtpremioL="compilare il premio lordo";
            return false;
        }
    }
    function verificaPremioN() {
        var valore= $("#premioImponibile").val();
        if (valore!=''){ return true; }
        else {
            txtPremioN="compilare il premio netto";
            return false;
        }
    }
    function verificaBonusMalus() {
        var valore= $("#bonus").val();
        if (valore!=''){ return true; }
        else {
            txtbonusmalus="selezionare un valore per il bonus malus";
            return false;
        }
    }
    function monthDiff(d2) {
        var d1=new Date();
        var day1= d1,day2= d2;
        if(day1<day2){
            d1= day2;
            d2= day1;
        }
        var months= (d1.getFullYear()-d2.getFullYear())*12+(d1.getMonth()-d2.getMonth());
        if(d1.getDate()<d2.getDate()) --months;
        return months <= 0 ? 0 : months;
    }
    function verificatargaprima() {
        var valore= $("#targa1a").val();
        var regtarga=/^[a-zA-Z]{2}[0-9]{3,4}[a-zA-Z]{2}$/;
        if(valore!='' ){ return true; }
        else  {
            txttarga_1a_imatr="compilare il campo targa prima immatr.";
            return false;
        }
    }
    function verificaStato() {
        var valore= $.trim($("#stato").val());
        if (valore!=''){ return true; }
        else {
            txtstato="compilare lo stato";
            return false;
        }
    }
    function verificaCausaleContatto() {
        var valore= $.trim($("#causaleContatto").val());
        if (valore!=''){ return true; }
        else {
            txtcausale="selezionare il causale contatto";
            return false;
        }
    }
    function verificaContattare() {
        var valore= $.trim($("#contattare").val());
        if (valore!=''){ return true; }
        else {
            txtcontattare="selezionare chi contattare";
            return false;
        }
    }
    function verificaMotivoAnnulla() {
        var valore= $.trim($("#motivoAnnulla").val());
        if (valore!=''){ return true; }
        else {
            txtmotivoannulla="selezionare il motivo dell'anullamento";
            return false;
        }
    }
    function verificaPrimaImmaBonusTargaOld() {
        //alert($("#primaImmatr").is(":checked"));
        //alert($("#primaImmatr").val());
        var prima=$("#primaImmatr").val();
        var bonus=$("#bonus").val();
        var targaOld=$("#targa1a").val();
        if((!$("#primaImmatr").is(":checked")) && bonus==null && targaOld.trim()==''){
            txtprimaImma="selezionare al meno uno dei campi nella situazione assicurativa";
            return false;
        }
        else{
            if($("#primaImmatr").is(":checked")){
                return true;
            }else if (bonus!=''){
                return true;
            }else if(targaOld!=null){
                return true;
            }
        }
    }

    $("#tipoPolizza").on("change", function(event) {
        var tipopoli=$("#tipoPolizza").val();
        if($("#tipoPolizza").val()!='CASH'){
            //$("#valoreAssicurato").prop('disabled', true);
            //$("#premioImponibile").prop('disabled', true);
            //$("#premioLordo").prop('disabled', true);
            //$(".bottoneWS").hide();
            //$(".chiamaWS").hide();
            $("#isCash").val(false);
            $("#noPolizza").prop('disabled', false);
        }else{
            $("#noPolizza").prop('disabled', true);
            //$("#valoreAssicurato").prop('readonly', false);
            //$(".bottoneWS").show();
            //$(".chiamaWS").show();
            $("#isCash").val(true);

        }
    });
    $("#tipoVeicolo").on("change", function(event) {
        var tipoveicolo=$('#tipoVeicolo').val();
        if(tipoveicolo=='AUTOCARRO'){
            $("#cavalli").val('');
            $("#cavalli").prop('disabled', true);
            $("#quintali").prop('disabled', false);
        }else{
            $("#quintali").val('');
            $("#quintali").prop('disabled', true);
            $("#cavalli").prop('disabled', false);
            var valcil=$('#cilindrata').val();
            var cfval
            if(valcil !='' && valcil>0  ) {
                if(valcil>=19.8 && valcil<=57.1){
                    cfval=1;
                }else if(valcil>=57.2 && valcil<=106.1){
                    cfval=2;
                }else if(valcil>=106.2 && valcil<=164.8){
                    cfval=3;
                }else if(valcil>=164.9 && valcil<=231.8){
                    cfval=4;
                }else if(valcil>=231.9 && valcil<=306.4){
                    cfval=5;
                }else if(valcil>=306.5 && valcil<=387.8){
                    cfval=6;
                }else if(valcil>=387.9 && valcil<=475.6){
                    cfval=7;
                }else if(valcil>=475.7 && valcil<=569.5){
                    cfval=8;
                }else if(valcil>=569.6 && valcil<=669.0){
                    cfval=9;
                }else if(valcil>=669.1 && valcil<=774.0){
                    cfval=10;
                }else if(valcil>=774.1 && valcil<=884.1){
                    cfval=11;
                }else if(valcil>=884.2 && valcil<=999.2){
                    cfval=12;
                }else if(valcil>=999.3 && valcil<=1119.1){
                    cfval=13;
                }else if(valcil>=1119.2 && valcil<=1243.6){
                    cfval=14;
                }else if(valcil>=1243.7 && valcil<=1372.5){
                    cfval=15;
                }else if(valcil>=1372.6 && valcil<=1505.8){
                    cfval=16;
                }else if(valcil>=1505.9 && valcil<=1643.3){
                    cfval=17;
                }else if(valcil>=1643.4 && valcil<=1784.9){
                    cfval=18;
                }else if(valcil>=1785.0 && valcil<=1930.5){
                    cfval=19;
                }else if(valcil>=1930.6 && valcil<=2080.1){
                    cfval=20;
                }else if(valcil>=2080.2 && valcil<=2233.4){
                    cfval=21;
                }else if(valcil>=2233.5 && valcil<=2390.4){
                    cfval=22;
                }else if(valcil>=2390.5 && valcil<=2551.1){
                    cfval=23;
                }else if(valcil>=2551.2 && valcil<=2715.4){
                    cfval=24;
                }else if(valcil>=2715.5 && valcil<=2883.2){
                    cfval=25;
                }else if(valcil>=2883.3 && valcil<=3054.5){
                    cfval=26;
                }else if(valcil>=3054.6 && valcil<=3229.1){
                    cfval=27;
                }else if(valcil>=3229.2 && valcil<=3407.1){
                    cfval=28;
                }else if(valcil>=3407.2 && valcil<=3588.4){
                    cfval=29;
                }else if(valcil>=3588.5 && valcil<=3772.8){
                    cfval=30;
                }else if(valcil>=3772.9 && valcil<=3960.5){
                    cfval=31;
                }else if(valcil>=3960.6 && valcil<=4151.2){
                    cfval=32;
                }else if(valcil>=4151.3 && valcil<=4345.1){
                    cfval=33;
                }else if(valcil>=4345.2 && valcil<=4542.0){
                    cfval=34;
                }else if(valcil>=4542.1 && valcil<=4741.9){
                    cfval=35;
                }else if(valcil>=4742.0 && valcil<=4944.7){
                    cfval=36;
                }else if(valcil>=4944.8 && valcil<=5150.5){
                    cfval=37;
                }else if(valcil>=5150.6 && valcil<=5359.2){
                    cfval=38;
                }else if(valcil>=5359.3 && valcil<=5570.7){
                    cfval=39;
                }else if(valcil>=5570.8 && valcil<=5785.0){
                    cfval=40;
                }else if(valcil>=5785.1 && valcil<=6002.1){
                    cfval=41;
                }else if(valcil>=6002.2 && valcil<=6221.9){
                    cfval=42;
                }else if(valcil>=6222.0 && valcil<=6444.5){
                    cfval=43;
                }else if(valcil>=6444.6 && valcil<=6669.8){
                    cfval=44;
                }else if(valcil>=6669.9 && valcil<=6897.7){
                    cfval=45;
                }else if(valcil>=6897.8 && valcil<=7128.2){
                    cfval=46;
                }else if(valcil>=7128.3 && valcil<=7361.4){
                    cfval=47;
                }else if(valcil>=7361.5 && valcil<=7597.2){
                    cfval=48;
                }else if(valcil>=7597.3 && valcil<=7835.5){
                    cfval=49;
                }else if(valcil>=7835.6){
                    cfval=50;  }
                $("#cavalli").val(cfval);
            }
        }
    });
    $("#cognome").on("change", function(event) {
       /* if(verificaTipoProdotto() && verificaTipoVeicolo() && verificaCognome() && verificaNomeCliente() && verificapartitaIva() && verificaEmailCliente() && verificaIndirizzoCliente()
            && verificaLocalitaCliente() && verificaCapCliente() && verificaProvCliente() && verificaPratica() && verificaMarchio() && verificaModello() && verificaVersione() && verificaValore()
            && verificaDataImmatricolazione()&& verificaDataDecorrenza() && verificaTarga() && verificacilindrata() && verificakw() && verificaprovImma() && verificaPremioL() && verificaPremioN()){
            $("#bottonSalva").prop('disabled', false);
        }else{
            $("#bottonSalva").prop('disabled', true);
        }*/
    });
    $("#modello").on("change", function(event) {
        var modello= $.trim($("#modello").val());
        if(modello.indexOf("CORSA 5 SERIE")>-1){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
            $( ".testoAlert").append("per questo modello "+modello+" bisogna inserire solo CORSA").append("<br>");
        }else if(modello.indexOf("COMBO TOUR")>-1 || modello.indexOf("COMBO TOUR")>-1){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
            $( ".testoAlert").append("per questo modello "+modello+" bisogna inserire solo COMBO").append("<br>");
        }else if(modello.indexOf("AGILA 1 SERIE")>-1 || modello.indexOf("AGILA 2 SERIE")>-1){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
            $( ".testoAlert").append("per questo modello "+modello+" bisogna inserire solo AGILA").append("<br>");
        }else if(modello.indexOf("ASTRA 3 SERIE")>-1 || modello.indexOf("ASTRA 4 SERIE")>-1 || modello.indexOf("ASTRA 5 SERIE")>-1){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
            $( ".testoAlert").append("per questo modello "+modello+" bisogna inserire solo ASTRA").append("<br>");
        }
        else if(modello.indexOf("MERIVA 2 S.")>-1){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
            $( ".testoAlert").append("per questo modello "+modello+" bisogna inserire solo MERIVA").append("<br>");
        }else if(modello.indexOf("MOKKA")>-1){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
            $( ".testoAlert").append("per questo modello "+modello+" bisogna inserire  MOKKA X").append("<br>");
        }else if(modello.indexOf("VIVARO 3 SERIE")>-1){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
            $( ".testoAlert").append("per questo modello "+modello+"  bisogna inserire  VIVARO").append("<br>");
        }else if(modello.indexOf("GRANDLAND X 2")>-1){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
            $( ".testoAlert").append("per questo modello "+modello+"  bisogna inserire  GRANDLAND X").append("<br>");
        }else if(modello.indexOf("INSIGNIA 2 SERIE")>-1){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
            $( ".testoAlert").append("per questo modello "+modello+"  bisogna inserire  INSIGNIA").append("<br>");
        }else if(modello.indexOf("ZAFIRA 3 SERIE")>-1){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
            $( ".testoAlert").append("per questo modello "+modello+"  bisogna inserire  ZAFIRA").append("<br>");
        }else if(modello.indexOf("MOVANO 4 SERIE")>-1){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
            $( ".testoAlert").append("per questo modello "+modello+"  bisogna inserire  MOVANO").append("<br>");
        }
    });
    $("#cilindrata").on("change", function(event) {
        var valcil=this.value;
        var tipoveicolo=$('#tipoVeicolo').val();
        var cfval
        if(valcil !='' && valcil>0 && tipoveicolo!='AUTOCARRO' ) {
            if(valcil>=19.8 && valcil<=57.1){
                cfval=1;
            }else if(valcil>=57.2 && valcil<=106.1){
                cfval=2;
            }else if(valcil>=106.2 && valcil<=164.8){
                cfval=3;
            }else if(valcil>=164.9 && valcil<=231.8){
                cfval=4;
            }else if(valcil>=231.9 && valcil<=306.4){
                cfval=5;
            }else if(valcil>=306.5 && valcil<=387.8){
                cfval=6;
            }else if(valcil>=387.9 && valcil<=475.6){
                cfval=7;
            }else if(valcil>=475.7 && valcil<=569.5){
                cfval=8;
            }else if(valcil>=569.6 && valcil<=669.0){
                cfval=9;
            }else if(valcil>=669.1 && valcil<=774.0){
                cfval=10;
            }else if(valcil>=774.1 && valcil<=884.1){
                cfval=11;
            }else if(valcil>=884.2 && valcil<=999.2){
                cfval=12;
            }else if(valcil>=999.3 && valcil<=1119.1){
                cfval=13;
            }else if(valcil>=1119.2 && valcil<=1243.6){
                cfval=14;
            }else if(valcil>=1243.7 && valcil<=1372.5){
                cfval=15;
            }else if(valcil>=1372.6 && valcil<=1505.8){
                cfval=16;
            }else if(valcil>=1505.9 && valcil<=1643.3){
                cfval=17;
            }else if(valcil>=1643.4 && valcil<=1784.9){
                cfval=18;
            }else if(valcil>=1785.0 && valcil<=1930.5){
                cfval=19;
            }else if(valcil>=1930.6 && valcil<=2080.1){
                cfval=20;
            }else if(valcil>=2080.2 && valcil<=2233.4){
                cfval=21;
            }else if(valcil>=2233.5 && valcil<=2390.4){
                cfval=22;
            }else if(valcil>=2390.5 && valcil<=2551.1){
                cfval=23;
            }else if(valcil>=2551.2 && valcil<=2715.4){
                cfval=24;
            }else if(valcil>=2715.5 && valcil<=2883.2){
                cfval=25;
            }else if(valcil>=2883.3 && valcil<=3054.5){
                cfval=26;
            }else if(valcil>=3054.6 && valcil<=3229.1){
                cfval=27;
            }else if(valcil>=3229.2 && valcil<=3407.1){
                cfval=28;
            }else if(valcil>=3407.2 && valcil<=3588.4){
                cfval=29;
            }else if(valcil>=3588.5 && valcil<=3772.8){
                cfval=30;
            }else if(valcil>=3772.9 && valcil<=3960.5){
                cfval=31;
            }else if(valcil>=3960.6 && valcil<=4151.2){
                cfval=32;
            }else if(valcil>=4151.3 && valcil<=4345.1){
                cfval=33;
            }else if(valcil>=4345.2 && valcil<=4542.0){
                cfval=34;
            }else if(valcil>=4542.1 && valcil<=4741.9){
                cfval=35;
            }else if(valcil>=4742.0 && valcil<=4944.7){
                cfval=36;
            }else if(valcil>=4944.8 && valcil<=5150.5){
                cfval=37;
            }else if(valcil>=5150.6 && valcil<=5359.2){
                cfval=38;
            }else if(valcil>=5359.3 && valcil<=5570.7){
                cfval=39;
            }else if(valcil>=5570.8 && valcil<=5785.0){
                cfval=40;
            }else if(valcil>=5785.1 && valcil<=6002.1){
                cfval=41;
            }else if(valcil>=6002.2 && valcil<=6221.9){
                cfval=42;
            }else if(valcil>=6222.0 && valcil<=6444.5){
                cfval=43;
            }else if(valcil>=6444.6 && valcil<=6669.8){
                cfval=44;
            }else if(valcil>=6669.9 && valcil<=6897.7){
                cfval=45;
            }else if(valcil>=6897.8 && valcil<=7128.2){
                cfval=46;
            }else if(valcil>=7128.3 && valcil<=7361.4){
                cfval=47;
            }else if(valcil>=7361.5 && valcil<=7597.2){
                cfval=48;
            }else if(valcil>=7597.3 && valcil<=7835.5){
                cfval=49;
            }else if(valcil>=7835.6){
                cfval=50;  }
            $("#cavalli").val(cfval);
        }

    });
    $("#nome").on("change", function(event) {
        /*if(verificaTipoProdotto() && verificaTipoVeicolo() && verificaCognome() && verificaNomeCliente() && verificapartitaIva() && verificaEmailCliente() && verificaIndirizzoCliente()
            && verificaLocalitaCliente() && verificaCapCliente() && verificaProvCliente() && verificaPratica() && verificaMarchio() && verificaModello() && verificaVersione() && verificaValore()
            && verificaDataImmatricolazione()&& verificaDataDecorrenza() && verificaTarga() && verificacilindrata() && verificakw() && verificaprovImma() && verificaPremioL() && verificaPremioN()){
            $("#bottonSalva").prop('disabled', false);
        }else{
            $("#bottonSalva").prop('disabled', true);
        }*/
    });
    $("#stato").on("change", function(event) {
        if(verificaStato()){
            $("#motivoAnnulla").val("");
            $("#dataAnnullamento").datepicker("setDate", null);
            $(".annullata").hide();
            $('#daChiamare').attr('checked', false);
            $("#daChiamare").val(false);
            $(".chiamastato").hide();
            $("#causaleContatto").val("");
            $("#contattare").val("");

            /*if(verificaDataAnnullamento()){
                var $dates = $('#dataAnnullamento').datepicker();
                $dates.datepicker('setDate', null);
            }*/
        }
    });
    $("#targa").on("change", function(event) {
        $( ".alert " ).remove();
        $( ".testoAlert " ).remove();
        var primaImmatr = false;
        var valore1= $.trim($("#targa").val());
        var valore2= $.trim($("#targa1a").val());
        var regtarga=/^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$/;
        if($("#primaImmatr").is(":checked")){
            primaImmatr= true;
        }
        if(($.trim($("#targa").val()) != '' )&& verificaTarga() && !primaImmatr && verificatargaprima()  /*&& ($.trim($("#email").val()) != '' )  && verificaEmailContr() && verificaTelefono() && verificaDataImmatricolazione() && verificaTelaio()  && verificaValore() && verificaDecorrenza()*/){
            if(!regtarga.test(valore1)||!regtarga.test(valore2)){
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                if(!regtarga.test(valore1)){
                    $( ".testoAlert").append("Da notare che la targa ha il formato vecchio").append("<br>");
                }
                if(!regtarga.test(valore2)){
                    $( ".testoAlert").append("Da notare che la targa di prima immatricolazione ha il formato vecchio").append("<br>");
                }
            }
        }
        else{
            if(!verificaTarga()){
               /* $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txttarga);*/
               // $("#bottonSalva").prop('disabled', true);
            }else if(!primaImmatr){
               /* $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append("compilare targa prima immatricolazione");*/
               // $("#bottonSalva").prop('disabled', true);
            }else if(primaImmatr && verificaTarga()){
                if(!regtarga.test(valore1)||!regtarga.test(valore2)){
                    $( ".alert " ).remove();
                    $( ".testoAlert " ).remove();
                    $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    if(!regtarga.test(valore1)){
                        $( ".testoAlert").append("Da notare che la targa ha il formato vecchio").append("<br>");
                    }
                }
               // $("#bottonSalva").prop('disabled', false);
            }
            /*$("#avanti1").prop('disabled', true);*/

        }
    });
    $("#targa1a").on("change", function(event) {
        $( ".alert " ).remove();
        $( ".testoAlert " ).remove();
        var primaImmatr = false;
        var valore1= $.trim($("#targa").val());
        var valore2= $.trim($("#targa1a").val());
        var regtarga=/^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$/;
        if($("#primaImmatr").is(":checked")){  primaImmatr= true;}
        if(($.trim($("#targa1a").val()) != '' )&& verificatargaprima() && verificaTarga() /*&& !primaImmatr&& ($.trim($("#email").val()) != '' )  && verificaEmailContr() && verificaTelefono() && verificaDataImmatricolazione() && verificaTelaio()  && verificaValore() && verificaDecorrenza()*/){

            if(!regtarga.test(valore1)||!regtarga.test(valore2)){
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                if(!regtarga.test(valore1)){
                    $( ".testoAlert").append("Da notare che la targa ha il formato vecchio").append("<br>");
                }
                if(!regtarga.test(valore2)){
                    $( ".testoAlert").append("Da notare che la targa di prima immatricolazione ha il formato vecchio").append("<br>");
                }
            }
        }
        else{
            if(!verificaTarga()){
            }else if(!verificatargaprima){
            }
        }
    });
    $("#primaImmatr").on("change", function(event) {
        var valore= $.trim($("#primaImmatr").val());
        var primaImmatr = false;
        if($("#primaImmatr").is(":checked")){
            $("#primaImmatr").val(true);
            primaImmatr= true;
        }else{
            $("#primaImmatr").val(false);
            $('#primaImmatr').attr('checked', false);
        }
        if((!primaImmatr)&& verificaTarga() && verificatargaprima() /*&& ($.trim($("#email").val()) != '' )  && verificaEmailContr() && verificaTelefono() && verificaDataImmatricolazione() && verificaTelaio()  && verificaValore() && verificaDecorrenza()*/){
           /* $("#targa1a").prop('disabled', false);
            $("#bonus").attr('disabled', false);*/
        }
        else{
            if(primaImmatr && verificaTarga()){
                /*$("#targa1a").prop('disabled', true);
                $("#targa1a").val("");
                $("#bonus").attr('disabled', true);
                $("#bonus").val("");*/
            }else if(!primaImmatr && !verificaTarga() && verificatargaprima()){
               /* $("#targa1a").prop('disabled', false);
                $("#bonus").attr('disabled', false);*/

            }else if(!primaImmatr && verificaTarga() && !verificatargaprima()){
               /* $("#targa1a").prop('disabled', false);
                $("#bonus").attr('disabled', false);*/

            }else if(primaImmatr && !verificaTarga()){
              /*  $("#targa1a").prop('disabled', true);
                $("#bonus").attr('disabled', true);
                $("#bonus").val("");*/

            }
            else{
               /* $("#targa1a").prop('disabled', false);
                $("#bonus").attr('disabled', false);*/
            }
            /**/
        }
    });
    $("#daChiamare").on("change", function(event) {
        if($("#daChiamare").is(":checked")){
            $(".annullata").hide();
            $("#dataAnnullamento").datepicker("setDate", null);
            $('#daChiamare').prop('checked', true);
            $("#daChiamare").val(true);
            $("#stato").val("");
            $("#motivoAnnulla").val("");
            $(".chiamastato").show();

        }else{
            $("#daChiamare").val(false);
            $("#causaleContatto").val("");
            $("#contattare").val("");
            $(".chiamastato").hide();
        }
    });
    $("#dataAnnullamento").on("changeDate", function(event) {
        if(verificaDataAnnullamento()){
            $(".annullata").show();
                $('#daChiamare').attr('checked', false);
                $("#daChiamare").val(false);
                $(".chiamastato").hide();
                $("#causaleContatto").val("");
                $("#contattare").val("");
            $(".chiamastato").hide();
            $("#stato").val("");
        }else{
            $(".annullata").hide();
            $("#motivoAnnulla").val("");
            if($("#daChiamare").is(":checked")){
                $("#daChiamare").val(true);
                $("#stato").val("");
                $("#motivoAnnulla").val("");
                /*if(verificaDataAnnullamento()){
                    var $dates = $('#dataAnnullamento').datepicker();
                    $dates.datepicker('setDate', null);
                }*/
                $(".chiamastato").show();
                $(".annullata").hide();
            }
        }

    });
    $("#dataDecorrenza").on("changeDate", function(event) {
        var data1 = $("#dataDecorrenza").val();
        if(data1 != ''){
            var nuovo= $("#nuovo").val();
            //var dataImma = $("#dataImmatricolazione").val();
            var data2 = $("#dataImmatricolazione").val().split('-');
            var one_day=1000;
            var messi=0;
            var dataDecorrenza=$("#dataDecorrenza").val();
            if(dataDecorrenza!=''){
                var dataDeco = $("#dataDecorrenza").val().split('-');
                var dateformat1= new Date(parseInt(dataDeco[2], 10), parseInt(dataDeco[1], 10)-1, parseInt(dataDeco[0], 10));
                var dateImma= new Date(parseInt(data2[2], 10), parseInt(data2[1], 10)-1, parseInt(data2[0], 10));
                var durata=parseInt($("#durata").val());
                var scadenza=new Date(new Date(dateformat1).setMonth(dateformat1.getMonth()+durata));
                var diffdates=scadenza.getTime()-dateImma.getTime();
                diffdates=Math.floor(diffdates/one_day);
                var weekpass=Math.floor(diffdates/(60 * 60 * 24 * 7 * 4));
                messi=Math.abs(Math.round(weekpass));
            }
                if(messi>=96){
                    $( ".labelErrore" ).remove();
                    $( ".datiPoli" ).after( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                    $( ".datiPoli" ).after( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoanni8'></span></div>" );
                    $( ".testoanni8").append("la data immatricolazione non puo' essere superiore a 8 anni").append("<br>");
                }else{
                    $("#dataScadenza").val(moment(scadenza).format("DD-MM-YYYY"));
                    $( ".testoanni8").remove();
                }
        }else{
            //$("#linkModulo").addClass("not-active");
            //removeParameters();
        }
    });
    $('#bottonSalva').click(function(e) {
        e.preventDefault();
        //alert('UNO');
        var primaImmatr = false;
        var bottone="salva";
        var dealerid=$('#id-dealer2').val();
        var data1 = $("#dataAnnullamento").val().split('-');

        if($("#daChiamare").is(":checked")){
            if(verificaCausaleContatto() && verificaContattare()){
                $("#polizzaRCAForm").submit();
            }else{
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append("Elenco campi mancanti:").append("<br>");
                $(".testoAlert").append(txtcausale).append("<br>");
                $(".testoAlert").append(txtcontattare).append("<br>");
            }
        }else  if(data1 != ''){
            if(verificaMotivoAnnulla()){
                $("#polizzaRCAForm").submit();
            }else{
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append("Elenco campi mancanti:").append("<br>");
                $(".testoAlert").append(txtmotivoannulla).append("<br>");
            }
        }
        else{
            if($("#primaImmatr").is(":checked")){  primaImmatr= true;}
            if( verificaTipoProdotto() && verificaTipoVeicolo() && verificaCognome() && verificaNomeCliente && verificapartitaIva()
                && verificaIndirizzoCliente() && verificaLocalitaCliente()  && verificaCapCliente()
                && verificaMarchio() && verificaModello() && verificaVersione() && verificaValore() && verificaDataImmatricolazione()&& verificaDataDecorrenza()
                && verificacilindrata() && verificakw() && verificaprovImma() && verificaPremioL() && verificaPremioN()  && verificaEmailDealer() &&verificaTarga()
                && verificaPrimaImmaBonusTargaOld && verificaVenditore()){

                if(verificaTipoVeicolo()){
                    var tipoveicolo=$('#tipoVeicolo').val();
                    if(tipoveicolo=='AUTOCARRO'){
                        if(!verificaquintali()){
                            $( ".alert " ).remove();
                            $( ".testoAlert " ).remove();
                            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                            $( ".testoAlert").append("Elenco campi mancanti:").append("<br>");
                            $(".testoAlert").append(txtquintali).append("<br>");
                        }
                        else $("#polizzaRCAForm").submit();
                    }else{
                        if(!verificavalli()){
                            $( ".alert " ).remove();
                            $( ".testoAlert " ).remove();
                            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                            $( ".testoAlert").append("Elenco campi mancanti:").append("<br>");
                            $(".testoAlert").append(txtcavalli).append("<br>");
                        }
                         else $("#polizzaRCAForm").submit();
                    }
                }

            }else {
                $( ".alert " ).remove();
                $( ".testoAlert " ).remove();
                $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append("Elenco campi mancanti:").append("<br>");
                if(!verificaTipoProdotto()) $(".testoAlert").append(txttipoprodotto).append("<br>");
                if(!verificaTipoVeicolo()) $(".testoAlert").append(txttipoveicolo).append("<br>");
                if(verificaTipoVeicolo()){
                    var tipoveicolo=$('#tipoVeicolo').val();
                    if(tipoveicolo=='AUTOCARRO'){
                        if(!verificaquintali())$(".testoAlert").append(txtquintali).append("<br>");
                    }else{
                        if(!verificavalli())$(".testoAlert").append(txtcavalli).append("<br>");
                    }
                }
                if(!verificaCognome()) $(".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $(".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificapartitaIva()) $(".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                if(!verificaVenditore()) $(".testoAlert").append(txtvenditore).append("<br>");
                if(!verificaIndirizzoCliente()) $(".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $(".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaCapCliente()) $(".testoAlert").append(txtCapCliente).append("<br>");
                if(!verificaPrimaImmaBonusTargaOld()) $( ".testoAlert").append(txtprimaImma).append("<br>");
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarca).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtmodello).append("<br>");
                if(!verificaVersione()) $( ".testoAlert").append(txtversione).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvalore).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImm).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdatadeco).append("<br>");
                if(!verificaTarga()) $( ".testoAlert").append(txttarga).append("<br>");
                //if(verificaTarga() && !primaImmatr && !verificatargaprima()){$( ".testoAlert").append(txttarga_1a_imatr).append("<br>");}
                //if(verificatargaprima()&&!verificaBonusMalus()) $( ".testoAlert").append(txtbonusmalus).append("<br>");
                if(!verificacilindrata()) $( ".testoAlert").append(txtcilindrata).append("<br>");
                if(!verificakw()) $( ".testoAlert").append(txtkw).append("<br>");
                if(!verificaprovImma()) $( ".testoAlert").append(txtprovimma).append("<br>");
                if(!verificaPremioL()) $( ".testoAlert").append(txtpremioL).append("<br>");
                if(!verificaPremioN()) $( ".testoAlert").append(txtPremioN).append("<br>");
                if(!verificaEmailDealer()) $( ".testoAlert").append(txtemaildealer).append("<br>");
            }
        }


    });
    $('#chiamataWS').click(function(e) {
        e.preventDefault();
        var bottone="chiamata";
            chiamataWS(bottone);
        });
    function chiamataWS(bottone){
        var provincia=$("#provincia").val();
        var localita=$("#localita").val();
        var dataDecorrenza=$("#dataDecorrenza").val();
        var valoreAssicurato=$("#valoreAssicurato").val();
        var modellor="";
        var modello= $.trim($("#modello").val());
        if(modello.indexOf("ANTARA")>-1){
            modellor="ANTARA";
        }else if(modello.indexOf("ADAM")>-1){
            modellor="ADAM";
        }else if(modello.indexOf("ASTRA")>-1){
            modellor="ASTRA 5 SERIE";
        }else if(modello.indexOf("CASCADA")>-1){
            modellor="CASCADA";
        }else if(modello.indexOf("COMBO")>-1){
            modellor="COMBO 4 SERIE";
        }else if(modello.indexOf("CORSA")>-1){
            modellor="CORSA 5 SERIE";
        }else if(modello.indexOf("CROSSLAND X")>-1){
            modellor="CROSSLAND X";
        }else if(modello.indexOf("INSIGNIA")>-1){
            modellor="INSIGNIA 2 SERIE";
        }else if(modello.indexOf("KARL")>-1){
            modellor="KARL";
        }else if(modello.indexOf("MERIVA")>-1){
            modellor="MERIVA 2 S.";
        }else if(modello.indexOf("MOKKA")>-1){
            modellor="MOKKA";
        }else if(modello.indexOf("ZAFIRA")>-1){
            modellor="ZAFIRA 3 SERIE";
        }else if(modello.indexOf("MOVANO")>-1){
            modellor="MOVANO 4 SERIE";
        }else if(modello.indexOf("VIVARO")>-1){
            modellor="VIVARO 3 SERIE";
        }else if(modello.indexOf("AGILA")>-1){
            modellor="AGILA 2 SERIE";
        }else if(modello.indexOf("GRANDLAND X")>-1){
            modellor="GRANDLAND X 2";
        }
        var versione=$('#versione').val();
        var dealerid=$('#id-dealer2').val();
        var polizzaTipo=$('#tipoPolizza').val();
        if(verificaDealer() && verificaVersione() && verificaValore()&& verificaProvCliente()&& verificaLocalitaCliente() && verificaDataDecorrenza() && verificaModello() && verificaTipoProdotto()){
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $.post("${createLink(controller: "polizzeRCA", action: "chiamataWS")}",{ bottone:bottone, dealerid:dealerid, versione:versione, valoreAssicurato:valoreAssicurato,provincia:provincia, localita:localita, dataDecorrenza:dataDecorrenza,modello:modellor,polizzaTipo:polizzaTipo, _csrf: "${request._csrf.token}"}, function(response) {
                    var risposta=response.risposta;
                    if(risposta==false){
                        var errore=response.errore;
                        var provvDealer=Math.round(response.provvDealer * 100) / 100
                        var provvGmfi=Math.round(response.provvgmfi * 100) / 100
                        $( ".labelErrore" ).remove();
                        //$( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                        $( ".datiPoli" ).after( "<div class=\"alert  alert-info\" role=\"alert\"><p>"+response.errore+"</p></div>" );

                    }else{
                        if(response.bottone){
                            var bottone=response.bottone;
                            if(bottone=="chiamata"){
                                $("#premioImponibile").val(response.premioImponibile);
                                $("#premioLordo").val(response.premioLordo);
                                $("#provvDealer").val(response.provvDealer);
                                $("#provvGmfi").val(response.provvGmfi);
                                $("#provvVenditore").val(response.provvVenditore);
                                $("#rappel").val(response.rappel);
                                $("#imposte").val(response.imposte);
                                $("#codiceZonaTerritoriale").val(response.zona);
                                $("#categoriaveicolo").val(response.categoria);
                            }else{
                                swal({
                                    title: "Polizza salvata!",
                                    type: "success",
                                    timer: "1800",
                                    showConfirmButton: false
                                });
                                $('#modalPolizza').modal('hide');
                                //location.reload();
                                setTimeout("location.reload(true);",5000);
                            }
                        }
                    }
                }, "json");
            }else {
            $( ".alert " ).remove();
            $( ".testoAlert " ).remove();
            $( ".datiPoli" ).before( "<div class=\"alert  alert-info\" role=\"alert\"> <span class='testoAlert'></span></div>" );
            $( ".testoAlert").append("Elenco campi mancanti per la chiamata WS:").append("<br>");
            if(!verificaDealer()) $(".testoAlert").append(txtiddealer).append("<br>");
            if(!verificaVersione()) $( ".testoAlert").append(txtversione).append("<br>");
            if(!verificaValore()) $( ".testoAlert").append(txtvalore).append("<br>");
            if(!verificaProvCliente()) $( ".testoAlert").append(txtprovcliente).append("<br>");
            if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
            if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdatadeco).append("<br>");
            if(!verificaModello()) $( ".testoAlert").append(txtmodello).append("<br>");
            if(!verificaTipoProdotto()) $( ".testoAlert").append(txttipoprodotto).append("<br>");
               /* swal({
                    title: "controllare che i seguenti campi siano compilati per poter effettuare la chiamata:dealer, variante, valore assicurativo, provincia cliente, localita cliente, data decorrenza, modello ",
                    type: "error",
                    timer: "2000",
                    showConfirmButton: false
                });*/
            }


        }
    function removeDiacritics (str) {

        var defaultDiacriticsRemovalMap = [
            {'base':'A', 'letters':/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
            {'base':'AA','letters':/[\uA732]/g},
            {'base':'AE','letters':/[\u00C6\u01FC\u01E2]/g},
            {'base':'AO','letters':/[\uA734]/g},
            {'base':'AU','letters':/[\uA736]/g},
            {'base':'AV','letters':/[\uA738\uA73A]/g},
            {'base':'AY','letters':/[\uA73C]/g},
            {'base':'B', 'letters':/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g},
            {'base':'C', 'letters':/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g},
            {'base':'D', 'letters':/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g},
            {'base':'DZ','letters':/[\u01F1\u01C4]/g},
            {'base':'Dz','letters':/[\u01F2\u01C5]/g},
            {'base':'E', 'letters':/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},
            {'base':'F', 'letters':/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g},
            {'base':'G', 'letters':/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g},
            {'base':'H', 'letters':/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g},
            {'base':'I', 'letters':/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},
            {'base':'J', 'letters':/[\u004A\u24BF\uFF2A\u0134\u0248]/g},
            {'base':'K', 'letters':/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g},
            {'base':'L', 'letters':/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g},
            {'base':'LJ','letters':/[\u01C7]/g},
            {'base':'Lj','letters':/[\u01C8]/g},
            {'base':'M', 'letters':/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g},
            {'base':'N', 'letters':/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g},
            {'base':'NJ','letters':/[\u01CA]/g},
            {'base':'Nj','letters':/[\u01CB]/g},
            {'base':'O', 'letters':/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
            {'base':'OI','letters':/[\u01A2]/g},
            {'base':'OO','letters':/[\uA74E]/g},
            {'base':'OU','letters':/[\u0222]/g},
            {'base':'P', 'letters':/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g},
            {'base':'Q', 'letters':/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g},
            {'base':'R', 'letters':/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g},
            {'base':'S', 'letters':/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g},
            {'base':'T', 'letters':/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g},
            {'base':'TZ','letters':/[\uA728]/g},
            {'base':'U', 'letters':/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},
            {'base':'V', 'letters':/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g},
            {'base':'VY','letters':/[\uA760]/g},
            {'base':'W', 'letters':/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g},
            {'base':'X', 'letters':/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g},
            {'base':'Y', 'letters':/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g},
            {'base':'Z', 'letters':/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g},
            {'base':'a', 'letters':/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
            {'base':'aa','letters':/[\uA733]/g},
            {'base':'ae','letters':/[\u00E6\u01FD\u01E3]/g},
            {'base':'ao','letters':/[\uA735]/g},
            {'base':'au','letters':/[\uA737]/g},
            {'base':'av','letters':/[\uA739\uA73B]/g},
            {'base':'ay','letters':/[\uA73D]/g},
            {'base':'b', 'letters':/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},
            {'base':'c', 'letters':/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},
            {'base':'d', 'letters':/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
            {'base':'dz','letters':/[\u01F3\u01C6]/g},
            {'base':'e', 'letters':/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},
            {'base':'f', 'letters':/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},
            {'base':'g', 'letters':/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},
            {'base':'h', 'letters':/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
            {'base':'hv','letters':/[\u0195]/g},
            {'base':'i', 'letters':/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
            {'base':'j', 'letters':/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},
            {'base':'k', 'letters':/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},
            {'base':'l', 'letters':/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},
            {'base':'lj','letters':/[\u01C9]/g},
            {'base':'m', 'letters':/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},
            {'base':'n', 'letters':/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},
            {'base':'nj','letters':/[\u01CC]/g},
            {'base':'o', 'letters':/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
            {'base':'oi','letters':/[\u01A3]/g},
            {'base':'ou','letters':/[\u0223]/g},
            {'base':'oo','letters':/[\uA74F]/g},
            {'base':'p','letters':/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},
            {'base':'q','letters':/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},
            {'base':'r','letters':/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},
            {'base':'s','letters':/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
            {'base':'t','letters':/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},
            {'base':'tz','letters':/[\uA729]/g},
            {'base':'u','letters':/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},
            {'base':'v','letters':/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},
            {'base':'vy','letters':/[\uA761]/g},
            {'base':'w','letters':/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
            {'base':'x','letters':/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},
            {'base':'y','letters':/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},
            {'base':'z','letters':/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}
        ];

        for(var i=0; i<defaultDiacriticsRemovalMap.length; i++) {
            str = str.replace(defaultDiacriticsRemovalMap[i].letters, defaultDiacriticsRemovalMap[i].base);
        }

        return str;

    }


</script>
</body>
</html>