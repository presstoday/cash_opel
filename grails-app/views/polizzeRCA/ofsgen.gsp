<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="main_RCA"/>
    <title>OFSGEN</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
%{--<div class="col-md-12 col-md-push-11"><asset:image src="/loghi/mansutti.png" height="30px" style="margin-bottom: 25px;" /></div>--}%
<div class="row">
    <div class="col-lg-12 col-xs-10"  style="margin-top: 15px;">
        <g:if test="${flash.error}"><div class="labelErrore marginesotto">${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>

        <f:form  id="OFSGEN" method="post" action="${createLink(action: "generaofsgen", params:[_csrf: request._csrf.token])}">
            <div class="col-xs-6 col-md-12 marginiBottLista">
                <label class="testoParagraph control-label">Selezionare il mese e  l'anno per generare il report OFSGEN</label>
            </div>
            <div class="col-xs-6 col-md-2 bordertbGMFEN borderlGMFEN">
                <label class="testoParagraph control-label">Mese</label>
                <select class="form-controlI testoParagraph" style="margin-bottom:10px;" name="mese" id="mese">
                    <option <g:if test="${new Date().getMonth()==0}"> selected </g:if>  value="0" selected >Gennaio</option>
                    <option <g:if test="${new Date().getMonth()==1}"> selected </g:if> value="1">Febbraio</option>
                    <option <g:if test="${new Date().getMonth()==2}"> selected </g:if> value="2">Marzo</option>
                    <option <g:if test="${new Date().getMonth()==3}"> selected </g:if> value="3">Aprile</option>
                    <option <g:if test="${new Date().getMonth()==4}"> selected </g:if> value="4">Maggio</option>
                    <option <g:if test="${new Date().getMonth()==5}"> selected </g:if> value="5">Giugno</option>
                    <option <g:if test="${new Date().getMonth()==6}"> selected </g:if> value="6">Luglio</option>
                    <option <g:if test="${new Date().getMonth()==7}"> selected </g:if> value="7">Agosto</option>
                    <option <g:if test="${new Date().getMonth()==8}"> selected </g:if> value="8">Settembre</option>
                    <option <g:if test="${new Date().getMonth()==9}"> selected </g:if> value="9">Ottobre</option>
                    <option <g:if test="${new Date().getMonth()==10}"> selected </g:if> value="10">Novembre</option>
                    <option <g:if test="${new Date().getMonth()==11}"> selected </g:if> value="11">Dicembre</option>
                </select>
            </div>
            <div class="col-xs-6 col-md-2 bordertbGMFEN">
                <label class="testoParagraph control-label">Anno</label>
                <select class="form-controlI testoParagraph" style="margin-bottom:10px;" name="anno" id="anno"></select>
            </div>
            <div class="col-md-3 col-xs-3 bordertbGMFEN borderRGMFEN">
                <label class="testoParagraph control-label"></label>
                <button type="submit"  class="btn btn-ricerca pull-right bottoneGMFEN">Estrarre OFSGEN</button>
            </div>
        </f:form>

    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {

        window.setTimeout(function() {
            $(".alert").fadeTo(1500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 5000);

        var cur_year=new Date().getFullYear();
        var obj=document.getElementById("anno");
        for (var i = 2016; i <= 2030; i++)     {
            opt = document.createElement("option");
            opt.value = i;
            opt.text=i;
            obj.appendChild(opt);
        }
        document.getElementById("anno").value = cur_year;


    });


</script>
</body>
</html>