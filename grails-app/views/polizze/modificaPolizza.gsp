<%@ page import="cashopel.polizze.TipoPolizza" contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="navigazione"/>
    <title>Modifica polizza</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
    <div class="col-md-6" style="margin-bottom: 10px;"><link:list page="1" controller="polizze" attrs="[class: 'btn btn-annulla pull-left']">Torna elenco polizze</link:list></div>

<div class="row">
    <div class="col-md-12 col-xs-10">
        <g:if test="${flash.error}"><div class="labelErrore marginesotto " > ${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
        <g:if test="${flash.message =="RICHIESTA_INVIATA"}">
            <script type="text/javascript">swal({
                title: "RICHIESTA INVIATA!",
                type: "success",
                timer: "1800",
                showConfirmButton: false
                });
            </script>
        </g:if>
        <div class="inner  marginesotto">  </div>
        <f:form  id="polizzaForm"  method="post" class="form-horizontal" enctype="multipart/form-data">
            <div class="">
                <div class="datiPoli">
                </div>
                <div class="datiDealer">
                    <input type="hidden" name="percVenditore" id="percVenditore" value="${polizza.dealer.percVenditore}" >
                    <input type="hidden" name="premioImponibile" id="premioImponibile" value="${polizza.premioImponibile}" >
                    <input type="hidden" name="codiceZonaTerritoriale" id="codiceZonaTerritoriale" value="${polizza.codiceZonaTerritoriale}" >
                    <input type="hidden" name="rappel" id="rappel" value="${polizza.rappel}" >
                    <input type="hidden" name="imposte" id="imposte" value="${polizza.imposte}" >
                </div>
                <div class="bs-example">
                    <div id="tabs">
                        <ul class="nav nav-tabs">
                            <li class="active"><a  href="#sectionA" class="legendaTabs" data-toggle="tab"><span class="fa fa-users"></span> DATI DEL CONCESSIONARIO</a></li>
                            <li><a  href="#sectionB" class="legendaTabs" data-toggle="tab"><span class="fa fa-user"></span> DATI DELL'ASSICURATO</a></li>
                            <li><a  href="#sectionC" class="legendaTabs" data-toggle="tab"><span class="fa fa-file-text-o"></span><g:if test="${polizza.tipoPolizza==cashopel.polizze.TipoPolizza.CASH}"> DATI WEB SERVICE</g:if><g:else> PREMI E PROVVIGGIONI</g:else>  </a></li>
                        </ul>
                    </div>
                    <div class="tab-content titoli">
                        <div id="sectionA" class="tab-pane fade in active">
                            <div class="col-xs-6 col-md-6" >
                                <label class="testoParagraph control-label">RAGIONE SOCIALE</label>
                                    <input id="ragioneSocialeD" type="text" align="center" name="ragioneSocialeD"  class="col-md-6 form-controlI"  value="${polizza.dealer.ragioneSocialeD}"  disabled>
                                    <input type="hidden" name="dealer.id" id="id-dealer" value="${polizza.dealer.id}" >
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <label class="testoParagraph control-label">COD. DEALER</label>
                                    <input id="codice" type="text" align="center" name="codice"  class="col-md-6 form-controlI"  value="${polizza.dealer.codice}" disabled>
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <label class="testoParagraph control-label">DEALER TYPE</label>
                                <input  type="text" name="dealerType" class="form-controlI testoParagraph"  id="dealerType" readonly="true" value="${polizza.dealer.dealerType}" disabled/>
                            </div>
                            <legend class="legendCO"></legend>
                            <div class="col-xs-6 col-md-4 ">
                                <label class="testoParagraph control-label">COD. FISCALE / P.IVA</label>
                                <input  type="text" name="pivaD" class="form-controlI testoParagraph maiuscola" id="pivaD"  value="${polizza.dealer.pivaD}" disabled />
                            </div>

                            <div class="col-xs-6 col-md-6">
                                <label class="testoParagraph control-label">INDIRIZZO</label>
                                <input  type="text" name="indirizzoD" class="form-controlI testoParagraph maiuscola" id="indirizzoD"  value="${polizza.dealer.indirizzoD}" disabled />
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <label class="testoParagraph control-label">CAP</label>
                                <input  type="text" name="capD" class="form-controlI testoParagraph maiuscola" id="capD"  value="${polizza.dealer.capD}" disabled />
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <label class="testoParagraph control-label">PROV.</label>
                                <input  type="text" name="provinciaD" class="form-controlI testoParagraph maiuscola" id="provinciaD"  value="${polizza.dealer.provinciaD}" disabled />
                            </div>
                            <div class="col-xs-6 col-md-6">
                                <label class="testoParagraph control-label">COMUNE</label>
                                <input  type="text" name="localitaD" class="form-controlI testoParagraph maiuscola" id="localitaD"  value="${polizza.dealer.localitaD}" disabled/>
                            </div>
                            <div class="col-xs-6 col-md-6">
                                <label class="testoParagraph control-label">E-MAIL</label>
                                <input  type="text" name="emailD" class="form-controlI testoParagraph maiuscola" id="emailD"  value="${polizza.dealer.emailD}" />
                            </div>
                            <div class="col-xs-6 col-md-4">
                                <label class="testoParagraph control-label">TELEFONO</label>
                                <input  type="text" name="telefonoD" class="form-controlI testoParagraph maiuscola" id="telefonoD"  value="${polizza.dealer.telefonoD}" />
                            </div>
                            <div class="col-xs-6 col-md-3 marginesotto">
                                <label class="testoParagraph control-label">FAX</label>
                                <input  type="text" name="faxD" class="form-controlI testoParagraph maiuscola" id="faxD"  value="${polizza.dealer.faxD}" />
                            </div>
                            <div class="row"></div>
                            <legend class="legenda"><span class="fa fa-user marginesinistro marginesopra" ></span> DATI DEL VENDITORE</legend>
                            <div class="col-xs-6 col-md-6 marginesotto">
                                <label class="testoParagraph control-label">NOME E COGNOME</label>
                                <input class="form-controlI maiuscola testoParagraph" type="text" name="venditore"  id="venditore" value="${polizza.venditore}" />
                            </div>
                            <div class="col-xs-6 col-md-7 marginesotto col-md-push-5" style="margin-top:20px;">
                                <button href="#sectionB" data-toggle="tab" id="avanti1" name="avanti1" class="btn btn-avanti pull-right" onclick="attivaTab('sectionB')">Avanti&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></button>
                            </div>
                            <div class="row"></div>
                        </div>
                        <div id="sectionB" class="tab-pane fade">
                            <div class="col-xs-6 col-md-4 ">
                                <label class="testoParagraph control-label ">COGNOME/ RAGIONE SOCIALE</label>
                                <input class="form-controlI testoParagraph maiuscola" type="text" name="cognome" id="cognome" value="${polizza.cognome}" />
                            </div>
                            <div class="col-xs-6 col-md-4 ">
                                <label class="testoParagraph control-label ">NOME</label>
                                <input class="form-controlI testoParagraph maiuscola" type="text" name="nome" id="nome" value="${polizza.nome}" />
                            </div>
                            <div class="col-xs-6 col-md-4" >
                                <label class="testoParagraph control-label">C.FISCALE / P.IVA</label>
                                <input class="form-controlI testoParagraph maiuscola" type="text" name="partitaIva"  id="partitaIva" value="${polizza.partitaIva}" />
                            </div>
                            <div class="col-xs-6 col-md-5">
                                <label class="testoParagraph control-label">INDIRIZZO</label>
                                <input  type="text" name="indirizzo" class="form-controlI testoParagraph maiuscola"  id="indirizzo"  value="${polizza.indirizzo}" />
                            </div>
                            <div class="col-xs-6 col-md-5">
                                <label class="testoParagraph control-label">LOCALITA'</label>
                                <input type="text" name="localita" class="form-controlI testoParagraph maiuscola" id="localita"  value="${polizza.localita}" />
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <label class="testoParagraph control-label">CAP</label>
                                <input type="text" name="cap" class="form-controlI testoParagraph" id="cap"  value="${polizza.cap}" />
                            </div>
                            <div class="col-xs-3 col-md-1 marginesotto" >
                                <label class="testoParagraph control-label">SESSO</label>
                                <select class="form-controlI testoParagraph" name="tipoCliente" id="tipoCliente">
                                    <option <g:if test="${polizza.tipoCliente.toString()=="donna"}"> selected </g:if>  value="F">F</option>
                                    <option <g:if test="${polizza.tipoCliente.toString()=="uomo"}">  selected </g:if> value="M" >M</option>
                                </select>
                            </div>
                            <div class="col-xs-3 col-md-1 marginesotto">
                                <label class="testoParagraph control-label">PROVINCIA</label>
                                <select class="form-controlI testoParagraph" name="provincia" id="provincia" >
                                     <g:each var="provincia" in="${province}">
                                         <option <g:if test="${polizza.provincia==provincia.value}"> value="${provincia.value}" selected </g:if> >${provincia.value}</option>
                                     </g:each>
                                </select>
                            </div>
                            <div class="col-xs-6 col-md-4 marginesotto">
                                <label class="testoParagraph control-label">TELEFONO</label>
                                <input class="form-controlI testoParagraph" type="text" name="telefono"  id="telefono" value="${polizza.telefono}"/>
                            </div>

                            <div class="col-xs-6 col-md-4 marginesotto">
                                <label class="testoParagraph control-label">EMAIL</label>
                                <input class="form-controlI testoParagraph" type="text" name="email"  id="email" value="${polizza.email}" />
                            </div>
                            <legend class="legenda"><span class="fa fa-calculator marginesinistro"></span> TARIFFA</legend>
                            <div class="col-xs-3 col-md-2 marginesotto" >
                                <label class="testoParagraph control-label">TARIFFA</label>
                                <select class="form-controlI testoParagraph" name="tariffa" id="tariffa">
                                    <option <g:if test="${polizza.tariffa==1}">  </g:if>  value="1">Flex</option>
                                    <option <g:if test="${polizza.tariffa==2}"> selected </g:if>  value="2">Flex 2.0</option>
                                </select>
                            </div>
                            <legend class="legenda"><span class="fa fa-car marginesinistro marginesopra"></span> DATI DELL'AUTOVEICOLO</legend>
                            <div class="col-xs-6 col-md-2">
                                <label class="testoParagraph control-label">MARCA</label>
                                <input class="form-controlI maiuscola testoParagraph" type="text" name="marca"  id="marca" value="${polizza.marca}" />
                            </div>
                            <div class="col-xs-6 col-md-4">
                                <label class="testoParagraph control-label">MODELLO</label>
                                <input class="form-controlI maiuscola testoParagraph" type="text" name="modello"  id="modello" value="${polizza.modello}" />
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <label class="testoParagraph targaLabel control-label">TARGA</label>
                                <input class="form-controlI maiuscola testoParagraph" type="text" name="targa"  id="targa" value="${polizza.targa}" />
                            </div>
                            <div class="col-xs-6 col-md-2" >
                                <label class="testoParagraph control-label">NUOVO/USATO</label>
                                <select class="form-controlI testoParagraph" name="nuovo" id="nuovo">
                                    <option <g:if test="${polizza.nuovo}">  selected </g:if> value="1">NUOVO</option>
                                    <option <g:if test="${!polizza.nuovo}">  selected </g:if> value="0" >USATO</option>
                                </select>
                            </div>
                            <div class="col-xs-3 col-md-2" >
                                <label class="testoParagraph control-label">ON STAR</label>
                                <select class="form-controlI testoParagraph" name="onStar" id="onStar">
                                    <option <g:if test="${polizza.onStar}"> selected </g:if>  value="1">SI</option>
                                    <option <g:if test="${!polizza.onStar}"> selected </g:if>  value="0">NO</option>
                                </select>
                            </div>
                            <div class="col-xs-6 col-md-3" style="margin-top: 20px;">
                                <label class="testoParagraph control-label">TELAIO</label>
                                <input class="form-controlI maiuscola testoParagraph" type="text" name="telaio"  id="telaio" value="${polizza.telaio}" />
                            </div>

                            <div class="col-xs-6 col-md-2">
                                <label class="testoParagraph control-label">VALORE VEICOLO COMPRESO IVA</label>
                                <input class="form-controlI maiuscola testoParagraph" type="number" min="0" step="Any" name="valoreAssicuratoconiva"  id="valoreAssicuratoconiva" value="${polizza.valoreAssicuratoconiva}" />
                            </div>
                            <div class="col-xs-6 col-md-2" style="margin-top: 20px;">
                                <label class="testoParagraph control-label">RECUPERO IVA</label>
                                <select class="form-controlI testoParagraph" name="iva" id="iva">
                                    <option <g:if test="${polizza.iva}">  selected </g:if> value="1" >SI</option>
                                    <option <g:if test="${!polizza.iva}">  selected </g:if> value="0" >NO</option>
                                </select>
                            </div>
                            <div class="col-xs-6 col-md-2" style="margin-top: 20px;">
                                <label class="testoParagraph control-label">VALORE ASSICURATO</label>
                                <input class="form-controlI maiuscola testoParagraph" type="number" min="0" step="Any" name="valoreAssicurato"  id="valoreAssicurato" readonly  value="${polizza.valoreAssicurato}" />
                            </div>
                            <div class="col-xs-6 col-md-2" style="margin-top: 20px;">
                                <label class="testoParagraph control-label">DATA IMMATR.</label>
                                <input class="form-controlI maiuscola testoParagraph" type="text" name="dataImmatricolazione"  id="dataImmatricolazione" value="${polizza.dataImmatricolazione?.format("dd-MM-yyyy")}" data-provide="datepicker" data-date-language="it"
                                       data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true" data-date-end-date="0d" data-date-today-highlight="false" data-date-toggle-active="false"
                                       data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"  />
                            </div>

                            <div class="col-xs-4 col-md-2" >
                                <label class="testoParagraph control-label">STEP</label>
                                <select class="form-controlI testoParagraph" name="step" id="step">
                                    <option  <g:if test="${polizza.step=='step 1'}">  selected </g:if> value="step 1">STEP 1</option>
                                    <option <g:if test="${polizza.step=='step 2'}"> selected </g:if>  value="step 2">STEP 2</option>
                                    <option <g:if test="${polizza.step=='step 3'}">  selected </g:if> value="step 3">STEP 3</option>
                                </select>
                            </div>
                            <div class="col-xs-6 col-md-3">
                                <label class="testoParagraph control-label">COPERTURA</label>
                                <select class="form-controlI testoParagraph" name="coperturaRichiesta" id="coperturaRichiesta">
                                    <option <g:if test="${polizza.coperturaRichiesta=='SILVER'}"> selected </g:if> value="silver" >SILVER</option>
                                    <option <g:if test="${polizza.coperturaRichiesta=='GOLD'}">  selected </g:if>  value="gold">GOLD</option>
                                    <option <g:if test="${polizza.coperturaRichiesta=='PLATINUM'}">  selected </g:if>  value="platinum" >PLATINUM</option>
                                    <option <g:if test="${polizza.coperturaRichiesta=='PLATINUM KASKO'}">  selected </g:if>  value="platinumK">PLATINUM KASKO</option>
                                    <option <g:if test="${polizza.coperturaRichiesta=='PLATINUM COLLISIONE'}">  selected </g:if>  value="platinumC">PLATINUM COLLISIONE</option>
                                </select>
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <label class="testoParagraph control-label">DURATA (MESI)</label>
                                <select class="form-controlI testoParagraph" name="durata" id="durata">
                                    <option <g:if test="${polizza.durata==12}">  selected </g:if> value="12"> 12 mesi</option>
                                    <option <g:if test="${polizza.durata==18}">  selected </g:if> value="18"> 18 mesi</option>
                                    <option <g:if test="${polizza.durata==24}">  selected </g:if> value="24" > 24 mesi</option>
                                    <option <g:if test="${polizza.durata==30}">  selected </g:if> value="30" > 30 mesi</option>
                                    <option <g:if test="${polizza.durata==36}">  selected </g:if> value="36" > 36 mesi</option>
                                    <option <g:if test="${polizza.durata==42}">  selected </g:if> value="42" > 42 mesi</option>
                                    <option <g:if test="${polizza.durata==48}">  selected </g:if> value="48" > 48 mesi</option>
                                    <option <g:if test="${polizza.durata==54}">  selected </g:if> value="54" > 54 mesi</option>
                                    <option <g:if test="${polizza.durata==60}">  selected </g:if> value="60"> 60 mesi</option>
                                    <option <g:if test="${polizza.durata==66}">  selected </g:if> value="66"> 66 mesi</option>
                                    <option <g:if test="${polizza.durata==72}">  selected </g:if> value="72"> 72 mesi</option>
                                </select>
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <label class="testoParagraph control-label">DATA EFFETTO</label>
                                <input class="form-controlI maiuscola testoParagraph" type="text" name="dataDecorrenza"  id="dataDecorrenza" value="${polizza.dataDecorrenza?.format("dd-MM-yyyy")}" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"
                                        data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                                       data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"/>
                            </div>
                            <div class="col-xs-6 col-md-2">
                                <label class="testoParagraph control-label">DATA INSERIMENTO</label>
                                <input class="form-controlI maiuscola testoParagraph" type="text" name="dataInserimento"  id="dataInserimento" value="${polizza.dataInserimento?.format("dd-MM-yyyy")}" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"
                                       data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                                       data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"/>
                            </div>
                            <div class="col-xs-6 col-md-4 marginiAvanti">
                                <a href="#sectionA" data-toggle="tab" id="indietro1" class="btn btn-avanti pull-left" onclick="attivaTab('sectionA')"><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Indietro</a>
                            </div>
                            <div class="col-xs-6 col-md-4 marginiAvanti" >
                                <a href="" %{--onclick="return chiamataWS();"--}% class="btn btn-avanti pull-right" name="chiamataWS" id="chiamataWS"  title="chiamata web service"><b>CHIAMATA WEB SERVICE</b></a>
                            </div>
                            <div class="col-xs-6 col-md-4 marginiAvanti">
                                <button href="#sectionC" data-toggle="tab" id="avanti2" name="avanti2" class="btn btn-avanti pull-right" onclick="attivaTab('sectionC')" disabled>Avanti&nbsp;&nbsp;<i class="fa fa-chevron-right"></i></button>
                            </div>
                            <div class="row"></div>
                        </div>
                        <div id="sectionC" class="tab-pane fade">
                            <div class="col-xs-6 col-md-3 col-md-push-1 marginesotto">
                                <label class="testoParagraph control-label">PREMIO ASSICURATIVO</label>
                                <input class="form-controlI maiuscola testoParagraph" type="number" min="0" step="Any"  name="premioLordo"  id="premioLordo" value="${polizza.premioLordo}" format="#,##0.0#" readonly />
                            </div>
                            <div class="col-xs-6 col-md-12 marginesotto"></div>
                            <div class="col-xs-6 col-md-3 col-md-push-1 marginesotto">
                                <label class="testoParagraph control-label">PROVV. DEALER</label>
                                <input class="form-controlI maiuscola testoParagraph" type="number" min="0" step="Any" name="provvDealer"  id="provvDealer" value="${polizza.provvDealer}" format="€ #,##0.0#" readonly />
                            </div>
                            <div class="col-xs-6 col-md-12 marginesotto"></div>
                            <div class="col-xs-6 col-md-3 col-md-push-1 marginesotto">
                                <label class="testoParagraph control-label">PROVV. GMFI</label>
                                <input class="form-controlI maiuscola testoParagraph"  min="0" step="Any" name="provvGmfi"  id="provvGmfi"  value="${polizza.provvGmfi}" type="number" maxFractionDigits="2" roundingMode="HALF_DOWN" readonly />
                            </div>
                            <div class="col-xs-6 col-md-12 marginesotto"></div>
                            <div class="col-xs-6 col-md-3 col-md-push-1 marginesotto">
                                <label class="testoParagraph control-label">PROVV. VENDITORE</label>
                                <input class="form-controlI maiuscola testoParagraph" type="number" min="0" step="Any" name="provvVenditore"  id="provvVenditore" value="${polizza.provvVenditore}" readonly />
                            </div>
                            <div class="col-xs-6 col-md-12 marginesopra marginesotto">
                                <div class="col-xs-6 col-md-6 marginesotto">
                                    <a href="#sectionB" data-toggle="tab" id="indietro2" class="btn btn-avanti pull-left" %{--onclick="attivaTab('sectionB')"--}%><i class="fa fa-chevron-left"></i>&nbsp;&nbsp;Indietro</a>
                                </div>
                                <div class="col-xs-6 col-md-6 marginesotto">
                                    <button type="submit" id="bottonSalva" value="Salva" class="btn btn-avanti pull-right">SALVA LA POLIZZA</button>
                                </div>
                            </div>
                            <div class="row"></div>
                        </div>
                    </div>
                </div>
            </div>
        </f:form>
    </div>
</div>
<script type="text/javascript">
    function attivaTab(tab){
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };

    $('#dealer .typeahead').typeahead({
                hint: true,
                highlight: true,
                minLength:4,
                classNames: {
                    menu: 'popover',
                    dataset: 'popover-content'
                }
            }, {
                name: "elencodealers",
                display: "ragioneSocialeD",
                limit: 20,
                source: new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: {
                        url: '${createLink(action: "dealerCensiti", params: [ragioneSocialeD: "nome"])}',
                        wildcard: 'nome'
                    }
                }),
                templates:{
                    header: Handlebars.compile( '<p class="testoTypeahead"><strong>Ragione sociale:</strong></p>'),
                    suggestion: Handlebars.compile('<div>{{#if ragioneSocialeD}} {{ragioneSocialeD}} {{else}} <p class="testoTypeahead"><strong><i>il dealer inserito non si trova nell\'elenco di dealer disponibili</i></strong></p>{{/if}}</div>')
                }
            }
    ).on("typeahead:select", function(event, selected) {
        if(selected.id >0){
            //if(selected.username){
            $.post("${createLink( action: 'getDealer')}",{ nodealer: selected.id,  _csrf: "${request._csrf.token}"}, function(response) {
                var risposta=response.risposta;
                if(risposta==false){
                }else{
                    $("#codice-input").val(response.codice);
                    $("#percVenditore").val(response.percVenditore);
                    $("#pivaD").val(response.pivaD);
                    $("#indirizzoD").val(response.indirizzoD);
                    $("#provinciaD").val(response.provinciaD);
                    $("#capD").val(response.capD);
                    $("#localitaD").val(response.localitaD);
                    $("#telefonoD").val(response.telefonoD);
                    $("#faxD").val(response.faxD);
                    $("#emailD").val(response.emailD);
                    $("#dealerType").val(response.dealerType);
                }
            }, "json");
            //}
        }
        $("#id-dealer").val(selected.id);
        /*        $.post("%{--${createLink(action: 'datiDealer')}",{ id: selected.id, _csrf: "${request._csrf.token}--}%"}, function(response) {
         if(response.id>0){
         $("#salva").prop('readonly', true);
         }

         });*/
    }).on("blur", function(event) {
        var input = $(this);
        if(input.val() == "") {
            input.typeahead("val", "");
            $("#id-dealer").val(null);
            $.post("${createLink(action: 'datiDealerT')}",{ id: null, _csrf: "${request._csrf.token}"}, function(response) {});
        }
    });
    $('#codice .typeahead').typeahead({
                hint: true,
                highlight: true,
                minLength:3,
                classNames: {
                    menu: 'popover',
                    dataset: 'popover-content'
                }
            }, {
                name: "elencocodici",
                display: "codice",
                limit: 20,
                source: new Bloodhound({
                    datumTokenizer: Bloodhound.tokenizers.obj.whitespace('value'),
                    queryTokenizer: Bloodhound.tokenizers.whitespace,
                    remote: {
                        url: '${createLink(action: "dealerCensitiCodici", params: [codice: "codiceD"])}',
                        wildcard: 'codiceD'
                    }
                }),
                templates:{
                    header: Handlebars.compile( '<p class="testoTypeahead"><strong>Codice:</strong></p>'),
                    suggestion: Handlebars.compile('<div>{{#if codice}} {{codice}} {{else}} <p class="testoTypeahead"><strong><i>il dealer inserito non si trova nell\'elenco di dealer disponibili</i></strong></p>{{/if}}</div>')
                }
            }
    ).on("typeahead:select", function(event, selected) {
        if(selected.id >0){
            //if(selected.username){
            $.post("${createLink( action: 'getDealerCodice')}",{ nodealer: selected.id,  _csrf: "${request._csrf.token}"}, function(response) {
                var risposta=response.risposta;
                if(risposta==false){
                }else{
                    $("#dealer-input").val(response.ragioneSocialeD);
                    $("#percVenditore").val(response.percVenditore);
                    $("#pivaD").val(response.pivaD);
                    $("#indirizzoD").val(response.indirizzoD);
                    $("#provinciaD").val(response.provinciaD);
                    $("#capD").val(response.capD);
                    $("#localitaD").val(response.localitaD);
                    $("#telefonoD").val(response.telefonoD);
                    $("#faxD").val(response.faxD);
                    $("#emailD").val(response.emailD);
                    $("#dealerType").val(response.dealerType);
                }
            }, "json");
            //}
        }
        $("#id-dealer2").val(selected.id);
        /*        $.post("%{--${createLink(action: 'datiDealer')}--}%",{ id: selected.id, _csrf: "%{--${request._csrf.token}--}%"}, function(response) {
         if(response.id>0){
         $("#salva").prop('readonly', true);
         }

         });*/
    }).on("blur", function(event) {
        var input = $(this);
        if(input.val() == "") {
            input.typeahead("val", "");
            $("#id-dealer2").val(null);
            $.post("${createLink(action: 'datiDealerTCodice')}",{ id: null, _csrf: "${request._csrf.token}"}, function(response) {});
        }
    });

    var txtpartitaIvaDealer="";
    var txtpartitaIvaCliente="";
    var txtNomeCliente="";
    var txtCognomeCliente="";
    var txtIndirizzoCliente="";
    var txtLocalitaCliente="";
    var txtCapCliente="";
    var txtprovinciaCliente="";
    var txtsessoCliente="";
    var txtemailCliente="";
    var txtemailDealer="";
    var txtcellCliente="";
    var txttargaCliente="";
    var txttelaioCliente="";
    var txtmarchioCliente="";
    var txtversioneCliente="";
    var txtdataImmCliente="";
    var txtdataDecoCliente="";
    var txtdataInsCliente="";
    var txtcvCliente="";
    var txtvaloreCliente="";
    var txtimmCliente="";
    var txtpostCliente="";
    var txtposizionePolizza="";
    var txttargaAcquisizione="";
    var txtpremioAss="";
    var txtprovvDealer="";
    var txtprovvGMFI="";
    var txtprovvVenditore="";
    var txtnomeVenditore="";
    var txtdataImmatsup8="";

    function verificaIndirizzoDealer() {
        var valore= $.trim($("#indirizzoD").val());
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaTipoDealer() {
        var valore= $.trim($("#dealerType").val());
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaPIvaDealer() {
        var valore= $.trim($("#pivaD").val());
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaPercVenditore() {
        var valore= $.trim($("#percVenditore").val());
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaComuneDealer() {
        var valore= $.trim($("#localitaD").val());
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaCapDealer() {
        var valore= $.trim($("#capD").val());
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaProvDealer() {
        var valore= $.trim($("#provinciaD").val());
        if (valore!=''){ return true; }
        else { return false; }
    }
    function verificaVenditore() {
        var valore= $.trim($("#venditore").val());
        if (valore!=''){ return true; }
        else {
            txtnomeVenditore="compilare il campo venditore";
            return false; }
    }
    function verificaEmailDealer() {
        var valore= $("#emailD").val();
        var regemailDef=/^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$/;
        if(valore!=''/* && regemailDef.test(valore)*/){
            $( ".alert" ).remove();
            return true;
        }
        else{
            if(/*!regemailDef.test(valore) &&*/ valore!='')/*{ txtemailDealer="verificare il formato del campo email del Dealer";}
            else*/ {txtemailDealer="compilare il campo email del dealer";}
            return false;
        }
    }
    function verificapartitaIvaCliente(){
        var valore= $("#partitaIva").val();
        var regpIva= /^[a-zA-Z]{6}[0-9]{2}[a-zA-Z][0-9]{2}[a-zA-Z][0-9]{3}[a-zA-Z]$/;
        var regCF=/^[0-9]{11}$/;
        if(valore!=''  && (regpIva.test(valore)|| (regCF.test(valore)))){ return true;}
        else{
            txtpartitaIvaCliente="il formato della partita iva/ codice fiscale è sbagliato";
            return false;
        }
    }
    function verificaNomeCliente(){
        var valore= $("#nome").val();
        if(valore!=''){
            var nomec=removeDiacritics(valore);
            $("#nome").val(nomec);
            return true;
        }
        else{
            txtNomeCliente="inserire il nome";
            return false;
        }
    }
    function verificaCognomeCliente(){
        var valore= $("#cognome").val();
        if(valore!=''){
            var cognomec=removeDiacritics(valore);
            $("#cognome").val(cognomec);
            return true;
        }
        else{
            txtCognomeCliente="inserire il cognome";
            return false;
        }
    }
    function verificaIndirizzoCliente(){
        var valore= $("#indirizzo").val();
        if(valore!=''){
            var indirizzoc=removeDiacritics(valore);
            $("#indirizzo").val(indirizzoc);
            return true;
        }
        else{
            txtIndirizzoCliente="inserire il indirizzo";
            return false;
        }
    }
    function verificaLocalitaCliente(){
        var valore= $("#localita").val();
        if(valore!=''){
            var localitaC=removeDiacritics(valore);
            $("#localita").val(localitaC);
            return true;
        }
        else{
            txtLocalitaCliente="inserire la localita'";
            return false;
        }
    }
    function verificaCapCliente(){
        var valore= $("#cap").val();
        var regcap=/^[0-9]{5}$/;
        if(valore!='' && regcap.test(valore)){
            return true;
        }
        else{
            txtCapCliente="inserire il cap o verificare il formato che sia di 5 numeri";
            return false;
        }
    }
    function verificaEmailCliente() {
        var valore= $("#email").val();
        var regemailDef=/^[a-zA-Z0-9_!#$%&’*+/=?`{|}~^.-]+@[a-zA-Z0-9.-]+$/;
        if(valore!='' && regemailDef.test(valore)){
            $( ".alert" ).remove();
            return true;
        }
        else{
            if(!regemailDef.test(valore) && valore!=''){ txtemailCliente="verificare il formato del campo email";}
            else {txtemailCliente="compilare il campo email";}
            return false;
        }
    }
    function verificaTelefono() {
        var valore= $.trim($("#telefono").val());
        if (valore!=''){ return true; }
        else {
            txtcellCliente="compilare il campo telefono";
            return false;
        }
    }
    function verificaTarga() {
        var valore= $.trim($("#targa").val());
        var regtarga=/^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$/;
        if(valore!='' && regtarga.test(valore)){
            return true;
        }
        else {
            if(!regtarga.test(valore) && valore!=''){txttargaCliente="il formato della targa è sbagliato";}
            else {txttargaCliente="compilare il campo targa";}
            return false;
        }
    }
    function verificaDataImmatricolazione(){
        var data1 = $("#dataImmatricolazione").val();
        //var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        if (data1 !=''){ return true;  }
        else{
            txtdataImmCliente="compilare la data Immatricolazione";
            return false;
        }
    }
    function verificaDataDecorrenza(){
        var data1 = $("#dataDecorrenza").val();
        //var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        if (data1 !=''){ return true;  }
        else{
            txtdataDecoCliente="compilare la data effetto";
            return false;
        }
    }
    function verificadataInserimento(){
        var data1 = $("#dataInserimento").val();
        //var dateformat1= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
        if (data1 !=''){ return true;  }
        else{
            txtdataInsCliente="compilare la data de Inserimento";
            return false;
        }
    }
    function verificaTelaio() {
        var valore= $.trim($("#telaio").val());
        if (valore!=''){
            return true;
        }
        else {
            txttelaioCliente="compilare il telaio";
            return false;
        }
    }
    function verificaMarchio() {
        var valore= $.trim($("#marca").val().toUpperCase());
        if (valore!=''){
            if(valore=='AUDI' || valore=='BENTLEY' || valore=='BMW' || valore=='FERRARI' || valore=='LAMBORGHINI' || valore=='LOTUS' || valore=='MERCEDES' || valore=='PORSCHE'|| valore=='ROLLS ROYCE'){
                txtmarchioCliente="QUESTA MARCA NON E' COMPRESSA NELLA TARIFFA";
                return false;
            }else{
                return true;
            }
        }
        else {
            txtmarchioCliente="compilare il marchio vettura";
            return false;
        }
    }
    function verificaModello() {
        var valore= $.trim($("#modello").val());
        if (valore!=''){ return true; }
        else {
            txtversioneCliente="compilare il modello";
            return false;
        }
    }
    function verificaValore() {
        var valore= $.trim($("#valoreAssicuratoconiva").val());

        if (valore!='' && valore >0){
            return true;
        }
        else {
            txtvaloreCliente="il valore assicurato deve essere superiore a 0";
            return false;
        }
    }
    function verificaPremioAssi() {
        var valore= $.trim($("#premioImponibile").val());
        if (valore!=''){ return true; }
        else {
            txtpremioAss="il premio premioImponibile non ha un valore";
            return false;
        }
    }
    function verificaProvvDealer() {
        var valore= $.trim($("#provvDealer").val());
        if (valore!=''){ return true; }
        else {
            txtprovvDealer="le provvigioni del dealer non contengono un valore";
            return false;
        }
    }
    function verificaProvvGMFI() {
        var valore= $.trim($("#provvGmfi").val());
        if (valore!=''){ return true; }
        else {
            txtprovvDealer="le provvigioni GMFI non contengono un valore";
            return false;
        }
    }
    function verificaProvvVenditore() {
        var valore= $.trim($("#provvVenditore").val());
        if (valore!=''){ return true; }
        else {
            txtprovvDealer="le provvigioni del venditore  non contengono un valore";
            return false;
        }
    }
    function setGetParameter(paramName, paramValue)
    {
        var url = $('#linkModulo').attr("href");
        var hash = location.hash;
        url = url.replace(hash, '');

        if (url.indexOf("?") < 0)
            url += "?" + paramName + "=" + paramValue;
        else
            url += "&" + paramName + "=" + paramValue;
        //}
        $('#linkModulo').attr("href",url);
    }
    function removeParameters()
    {
        var url = $('#linkModulo').attr("href");

        if (url.indexOf("?") < 0){     }
        else{
            var subfix=url.indexOf("?");
            url=url.substring(0,subfix);
        }
        $('#linkModulo').attr("href",url);
    }
    $("#pivaD").on("change",function(event){
        if(verificaPIvaDealer() && verificaTipoDealer() && verificaCapDealer() && verificaProvDealer()&&verificaIndirizzoDealer()&&verificaComuneDealer() && verificaEmailDealer() && verificaVenditore()){
            $("#avanti1").prop('disabled', false);
            $("#avanti1").removeClass("disabilita");
        }else{
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#dealerType").on("change",function(event){
        if(verificaPIvaDealer() && verificaTipoDealer() && verificaCapDealer() && verificaProvDealer()&&verificaIndirizzoDealer()&&verificaComuneDealer() && verificaEmailDealer() && verificaVenditore()){
            $("#avanti1").prop('disabled', false);
        }else{
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#dealer-input").on("change",function(event){
        if( verificaPIvaDealer() && verificaTipoDealer() && verificaCapDealer() && verificaProvDealer()&&verificaIndirizzoDealer()&&verificaComuneDealer() && verificaEmailDealer() && verificaVenditore()){
            $("#avanti1").prop('disabled', false);
        }else{
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#capD").on("change",function(event){
        if( verificaPIvaDealer() && verificaTipoDealer() && verificaCapDealer() && verificaProvDealer()&&verificaIndirizzoDealer()&&verificaComuneDealer() && verificaEmailDealer() && verificaVenditore()){
            $("#avanti1").prop('disabled', false);
        }else{
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#provinciaD").on("change",function(event){
        if( verificaPIvaDealer() && verificaTipoDealer() && verificaCapDealer() && verificaProvDealer()&&verificaIndirizzoDealer()&&verificaComuneDealer() && verificaEmailDealer() && verificaVenditore()){
            $("#avanti1").prop('disabled', false);
        }else{
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#localitaD").on("change",function(event){
        if( verificaPIvaDealer() && verificaTipoDealer() && verificaCapDealer() && verificaProvDealer()&&verificaIndirizzoDealer()&&verificaComuneDealer() && verificaEmailDealer() && verificaVenditore()){
            $("#avanti1").prop('disabled', false);
        }else{
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#venditore").on("change",function(event){
        if(  verificaEmailDealer() && verificaVenditore()){
            $("#avanti1").prop('disabled', false);
        }else{
            $( ".labelErrore " ).remove();
            $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
            if(!verificaVenditore()) $( ".testoAlert").append(txtnomeVenditore).append("<br>");
            if(!verificaEmailDealer()) $( ".testoAlert").append(txtemailDealer).append("<br>");
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#indirizzoD").on("change",function(event){
        if( verificaPIvaDealer() && verificaTipoDealer() && verificaCapDealer() && verificaProvDealer()&&verificaIndirizzoDealer()&&verificaComuneDealer() && verificaEmailDealer() && verificaVenditore()){
            $("#avanti1").prop('disabled', false);
            $("#avanti1").removeClass("disabilita");
        }else{
            $("#avanti1").prop('disabled', true);
        }
    });
    $("#emailD").on("change", function(event) {
        if(verificaEmailDealer() && verificaTipoDealer() && verificaCapDealer()&& verificaComuneDealer()&& verificaIndirizzoDealer()&&verificaPIvaDealer()&&verificaProvDealer() &&verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente() &&verificaIndirizzoCliente()  &&verificaLocalitaCliente()&& verificapartitaIvaCliente() /*&& verificaEmailCliente() && verificaTelefono() */&& verificaTelaio() && verificaTarga() && verificaDataImmatricolazione() && verificaMarchio()  && verificaModello() && verificaValore() && (verificaTelefono() ||verificaEmailCliente() && verificadataInserimento() && verificaDataDecorrenza()) ){
            $("#avanti1").prop('disabled', false);
        }else{
            $( ".labelErrore " ).remove();
            $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
            if(!verificaVenditore()) $( ".testoAlert").append(txtnomeVenditore).append("<br>");
            if(!verificaEmailDealer()) $( ".testoAlert").append(txtemailDealer).append("<br>");
            $("#avanti1").prop('disabled', true);
        }

    });
    $("#nome").on("change",function(event){
        if(verificaNomeCliente()&& verificaCognomeCliente() && verificaIndirizzoCliente()  && verificaLocalitaCliente()  &&  verificaCapCliente() && verificapartitaIvaCliente() /*&& verificaTelefono() && verificaEmailCliente()*/ && (verificaTarga() || verificaTelaio()) && verificaDataImmatricolazione() &&  verificaMarchio() && verificaModello() && verificaValore()  && verificadataInserimento() && verificaDataDecorrenza()){
            $("#avanti2").prop('disabled', false);
            var nome= $("#nomeInt").val();
            var cognome= $("#cognomeInt").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
        }else{
            /*if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificaEmailCliente() || !verificaTelefono() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
                if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }
                if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}
            }*/
            $("#avanti2").prop('disabled', true);
        }

    });
    $("#cognome").on("change",function(event){
        if( verificaCognomeCliente()&& verificaNomeCliente() && verificaIndirizzoCliente()  && verificaLocalitaCliente() && verificaCapCliente() &&verificapartitaIvaCliente() /*&& verificaTelefono() && verificaEmailCliente()*/ && (verificaTarga() || verificaTelaio()) && verificaDataImmatricolazione() && verificaMarchio() && verificaModello() && verificaValore()   && verificadataInserimento() && verificaDataDecorrenza() ){

            $("#avanti2").prop('disabled', false);

        }else{
           /* if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificaEmailCliente() || !verificaTelefono() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
                if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }
                if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}
            }*/
            $("#avanti2").prop('disabled', true);
        }

    });
    $("#provincia").on("change",function(event){
        if( verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente() &&verificaIndirizzoCliente()  &&verificaLocalitaCliente()&&verificapartitaIvaCliente() /*&& verificaTelefono() && verificaEmailCliente() */&& (verificaTarga() || verificaTelaio()) && verificaDataImmatricolazione() &&  verificaMarchio() && verificaModello() && verificaValore()  && verificadataInserimento() && verificaDataDecorrenza() ){
            $("#avanti2").prop('disabled', false);
            $("#chiamataWS").attr("disabled",false);

        }else{
            if(verificaLocalitaCliente()&&verificaValore()){
                $("#chiamataWS").attr("disabled",false);
            }else{
                $("#chiamataWS").attr("disabled",true);
            }
            /*if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificaEmailCliente() || !verificaTelefono() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
                if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }
                if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}
            }*/
            $("#avanti2").prop('disabled', true);

        }

    });
    $("#indirizzo").on("change",function(event){
        if( verificaIndirizzoCliente()  && verificaCognomeCliente() && verificaNomeCliente() && verificaLocalitaCliente() && verificaCapCliente() &&verificapartitaIvaCliente() /*&& verificaEmailCliente() && verificaTelefono() */ && (verificaTarga() || verificaTelaio()) && verificaDataImmatricolazione() && verificaMarchio() && verificaModello() && verificaValore()  && verificadataInserimento() && verificaDataDecorrenza() ){

            $("#avanti2").prop('disabled', false);
            var nome= $("#nome").val();
            var cognome= $("#cognome").val();
            var targa= $("#targa").val();
            var nopratica= $("#noPratica").val();
            //setGetParameter("nome",nome);
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
        }else{
          /*  if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificaEmailCliente() || !verificaTelefono() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
                if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }
                if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}
            }*/
            $("#avanti2").prop('disabled', true);
        }

    });
    $("#localita").on("change",function(event){
        if(verificaLocalitaCliente() && verificaCognomeCliente() && verificaNomeCliente() && verificaIndirizzoCliente() && verificaCapCliente()   &&verificapartitaIvaCliente() /*&& verificaEmailCliente() && verificaTelefono() */&& (verificaTarga() || verificaTelaio()) && verificaDataImmatricolazione() && verificaMarchio() && verificaModello() && verificaValore()  && verificadataInserimento() && verificaDataDecorrenza() ){
            $("#avanti2").prop('disabled', false);
            $("#chiamataWS").attr("disabled",false);
        }else{
           /* if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificaEmailCliente() || !verificaTelefono() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
                if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }
                if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}
            }*/
            $("#avanti2").prop('disabled', true);
            if(verificaDataDecorrenza() && verificaValore() && verificaLocalitaCliente() && verificapartitaIvaCliente()){
                $("#chiamataWS").attr("disabled",false);
            }
            else{
                $("#chiamataWS").attr("disabled",true);
            }
        }

    });
    $("#cap").on("change",function(event){
        if( verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente() && verificaIndirizzoCliente()  &&verificaLocalitaCliente()&&verificapartitaIvaCliente() /*&& verificaEmailCliente()  && verificaTelefono() */&& (verificaTarga() || verificaTelaio()) && verificaDataImmatricolazione() &&  verificaMarchio() && verificaModello() && verificaValore()  && verificadataInserimento() && verificaDataDecorrenza() ){
            $( ".labelErrore " ).remove();
            $("#avanti2").prop('disabled', false);
            var nome= $("#nome").val();
            var cognome= $("#cognome").val();
            var targa= $("#targa").val();
            var id= $("#idPolizza").val();
            //setGetParameter("cognome",cognome);
            //setGetParameter("targa",targa);
            //setGetParameter("nopratica",nopratica);
            //$("#linkModulo").removeClass("not-active");
        }else{
            if(verificaCapCliente()) {
                $( ".labelErrore " ).remove();
            }
            else if(!verificaCapCliente()) {
                $( ".labelErrore " ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txtCapCliente);
            }
           /* if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificaEmailCliente() || !verificaTelefono() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
                if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificaCapCliente()) $( ".testoAlert").append(txtcapCliente).append("<br>");
                if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }
                if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}
            }*/
            $("#avanti2").prop('disabled', true);
        }
    });
    $("#partitaIva").on("change",function(event){
        if( verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente() &&verificaIndirizzoCliente()  &&verificaLocalitaCliente()&& verificapartitaIvaCliente() /*&& verificaTelefono() && verificaEmailCliente()*/ && (verificaTarga() || verificaTelaio()) && verificaDataImmatricolazione()  && verificaMarchio() && verificaModello() && verificaValore()  && verificadataInserimento() && verificaDataDecorrenza()){
            $("#avanti2").prop('disabled', false);
            $("#chiamataWS").attr("disabled", false);
            $( ".labelErrore" ).remove();
           // $("#linkModulo").removeClass("not-active");
        }else{
            if(verificapartitaIvaCliente()){
                $( ".labelErrore" ).remove();
            }
            else if(!verificapartitaIvaCliente()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                $("#chiamataWS").attr("disabled",true);
            }
            else if(verificaDataDecorrenza() && verificaValore() && verificaLocalitaCliente() && verificapartitaIvaCliente()){
                $("#chiamataWS").attr("disabled",false);
            }

            $("#avanti2").prop('disabled', true);
        }
    });
    $("#telefono").on("change", function(event) {
        if( verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente() &&verificaIndirizzoCliente()  &&verificaLocalitaCliente()&&verificapartitaIvaCliente() && /*verificaTelefono() && verificaEmailCliente() &&*/ (verificaTarga() || verificaTelaio()) && verificaDataImmatricolazione() &&  verificaMarchio() && verificaModello() && verificaValore()  && verificadataInserimento() && verificaDataDecorrenza()){
            $("#avanti2").prop('disabled', false);
            $( ".labelErrore" ).remove();
           // $("#linkModulo").removeClass("not-active");
        }else{
           // if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificaEmailCliente() || !verificaTelefono() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
               /* if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
                if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }*/
                if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}
           // }
            $("#avanti2").prop('disabled', true);
           // $("#linkModulo").addClass("not-active");
        }
    });
    $("#email").on("change", function(event) {
        if( verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente() &&verificaIndirizzoCliente()  &&verificaLocalitaCliente()&&verificapartitaIvaCliente() /*&& verificaTelefono()  && verificaEmailCliente() */&& (verificaTarga() || verificaTelaio()) && verificaDataImmatricolazione()  && verificaMarchio() && verificaModello() && verificaValore()&& verificadataInserimento() && verificaDataDecorrenza()){
            $("#avanti2").prop('disabled', false);
            $( ".labelErrore" ).remove();
            //$("#linkModulo").removeClass("not-active");
        }else{
            //if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificaEmailCliente() || !verificaTelefono() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
               if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                /*if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
                if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}*/
                if(!verificaEmailCliente()){$( ".testoAlert").append(txtemailCliente);}
           // }
            $("#avanti2").prop('disabled', true);
        }
    });
    $("#targa").on("change", function(event) {
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente() &&verificaIndirizzoCliente()  &&verificaLocalitaCliente()&&verificapartitaIvaCliente() && /*verificaTelefono() && verificaEmailCliente() &&*/ (verificaTarga() || verificaTelaio()) && verificaDataImmatricolazione()  && verificaMarchio() && verificaModello() && verificaValore() && verificadataInserimento()&& verificaDataDecorrenza() ){
            $("#avanti2").prop('disabled', false);
            $( ".labelErrore" ).remove();
        }else{
            if(!verificaTarga()){
                $( ".labelErrore " ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\" role=\"alert\"> <span class='testoAlert'></span></div>" );
                $( ".testoAlert").append(txttargaCliente);
            }
           /* if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificaEmailCliente() || !verificaTelefono() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
                if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }
                if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}
            }*/
            $("#avanti2").prop('disabled', true);
            //$("#linkModulo").addClass("not-active");
        }
    });
    $("#telaio").on("change", function(event) {
        if( verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente() &&verificaIndirizzoCliente()  &&verificaLocalitaCliente()&&verificapartitaIvaCliente() /*&& verificaEmailCliente() && verificaTelefono() */&& (verificaTarga() || verificaTelaio()) && verificaDataImmatricolazione() && verificaMarchio() && verificaModello() && verificaValore() && verificadataInserimento() && verificaDataDecorrenza()){
            $("#avanti2").prop('disabled', false);
            $( ".labelErrore" ).remove();
        }else{
            /*if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificaEmailCliente() || !verificaTelefono() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
                if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }
                if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}
            }*/
            $("#avanti2").prop('disabled', true);
        }
    });
    $("#marca").on("change", function(event) {
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente() &&verificaIndirizzoCliente()  &&verificaLocalitaCliente()&&verificapartitaIvaCliente() /*&& verificaEmailCliente() && verificaTelefono() */&& verificaMarchio() && verificaDataImmatricolazione() && (verificaTarga() || verificaTelaio())  && verificaModello() && verificaValore() && verificadataInserimento() ){
            $("#avanti2").prop('disabled', false);
            $( ".labelErrore" ).remove();
        }else{
          /*  if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificaEmailCliente() || !verificaTelefono() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
                if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }
                if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}
            }*/
            $( ".labelErrore" ).remove();
            $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
            if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
            $("#avanti2").prop('disabled', true);
        }
    });
    $("#modello").on("change", function(event) {
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente() &&verificaIndirizzoCliente()  &&verificaLocalitaCliente()&&verificapartitaIvaCliente() /*&& verificaEmailCliente() && verificaTelefono() */&& verificaMarchio() && verificaDataImmatricolazione() && (verificaTarga() || verificaTelaio()) && verificaModello()  && verificaValore() && verificadataInserimento()&& verificaDataDecorrenza() ){
            $("#avanti2").prop('disabled', false);
            $( ".labelErrore" ).remove();
        }else{
           /* if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificaEmailCliente() || !verificaTelefono() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
                if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }
                if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}
            }*/
            $("#avanti2").prop('disabled', true);
        }
    });
    $("#valoreAssicurato").on("change", function(event) {
        if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente() &&verificaIndirizzoCliente()  &&verificaLocalitaCliente()&&verificapartitaIvaCliente() /*&& verificaEmailCliente() && verificaTelefono()*/ && (verificaTarga() || verificaTelaio()) && verificaValore() && verificaDataImmatricolazione() &&  verificaMarchio() && verificaModello() && verificadataInserimento() && verificaDataDecorrenza()){
            //removeParameters();
            $("#avanti2").prop('disabled', false);
            $( ".labelErrore" ).remove();
        }else{
            $("#avanti2").prop('disabled', true);
            /*if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificaEmailCliente() || !verificaTelefono() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
                if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }
                if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}
            }*/
            if(verificaDataDecorrenza() && verificaValore() && verificaLocalitaCliente() && verificapartitaIvaCliente()){
                $("#chiamataWS").attr("disabled",false);
            }
            else{
                $("#chiamataWS").attr("disabled",true);
            }
        }
    });
    $("#dataImmatricolazione").on("changeDate", function(event) {
        var data2 = $("#dataImmatricolazione").val();
        if(data2 != ''){
            var nuovo= $("#nuovo").val();
            var data1 = $("#dataImmatricolazione").val().split('-');
            var one_day=1000;
            var messi=0;

            //$("#dataDecorrenza").val(data2).attr("readonly",true);
            //verifica 8 anni(96 mesi)
            var dataDecorrenza=$("#dataDecorrenza").val();
            if(dataDecorrenza !=''){
                var dataDeco = $("#dataDecorrenza").val().split('-');
                var dateImma= new Date(parseInt(data1[2], 10), parseInt(data1[1], 10)-1, parseInt(data1[0], 10));
                var dateformat1= new Date(parseInt(dataDeco[2], 10), parseInt(dataDeco[1], 10)-1, parseInt(dataDeco[0], 10));
                var durata=parseInt($("#durata").val());
                var scadenza=new Date(new Date(dateformat1).setMonth(dateformat1.getMonth()+durata));
                var diffdates=scadenza.getTime()-dateImma.getTime();
                diffdates=Math.floor(diffdates/one_day);
                var weekpass=Math.floor(diffdates/(60 * 60 * 24 * 7 * 4));
                messi=Math.abs(Math.round(weekpass));

            }
            if(verificaCognomeCliente()&& verificaNomeCliente() && verificaIndirizzoCliente()  && verificaLocalitaCliente() && verificaCapCliente() && verificapartitaIvaCliente() /*&& verificaEmailCliente() && verificaTelefono()*/ && (verificaTarga() || verificaTelaio()) && verificaMarchio() && verificaModello() && verificaValore()&& verificadataInserimento()  && verificaDataDecorrenza() ){
                if(messi>=96){
                    txtdataImmatsup8="la data immatricolazione non puo' essere superiore a 8 anni";
                    $( ".labelErrore" ).remove();
                    $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoanni8'></span></div>" );
                    $( ".testoanni8").append(txtdataImmatsup8).append("<br>");
                    $("#chiamataWS").attr("disabled","disabled");
                }else{
                    $( ".testoanni8").remove();
                    $("#chiamataWS").attr("disabled", false);
                }
                $("#avanti2").prop('disabled', false);
            }else{
               //if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() /*|| !verificaEmailCliente() || !verificaTelefono()*/ || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                   $( ".labelErrore" ).remove();
                   $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                   if(messi>=96){
                       $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoanni8'></span></div>" );
                       $( ".testoanni8").append(txtdataImmatsup8).append("<br>");
                       $("#chiamataWS").attr("disabled","disabled");
                   }else{
                       $( ".testoanni8").remove();
                       $("#chiamataWS").attr("disabled", false);
                   }
                   if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                   if(!verificaTarga() && !verificaTelaio()) {
                       $(".testoAlert").append("compilare al meno uno dei due: telaio o targa").append("<br>");
                   }
                    if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                    if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                    if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                    if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                    if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                    if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                    if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                    if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                    if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                    if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                    if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                  /*if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }
                    if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}*/
                $("#avanti2").prop('disabled', true);
            }
        }else{
            $("#avanti2").prop('disabled', true);
            $("#chiamataWS").attr("disabled","disabled");
            //$("#linkModulo").addClass("not-active");
            //removeParameters();
        }
    });
    $("#dataDecorrenza").on("changeDate", function(event) {
        var data1 = $("#dataDecorrenza").val();
        if(data1 != ''){
            var nuovo= $("#nuovo").val();
            //var dataImma = $("#dataImmatricolazione").val();
            var data2 = $("#dataImmatricolazione").val().split('-');
            var one_day=1000;
            var messi=0;
            var dataDecorrenza=$("#dataDecorrenza").val();
            if(dataDecorrenza!=''){
                var dataDeco = $("#dataDecorrenza").val().split('-');
                var dateformat1= new Date(parseInt(dataDeco[2], 10), parseInt(dataDeco[1], 10)-1, parseInt(dataDeco[0], 10));
                var dateImma= new Date(parseInt(data2[2], 10), parseInt(data2[1], 10)-1, parseInt(data2[0], 10));
                var durata=parseInt($("#durata").val());
                var scadenza=new Date(new Date(dateformat1).setMonth(dateformat1.getMonth()+durata));
                var diffdates=scadenza.getTime()-dateImma.getTime();
                diffdates=Math.floor(diffdates/one_day);
                var weekpass=Math.floor(diffdates/(60 * 60 * 24 * 7 * 4));
                messi=Math.abs(Math.round(weekpass));
            }
            if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente() &&verificaIndirizzoCliente()  &&verificaLocalitaCliente()&&verificapartitaIvaCliente() /*&& verificaEmailCliente() && verificaTelefono()*/ && (verificaTarga() || verificaTelaio()) && verificaMarchio() && verificaModello() && verificaValore()&& verificadataInserimento()  && verificaDataDecorrenza() ){
                $( ".labelErrore" ).remove();
                if(messi>=96){
                    $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoanni8'></span></div>" );
                    $( ".testoanni8").append("la data immatricolazione non puo' essere superiore a 8 anni").append("<br>");
                    $("#chiamataWS").attr("disabled","disabled");
                }else{
                    $( ".testoanni8").remove();
                    $("#chiamataWS").attr("disabled", false);
                }
                $("#avanti2").prop('disabled', false);
            }else{
                //if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificaEmailCliente() || !verificaTelefono() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                    $( ".labelErrore" ).remove();
                    $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                    if(messi>=96){
                        $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoanni8'></span></div>" );
                        $( ".testoanni8").append("la data immatricolazione non puo' essere superiore a 8 anni").append("<br>");
                        $("#chiamataWS").attr("disabled","disabled");
                    }else{
                        $( ".testoanni8").remove();
                        $("#chiamataWS").attr("disabled", false);
                    }
                    if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                    if(!verificaTarga() && !verificaTelaio()) {
                        $(".testoAlert").append("compilare al meno uno dei due: telaio o targa").append("<br>");
                    }
                    if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                    if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                    if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                    //if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                    if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                    if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                    if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                    if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                    if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                    if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                    if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                   /* if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }
                    if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}*/
                //}
                $("#avanti2").prop('disabled', true);
            }
        }else{
            $("#avanti2").prop('disabled', true);
            $("#chiamataWS").attr("disabled","disabled");
            //$("#linkModulo").addClass("not-active");
            //removeParameters();
        }
    });
    $("#dataInserimento").on("changeDate", function(event) {
        var data1 = $("#dataInserimento").val();
        if(data1 != ''){
            if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente() &&verificaIndirizzoCliente()  &&verificaLocalitaCliente()&&verificapartitaIvaCliente() /*&& verificaEmailCliente() && verificaTelefono() */&& (verificaTarga() || verificaTelaio()) && verificaMarchio() && verificaModello() && verificaValore() && verificadataInserimento()  && verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $("#avanti2").prop('disabled', false);
            }else{
                if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() ||/* !verificaEmailCliente() || !verificaTelefono() ||*/ !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                    $( ".labelErrore" ).remove();
                    $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                    if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                    if(!verificaTarga() && !verificaTelaio()) {
                        $(".testoAlert").append("compilare al meno uno dei due: telaio o targa").append("<br>");
                    }
                    if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                    if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                    if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                    if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                    if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                    if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                    if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                    if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                    if(!verificaCapCliente()) $( ".testoAlert").append(txtCapCliente).append("<br>");
                    if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                    if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                   /* if(!verificaEmailCliente()) { $( ".testoAlert").append(txtemailCliente).append("<br>"); }
                    if(!verificaTelefono()) {$( ".testoAlert").append(txtcellCliente).append("<br>");}*/
                }
                $("#avanti2").prop('disabled', true);
                //$("#linkModulo").addClass("not-active");
                //removeParameters();
            }
        }else{
            $("#avanti2").prop('disabled', true);
            //$("#linkModulo").addClass("not-active");
            //removeParameters();
        }
    });
    $("#avanti1").on ("click", function(event){
        href="#sectionB";
        attivaTab('sectionB');
        verificaPIvaDealer();
        verificaIndirizzoDealer();
        verificaCapDealer();
        verificaProvDealer();
        verificaComuneDealer();
        verificaEmailDealer();
        verificaVenditore();
        if(!verificaPIvaDealer() || !verificaIndirizzoDealer() || !verificaCapDealer() || !verificaProvDealer() || !verificaComuneDealer() || !verificaEmailDealer() || !verificaVenditore()){
            $( ".labelErrore" ).remove();
            $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
            if(!verificaEmailDealer()) $( ".testoAlert").append(txtemailDealer).append("<br>");
            if(!verificaVenditore()) $( ".testoAlert").append(txtnomeVenditore).append("<br>");

        }
        /*if(verificaEmailDealer() && verificaCapDealer()&& verificaComuneDealer()&& verificaIndirizzoDealer()&&verificaPIvaDealer()&&verificaProvDealer() ){
            $("#bottonSalva").prop('disabled', false);
        }*/
    });
    $("#avanti2").on ("click", function(event){
        href="#sectionC";
        attivaTab('sectionC');
        verificapartitaIvaCliente();
        verificaTarga();
        verificaTelaio();
        verificaMarchio();
        verificaModello();
        verificaDataImmatricolazione();
        verificaDataDecorrenza();
        verificadataInserimento();
        verificaValore();
        verificaCognomeCliente();
        verificaNomeCliente();
        verificaCapCliente();
        verificaIndirizzoCliente();
        verificaLocalitaCliente();
        verificaProvvDealer();
        verificaProvvGMFI();
        verificaProvvVenditore();
        verificaPremioAssi();

        if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
            $( ".labelErrore" ).remove();
            $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
            if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
            if(!verificaTarga() && !verificaTelaio()) {
                    $(".testoAlert").append("compilare al meno uno dei due: telaio o targa").append("<br>");
            }

            if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
            if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
            if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
            if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
            if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
            if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
            if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
            if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
            if(!verificaCapCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
            if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
            if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");


        }

    });
    $("#indietro1").on ("click", function(event){
        href="#sectionA";
        attivaTab('sectionA');

    });
    $("#indietro2").on ("click", function(event){
        href="#sectionB";
        attivaTab('sectionB');

    });
    $('#bottonSalva').click(function(e) {
        if( !verificadataInserimento() && !verificaDataDecorrenza()  && !verificaDataImmatricolazione() /*&& (!verificaPremioAssi() && !verificaProvvDealer() && !verificaProvvGMFI() && !verificaProvvVenditore())*/){
            e.preventDefault();
            swal({
                title: "controllare che i valori delle date siano compilati!",
                type: "error",
                timer: "2000",
                showConfirmButton: false
            });
        }
        //do other stuff when a click happens
    });
    $('#chiamataWS').click(function(e) {
        e.preventDefault();
        if(verificaLocalitaCliente() && verificaValore() && verificaTipoDealer() && verificaPercVenditore() && verificaDataDecorrenza() && verificapartitaIvaCliente() ){
            $( ".labelErrore" ).remove();
            chiamataWS();
        }else{
            swal({
                title: "controllare che il tipo dealer, la partita iva del cliente, la località, la data effetto e il valore assicurato siano compilati!",
                type: "error",
                timer: "2000",
                showConfirmButton: false
            });
        }
        //do other stuff when a click happens
    });
    function chiamataWS(){
        var valoreAssicurato=$("#valoreAssicurato").val();
        var provincia=$("#provincia").val();
        var comune=$("#localita").val();
        var durata=$("#durata").val();
        var onStar=$("#onStar").val();
        var dealerType=$("#dealerType").val();
        var step=$("#step").val();
        var percVenditore=$("#percVenditore").val();
        var copertura=$("#coperturaRichiesta").val();
        var dataDecorrenza=$("#dataDecorrenza").val();
        var tipoOperazione="A";
        var tariffa=$('#tariffa').val();
        var modello=$('#modello').val();
        $.post("${createLink(controller: "polizze", action: "chiamataWS")}",{ tipoPolizza:"CASH",  copertura:copertura, valoreAssicurato:valoreAssicurato, provincia:provincia, comune:comune, durata:durata,onStar:onStar,dealerType:dealerType,step:step,percVenditore:percVenditore,dataDecorrenza:dataDecorrenza,operazione:tipoOperazione,tariffa:tariffa, modello:modello, _csrf: "${request._csrf.token}"}, function(response) {
            var risposta=response.risposta;
            if(risposta==false){
                var errore=response.errore;
                var provvDealer=Math.round(response.provvDealer * 100) / 100
                var provvGmfi=Math.round(response.provvgmfi * 100) / 100
                $( ".labelErrore" ).remove();
                //$( ".inner" ).append( "<p>"+errore+"</p>" );
                $( ".inner" ).after( "<div class=\"labelErrore marginesotto\"><p>"+response.errore+"</p></div>" );
                $("#premioImponibile").val(response.premioImponibile);
                $("#premioLordo").val(response.premioLordo);
                $("#provvDealer").val(provvDealer);
                $("#provvGmfi").val(provvGmfi);
                $("#provvVenditore").val(response.provvVend);
                $("#codiceZonaTerritoriale").val(response.codiceZonaTerritoriale);
                $("#rappel").val(response.rappel);
                $("#imposte").val(response.imposte);
            }else{
                var provvDealer=Math.round(response.provvDealer * 100) / 100
                var provvGmfi=Math.round(response.provvgmfi * 100) / 100
                $( ".labelErrore" ).remove();
                //$( ".inner" ).append( "<p>I dati sono stati caricati dal webservice</p>" );
                $( ".inner" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'>I dati sono stati caricati dal webservice</span></div>" );
                $("#premioImponibile").val(response.premioImponibile);
                $("#premioLordo").val(response.premioLordo);
                $("#provvDealer").val(provvDealer);
                $("#provvGmfi").val(provvGmfi);
                $("#provvVenditore").val(response.provvVend);
                $("#codiceZonaTerritoriale").val(response.codiceZonaTerritoriale);
                $("#rappel").val(response.rappel);
                $("#imposte").val(response.imposte);
            }
        }, "json");
       /*$.post("\\${createLink(controller: "polizze", action: "chiamataWS")}",{ tipoPolizza:"CASH",  copertura:copertura, valoreAssicurato:valoreAssicurato, provincia:provincia, comune:comune, durata:durata,onStar:onStar,dealerType:dealerType,step:step,percVenditore:percVenditore,dataDecorrenza:dataDecorrenza,operazione:tipoOperazione, _csrf: "\\${request._csrf.token}"}, function(response) {
            var risposta=response.risposta;
            if(risposta==false){
                var errore=response.errore;
                var provvDealer=Math.round(response.provvDealer * 100) / 100
                var provvGmfi=Math.round(response.provvgmfi * 100) / 100
                $( ".labelErrore" ).remove();
                //$( ".inner" ).append( "<p>"+errore+"</p>" );
                $( ".inner" ).after( "<div class=\"labelErrore marginesotto\"><p>"+response.errore+"</p></div>" );
                $("#premioImponibile").val(response.premioImponibile);
                $("#premioLordo").val(response.premioLordo);
                $("#provvDealer").val(provvDealer);
                $("#provvGmfi").val(provvGmfi);
                $("#provvVenditore").val(response.provvVend);
                $("#codiceZonaTerritoriale").val(response.codiceZonaTerritoriale);
                $("#rappel").val(response.rappel);
                $("#imposte").val(response.imposte);
            }else{
                var provvDealer=Math.round(response.provvDealer * 100) / 100
                var provvGmfi=Math.round(response.provvgmfi * 100) / 100
                $( ".labelErrore" ).remove();
                //$( ".inner" ).append( "<p>I dati sono stati caricati dal webservice</p>" );
                $( ".inner" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'>I dati sono stati caricati dal webservice</span></div>" );
                $("#premioImponibile").val(response.premioImponibile);
                $("#premioLordo").val(response.premioLordo);
                $("#provvDealer").val(provvDealer);
                $("#provvGmfi").val(provvGmfi);
                $("#provvVenditore").val(response.provvVend);
                $("#codiceZonaTerritoriale").val(response.codiceZonaTerritoriale);
                $("#rappel").val(response.rappel);
                $("#imposte").val(response.imposte);
            }
        }, "json");*/
    }
    $("#iva").on("change", function(event){
        var valIva=$( "#iva" ).val();
        var valAssicIVA=$("#valoreAssicuratoconiva").val();
        if(valIva == "1"){
            var valsenzIVA=(valAssicIVA/1.22).toFixed(2);
            $("#valoreAssicurato").val( valsenzIVA).attr("readonly",true);
        }else{
            $("#valoreAssicurato").val( valAssicIVA ).attr("readonly",true);
        }
    });
    $("#valoreAssicuratoconiva").on("change", function(event){
        var valIva=$( "#iva" ).val();
        var valAssicIVA=$("#valoreAssicuratoconiva").val();

        if(valAssicIVA != '' && valAssicIVA>0){
            if(valIva == "1"){
                var valsenzIVA=(valAssicIVA/1.22).toFixed(2);
                $("#valoreAssicurato").val( valsenzIVA).attr("readonly",true);
            }else{
                $("#valoreAssicurato").val( valAssicIVA ).attr("readonly",true);
            }
            if(verificaCognomeCliente()&& verificaNomeCliente()&& verificaCapCliente() && verificaIndirizzoCliente()  && verificaLocalitaCliente()&&verificapartitaIvaCliente() && verificaEmailCliente() && verificaTelefono() && (verificaTarga() || verificaTelaio()) && verificaMarchio() && verificaModello() && verificaValore() && verificadataInserimento() && verificaDataDecorrenza()){
                //removeParameters();
                $("#avanti2").prop('disabled', false);
            }else{
                /*if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                    $( ".labelErrore" ).remove();
                    $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                    if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                    if(!verificaTarga()) $( ".testoAlert").append(txttargaCliente).append("<br>");
                    if(!verificaTelaio()) $( ".testoAlert").append(txttelaioCliente).append("<br>");
                    if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                    if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                    if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                    if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                    if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                    if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                    if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                    if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                    if(!verificaCapCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                    if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                    if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                    if(!verificaEmailDealer()) $( ".testoAlert").append(txtemailDealer).append("<br>");

                }*/
                $("#avanti2").prop('disabled', true);
            }
        }else{
            //if(!verificaCognomeCliente() || !verificaNomeCliente() || !verificaCapCliente() || !verificaIndirizzoCliente() || !verificaLocalitaCliente() || !verificapartitaIvaCliente() || (!verificaTarga() || !verificaTelaio()) || !verificaMarchio() || !verificaModello() || !verificaDataImmatricolazione()  || !verificaValore() || !verificadataInserimento() || !verificaDataDecorrenza()){
                $( ".labelErrore" ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                if(!verificapartitaIvaCliente()) $( ".testoAlert").append(txtpartitaIvaCliente).append("<br>");
                if(!verificaTarga() && !verificaTelaio()) {
                    $(".testoAlert").append("compilare al meno uno dei due: telaio o targa").append("<br>");
                }
                if(!verificaMarchio()) $( ".testoAlert").append(txtmarchioCliente).append("<br>");
                if(!verificaModello()) $( ".testoAlert").append(txtversioneCliente).append("<br>");
                if(!verificaDataImmatricolazione()) $( ".testoAlert").append(txtdataImmCliente).append("<br>");
                if(!verificaDataDecorrenza()) $( ".testoAlert").append(txtdataDecoCliente).append("<br>");
                if(!verificadataInserimento()) $( ".testoAlert").append(txtdataInsCliente).append("<br>");
                if(!verificaValore()) $( ".testoAlert").append(txtvaloreCliente).append("<br>");
                if(!verificaCognomeCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaNomeCliente()) $( ".testoAlert").append(txtNomeCliente).append("<br>");
                if(!verificaCapCliente()) $( ".testoAlert").append(txtCognomeCliente).append("<br>");
                if(!verificaIndirizzoCliente()) $( ".testoAlert").append(txtIndirizzoCliente).append("<br>");
                if(!verificaLocalitaCliente()) $( ".testoAlert").append(txtLocalitaCliente).append("<br>");
                if(!verificaEmailDealer()) $( ".testoAlert").append(txtemailDealer).append("<br>");

           // }
            $("#avanti2").prop('disabled', true);
        }

    });

    $("#nuovo").on("change", function(event){
        var data1 = $("#dataImmatricolazione").val();
        var nuovo= $("#nuovo").val();
        if(data1 != ''){
            var data2 = $("#dataImmatricolazione").val().split('-');
            if(nuovo==1){
                //$("#dataDecorrenza").val(data1).attr("readonly",true);
                //verifica 8 anni(96 mesi)
                var dataDecorrenza=$("#dataDecorrenza").val();
                var dataDeco = $("#dataDecorrenza").val().split('-');
                var dateformat1= new Date(parseInt(dataDeco[2], 10), parseInt(dataDeco[1], 10)-1, parseInt(dataDeco[0], 10));
                var durata=parseInt($("#durata").val());
                var scadenza=new Date(new Date(dateformat1).setMonth(dateformat1.getMonth()+durata));
                var data96=new Date(new Date(dateformat1).setMonth(dateformat1.getMonth()+96));

                if(scadenza.getTime()>data96.getTime()){
                    $( ".labelErrore" ).remove();
                    $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append("la data immatricolazione non puo' essere superiore a 8 anni").append("<br>");
                }
            }else{
                $("#dataDecorrenza").attr("readonly",false);

                //verifica 8 anni(96 mesi)
                var dataDeco = $("#dataDecorrenza").val().split('-');
                var dateformat1= new Date(parseInt(dataDeco[2], 10), parseInt(dataDeco[1], 10)-1, parseInt(dataDeco[0], 10));
                var dateImma= new Date(parseInt(data2[2], 10), parseInt(data2[1], 10)-1, parseInt(data2[0], 10));
                var durata=parseInt($("#durata").val());
                var scadenza=new Date(new Date(dateformat1).setMonth(dateformat1.getMonth()+durata));
                var data96=new Date(new Date(dateImma).setMonth(dateImma.getMonth()+96));
                if(scadenza.getTime()>data96.getTime()){
                    $( ".labelErrore" ).remove();
                    $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'></span></div>" );
                    $( ".testoAlert").append("la data immatricolazione non puo' essere superiore a 8 anni").append("<br>");
                }
            }
        }
    });
    function removeDiacritics (str) {

        var defaultDiacriticsRemovalMap = [
            {'base':'A', 'letters':/[\u0041\u24B6\uFF21\u00C0\u00C1\u00C2\u1EA6\u1EA4\u1EAA\u1EA8\u00C3\u0100\u0102\u1EB0\u1EAE\u1EB4\u1EB2\u0226\u01E0\u00C4\u01DE\u1EA2\u00C5\u01FA\u01CD\u0200\u0202\u1EA0\u1EAC\u1EB6\u1E00\u0104\u023A\u2C6F]/g},
            {'base':'AA','letters':/[\uA732]/g},
            {'base':'AE','letters':/[\u00C6\u01FC\u01E2]/g},
            {'base':'AO','letters':/[\uA734]/g},
            {'base':'AU','letters':/[\uA736]/g},
            {'base':'AV','letters':/[\uA738\uA73A]/g},
            {'base':'AY','letters':/[\uA73C]/g},
            {'base':'B', 'letters':/[\u0042\u24B7\uFF22\u1E02\u1E04\u1E06\u0243\u0182\u0181]/g},
            {'base':'C', 'letters':/[\u0043\u24B8\uFF23\u0106\u0108\u010A\u010C\u00C7\u1E08\u0187\u023B\uA73E]/g},
            {'base':'D', 'letters':/[\u0044\u24B9\uFF24\u1E0A\u010E\u1E0C\u1E10\u1E12\u1E0E\u0110\u018B\u018A\u0189\uA779]/g},
            {'base':'DZ','letters':/[\u01F1\u01C4]/g},
            {'base':'Dz','letters':/[\u01F2\u01C5]/g},
            {'base':'E', 'letters':/[\u0045\u24BA\uFF25\u00C8\u00C9\u00CA\u1EC0\u1EBE\u1EC4\u1EC2\u1EBC\u0112\u1E14\u1E16\u0114\u0116\u00CB\u1EBA\u011A\u0204\u0206\u1EB8\u1EC6\u0228\u1E1C\u0118\u1E18\u1E1A\u0190\u018E]/g},
            {'base':'F', 'letters':/[\u0046\u24BB\uFF26\u1E1E\u0191\uA77B]/g},
            {'base':'G', 'letters':/[\u0047\u24BC\uFF27\u01F4\u011C\u1E20\u011E\u0120\u01E6\u0122\u01E4\u0193\uA7A0\uA77D\uA77E]/g},
            {'base':'H', 'letters':/[\u0048\u24BD\uFF28\u0124\u1E22\u1E26\u021E\u1E24\u1E28\u1E2A\u0126\u2C67\u2C75\uA78D]/g},
            {'base':'I', 'letters':/[\u0049\u24BE\uFF29\u00CC\u00CD\u00CE\u0128\u012A\u012C\u0130\u00CF\u1E2E\u1EC8\u01CF\u0208\u020A\u1ECA\u012E\u1E2C\u0197]/g},
            {'base':'J', 'letters':/[\u004A\u24BF\uFF2A\u0134\u0248]/g},
            {'base':'K', 'letters':/[\u004B\u24C0\uFF2B\u1E30\u01E8\u1E32\u0136\u1E34\u0198\u2C69\uA740\uA742\uA744\uA7A2]/g},
            {'base':'L', 'letters':/[\u004C\u24C1\uFF2C\u013F\u0139\u013D\u1E36\u1E38\u013B\u1E3C\u1E3A\u0141\u023D\u2C62\u2C60\uA748\uA746\uA780]/g},
            {'base':'LJ','letters':/[\u01C7]/g},
            {'base':'Lj','letters':/[\u01C8]/g},
            {'base':'M', 'letters':/[\u004D\u24C2\uFF2D\u1E3E\u1E40\u1E42\u2C6E\u019C]/g},
            {'base':'N', 'letters':/[\u004E\u24C3\uFF2E\u01F8\u0143\u00D1\u1E44\u0147\u1E46\u0145\u1E4A\u1E48\u0220\u019D\uA790\uA7A4]/g},
            {'base':'NJ','letters':/[\u01CA]/g},
            {'base':'Nj','letters':/[\u01CB]/g},
            {'base':'O', 'letters':/[\u004F\u24C4\uFF2F\u00D2\u00D3\u00D4\u1ED2\u1ED0\u1ED6\u1ED4\u00D5\u1E4C\u022C\u1E4E\u014C\u1E50\u1E52\u014E\u022E\u0230\u00D6\u022A\u1ECE\u0150\u01D1\u020C\u020E\u01A0\u1EDC\u1EDA\u1EE0\u1EDE\u1EE2\u1ECC\u1ED8\u01EA\u01EC\u00D8\u01FE\u0186\u019F\uA74A\uA74C]/g},
            {'base':'OI','letters':/[\u01A2]/g},
            {'base':'OO','letters':/[\uA74E]/g},
            {'base':'OU','letters':/[\u0222]/g},
            {'base':'P', 'letters':/[\u0050\u24C5\uFF30\u1E54\u1E56\u01A4\u2C63\uA750\uA752\uA754]/g},
            {'base':'Q', 'letters':/[\u0051\u24C6\uFF31\uA756\uA758\u024A]/g},
            {'base':'R', 'letters':/[\u0052\u24C7\uFF32\u0154\u1E58\u0158\u0210\u0212\u1E5A\u1E5C\u0156\u1E5E\u024C\u2C64\uA75A\uA7A6\uA782]/g},
            {'base':'S', 'letters':/[\u0053\u24C8\uFF33\u1E9E\u015A\u1E64\u015C\u1E60\u0160\u1E66\u1E62\u1E68\u0218\u015E\u2C7E\uA7A8\uA784]/g},
            {'base':'T', 'letters':/[\u0054\u24C9\uFF34\u1E6A\u0164\u1E6C\u021A\u0162\u1E70\u1E6E\u0166\u01AC\u01AE\u023E\uA786]/g},
            {'base':'TZ','letters':/[\uA728]/g},
            {'base':'U', 'letters':/[\u0055\u24CA\uFF35\u00D9\u00DA\u00DB\u0168\u1E78\u016A\u1E7A\u016C\u00DC\u01DB\u01D7\u01D5\u01D9\u1EE6\u016E\u0170\u01D3\u0214\u0216\u01AF\u1EEA\u1EE8\u1EEE\u1EEC\u1EF0\u1EE4\u1E72\u0172\u1E76\u1E74\u0244]/g},
            {'base':'V', 'letters':/[\u0056\u24CB\uFF36\u1E7C\u1E7E\u01B2\uA75E\u0245]/g},
            {'base':'VY','letters':/[\uA760]/g},
            {'base':'W', 'letters':/[\u0057\u24CC\uFF37\u1E80\u1E82\u0174\u1E86\u1E84\u1E88\u2C72]/g},
            {'base':'X', 'letters':/[\u0058\u24CD\uFF38\u1E8A\u1E8C]/g},
            {'base':'Y', 'letters':/[\u0059\u24CE\uFF39\u1EF2\u00DD\u0176\u1EF8\u0232\u1E8E\u0178\u1EF6\u1EF4\u01B3\u024E\u1EFE]/g},
            {'base':'Z', 'letters':/[\u005A\u24CF\uFF3A\u0179\u1E90\u017B\u017D\u1E92\u1E94\u01B5\u0224\u2C7F\u2C6B\uA762]/g},
            {'base':'a', 'letters':/[\u0061\u24D0\uFF41\u1E9A\u00E0\u00E1\u00E2\u1EA7\u1EA5\u1EAB\u1EA9\u00E3\u0101\u0103\u1EB1\u1EAF\u1EB5\u1EB3\u0227\u01E1\u00E4\u01DF\u1EA3\u00E5\u01FB\u01CE\u0201\u0203\u1EA1\u1EAD\u1EB7\u1E01\u0105\u2C65\u0250]/g},
            {'base':'aa','letters':/[\uA733]/g},
            {'base':'ae','letters':/[\u00E6\u01FD\u01E3]/g},
            {'base':'ao','letters':/[\uA735]/g},
            {'base':'au','letters':/[\uA737]/g},
            {'base':'av','letters':/[\uA739\uA73B]/g},
            {'base':'ay','letters':/[\uA73D]/g},
            {'base':'b', 'letters':/[\u0062\u24D1\uFF42\u1E03\u1E05\u1E07\u0180\u0183\u0253]/g},
            {'base':'c', 'letters':/[\u0063\u24D2\uFF43\u0107\u0109\u010B\u010D\u00E7\u1E09\u0188\u023C\uA73F\u2184]/g},
            {'base':'d', 'letters':/[\u0064\u24D3\uFF44\u1E0B\u010F\u1E0D\u1E11\u1E13\u1E0F\u0111\u018C\u0256\u0257\uA77A]/g},
            {'base':'dz','letters':/[\u01F3\u01C6]/g},
            {'base':'e', 'letters':/[\u0065\u24D4\uFF45\u00E8\u00E9\u00EA\u1EC1\u1EBF\u1EC5\u1EC3\u1EBD\u0113\u1E15\u1E17\u0115\u0117\u00EB\u1EBB\u011B\u0205\u0207\u1EB9\u1EC7\u0229\u1E1D\u0119\u1E19\u1E1B\u0247\u025B\u01DD]/g},
            {'base':'f', 'letters':/[\u0066\u24D5\uFF46\u1E1F\u0192\uA77C]/g},
            {'base':'g', 'letters':/[\u0067\u24D6\uFF47\u01F5\u011D\u1E21\u011F\u0121\u01E7\u0123\u01E5\u0260\uA7A1\u1D79\uA77F]/g},
            {'base':'h', 'letters':/[\u0068\u24D7\uFF48\u0125\u1E23\u1E27\u021F\u1E25\u1E29\u1E2B\u1E96\u0127\u2C68\u2C76\u0265]/g},
            {'base':'hv','letters':/[\u0195]/g},
            {'base':'i', 'letters':/[\u0069\u24D8\uFF49\u00EC\u00ED\u00EE\u0129\u012B\u012D\u00EF\u1E2F\u1EC9\u01D0\u0209\u020B\u1ECB\u012F\u1E2D\u0268\u0131]/g},
            {'base':'j', 'letters':/[\u006A\u24D9\uFF4A\u0135\u01F0\u0249]/g},
            {'base':'k', 'letters':/[\u006B\u24DA\uFF4B\u1E31\u01E9\u1E33\u0137\u1E35\u0199\u2C6A\uA741\uA743\uA745\uA7A3]/g},
            {'base':'l', 'letters':/[\u006C\u24DB\uFF4C\u0140\u013A\u013E\u1E37\u1E39\u013C\u1E3D\u1E3B\u017F\u0142\u019A\u026B\u2C61\uA749\uA781\uA747]/g},
            {'base':'lj','letters':/[\u01C9]/g},
            {'base':'m', 'letters':/[\u006D\u24DC\uFF4D\u1E3F\u1E41\u1E43\u0271\u026F]/g},
            {'base':'n', 'letters':/[\u006E\u24DD\uFF4E\u01F9\u0144\u00F1\u1E45\u0148\u1E47\u0146\u1E4B\u1E49\u019E\u0272\u0149\uA791\uA7A5]/g},
            {'base':'nj','letters':/[\u01CC]/g},
            {'base':'o', 'letters':/[\u006F\u24DE\uFF4F\u00F2\u00F3\u00F4\u1ED3\u1ED1\u1ED7\u1ED5\u00F5\u1E4D\u022D\u1E4F\u014D\u1E51\u1E53\u014F\u022F\u0231\u00F6\u022B\u1ECF\u0151\u01D2\u020D\u020F\u01A1\u1EDD\u1EDB\u1EE1\u1EDF\u1EE3\u1ECD\u1ED9\u01EB\u01ED\u00F8\u01FF\u0254\uA74B\uA74D\u0275]/g},
            {'base':'oi','letters':/[\u01A3]/g},
            {'base':'ou','letters':/[\u0223]/g},
            {'base':'oo','letters':/[\uA74F]/g},
            {'base':'p','letters':/[\u0070\u24DF\uFF50\u1E55\u1E57\u01A5\u1D7D\uA751\uA753\uA755]/g},
            {'base':'q','letters':/[\u0071\u24E0\uFF51\u024B\uA757\uA759]/g},
            {'base':'r','letters':/[\u0072\u24E1\uFF52\u0155\u1E59\u0159\u0211\u0213\u1E5B\u1E5D\u0157\u1E5F\u024D\u027D\uA75B\uA7A7\uA783]/g},
            {'base':'s','letters':/[\u0073\u24E2\uFF53\u00DF\u015B\u1E65\u015D\u1E61\u0161\u1E67\u1E63\u1E69\u0219\u015F\u023F\uA7A9\uA785\u1E9B]/g},
            {'base':'t','letters':/[\u0074\u24E3\uFF54\u1E6B\u1E97\u0165\u1E6D\u021B\u0163\u1E71\u1E6F\u0167\u01AD\u0288\u2C66\uA787]/g},
            {'base':'tz','letters':/[\uA729]/g},
            {'base':'u','letters':/[\u0075\u24E4\uFF55\u00F9\u00FA\u00FB\u0169\u1E79\u016B\u1E7B\u016D\u00FC\u01DC\u01D8\u01D6\u01DA\u1EE7\u016F\u0171\u01D4\u0215\u0217\u01B0\u1EEB\u1EE9\u1EEF\u1EED\u1EF1\u1EE5\u1E73\u0173\u1E77\u1E75\u0289]/g},
            {'base':'v','letters':/[\u0076\u24E5\uFF56\u1E7D\u1E7F\u028B\uA75F\u028C]/g},
            {'base':'vy','letters':/[\uA761]/g},
            {'base':'w','letters':/[\u0077\u24E6\uFF57\u1E81\u1E83\u0175\u1E87\u1E85\u1E98\u1E89\u2C73]/g},
            {'base':'x','letters':/[\u0078\u24E7\uFF58\u1E8B\u1E8D]/g},
            {'base':'y','letters':/[\u0079\u24E8\uFF59\u1EF3\u00FD\u0177\u1EF9\u0233\u1E8F\u00FF\u1EF7\u1E99\u1EF5\u01B4\u024F\u1EFF]/g},
            {'base':'z','letters':/[\u007A\u24E9\uFF5A\u017A\u1E91\u017C\u017E\u1E93\u1E95\u01B6\u0225\u0240\u2C6C\uA763]/g}
        ];

        for(var i=0; i<defaultDiacriticsRemovalMap.length; i++) {
            str = str.replace(defaultDiacriticsRemovalMap[i].letters, defaultDiacriticsRemovalMap[i].base);
        }

        return str;

    }
</script>
</body>
</html>