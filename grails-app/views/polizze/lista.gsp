<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="navigazione"/>
    <title>Elenco</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
    <style>
    .ui-datepicker-calendar {
        display: none;
    }
    </style>
</head>

<body>
<div class="row">
   <div class="col-md-12 col-xs-10">
       <g:if test="${flash.error}"><div class="labelErrore marginesotto">${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
       <g:if test="${flash.message}"><div class="labelErrore marginesotto">${flash.message.toString().replaceAll("\\<.*?>","")} </div></g:if>
        <g:if test="${flash.errorePratica}">
            <div class="labelErrore marginesotto">
                <g:each var="errore" in="${flash.errorePratica}">
                    <p>${errore.toString().replaceAll("\\<.*?>\\[]","")}</p>
                </g:each>
            </div>
        </g:if>
       <g:if test="${flash.certificati}">
           <div class="labelErrore marginesotto">
               <g:each var="messageP" in="${flash.certificati}">
                   <p>${messageP}</p>
               </g:each>
           </div>
       </g:if>
        <g:if test="${flash.praticheNuove}">
            <div class="labelErrore marginesotto">
                <g:each var="messageP" in="${flash.praticheNuove}">
                    <p>${messageP.toString().replaceAll("\\<.*?>\\[]","")}</p>
                </g:each>
            </div>
        </g:if>
       <g:if test="${flash.documentiIns}">
           <div class="labelErrore marginesotto">
               <g:each var="messageD" in="${flash.documentiIns}">
                   <p>${messageD.toString().replaceAll("\\<.*?>\\[]","")}</p>
               </g:each>
           </div>
       </g:if>
       <div class="datiPoli">
       </div>
        <f:form  id="listform" method="get">
            <div class="col-md-3 col-xs-6  marginiBottLista area-ricerca" style="margin-bottom: 4px;">
                <div class="input-group input-ricerca">
                    <input type="text" class="form-controlC" placeholder="ricerca polizze CASH" name="search" value="${params.search}">
                    <span class="input-group-btn">
                        <button type="submit" id="button_search" class="btn btn-ricerca" onchange="window.location.href = window.location.href.substr(0,window.location.href.lastIndexOf('?'));"><i class="glyphicon glyphicon-search"></i></button>
                    </span>
                </div>
            </div>
            <div class="col-md-3 col-xs-6 marginiBottLista">
                <a href="${createLink(action: "nuovaPolizza")}" class="btn btn-ricerca"><b>Nuova polizza</b> <i class="fa fa-file-text"></i></a>
            </div>
            <div class="col-md-3 col-xs-6 marginiBottLista">
                <a href="#" id="new-pDSLIP" class="btn btn-ricerca" title="polizzeDSLIP"><b>Caricare polizze con DSLIP</b> <i class="fa fa-upload"></i></a>
            </div>
            <div class="col-md-3 col-xs-3 marginiBottLista">
                <a href="${createLink(controller: "polizze", action: "generaFilePolizzeattivate")}" target="_blank" class="btn btn-ricerca" title="Estrazione polizze"><b>Riepilogo polizze CASH</b> <i class="fa fa-table"></i></a>
            </div>
        </f:form>

        <f:form  id="GMFEN" method="post" action="${createLink(action: "generaFileGMFGEN", params:[_csrf: request._csrf.token])}">
           <div class="col-xs-6 col-md-12 marginiBottLista">
               <label class="testoParagraph control-label">Selezionare il mese e  l'anno per generare il report GMFEN</label>
           </div>
           <div class="col-xs-6 col-md-2 bordertbGMFEN borderlGMFEN">
               <label class="testoParagraph control-label">Mese</label>
               <select class="form-controlI testoParagraph" style="margin-bottom:10px;" name="mese" id="mese">
                   <option <g:if test="${new Date().getMonth()==0}"> selected </g:if>  value="0" selected >Gennaio</option>
                   <option <g:if test="${new Date().getMonth()==1}"> selected </g:if> value="1" >Febbraio</option>
                   <option <g:if test="${new Date().getMonth()==2}"> selected </g:if> value="2" >Marzo</option>
                   <option <g:if test="${new Date().getMonth()==3}"> selected </g:if> value="3" >Aprile</option>
                   <option <g:if test="${new Date().getMonth()==4}"> selected </g:if> value="4" >Maggio</option>
                   <option <g:if test="${new Date().getMonth()==5}"> selected </g:if> value="5" >Giugno</option>
                   <option <g:if test="${new Date().getMonth()==6}"> selected </g:if> value="6" >Luglio</option>
                   <option <g:if test="${new Date().getMonth()==7}"> selected </g:if> value="7" >Agosto</option>
                   <option <g:if test="${new Date().getMonth()==8}"> selected </g:if> value="8" >Settembre</option>
                   <option <g:if test="${new Date().getMonth()==9}"> selected </g:if> value="9" >Ottobre</option>
                   <option <g:if test="${new Date().getMonth()==10}"> selected </g:if> value="10" >Novembre</option>
                   <option <g:if test="${new Date().getMonth()==11}"> selected </g:if> value="11" >Dicembre</option>
               </select>
           </div>
           <div class="col-xs-6 col-md-2 bordertbGMFEN">
               <label class="testoParagraph control-label">Anno</label>
               <select class="form-controlI testoParagraph" style="margin-bottom:10px;" name="anno" id="anno"></select>
           </div>
           <div class="col-md-2 col-xs-3 bordertbGMFEN borderRGMFEN">
               <label class="testoParagraph control-label"></label>
               <button type="submit"  class="btn btn-ricerca pull-right bottoneGMFEN">Estrarre GMFGEN</button>
               %{--<a href=""  class="btn btn-avanti pull-right" name="generaFileGMFGEN" id="generaFileGMFGEN"  title="Estrazione GMFGEN"><b>Estrarre GMFGEN</b></a>
               %{--<a href="${createLink(controller: "polizze", action: "generaFileGMFGEN")}" target="_blank" class="btn btn-ricerca" title="Estrazione GMFGEN"><b>Estrazione GMFGEN</b> <i class="fa fa-table"></i></a>--}%
           </div>
            <div class="col-md-3 col-xs-3" >
                <a href="${createLink(controller: "polizze", action: "invioCertMail", params:[isCash: true])}"  target="" class="btn btn-ricerca" title="IASSICUR"><b>Invio Cert. Cash ai Dealers</b> <i class="fa fa-envelope"></i></a>
            </div>
        </f:form>
       <f:form  id="AUTOIMPORT" method="post" action="${createLink(action: "inviaRiassuntoAUTOIMPORT", params:[_csrf: request._csrf.token])}">
           <div class="col-xs-6 col-md-8 borderLCERT bordertbCERT  borderRCERT">
               <div class="col-xs-6 col-md-12">
                   <label class="testoParagraph control-label">Selezionare il periodo per la generazione del riepilogo AUTOIMPORT:</label>
               </div>
               <div class="col-md-1"><label for="dataGenera1">Da:</label></div>
               <div class="col-xs-3 col-md-3 col-md-pull-1">
                   <input class="form-controlI maiuscola testoParagraph bottoneCERT"  type="text" name="dataGenera1"  id="dataGenera1" value="${new Date().format("dd-MM-yyyy")}" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"
                          data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                          data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"/>
               </div>
               <div class="col-md-1"><label for="dataGenera2">A:</label></div>
               <div class="col-xs-3 col-md-3 col-md-pull-1">
                   <input class="form-controlI maiuscola testoParagraph bottoneCERT"  type="text" name="dataGenera2"  id="dataGenera2" value="${new Date().format("dd-MM-yyyy")}" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"
                          data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                          data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"/>
               </div>
               <div class="col-md-4 col-xs-3">
                   <button type="submit"  class="btn btn-ricerca pull-right bottoneCERT"><b>Genera estrazione AUTOIMPORT</b> <i class="fa fa-file-archive-o"></i></button>
               </div>
           </div>
       </f:form>
       <sec:ifHasRole role="ADMINPRESSTODAY">
           <div class="col-md-3 col-xs-6 btnFinanziate">
               <div class="col-md-3 col-xs-3 ">
                   <a href="#" id="new-exceldealers" class="btn btn-ricerca" title="Carica dealers"><b>Carica dealers</b> <i class="fa fa-upload"></i></a>
               </div>
           </div>
       </sec:ifHasRole>
       %{-- </div>--}%
        <f:form  id="polizzeForm"  method="post" class="form-horizontal" enctype="multipart/form-data">
            <div class="table-responsive col-md-12 col-xs-12">
                <table class="table table-condensed titoli" id="tabellaPolizza">
                    <thead>
                        <tr>
                            <th class="text-left vcenter-column">DEALER</th>
                            <th class="text-left vcenter-column"></th>
                            <th class="text-left ">DATA DECORRENZA</th>
                            <th class="text-left ">DATA INSERIMENTO</th>
                            <th class="text-left">NO. POLIZZA</th>
                            <th class="text-left">COPERTURA</th>
                            <th class="text-left">ASSICURATO</th>
                            <th class="text-left">COD. FISCALE/<br>PARTITA IVA</th>

                            <sec:ifHasRole role="ADMINPRESSTODAY">
                                <th class="text-left vcenter-column">DATA RECESSO</th>
                                <th class="text-left "></th>
                            <th class="text-left">TRACCIATO IASSICUR</th>
                            <th class="text-left">TRACCIATO COMPAGNIA</th>
                            </sec:ifHasRole>
                        </tr>
                    </thead>
                    <tbody>
                    <g:each var="polizza" in="${polizze}">
                    <tr class="polizza">
                        <input type="hidden" name="datDeco${polizza.id}" id="datDeco${polizza.id}" value="${polizza.dataDecorrenza.format("dd-MM-yyyy")}"/>
                        <input type="hidden" name="idPolizzasel${polizza.id}" id="idPolizzasel${polizza.id}" value="${polizza.id}"/>
                        <td class="text-left vcenter-column dealerNome" data-id="${polizza.dealer.id}"  id="dealerNome" name="dealerNome" value="${polizza.dealer.id}"><b>${polizza.dealer.ragioneSocialeD}</b><br>${polizza.dealer.indirizzoD}, ${polizza.dealer.provinciaD}</td>
                        <td class="text-left vcenter-column">%{--<g:if test="${polizza.stato != cashopel.polizze.StatoPolizza.RECEDUTA && polizza.stato!=cashopel.polizze.StatoPolizza.ANNULLATA }">--}%<a href="${createLink(action: "modificaPolizza", id: polizza.id)}"  style="cursor: pointer; font-size: 1.25em;"><i class="fa fa-pencil"></i></a>%{--</g:if>--}%</td>
                        <td class="text-left vcenter-column" id="dataDecorrenza">${polizza.dataDecorrenza?.format("dd/MM/yyyy")} </td>
                        <td class="text-left vcenter-column" id="dateInserimentoPolizza">${polizza.dataInserimento?.format("dd/MM/yyyy")}</td>
                        <td class="text-left vcenter-column">${polizza.noPolizza}</td>
                        <td class="text-left vcenter-column">${polizza.coperturaRichiesta.toString().toUpperCase()}</td>
                        <td class="text-left vcenter-column">${polizza.nome?.toString().toUpperCase()} ${polizza.cognome?.toString().toUpperCase()}</td>
                        <td class="text-left vcenter-column">${polizza.partitaIva.toString().toUpperCase()}</td>

                        <sec:ifHasRole role="ADMINPRESSTODAY">
                        <td class="text-center vcenter-column"><g:if test="${polizza.stato==cashopel.polizze.StatoPolizza.RECEDUTA}"></g:if><g:else><input class="form-controlI recesso" type="text" name="${polizza.id}"  id="dataRecesso${polizza.id}" value="" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"
                                                                                                                                                           data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                                                                                                                                                           data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"/></g:else></td>
                        <td class="text-left vcenter-column"><g:if test="${polizza.stato==cashopel.polizze.StatoPolizza.RECEDUTA}">RECEDUTA</g:if><g:else><button type="submit"  name="${polizza.id}" id="recessobtn${polizza.id}" value="${polizza.id}" class="btn btn-success  recessoBtn" disabled="disabled"> RECEDI </button></g:else></td>
                        <td class="text-center vcenter-column">%{--<g:if test="${polizza.tipoPolizza.toString().toUpperCase()!="CASH"}">--}%<a class="bottoneDoc" href="${createLink(controller: "polizze", action: "flussoIassicur", id: polizza.id)}" target="_blank"  title="Tracciato polizza ${polizza.noPolizza}"><i class="fa fa-file-text-o" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></a>%{--</g:if>--}%</td>
                        <td class="text-center vcenter-column">%{--<g:if test="${polizza.tipoPolizza.toString().toUpperCase()!="CASH"}">--}%<a class="bottoneDoc" href="${createLink(controller: "polizze", action: "flussoDirectLine", id: polizza.id)}" target="_blank"  title="Tracciato polizza ${polizza.noPolizza}"><i class="fa fa-file-text-o" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></a>%{--</g:if>--}%</td>
                        </sec:ifHasRole>
                    </tr>
                    </g:each>
                    </tbody>
                    <tfoot>
                    <tr>
                        <th colspan="18" class="text-center"><b>Sono state trovate ${polizze?.totalCount ?: 0} polizze</b></th>
                    </tr>
                    </tfoot>
                </table>
            </div>
        </f:form>

            <nav aria-label="Page navigation">
                <ul class="pagination pull-right">
                    <li>
                        <link:list page="1" controller="${controllerName}">
                            <span aria-hidden="true">&laquo;</span>
                        </link:list>
                    </li>
                    <g:each var="p" in="${pages}">
                        <g:if test="${p == page}"><li class="active"></g:if><g:else><li></g:else>
                        <g:if test="${params.search}"><link:list page="${p}" controller="${controllerName}" search="${params.search}">${p}</link:list></g:if>
                        <g:else><link:list page="${p}" controller="${controllerName}">${p}</link:list></g:else>
                        </li>
                    </g:each>
                    <li>
                        <link:list page="${totalPages}" controller="${controllerName}">
                            <span aria-hidden="true">&raquo;</span>
                        </link:list>
                    </li>
                </ul>
            </nav>

            </div>
    <div class="modal" id="form-pratica">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center titoli"> Caricamento Polizze DSLIP</h3>
                    </div>
                    <div class="modal-body" style="background: white">
                        <form action="${createLink(action: "caricaPraticaDSLIP", params:[_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-idExcelPDSLIP" type="file" name="excelPDSLIP" class="file form-controlI" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn btn-ricerca pull-right">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
    <div class="modal" id="form-exceldealer">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center titoli"> Caricamento dealer</h3>
                    </div>
                    <div class="modal-body" style="background: white">
                        <form action="${createLink( controller:"polizze", action: "caricaDealers", params:[_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-dealer" type="file" name="dealers" class="file form-controlI" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn btn-ricerca pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">

    //Ladda.bind( 'button[type=submit]' );


    $("#new-pDSLIP").click(function() { $("#form-pratica").modal("show"); });
    $("#new-exceldealers").click(function() { $("#form-exceldealer").modal("show"); });
    $(document).ready(function() {
        window.setTimeout(function() {
            $(".alert").fadeTo(1500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 5000);

        var cur_year=new Date().getFullYear();
        var obj=document.getElementById("anno");
        for (var i = 2016; i <= 2030; i++)     {
            opt = document.createElement("option");
            opt.value = i;
            opt.text=i;
            obj.appendChild(opt);
        }
        document.getElementById("anno").value = cur_year;

    });
    $('.recesso').on("changeDate", function(event) {
        var data1 = $.trim($(this).val());
        var nome=this.name;
        if(data1 != ''){
            var dataRec = data1.split('-');
            var dataDeco = document.getElementById('datDeco' + nome).value;
            var splitDataDeco=dataDeco.split('-');
            var datRec= new Date(parseInt(dataRec[2], 10), parseInt(dataRec[1], 10)-1, parseInt(dataRec[0], 10));
            var dataDecoFr= new Date(parseInt(splitDataDeco[2], 10), parseInt(splitDataDeco[1], 10)-1, parseInt(splitDataDeco[0], 10));
            var diffdates=Math.round((datRec-dataDecoFr)/(1000*60*60*24));
            if(diffdates>=0 && diffdates<=14){
                $( ".labelErrore " ).remove();
                if(document.getElementById('recessobtn' + nome)){
                    document.getElementById('recessobtn' + nome).removeAttribute("disabled");
                }
            }else if(diffdates<=-1){
                var errStr="la data di recesso non puo essere inferiore alla data di decorrenza: "+dataDeco
                $( ".labelErrore " ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"><p>"+errStr+"</p></div>" );
                if(document.getElementById('recessobtn' + nome)){
                    document.getElementById('recessobtn' + nome).setAttribute("disabled","disabled");
                }
            }else{
                var errStr="la data di recesso e' superiore a 14 gg dalla data di decorrenza: "+dataDeco
                $( ".labelErrore " ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"><p>"+errStr+"</p></div>" );
                if(document.getElementById('recessobtn' + nome)){
                    document.getElementById('recessobtn' + nome).setAttribute("disabled","disabled");
                }
            }
        }else{
            //$("#linkModulo").addClass("not-active");
            //removeParameters();
        }
        $('.datepicker').hide();
    });
    $('.recessoBtn').click(function(e) {
        e.preventDefault();
        $( ".labelErrore" ).remove();
        var nome=this.name;
        var dataRecesso= document.getElementById('dataRecesso' + nome).value;
        if(dataRecesso != null){
            recedi(nome);
        }
    });
    function recedi(idPolizza){
        var codOperazione="S";
        $.post("${createLink(controller: "polizze", action: "recedePolizza")}",{ idPolizza:idPolizza,codOperazione:codOperazione, _csrf: "${request._csrf.token}"}, function(response) {
            var risposta=response.risposta;
            if(risposta==false){
                var errore=response.errore;
                $( ".labelErrore " ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"><p>"+errore+"</p></div>" );
            }else if(risposta==true){
                $( ".labelErrore " ).remove();
                /*$( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"><p>"+'Polizza recesa'+"</p></div>" );*/
                location.reload();

            }
        }, "json");

    };
    $('.btnexcel').click(function(e) {
        e.preventDefault();
        $( ".labelErrore" ).remove();
        var nome=this.name;
        var dataGenera1= document.getElementById('dataGenera1').value;
        var dataGenera2= document.getElementById('dataGenera2').value;
        if(dataGenera1 != null && dataGenera2!=null){
            excel();
        }
    });
    function excel(){
        var dataGenera1= document.getElementById('dataGenera1').value;
        var dataGenera2= document.getElementById('dataGenera2').value;
        $.post("${createLink(controller: "polizze", action: "generaFilePolizzeAutoimport")}",{ dataGenera1:dataGenera1,dataGenera2:dataGenera2, _csrf: "${request._csrf.token}"}, function(response) {
            var risposta=response.risposta;
            if(risposta==false){
                var errore=response.errore;
                $( ".labelErrore " ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"><p>"+errore+"</p></div>" );
            }else if(risposta==true){
                $( ".labelErrore " ).remove();
                /*$( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"><p>"+'Polizza recesa'+"</p></div>" );*/
                location.reload();

            }
        }, "json");

    };

</script>
</body>
</html>