<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="navigazioneAnnulla"/>
    <title>Elenco</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
<div class="row">
    <div class="col-md-12 col-xs-10 marginesotto">
        <div class="inner  marginesotto">  </div>
        <g:if test="${flash.message}"><div class="labelErrore marginesotto">${flash.message.toString().replaceAll("\\<.*?>","")} </div></g:if>
        <g:if test="${flash.error}"><div class="labelErrore marginesotto">${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
       <div class="col-md-12 col-md-push-1" style="margin-top: 55px;">
            <div class="col-md-6">
            </div>
            <div class="col-md-6 col-md-push-2">
                <g:link action="recederePolizze"><div class="btn btn-cerca"><b>RECEDE POLIZZE</b></div></g:link>
                %{--<a href="${createLink(action: "annullaCashFinLeasing")}" class="btn btn-ricerca"><b>ANNULLAMENTI CASH/FINANZIATE/LEASING</b></a>--}%
            </div>
        </div>
        <f:form  id="listAnnullaform" method="post">
            <div class="col-md-12">
                <div class="col-md-8">
                    <label class="label-Annulla control-label">Ricerca per:</label>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12 col-xs-6 " >
                        <div class="col-md-12 marginesotto marginesopra">
                            <div class="col-md-4 col-md-pull-1">
                                <label class="control-label">numero polizza:</label>
                            </div>
                            <div class="col-md-6 col-md-pull-2">
                                <input type="text" class="form-controlA" placeholder="inserisce no. di polizza" name="searchNo" id="searchNo" value="${params.searchNo}">
                            </div>
                        </div>
                        %{--<div class="col-md-12 marginesotto marginesopra">
                            <div class="col-md-3 col-md-pull-1">
                                <label class="control-label">cognome cliente:</label>
                            </div>
                            <div class="col-md-9 col-md-pull-1">
                                <input type="text" class="form-controlA" placeholder="inserisce il cognome cliente o la ragione sociale" name="searchCognome" id="searchCognome"  value="${params.searchCognome}">
                            </div>
                        </div>--}%
                        <div class="col-md-12 col-md-push-2 marginesotto marginesopra marginiAvanti">
                            <button type="submit" name="button_search" id="button_search" class="btn btn-cerca" <g:if test="${params.button_search}"></g:if><g:else></g:else> onchange="window.location.href = window.location.href.substr(0,window.location.href.lastIndexOf('?'));">Trova</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="col-md-4 col-xs-3 col-md-pull-3 btnFinanziate">
                        <a href="${createLink(controller: "polizze", action: "elencoFlussiIassicurAnnulla")}" target="" class="btn btn-ricerca" title="Estrazione flussi IASSICUR ANNULLATI"><b>Elenco flussi IAssicur polizze annullate</b> <i class="fa fa-download"></i></a>
                    </div>
                    <div class="col-md-4 col-xs-3 col-md-push-1 btnFinanziate" >
                        <a href="${createLink(controller: "polizze", action: "elencoFlussiDLAnnulla")}" target="" class="btn btn-ricerca" title="Estrazione Flussi DL ANNULLATI"><b>Elenco flussi DL polizze annullate</b> <i class="fa fa-download"></i></a>
                    </div>
                </div>
            </div>

            <div class="table-responsive col-md-12 col-xs-12">
                <table class="table table-condensed titoli" id="tabellaPolizza">
                    <thead>
                    <tr>
                    <th class="text-left">NO. POLIZZA</th>
                    <th class="text-left">NOME</th>
                    <th class="text-left">TIPO CONTRATTO</th>
                    <th class="text-left ">DATA DECORRENZA</th>
                    <th class="text-left ">DATA SCADENZA</th>
                    <th class="text-left ">STATO</th>
                    %{--<th class="text-left ">TIPO ANNULLAMENTO</th>--}%
                    <th class="text-left">DATA ANNULLAMENTO</th>
                    <th class="text-left"></th>
                    <th class="text-left">RIMBORSO</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="polizza" in="${polizze}">
                        <tr class="polizza">
                            <input type="hidden" name="idPolizza${polizza.noPolizza}" id="idPolizza${polizza.noPolizza}" value="<g:if test="${rinnovi}"></g:if><g:else>${polizza.id}</g:else>"/>
                            <input type="hidden" name="valoreAssicurato${polizza.noPolizza}" id="valoreAssicurato${polizza.noPolizza}" value="${polizza.valoreAssicurato}"/>
                            <input type="hidden" name="provincia" id="provincia${polizza.noPolizza}" value="${polizza.provincia}"/>
                            <input type="hidden" name="localita" id="localita${polizza.noPolizza}" value="${polizza.localita}"/>
                            <input type="hidden" name="cap" id="cap${polizza.noPolizza}" value="${polizza.cap}"/>
                            <input type="hidden" name="indirizzo" id="indirizzo${polizza.noPolizza}" value="${polizza.indirizzo}"/>
                            <input type="hidden" name="durata" id="durata${polizza.noPolizza}" value="${polizza.durata}"/>
                            <input type="hidden" name="onStar" id="onStar${polizza.noPolizza}" value="<g:if test="${rinnovi}"><g:if test="${polizza.antifurto.toString().trim().isEmpty()}">N</g:if><g:else>S</g:else></g:if><g:else>${polizza.onStar}</g:else>"/>
                            <input type="hidden" name="rinnovi" id="rinnovi${polizza.noPolizza}" value="${rinnovi}"/>
                            <input type="hidden" name="dealerType" id="dealerType${polizza.noPolizza}" value="<g:if test="${rinnovi}"></g:if><g:else>${polizza.dealer.dealerType}</g:else>"/>
                            <input type="hidden" name="step" id="step${polizza.noPolizza}" value="<g:if test="${rinnovi}">0</g:if><g:else>${polizza.step}</g:else>"/>
                            <input type="hidden" name="percVenditore" id="percVenditore${polizza.noPolizza}" value="<g:if test="${rinnovi}"></g:if><g:else>${polizza.dealer.percVenditore}</g:else>"/>
                            <input type="hidden" name="premioImponibile" id="premioImponibile${polizza.noPolizza}" value=""/>
                            <input type="hidden" name="premioLordo" id="premioLordo${polizza.noPolizza}" value=""/>
                            <input type="hidden" name="provvDealer" id="provvDealer${polizza.noPolizza}" value=""/>
                            <input type="hidden" name="provvGmfi" id="provvGmfi${polizza.noPolizza}" value=""/>
                            <input type="hidden" name="provvMansutti" id="provvMansutti${polizza.noPolizza}" value=""/>
                            <input type="hidden" name="provvMach1" id="provvMach1${polizza.noPolizza}" value=""/>
                            <input type="hidden" name="provvVenditore" id="provvVenditore${polizza.noPolizza}" value=""/>
                            <input type="hidden" name="codiceZonaTerritoriale" id="codiceZonaTerritoriale${polizza.noPolizza}" value=""/>
                            <input type="hidden" name="codPacchetto" id="codPacchetto${polizza.noPolizza}" value="<g:if test="${rinnovi}">${polizza.codPacchetto}</g:if><g:else></g:else>"/>
                            <input type="hidden" name="rappel" id="rappel${polizza.noPolizza}" value=""/>
                            <input type="hidden" name="imposte" id="imposte${polizza.noPolizza}" value=""/>
                            <input type="hidden" name="cFiscale" id="cFiscale${polizza.noPolizza}" value="<g:if test="${rinnovi}">${polizza.cFiscale}</g:if><g:else>${polizza.partitaIva}</g:else>"/>
                            <input type="hidden" name="targa" id="targa${polizza.noPolizza}" value="${polizza.targa}"/>
                            <input type="hidden" name="telaio" id="telaio${polizza.noPolizza}" value="${polizza.telaio}"/>
                            <input type="hidden" name="modello" id="modello${polizza.noPolizza}" value="${polizza.modello}"/>
                            <input type="hidden" name="marca" id="marca${polizza.noPolizza}" value="${polizza.marca}"/>
                            <g:if test="${rinnovi}">
                                <input type="hidden" name="ramo" id="ramo${polizza.noPolizza}" value="${polizza.ramo}"/>
                            </g:if>
                            <td class="text-left vcenter-column" id="noPolizza${polizza.noPolizza}">${polizza.noPolizza}</td>
                            <td class="text-left vcenter-column" id="cliente${polizza.noPolizza}"><g:if test="${rinnovi}">${polizza.cliente.toString().toUpperCase()}</g:if><g:else>${polizza.nome} ${polizza.cognome}</g:else></td>
                            <td class="text-left vcenter-column" id="coperturaRichiesta${polizza.noPolizza}"><g:if test="${rinnovi}">${polizza.copertura.toString().toUpperCase()}</g:if><g:else>${polizza.coperturaRichiesta} </g:else></td>
                            <td class="text-left vcenter-column" id="datDeco${polizza.noPolizza}"><g:if test="${rinnovi}">${polizza.dataDecorrenza}</g:if><g:else>${polizza.dataDecorrenza.format("dd/MM/yyyy")}</g:else></td>
                            <td class="text-left vcenter-column" id="dataScadenza${polizza.noPolizza}"><g:if test="${rinnovi}">${polizza.dataScadenza}</g:if><g:else>${polizza.dataScadenza.format("dd/MM/yyyy")}</g:else></td>
                            <td class="text-left vcenter-column" id="stato${polizza.noPolizza}"><g:if test="${polizza.stato.toString().toUpperCase()=="POLIZZA IN ATTESA DI ATTIVAZIONE"}">IN ATTESA DI ATTIVAZIONE</g:if><g:else>${polizza.stato.toString().toUpperCase()}</g:else> </td>
                            %{--nuova implementazione tipi di annullamento--}%
                            %{--<td class="text-left vcenter-column" id="tipo${polizza.noPolizza}"><g:if test="${polizza.stato.toString().toUpperCase()=="ANNULLATA"}">ANNULLATA</g:if>
                            <g:else>
                                <select class="form-controlS testoParagraph" name="tipoOper${polizza.noPolizza}" id="tipoOper${polizza.noPolizza}">
                                    <option value="E" <g:if test="${polizza.codOperazione=="E"}">selected</g:if> >&nbsp;Annullamento&nbsp;</option>
                                    <option value="F" <g:if test="${polizza.codOperazione=="F"}">selected</g:if> >&nbsp;Furto totale&nbsp;</option>
                                    <option value="G" <g:if test="${polizza.codOperazione=="G"}">selected</g:if> >&nbsp;Danno totale&nbsp;</option>
                                    <option value="V" <g:if test="${polizza.codOperazione=="V"}">selected</g:if> >&nbsp;Variazione&nbsp;</option>
                                </select>
                            </g:else>
                            </td>--}%
                            <td class="text-center vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()=="ANNULLATA"}">ANNULLATA</g:if><g:else><input class="form-controlI storno" type="text" name="${polizza.noPolizza}"  id="dataStorno${polizza.noPolizza}" value="" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"
                                                                                                                                                                      data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                                                                                                                                                                      data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"/></g:else></td>
                            <td class="text-left vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()=="ANNULLATA"}"></g:if><g:else><a href="" %{--onclick="return chiamataWS();"--}% class="btn btn-avanti pull-right chiamaWs" name="${polizza.noPolizza}" id="chiamataWS${polizza.noPolizza}"  title="chiamata web service" disabled="true"><b>CHIAMATA WEB SERVICE</b></a></g:else></td>
                            <td class="text-left vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()=="ANNULLATA"}"></g:if><g:else><input type="text" id="importoRimborso${polizza.noPolizza}" name="importoRimborso${polizza.noPolizza}" class="form-controlA testoParagraph importo" readonly="true"/></g:else></td>
                            <td class="text-left vcenter-column"><g:if test="${polizza.stato.toString().toUpperCase()=="ANNULLATA"}"></g:if><g:else><button type="submit" id="annulla${polizza.noPolizza}" name="${polizza.noPolizza}" class="btn btn-success annullamento" disabled="disabled"> ANNULLA </button></g:else></td>
                        </tr>
                    </g:each>

                    </tbody>
                    %{--<tfoot>
                    <tr>
                        <th colspan="18" class="text-center"><b>Sono state trovate ${polizze?.totalCount ?: 0} polizze</b></th>
                    </tr>
                    </tfoot>--}%
                </table>
            </div>
        </f:form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function() {
        window.setTimeout(function() {
            $(".alert").fadeTo(1500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 5000);
    });
    window.onload = function() {
        var txt = document.getElementById('errori');
        var txtPra = document.getElementById('pratiche');
        //txt.value = window.onload + '';

        if(txt != null){
            var array = txt.value.toString().split(',').join("\r\n");
            if(document.getElementById('link') != null){
                document.getElementById('link').onclick = function(code) {
                    this.href = 'data:text/plain;charset=utf-8,'
                            + encodeURIComponent(array);
                };
            }

        }
        if(txtPra !=null){
            var arrayP = txtPra.value.toString().split(',').join("\r\n");
            if(document.getElementById('linkP') != null){
                document.getElementById('linkP').onclick = function(code) {
                    this.href = 'data:text/plain;charset=utf-8,'
                            + encodeURIComponent(arrayP);
                };
            }
        }

    };
    $("#searchCognome").on("change",function(event){
        var valore= $.trim($("#searchCognome").val());
        if(valore!=''){
            $("#button_search").prop('disabled', false);
            $("#button_search").val(true);
        }
    });
    $("#searchNo").on("change",function(event){
        var valore= $.trim($("#searchNo").val());
        if(valore!=''){
            $("#button_search").prop('disabled', false);
            $("#button_search").val(true);
        }
    });
    $('.storno').on("changeDate", function(event) {
        var data1 = $.trim($(this).val());
        var nome=this.name;
        if(data1 != ''){
            if(document.getElementById('chiamataWS' + nome)){
                document.getElementById('chiamataWS' + nome).removeAttribute("disabled");
            }
        }else{
            //$("#linkModulo").addClass("not-active");
            //removeParameters();
        }
        $('.datepicker').hide();
    });
    $('.chiamaWs').click(function(e) {
        e.preventDefault();
        $( ".labelErrore" ).remove();
        var nome=this.name;
        var disabledA=this.getAttribute("disabled");
        var dataStorno= document.getElementById('dataStorno' + nome).value;
        //alert(disabledA);
        if((dataStorno != null || dataStorno!='') && (disabledA!="true" && disabledA !="disabled")){
            chiamataWS(nome);
        }else{
            $( ".labelErrore" ).remove();
            $( ".inner" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'>inserire una data di annullamento</span></div>" );
        }
    });
    function chiamataWS(nome){
        var rinnovi=document.getElementById('rinnovi' + nome).value;
        var tipoPolizza="CFL"
        var codPacchetto=""
       if(rinnovi=='true'){
           var codPacchetto=document.getElementById('codPacchetto' + nome).value;
           tipoPolizza="RINN"

       }else{

       }
        var valoreAssicurato=document.getElementById('valoreAssicurato' + nome).value;
        var provincia=document.getElementById('provincia' + nome).value;
        var comune=document.getElementById('localita' + nome).value;
        var durata=document.getElementById('durata' + nome).value;
        var onStar=document.getElementById('onStar' + nome).value;
        var dealerType=document.getElementById('dealerType' + nome).value;
        var step=document.getElementById('step' + nome).value;
        var percVenditore=document.getElementById('percVenditore' + nome).value;
        var coperturacodifica=document.getElementById('coperturaRichiesta' + nome).innerText;
        var copertura=""
        if(coperturacodifica=== "SILVER"){
            copertura="silver"
        }else if(coperturacodifica==="GOLD"){
            copertura="gold"
        }else if(coperturacodifica==="PLATINUM"){
            copertura="platinum"
        }else if(coperturacodifica.toString()==="PLATINUM KASKO"){
            copertura="platinumK"
        }else if(coperturacodifica.toString()==="PLATINUM COLLISIONE"){
            copertura="platinumC"
        }
        var dataDeco = document.getElementById('datDeco' + nome).innerText.split('/');
        var dataDecorrenza=dataDeco[2]+'-'+dataDeco[1]+'-'+dataDeco[0];
        /**nuova implementazione tipi annullamento*/
        //var tipoOperazione=document.getElementById('tipoOper' + nome).value;
        var tipoOperazione="E";
        var dataScad = document.getElementById('dataScadenza' + nome).innerText.split('/');
        var dataScadenza=dataScad[2]+'-'+dataScad[1]+'-'+dataScad[0];
        // var dataStorno=$("#dataStorno").val();
        var dataSt= document.getElementById('dataStorno' + nome).value.split('-');
        var dataStorno= dataSt[2]+'-'+dataSt[1]+'-'+dataSt[0];
        $.post("${createLink(controller: "polizze", action: "chiamataWS")}",{tipoPolizza:tipoPolizza,codPacchetto:codPacchetto, copertura:copertura, valoreAssicurato:valoreAssicurato, provincia:provincia, comune:comune, durata:durata,onStar:onStar,dealerType:dealerType,step:step,percVenditore:percVenditore,dataDecorrenza:dataDecorrenza,operazione:tipoOperazione,dataScadenza:dataScadenza,dataStorno:dataStorno, _csrf: "${request._csrf.token}"}, function(response) {
            var risposta=response.risposta;
            if(risposta==false){
                var errore=response.errore;
                var provvDealer=Math.round(response.provvDealer * 100) / 100
                var provvGmfi=Math.round(response.provvgmfi * 100) / 100
                var provvMansutti=Math.round(response.provvMansutti * 100) / 100
                var provvMach1=Math.round(response.provvMach1 * 100) / 100

                $( ".labelErrore" ).remove();
                //$( ".inner" ).append( "<p>"+errore+"</p>" );
                $( ".inner" ).after( "<div class=\"labelErrore marginesotto\"><p>"+response.errore+"</p></div>" );
                document.getElementById('premioImponibile' + nome).value=response.premioImponibile;
                document.getElementById('premioLordo' + nome).value=response.premioLordo;
                document.getElementById('provvDealer' + nome).value=provvDealer;
                document.getElementById('provvGmfi' + nome).value=provvGmfi;
                document.getElementById('provvMansutti' + nome).value=provvMansutti;
                document.getElementById('provvMach1' + nome).value=provvMach1;
                document.getElementById('provvVenditore' + nome).value=response.provvvend;
                document.getElementById('codiceZonaTerritoriale' + nome).value=response.codiceZonaTerritoriale;
                document.getElementById('rappel' + nome).value=response.rappel;
                document.getElementById('imposte' + nome).value=response.imposte;
            }else{
                var provvDealer=Math.round(response.provvDealer * 100) / 100
                var provvGmfi=Math.round(response.provvgmfi * 100) / 100
                var provvMansutti=Math.round(response.provvMansutti * 100) / 100
                var provvMach1=Math.round(response.provvMach1 * 100) / 100
                $( ".labelErrore" ).remove();
                $( ".inner" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'>I dati sono stati caricati dal webservice</span></div>" );
                document.getElementById('importoRimborso' + nome).value=response.premioImponibile;
                document.getElementById('premioImponibile' + nome).value=response.premioImponibile;
                document.getElementById('premioLordo' + nome).value=response.premioLordo;
                document.getElementById('provvDealer' + nome).value=provvDealer;
                document.getElementById('provvGmfi' + nome).value=provvGmfi;
                document.getElementById('provvMansutti' + nome).value=provvMansutti;
                document.getElementById('provvMach1' + nome).value=provvMach1;
                document.getElementById('provvVenditore' + nome).value=response.provvVend;
                document.getElementById('codiceZonaTerritoriale' + nome).value=response.codiceZonaTerritoriale;
                document.getElementById('rappel' + nome).value=response.rappel;
                document.getElementById('imposte' + nome).value=response.imposte;
                if(document.getElementById('annulla' + nome)){
                    document.getElementById('annulla' + nome).removeAttribute("disabled");
                }
            }
        }, "json");
    }

    $('.annullamento').click(function(e) {
        e.preventDefault();
        $( ".labelErrore" ).remove();
        var nome=this.name;
        var importoR= document.getElementById('importoRimborso' + nome).value;
        if(importoR != null){
            annullamentoPolizza(nome);
        }
    });
    function annullamentoPolizza(nome){
        var rinnovi=document.getElementById('rinnovi' + nome).value;
        var tipoPolizza="CFL";
        var idPolizza=null;
        if(rinnovi=='true'){
            var codPacchetto=document.getElementById('codPacchetto' + nome).value;
            tipoPolizza="RINN";
            var ramo=document.getElementById('ramo' + nome).value;
        }else{
            idPolizza=document.getElementById('idPolizza' + nome).value;
            var ramo="";
        }
        var valoreAssicurato=document.getElementById('valoreAssicurato' + nome).value;
        var provincia=document.getElementById('provincia' + nome).value;
        var cap=document.getElementById('cap' + nome).value;
        var indirizzo=document.getElementById('indirizzo' + nome).value;
        var localita=document.getElementById('localita' + nome).value;
        var durata=document.getElementById('durata' + nome).value;
        var onStar=document.getElementById('onStar' + nome).value;
        var coperturaRichiesta=document.getElementById('coperturaRichiesta' + nome).innerText;
        var codPacchetto=document.getElementById('codPacchetto' + nome).value;
        var codOperazione="E";
        var dataDeco = document.getElementById('datDeco' + nome).innerText.split('/');
        var dataDecorrenza=dataDeco[2]+'-'+dataDeco[1]+'-'+dataDeco[0];
        var dataScad = document.getElementById('dataScadenza' + nome).innerText.split('/');
        var dataScadenza=dataScad[2]+'-'+dataScad[1]+'-'+dataScad[0];
        var premioImponibile=document.getElementById('premioImponibile' + nome).value;
        var premioLordo=document.getElementById('premioLordo' + nome).value;
        var provvDealer=document.getElementById('provvDealer' + nome).value;
        var provvGmfi=document.getElementById('provvGmfi' + nome).value;
        var provvMansutti=document.getElementById('provvMansutti' + nome).value;
        var provvMach1=document.getElementById('provvMach1' + nome).value;
        var provvVenditore=document.getElementById('provvVenditore' + nome).value;
        var rappel=document.getElementById('rappel' + nome).value;
        var imposte=document.getElementById('imposte' + nome).value;
        var noPolizza=document.getElementById('noPolizza' + nome).innerText;
        var targa=document.getElementById('targa' + nome).value;
        var telaio=document.getElementById('telaio' + nome).value;
        var modello=document.getElementById('modello' + nome).value;
        var marca=document.getElementById('marca' + nome).value;
        var cFiscale=document.getElementById('cFiscale' + nome).value;
        var cliente=document.getElementById('cliente' + nome).innerText;
        var dataSt= document.getElementById('dataStorno' + nome).value.split('-');
        var dataStorno= dataSt[2]+'-'+dataSt[1]+'-'+dataSt[0];
        $.post("${createLink(controller: "polizze", action: "recedePolizza")}",{tipoPolizza:tipoPolizza,durata:durata, provvMansutti:provvMansutti, provvMach1:provvMach1,idPolizza:idPolizza, dataScadenza:dataScadenza, dataDecorrenza:dataDecorrenza, codPacchetto:codPacchetto, cap:cap, indirizzo:indirizzo,valoreAssicurato:valoreAssicurato,coperturaRichiesta:coperturaRichiesta, cliente:cliente,dataStorno:dataStorno, noPolizza:noPolizza, codOperazione:codOperazione, premioImponibile:premioImponibile, premioLordo:premioLordo, provvDealer:provvDealer, provvGmfi:provvGmfi, provvVenditore:provvVenditore,rappel:rappel,imposte:imposte,targa:targa,telaio:telaio,modello:modello,marca:marca,cFiscale:cFiscale, provincia:provincia, localita:localita, ramo:ramo, _csrf: "${request._csrf.token}"}, function(response) {
            var risposta=response.risposta;
            if(risposta==false){
                var errore=response.errore;
                $( ".labelErrore" ).remove();
                //$( ".inner" ).append( "<p>"+errore+"</p>" );
                $( ".inner" ).after( "<div class=\"labelErrore marginesotto\"><p>"+response.errore+"</p></div>" );
            }else{
                $( ".labelErrore" ).remove();
                    location.reload();
                if(document.getElementById('annulla' + nome)){
                    document.getElementById('annulla' + nome).setAttribute("disabled","disabled");
                }
                if(document.getElementById('chiamataWS' + nome)){
                    document.getElementById('chiamataWS' + nome).setAttribute("disabled","disabled");
                }
                if(document.getElementById('dataStorno' + nome)){
                    document.getElementById('dataStorno' + nome).setAttribute("disabled","disabled");
                }
            }
        }, "json");
    }

</script>
</body>
</html>