<html>
<head>
    <meta name="layout" content="navigazioneMenu">
    <title>Menu</title>

</head>
<body>
<div class="row">
    <div class="col-xs-10 col-md-12">
        <div class="login-panel" style="margin-top: 100px;">
            <div class="row">
                <sec:ifHasRole role="DEALER">
                    <div class="col-md-10 col-md-push-5 col-xs-6 marginiBottLista">
                        <a href="${createLink(controller: "polizzeRCA", action: "listaRCA")}" class="btn btn-menu"><b>POLIZZE RCA</b></a>
                    </div>
                </sec:ifHasRole>
                <sec:ifHasNotRole role="DEALER">
                    <div class="col-md-6 col-md-push-2 col-xs-6 marginiBottLista">
                        <a href="${createLink(controller:"polizze" , action: "lista")}" class="btn btn-menu"> <b>POLIZZE CVT</b></a>
                    </div>
                    <div class="col-md-6 col-md-push-1 col-xs-6 marginiBottLista">
                        <a href="${createLink(controller: "polizzeRCA", action: "listaRCA")}" class="btn btn-menu"><b>POLIZZE RCA</b></a>
                    </div>
                </sec:ifHasNotRole>
                <div class="col-md-12 col-xs-12" style="margin-top: 50px;">
                    <footer class="col-md-10 col-md-push-0 col-xs-10 col-xs-push-1" >
                        <div class="col-md-10 col-md-push-2 col-xs-10 col-xs-push-1">
                            <p class="text-center"><asset:image src="/loghi/mach1.png" height="50px" style="margin-top:200px; margin-left:80px;" /></p>
                        </div>
                        <div  class="col-md-12 col-md-push-1 col-xs-12" style="margin-left: 30px;">
                            <p class="testofooter" align="center"> Mach1 s.r.l. - Via Vittor Pisani, 13/B  – 20124 Milano - TEL. 02 30465068 FAX 02 62694254 - Email: attivazioni.ofs@mach-1.it - CCIAA Milano - REA MI 1908726 - C.F.,  P. IVA e Reg. Imprese Milano 06680830962 - Capitale sociale 100.000 EURO - Registro Unico Intermediari  A000317603 - Banco Posta Sede di Milano, Piazza Cordusio, 1 - IBAN IT 54 C 07601 01600 0000 99701393</p>
                        </div>
                    </footer>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>