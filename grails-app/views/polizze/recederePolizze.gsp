<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="navigazioneRecesso"/>
    <title>Elenco</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
<div class="row">
    <div class="col-md-12 col-xs-10">
        <g:if test="${flash.error}"><div class="labelErrore marginesotto">${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
        <g:if test="${flash.message}"><div class="labelErrore marginesotto">${flash.message.toString().replaceAll("\\<.*?>","")} </div></g:if>
        <div class="inner  marginesotto">  </div>
        <div class="datiPoli">
        </div>
        <f:form  id="listAnnullaform" method="post">
            <div class="col-md-12">
                <div class="col-md-8">
                    <label class="label-Annulla control-label">Ricerca per:</label>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12 col-xs-6 " >
                        <div class="col-md-12 marginesotto marginesopra">
                            <div class="col-md-4 col-md-pull-1">
                                <label class="control-label">numero polizza:</label>
                            </div>
                            <div class="col-md-6 col-md-pull-2">
                                <input type="text" class="form-controlA" placeholder="inserisce no. di Polizza" name="searchNo" id="searchNo" value="${params.searchNo}">
                            </div>
                        </div>
                        %{--<div class="col-md-12 marginesotto marginesopra">
                            <div class="col-md-3 col-md-pull-1">
                                <label class="control-label">cognome cliente:</label>
                            </div>
                            <div class="col-md-9 col-md-pull-1">
                                <input type="text" class="form-controlA" placeholder="inserisce il cognome cliente o la ragione sociale" name="searchCognome" id="searchCognome"  value="${params.searchCognome}">
                            </div>
                        </div>--}%
                        <div class="col-md-12 col-md-push-2 marginesotto marginesopra marginiAvanti">
                            <button type="submit" name="button_search" id="button_search" class="btn btn-cerca" <g:if test="${params.button_search}"></g:if><g:else></g:else> onchange="window.location.href = window.location.href.substr(0,window.location.href.lastIndexOf('?'));">Trova</button>
                        </div>
                    </div>
                </div>
                %{--<div class="col-md-6">
                    <div class="col-md-6 col-xs-3 col-xs-pull-3 btnFinanziate">
                        <a href="${createLink(controller: "polizze", action: "elencoFlussiIassicurRecede")}" target="" class="btn btn-ricerca" title="Estrazione flussi IASSICUR RECEDUTI"><b>Elenco flussi IAssicur polizze recedute</b> <i class="fa fa-download"></i></a>
                    </div>
                    <div class="col-md-6 col-xs-3 btnFinanziate" >
                        <a href="${createLink(controller: "polizze", action: "elencoFlussiDLRecede")}" target="" class="btn btn-ricerca" title="Estrazione Flussi DL RECEDUTI"><b>Elenco flussi DL polizze recedute</b> <i class="fa fa-download"></i></a>
                    </div>
                </div>--}%
            </div>

            <div class="table-responsive col-md-12 col-xs-12">
                <table class="table table-condensed titoli" id="tabellaPolizza">
                    <thead>
                    <th class="text-left">NO. POLIZZA</th>
                    <th class="text-left">NOME</th>
                    <th class="text-left">TIPO CONTRATTO</th>
                    <th class="text-left ">DATA DECORRENZA</th>
                    <th class="text-left ">DATA SCADENZA</th>
                    <th class="text-left ">STATO</th>
                    <th class="text-left">DATA RECESSO</th>
                    <th class="text-left"></th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:if test="${polizze}"></g:if>
                    <g:each var="polizza" in="${polizze}">
                        <tr class="polizza">
                            <input type="hidden" name="idPolizza${polizza.noPolizza}" id="idPolizza${polizza.noPolizza}" value="<g:if test="${rinnovi}"></g:if><g:else>${polizza.id}</g:else>"/>
                            <input type="hidden" name="valoreAssicurato${polizza.noPolizza}" id="valoreAssicurato${polizza.noPolizza}" value="${polizza.valoreAssicurato}"/>
                            <input type="hidden" name="provincia" id="provincia${polizza.noPolizza}" value="${polizza.provincia}"/>
                            <input type="hidden" name="localita" id="localita${polizza.noPolizza}" value="${polizza.localita}"/>
                            <input type="hidden" name="cap" id="cap${polizza.noPolizza}" value="${polizza.cap}"/>
                            <input type="hidden" name="indirizzo" id="indirizzo${polizza.noPolizza}" value="${polizza.indirizzo}"/>
                            <input type="hidden" name="durata" id="durata${polizza.noPolizza}" value="${polizza.durata}"/>
                            <input type="hidden" name="onStar" id="onStar${polizza.noPolizza}" value="<g:if test="${rinnovi}"><g:if test="${polizza.antifurto.toString().trim().isEmpty()}">N</g:if><g:else>S</g:else></g:if><g:else>${polizza.onStar}</g:else>"/>
                            <input type="hidden" name="rinnovi" id="rinnovi${polizza.noPolizza}" value="${rinnovi}"/>
                            <input type="hidden" name="step" id="step${polizza.noPolizza}" value="<g:if test="${rinnovi}">0</g:if><g:else>${polizza.step}</g:else>"/>
                            <input type="hidden" name="codPacchetto" id="codPacchetto${polizza.noPolizza}" value="<g:if test="${rinnovi}">${polizza.codPacchetto}</g:if><g:else></g:else>"/>
                            <input type="hidden" name="cFiscale" id="cFiscale${polizza.noPolizza}" value="<g:if test="${rinnovi}">${polizza.cFiscale}</g:if><g:else>${polizza.partitaIva}</g:else>"/>
                            <input type="hidden" name="targa" id="targa${polizza.noPolizza}" value="${polizza.targa}"/>
                            <input type="hidden" name="telaio" id="telaio${polizza.noPolizza}" value="${polizza.telaio}"/>
                            <input type="hidden" name="modello" id="modello${polizza.noPolizza}" value="${polizza.modello}"/>
                            <input type="hidden" name="marca" id="marca${polizza.noPolizza}" value="${polizza.marca}"/>
                            <g:if test="${rinnovi}">
                                <input type="hidden" name="ramo" id="ramo${polizza.noPolizza}" value="${polizza.ramo}"/>
                            </g:if>
                            <td class="text-left vcenter-column" id="noPolizza${polizza.noPolizza}">${polizza.noPolizza}</td>
                            <td class="text-left vcenter-column" id="cliente${polizza.noPolizza}"><g:if test="${rinnovi}">${polizza.cliente.toString().toUpperCase()}</g:if><g:else>${polizza.nome} ${polizza.cognome}</g:else></td>
                            <td class="text-left vcenter-column" id="coperturaRichiesta${polizza.noPolizza}"><g:if test="${rinnovi}">${polizza.copertura.toString().toUpperCase()}</g:if><g:else>${polizza.coperturaRichiesta} </g:else></td>
                            <td class="text-left vcenter-column" id="datDeco${polizza.noPolizza}"><g:if test="${rinnovi}">${polizza.dataDecorrenza}</g:if><g:else>${polizza.dataDecorrenza.format("dd/MM/yyyy")}</g:else></td>
                            <td class="text-left vcenter-column" id="dataScadenza${polizza.noPolizza}"><g:if test="${rinnovi}">${polizza.dataScadenza}</g:if><g:else>${polizza.dataScadenza.format("dd/MM/yyyy")}</g:else></td>
                            <td class="text-left vcenter-column" id="stato${polizza.noPolizza}"><g:if test="${polizza.stato.toString().toUpperCase()=="POLIZZA IN ATTESA DI ATTIVAZIONE"}">IN ATTESA DI ATTIVAZIONE</g:if><g:else>${polizza.stato.toString().toUpperCase()}</g:else> </td>
                            <td class="text-center vcenter-column"><g:if test="${polizza.stato==cashopel.polizze.StatoPolizza.RECEDUTA}"></g:if><g:else><input class="form-controlI recesso" type="text" name="${polizza.noPolizza}"  id="dataRecesso${polizza.noPolizza}" value="" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"
                                                                                                                                                               data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                                                                                                                                                               data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"/></g:else></td>
                            <td class="text-left vcenter-column"><g:if test="${polizza.stato==cashopel.polizze.StatoPolizza.RECEDUTA}">RECEDUTA</g:if><g:else><button type="submit"  name="${polizza.noPolizza}" id="recessobtn${polizza.noPolizza}" value="${polizza.noPolizza}" class="btn btn-success  recessoBtn" disabled="disabled"> RECEDI </button></g:else></td>
                        </tr>
                    </g:each>
                    </tbody>
                </table>
            </div>
        </f:form>

    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        window.setTimeout(function() {
            $(".alert").fadeTo(1500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 5000);
    });
    window.onload = function() {
        var txt = document.getElementById('errori');
        var txtPra = document.getElementById('pratiche');
        //txt.value = window.onload + '';

        if(txt != null){
            var array = txt.value.toString().split(',').join("\r\n");
            if(document.getElementById('link') != null){
                document.getElementById('link').onclick = function(code) {
                    this.href = 'data:text/plain;charset=utf-8,'
                            + encodeURIComponent(array);
                };
            }

        }
        if(txtPra !=null){
            var arrayP = txtPra.value.toString().split(',').join("\r\n");
            if(document.getElementById('linkP') != null){
                document.getElementById('linkP').onclick = function(code) {
                    this.href = 'data:text/plain;charset=utf-8,'
                            + encodeURIComponent(arrayP);
                };
            }
        }

    };
    $("#searchCognome").on("change",function(event){
        var valore= $.trim($("#searchCognome").val());
        if(valore!=''){
            $("#button_search").prop('disabled', false);
            $("#button_search").val(true);
        }
    });
    $("#searchNo").on("change",function(event){
        var valore= $.trim($("#searchNo").val());
        if(valore!=''){
            $("#button_search").prop('disabled', false);
            $("#button_search").val(true);
        }
    });
    $('.recesso').on("changeDate", function(event) {
        var data1 = $.trim($(this).val());
        var nome=this.name;
        if(data1 != ''){
            var dataRec = data1.split('-');
            var dataDeco = document.getElementById('datDeco' + nome).innerText;
            var splitDataDeco=dataDeco.split('/');
            var datRec= new Date(parseInt(dataRec[2], 10), parseInt(dataRec[1], 10)-1, parseInt(dataRec[0], 10));
            var dataDecoFr= new Date(parseInt(splitDataDeco[2], 10), parseInt(splitDataDeco[1], 10)-1, parseInt(splitDataDeco[0], 10));
            var diffdates=Math.round((datRec-dataDecoFr)/(1000*60*60*24));
            if(diffdates>=0 /*&& diffdates<=14*/){
                $( ".labelErrore " ).remove();
                if(document.getElementById('recessobtn' + nome)){
                    document.getElementById('recessobtn' + nome).removeAttribute("disabled");
                }
            }else if(diffdates<=-1){
                var errStr="la data di recesso non puo essere inferiore alla data di decorrenza: "+dataDeco
                $( ".labelErrore " ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"><p>"+errStr+"</p></div>" );
                if(document.getElementById('recessobtn' + nome)){
                    document.getElementById('recessobtn' + nome).setAttribute("disabled","disabled");
                }
            }/*else{
                var errStr="la data di recesso e' superiore a 14 gg dalla data di decorrenza: "+dataDeco
                $( ".labelErrore " ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"><p>"+errStr+"</p></div>" );
                if(document.getElementById('recessobtn' + nome)){
                    document.getElementById('recessobtn' + nome).setAttribute("disabled","disabled");
                }
            }*/
        }else{
            //$("#linkModulo").addClass("not-active");
            //removeParameters();
        }
        $('.datepicker').hide();
    });
    $('.recessoBtn').click(function(e) {
        e.preventDefault();
        $( ".labelErrore" ).remove();
        var nome=this.name;
        var dataRecesso= document.getElementById('dataRecesso' + nome).value;
        if(dataRecesso != null){
            recedi(nome);
        }
    });
    function recedi(noPolizza){
        var rinnovi=document.getElementById('rinnovi' + noPolizza).value;
        var codOperazione="S";
        var tipoPolizza="CFL";
        var idPolizza=null;
        if(rinnovi=='true'){
            tipoPolizza="RINN"
            var valoreAssicurato=document.getElementById('valoreAssicurato' + noPolizza).value;
            var provincia=document.getElementById('provincia' + noPolizza).value;
            var cap=document.getElementById('cap' + noPolizza).value;
            var indirizzo=document.getElementById('indirizzo' + noPolizza).value;
            var localita=document.getElementById('localita' + noPolizza).value;
            var durata=document.getElementById('durata' + noPolizza).value;
            var onStar=document.getElementById('onStar' + noPolizza).value;
            var coperturaRichiesta=document.getElementById('coperturaRichiesta' + noPolizza).innerText;
            var codPacchetto=document.getElementById('codPacchetto' + noPolizza).value;
            var dataDeco = document.getElementById('datDeco' + noPolizza).innerText.split('/');
            var dataDecorrenza=dataDeco[2]+'-'+dataDeco[1]+'-'+dataDeco[0];
            var dataScad = document.getElementById('dataScadenza' + noPolizza).innerText.split('/');
            var dataScadenza=dataScad[2]+'-'+dataScad[1]+'-'+dataScad[0];
            var noPolizza=document.getElementById('noPolizza' + noPolizza).innerText;
            var targa=document.getElementById('targa' + noPolizza).value;
            var telaio=document.getElementById('telaio' + noPolizza).value;
            var modello=document.getElementById('modello' + noPolizza).value;
            var marca=document.getElementById('marca' + noPolizza).value;
            var cFiscale=document.getElementById('cFiscale' + noPolizza).value;
            var cliente=document.getElementById('cliente' + noPolizza).innerText;
            var dataSt= document.getElementById('dataRecesso' + noPolizza).value.split('-');
            var dataStorno= dataSt[2]+'-'+dataSt[1]+'-'+dataSt[0];
            var ramo=document.getElementById('ramo' + noPolizza).value;
        }else{
            idPolizza=document.getElementById('idPolizza' + noPolizza).value;
            var ramo="";
        }
        $.post("${createLink(controller: "polizze", action: "recedePolizza")}",{ tipoPolizza:tipoPolizza,durata:durata, idPolizza:idPolizza, dataScadenza:dataScadenza, dataDecorrenza:dataDecorrenza, codPacchetto:codPacchetto, cap:cap, indirizzo:indirizzo,valoreAssicurato:valoreAssicurato,coperturaRichiesta:coperturaRichiesta, cliente:cliente,dataStorno:dataStorno, noPolizza:noPolizza, codOperazione:codOperazione, targa:targa,telaio:telaio,modello:modello,marca:marca,cFiscale:cFiscale, provincia:provincia, localita:localita,ramo:ramo, _csrf: "${request._csrf.token}"}, function(response) {
            var risposta=response.risposta;
            if(risposta==false){
                var errore=response.errore;
                $( ".labelErrore " ).remove();
                $( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"><p>"+errore+"</p></div>" );
            }else if(risposta==true){
                $( ".labelErrore " ).remove();
                /*$( ".datiPoli" ).after( "<div class=\"labelErrore marginesotto\"><p>"+'Polizza recesa'+"</p></div>" );*/
                location.reload();

            }
        }, "json");

    };
</script>
</body>
</html>