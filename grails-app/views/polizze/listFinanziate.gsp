<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="navigazioneFin"/>
    <title>Elenco</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
<div class="row">
   <div class="col-md-12 col-xs-10">
        <g:if test="${flash.error}"><div class="labelErrore marginesotto">${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
       <g:if test="${flash.flussiPAI}">
           <div class="labelErrore marginesotto">
               <input type="hidden" id="pai" value="${flash.flussiPAI}"/>
               <p>Premere il link in basso per  il riassunto dei flussi PAI caricati:</p>
               <a href="" class="link" id="linkPai" download="flussiPAI.txt">Lista flussi</a><br/>
               %{--<g:each var="messageP" in="${flash.praticheNuove}">
                   <p>${messageP}</p>
               </g:each>--}%
           </div>
       </g:if>
       <g:if test="${flash.praticheNuove}">
           <div class="labelErrore marginesotto">
               <input type="hidden" id="pratiche" value="${flash.praticheNuove}"/>
               <p>Premere il link in basso per il riassunto delle polizze caricate:</p>
               <a href="" class="link" id="linkP" download="pratiche.txt">Lista pratiche</a><br/>
               %{--<g:each var="messageP" in="${flash.praticheNuove}">
                   <p>${messageP}</p>
               </g:each>--}%
           </div>
       </g:if>
       <g:if test="${flash.associazione}">
           <div class="labelErrore marginesotto">${flash.associazione.toString().replaceAll("\\<.*?>","")} </div>
       </g:if>
       <g:if test="${flash.errorePratica}">
            <div class="labelErrore marginesotto">
                <input type="hidden" id="errori" value="${flash.errorePratica}"/>
                <p>Premere il link in basso per il riassunto degli errori:</p>
                <a class="link" href="" id="link" download="errori.txt">Lista errori</a><br/>
                %{--<g:each var="errore" in="${flash.errorePratica}">
                    <p>${errore.toString().replaceAll("\\<.*?>\\[]","")}</p>
                </g:each>--}%
            </div>
        </g:if>

       <g:if test="${flash.certificati}">
           <div class="labelErrore marginesotto">
               <g:each var="messageP" in="${flash.certificati}">
                   <p>${messageP}</p>
               </g:each>
           </div>
       </g:if>
            <f:form  id="listFinanzform" method="post">
                <sec:ifHasRole role="ADMINPRESSTODAY">
                    <div class="col-md-3 col-xs-6 area-ricerca" >
                        <div class="input-group input-ricerca">
                            <input type="text" class="form-controlC" placeholder="ricerca polizze FINANZIATE" name="search" value="${params.search}">
                            <span class="input-group-btn">
                                <button type="submit" id="button_search" class="btn btn-ricerca" onchange="window.location.href = window.location.href.substr(0,window.location.href.lastIndexOf('?'));"><i class="glyphicon glyphicon-search"></i></button>
                            </span>
                        </div>
                    </div>
                    <div class="col-md-4 col-xs-3" >
                        <a href="${createLink(controller: "polizze", action: "elencoFlussiDL")}" target="" class="btn btn-ricerca" title="Estrazione flussi DL"><b>Elenco flussi DL</b> <i class="fa fa-download"></i></a>
                        %{--<a href="${createLink(controller: "polizze", action: "elencoPolizzeSettimanale")}" target="_blank" class="btn btn-ricerca" title="Estrazione polizze settimanale"><b>Riepilogo polizze settimanale</b> <i class="fa fa-download"></i></a>--}%
                    </div>
                    <div class="col-md-4 col-xs-3 marginiBottLista" >
                        <a href="${createLink(controller: "polizze", action: "elencotuttiFlussiDL")}" target="" class="btn btn-ricerca" title="Estrazione tutti flussi DL"><b>Elenco tutti flussi DL</b> <i class="fa fa-download"></i></a>
                        %{--<a href="${createLink(controller: "polizze", action: "elencoPolizzeSettimanale")}" target="_blank" class="btn btn-ricerca" title="Estrazione polizze settimanale"><b>Riepilogo polizze settimanale</b> <i class="fa fa-download"></i></a>--}%
                    </div>
                    <div class="col-md-3 col-xs-3  btnFinanziate">
                        <a href="${createLink(controller: "polizze", action: "generaflussicompagnia")}" target="" class="btn btn-ricerca" title="genera flussi COMPAGNIA"><b>Elenco flussi DL temporaneo</b> <i class="fa fa-download"></i></a>
                        %{--<a href="${createLink(controller: "polizze", action: "elencoPolizzeSettimanale")}" target="_blank" class="btn btn-ricerca" title="Estrazione polizze settimanale"><b>Riepilogo polizze settimanale</b> <i class="fa fa-download"></i></a>--}%
                    </div>
                    <div class="col-md-3 col-xs-6 btnFinanziate">
                        <a href="#" id="corre-pratica" class="btn btn-ricerca" title="Nuova pratica"><b>Correzzione polizze finanziate</b> <i class="fa fa-upload"></i></a>
                    </div>
                </sec:ifHasRole>
                <div class="col-md-3 col-xs-6 btnFinanziate">
                    <a href="#" id="new-pratica" class="btn btn-ricerca" title="Nuova pratica"><b>Caricare polizze finanziate</b> <i class="fa fa-upload"></i></a>
                </div>

                %{--<div class="col-md-3 col-xs-6 btnFinanziate">
                    <div class="col-md-3 col-xs-3 ">
                        <a href="#" id="new-exceltelaiotariffa" class="btn btn-ricerca"
                           title="Carica file telai"><b>Carica file telai/data tariffa</b> <i class="fa fa-upload"></i>
                        </a>
                    </div>
                </div>--}%

                <div class="col-md-3 col-xs-3  btnFinanziate">
                    <a href="${createLink(controller: "polizze", action: "elencoFlussiIassicur")}" target="" class="btn btn-ricerca" title="Estrazione flussi IASSICUR"><b>Elenco flussi finanziate</b> <i class="fa fa-download"></i></a>
                    %{--<a href="${createLink(controller: "polizze", action: "elencoPolizzeSettimanale")}" target="_blank" class="btn btn-ricerca" title="Estrazione polizze settimanale"><b>Riepilogo polizze settimanale</b> <i class="fa fa-download"></i></a>--}%
                </div>
                <div class="col-md-3 col-xs-3 btnFinanziate" >
                    <a href="${createLink(controller: "polizze", action: "elencoFlussiPAI")}" target="" class="btn btn-ricerca" title="Estrazione Flussi PAI"><b>Elenco flussi PAI</b> <i class="fa fa-download"></i></a>
                    %{--<a href="${createLink(controller: "polizze", action: "riepilogoPolizze")}" target="_blank" class="btn btn-ricerca" title="Estrazione polizze"><b>Riepilogo polizze</b> <i class="fa fa-download"></i></a>--}%
                </div>

            </f:form>
            <f:form  id="certFINLEA" method="post" action="${createLink(action: "generaCertFinLeasing", params:[_csrf: request._csrf.token])}">
                <div class="col-xs-6 col-md-4 borderLCERT bordertbCERT  borderRCERT" style="margin-top: 90px;">
                    <div class="col-xs-6 col-md-12">
                        <label class="testoParagraph control-label">Selezionare la data per generare i certificati:</label>
                    </div>
                    <div class="col-xs-3 col-md-4">
                        <input class="form-controlI maiuscola testoParagraph bottoneCERT"  type="text" name="dataGeneraCert"  id="dataGeneraCert" value="${new Date().format("dd-MM-yyyy")}" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"
                               data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                               data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"/>
                    </div>
                    <div class="col-md-4 col-md-push-4 col-xs-3">
                        <button type="submit"  class="btn btn-ricerca pull-right bottoneCERT"><b>Genera Cert. Fin./Leasing</b></button>
                    </div>
                </div>
                <div class="col-md-6 col-xs-3" style="margin-top: 90px;" >
                    <a href="${createLink(controller: "polizze", action: "invioCertMail", params:[isCash: false])}" target="" class="btn btn-ricerca" title="IASSICUR"><b>Invio Cert. Fin./Leasing ai Dealers</b> <i class="fa fa-envelope"></i></a>
                </div>

                <div class="col-md-12 col-xs-6 btnFinanziate">
                    <div class="col-md-3 col-xs-3 ">
                        <a href="#" id="new-praticaSpe" class="btn btn-ricerca" title="Nuova pratica speciale"><b>Carica polizze fin. speciali</b> <i class="fa fa-upload"></i></a>
                    </div>
                    <div class="col-md-3 col-xs-3  " >
                        <a href="${createLink(controller: "polizze", action: "elencoFlussiPAISpe")}" target="" class="btn btn-ricerca" title="Estrazione Flussi PAI"><b>Elenco flussi PAI speciali</b> <i class="fa fa-download"></i></a>
                        %{--<a href="${createLink(controller: "polizze", action: "riepilogoPolizze")}" target="_blank" class="btn btn-ricerca" title="Estrazione polizze"><b>Riepilogo polizze</b> <i class="fa fa-download"></i></a>--}%
                    </div>
                    <div class="col-md-3 col-xs-3 ">
                        <a href="#" id="new-praticaPAIOmaggio" class="btn btn-ricerca" title="PAI omaggio"><b>Carica PAI Gratuita speciale</b> <i class="fa fa-upload"></i></a>
                    </div>
                    <div class="col-md-3 col-xs-3 " >
                        <a href="${createLink(controller: "polizze", action: "elencoFlussiPAIOmaggio")}" target="" class="btn btn-ricerca" title="Estrazione Flussi PAI omaggio"><b>Elenco flussi PAI gratuita</b> <i class="fa fa-download"></i></a>
                    </div>
                </div>

            </f:form>

       <sec:ifHasRole role="ADMINPRESSTODAY">
           <f:form  id="generatuttiflussicompa" method="post" action="${createLink(action: "generaflussicompagnia", params:[_csrf: request._csrf.token])}">
               <div class="col-xs-6 col-md-8 borderLCERT bordertbCERT  borderRCERT">
                   <div class="col-xs-6 col-md-12">
                       <label class="testoParagraph control-label">Selezionare il periodo per la chiamata del ws:</label>
                   </div>
                   <div class="col-md-1"><label for="dataGenera1">Da:</label></div>
                   <div class="col-xs-3 col-md-3 col-md-pull-1">
                       <input class="form-controlI maiuscola testoParagraph bottoneCERT"  type="text" name="dataGenera1"  id="dataGenera1" value="${new Date().format("dd-MM-yyyy")}" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"
                              data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                              data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"/>
                   </div>
                   <div class="col-md-1"><label for="dataGenera2">A:</label></div>
                   <div class="col-xs-3 col-md-3 col-md-pull-1">
                       <input class="form-controlI maiuscola testoParagraph bottoneCERT"  type="text" name="dataGenera2"  id="dataGenera2" value="${new Date().format("dd-MM-yyyy")}" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"
                              data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                              data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"/>
                   </div>
                   <div class="col-md-4 col-xs-3">
                       <button type="submit"  class="btn btn-ricerca pull-right bottoneCERT"><b>genera tracciati compagnia</b> <i class="fa fa-file-archive-o"></i></button>
                   </div>
               </div>
           </f:form>
           <div class="table-responsive col-md-12 col-xs-12">
               <table class="table table-condensed titoli" id="tabellaPolizza">
                   <thead>
                       <tr>
                           <th class="text-left vcenter-column">DEALER</th>
                           <th class="text-left vcenter-column"></th>
                           <th class="text-left ">DATA DECORRENZA</th>
                           <th class="text-left ">DATA INSERIMENTO</th>
                           <th class="text-left">NO. POLIZZA</th>
                           <th class="text-left">COPERTURA</th>
                           <th class="text-left">ASSICURATO</th>
                           <th class="text-left">COD. FISCALE/<br>PARTITA IVA</th>
                           <th class="text-left">TRACCIATO IASSICUR</th>
                           <th class="text-left">TRACCIATO COMPAGNIA</th>
                           <th class="text-left">TRACCIATO PAI</th>
                       </tr>
                   </thead>
                   <tbody>
                   <g:each var="polizza" in="${polizze}">
                       <input type="hidden" name="idPolizzasel" id="idPolizzasel" value="${polizza.id}"/>
                       <tr class="polizza">
                           <td class="text-left vcenter-column dealerNome" data-id="${polizza.dealer.id}"  id="dealerNome" name="dealerNome" value="${polizza.dealer.id}"><b>${polizza.dealer.ragioneSocialeD}</b><br>${polizza.dealer.indirizzoD}, ${polizza.dealer.provinciaD}</td>
                           <td class="text-left vcenter-column"><a href="${createLink(action: "modificaPolizza", id: polizza.id)}"  style="cursor: pointer; font-size: 1.25em;"><i class="fa fa-pencil"></i></a></td>
                           <td class="text-left vcenter-column" id="dataDecorrenza">${polizza.dataDecorrenza.format("dd/MM/yyyy")} </td>
                           <td class="text-left vcenter-column" id="dateCreatedPolizza">${polizza.dateCreated.format("dd/MM/yyyy")}</td>
                           <td class="text-left vcenter-column">${polizza.noPolizza}</td>
                           <td class="text-left vcenter-column">${polizza.coperturaRichiesta.toString().toUpperCase()}</td>
                           <td class="text-left vcenter-column">${polizza.nome.toString().toUpperCase()} ${polizza.cognome.toString().toUpperCase()}</td>
                           <td class="text-left vcenter-column">${polizza.partitaIva.toString().toUpperCase()}</td>
                           <td class="text-center vcenter-column">%{--<g:if test="${polizza.tipoPolizza.toString().toUpperCase()!="CASH"}">--}%<a class="bottoneDoc" href="${createLink(controller: "polizze", action: "flussoIassicur", id: polizza.id)}" target="_blank"  title="Tracciato polizza ${polizza.noPolizza}"><i class="fa fa-file-text-o" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></a>%{--</g:if>--}%</td>
                           <td class="text-center vcenter-column">%{--<g:if test="${polizza.tipoPolizza.toString().toUpperCase()!="CASH"}">--}%<a class="bottoneDoc" href="${createLink(controller: "polizze", action: "flussoDirectLine", id: polizza.id)}" target="_blank"  title="Tracciato polizza ${polizza.noPolizza}"><i class="fa fa-file-text-o" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></a>%{--</g:if>--}%</td>
                           <td class="text-center vcenter-column">%{--<g:if test="${polizza.tipoPolizza.toString().toUpperCase()!="CASH"}">--}%<a class="bottoneDoc" href="${createLink(controller: "polizze", action: "flussoPAI", id: polizza.id)}" target="_blank"  title="Tracciato polizza ${polizza.noPolizza}"><i class="fa fa-file-text-o" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></a>%{--</g:if>--}%</td>
                       </tr>
                   </g:each>
                   </tbody>
                   <tfoot>
                   <tr>
                       <th colspan="18" class="text-center"><b>Sono state trovate ${polizze?.totalCount ?: 0} polizze</b></th>
                   </tr>
                   </tfoot>
               </table>
           </div>
       </sec:ifHasRole>
   </div>
    <div class="modal" id="form-pratica">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center titoli"> Caricamento Pratica</h3>
                    </div>
                    <div class="modal-body" style="background: white">
                        <form action="${createLink(action: "caricaPraticaCSV", params:[speciale:false,_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-idExcelPratica" type="file" name="excelPratica" class="file form-controlI" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn btn-ricerca pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
    <div class="modal" id="form-praticaSpeciale">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center titoli"> Caricamento Pratica</h3>
                    </div>
                    <div class="modal-body" style="background: white">
                        <form action="${createLink(action: "caricaPraticaCSV", params:[speciale:true,_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-idExcelPratica" type="file" name="excelPratica" class="file form-controlI" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn btn-ricerca pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
    <div class="modal" id="form-praticaOmaggio">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center titoli"> Caricamento Pratiche PAI omaggio</h3>
                    </div>
                    <div class="modal-body" style="background: white">
                        <form action="${createLink(action: "caricaPolizzaPAIOmaggio", params:[speciale:true,_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-idExcelPratica" type="file" name="excelPratica" class="file form-controlI" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn btn-ricerca pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
    <div class="modal" id="form-exceltelaiotariffa">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center titoli"> Caricamento telai/data tariffa</h3>
                    </div>

                        <div class="modal-body" style="background: white">
                            <form action="${createLink( controller:"polizze", action: "associatelaiodatatar", params:[speciale:true,_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                                <input id="input-telaio" type="file" name="telaiodatatar" class="file form-controlI" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                                %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <button type="submit" style="margin-top: 10px;"  class="btn btn-ricerca pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                    </div>
                                </div>
                            </form>
                        </div>


                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
    <div class="modal" id="form-correpratica">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center titoli"> Caricamento Pratica</h3>
                    </div>
                    <div class="modal-body" style="background: white">
                        <form action="${createLink(action: "correfinleasing", params:[speciale:false,_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-idCorrePratica" type="file" name="correPratica" class="file form-controlI" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn btn-ricerca pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>

</div>

<script type="text/javascript">
    $("#new-pratica").click(function() { $("#form-pratica").modal("show"); });
    $("#corre-pratica").click(function() { $("#form-correpratica").modal("show"); });
    $("#new-praticaSpe").click(function() { $("#form-praticaSpeciale").modal("show"); });
    $("#new-praticaPAIOmaggio").click(function() { $("#form-praticaOmaggio").modal("show"); });
    $("#new-exceltelaiotariffa").click(function() { $("#form-exceltelaiotariffa").modal("show"); });

    $(document).ready(function() {
        window.setTimeout(function() {
            $(".alert").fadeTo(1500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 5000);
    });
    window.onload = function() {
        var txt = document.getElementById('errori');
        var txtPra = document.getElementById('pratiche');
        var txtPai = document.getElementById('pai');
        var txtAssoc = document.getElementById('associazione');
        //txt.value = window.onload + '';

        if(txt != null){
            var array = txt.value.toString().split(',').join("\r\n");
            if(document.getElementById('link') != null){
                document.getElementById('link').onclick = function(code) {
                    this.href = 'data:text/plain;charset=utf-8,'
                            + encodeURIComponent(array);
                };
            }

        }
        if(txtPra !=null){
            var arrayP = txtPra.value.toString().split(',').join("\r\n");
            if(document.getElementById('linkP') != null){
                document.getElementById('linkP').onclick = function(code) {
                    this.href = 'data:text/plain;charset=utf-8,'
                            + encodeURIComponent(arrayP);
                };
            }
        }
        if(txtPai !=null){
            var arrayPai = txtPai.value.toString().split(',').join("\r\n");
            if(document.getElementById('linkPai') != null){
                document.getElementById('linkPai').onclick = function(code) {
                    this.href = 'data:text/plain;charset=utf-8,'
                            + encodeURIComponent(arrayPai);
                };
            }
        }
        if(txtAssoc !=null){
            var arrayAssoc = txtAssoc.value.toString().split(',').join("\r\n");
            if(document.getElementById('linkAss') != null){
                document.getElementById('linkAss').onclick = function(code) {
                    this.href = 'data:text/plain;charset=utf-8,'
                        + encodeURIComponent(arrayAssoc);
                };
            }
        }

    };
    function attivaTab(tab){
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };
</script>
</body>
</html>