<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="navigazioneAnnullaCFLR"/>
    <title>Elenco</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
<div class="row">
    <div class="col-md-12 col-xs-10">
        <div class="inner  marginesotto">  </div>

        <f:form  id="listAnnullaform" method="post">
            <div class="col-md-12">
                <div class="col-md-8">
                    <label class="label-Annulla control-label">Ricerca per:</label>
                </div>
                <div class="col-md-6">
                    <div class="col-md-12 col-xs-6 " >
                        <div class="col-md-12 marginesotto marginesopra">
                            <div class="col-md-4 col-md-pull-1">
                                <label class="control-label">numero polizza:</label>
                            </div>
                            <div class="col-md-6 col-md-pull-2">
                                <input type="text" class="form-controlA" placeholder="inserisce no. di Polizza" name="searchNo" id="searchNo" value="${params.searchNo}">
                            </div>
                        </div>
                        <div class="col-md-12 marginesotto marginesopra">
                            <div class="col-md-3 col-md-pull-1">
                                <label class="control-label">cognome cliente:</label>
                            </div>
                            <div class="col-md-9 col-md-pull-1">
                                <input type="text" class="form-controlA" placeholder="inserisce il cognome cliente o la ragione sociale" name="searchCognome" id="searchCognome"  value="${params.searchCognome}">
                            </div>
                        </div>
                        <div class="col-md-12 col-md-push-2 marginesotto marginesopra marginiAvanti">
                            <button type="submit" name="button_search" id="button_search" class="btn btn-cerca" <g:if test="${params.button_search}"></g:if><g:else></g:else> onchange="window.location.href = window.location.href.substr(0,window.location.href.lastIndexOf('?'));">Trova</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive col-md-12 col-xs-12">
                <table class="table table-condensed titoli" id="tabellaPolizza">
                    <thead>
                    <th class="text-left">NO. POLIZZA</th>
                    <th class="text-left">NOME</th>
                    <th class="text-left">TIPO CONTRATTO</th>
                    <th class="text-left ">DATA DECORRENZA</th>
                    <th class="text-left ">DATA SCADENZA</th>
                    <th class="text-left ">STATO</th>
                    <th class="text-left">DATA ANNULLAMENTO</th>
                    <th class="text-left"></th>
                    <th class="text-left">RIMBORSO</th>
                    </tr>
                    </thead>
                    <tbody>
                    <g:each var="polizza" in="${polizze}">
                        <tr class="polizza">
                            <input type="hidden" name="idPolizzasel" id="idPolizzasel" value=""/>
                            <input type="hidden" name="idPolizza" id="idPolizza${polizza.id}" value="${polizza.id}"/>
                            <input type="hidden" name="valoreAssicurato${polizza.id}" id="valoreAssicurato${polizza.id}" value="${polizza.valoreAssicurato}"/>
                            <input type="hidden" name="provincia" id="provincia${polizza.id}" value="${polizza.provincia}"/>
                            <input type="hidden" name="localita" id="localita${polizza.id}" value="${polizza.localita}"/>
                            <input type="hidden" name="durata" id="durata${polizza.id}" value="${polizza.durata}"/>
                            <input type="hidden" name="onStar" id="onStar${polizza.id}" value="${polizza.onStar}"/>
                            <input type="hidden" name="dealerType" id="dealerType${polizza.id}" value="${polizza.dealer.dealerType}"/>
                            <input type="hidden" name="step" id="step${polizza.id}" value="${polizza.step}"/>
                <input type="hidden" name="percVenditore" id="percVenditore${polizza.id}" value="${polizza.dealer.percVenditore}"/>
                            <input type="hidden" name="premioImponibile" id="premioImponibile${polizza.id}" value=""/>
                            <input type="hidden" name="premioLordo" id="premioLordo${polizza.id}" value=""/>
                            <input type="hidden" name="provvDealer" id="provvDealer${polizza.id}" value=""/>
                            <input type="hidden" name="provvGmfi" id="provvGmfi${polizza.id}" value=""/>
                            <input type="hidden" name="provvVenditore" id="provvVenditore${polizza.id}" value=""/>
                            <input type="hidden" name="codiceZonaTerritoriale" id="codiceZonaTerritoriale${polizza.id}" value=""/>
                            <input type="hidden" name="rappel" id="rappel${polizza.id}" value=""/>
                            <input type="hidden" name="imposte" id="imposte${polizza.id}" value=""/>
                            <td class="text-left vcenter-column">${polizza.noPolizza}</td>
                            <td class="text-left vcenter-column">${polizza.nome.toString().toUpperCase()} ${polizza.cognome.toString().toUpperCase()}</td>
                            <td class="text-left vcenter-column" id="coperturaRichiesta${polizza.id}">${polizza.coperturaRichiesta.toString().toUpperCase()}</td>
                            <td class="text-left vcenter-column" id="datDeco${polizza.id}">${polizza.dataDecorrenza.format("dd/MM/yyyy")}</td>
                            <td class="text-left vcenter-column" id="dataScadenza${polizza.id}">${polizza.dataScadenza.format("dd/MM/yyyy")}</td>
                            <td class="text-left vcenter-column" id="stato${polizza.id}"><g:if test="${polizza.stato.toString().toUpperCase()=="POLIZZA IN ATTESA DI ATTIVAZIONE"}">IN ATTESA DI ATTIVAZIONE</g:if><g:else>${polizza.stato.toString().toUpperCase()}</g:else> </td>
                            <td class="text-center vcenter-column"><g:if test="${polizza.annullata}">ANNULLATA</g:if><g:else><input class="form-controlI storno" type="text" name="${polizza.id}"  id="dataStorno${polizza.id}" value="" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"
                                                                        data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                                                                        data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"/></g:else></td>
                            <td class="text-left vcenter-column"><g:if test="${polizza.annullata}"></g:if><g:else><a href="" %{--onclick="return chiamataWS();"--}% class="btn btn-avanti pull-right chiamaWs" name="${polizza.id}" id="chiamataWS${polizza.id}"  title="chiamata web service" disabled="true"><b>CHIAMATA WEB SERVICE</b></a></g:else></td>
                            <td class="text-left vcenter-column"><g:if test="${polizza.annullata}"></g:if><g:else><input type="text" id="importoRimborso${polizza.id}" name="importoRimborso${polizza.id}" class="form-controlA testoParagraph importo" readonly="true"/></g:else></td>
                            <td class="text-left vcenter-column"><g:if test="${polizza.annullata}"></g:if><g:else><button type="submit" id="annulla${polizza.id}" name="${polizza.id}" class="btn btn-success annullamento" disabled="disabled"> ANNULLA </button></g:else></td>
                        </tr>
                    </g:each>
                    </tbody>
                    %{--<tfoot>
                    <tr>
                        <th colspan="18" class="text-center"><b>Sono state trovate ${polizze?.totalCount ?: 0} polizze</b></th>
                    </tr>
                    </tfoot>--}%
                </table>
            </div>
        </f:form>
        %{--<nav aria-label="Page navigation">
            <ul class="pagination pull-right">
                <g:each var="p" in="${pages}">
                    <g:if test="${p == page}"><li class="active"></g:if><g:else><li></g:else>
                    <g:if test="${params.search}"><link:listAnn page="${p}" controller="${controllerName}" search="${params.search}">${p}</link:listAnn></g:if>
                    <g:else><link:listAnn page="${p}" controller="${controllerName}">${p}</link:listAnn></g:else>
                    </li>
                </g:each>
            </ul>
        </nav>--}%
    </div>
</div>

<script type="text/javascript">

    $(document).ready(function() {
        window.setTimeout(function() {
            $(".alert").fadeTo(1500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 5000);
    });
    window.onload = function() {
        var txt = document.getElementById('errori');
        var txtPra = document.getElementById('pratiche');
        //txt.value = window.onload + '';

        if(txt != null){
            var array = txt.value.toString().split(',').join("\r\n");
            if(document.getElementById('link') != null){
                document.getElementById('link').onclick = function(code) {
                    this.href = 'data:text/plain;charset=utf-8,'
                            + encodeURIComponent(array);
                };
            }

        }
        if(txtPra !=null){
            var arrayP = txtPra.value.toString().split(',').join("\r\n");
            if(document.getElementById('linkP') != null){
                document.getElementById('linkP').onclick = function(code) {
                    this.href = 'data:text/plain;charset=utf-8,'
                            + encodeURIComponent(arrayP);
                };
            }
        }

    };
    $("#searchCognome").on("change",function(event){
        var valore= $.trim($("#searchCognome").val());
        if(valore!=''){
            $("#button_search").prop('disabled', false);
            $("#button_search").val(true);
        }
    });
    $("#searchNo").on("change",function(event){
        var valore= $.trim($("#searchNo").val());
        if(valore!=''){
            $("#button_search").prop('disabled', false);
            $("#button_search").val(true);
        }
    });
    $('.storno').on("changeDate", function(event) {
        var data1 = $.trim($(this).val());
        var nome=this.name;
        if(data1 != ''){
            if(document.getElementById('chiamataWS' + nome)){
                document.getElementById('chiamataWS' + nome).removeAttribute("disabled");
            }
        }else{
            //$("#linkModulo").addClass("not-active");
            //removeParameters();
        }
        $('.datepicker').hide();
    });
    $('.chiamaWs').click(function(e) {
        e.preventDefault();
        $( ".labelErrore" ).remove();
        var nome=this.name;
        var disabledA=this.getAttribute("disabled");
        var dataStorno= document.getElementById('dataStorno' + nome).value;
        if(dataStorno != null && (disabledA!=true || disabledA !="disabled")){
            chiamataWS(nome);
        }
    });
    function chiamataWS(nome){
        var idPolizza=document.getElementById('idPolizza' + nome).value;
        var valoreAssicurato=document.getElementById('valoreAssicurato' + nome).value;
        var provincia=document.getElementById('provincia' + nome).value;
        var comune=document.getElementById('localita' + nome).value;
        var durata=document.getElementById('durata' + nome).value;
        var onStar=document.getElementById('onStar' + nome).value;
        var dealerType=document.getElementById('dealerType' + nome).value;
        var step=document.getElementById('step' + nome).value;
        var percVenditore=document.getElementById('percVenditore' + nome).value;
        var coperturacodifica=document.getElementById('coperturaRichiesta' + nome).innerHTML;
        var copertura
        if(coperturacodifica.toString()=="SILVER"){
            copertura="silver"
        }else if(coperturacodifica.toString()=="GOLD"){
            copertura="gold"
        }else if(coperturacodifica.toString()=="PLATINUM"){
            copertura="platinum"
        }else if(coperturacodifica.toString()=="PLATINUM KASKO"){
            copertura="platinumK"
        }else if(coperturacodifica.toString()=="PLATINUM COLLISIONE"){
            copertura="platinumC"
        }
        var dataDeco = document.getElementById('datDeco' + nome).innerHTML.split('/');
        var dataDecorrenza=dataDeco[2]+'-'+dataDeco[1]+'-'+dataDeco[0];
        var tipoOperazione="E";
        var dataScad = document.getElementById('dataScadenza' + nome).innerHTML.split('/');
        var dataScadenza=dataScad[2]+'-'+dataScad[1]+'-'+dataScad[0];
       // var dataStorno=$("#dataStorno").val();
        var dataSt= document.getElementById('dataStorno' + nome).value.split('-');
        var dataStorno= dataSt[2]+'-'+dataSt[1]+'-'+dataSt[0];
        $.post("${createLink(controller: "polizze", action: "chiamataWS")}",{ tipoPolizza:"CFL", copertura:copertura, valoreAssicurato:valoreAssicurato, provincia:provincia, comune:comune, durata:durata,onStar:onStar,dealerType:dealerType,step:step,percVenditore:percVenditore,dataDecorrenza:dataDecorrenza,operazione:tipoOperazione,dataScadenza:dataScadenza,dataStorno:dataStorno, _csrf: "${request._csrf.token}"}, function(response) {
            var risposta=response.risposta;
            if(risposta==false){
                var errore=response.errore;
                var provvDealer=Math.round(response.provvDealer * 100) / 100
                var provvGmfi=Math.round(response.provvgmfi * 100) / 100
                $( ".labelErrore" ).remove();
                //$( ".inner" ).append( "<p>"+errore+"</p>" );
                $( ".inner" ).after( "<div class=\"labelErrore marginesotto\"><p>"+response.errore+"</p></div>" );
                document.getElementById('premioImponibile' + nome).value=response.premioImponibile;
                document.getElementById('premioLordo' + nome).value=response.premioLordo;
                document.getElementById('provvDealer' + nome).value=provvDealer;
                document.getElementById('provvGmfi' + nome).value=provvGmfi;
                document.getElementById('provvVenditore' + nome).value=response.provvVend;
                document.getElementById('codiceZonaTerritoriale' + nome).value=response.codiceZonaTerritoriale;
                document.getElementById('rappel' + nome).value=response.rappel;
                document.getElementById('imposte' + nome).value=response.imposte;
            }else{
                var provvDealer=Math.round(response.provvDealer * 100) / 100
                var provvGmfi=Math.round(response.provvgmfi * 100) / 100
                $( ".labelErrore" ).remove();
                $( ".inner" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'>I dati sono stati caricati dal webservice</span></div>" );
                document.getElementById('importoRimborso' + nome).value=response.premioImponibile;
                document.getElementById('premioImponibile' + nome).value=response.premioImponibile;
                document.getElementById('premioLordo' + nome).value=response.premioLordo;
                document.getElementById('provvDealer' + nome).value=provvDealer;
                document.getElementById('provvGmfi' + nome).value=provvGmfi;
                document.getElementById('provvVenditore' + nome).value=response.provvVend;
                document.getElementById('codiceZonaTerritoriale' + nome).value=response.codiceZonaTerritoriale;
                document.getElementById('rappel' + nome).value=response.rappel;
                document.getElementById('imposte' + nome).value=response.imposte;
                if(document.getElementById('annulla' + nome)){
                    document.getElementById('annulla' + nome).removeAttribute("disabled");
                }
            }
        }, "json");
    }

    $('.annullamento').click(function(e) {
        e.preventDefault();
        $( ".labelErrore" ).remove();
        var nome=this.name;
        var importoR= document.getElementById('importoRimborso' + nome).value;
        if(importoR != null){
            annullamentoPolizza(nome);
        }
    });
    function annullamentoPolizza(nome){
        var codOperazione="E";
        var premioImponibile=document.getElementById('premioImponibile' + nome).value;
        var premioLordo=document.getElementById('premioLordo' + nome).value;
        var provvDealer=document.getElementById('provvDealer' + nome).value;
        var provvGmfi=document.getElementById('provvGmfi' + nome).value;
        var provvVenditore=document.getElementById('provvVenditore' + nome).value;
        var rappel=document.getElementById('rappel' + nome).value;
        var imposte=document.getElementById('imposte' + nome).value;
        var idPolizza=document.getElementById('idPolizza' + nome).value;
        var dataSt= document.getElementById('dataStorno' + nome).value.split('-');
        var dataStorno= dataSt[2]+'-'+dataSt[1]+'-'+dataSt[0];
        $.post("${createLink(controller: "polizze", action: "recedePolizza")}",{ dataStorno:dataStorno, idPolizza:idPolizza, codOperazione:codOperazione, premioImponibile:premioImponibile, premioLordo:premioLordo, provvDealer:provvDealer, provvGmfi:provvGmfi, provvVenditore:provvVenditore,rappel:rappel,imposte:imposte, _csrf: "${request._csrf.token}"}, function(response) {
            var risposta=response.risposta;
            if(risposta==false){
                var errore=response.errore;
                $( ".labelErrore" ).remove();
                //$( ".inner" ).append( "<p>"+errore+"</p>" );
                $( ".inner" ).after( "<div class=\"labelErrore marginesotto\"><p>"+response.errore+"</p></div>" );
            }else{
                $( ".labelErrore" ).remove();
                $( ".inner" ).after( "<div class=\"labelErrore marginesotto\"> <span class='testoAlert'>La polizza è stata annullata</span></div>" );
                location.reload();
                if(document.getElementById('annulla' + nome)){
                    document.getElementById('annulla' + nome).setAttribute("disabled","disabled");
                }
                if(document.getElementById('chiamataWS' + nome)){
                    document.getElementById('chiamataWS' + nome).setAttribute("disabled","disabled");
                }
                if(document.getElementById('dataStorno' + nome)){
                    document.getElementById('dataStorno' + nome).setAttribute("readOnly","true");
                }
            }
        }, "json");
    }
</script>
</body>
</html>