<%@ page contentType="text/html;charset=UTF-8" %>
<html>
<head>
    <meta name="layout" content="navigazionePaiPag"/>
    <title>Elenco</title>
    <script language="javascript">
        if(history.length > 0) history.forward()
    </script>
</head>

<body>
<div class="row">
   <div class="col-md-12 col-xs-10">
        <g:if test="${flash.error}"><div class="labelErrore marginesotto">${flash.error.toString().replaceAll("\\<.*?>","")} </div></g:if>
       <g:if test="${flash.flussiPAI}">
           <div class="labelErrore marginesotto">
               <input type="hidden" id="pai" value="${flash.flussiPAI}"/>
               <p>Premere il link in basso per  il riassunto dei flussi PAI caricati:</p>
               <a href="" class="link" id="linkP" download="flussiPAI.txt">Lista flussi</a><br/>
               %{--<g:each var="messageP" in="${flash.praticheNuove}">
                   <p>${messageP}</p>
               </g:each>--}%
           </div>
       </g:if>
       <g:if test="${flash.certPAI}">
           <div class="labelErrore marginesotto">
               <input type="hidden" id="certPAI" value="${flash.certPAI}"/>
               <p>Premere il link in basso per  il riassunto dei certificati caricati:</p>
               <a href="" class="link" id="linkC" download="certAXA.txt">Lista certificati</a><br/>
               %{--<g:each var="messageP" in="${flash.praticheNuove}">
                   <p>${messageP}</p>
               </g:each>--}%
           </div>
       </g:if>
       <g:if test="${flash.errorePratica}">
            <div class="labelErrore marginesotto">
                <input type="hidden" id="errori" value="${flash.errorePratica}"/>
                <p>Premere il link in basso per il riassunto degli errori:</p>
                <a class="link" href="" id="link" download="errori.txt">Lista errori</a><br/>
                %{--<g:each var="errore" in="${flash.errorePratica}">
                    <p>${errore.toString().replaceAll("\\<.*?>\\[]","")}</p>
                </g:each>--}%
            </div>
        </g:if>
       <g:if test="${flash.associazione}">
           <div class="labelErrore marginesotto">${flash.associazione.toString().replaceAll("\\<.*?>","")} </div>
       </g:if>
       <g:if test="${flash.certificati}">
           <div class="labelErrore marginesotto">
               <g:each var="messageP" in="${flash.certificati}">
                   <p>${messageP}</p>
               </g:each>
           </div>
       </g:if>
            <f:form  id="listFinanzform" method="post">
                <sec:ifHasRole role="ADMINPRESSTODAY">
                    <div class="col-md-3 col-xs-6 area-ricerca" >
                        <div class="input-group input-ricerca">
                            <input type="text" class="form-controlC" placeholder="ricerca polizze PAI PAGAMENTO" name="search" value="${params.search}">
                            <span class="input-group-btn">
                                <button type="submit" id="button_search" class="btn btn-ricerca" onchange="window.location.href = window.location.href.substr(0,window.location.href.lastIndexOf('?'));"><i class="glyphicon glyphicon-search"></i></button>
                            </span>
                        </div>
                    </div>

                </sec:ifHasRole>
            </f:form>

           <f:form  id="GMFEN" method="post" action="${createLink(action: "generaFileGMFGENPAI", params:[_csrf: request._csrf.token])}">

               <div class="col-md-12 col-xs-6 btnFinanziate">
                   <div class="col-md-3 col-xs-3 ">
                       <a href="#" id="new-praticaPag" class="btn btn-ricerca" title="Nuova pratica pagamento"><b>Carica polizze PAI pagamento</b> <i class="fa fa-upload"></i></a>
                   </div>
                   <div class="col-md-3 col-xs-3  " >
                       <a href="${createLink(controller: "polizze", action: "elencoFlussiPAIPagamento")}" target="" class="btn btn-ricerca" title="Estrazione Flussi PAI"><b>Elenco flussi PAI pagamento</b> <i class="fa fa-download"></i></a>
                   </div>
               </div>
               <div class="col-xs-6 col-md-12 marginiBottLista">
                   <label class="testoParagraph control-label">Selezionare il mese e  l'anno per generare il report GMFEN</label>
               </div>
               <div class="col-xs-6 col-md-2 bordertbGMFEN borderlGMFEN">
                   <label class="testoParagraph control-label">Mese</label>
                   <select class="form-controlI testoParagraph" style="margin-bottom:10px;" name="mese" id="mese">
                       <option <g:if test="${new Date().getMonth()==0}"> selected </g:if>  value="0" selected >Gennaio</option>
                       <option <g:if test="${new Date().getMonth()==1}"> selected </g:if> value="1" >Febbraio</option>
                       <option <g:if test="${new Date().getMonth()==2}"> selected </g:if> value="2" >Marzo</option>
                       <option <g:if test="${new Date().getMonth()==3}"> selected </g:if> value="3" >Aprile</option>
                       <option <g:if test="${new Date().getMonth()==4}"> selected </g:if> value="4" >Maggio</option>
                       <option <g:if test="${new Date().getMonth()==5}"> selected </g:if> value="5" >Giugno</option>
                       <option <g:if test="${new Date().getMonth()==6}"> selected </g:if> value="6" >Luglio</option>
                       <option <g:if test="${new Date().getMonth()==7}"> selected </g:if> value="7" >Agosto</option>
                       <option <g:if test="${new Date().getMonth()==8}"> selected </g:if> value="8" >Settembre</option>
                       <option <g:if test="${new Date().getMonth()==9}"> selected </g:if> value="9" >Ottobre</option>
                       <option <g:if test="${new Date().getMonth()==10}"> selected </g:if> value="10" >Novembre</option>
                       <option <g:if test="${new Date().getMonth()==11}"> selected </g:if> value="11" >Dicembre</option>
                   </select>
               </div>
               <div class="col-xs-6 col-md-2 bordertbGMFEN">
                   <label class="testoParagraph control-label">Anno</label>
                   <select class="form-controlI testoParagraph" style="margin-bottom:10px;" name="anno" id="anno"></select>
               </div>
               <div class="col-md-2 col-xs-3 bordertbGMFEN borderRGMFEN">
                   <label class="testoParagraph control-label"></label>
                   <button type="submit"  class="btn btn-ricerca pull-right bottoneGMFEN">Estrarre GMFGEN</button>
                   %{--<a href=""  class="btn btn-avanti pull-right" name="generaFileGMFGEN" id="generaFileGMFGEN"  title="Estrazione GMFGEN"><b>Estrarre GMFGEN</b></a>
                   %{--<a href="${createLink(controller: "polizze", action: "generaFileGMFGEN")}" target="_blank" class="btn btn-ricerca" title="Estrazione GMFGEN"><b>Estrazione GMFGEN</b> <i class="fa fa-table"></i></a>--}%
               </div>
           </f:form>

            <sec:ifHasRole role="ADMINPRESSTODAY">
                <div class="col-md-12">
                    <f:form  id="certPAIPAG" method="post" action="${createLink(action: "generaCertPaiPAG", params:[_csrf: request._csrf.token])}">
                        <div class="col-xs-6 col-md-4 borderLCERT bordertbCERT  borderRCERT" style="margin-top: 90px;">
                            <div class="col-xs-6 col-md-12">
                                <label class="testoParagraph control-label">Selezionare la data per generare i certificati:</label>
                            </div>
                            <div class="col-xs-3 col-md-4">
                                <input class="form-controlI maiuscola testoParagraph bottoneCERT"  type="text" name="dataGeneraCert"  id="dataGeneraCert" value="${new Date().format("dd-MM-yyyy")}" data-provide="datepicker" data-date-language="it" data-date-format="dd-mm-yyyy" data-date-today-btn="false" data-date-autoclose="true"
                                       data-date-today-highlight="false" data-date-toggle-active="false" class="testoParagraph" max-view-mode="1"
                                       data-date-disable-touch-keyboard="true" data-date-enable-on-readonly="false"/>
                            </div>
                            <div class="col-md-4 col-md-push-4 col-xs-3">
                                <button type="submit"  class="btn btn-ricerca pull-right bottoneCERT"><b>Genera Cert. PAI</b></button>
                            </div>
                        </div>
                    </f:form>
                </div>

                <div class="col-md-8 col-xs-6 btnFinanziate">
                    <div class="col-md-3 col-xs-3 ">
                        <a href="#" id="new-certAXA" class="btn btn-ricerca" title="Nuova pratica pagamento"><b>Carica certificati AXA</b> <i class="fa fa-upload"></i></a>
                    </div>
                </div>
                <div class="col-md-4 col-xs-6 btnFinanziate">
                    <div class="col-md-3 col-xs-3 ">
                        <a href="#" id="new-exceltelaiopiva" class="btn btn-ricerca" title="Carica file telai"><b>Carica file telai</b> <i class="fa fa-upload"></i></a>
                    </div>
                </div>


           <div class="table-responsive col-md-12 col-xs-12">
               <table class="table table-condensed titoli" id="tabellaPolizza">
                   <thead>
                       <tr>
                           <th class="text-left vcenter-column">DEALER</th>
                           <th class="text-left ">DATA DECORRENZA</th>
                           <th class="text-left ">DATA INSERIMENTO</th>
                           <th class="text-left">NO. POLIZZA</th>
                           <th class="text-left">COPERTURA</th>
                           <th class="text-left">ASSICURATO</th>
                           <th class="text-left">COD. FISCALE/<br>PARTITA IVA</th>
                           <th class="text-left">TRACCIATO PAI</th>
                       </tr>
                   </thead>
                   <tbody>
                   <g:each var="polizza" in="${polizze}">
                       <input type="hidden" name="idPolizzasel" id="idPolizzasel" value="${polizza.id}"/>
                       <tr class="polizza">
                           <td class="text-left vcenter-column dealerNome" data-id="${polizza.dealer.id}"  id="dealerNome" name="dealerNome" value="${polizza.dealer.id}"><b>${polizza.dealer.ragioneSocialeD}</b><br>${polizza.dealer.indirizzoD}, ${polizza.dealer.provinciaD}</td>
                           <td class="text-left vcenter-column" id="dataDecorrenza">${polizza.dataDecorrenza.format("dd/MM/yyyy")} </td>
                           <td class="text-left vcenter-column" id="dateCreatedPolizza">${polizza.dateCreated.format("dd/MM/yyyy")}</td>
                           <td class="text-left vcenter-column">${polizza.noPolizza}</td>
                           <td class="text-left vcenter-column">${polizza.coperturaRichiesta.toString().toUpperCase()}</td>
                           <td class="text-left vcenter-column">${polizza.nome.toString().toUpperCase()} ${polizza.cognome.toString().toUpperCase()}</td>
                           <td class="text-left vcenter-column">${polizza.partitaIva.toString().toUpperCase()}</td>
                           <td class="text-center vcenter-column"><a class="bottoneDoc" href="${createLink(controller: "polizze", action: "flussoPAIPag", id: polizza.id)}" target="_blank"  title="Tracciato polizza ${polizza.noPolizza}"><i class="fa fa-file-text-o" style="cursor: pointer; font-size: 1.40em; color: #555555"></i></a>%{--</g:if>--}%</td>
                       </tr>
                   </g:each>
                   </tbody>
                   <tfoot>
                   <tr>
                       <th colspan="18" class="text-center"><b>Sono state trovate ${polizze?.totalCount ?: 0} polizze</b></th>
                   </tr>
                   </tfoot>
               </table>
           </div>
       </sec:ifHasRole>
   </div>
    <div class="modal" id="form-praticaPagamento">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center titoli"> Caricamento Pratiche PAI pagamento</h3>
                    </div>
                    <div class="modal-body" style="background: white">
                        <form action="${createLink( controller:"polizze", action: "caricaPolizzaPAIPagamento", params:[speciale:true,_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-idExcelPratica" type="file" name="excelPratica" class="file form-controlI" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn btn-ricerca pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
    <div class="modal" id="form-certAXA">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center titoli"> Caricamento certificati AXA</h3>
                    </div>
                    <div class="modal-body" style="background: white">
                        <form action="${createLink( controller:"polizze", action: "caricacertAXA", params:[speciale:true,_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-idPdfCertAXA" type="file" name="pdfCert" class="file form-controlI" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn btn-ricerca pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>
    <div class="modal" id="form-telaiopiva">
        <div class="vertical-alignment-helper">
            <div class="modal-dialog vertical-align-center">
                <div class="modal-content1">
                    <div class="modal-header" style="background: #f7d900">
                        <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">&times;</span><span class="sr-only">Close</span></button>
                        <h3 class="article-title text-center titoli"> Caricamento telai</h3>
                    </div>
                    <div class="modal-body" style="background: white">
                        <form action="${createLink( controller:"polizze", action: "associapivacfPAI", params:[speciale:true,_csrf: request._csrf.token])}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            <input id="input-telaio" type="file" name="telaiopiva" class="file form-controlI" multiple data-preview-file-type="text" data-upload-async="false"data-show-preview="false" data-language="it" data-max-file-count="1" data-show-upload="false"/>
                            %{--<input id="input-idAcquisizione" type="file" name="fileContentAcquisizione" class="file allegatoPassaggio" data-upload-async="false" data-show-preview="false"  data-language="it" data-max-file-count="1" data-show-upload="false">--}%
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <button type="submit" style="margin-top: 10px;"  class="btn btn-ricerca pull-right" data-uploading-text="${'<i class="fa fa-spinner fa-spin fa-lg"></i>'}">Carica</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div><!-- /.modal-content -->
            </div>
        </div>
    </div>

</div>
<script type="text/javascript">
    $("#new-praticaPag").click(function() { $("#form-praticaPagamento").modal("show"); });
    $("#new-certAXA").click(function() { $("#form-certAXA").modal("show"); });
    $("#new-exceltelaiopiva").click(function() { $("#form-telaiopiva").modal("show"); });
    $(document).ready(function() {
        window.setTimeout(function() {
            $(".alert").fadeTo(1500, 0).slideUp(500, function(){
                $(this).remove();
            });
        }, 5000);
        var cur_year=new Date().getFullYear();
        var obj=document.getElementById("anno");
        for (var i = 2016; i <= 2030; i++)     {
            opt = document.createElement("option");
            opt.value = i;
            opt.text=i;
            obj.appendChild(opt);
        }
        document.getElementById("anno").value = cur_year;
    });
    window.onload = function() {
        var txt = document.getElementById('errori');
        var txtPra = document.getElementById('pai');
        var txtPai = document.getElementById('certPAI');
        //txt.value = window.onload + '';

        if(txt != null){
            var array = txt.value.toString().split(',').join("\r\n");
            if(document.getElementById('link') != null){
                document.getElementById('link').onclick = function(code) {
                    this.href = 'data:text/plain;charset=utf-8,'
                            + encodeURIComponent(array);
                };
            }

        }
        if(txtPra !=null){
            var arrayP = txtPra.value.toString().split(',').join("\r\n");
            if(document.getElementById('linkP') != null){
                document.getElementById('linkP').onclick = function(code) {
                    this.href = 'data:text/plain;charset=utf-8,'
                            + encodeURIComponent(arrayP);
                };
            }
        }
        if(txtPai !=null){
            var arrayPai = txtPai.value.toString().split(',').join("\r\n");
            if(document.getElementById('linkC') != null){
                document.getElementById('linkC').onclick = function(code) {
                    this.href = 'data:text/plain;charset=utf-8,'
                            + encodeURIComponent(arrayPai);
                };
            }
        }

    };
    function attivaTab(tab){
        $('.nav-tabs a[href="#' + tab + '"]').tab('show');
    };
</script>
</body>
</html>