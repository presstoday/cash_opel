<html>
    <head>
        <meta name="layout" content="main">
        <title>Password scaduta</title>

    </head>
    <body>
    <div class="row">
        <div class="col-xs-12 col-sm-offset-3 col-sm-6">
            <div class="login-panel">
                <div class="login-heading">
                    <div class="row">
                        <div class="col-xs-12 page-header text-left">
                            <h3>Password scaduta</h3>
                            <p>Aggiorna le tue credenziali per accedere al portale</p>
                        </div>

                    </div>
                </div>
                <div class="login-body">
                    <div class="row">
                        <div class="col-xs-12 text-left">
                            <f:form>
                                <g:renderFlash/>
                                <f:field name="passwordAttuale" label="Password attuale" type="password" required="true" label-class="col-sm-4" control-class="col-sm-8"/>
                                <f:field name="nuovaPassword" label="Nuova password" type="password" required="true" label-class="col-sm-4" control-class="col-sm-8"/>
                                <f:field name="confermaNuovaPassword" label="Conferma password" type="password" required="true" label-class="col-sm-4" control-class="col-sm-8"/>
                                <button type="submit" class="btn  col-xs-offset-8 col-xs-4">Aggiorna password</button>
                            </f:form>
                        </div>

                    </div>

                </div>
            </div>
        </div>
    </div>
    </body>
</html>