import cashopel.polizze.CodProdotti
import cashopel.polizze.ZoneTarif
import cashopel.utenti.Admin
import cashopel.utenti.Dealer
import cashopel.utenti.Ruolo
import grails.util.Environment
import org.apache.commons.lang3.StringUtils
import org.apache.poi.ss.usermodel.Cell
import org.apache.poi.ss.usermodel.Row
import org.apache.poi.ss.util.CellReference
import org.apache.poi.xssf.usermodel.XSSFWorkbook

import java.lang.reflect.Field
import java.security.*
import java.text.DecimalFormat
import java.text.SimpleDateFormat
import java.util.regex.Pattern

import static org.apache.poi.ss.usermodel.DateUtil.getJavaDate

class BootStrap {
    def enhanceMetaClass() {
        String.metaClass {
            splitPhrase = { int length = 30 ->
                def matcher = delegate =~ "(?s)(.{1,${length}})(?:\\s|\$)"
                return matcher.collect { it[1] }
            }
        }
    }
    private def cellValue(cell) {
        switch(cell.cellType) {

            case Cell.CELL_TYPE_BOOLEAN: return cell.booleanCellValue
            case Cell.CELL_TYPE_ERROR: return cell.errorCellValue
            case Cell.CELL_TYPE_NUMERIC: return cell.numericCellValue
            case Cell.CELL_TYPE_STRING: return cell.stringCellValue
            case Cell.CELL_TYPE_FORMULA: return cell.getRichStringCellValue()
            default: return null
        }
    }
    private def cell(row, index, date = false) {
        def value = cellValue(row.getCell(index, Row.CREATE_NULL_AS_BLANK))
        if(date) return value ? getJavaDate(value) : null
        return value
    }
    void caricaExcelDealer() {
        def excel = this.class.getResource("excel/active_dealers.xlsx")
        println "Loading DEALERS.xlsx from: ${excel.path}"
        boolean attivo=false
        def wb = new XSSFWorkbook(excel.openStream())
        def decimalFormat = new DecimalFormat("#")
        def format = { dato ->
            if(dato instanceof String) return dato
            try {
                return decimalFormat.format(dato)
            } catch(e) {
                println "$dato: ${dato.getClass()} non convertibile"
                return null
            }
        }
        String special = "+!@#\$%^&*()_\\/[a-zA-Z] "
        String pattern = ".*[" + Pattern.quote(special) + "].*"
        def percVenditore
        Dealer.withTransaction {
            wb.getSheetAt(0).with {
                def cellaCodice = new CellReference("A2")
                def cellaRagioneSociale = new CellReference("D2")
                def cellPartitaIva = new CellReference("U2")
                def cellaSalesSpecialist = new CellReference("H2")
                def cellaDealerType = new CellReference("K2")
                def cellapercVenditore = new CellReference("M2")
                def cellTelefono = new  CellReference("O2")
                def cellFax = new  CellReference("P2")
                def cellmailD=new CellReference("N2")
                def cellmailSales=new CellReference("I2")
                def cellIndirizzo=new CellReference("R2")
                def cellCap=new CellReference("S2")
                def cellLocalita=new CellReference("T2")
                def cellProvincia=new CellReference("V2")
                def cellCodIassicur=new CellReference("X2")
                def ultimaRiga = new CellReference("A169")
                def firstRow = cellaCodice.row
                def lastRow = ultimaRiga.row
                (firstRow..lastRow).each { rowIndex ->
                    println "Analizzo riga: ${rowIndex}"
                    def row = getRow(rowIndex)
                    def dealerType=cell(row, cellaDealerType.col)
                    if(!(dealerType.toString().contains("NO FINLINK")) && !(dealerType.toString().contains("NO IVASS"))){
                        attivo=true
                        percVenditore=cell(row, cellapercVenditore.col)
                        if(percVenditore==null){
                            percVenditore=0.0
                        }
                    }else{
                        attivo=false
                        percVenditore=0.0
                    }
                    def codice=cell(row,cellaCodice.col)
                    codice=codice.toString()
                    if(codice.contains(".")){
                        /*def punto=codice.indexOf(".")
                        codice=codice.substring(0,punto).replaceAll("[^0-9]", "")*/
                        codice=Double.valueOf(codice).longValue().toString()
                    }
                    if (codice.toString().matches("[0].*")) {
                        //codice=codice.toString().replaceFirst("[^1-9]", "")
                        codice=codice.toString().replaceFirst("^0+(?!\\\$)", "")
                    }
                    def partitaIva = cell(row, cellPartitaIva.col)
                    partitaIva=partitaIva.toString()
                    if(partitaIva.trim()!='null'){
                        if(!partitaIva.toString().matches("^[a-z|A-Z]{6}[0-9]{2}[A-Z|a-z][0-9]{2}[A-Z|a-z][0-9]{3}[A-Z|a-z]\$") && !partitaIva.toString().matches("^[a-z|A-Z]{2}[0-9]{5}") && !partitaIva.matches("^0+\\d*(?:\\.\\d+)?\$")){
                            partitaIva=Double.valueOf(partitaIva).longValue().toString()
                        }
                    }
                    if(!partitaIva.matches("[0-9]{11}")){
                        partitaIva=partitaIva.padLeft(11,"0")
                    }
                    def indirizzo = cell(row, cellIndirizzo.col)
                    def localita = cell(row, cellLocalita.col).toString()
                    def provincia = cell(row, cellProvincia.col).toString()
                    def cap = cell(row, cellCap.col)
                    cap=cap.toString()
                    if(cap.contains(".")){
                        def puntocap=cap.indexOf(".")
                        cap=cap.substring(0,puntocap).replaceAll("[^0-9]", "")
                    }
                    if (!cap.matches("[0-9]{5}")) {
                        cap=cap.padLeft(5,"0")
                    }
                    def salesSpecialist= cell(row, cellaSalesSpecialist.col)


                    def ragioneSociale = cell(row, cellaRagioneSociale.col)
                    def telefono = cell(row, cellTelefono.col)
                    telefono=telefono.toString()
                    if(telefono.trim()!='null'){
                        telefono=telefono.trim().toString()
                        if(telefono.toString().contains(".")){
                            int zero=telefono.toString().indexOf(".")
                            telefono=telefono.toString().substring(0,zero)
                        }

                    }else{telefono=null}
                    def fax = cell(row, cellFax.col)
                    fax=fax.toString()
                    if(fax.trim()!='null'){
                        fax=fax.toString().trim()
                        if(fax.toString().contains(".")){
                            int zero=fax.toString().indexOf(".")
                            fax=fax.toString().substring(0,zero)
                        }
                    }else{
                        fax=null
                    }
                    def emaild=cell(row, cellmailD.col)
                    def emailsales=cell(row, cellmailSales.col)
                    def codIassicur=cell(row, cellCodIassicur.col)
                    def dealer = new Dealer(
                            codice: codice,
                            ragioneSocialeD: ragioneSociale,
                            pivaD: partitaIva,
                            indirizzoD: indirizzo,
                            localitaD: localita,
                            capD: cap,
                            provinciaD: provincia,
                            dealerType: dealerType,
                            telefonoD: telefono,
                            faxD: fax,
                            emailD: emaild,
                            emailSales: emailsales,
                            salesSpecialist: salesSpecialist,
                            percVenditore: percVenditore,
                            attivo: attivo,
                            codiceIASSICUR: codIassicur
                    )
                    if(dealer.save(flush: true)) println "Dealer creato correttamente ${codice}"
                    else println "Errore creazione Dealer: ${dealer.errors}"
                }
            }
            wb.close()
        }
         def userAdmin= new Admin()
         userAdmin.username= "ITADMIN"
         userAdmin.password="adminMach1"
         userAdmin.nome="admin"
         userAdmin.cognome="admin"
         userAdmin.passwordScaduta=false
         if(userAdmin.save(flush: true)) println "L'utente admin è stato creato"
         else println "Errore creazione utente admin: ${userAdmin.errors}"
        def userAdminPres= new Admin()
        userAdminPres.username= "ITADMINPRESS"
        userAdminPres.password="adminMach1"
        userAdminPres.nome="adminPressToday"
        userAdminPres.cognome="adminPressToday"
        userAdminPres.passwordScaduta=false

         if(userAdminPres.save(flush: true)) {
             userAdminPres.removeRuolo(Ruolo.ADMIN)
             userAdminPres.addRuolo(Ruolo.ADMINPRESSTODAY)
             println "L'utente admin presstoday è stato creato"
         }
         else println "Errore creazione utente admin posticipate: ${userAdminPres.errors}"

    }
    void caricaCodProd(){
        def prod1=new CodProdotti(codProdotto: "AA", step: "step 1", copertura: "SILVER", pacchetto: "CASH", codPacchetto: "OS030")
        if(!prod1.save()) println prod1.errors
        def prod2=new CodProdotti(codProdotto: "AB", step: "step 2", copertura: "SILVER", pacchetto: "CASH",codPacchetto: "OS040")
        if(!prod2.save()) println prod2.errors
        def prod3=new CodProdotti(codProdotto: "AC", step: "step 3", copertura: "SILVER", pacchetto: "CASH",codPacchetto: "OS050")
        if(!prod3.save()) println prod3.errors
        def prod4=new CodProdotti(codProdotto: "AD", step: "step 1", copertura: "GOLD", pacchetto: "CASH", codPacchetto: "OG030")
        if(!prod4.save()) println prod4.errors
        def prod5=new CodProdotti(codProdotto: "AE", step: "step 2", copertura: "GOLD", pacchetto: "CASH", codPacchetto: "OG040")
        if(!prod5.save()) println prod5.errors
        def prod6=new CodProdotti(codProdotto: "AF", step: "step 3", copertura: "GOLD", pacchetto: "CASH", codPacchetto: "OG050")
        if(!prod6.save()) println prod6.errors
        def prod7=new CodProdotti(codProdotto: "AG", step: "step 1", copertura: "PLATINUM", pacchetto: "CASH", codPacchetto: "OP030")
        if(!prod7.save()) println prod7.errors
        def prod8=new CodProdotti(codProdotto: "AH", step: "step 2", copertura: "PLATINUM", pacchetto: "CASH",codPacchetto: "OP040")
        if(!prod8.save()) println prod8.errors
        def prod9=new CodProdotti(codProdotto: "AI", step: "step 3", copertura: "PLATINUM", pacchetto: "CASH", codPacchetto: "OP050")
        if(!prod9.save()) println prod9.errors
        def prod10=new CodProdotti(codProdotto: "AJ", step: "step 1", copertura: "PLATINUM COLLISIONE", pacchetto: "CASH", codPacchetto: "OPC30")
        if(!prod10.save()) println prod7.errors
        def prod11=new CodProdotti(codProdotto: "AK", step: "step 2", copertura: "PLATINUM COLLISIONE", pacchetto: "CASH", codPacchetto: "OPC40")
        if(!prod11.save()) println prod8.errors
        def prod12=new CodProdotti(codProdotto: "AL", step: "step 3", copertura: "PLATINUM COLLISIONE", pacchetto: "CASH", codPacchetto: "OPC50")
        if(!prod12.save()) println prod9.errors
        def prod13=new CodProdotti(codProdotto: "AM", step: "step 1", copertura: "PLATINUM KASKO", pacchetto: "CASH", codPacchetto: "OPK30")
        if(!prod13.save()) println prod7.errors
        def prod14=new CodProdotti(codProdotto: "AN", step: "step 2", copertura: "PLATINUM KASKO", pacchetto: "CASH",codPacchetto: "OPK40")
        if(!prod14.save()) println prod8.errors
        def prod15=new CodProdotti(codProdotto: "AO", step: "step 3", copertura: "PLATINUM KASKO", pacchetto: "CASH", codPacchetto: "OPK50")
        if(!prod15.save()) println prod9.errors
        def prod16=new CodProdotti(codProdotto: "AR", step: "step 1", copertura: "SILVER", pacchetto: "LEASING", codPacchetto: "OS030")
        if(!prod16.save()) println prod1.errors
        def prod17=new CodProdotti(codProdotto: "AS", step: "step 2", copertura: "SILVER", pacchetto: "LEASING",codPacchetto: "OS040")
        if(!prod17.save()) println prod2.errors
        def prod18=new CodProdotti(codProdotto: "AT", step: "step 3", copertura: "SILVER", pacchetto: "LEASING",codPacchetto: "OS050")
        if(!prod18.save()) println prod3.errors
        def prod19=new CodProdotti(codProdotto: "AU", step: "step 1", copertura: "GOLD", pacchetto: "LEASING",codPacchetto: "OG030")
        if(!prod19.save()) println prod4.errors
        def prod20=new CodProdotti(codProdotto: "AV", step: "step 2", copertura: "GOLD", pacchetto: "LEASING",codPacchetto: "OG040")
        if(!prod20.save()) println prod5.errors
        def prod21=new CodProdotti(codProdotto: "AW", step: "step 3", copertura: "GOLD", pacchetto: "LEASING",codPacchetto: "OG050")
        if(!prod21.save()) println prod6.errors
        def prod22=new CodProdotti(codProdotto: "AX", step: "step 1", copertura: "PLATINUM", pacchetto: "LEASING",codPacchetto: "OP030")
        if(!prod22.save()) println prod7.errors
        def prod23=new CodProdotti(codProdotto: "AY", step: "step 2", copertura: "PLATINUM", pacchetto: "LEASING",codPacchetto: "OP040")
        if(!prod23.save()) println prod8.errors
        def prod24=new CodProdotti(codProdotto: "AZ", step: "step 3", copertura: "PLATINUM", pacchetto: "LEASING",codPacchetto: "OP050")
        if(!prod24.save()) println prod9.errors
        def prod25=new CodProdotti(codProdotto: "BA", step: "step 1", copertura: "PLATINUM COLLISIONE", pacchetto: "LEASING",codPacchetto: "OPC30")
        if(!prod25.save()) println prod7.errors
        def prod26=new CodProdotti(codProdotto: "BB", step: "step 2", copertura: "PLATINUM COLLISIONE", pacchetto: "LEASING",codPacchetto: "OPC40")
        if(!prod26.save()) println prod8.errors
        def prod27=new CodProdotti(codProdotto: "BC", step: "step 3", copertura: "PLATINUM COLLISIONE", pacchetto: "LEASING",codPacchetto: "OPC50")
        if(!prod27.save()) println prod9.errors
        def prod28=new CodProdotti(codProdotto: "BD", step: "step 1", copertura: "PLATINUM KASKO", pacchetto: "LEASING",codPacchetto: "OPK30")
        if(!prod28.save()) println prod7.errors
        def prod29=new CodProdotti(codProdotto: "BE", step: "step 2", copertura: "PLATINUM KASKO", pacchetto: "LEASING",codPacchetto: "OPK40")
        if(!prod29.save()) println prod8.errors
        def prod30=new CodProdotti(codProdotto: "BF", step: "step 3", copertura: "PLATINUM KASKO", pacchetto: "LEASING",codPacchetto: "OPK50")
        if(!prod30.save()) println prod9.errors
        def prod31=new CodProdotti(codProdotto: "BG", step: "step 1", copertura: "SILVER", pacchetto: "FINANZIATE", codPacchetto: "OS030")
        if(!prod31.save()) println prod1.errors
        def prod32=new CodProdotti(codProdotto: "BH", step: "step 2", copertura: "SILVER", pacchetto: "FINANZIATE", codPacchetto: "OS040")
        if(!prod32.save()) println prod2.errors
        def prod33=new CodProdotti(codProdotto: "BI", step: "step 3", copertura: "SILVER", pacchetto: "FINANZIATE", codPacchetto: "OS050")
        if(!prod33.save()) println prod3.errors
        def prod34=new CodProdotti(codProdotto: "BJ", step: "step 1", copertura: "GOLD", pacchetto: "FINANZIATE", codPacchetto: "OG030")
        if(!prod34.save()) println prod4.errors
        def prod35=new CodProdotti(codProdotto: "BK", step: "step 2", copertura: "GOLD", pacchetto: "FINANZIATE", codPacchetto: "OG040")
        if(!prod35.save()) println prod5.errors
        def prod36=new CodProdotti(codProdotto: "BL", step: "step 3", copertura: "GOLD", pacchetto: "FINANZIATE", codPacchetto: "OG050")
        if(!prod36.save()) println prod6.errors
        def prod37=new CodProdotti(codProdotto: "BM", step: "step 1", copertura: "PLATINUM", pacchetto: "FINANZIATE", codPacchetto: "OP030")
        if(!prod37.save()) println prod7.errors
        def prod38=new CodProdotti(codProdotto: "BN", step: "step 2", copertura: "PLATINUM", pacchetto: "FINANZIATE", codPacchetto: "OP040")
        if(!prod38.save()) println prod8.errors
        def prod39=new CodProdotti(codProdotto: "BO", step: "step 3", copertura: "PLATINUM", pacchetto: "FINANZIATE", codPacchetto: "OP050")
        if(!prod39.save()) println prod9.errors
        def prod40=new CodProdotti(codProdotto: "BP", step: "step 1", copertura: "PLATINUM COLLISIONE", pacchetto: "FINANZIATE", codPacchetto: "OPC30")
        if(!prod40.save()) println prod7.errors
        def prod41=new CodProdotti(codProdotto: "BQ", step: "step 2", copertura: "PLATINUM COLLISIONE", pacchetto: "FINANZIATE", codPacchetto: "OPC40")
        if(!prod41.save()) println prod8.errors
        def prod42=new CodProdotti(codProdotto: "BR", step: "step 3", copertura: "PLATINUM COLLISIONE", pacchetto: "FINANZIATE", codPacchetto: "OPC50")
        if(!prod42.save()) println prod9.errors
        def prod43=new CodProdotti(codProdotto: "BS", step: "step 1", copertura: "PLATINUM KASKO", pacchetto: "FINANZIATE", codPacchetto: "OPK30")
        if(!prod43.save()) println prod7.errors
        def prod44=new CodProdotti(codProdotto: "BT", step: "step 2", copertura: "PLATINUM KASKO", pacchetto: "FINANZIATE", codPacchetto: "OPK40")
        if(!prod44.save()) println prod8.errors
        def prod45=new CodProdotti(codProdotto: "BU", step: "step 3", copertura: "PLATINUM KASKO", pacchetto: "FINANZIATE", codPacchetto: "OPK50")
        if(!prod45.save()) println prod9.errors
    }
    void caricaZoneTarif(){
        def zona1=new ZoneTarif(provincia: "VR", zona: "1")
        if(!zona1.save()) println zona1.errors
        def zona2=new ZoneTarif(provincia: "VI", zona: "1")
        if(!zona2.save()) println zona2.errors
        def zona3=new ZoneTarif(provincia: "UD", zona: "1")
        if(!zona3.save()) println zona3.errors
        def zona4=new ZoneTarif(provincia: "TV", zona: "1")
        if(!zona4.save()) println zona4.errors
        def zona5=new ZoneTarif(provincia: "TS", zona: "1")
        if(!zona5.save()) println zona5.errors
        def zona6=new ZoneTarif(provincia: "TN", zona: "1")
        if(!zona6.save()) println zona6.errors
        def zona7=new ZoneTarif(provincia: "SI", zona: "1")
        if(!zona7.save()) println zona7.errors
        def zona8=new ZoneTarif(provincia: "RSM", zona: "1")
        if(!zona8.save()) println zona8.errors
        def zona9=new ZoneTarif(provincia: "RN", zona: "1")
        if(!zona9.save()) println zona9.errors
        def zona10=new ZoneTarif(provincia: "RE", zona: "1")
        if(!zona10.save()) println zona10.errors
        def zona11=new ZoneTarif(provincia: "RA", zona: "1")
        if(!zona11.save()) println zona11.errors
        def zona12=new ZoneTarif(provincia: "PU", zona: "1")
        if(!zona12.save()) println zona12.errors
        def zona13=new ZoneTarif(provincia: "PT", zona: "1")
        if(!zona13.save()) println zona13.errors
        def zona14=new ZoneTarif(provincia: "PR", zona: "1")
        if(!zona14.save()) println zona14.errors
        def zona15=new ZoneTarif(provincia: "PN", zona: "1")
        if(!zona15.save()) println zona15.errors
        def zona16=new ZoneTarif(provincia: "PI", zona: "1")
        if(!zona16.save()) println zona16.errors
        def zona17=new ZoneTarif(provincia: "PG", zona: "1")
        if(!zona17.save()) println zona17.errors
        def zona18=new ZoneTarif(provincia: "PC", zona: "1")
        if(!zona18.save()) println zona18.errors
        def zona19=new ZoneTarif(provincia: "MS", zona: "1")
        if(!zona19.save()) println zona19.errors
        def zona20=new ZoneTarif(provincia: "MO", zona: "1")
        if(!zona20.save()) println zona20.errors
        def zona21=new ZoneTarif(provincia: "MN", zona: "1")
        if(!zona21.save()) println zona21.errors
        def zona22=new ZoneTarif(provincia: "MC", zona: "1")
        if(!zona22.save()) println zona22.errors
        def zona23=new ZoneTarif(provincia: "LU", zona: "1")
        if(!zona23.save()) println zona23.errors
        def zona24=new ZoneTarif(provincia: "LI", zona: "1")
        if(!zona24.save()) println zona24.errors
        def zona25=new ZoneTarif(provincia: "GR", zona: "1")
        if(!zona25.save()) println zona25.errors
        def zona26=new ZoneTarif(provincia: "GO", zona: "1")
        if(!zona26.save()) println zona26.errors
        def zona27=new ZoneTarif(provincia: "FM", zona: "1")
        if(!zona27.save()) println zona27.errors
        def zona28=new ZoneTarif(provincia: "FE", zona: "1")
        if(!zona28.save()) println zona28.errors
        def zona29=new ZoneTarif(provincia: "FC", zona: "1")
        if(!zona29.save()) println zona29.errors
        def zona30=new ZoneTarif(provincia: "BZ", zona: "1")
        if(!zona30.save()) println zona30.errors
        def zona31=new ZoneTarif(provincia: "BS", zona: "1")
        if(!zona31.save()) println zona31.errors
        def zona32=new ZoneTarif(provincia: "BO", zona: "1")
        if(!zona32.save()) println zona32.errors
        def zona33=new ZoneTarif(provincia: "BL", zona: "1")
        if(!zona33.save()) println zona33.errors
        def zona34=new ZoneTarif(provincia: "AR", zona: "1")
        if(!zona34.save()) println zona34.errors
        def zona35=new ZoneTarif(provincia: "AP", zona: "1")
        if(!zona35.save()) println zona35.errors
        def zona36=new ZoneTarif(provincia: "AN", zona: "1")
        if(!zona36.save()) println zona36.errors
        def zona37=new ZoneTarif(provincia: "VS", zona: "2")
        if(!zona37.save()) println zona37.errors
        def zona38=new ZoneTarif(provincia: "VE", zona: "2")
        if(!zona38.save()) println zona38.errors
        def zona39=new ZoneTarif(provincia: "TP", zona: "2")
        if(!zona39.save()) println zona39.errors
        def zona40=new ZoneTarif(provincia: "SV", zona: "2")
        if(!zona40.save()) println zona40.errors
        def zona41=new ZoneTarif(provincia: "SS", zona: "2")
        if(!zona41.save()) println zona41.errors
        def zona42=new ZoneTarif(provincia: "SR", zona: "2")
        if(!zona42.save()) println zona42.errors
        def zona43=new ZoneTarif(provincia: "SP", zona: "2")
        if(!zona43.save()) println zona43.errors
        def zona44=new ZoneTarif(provincia: "RO", zona: "2")
        if(!zona44.save()) println zona44.errors
        def zona45=new ZoneTarif(provincia: "RG", zona: "2")
        if(!zona45.save()) println zona45.errors
        def zona46=new ZoneTarif(provincia: "PD", zona: "2")
        if(!zona46.save()) println zona46.errors
        def zona47=new ZoneTarif(provincia: "OR", zona: "2")
        if(!zona47.save()) println zona47.errors
        def zona48=new ZoneTarif(provincia: "OG", zona: "2")
        if(!zona48.save()) println zona48.errors
        def zona49=new ZoneTarif(provincia: "LT", zona: "2")
        if(!zona49.save()) println zona49.errors
        def zona50=new ZoneTarif(provincia: "IM", zona: "2")
        if(!zona50.save()) println zona50.errors
        def zona51=new ZoneTarif(provincia: "FR", zona: "2")
        if(!zona51.save()) println zona51.errors
        def zona52=new ZoneTarif(provincia: "EN", zona: "2")
        if(!zona52.save()) println zona52.errors
        def zona53=new ZoneTarif(provincia: "CL", zona: "2")
        if(!zona53.save()) println zona53.errors
        def zona54=new ZoneTarif(provincia: "CI", zona: "2")
        if(!zona54.save()) println zona54.errors
        def zona55=new ZoneTarif(provincia: "CA", zona: "2")
        if(!zona55.save()) println zona55.errors
        def zona56=new ZoneTarif(provincia: "AQ", zona: "2")
        if(!zona56.save()) println zona56.errors
        def zona57=new ZoneTarif(provincia: "AO", zona: "2")
        if(!zona57.save()) println zona57.errors
        def zona58=new ZoneTarif(provincia: "AG", zona: "2")
        if(!zona58.save()) println zona58.errors
        def zona59=new ZoneTarif(provincia: "VC", zona: "3")
        if(!zona59.save()) println zona59.errors
        def zona60=new ZoneTarif(provincia: "VB", zona: "3")
        if(!zona60.save()) println zona60.errors
        def zona61=new ZoneTarif(provincia: "VA", zona: "3")
        if(!zona61.save()) println zona61.errors
        def zona62=new ZoneTarif(provincia: "SO", zona: "3")
        if(!zona62.save()) println zona62.errors
        def zona63=new ZoneTarif(provincia: "NO", zona: "3")
        if(!zona63.save()) println zona63.errors
        def zona64=new ZoneTarif(provincia: "LC", zona: "3")
        if(!zona64.save()) println zona64.errors
        def zona65=new ZoneTarif(provincia: "GE", zona: "3")
        if(!zona65.save()) println zona65.errors
        def zona66=new ZoneTarif(provincia: "FI", zona: "3")
        if(!zona66.save()) println zona66.errors
        def zona67=new ZoneTarif(provincia: "CR", zona: "3")
        if(!zona67.save()) println zona67.errors
        def zona68=new ZoneTarif(provincia: "CO", zona: "3")
        if(!zona68.save()) println zona68.errors
        def zona69=new ZoneTarif(provincia: "CN", zona: "3")
        if(!zona69.save()) println zona69.errors
        def zona70=new ZoneTarif(provincia: "BI", zona: "3")
        if(!zona70.save()) println zona70.errors
        def zona71=new ZoneTarif(provincia: "AT", zona: "3")
        if(!zona71.save()) println zona71.errors
        def zona72=new ZoneTarif(provincia: "AL", zona: "3")
        if(!zona72.save()) println zona72.errors
        def zona73=new ZoneTarif(provincia: "VV", zona: "4")
        if(!zona73.save()) println zona73.errors
        def zona74=new ZoneTarif(provincia: "VT", zona: "4")
        if(!zona74.save()) println zona74.errors
        def zona75=new ZoneTarif(provincia: "TR", zona: "4")
        if(!zona75.save()) println zona75.errors
        def zona76=new ZoneTarif(provincia: "TE", zona: "4")
        if(!zona76.save()) println zona76.errors
        def zona77=new ZoneTarif(provincia: "RC", zona: "4")
        if(!zona77.save()) println zona77.errors
        def zona78=new ZoneTarif(provincia: "PZ", zona: "4")
        if(!zona78.save()) println zona78.errors
        def zona79=new ZoneTarif(provincia: "PV", zona: "4")
        if(!zona79.save()) println zona79.errors
        def zona80=new ZoneTarif(provincia: "PO", zona: "4")
        if(!zona80.save()) println zona80.errors
        def zona81=new ZoneTarif(provincia: "PE", zona: "4")
        if(!zona81.save()) println zona81.errors
        def zona82=new ZoneTarif(provincia: "OT", zona: "4")
        if(!zona82.save()) println zona82.errors
        def zona83=new ZoneTarif(provincia: "NU", zona: "4")
        if(!zona83.save()) println zona83.errors
        def zona84=new ZoneTarif(provincia: "MT", zona: "4")
        if(!zona84.save()) println zona84.errors
        def zona85=new ZoneTarif(provincia: "MI", zona: "4")
        if(!zona85.save()) println zona85.errors
        def zona86=new ZoneTarif(provincia: "ME", zona: "4")
        if(!zona86.save()) println zona86.errors
        def zona87=new ZoneTarif(provincia: "MB", zona: "4")
        if(!zona87.save()) println zona87.errors
        def zona88=new ZoneTarif(provincia: "LO", zona: "4")
        if(!zona88.save()) println zona88.errors
        def zona89=new ZoneTarif(provincia: "CZ", zona: "4")
        if(!zona89.save()) println zona89.errors
        def zona90=new ZoneTarif(provincia: "CT", zona: "4")
        if(!zona90.save()) println zona90.errors
        def zona91=new ZoneTarif(provincia: "CS", zona: "4")
        if(!zona91.save()) println zona91.errors
        def zona92=new ZoneTarif(provincia: "BN", zona: "4")
        if(!zona92.save()) println zona92.errors
        def zona93=new ZoneTarif(provincia: "BG", zona: "4")
        if(!zona93.save()) println zona93.errors
        def zona94=new ZoneTarif(provincia: "AV", zona: "4")
        if(!zona94.save()) println zona94.errors
        def zona95=new ZoneTarif(provincia: "PA", zona: "5")
        if(!zona95.save()) println zona95.errors
        def zona96=new ZoneTarif(provincia: "KR", zona: "5")
        if(!zona96.save()) println zona96.errors
        def zona97=new ZoneTarif(provincia: "IS", zona: "5")
        if(!zona97.save()) println zona97.errors
        def zona98=new ZoneTarif(provincia: "CH", zona: "5")
        if(!zona98.save()) println zona98.errors
        def zona99=new ZoneTarif(provincia: "CB", zona: "5")
        if(!zona99.save()) println zona99.errors
        def zona100=new ZoneTarif(provincia: "TO", zona: "6")
        if(!zona100.save()) println zona100.errors
        def zona101=new ZoneTarif(provincia: "SA", zona: "6")
        if(!zona101.save()) println zona101.errors
        def zona102=new ZoneTarif(provincia: "RM", zona: "6")
        if(!zona102.save()) println zona102.errors
        def zona103=new ZoneTarif(provincia: "RI", zona: "6")
        if(!zona103.save()) println zona103.errors
        def zona104=new ZoneTarif(provincia: "TA", zona: "7")
        if(!zona104.save()) println zona104.errors
        def zona105=new ZoneTarif(provincia: "NA", zona: "8")
        if(!zona105.save()) println zona105.errors
        def zona106=new ZoneTarif(provincia: "LE", zona: "8")
        if(!zona106.save()) println zona106.errors
        def zona107=new ZoneTarif(provincia: "FG", zona: "8")
        if(!zona107.save()) println zona107.errors
        def zona108=new ZoneTarif(provincia: "CE", zona: "8")
        if(!zona108.save()) println zona108.errors
        def zona109=new ZoneTarif(provincia: "BT", zona: "8")
        if(!zona109.save()) println zona109.errors
        def zona110=new ZoneTarif(provincia: "BR", zona: "8")
        if(!zona110.save()) println zona110.errors
        def zona111=new ZoneTarif(provincia: "BA", zona: "8")
        if(!zona111.save()) println zona111.errors
    }
    def init = { servletContext ->
        enhanceMetaClass()
        unlimitedAES()
        if(Environment.current == Environment.DEVELOPMENT) {
            //caricaExcelDealer()
            //caricaCodProd()
            // caricaAdminDealer()
            //caricaZoneTarif()
        }
        println "CASH OPEL started!"
    }
    def destroy = {
    }


    private void unlimitedAES() {
        if(!isAESRestricted()) return
        try {
            final Class<?> jceSecurity = Class.forName("javax.crypto.JceSecurity")
            final Class<?> cryptoPermissions = Class.forName("javax.crypto.CryptoPermissions")
            final Class<?> cryptoAllPermission = Class.forName("javax.crypto.CryptoAllPermission")

            final Field isRestrictedField = jceSecurity.getDeclaredField("isRestricted")
            isRestrictedField.setAccessible(true)
            isRestrictedField.set(null, false)

            final Field defaultPolicyField = jceSecurity.getDeclaredField("defaultPolicy")
            defaultPolicyField.setAccessible(true)
            final PermissionCollection defaultPolicy = (PermissionCollection) defaultPolicyField.get(null)

            final Field perms = cryptoPermissions.getDeclaredField("perms")
            perms.setAccessible(true)
            ((Map<?, ?>) perms.get(defaultPolicy)).clear()

            final Field instance = cryptoAllPermission.getDeclaredField("INSTANCE")
            instance.setAccessible(true)
            defaultPolicy.add((Permission) instance.get(null))

            log.debug "Successfully removed cryptography restrictions"
        } catch(e) {
            log.warn "Failed to remove cryptography restrictions: ${e.message}"
        }
    }

    private boolean isAESRestricted() { return "Java(TM) SE Runtime Environment" == System.getProperty("java.runtime.name") }
}
