import groovyx.net.http.RESTClient
import org.springframework.web.servlet.i18n.SessionLocaleResolver
import wslite.http.auth.HTTPBasicAuthorization
import wslite.rest.RESTClient
import wslite.http.HTTPClient
// Place your Spring DSL code here
beans = {

    localeResolver(SessionLocaleResolver) {
        Locale.default = Locale.ITALY
        defaultLocale = Locale.default
    }
    myInterceptor(org.springframework.boot.context.embedded.FilterRegistrationBean) {
        filter = bean(security.InterceptFilter)
        order = org.springframework.core.Ordered.HIGHEST_PRECEDENCE
    }
    iAssicurHTTPClient(HTTPClient) {
        sslTrustAllCerts = true
    }

    iAssicurAuthorization(HTTPBasicAuthorization) {
        username = application.config.iAssicur.username
        password = application.config.iAssicur.password
    }
    iAssicurClient(RESTClient){
        httpClient = ref("iAssicurHTTPClient")
        url = application.config.iAssicur.url
        defaultContentTypeHeader = "text/xml"
        defaultAcceptHeader = "text/xml"
        authorization = ref("iAssicurAuthorization")
    }
}
