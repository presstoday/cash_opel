package mail

import cashopel.polizze.Documenti
import cashopel.polizze.DocumentiSalesSpecialist
import cashopel.polizze.Polizza
import cashopel.polizze.PolizzaRCA
import cashopel.polizze.StatoPolizza
import cashopel.polizze.TipoPolizza
import cashopel.utenti.Log
import com.itextpdf.text.Document
import com.itextpdf.text.PageSize
import com.itextpdf.text.pdf.BaseFont
import com.itextpdf.text.pdf.PdfContentByte
import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.PdfStamper
import com.itextpdf.text.pdf.PdfWriter
import com.presstoday.groovy.Stopwatch
import excel.builder.ExcelBuilder
import grails.core.GrailsApplication
import grails.transaction.Transactional
import grails.util.Environment
import groovy.sql.Sql
import groovy.time.TimeCategory
import mandrill.MandrillAttachment
import mandrill.MandrillMessage
import mandrill.MandrillRecipient
import mandrill.MergeVar
import java.text.NumberFormat
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

@Transactional
class MailService {
    def mandrillService
    GrailsApplication grailsApplication
    def httpWrapperService
    def BASE_URL = "https://mandrillapp.com/api/1.0/"
    def dataSource_rinnovi
    def invioMailSettimanale(DocumentiSalesSpecialist documenti) {
        def logg
        def recpts = []
        def attachs=[]
        def contents = []
        def data= new Date()
        def inizio=use (TimeCategory){data - 7.days}
        def fin=use (TimeCategory){data - 3.days}
        def dataodierna = new Date()
        dataodierna=dataodierna.clearTime()
        def vars = [new MergeVar(name: "DATA1", content: "${inizio.format("dd/MM/yyyy")}"),new MergeVar(name: "DATA2", content: "${fin.format("dd/MM/yyyy")}") ]
        def fileEncode=null
        def fileNameAllegato, fileContentAllegato
        if(Environment.current == Environment.DEVELOPMENT) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))

        }else if(Environment.current == Environment.TEST) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:"Cristina", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Silvia", email: "s.carino@mach-1.it"))
        }
        else{
            def polizze = /*SqlLogger.log {*/Polizza.createCriteria().list() {
                ge "dataInserimento", dataodierna-8
                //le "dataInserimento", dataodierna-3
                le "dataInserimento", dataodierna-1
            }
            if(polizze.size()>0){
                def salesSpecialist=[:]
                polizze.each{polizza->
                    salesSpecialist.put(polizza.dealer.salesSpecialist,polizza.dealer.emailSales)

                }
                salesSpecialist.groupBy {it.value}
                salesSpecialist.each{k,v ->
                    logg =new Log(parametri: "sales specialist: destinatario -> ${k} email ${v} ", operazione: "invioMailSettimanale", pagina: "MAIL SERVICE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    recpts.add(new MandrillRecipient(name:"${k}", email: "${v}"))
                }
            }
            recpts.add(new MandrillRecipient(name:"Sales manager", email: "Michelangelo.DeFazio@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:" Sales&BD Director", email: "Paolo.Ricceri@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:"Insurance Manager", email: "Ciro.Campagna@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:"FinancialMarkenting", email: "GMFinancialMarketing@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))
        }
        fileEncode = new sun.misc.BASE64Encoder().encode(documenti.fileContent)
        attachs.add(new MandrillAttachment(type: "application/pdf", name: "${documenti.fileName}", content: fileEncode))
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco settimanale polizze Flex Protection",
                from_email:"attivazioni.ofs@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "invio-settimanale-polizze-cashopel", contents )
        def inviatoCliente = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailSettimanaleMACH1() {
        def recpts = []
        def attachs=[]
        def contents = []
        def data= new Date()
        def inizio=use (TimeCategory){data - 7.days}
        //def fin=use (TimeCategory){data - 3.days}
        def fin=use (TimeCategory){data - 3.days}
        def vars = [new MergeVar(name: "DATA1", content: "${inizio.format("dd/MM/yyyy")}"),new MergeVar(name: "DATA2", content: "${fin.format("dd/MM/yyyy")}") ]
        def fileEncode=null
        def fileNameAllegato, fileContentAllegato
        if(Environment.current == Environment.DEVELOPMENT) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
        }else  {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:"Cristina", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Silvia", email: "s.carino@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Susanna", email: "s.facciotto@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))
        }
        elencoPolizzeSettimanaleMACH1().each {fileName, fileContent ->
            fileNameAllegato = fileName
            fileContentAllegato = fileContent
        }
        fileEncode = new sun.misc.BASE64Encoder().encode(fileContentAllegato)
        attachs.add(new MandrillAttachment(type: "application/pdf", name: "${fileNameAllegato}", content: fileEncode))
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco settimanale polizze Flex Protection",
                from_email:"attivazioni.ofs@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "invio-settimanale-polizze-cashopel", contents )
        def inviatoCliente = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailSettimanaleRCAMACH1() {
        def recpts = []
        def attachs=[]
        def contents = []
        def data= new Date()
        def inizio=use (TimeCategory){data - 7.days}
        //def fin=use (TimeCategory){data - 3.days}
        def fin=use (TimeCategory){data - 3.days}
        def vars = [new MergeVar(name: "DATA1", content: "${inizio.format("dd/MM/yyyy")}"),new MergeVar(name: "DATA2", content: "${fin.format("dd/MM/yyyy")}") ]
        def fileEncode=null
        def fileNameAllegato, fileContentAllegato
        if(Environment.current == Environment.DEVELOPMENT) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
        }else if(Environment.current == Environment.TEST) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:"Cristina", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"IT", email: "it@mach-1.it "))

        }else {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:"Cristina", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Silvia", email: "s.carino@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Susanna", email: "s.facciotto@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))
        }
        elencoPolizzeSettimanaleRCAMACH1().each {fileName, fileContent ->
            fileNameAllegato = fileName
            fileContentAllegato = fileContent
        }
        fileEncode = new sun.misc.BASE64Encoder().encode(fileContentAllegato)
        attachs.add(new MandrillAttachment(type: "application/pdf", name: "${fileNameAllegato}", content: fileEncode))
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco settimanale polizze RCA Flex Protection",
                from_email:"attivazioni.ofs@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "invio-settimanale-polizze-cashopel", contents )
        def inviatoCliente = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def inviosollecitoMailSettimanaleRCAMACH1(PolizzaRCA polizza) {
        def recpts = []
        def attachs=[]
        def contents = []
        def data= new Date()
        def inizio=use (TimeCategory){data - 7.days}
        //def fin=use (TimeCategory){data - 3.days}
        def fin=use (TimeCategory){data - 3.days}
        def vars = [new MergeVar(name: "DATAC", content: "Buongiorno,<br> vi ricordiamo che siamo in attesa della documentazione (libretto e modulo di adesione) per procedere all'invio alla compagnia della richiesta di copertura rca per la pratica in oggetto.")]
        def fileEncode=null
        def fileNameAllegato, fileContentAllegato
        if(Environment.current == Environment.DEVELOPMENT) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
        }else if(Environment.current == Environment.TEST) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:"Cristina", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"IT", email: "it@mach-1.it "))

        }else {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:"${polizza.dealer.ragioneSocialeD}", email: "${polizza.dealer.emailD}"))
            recpts.add(new MandrillRecipient(name:"${polizza.dealer.salesSpecialist}", email: "${polizza.dealer.emailSales}"))
            recpts.add(new MandrillRecipient(name:"Analisi", email: "analisi@mach-1.it"))
        }
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"RICHIESTA DOCUMENTAZIONE PER L'ATTIVAZIONE DELLA POLIZZA RCA ${polizza.cognome.toUpperCase()} ${polizza.nome.toUpperCase()}, ${polizza.noPolizza}, ${polizza.telaio.toUpperCase()}",
                from_email:"attivazioni.ofs@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "elenco-certificati-inviati-email", contents )
        def inviatoCliente = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    /*def invioMailSettimanaleAUTOIMPORT(def dataInizio, def dataFine) {
        def recpts = []
        def attachs=[]
        def contents = []
        def dataI= new Date()
        def inizio=use (TimeCategory){dataI - 7.days}
        //def fin=use (TimeCategory){data - 3.days}
        def fin=use (TimeCategory){dataI - 3.days}
        def vars = [new MergeVar(name: "DATA1", content: "${inizio.format("dd/MM/yyyy")}"),new MergeVar(name: "DATA2", content: "${fin.format("dd/MM/yyyy")}") ]
        def fileEncode=null
        def fileNameAllegato, fileContentAllegato
        if(Environment.current == Environment.DEVELOPMENT) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
        }else  {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:"Cristina", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Silvia", email: "s.carino@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Susanna", email: "s.facciotto@mach-1.it"))
        }
        elencoPolizzeSettimanaleAUTOIMPORT().each {fileName, fileContent ->
            fileNameAllegato = fileName
            fileContentAllegato = fileContent
        }
        fileEncode = new sun.misc.BASE64Encoder().encode(fileContentAllegato)
        attachs.add(new MandrillAttachment(type: "application/pdf", name: "${fileNameAllegato}", content: fileEncode))
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco settimanale polizze Flex Protection",
                from_email:"attivazioni.ofs@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "invio-settimanale-polizze-cashopel", contents )
        def inviatoCliente = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
      /*  return response1
    }*/
    def invioMailMensileMACH1() {
        def recpts = []
        def attachs=[]
        def contents = []
        Calendar c = Calendar.getInstance()
        c.add(Calendar.MONTH, -1)
        Calendar cal = Calendar.getInstance()
        def dataodierna = new Date()
        dataodierna=dataodierna.clearTime()
        cal.setTime(dataodierna)
        cal.add(Calendar.MONTH,-1)
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMinimum(Calendar.DAY_OF_MONTH))
        Date firstDayOfTheMonth = cal.getTime()
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DAY_OF_MONTH))
        Date lastDayOfTheMonth = cal.getTime()
        def inizio=use (TimeCategory){firstDayOfTheMonth}
        def fin=use (TimeCategory){lastDayOfTheMonth}
        def vars = [new MergeVar(name: "DATA1", content: "${inizio.format("dd/MM/yyyy")}"),new MergeVar(name: "DATA2", content: "${fin.format("dd/MM/yyyy")}") ]
        def fileEncode=null
        def fileNameAllegato, fileContentAllegato
        if(Environment.current == Environment.DEVELOPMENT) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
        }else  {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:"Cristina", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Silvia", email: "s.carino@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Susanna", email: "s.facciotto@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))
        }
        elencoPolizzeMensileMACH1().each {fileName, fileContent ->
            fileNameAllegato = fileName
            fileContentAllegato = fileContent
        }
        fileEncode = new sun.misc.BASE64Encoder().encode(fileContentAllegato)
        attachs.add(new MandrillAttachment(type: "application/pdf", name: "${fileNameAllegato}", content: fileEncode))
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco mensile polizze Flex Protection",
                from_email:"attivazioni.ofs@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "invio-settimanale-polizze-cashopel", contents )
        def inviatoCliente = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailMensile(DocumentiSalesSpecialist documenti) {
        def logg
        def recpts = []
        def attachs=[]
        def contents = []
        def data= new Date()
        Calendar c = Calendar.getInstance()
        c.add(Calendar.MONTH, -1)
        Calendar cal = Calendar.getInstance()
        cal.setTime(data)
        cal.add(Calendar.MONTH,-1)
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMinimum(Calendar.DAY_OF_MONTH))
        Date firstDayOfTheMonth = cal.getTime()
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DAY_OF_MONTH))
        Date lastDayOfTheMonth = cal.getTime()
        def inizio=use (TimeCategory){firstDayOfTheMonth}
        def fin=use (TimeCategory){lastDayOfTheMonth}
        def vars = [new MergeVar(name: "DATA1", content: "${inizio.format("dd/MM/yyyy")}"),new MergeVar(name: "DATA2", content: "${fin.format("dd/MM/yyyy")}") ]
        def fileEncode=null
        def fileNameAllegato, fileContentAllegato
        if(Environment.current == Environment.DEVELOPMENT) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
        }else if(Environment.current == Environment.TEST) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:"Cristina", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Silvia", email: "s.carino@mach-1.it"))
        }
        else{
            def polizze = /*SqlLogger.log {*/Polizza.createCriteria().list() {
                ge "dataInserimento", firstDayOfTheMonth
                le "dataInserimento", lastDayOfTheMonth
            }
            if(polizze.size()>0){
                def salesSpecialist=[:]
                polizze.each{polizza->
                    salesSpecialist.put(polizza.dealer.salesSpecialist,polizza.dealer.emailSales)
                }
                salesSpecialist.groupBy {it.value}
                salesSpecialist.each{k,v ->
                    logg =new Log(parametri: "sales specialist: destinatario -> ${k} email ${v} ", operazione: "invioMailMensile", pagina: "MAIL SERVICE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    recpts.add(new MandrillRecipient(name:"${k}", email: "${v}"))
                }
            }
            recpts.add(new MandrillRecipient(name:"Sales manager", email: "Michelangelo.DeFazio@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:" Sales&BD Director", email: "Paolo.Ricceri@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:"Insurance Manager", email: "Ciro.Campagna@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:"FinancialMarkenting", email: "GMFinancialMarketing@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))

        }
        fileEncode = new sun.misc.BASE64Encoder().encode(documenti.fileContent)
        attachs.add(new MandrillAttachment(type: "application/pdf", name: "${documenti.fileName}", content: fileEncode))
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco mensile polizze Flex Protection",
                from_email:"attivazioni.ofs@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "invio-settimanale-polizze-cashopel", contents )
        def inviatoCliente = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailSemestraleGennaioMACH1() {
        def recpts = []
        def attachs=[]
        def contents = []
        int year = Calendar.getInstance().get(Calendar.YEAR)
        year=year-1
        Date data_inizio = Date.parse( 'dd/MM/yyyy', "01/07/${year}")
        Date data_fine = Date.parse( 'dd/MM/yyyy', "31/12/${year}")
        def inizio=use (TimeCategory){data_inizio}
        def fin=use (TimeCategory){data_fine}
        def vars = [new MergeVar(name: "DATA1", content: "${inizio.format("dd/MM/yyyy")}"),new MergeVar(name: "DATA2", content: "${fin.format("dd/MM/yyyy")}") ]
        def fileEncode=null
        def fileNameAllegato, fileContentAllegato
        if(Environment.current == Environment.DEVELOPMENT) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
        }else  {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:"Cristina", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Silvia", email: "s.carino@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Susanna", email: "s.facciotto@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))

        }
        elencoPolizzeSemestraleGennaioMACH1().each {fileName, fileContent ->
            fileNameAllegato = fileName
            fileContentAllegato = fileContent
        }
        fileEncode = new sun.misc.BASE64Encoder().encode(fileContentAllegato)
        attachs.add(new MandrillAttachment(type: "application/pdf", name: "${fileNameAllegato}", content: fileEncode))
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco semestrale polizze Flex Protection",
                from_email:"attivazioni.ofs@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "invio-settimanale-polizze-cashopel", contents )
        def inviatoCliente = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailSemestraleLuglioMACH1() {
        def recpts = []
        def attachs=[]
        def contents = []
        int year = Calendar.getInstance().get(Calendar.YEAR);
        Date data_inizio = Date.parse( 'dd/MM/yyyy', "01/01/${year}")
        Date data_fine = Date.parse( 'dd/MM/yyyy', "30/06/${year}")
        def inizio=use (TimeCategory){data_inizio}
        def fin=use (TimeCategory){data_fine}
        def vars = [new MergeVar(name: "DATA1", content: "${inizio.format("dd/MM/yyyy")}"),new MergeVar(name: "DATA2", content: "${fin.format("dd/MM/yyyy")}") ]
        def fileEncode=null
        def fileNameAllegato, fileContentAllegato
        if(Environment.current == Environment.DEVELOPMENT) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
        }else  {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:"Cristina", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Silvia", email: "s.carino@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Susanna", email: "s.facciotto@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))

        }
        elencoPolizzeSemestraleLuglioMACH1().each {fileName, fileContent ->
            fileNameAllegato = fileName
            fileContentAllegato = fileContent
        }
        fileEncode = new sun.misc.BASE64Encoder().encode(fileContentAllegato)
        attachs.add(new MandrillAttachment(type: "application/pdf", name: "${fileNameAllegato}", content: fileEncode))
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco semestrale polizze Flex Protection",
                from_email:"attivazioni.ofs@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "invio-settimanale-polizze-cashopel", contents )
        def inviatoCliente = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailSemestraleGennaio(DocumentiSalesSpecialist documenti) {
        def logg
        def recpts = []
        def attachs=[]
        def contents = []
        int year = Calendar.getInstance().get(Calendar.YEAR);
        Date data_inizio = Date.parse( 'dd/MM/yyyy', "01/07/${year}")
        Date data_fine = Date.parse( 'dd/MM/yyyy', "31/12/${year}")
        def inizio=use (TimeCategory){data_inizio}
        def fin=use (TimeCategory){data_fine}
        def vars = [new MergeVar(name: "DATA1", content: "${inizio.format("dd/MM/yyyy")}"),new MergeVar(name: "DATA2", content: "${fin.format("dd/MM/yyyy")}") ]
        def fileEncode=null
        def fileNameAllegato, fileContentAllegato
        if(Environment.current == Environment.DEVELOPMENT) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
        }else if(Environment.current == Environment.TEST) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:"Cristina", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Silvia", email: "s.carino@mach-1.it"))
        }
        else{
            def dinizio = "${year}0701"
            def dfine = "${year}1231"
            Date d_inizio = Date.parse( 'yyyyMMdd', dinizio)
            Date d_fine = Date.parse( 'yyyyMMdd', dfine)
            def polizze = /*SqlLogger.log {*/Polizza.createCriteria().list() {
                ge "dataInserimento", d_inizio
                le "dataInserimento", d_fine

            }
            if(polizze.size()>0){
                def salesSpecialist=[:]
                polizze.each{polizza->
                    salesSpecialist.put(polizza.dealer.salesSpecialist,polizza.dealer.emailSales)
                }
                salesSpecialist.groupBy {it.value}
                salesSpecialist.each{k,v ->
                    logg =new Log(parametri: "sales specialist: destinatario -> ${k} email ${v} ", operazione: "invioMailSemestraleGennaio", pagina: "MAIL SERVICE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    recpts.add(new MandrillRecipient(name:"${k}", email: "${v}"))
                }
            }
            recpts.add(new MandrillRecipient(name:"Sales manager", email: "Michelangelo.DeFazio@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:" Sales&BD Director", email: "Paolo.Ricceri@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:"Insurance Manager", email: "Ciro.Campagna@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:"FinancialMarkenting", email: "GMFinancialMarketing@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))

        }
        /*elencoPolizzeSemestrale().each {fileName, fileContent ->
            fileNameAllegato = fileName
            fileContentAllegato = fileContent
        }*/
        fileEncode = new sun.misc.BASE64Encoder().encode(documenti.fileContent)
        attachs.add(new MandrillAttachment(type: "application/pdf", name: "${documenti.fileName}", content: fileEncode))
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco semestrale polizze Flex Protection",
                from_email:"attivazioni.ofs@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "invio-settimanale-polizze-cashopel", contents )
        def inviatoCliente = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailSemestraleLuglio(DocumentiSalesSpecialist documenti) {
        def logg
        def recpts = []
        def attachs=[]
        def contents = []
        int year = Calendar.getInstance().get(Calendar.YEAR);
        Date data_inizio = Date.parse( 'dd/MM/yyyy', "01/01/${year}")
        Date data_fine = Date.parse( 'dd/MM/yyyy', "30/06/${year}")
        def inizio=use (TimeCategory){data_inizio}
        def fin=use (TimeCategory){data_fine}
        def vars = [new MergeVar(name: "DATA1", content: "${inizio.format("dd/MM/yyyy")}"),new MergeVar(name: "DATA2", content: "${fin.format("dd/MM/yyyy")}") ]
        def fileEncode=null
        def fileNameAllegato, fileContentAllegato
        if(Environment.current == Environment.DEVELOPMENT) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
        }else if(Environment.current == Environment.TEST) {
            recpts.add(new MandrillRecipient(name:"priscila", email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:"Cristina", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Silvia", email: "s.carino@mach-1.it"))
        }
        else{
            def dinizio = "${year}0101"
            def dfine = "${year}0630"
            Date d_inizio = Date.parse( 'yyyyMMdd', dinizio)
            Date d_fine = Date.parse( 'yyyyMMdd', dfine)
            def polizze = /*SqlLogger.log {*/Polizza.createCriteria().list() {
                ge "dataInserimento", d_inizio
                le "dataInserimento", d_fine

            }
            if(polizze.size()>0){
                def salesSpecialist=[:]
                polizze.each{polizza->
                    salesSpecialist.put(polizza.dealer.salesSpecialist,polizza.dealer.emailSales)
                }
                salesSpecialist.groupBy {it.value}
                salesSpecialist.each{k,v ->
                    logg =new Log(parametri: "sales specialist: destinatario -> ${k} email ${v} ", operazione: "invioMailSemestraleLuglio", pagina: "MAIL SERVICE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    recpts.add(new MandrillRecipient(name:"${k}", email: "${v}"))
                }
            }
            recpts.add(new MandrillRecipient(name:"Sales manager", email: "Michelangelo.DeFazio@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:" Sales&BD Director", email: "Paolo.Ricceri@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:"Insurance Manager", email: "Ciro.Campagna@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:"FinancialMarkenting", email: "GMFinancialMarketing@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))

        }
        /*elencoPolizzeSemestrale().each {fileName, fileContent ->
            fileNameAllegato = fileName
            fileContentAllegato = fileContent
        }*/
        fileEncode = new sun.misc.BASE64Encoder().encode(documenti.fileContent)
        attachs.add(new MandrillAttachment(type: "application/pdf", name: "${documenti.fileName}", content: fileEncode))
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco semestrale polizze Flex Protection",
                from_email:"attivazioni.ofs@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "invio-settimanale-polizze-cashopel", contents )
        def inviatoCliente = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailCertificato(Polizza polizza, Documenti documento) {
        def logg
        def recpts = []
        def attachs=[]
        def contents = []
        def inviatoDealer
        def datiDealer= "${polizza.dealer.ragioneSocialeD}"
        def data= new Date().format("dd/MM/yyyy")
        def vars = [new MergeVar(name: "DATA", content: "${data}")]
        def fileEncode=null
        def fileNameCert, fileContentCert, oggetto,response1
        if(Environment.current == Environment.DEVELOPMENT) {
            println "email dealer= ${polizza.dealer.emailD.toString()}"
            if(polizza.dealer.emailD.toString()=='' && polizza.dealer.emailD==null){
                logg =new Log(parametri: "la mail del dealer ${polizza.dealer.ragioneSocialeD} non e' impostata controlare", operazione: "invioMailCertificato", pagina: "MAIL SERVICE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
            recpts.add(new MandrillRecipient(name:"${datiDealer}", email: "priscila@presstoday.com"))
        }else if(Environment.current == Environment.TEST) {
            recpts.add(new MandrillRecipient(name:"${datiDealer}", email: "priscila@presstoday.com"))
            recpts.add(new MandrillRecipient(name:"${datiDealer}", email: "c.sivelli@mach-1.it"))
        }
        else{
            def emailDealers=[]
            if(polizza.dealer.emailD.toString()!='' && polizza.dealer.emailD != null){
                emailDealers= polizza.dealer.emailD.split(";")
                if(emailDealers.size()>0){
                    for (int ind=0; ind<emailDealers.size();ind++) {
                        logg =new Log(parametri: "dealers: email -> ${emailDealers[ind]} ", operazione: "invioMailCertificato", pagina: "MAIL SERVICE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        recpts.add(new MandrillRecipient(name:"${datiDealer}", email: "${emailDealers[ind]}"))
                    }
                }else{
                    logg =new Log(parametri: "dealer: email -> ${polizza.dealer.emailD} ", operazione: "invioMailCertificato", pagina: "MAIL SERVICE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    recpts.add(new MandrillRecipient(name:"${datiDealer}", email: "${polizza.dealer.emailD}"))
                }
            }else{
                logg =new Log(parametri: "la mail del dealer ${polizza.dealer.ragioneSocialeD} non e' impostata controllare", operazione: "invioMailCertificato", pagina: "MAIL SERVICE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
            //recpts.add(new MandrillRecipient(name:"${datiDealer}", email: "${dealer.emailD}"))
            recpts.add(new MandrillRecipient(name:"Sales specialist", email: "${polizza.dealer.emailSales}"))
            //recpts.add(new MandrillRecipient(name:"attivazioni", email: "attivazioni.ofs@mach-1.it"))
        }
            fileNameCert=documento.fileName
            fileContentCert=documento.fileContent
            //genero la mail
            def nomeFile=fileNameCert.toString().substring(0,fileNameCert.toString().indexOf(".pdf"))
            oggetto="${nomeFile} - ATTIVAZIONE FLEX PROTECTION"
            fileEncode = new sun.misc.BASE64Encoder().encode(fileContentCert)
            attachs.add(new MandrillAttachment(type: "application/pdf", name: "${fileNameCert}", content: fileEncode))
            contents.add([name:"", content:""])
            def mandrillMessage = new MandrillMessage(
                    text:"this is a text message",
                    subject:"${oggetto}",
                    from_email:"attivazioni.ofs@mach-1.it",
                    to:recpts,
                    auto_text: true,
                    attachments : attachs,
                    global_merge_vars:vars
            )
            def mailinviata = mandrillService.sendTemplate(mandrillMessage, "certificati-finanziati-leasing", contents )
            inviatoDealer = mailinviata.count { r -> r.success == false } == 0
            response1 = mailinviata.collect { response ->
                [
                        status: response?.status,
                        message: response?.message,
                        emailCliente: response?.email,
                        id: response?.id,
                        rejectReason: response?.rejectReason,
                        successCliente: response?.success,
                ].collect { k, v -> "$k: $v" }.join(", ")
            }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/

        return  response1
    }
    def generaCertIASSPDF(def polizza) {
        def logg
        //try {
        def datacaricamento = "${polizza.dataContae}"
        def dataCaric = new Date().parse("dd/MM/yyyy", datacaricamento)
        def dataIniPolizza=polizza.inizio
        def dataInizio=new Date().parse("dd/MM/yyyy", dataIniPolizza)
        def annoPolizza=dataInizio.format('y')
        def mesePolizza=dataInizio.format('MM')
        def giornoPolizza=dataInizio.format('dd')
        def annoInserimento=dataCaric.format('y')
        def meseInserimento=dataCaric.format('MM')
        def giornoInserimento=dataCaric.format('dd')
        def imposte=polizza.imposte
        def  fmt_IT = NumberFormat.getNumberInstance(Locale.ITALIAN)
        fmt_IT.setMaximumFractionDigits(2)
        def src = grailsApplication.mainContext.getResource("/pdf/DLI950000001_cert.pdf").file

        def polizzaC= Polizza.findByNoPolizza(polizza.noPolizza)
        def codDealer=""
        if(polizzaC){
            codDealer=polizzaC.dealer.codice
            if(polizzaC.tariffa==2){
                logg =new Log(parametri: "la polizza ${polizza.noPolizza} ha una tariffa 2", operazione: "generaCertIASSPDF", pagina: "JOB CASH IAssicur")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                src = grailsApplication.mainContext.getResource("/pdf/DLI950000006_cert.pdf").file
            }
        }else{
            logg =new Log(parametri: "la polizza ${polizza.noPolizza} non e' stata trovata controllare", operazione: "generaCertIASSPDF", pagina: "JOB CASH IAssicur")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }

        //def src = this.class.getResource("/pdf/DLI950000001_cert.pdf").file
        def stream = new FileInputStream(src)
        def reader = new PdfReader(stream)
        def (canvas, output, writer) = createCanvasAndStreams(reader, 1)
        canvas.saveState()
        def cliente
        cliente="${polizza.cliente.toUpperCase()}"
        def indirizzo="${polizza.indirizzo?.toUpperCase()}"
        def capCittaProvincia="${polizza.cap} ${polizza.localita.toUpperCase()} ${polizza.provincia?.toUpperCase()}"
        def valoreassicurato=polizza.valoreAssicurato?:0.0

        datiCliente(canvas, cliente, indirizzo, capCittaProvincia)
        //writeText(canvas, 347, 722, "${polizza.nome?.toUpperCase()} ${polizza.cognome?.toUpperCase()}", 10, "nero", "normal")
        //writeText(canvas, 347, 712, "${polizza.indirizzo?.toUpperCase()}", 10, "nero", "normal")
        //writeText(canvas, 347, 702, "${polizza.cap} ${polizza.localita.toUpperCase()} ${polizza.provincia?.toUpperCase()}", 10, "nero", "normal")
        writeText(canvas, 194, 617, polizza.noPolizza, 11, "nero", "bold")
        writeText(canvas, 105, 570, "${polizza.marca?.toUpperCase()} ${polizza.nota?.toUpperCase()}", 10, "nero", "normal")
        writeText(canvas, 63, 555, "${polizza.telaio?polizza.telaio.toUpperCase():""}", 10, "nero", "normal")
        writeText(canvas, 240, 555, polizza.targa?polizza.targa.toUpperCase():"", 10, "nero", "normal")
        writeText(canvas, 135, 540, polizza.cFiscale?.toUpperCase(), 10, "nero", "normal")
        if(polizza.ramo.contains("U/18") || polizza.ramo.contains("U/19") || polizza.ramo.contains("U/20") || polizza.ramo.contains("U/35")
                || polizza.ramo.contains("U/36") || polizza.ramo.contains("U/37")  || polizza.ramo.contains("U/50") || polizza.ramo.contains("U/51")
                || polizza.ramo.contains("U/52") || polizza.ramo.contains("W/52") || polizza.ramo.contains("W/67")|| polizza.ramo.contains("W/53")|| polizza.ramo.contains("W/83")
                || polizza.ramo.contains("W/68")|| polizza.ramo.contains("W/84")|| polizza.ramo.contains("W/54")|| polizza.ramo.contains("W/69")|| polizza.ramo.contains("W/85")){
            writeText(canvas, 490, 454, "x", 11, "nero", "normal")
        }else if(polizza.ramo.contains("U/12") || polizza.ramo.contains("U/13") || polizza.ramo.contains("U/14")
                || polizza.ramo.contains("U/29") || polizza.ramo.contains("U/30") || polizza.ramo.contains("U/31")
                || polizza.ramo.contains("U/44") || polizza.ramo.contains("U/45") || polizza.ramo.contains("U/46") || polizza.ramo.contains("W/46") || polizza.ramo.contains("W/61")
                || polizza.ramo.contains("W/76")|| polizza.ramo.contains("W/47") || polizza.ramo.contains("W/62")|| polizza.ramo.contains("W/77")|| polizza.ramo.contains("W/48")
                || polizza.ramo.contains("W/63")|| polizza.ramo.contains("W/79")){
            writeText(canvas, 177, 454, "x", 11, "nero", "normal")
        }else if(polizza.ramo.contains("U/15") || polizza.ramo.contains("U/16") || polizza.ramo.contains("U/17")
                || polizza.ramo.contains("U/32") || polizza.ramo.contains("U/33") || polizza.ramo.contains("U/34")
                || polizza.ramo.contains("U/47") || polizza.ramo.contains("U/48") || polizza.ramo.contains("U/49")|| polizza.ramo.contains("W/49") || polizza.ramo.contains("W/64")
                || polizza.ramo.contains("W/80") || polizza.ramo.contains("W/50") || polizza.ramo.contains("W/65") || polizza.ramo.contains("W/81") || polizza.ramo.contains("W/51")
                || polizza.ramo.contains("W/66") || polizza.ramo.contains("W/82")){
            writeText(canvas, 358, 454, "x", 11, "nero", "normal")
        }else if(polizza.ramo.contains("U/21") || polizza.ramo.contains("U/22") || polizza.ramo.contains("U/23")
                || polizza.ramo.contains("U/38") || polizza.ramo.contains("U/39") || polizza.ramo.contains("U/40")
                || polizza.ramo.contains("U/53") || polizza.ramo.contains("U/54") || polizza.ramo.contains("U/55") || polizza.ramo.contains("W/55") || polizza.ramo.contains("W/70")
                || polizza.ramo.contains("W/86") || polizza.ramo.contains("W/56") || polizza.ramo.contains("W/71") || polizza.ramo.contains("W/87") || polizza.ramo.contains("W/57")
                || polizza.ramo.contains("W/72") || polizza.ramo.contains("W/88")){
            writeText(canvas, 177, 434, "x", 11, "nero", "normal")
        }else if(polizza.ramo.contains("U/24") || polizza.ramo.contains("U/25") || polizza.ramo.contains("U/26")
                || polizza.ramo.contains("U/41") || polizza.ramo.contains("U/42") || polizza.ramo.contains("U/43")
                || polizza.ramo.contains("U/56") || polizza.ramo.contains("U/57") || polizza.ramo.contains("U/58") || polizza.ramo.contains("W/58") || polizza.ramo.contains("W/73")
                || polizza.ramo.contains("W/89") || polizza.ramo.contains("W/59") || polizza.ramo.contains("W/74") || polizza.ramo.contains("W/90") || polizza.ramo.contains("W/60")
                || polizza.ramo.contains("W/75") || polizza.ramo.contains("W/91")){
            writeText(canvas, 358, 434, "x", 11, "nero", "normal")
        }
        writeText(canvas, 228, 330, "${giornoPolizza}", 8, "nero", "normal")
        writeText(canvas, 248, 330, "${mesePolizza}", 8, "nero", "normal")
        writeText(canvas, 268, 330, "${annoPolizza}", 8, "nero", "normal")
        writeText(canvas, 367, 332, "${polizza.durata}", 8, "nero", "normal")
        writeText(canvas, 106, 306, "${fmt_IT.format(valoreassicurato)}", 8, "nero", "normal")
        writeText(canvas, 160, 271, "${polizza.premio}", 8, "nero", "normal")
        writeText(canvas, 300, 271, "${imposte}", 8, "nero", "normal")
        writeText(canvas, 478, 268, "${giornoInserimento}", 8, "nero", "normal")
        writeText(canvas, 498, 268, "${meseInserimento}", 8, "nero", "normal")
        writeText(canvas, 518, 268, "${annoInserimento}", 8, "nero", "normal")
        writeText(canvas, 100, 151, "${dataCaric.format("dd MMMM yyyy")}", 8, "nero", "normal")
        canvas = writer.getOverContent(1)
        writer.close()
        reader.close()
        def fileName
        fileName = "dealercode ${codDealer} - NR POLIZZA ${polizza.noPolizza} - ${cliente}.pdf"
        def fileContent = output.toByteArray()
            return [(fileName): fileContent]
        /*} catch(e) {
            logg =new Log(parametri: "errore try cash ${e.toString()}", operazione: "generaCertIASSPDF", pagina: "JOB CASH IAssicur")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }*/

    }
    def generaCertRinnIASSPDF(def polizza) {
        def datacaricamento = "${polizza.dataContae}"
        def dataCaric = new Date().parse("dd/MM/yyyy", datacaricamento)
        def dataIniPolizza=polizza.inizio
        def dataInizio=new Date().parse("dd/MM/yyyy", dataIniPolizza)
        def annoPolizza=dataInizio.format('y')
        def mesePolizza=dataInizio.format('MM')
        def giornoPolizza=dataInizio.format('dd')
        def annoInserimento=dataCaric.format('y')
        def meseInserimento=dataCaric.format('MM')
        def giornoInserimento=dataCaric.format('dd')
        def imposte=polizza.imposte
        def  fmt_IT = NumberFormat.getNumberInstance(Locale.ITALIAN)
        fmt_IT.setMaximumFractionDigits(2)
        //def src = this.class.getResource("/pdf/DLI950000001_cert.pdf").file
        def src = grailsApplication.mainContext.getResource("/pdf/DLI950000003_cert.pdf").file
        def dataTariffa=Date.parse("yyyy-MM-dd","2017-06-16")
        //println "dataInizio-->$dataInizio "
        //println "dataTariffa $dataTariffa"
        if (dataInizio > dataTariffa) {
            src = grailsApplication.mainContext.getResource("/pdf/DLI950000007_cert.pdf").file
        }
        def stream = new FileInputStream(src)
        def reader = new PdfReader(stream)
        def (canvas, output, writer) = createCanvasAndStreams(reader, 1)
        canvas.saveState()
        def cliente
        cliente="${polizza.cliente.toUpperCase()}"
        def indirizzo="${polizza.indirizzo?.toUpperCase()}"
        def capCittaProvincia="${polizza.cap} ${polizza.localita.toUpperCase()} ${polizza.provincia?.toUpperCase()}"
        datiCliente(canvas, cliente, indirizzo, capCittaProvincia)
        //writeText(canvas, 347, 722, "${polizza.nome?.toUpperCase()} ${polizza.cognome?.toUpperCase()}", 10, "nero", "normal")
        //writeText(canvas, 347, 712, "${polizza.indirizzo?.toUpperCase()}", 10, "nero", "normal")
        //writeText(canvas, 347, 702, "${polizza.cap} ${polizza.localita.toUpperCase()} ${polizza.provincia?.toUpperCase()}", 10, "nero", "normal")
        writeText(canvas, 194, 617, polizza.noPolizza, 11, "nero", "bold")
        writeText(canvas, 105, 570, "${polizza.marca?.toUpperCase()} ${polizza.nota?.toUpperCase()}", 10, "nero", "normal")
        writeText(canvas, 63, 556, "${polizza.telaio?polizza.telaio.toUpperCase():""}", 10, "nero", "normal")
        writeText(canvas, 240, 555, polizza.targa?polizza.targa.toUpperCase():"", 10, "nero", "normal")
        writeText(canvas, 135, 540, polizza.cFiscale?.toUpperCase(), 10, "nero", "normal")
        if(polizza.ramo.contains("U/61") || polizza.ramo.contains("U/66") || polizza.ramo.contains("W/94") || polizza.ramo.contains("W/99") || polizza.ramo.contains("D/05")
                || polizza.ramo.contains("D/10")  || polizza.ramo.contains("D/23")  || polizza.ramo.contains("D/28")){

            writeText(canvas, 490, 454, "x", 11, "nero", "normal")
        }else if(polizza.ramo.contains("U/59") || polizza.ramo.contains("U/64") || polizza.ramo.contains("W/92")
                || polizza.ramo.contains("W/97") || polizza.ramo.contains("D/03") || polizza.ramo.contains("D/08") || polizza.ramo.contains("D/21")  || polizza.ramo.contains("D/26") ){
            writeText(canvas, 177, 454, "x", 11, "nero", "normal")
        }else if(polizza.ramo.contains("U/60")
                || polizza.ramo.contains("U/65") || polizza.ramo.contains("W/93") || polizza.ramo.contains("W/98") || polizza.ramo.contains("D/04")
                || polizza.ramo.contains("D/09") || polizza.ramo.contains("D/22")  || polizza.ramo.contains("D/27")){
            writeText(canvas, 358, 454, "x", 11, "nero", "normal")
        }else if(polizza.ramo.contains("U/62") || polizza.ramo.contains("U/67") || polizza.ramo.contains("W/95")
                || polizza.ramo.contains("D/01") || polizza.ramo.contains("D/06") || polizza.ramo.contains("D/11")  || polizza.ramo.contains("D/24")  || polizza.ramo.contains("D/29")){
            writeText(canvas, 177, 434, "x", 11, "nero", "normal")
        }else if(polizza.ramo.contains("U/63")
                || polizza.ramo.contains("U/68") || polizza.ramo.contains("W/96") || polizza.ramo.contains("D/02") || polizza.ramo.contains("D/07") || polizza.ramo.contains("D/12")
                || polizza.ramo.contains("D/25")  || polizza.ramo.contains("D/30"))
        {
            writeText(canvas, 358, 434, "x", 11, "nero", "normal")
        }
        writeText(canvas, 228, 330, "${giornoPolizza}", 8, "nero", "normal")
        writeText(canvas, 248, 330, "${mesePolizza}", 8, "nero", "normal")
        writeText(canvas, 268, 330, "${annoPolizza}", 8, "nero", "normal")
        writeText(canvas, 367, 334, "${polizza.durata}", 8, "nero", "normal")
        writeText(canvas, 106, 308, "${fmt_IT.format(polizza.valoreAssicurato)}", 8, "nero", "normal")
        writeText(canvas, 160, 272, "${polizza.premio}", 8, "nero", "normal")
        writeText(canvas, 300, 272, "${imposte}", 8, "nero", "normal")
        writeText(canvas, 478, 268, "${giornoInserimento}", 8, "nero", "normal")
        writeText(canvas, 498, 268, "${meseInserimento}", 8, "nero", "normal")
        writeText(canvas, 518, 268, "${annoInserimento}", 8, "nero", "normal")
        writeText(canvas, 100, 151, "${dataCaric.format("dd MMMM yyyy")}", 8, "nero", "normal")

        canvas = writer.getOverContent(1)
        writer.close()
        reader.close()
        def fileName
        fileName = "NR POLIZZA ${polizza.noPolizza} - ${cliente}.pdf"
        def fileContent = output.toByteArray()
        return [(fileName): fileContent]
    }
    def generaWelcomeIASSPDF(def polizza) {
        def src = grailsApplication.mainContext.getResource("/pdf/welcomeletterflexprotection.pdf").file
        //src = this.class.getResource("/pdf/welcomeletterflexprotection.pdf").file
        def stream = new FileInputStream(src)
        def reader = new PdfReader(stream)
        def (canvas, output, writer) = createCanvasAndStreams(reader, 1)
        canvas.saveState()
        def cliente
            cliente="${polizza.cliente.toUpperCase()}"
        def indirizzo="${polizza.indirizzo?.toUpperCase()}"
        def capCittaProvincia="${polizza.cap} ${polizza.localita.toUpperCase()} ${polizza.provincia?.toUpperCase()}"
        datiClienteWelcome(canvas, cliente, indirizzo, capCittaProvincia)
        //writeText(canvas, 347, 722, "${polizza.nome?.toUpperCase()} ${polizza.cognome?.toUpperCase()}", 10, "nero", "normal")
        //writeText(canvas, 347, 712, "${polizza.indirizzo?.toUpperCase()}", 10, "nero", "normal")
        //writeText(canvas, 347, 702, "${polizza.cap} ${polizza.localita.toUpperCase()} ${polizza.provincia?.toUpperCase()}", 10, "nero", "normal")
        canvas = writer.getOverContent(1)
        writer.close()
        reader.close()
        def fileName
        fileName = "WELCOME LETTER ${polizza.noPolizza} - ${polizza.cliente}.pdf"

        def fileContent = output.toByteArray()
        return [(fileName): fileContent]
    }
    def generaWelcomeIASSPAIFINPDF(def polizza) {
        def src = grailsApplication.mainContext.getResource("/pdf/welcomeFINPAI.pdf").file
        //src = this.class.getResource("/pdf/welcomeletterflexprotection.pdf").file
        def stream = new FileInputStream(src)
        def reader = new PdfReader(stream)
        def (canvas, output, writer) = createCanvasAndStreams(reader, 1)
        canvas.saveState()
        def cliente
            cliente="${polizza.cliente.toUpperCase()}"
        def indirizzo="${polizza.indirizzo?.toUpperCase()}"
        def capCittaProvincia="${polizza.cap} ${polizza.localita.toUpperCase()} ${polizza.provincia?.toUpperCase()}"
        datiClienteWelcome(canvas, cliente, indirizzo, capCittaProvincia)
        //writeText(canvas, 347, 722, "${polizza.nome?.toUpperCase()} ${polizza.cognome?.toUpperCase()}", 10, "nero", "normal")
        //writeText(canvas, 347, 712, "${polizza.indirizzo?.toUpperCase()}", 10, "nero", "normal")
        //writeText(canvas, 347, 702, "${polizza.cap} ${polizza.localita.toUpperCase()} ${polizza.provincia?.toUpperCase()}", 10, "nero", "normal")
        canvas = writer.getOverContent(1)
        writer.close()
        reader.close()
        def fileName
        fileName = "WELCOME LETTER ${polizza.noPolizza} - ${polizza.cliente}.pdf"

        def fileContent = output.toByteArray()
        return [(fileName): fileContent]
    }
    def generaWelcomeIASSPAIPDF(def polizza) {
        def src = grailsApplication.mainContext.getResource("/pdf/welcomePAI.pdf").file
        //src = this.class.getResource("/pdf/welcomeletterflexprotection.pdf").file
        def stream = new FileInputStream(src)
        def reader = new PdfReader(stream)
        def (canvas, output, writer) = createCanvasAndStreams(reader, 1)
        canvas.saveState()
        def cliente
            cliente="${polizza.cliente.toUpperCase()}"
        def indirizzo="${polizza.indirizzo?.toUpperCase()}"
        def capCittaProvincia="${polizza.cap} ${polizza.localita.toUpperCase()} ${polizza.provincia?.toUpperCase()}"
        datiClienteWelcome(canvas, cliente, indirizzo, capCittaProvincia)
        //writeText(canvas, 347, 722, "${polizza.nome?.toUpperCase()} ${polizza.cognome?.toUpperCase()}", 10, "nero", "normal")
        //writeText(canvas, 347, 712, "${polizza.indirizzo?.toUpperCase()}", 10, "nero", "normal")
        //writeText(canvas, 347, 702, "${polizza.cap} ${polizza.localita.toUpperCase()} ${polizza.provincia?.toUpperCase()}", 10, "nero", "normal")
        canvas = writer.getOverContent(1)
        writer.close()
        reader.close()
        def fileName
        fileName = "WELCOME LETTER ${polizza.noPolizza} - ${polizza.cliente}.pdf"

        def fileContent = output.toByteArray()
        return [(fileName): fileContent]
    }
    def datiCliente(canvas, nome, indirizzo, capCittaProvincia, y = 695, x = 347){
        nome.splitPhrase().eachWithIndex { token, idx ->
            y -= (idx * 10)
            writeText(canvas, x, y, token, 10, "nero", "normal")
        }
        y -= 10
        indirizzo?.splitPhrase().eachWithIndex { token, idx ->
            y -= (idx * 10)
            writeText(canvas, x, y, token, 10, "nero", "normal")
        }
        y -= 10
        capCittaProvincia?.splitPhrase().eachWithIndex { token, idx ->
            y -= (idx * 10)
            writeText(canvas, x, y, token, 10, "nero", "normal")
        }
    }
    def datiClienteWelcome(canvas, nome, indirizzo, capCittaProvincia, y = 668, x = 347){
        nome.splitPhrase().eachWithIndex { token, idx ->
            y -= (idx * 10)
            writeText(canvas, x, y, token, 10, "nero", "normal")
        }
        y -= 10
        indirizzo?.splitPhrase().eachWithIndex { token, idx ->
            y -= (idx * 10)
            writeText(canvas, x, y, token, 10, "nero", "normal")
        }
        y -= 10
        capCittaProvincia?.splitPhrase().eachWithIndex { token, idx ->
            y -= (idx * 10)
            writeText(canvas, x, y, token, 10, "nero", "normal")
        }
    }
    def writeText(canvas, x, y, String text, fontSize, color, font){
        def fontsB = [
                "normal": BaseFont.HELVETICA,
                "bold": BaseFont.HELVETICA_BOLD
        ]
        def carattere = BaseFont.createFont(fontsB[font], BaseFont.CP1252, BaseFont.NOT_EMBEDDED)
        canvas.beginText()
        canvas.moveText(x as float,y as float)
        canvas.setFontAndSize(carattere, fontSize as float)
        if(color == "rosso") canvas.setRGBColorFill(192, 0, 0)
        else if (color == "bianco") canvas.setRGBColorFill(255, 255, 255)
        else if (color == "grigio") canvas.setRGBColorFill(147, 147, 147)
        else canvas.setRGBColorFill(0, 0, 0)
        canvas.showText(text)
        canvas.endText()
    }
    def createCanvasAndStreams(document, page) {
        def output = new ByteArrayOutputStream()
        def writer = new PdfStamper(document, output)
        PdfContentByte canvas = writer.getOverContent(page)
        return [canvas, output, writer]
    }
    def mergePdfAUTOIMPORT(streams) {
        def pdf = new Document(PageSize.A4.rotate())
        def output = new ByteArrayOutputStream()
        def pdfWriter = PdfWriter.getInstance(pdf, output)
        pdf.open()
        def directContent = pdfWriter.getDirectContent()
        streams.each { stream ->
            def pdfReader = new PdfReader(stream.toByteArray())
            for(def i = 1; i <= pdfReader.getNumberOfPages(); i++) {
                pdf.newPage()
                def page = pdfWriter.getImportedPage(pdfReader, i)
                directContent.addTemplate(page, 0, 0)
            }
        }
        output.flush()
        pdf.close()
        output.close()
        return output
    }
    def mergePdf(streams) {
        def pdf = new Document()
        def output = new ByteArrayOutputStream()
        def pdfWriter = PdfWriter.getInstance(pdf, output)
        pdf.open()
        def directContent = pdfWriter.getDirectContent()
        streams.each { stream ->
            def pdfReader = new PdfReader(stream.toByteArray())
            for(def i = 1; i <= pdfReader.getNumberOfPages(); i++) {
                pdf.newPage()
                def page = pdfWriter.getImportedPage(pdfReader, i)
                directContent.addTemplate(page, 0, 0)
            }
        }
        output.flush()
        pdf.close()
        output.close()
        return output
    }
    def elencoPolizzeSettimanaleMACH1(){
        def dataodierna = new Date()
        dataodierna=dataodierna.clearTime()
        def wb
        def polizze = /*SqlLogger.log {*/Polizza.createCriteria().list() {
            ge "dataInserimento", dataodierna-7
            //le "dataInserimento", dataodierna-3
            le "dataInserimento", dataodierna-3
            isNotNull "tracciato"
            isNotNull "tracciatoCompagnia"
        }
        wb = Stopwatch.log {
            ExcelBuilder.create {
                style("header") {
                    background bisque
                    font {
                        bold(true)
                    }
                }
                sheet("Polizze cash_leasing_finanziate") {
                    row(style: "header") {
                        cell("Programma")
                        cell("Prodotto")
                        cell("Polizza")
                        cell("N/R")
                        cell("Liv.")
                        cell("Ass.")
                        cell("Prov. Ass.")
                        cell("Tel.")
                        cell("CF")
                        cell("Indirizzo")
                        cell("Modello")
                        cell("Targa")
                        cell("Val. Ass.")
                        cell("Premio")
                        cell("Pacchetto")
                        cell("Zona")
                        cell("Cop.")
                        cell("Scad.")
                        cell("Conc.")
                        cell("CodConc.")
                        cell("Provv. Conc.")
                        cell("Vend.")
                        cell("Provv. Vend.")
                        cell("Stato Pol.")
                        cell("Auto Usata")
                        cell("Account GMAC")
                        cell("Finanziata")
                        cell("On Star")

                    }
                    if (polizze.size() > 0) {
                        polizze.each { polizza ->
                            def durata =polizza.durata
                            def anni="0"
                            if(durata==12){
                                anni="1"
                            }else if(durata==18){
                                anni="1,5"
                            }
                            else if(durata==24){
                                anni="2"
                            }else if(durata==30){
                                anni="2,5"
                            }
                            else if(durata==36){
                                anni="3"
                            }else if(durata==42){
                                anni="3,5"
                            }
                            else if(durata==48){
                                anni="4"
                            }else if(durata==54){
                                anni="4,5"
                            }
                            else if(durata==60){
                                anni="5"
                            }else if(durata==66){
                                anni="5,5"
                            }
                            else if(durata==72){
                                anni="6"
                            }
                            def modello="${polizza.marca.toString().toUpperCase()}  ${polizza.modello.toString().toUpperCase()}"
                            row {
                                cell("Flex Protection Programma 2016")
                                cell("Flex Protection")
                                cell(polizza.noPolizza)
                                cell("Nuovi")
                                cell("${anni}")
                                cell("${polizza.cognome.toString().toUpperCase()} ${polizza.nome.toString().toUpperCase()}")
                                cell(polizza.provincia.toString().toUpperCase())
                                cell(polizza.telefono)
                                cell(polizza.partitaIva.toString().toUpperCase())
                                cell(polizza.indirizzo.toString().toUpperCase())
                                cell(modello)
                                cell(polizza.targa?polizza.targa.toString().toUpperCase():"")
                                cell(polizza.valoreAssicurato)
                                cell(polizza.premioLordo)
                                cell(polizza.coperturaRichiesta)
                                cell(polizza.codiceZonaTerritoriale)
                                cell(polizza.dataDecorrenza)
                                cell(polizza.dataScadenza)
                                cell(polizza.dealer.ragioneSocialeD.toString().toUpperCase())
                                cell(polizza.dealer.codice)
                                cell(polizza.provvDealer)
                                cell(polizza.venditore.toString().toUpperCase())
                                cell(polizza.provvVenditore)
                                cell(polizza.dSlip?polizza.dSlip.toString():"")
                                cell(polizza.nuovo? "N":"S")
                                cell(polizza.dealer.salesSpecialist.toString().toUpperCase())
                                cell((polizza.tipoPolizza==TipoPolizza.FINANZIATE)?"S":"N")
                                cell((polizza.onStar)?"Y":"N")
                            }
                            polizza.discard()
                        }
                        for (int i = 0; i < 17; i++) {
                            sheet.autoSizeColumn(i);
                        }
                    }
                    else {
                        row {
                            cell("non ci sono polizze attivate")
                        }
                    }

                }

            }
        }
        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        def fileName = "output_${new Date().format("yyyyMMdd")}_VerificaSettimanale_DSLIP.xlsx"
        def fileContent = stream.toByteArray()
        return [(fileName): fileContent]

    }
    def elencoPolizzeSettimanaleRCAMACH1(){
        def dataodierna = new Date()
        dataodierna=dataodierna.clearTime()
        def wb
        def polizze = /*SqlLogger.log {*/PolizzaRCA.createCriteria().list() {
            ge "dataInserimento", dataodierna-7
            //le "dataInserimento", dataodierna-3
            le "dataInserimento", dataodierna-3
           eq "stato", StatoPolizza.POLIZZA
        }
        wb = Stopwatch.log {
            ExcelBuilder.create {
                style("header") {
                    background bisque
                    font {
                        bold(true)
                    }
                }
                sheet("Polizze cash_leasing_finanziate") {
                    row(style: "header") {
                        cell("Programma")
                        cell("Prodotto")
                        cell("Polizza")
                        cell("N/R")
                        cell("Liv.")
                        cell("Ass.")
                        cell("Prov. Ass.")
                        cell("Tel.")
                        cell("CF")
                        cell("Indirizzo")
                        cell("Modello")
                        cell("Targa")
                        cell("Val. Ass.")
                        cell("Premio")
                        cell("Pacchetto")
                        cell("Zona")
                        cell("Cop.")
                        cell("Scad.")
                        cell("Conc.")
                        cell("CodConc.")
                        cell("Provv. Conc.")
                        cell("Vend.")
                        cell("Provv. Vend.")
                        cell("Stato Pol.")
                        cell("Auto Usata")
                        cell("Account GMAC")
                        cell("Finanziata")
                        cell("On Star")

                    }
                    if (polizze.size() > 0) {
                        polizze.each { polizza ->
                            def modello=polizza.modello.toString().toUpperCase()
                            def modellor=polizza.modello.toString().toUpperCase()
                            if(modello.contains("ANTARA") && !polizza.dacsv){
                                modellor="ANTARA"
                            }else if(modello.contains("ADAM") && !polizza.dacsv){
                                modellor="ADAM"
                            }else if(modello.contains("ASTRA") && !polizza.dacsv){
                                modellor="ASTRA"
                            }else if(modello.contains("CASCADA")&& !polizza.dacsv){
                                modellor="CASCADA"
                            }else if(modello.contains("COMBO")&& !polizza.dacsv){
                                modellor="COMBO"
                            }else if(modello.contains("CORSA")&& !polizza.dacsv){
                                modellor="CORSA"
                            }else if(modello.contains("CROSSLAND X") && !polizza.dacsv){
                                modellor="CROSSLAND X"
                            }else if(modello.contains("INSIGNIA")&& !polizza.dacsv){
                                modellor="INSIGNIA"
                            }else if(modello.contains("KARL")&& !polizza.dacsv){
                                modellor="KARL"
                            }else if(modello.contains("MERIVA")&& !polizza.dacsv){
                                modellor="MERIVA"
                            }else if(modello.contains("MOKKA")&& !polizza.dacsv){
                                modellor="MOKKA"
                            }else if(modello.contains("ZAFIRA") && !polizza.dacsv){
                                modellor="ZAFIRA"
                            }else if(modello.contains("MOVANO") && !polizza.dacsv){
                                modellor="MOVANO"
                            }else if(modello.contains("VIVARO") && !polizza.dacsv){
                                modellor="VIVARO"
                            }else if(modello.contains("AGILA") && !polizza.dacsv){
                                modellor="AGILA"
                            }else if(modello.contains("GRANDLAND X")&& !polizza.dacsv){
                                modellor="GRANDLAND X"
                            }
                            def durata =polizza.durata
                            def anni="0"
                            if(durata==12){
                                anni="1"
                            }else if(durata==18){
                                anni="1,5"
                            }
                            else if(durata==24){
                                anni="2"
                            }else if(durata==30){
                                anni="2,5"
                            }
                            else if(durata==36){
                                anni="3"
                            }else if(durata==42){
                                anni="3,5"
                            }
                            else if(durata==48){
                                anni="4"
                            }else if(durata==54){
                                anni="4,5"
                            }
                            else if(durata==60){
                                anni="5"
                            }else if(durata==66){
                                anni="5,5"
                            }
                            else if(durata==72){
                                anni="6"
                            }
                            def modelloPol="${polizza.marca.toString().toUpperCase()}  ${modellor}"
                            row {
                                cell("Flex Protection Programma 2016")
                                cell("Flex Protection")
                                cell(polizza.noPolizza)
                                cell("Nuovi")
                                cell("${anni}")
                                cell("${polizza.cognome.toString().toUpperCase()} ${polizza.nome.toString().toUpperCase()}")
                                cell(polizza.provincia.toString().toUpperCase())
                                cell(polizza.telefono)
                                cell(polizza.partitaIva.toString().toUpperCase())
                                cell(polizza.indirizzo.toString().toUpperCase())
                                cell(modelloPol)
                                cell(polizza.targa?polizza.targa.toString().toUpperCase():"")
                                cell(polizza.valoreAssicurato)
                                cell(polizza.premioLordo)
                                cell(polizza.coperturaRichiesta)
                                cell(polizza.codiceZonaTerritoriale)
                                cell(polizza.dataDecorrenza)
                                cell(polizza.dataScadenza)
                                cell(polizza.dealer.ragioneSocialeD.toString().toUpperCase())
                                cell(polizza.dealer.codice)
                                cell(polizza.provvDealer)
                                cell(polizza.venditore.toString().toUpperCase())
                                cell(polizza.provvVenditore)
                                cell(polizza.dSlip?polizza.dSlip.toString():"")
                                cell(polizza.nuovo? "N":"S")
                                cell(polizza.dealer.salesSpecialist.toString().toUpperCase())
                                cell((polizza.tipoPolizza==TipoPolizza.FINANZIATE)?"S":"N")
                                cell((polizza.onStar)?"Y":"N")
                            }
                            polizza.discard()
                        }
                        for (int i = 0; i < 17; i++) {
                            sheet.autoSizeColumn(i)
                        }
                    }
                    else {
                        row {
                            cell("non ci sono polizze attivate")
                        }
                    }

                }

            }
        }
        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        def fileName = "output_${new Date().format("yyyyMMdd")}_VerificaSettimanale_DSLIP.xlsx"
        def fileContent = stream.toByteArray()
        return [(fileName): fileContent]

    }
    def elencoPolizzeMensileMACH1(){
        def dataodierna = new Date()
        dataodierna=dataodierna.clearTime()
        def c = Calendar.getInstance()
        c.add(Calendar.MONTH, -1)
        def cal = Calendar.getInstance()
        cal.setTime(dataodierna)
        cal.add(Calendar.MONTH,-1)
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMinimum(Calendar.DAY_OF_MONTH))
        def firstDayOfTheMonth = cal.getTime()
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DAY_OF_MONTH))
        def lastDayOfTheMonth = cal.getTime()
        def polizze = /*SqlLogger.log {*/Polizza.createCriteria().list() {
            ge "dataInserimento", firstDayOfTheMonth
            le "dataInserimento", lastDayOfTheMonth
            isNotNull "tracciato"
            isNotNull "tracciatoCompagnia"

        }
        def wb
        wb = Stopwatch.log {
            ExcelBuilder.create {
                style("header") {
                    background bisque
                    font {
                        bold(true)
                    }
                }
                sheet("Polizze cash_leasing_finanziate") {
                    row(style: "header") {
                        cell("Programma")
                        cell("Prodotto")
                        cell("Polizza")
                        cell("N/R")
                        cell("Liv.")
                        cell("Ass.")
                        cell("Prov. Ass.")
                        cell("Tel.")
                        cell("CF")
                        cell("Indirizzo")
                        cell("Modello")
                        cell("Targa")
                        cell("Val. Ass.")
                        cell("Premio")
                        cell("Pacchetto")
                        cell("Zona")
                        cell("Cop.")
                        cell("Scad.")
                        cell("Conc.")
                        cell("CodConc.")
                        cell("Provv. Conc.")
                        cell("Vend.")
                        cell("Provv. Vend.")
                        cell("Stato Pol.")
                        cell("Auto Usata")
                        cell("Account GMAC")
                        cell("Finanziata")
                        cell("On Star")

                    }
                    if (polizze.size() > 0) {
                        polizze.each { polizza ->
                            def durata =polizza.durata
                            def anni="0"
                            if(durata==12){
                                anni="1"
                            }else if(durata==18){
                                anni="1,5"
                            }
                            else if(durata==24){
                                anni="2"
                            }else if(durata==30){
                                anni="2,5"
                            }
                            else if(durata==36){
                                anni="3"
                            }else if(durata==42){
                                anni="3,5"
                            }
                            else if(durata==48){
                                anni="4"
                            }else if(durata==54){
                                anni="4,5"
                            }
                            else if(durata==60){
                                anni="5"
                            }else if(durata==66){
                                anni="5,5"
                            }
                            else if(durata==72){
                                anni="6"
                            }
                            def modello="${polizza.marca.toString().toUpperCase()}  ${polizza.modello.toString().toUpperCase()}"
                            row {
                                cell("Flex Protection Programma 2016")
                                cell("Flex Protection")
                                cell(polizza.noPolizza)
                                cell("Nuovi")
                                cell("${anni}")
                                cell("${polizza.cognome.toString().toUpperCase()} ${polizza.nome.toString().toUpperCase()}")
                                cell(polizza.provincia.toString().toUpperCase())
                                cell(polizza.telefono)
                                cell(polizza.partitaIva.toString().toUpperCase())
                                cell(polizza.indirizzo.toString().toUpperCase())
                                cell(modello)
                                cell(polizza.targa.toString().toUpperCase())
                                cell(polizza.valoreAssicurato)
                                cell(polizza.premioLordo)
                                cell(polizza.coperturaRichiesta)
                                cell(polizza.codiceZonaTerritoriale)
                                cell(polizza.dataDecorrenza)
                                cell(polizza.dataScadenza)
                                cell(polizza.dealer.ragioneSocialeD.toString().toUpperCase())
                                cell(polizza.dealer.codice)
                                cell(polizza.provvDealer)
                                cell(polizza.venditore.toString().toUpperCase())
                                cell(polizza.provvVenditore)
                                cell(polizza.dSlip?polizza.dSlip.toString():"")
                                cell(polizza.nuovo? "N":"S")
                                cell(polizza.dealer.salesSpecialist.toString().toUpperCase())
                                cell((polizza.tipoPolizza==TipoPolizza.FINANZIATE)?"S":"N")
                                cell((polizza.onStar)?"Y":"N")
                            }
                            polizza.discard()
                        }
                        for (int i = 0; i < 17; i++) {
                            sheet.autoSizeColumn(i);
                        }
                    }
                    else {
                        row {
                            cell("non ci sono polizze attivate")
                        }
                    }

                }

            }
        }
        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        def fileName = "output_${new Date().format("yyyyMMdd")}_VerificaMensile_DSLIP.xlsx"
        def fileContent = stream.toByteArray()
        return [(fileName): fileContent]

    }
    def elencoPolizzeSemestraleGennaioMACH1(){
        int year = Calendar.getInstance().get(Calendar.YEAR)
        year=year-1
        def inizio = "${year}0701"
        def fine = "${year}1231"
        Date data_inizio = Date.parse( 'yyyyMMdd', inizio)
        Date data_fine = Date.parse( 'yyyyMMdd', fine)
        def wb
        def polizze = /*SqlLogger.log {*/Polizza.createCriteria().list() {
            ge "dataInserimento", data_inizio
            le "dataInserimento", data_fine
            isNotNull "tracciato"
            isNotNull "tracciatoCompagnia"

        }
        wb = Stopwatch.log {
            ExcelBuilder.create {
                style("header") {
                    background bisque
                    font {
                        bold(true)
                    }
                }
                sheet("Polizze cash_leasing_finanziate") {
                    row(style: "header") {
                        cell("Programma")
                        cell("Prodotto")
                        cell("Polizza")
                        cell("N/R")
                        cell("Liv.")
                        cell("Ass.")
                        cell("Prov. Ass.")
                        cell("Tel.")
                        cell("CF")
                        cell("Indirizzo")
                        cell("Modello")
                        cell("Targa")
                        cell("Val. Ass.")
                        cell("Premio")
                        cell("Pacchetto")
                        cell("Zona")
                        cell("Cop.")
                        cell("Scad.")
                        cell("Conc.")
                        cell("CodConc.")
                        cell("Provv. Conc.")
                        cell("Vend.")
                        cell("Provv. Vend.")
                        cell("Stato Pol.")
                        cell("Auto Usata")
                        cell("Account GMAC")
                        cell("Finanziata")
                        cell("On Star")

                    }
                    if (polizze.size() > 0) {
                        polizze.each { polizza ->
                            def durata =polizza.durata
                            def anni="0"
                            if(durata==12){
                                anni="1"
                            }else if(durata==18){
                                anni="1,5"
                            }
                            else if(durata==24){
                                anni="2"
                            }else if(durata==30){
                                anni="2,5"
                            }
                            else if(durata==36){
                                anni="3"
                            }else if(durata==42){
                                anni="3,5"
                            }
                            else if(durata==48){
                                anni="4"
                            }else if(durata==54){
                                anni="4,5"
                            }
                            else if(durata==60){
                                anni="5"
                            }else if(durata==66){
                                anni="5,5"
                            }
                            else if(durata==72){
                                anni="6"
                            }
                            def modello="${polizza.marca.toString().toUpperCase()}  ${polizza.modello.toString().toUpperCase()}"
                            row {
                                cell("Flex Protection Programma 2016")
                                cell("Flex Protection")
                                cell(polizza.noPolizza)
                                cell("Nuovi")
                                cell("${anni}")
                                cell("${polizza.cognome.toString().toUpperCase()} ${polizza.nome.toString().toUpperCase()}")
                                cell(polizza.provincia.toString().toUpperCase())
                                cell(polizza.telefono)
                                cell(polizza.partitaIva.toString().toUpperCase())
                                cell(polizza.indirizzo.toString().toUpperCase())
                                cell(modello)
                                cell(polizza.targa?polizza.targa.toString().toUpperCase():"")
                                cell(polizza.valoreAssicurato)
                                cell(polizza.premioLordo)
                                cell(polizza.coperturaRichiesta)
                                cell(polizza.codiceZonaTerritoriale)
                                cell(polizza.dataDecorrenza)
                                cell(polizza.dataScadenza)
                                cell(polizza.dealer.ragioneSocialeD.toString().toUpperCase())
                                cell(polizza.dealer.codice)
                                cell(polizza.provvDealer)
                                cell(polizza.venditore.toString().toUpperCase())
                                cell(polizza.provvVenditore)
                                cell(polizza.dSlip?polizza.dSlip.toString():"")
                                cell(polizza.nuovo? "N":"S")
                                cell(polizza.dealer.salesSpecialist.toString().toUpperCase())
                                cell((polizza.tipoPolizza==TipoPolizza.FINANZIATE)?"S":"N")
                                cell((polizza.onStar)?"Y":"N")
                            }
                            polizza.discard()
                        }
                        for (int i = 0; i < 17; i++) {
                            sheet.autoSizeColumn(i);
                        }
                    }
                    else {
                        row {
                            cell("non ci sono polizze attivate")
                        }
                    }

                }

            }
        }
        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        def fileName = "output_${data_fine.format("yyyyMMdd")}_VerificaSemestrale1_DSLIP.xlsx"
        def fileContent = stream.toByteArray()
        return [(fileName): fileContent]

    }
    def elencoPolizzeSemestraleLuglioMACH1(){
        int year = Calendar.getInstance().get(Calendar.YEAR);
        def inizio = "${year}0101"
        def fine = "${year}0630"
        Date data_inizio = Date.parse( 'yyyyMMdd', inizio)
        Date data_fine = Date.parse( 'yyyyMMdd', fine)
        def wb
        def polizze = /*SqlLogger.log {*/Polizza.createCriteria().list() {
            ge "dataInserimento", data_inizio
            le "dataInserimento", data_fine
            isNotNull "tracciato"
            isNotNull "tracciatoCompagnia"

        }
        wb = Stopwatch.log {
            ExcelBuilder.create {
                style("header") {
                    background bisque
                    font {
                        bold(true)
                    }
                }
                sheet("Polizze cash_leasing_finanziate") {
                    row(style: "header") {
                        cell("Programma")
                        cell("Prodotto")
                        cell("Polizza")
                        cell("N/R")
                        cell("Liv.")
                        cell("Ass.")
                        cell("Prov. Ass.")
                        cell("Tel.")
                        cell("CF")
                        cell("Indirizzo")
                        cell("Modello")
                        cell("Targa")
                        cell("Val. Ass.")
                        cell("Premio")
                        cell("Pacchetto")
                        cell("Zona")
                        cell("Cop.")
                        cell("Scad.")
                        cell("Conc.")
                        cell("CodConc.")
                        cell("Provv. Conc.")
                        cell("Vend.")
                        cell("Provv. Vend.")
                        cell("Stato Pol.")
                        cell("Auto Usata")
                        cell("Account GMAC")
                        cell("Finanziata")
                        cell("On Star")

                    }
                    if (polizze.size() > 0) {
                        polizze.each { polizza ->
                            def durata =polizza.durata
                            def anni="0"
                            if(durata==12){
                                anni="1"
                            }else if(durata==18){
                                anni="1,5"
                            }
                            else if(durata==24){
                                anni="2"
                            }else if(durata==30){
                                anni="2,5"
                            }
                            else if(durata==36){
                                anni="3"
                            }else if(durata==42){
                                anni="3,5"
                            }
                            else if(durata==48){
                                anni="4"
                            }else if(durata==54){
                                anni="4,5"
                            }
                            else if(durata==60){
                                anni="5"
                            }else if(durata==66){
                                anni="5,5"
                            }
                            else if(durata==72){
                                anni="6"
                            }
                            def modello="${polizza.marca.toString().toUpperCase()}  ${polizza.modello.toString().toUpperCase()}"
                            row {
                                cell("Flex Protection Programma 2016")
                                cell("Flex Protection")
                                cell(polizza.noPolizza)
                                cell("Nuovi")
                                cell("${anni}")
                                cell("${polizza.cognome.toString().toUpperCase()} ${polizza.nome.toString().toUpperCase()}")
                                cell(polizza.provincia.toString().toUpperCase())
                                cell(polizza.telefono)
                                cell(polizza.partitaIva.toString().toUpperCase())
                                cell(polizza.indirizzo.toString().toUpperCase())
                                cell(modello)
                                cell(polizza.targa?polizza.targa.toString().toUpperCase():"")
                                cell(polizza.valoreAssicurato)
                                cell(polizza.premioLordo)
                                cell(polizza.coperturaRichiesta)
                                cell(polizza.codiceZonaTerritoriale)
                                cell(polizza.dataDecorrenza)
                                cell(polizza.dataScadenza)
                                cell(polizza.dealer.ragioneSocialeD.toString().toUpperCase())
                                cell(polizza.dealer.codice)
                                cell(polizza.provvDealer)
                                cell(polizza.venditore.toString().toUpperCase())
                                cell(polizza.provvVenditore)
                                cell(polizza.dSlip?polizza.dSlip.toString():"")
                                cell(polizza.nuovo? "N":"S")
                                cell(polizza.dealer.salesSpecialist.toString().toUpperCase())
                                cell((polizza.tipoPolizza==TipoPolizza.FINANZIATE)?"S":"N")
                                cell((polizza.onStar)?"Y":"N")
                            }
                            polizza.discard()
                        }
                        for (int i = 0; i < 17; i++) {
                            sheet.autoSizeColumn(i);
                        }
                    }
                    else {
                        row {
                            cell("non ci sono polizze attivate")
                        }
                    }

                }

            }
        }
        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        def fileName = "output_${new Date().format("yyyyMMdd")}_VerificaSemestrale2_DSLIP.xlsx"
        def fileContent = stream.toByteArray()
        return [(fileName): fileContent]

    }
    def invioMailCaricamento(risposta, filename, boolean cash) {
        def streamRiassunti = new ByteArrayOutputStream()
        def zipRiassunti = new ZipOutputStream(streamRiassunti)
        def rispostaFinale, fileNameRisposta
        rispostaFinale=risposta
        fileNameRisposta = filename.toString().substring(0,filename.toString().indexOf(".txt"))
        zipRiassunti.putNextEntry(new ZipEntry(filename))
        zipRiassunti.write(rispostaFinale.bytes)
        zipRiassunti.close()
        streamRiassunti.close()
        def recpts = []
        def attachs=[]
        def contents = []
        def fileEncode = null
        def oggetto=""
        if(cash){
            oggetto="Elenco certificati generati CASH"
        }else{
            oggetto="Elenco certificati generati Finanziati/Leasing"

        }
        def vars = [new MergeVar(name: "DATAC", content: "In allegato un riassunto dei certificati inviati per email in data: ${new Date().format("dd-MMM-yyyy")}")]
        fileEncode = new sun.misc.BASE64Encoder().encode(streamRiassunti.toByteArray())
        attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "${fileNameRisposta}.zip", content: fileEncode))
        if(Environment.current == Environment.PRODUCTION) {
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))
        }else if (Environment.current==Environment.TEST){
            recpts.add(new MandrillRecipient(name:"Cristina Sivelli", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }else if (Environment.current==Environment.DEVELOPMENT){
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }

        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"${oggetto}",
                from_email:"documenti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "elenco-certificati-inviati-email", contents )
        def inviatoD = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailCaricamentoPolizzaFinLeas(risposta, filename) {
        def streamRiassunti = new ByteArrayOutputStream()
        def zipRiassunti = new ZipOutputStream(streamRiassunti)
        def rispostaFinale, fileNameRisposta
        rispostaFinale=risposta
        fileNameRisposta = filename.toString().substring(0,filename.toString().indexOf(".txt"))
        zipRiassunti.putNextEntry(new ZipEntry(filename))
        zipRiassunti.write(rispostaFinale.bytes)
        zipRiassunti.close()
        streamRiassunti.close()
        def recpts = []
        def attachs=[]
        def contents = []
        def fileEncode = null
        def oggetto=""
        oggetto="Elenco Polizze caricate Finanziati/Leasing"
        def vars = [new MergeVar(name: "DATAC", content: "In allegato un riassunto delle polizze caricate in data: ${new Date().format("dd-MMM-yyyy")}")]
        fileEncode = new sun.misc.BASE64Encoder().encode(streamRiassunti.toByteArray())
        attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "${fileNameRisposta}.zip", content: fileEncode))
        if(Environment.current == Environment.PRODUCTION) {
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))
        }else if (Environment.current==Environment.TEST){
            recpts.add(new MandrillRecipient(name:"Cristina Sivelli", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"IT", email: "it@mach-1.it"))
            //recpts.add(new MandrillRecipient(name:"analisi", email: "analisi@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }else if (Environment.current==Environment.DEVELOPMENT){
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }

        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"${oggetto}",
                from_email:"documenti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "elenco-certificati-inviati-email", contents )
        def inviatoD = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailAggiornamentoPolizzeRCA(risposta, filename) {
        def streamRiassunti = new ByteArrayOutputStream()
        def zipRiassunti = new ZipOutputStream(streamRiassunti)
        def rispostaFinale, fileNameRisposta
        rispostaFinale=risposta
        fileNameRisposta = filename.toString().substring(0,filename.toString().indexOf(".txt"))
        zipRiassunti.putNextEntry(new ZipEntry(filename))
        zipRiassunti.write(rispostaFinale.bytes)
        zipRiassunti.close()
        streamRiassunti.close()
        def recpts = []
        def attachs=[]
        def contents = []
        def fileEncode = null
        def oggetto=""
        oggetto="Elenco Polizze RCA aggiornate"
        def vars = [new MergeVar(name: "DATAC", content: "In allegato un riassunto delle polizze aggiornate in data: ${new Date().format("dd-MMM-yyyy")}")]
        fileEncode = new sun.misc.BASE64Encoder().encode(streamRiassunti.toByteArray())
        attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "${fileNameRisposta}.zip", content: fileEncode))
        if(Environment.current == Environment.PRODUCTION) {
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))
        }else if (Environment.current==Environment.TEST){
            recpts.add(new MandrillRecipient(name:"Cristina Sivelli", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"IT", email: "it@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"analisi", email: "analisi@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }else if (Environment.current==Environment.DEVELOPMENT){
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }

        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"${oggetto}",
                from_email:"documenti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "elenco-certificati-inviati-email", contents )
        def inviatoD = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailCaricamentoPolizzePaiPag(risposta, filename) {
        def streamRiassunti = new ByteArrayOutputStream()
        def zipRiassunti = new ZipOutputStream(streamRiassunti)
        def rispostaFinale, fileNameRisposta
        rispostaFinale=risposta
        fileNameRisposta = filename.toString().substring(0,filename.toString().indexOf(".txt"))
        zipRiassunti.putNextEntry(new ZipEntry(filename))
        zipRiassunti.write(rispostaFinale.bytes)
        zipRiassunti.close()
        streamRiassunti.close()
        def recpts = []
        def attachs=[]
        def contents = []
        def fileEncode = null
        def oggetto=""
        oggetto="Elenco Polizze caricate PAI Pagamento"
        def vars = [new MergeVar(name: "DATAC", content: "In allegato un riassunto delle polizze caricate in data: ${new Date().format("dd-MMM-yyyy")}")]
        fileEncode = new sun.misc.BASE64Encoder().encode(streamRiassunti.toByteArray())
        attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "${fileNameRisposta}.zip", content: fileEncode))
        if(Environment.current == Environment.PRODUCTION) {
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))
        }else if (Environment.current==Environment.TEST){
            recpts.add(new MandrillRecipient(name:"Cristina Sivelli", email: "c.sivelli@mach-1.it"))
            //recpts.add(new MandrillRecipient(name:"IT", email: "it@mach-1.it"))
           // recpts.add(new MandrillRecipient(name:"analisi", email: "analisi@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }else if (Environment.current==Environment.DEVELOPMENT){
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }

        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"${oggetto}",
                from_email:"documenti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "elenco-certificati-inviati-email", contents )
        def inviatoD = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailCaricamentoMach1(risposta, filename, boolean cash, streamCertMach1=null) {

        def streamRiassunti = new ByteArrayOutputStream()
        def zipRiassunti = new ZipOutputStream(streamRiassunti)
        def rispostaFinale, fileNameRisposta, fileNameZip
        if(cash){
            fileNameZip = "CASH_${new Date().format("ddMMyyyyHms")}"

        }else{
            fileNameZip = "FINANZIATE_${new Date().format("ddMMyyyyHms")}"

        }
        rispostaFinale=risposta
        fileNameRisposta = filename.toString().substring(0,filename.toString().indexOf(".txt"))
        zipRiassunti.putNextEntry(new ZipEntry(filename))
        zipRiassunti.write(rispostaFinale.bytes)
        zipRiassunti.close()
        streamRiassunti.close()
        def recpts = []
        def attachs=[]
        def contents = []
        def fileEncode = null
        def oggetto=""
        if(cash){
            oggetto="Elenco certificati inviati CASH"
        }else{
            oggetto="Elenco certificati inviati Finanziati/Leasing"

        }
        def vars = [new MergeVar(name: "DATAC", content: "In allegato un riassunto dei certificati generati in data: ${new Date().format("dd-MMM-yyyy")} e un file contenente dei certificati a campione")]
        fileEncode = new sun.misc.BASE64Encoder().encode(streamRiassunti.toByteArray())
        attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "${fileNameRisposta}.zip", content: fileEncode))
        if(streamCertMach1!=null){
            fileEncode = new sun.misc.BASE64Encoder().encode(streamCertMach1.toByteArray())
            attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "${fileNameZip}.zip", content: fileEncode))
        }
        if(Environment.current == Environment.PRODUCTION) {
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))
        }else if (Environment.current==Environment.TEST){
            recpts.add(new MandrillRecipient(name:"Cristina Sivelli", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }else if (Environment.current==Environment.DEVELOPMENT){
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }

        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"${oggetto}",
                from_email:"documenti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "elenco-certificati-inviati-email", contents )
        def inviatoD = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailGMFGENMach1(caricamentoftp, filename=null,  streamGMFGENMach1=null) {
        def logg
        def recpts = []
        def attachs=[]
        def contents = []
        def fileEncode = null
        def oggetto=""

        oggetto="REPORT GMFGEN"
        def vars
        if(caricamentoftp){
            vars = [new MergeVar(name: "DATAC", content: "Il report GMFGEN, e' stato generato potete trovarlo nel server ftp : /flussi-nais/FLUSSI cashOpel/GMFGEN")]

        }else{
            vars = [new MergeVar(name: "DATAC", content: "Il report GMFGEN non è stato generato, chiamare l'assitenza")]
        }

        /*if(streamGMFGENMach1 != null){
            def filecontent=streamGMFGENMach1.toByteArray()
            //fileEncode = new sun.misc.BASE64Encoder().encode(streamRiassunti.toByteArray())
            fileEncode = new sun.misc.BASE64Encoder().encode(filecontent)
            attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "${filename}", content: fileEncode))
            //attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "${fileNameRisposta}.zip", content: fileEncode))
        }*/
        if(Environment.current == Environment.PRODUCTION) {
            //recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Cristina Sivelli", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Silvia Carino", email: "s.carino@mach-1.it"))
        }else if (Environment.current==Environment.TEST){
            recpts.add(new MandrillRecipient(name:"Cristina Sivelli", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }else if (Environment.current==Environment.DEVELOPMENT){
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }
        logg = new Log(parametri: "preparo i dati da inviare per email", operazione: "invio GMFGEN", pagina: "INVIO MAIL SERVICE")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"${oggetto}",
                from_email:"documenti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "elenco-certificati-inviati-email", contents )
        println "mailinviata $mailinviata"
        def inviatoD = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        logg = new Log(parametri: "ho inviato la mail risposta invio-->${response1}", operazione: "invio GMFGEN", pagina: "INVIO MAIL SERVICE")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        return response1

    }
    def invioMailOFSGENMach1(caricamentoftp, filename=null,  streamGMFGENMach1=null) {
        def logg
        def recpts = []
        def attachs=[]
        def contents = []
        def fileEncode = null
        def oggetto=""

        oggetto="REPORT OFSGEN"
        def vars
        if(caricamentoftp){
            vars = [new MergeVar(name: "DATAC", content: "Il report OFSGEN, e' stato generato potete trovarlo nel server ftp : /flussi-nais/RCA-OPEL/OFSGEN")]

        }else{
            vars = [new MergeVar(name: "DATAC", content: "Il report OFSGEN non è stato generato, chiamare l'assitenza")]
        }

        /*if(streamGMFGENMach1 != null){
            def filecontent=streamGMFGENMach1.toByteArray()
            //fileEncode = new sun.misc.BASE64Encoder().encode(streamRiassunti.toByteArray())
            fileEncode = new sun.misc.BASE64Encoder().encode(filecontent)
            attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "${filename}", content: fileEncode))
            //attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "${fileNameRisposta}.zip", content: fileEncode))
        }*/
        if(Environment.current == Environment.PRODUCTION) {
            //recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Cristina Sivelli", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Silvia Carino", email: "s.carino@mach-1.it"))
        }else if (Environment.current==Environment.TEST){
            recpts.add(new MandrillRecipient(name:"Cristina Sivelli", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"IT", email: "it@mach-1.it "))
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }else if (Environment.current==Environment.DEVELOPMENT){
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }
        logg = new Log(parametri: "preparo i dati da inviare per email", operazione: "invio OFSGEN", pagina: "INVIO MAIL SERVICE")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"${oggetto}",
                from_email:"documenti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "elenco-certificati-inviati-email", contents )
       // println "mailinviata $mailinviata"
        def inviatoD = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        logg = new Log(parametri: "ho inviato la mail risposta invio-->${response1}", operazione: "invio OFSGEN", pagina: "INVIO MAIL SERVICE")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        return response1

    }
    /*def invioMailCertificatiCaricati(risposta, filename) {
        def streamRiassunti = new ByteArrayOutputStream()
        def zipRiassunti = new ZipOutputStream(streamRiassunti)
        def rispostaFinale, fileNameRisposta
        rispostaFinale=risposta
        fileNameRisposta = filename.toString().substring(0,filename.toString().indexOf(".txt"))
        zipRiassunti.putNextEntry(new ZipEntry(filename))
        zipRiassunti.write(rispostaFinale.bytes)
        zipRiassunti.close()
        streamRiassunti.close()
        def recpts = []
        def attachs=[]
        def contents = []
        def fileEncode = null
        def vars = [new MergeVar(name: "DATAC", content: "In allegato un riassunto dei certificati caricati in data: ${new Date().format("dd-MMM-yyyy")}")]
        fileEncode = new sun.misc.BASE64Encoder().encode(streamRiassunti.toByteArray())
        attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "${fileNameRisposta}.zip", content: fileEncode))
        if(Environment.current == Environment.PRODUCTION) {
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))
        }else if (Environment.current==Environment.TEST){
            recpts.add(new MandrillRecipient(name:"Silvia Carino", email: "s.carino@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }else if (Environment.current==Environment.DEVELOPMENT){
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }

        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco certificati generati polizze Finanziate/Leasing",
                from_email:"documenti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "elenco-certificati-inviati-email", contents )
        def inviatoD = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")*//*.collect { k, v -> "$k: $v" }.join("\n")*//*
        return response1
    }*/
    def invioMailCertificatiPAICaricati(risposta, filename) {
        def recpts = []
        def attachs=[]
        def contents = []
        def fileEncode = null
        def vars = [new MergeVar(name: "DATAC", content: "In allegato i certificati PAI da spedire ")]
        fileEncode = new sun.misc.BASE64Encoder().encode(risposta.toByteArray())
        attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "${filename}.zip", content: fileEncode))
        if(Environment.current == Environment.PRODUCTION) {
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))
        }else if (Environment.current==Environment.TEST){
            recpts.add(new MandrillRecipient(name:"Silvia Carino", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }else if (Environment.current==Environment.DEVELOPMENT){
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco certificati generati polizze PAI pagamento",
                from_email:"documenti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "elenco-certificati-inviati-email", contents )
        def inviatoD = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailDeleted(deleted) {
        def recpts = []
        def attachs=[]
        def contents = []
        def elencoDel =""
        for(String s: deleted.split(","))
        {
            elencoDel+=s+"<br>"
        }

        def vars = [new MergeVar(name: "DATAC", content: "Di seguito i contratti Deleted del: ${new Date().format("ddMMyyyy")}<br><br>${elencoDel}")]
        if(Environment.current == Environment.PRODUCTION) {
            recpts.add(new MandrillRecipient(name:"Assunta Carnovale", email: "assunta.carnovale@gmfinancial.com"))
            recpts.add(new MandrillRecipient(name:"Cristina Sivelli", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Susanna Facciotto", email: "s.facciotto@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Attivazioni", email: "attivazioni.ofs@mach-1.it"))

        }else if (Environment.current==Environment.TEST){
            recpts.add(new MandrillRecipient(name:"Cristina Sivelli", email: "c.sivelli@mach-1.it"))
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }else if (Environment.current==Environment.DEVELOPMENT){
            recpts.add(new MandrillRecipient(name:"Team Mach1", email: "priscila@presstoday.com"))
        }

        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Deleted ${new Date().format("ddMMyyyy")}",
                from_email:"attivazioni.ofs@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "elenco-polizze-deleted", contents )
        def inviatoD = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    def invioMailDealerCaricati(risposta, filename) {
        def streamRiassunti = new ByteArrayOutputStream()
        def zipRiassunti = new ZipOutputStream(streamRiassunti)
        def rispostaFinale, fileNameRisposta
        rispostaFinale=risposta
        fileNameRisposta = filename.toString().substring(0,filename.toString().indexOf(".txt"))
        zipRiassunti.putNextEntry(new ZipEntry(filename))
        zipRiassunti.write(rispostaFinale.bytes)
        zipRiassunti.close()
        streamRiassunti.close()
        def recpts = []
        def attachs=[]
        def contents = []
        def fileEncode = null
        def vars = [new MergeVar(name: "DATAC", content: "In allegato un riassunto dei dealer caricati in data: ${new Date().format("dd-MMM-yyyy")}")]
        fileEncode = new sun.misc.BASE64Encoder().encode(streamRiassunti.toByteArray())
        attachs.add(new MandrillAttachment(type: "application/octet-stream", name: "${fileNameRisposta}.zip", content: fileEncode))
        recpts.add(new MandrillRecipient(name:"Priscila del Valle", email: "priscila@presstoday.com"))
        contents.add([name:"", content:""])
        def mandrillMessage = new MandrillMessage(
                text:"this is a text message",
                subject:"Elenco dealer caricati",
                from_email:"documenti@mach-1.it",
                to:recpts,
                attachments : attachs,
                global_merge_vars:vars
        )
        def mailinviata = mandrillService.sendTemplate(mandrillMessage, "elenco-certificati-inviati-email", contents )
        def inviatoD = mailinviata.count { r -> r.success == false } == 0
        def response1 = mailinviata.collect { response1 ->
            [
                    status: response1?.status,
                    message: response1?.message,
                    emailCliente: response1?.email,
                    id: response1?.id,
                    rejectReason: response1?.rejectReason,
                    successCliente: response1?.success
            ].collect { k, v -> "$k: $v" }.join(", ")
        }.join("\n")/*.collect { k, v -> "$k: $v" }.join("\n")*/
        return response1
    }
    private def getRinnDb() {
        //println dataSource_rinnovi
        return new Sql(dataSource_rinnovi)
    }
}
