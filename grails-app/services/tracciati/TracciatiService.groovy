package tracciati

import cashopel.polizze.CodProdotti
import cashopel.polizze.Marche
import cashopel.polizze.Polizza
import cashopel.polizze.PolizzaPaiPag
import cashopel.polizze.PolizzaRCA
import cashopel.polizze.TipoCliente
import cashopel.polizze.TipoPai
import cashopel.polizze.TipoPolizza
import cashopel.polizze.TipoVeicolo
import cashopel.polizze.Tracciato
import cashopel.polizze.TracciatoCompagnia
import cashopel.polizze.TracciatoDLRCA
import cashopel.polizze.TracciatoIASSRCA
import cashopel.polizze.TracciatoPAI
import cashopel.polizze.TracciatoPAIPag
import cashopel.utenti.Log
import com.presstoday.groovy.Stopwatch
import excel.builder.ExcelBuilder
import grails.transaction.Transactional
import grails.util.Environment
import groovy.time.TimeCategory


@Transactional
class TracciatiService {
    static transactional = false
    def mailService
    def ftpService
    def generaTracciatoIAssicur(Polizza polizza) {

        boolean isCash=false
        boolean annullata=false
        if(polizza.tipoPolizza==TipoPolizza.CASH){
            isCash=true
        }
        if(polizza.codOperazione=="E" || polizza.codOperazione=="F"){
            annullata=true
        }
        def copertura=(polizza.coperturaRichiesta)? polizza.coperturaRichiesta.toUpperCase():""
        def kasko=(copertura)? copertura.contains("KASKO") ?"K":"I" :"I"
        def cognome=(isCash)? "${polizza.cognome.trim()}" : "${polizza.cognome.trim()} ${polizza.nome?.trim()}"
        def nome=(isCash)? "${polizza.nome?.trim()}":""
        def sesso=(isCash)? "${polizza.tipoCliente.value.toString().toUpperCase()}" : ""
        def codFiscale= "${polizza.partitaIva?polizza.partitaIva.trim().toUpperCase():""}"
        def pcl= "${(polizza.premioLordo*100).round()}"
        def telCasa=(!isCash)? "${(polizza.telefono.trim())}" : ""
        def imponibile=(polizza.premioImponibile*100).round()
        def imposte= ((polizza.imposte)*100).round()
        def provvMansutti=(polizza.rappel*0.75)
         provvMansutti=((provvMansutti.round(2))*100).round()
        def provvMach1=(polizza.rappel*0.25)
        provvMach1=((provvMach1.round(2))*100).round()
        def provvSocComme=((polizza.provvGmfi+polizza.provvDealer+polizza.provvVenditore)*100).round()
        def telefono=polizza.telefono? polizza.telefono.toString().trim().replaceAll("[^0-9]+",""):""
        def cellulare=(isCash)? "${polizza.telefono.toString().trim().replaceAll("[^0-9]+","")}":"${polizza.cellulare.toString().trim().replaceAll("[^0-9]+","")}"
        def totalecaricamenti=provvMansutti+provvMach1+provvSocComme

        if(polizza.codOperazione=="E" || polizza.codOperazione=="S" || polizza.codOperazione=="F"){
            pcl= "-${pcl}"
            imponibile= "-${imponibile}"
            imposte= "-${imposte}"
            provvMansutti= "-${provvMansutti}"
            provvMach1= "-${provvMach1}"
            provvSocComme= "-${provvSocComme}"
            totalecaricamenti= "-${totalecaricamenti}"
        }
        def marca
        if(Marche.findByMarca(polizza.marca.toString().trim())){
            marca=Marche.findByMarca(polizza.marca.toString().trim()).codice
        }else{marca=""}
        def codiceprod=CodProdotti.findByCoperturaAndStepAndPacchetto(polizza.coperturaRichiesta,polizza.step,polizza.tipoPolizza)
        /**implementazione per campagna*/
        def numpolInt=""
        def codProd=''
        if(polizza.codCampagna!='0'){
            numpolInt=polizza.codCampagna
            if(codiceprod.codProdottoCampagna!='null' || codiceprod.codProdottoCampagna!=null || codiceprod.codProdottoCampagna!=''){
                codProd=codiceprod.codProdottoCampagna
            }else{
                codProd=''
            }
        }else{
            if(codiceprod && (polizza.tariffa==1)){
                codProd=codiceprod.codProdotto
            }else {

                codProd=codiceprod.codProdottoflex2
            }
        }

            def tracciato = [
                    "Kasko":"${kasko}".padLeft(1),
                    "Codice modello":"".padLeft(5),
                    "Modello veicolo":polizza.modello.trim().toUpperCase().padLeft(20),
                    "Targa":"${polizza.targa? polizza.targa.trim().toUpperCase():""}".padLeft(15),
                    "Cognome":cognome.toUpperCase().padLeft(40),
                    "Nome":nome.toUpperCase().padLeft(40),
                    "Provincia":polizza.provincia.toUpperCase().padLeft(2),
                    "Valuta":"E".padLeft(1),
                    "Valore assicurato":"${(polizza.valoreAssicurato *100).round()}".padLeft(9),
                    "Data immatricolazione":"${polizza.dataImmatricolazione? polizza.dataImmatricolazione.format('yyyyMMdd') : ""}".padLeft(8),
                    "Durata furto incendio":"".padLeft(3),
                    "Indirizzo":polizza.indirizzo.toUpperCase().padLeft(30),
                    "Cap":polizza.cap.padLeft(5),
                    "Città":polizza.localita?.toUpperCase().padLeft(30),
                    "Data di nascita":"".padLeft(8),
                    "Provincia nascita":"".padLeft(2),
                    "Sesso":sesso.padLeft(1),
                    "Numero polizza":"${polizza.noPolizza}".padLeft(10),
                    "Codice operazione":"${polizza.codOperazione}".padLeft(3),
                    "Data apertura contratto":"".padLeft(8),
                    "Importo assicurato":"".padLeft(9),
                    "Codice fiscale / Partita IVA":"${codFiscale}".padLeft(16),
                    "Marca":  "${marca}".padLeft(5),
                    "Autocarro": "".padLeft(1),
                    "Uso": "".padLeft(1),
                    "Scadenza vincolo": "".padLeft(8),
                    "Codice dealer": "".padLeft(5),
                    "Numero telaio": "${polizza.telaio? polizza.telaio.trim().toUpperCase():""}".padLeft(17),
                    "Numero rinnovo": "".padLeft(2),
                    "Vincolo": "".padLeft(1),
                    "Pcl":  "${pcl}".padLeft(9),
                    "Old contra": "".padLeft(10),
                    "Cv fisc": "".padLeft(4),
                    "Quintali": "".padLeft(4),
                    "Data decorrenza":"${(polizza.dataDecorrenza)? polizza.dataDecorrenza.format("yyyyMMdd"):""}".padLeft(8),
                    "Buyback": "".padLeft(1),
                    "Satellitare": "${polizza.onStar ? "S" : ""}".padLeft(1),
                    "Data scadenza": "${(polizza.dataScadenza)? polizza.dataScadenza.format("yyyyMMdd") :""}".padLeft(8),
                    "Rc": "".padLeft(1),
                    "Telefono casa": "${telCasa}".padLeft(15),
                    "Telefono ufficio": "".padLeft(15),
                    "Telefono": "".padLeft(15),
                    "Cellulare":"${cellulare}".padLeft(15),
                    "Provvigioni dealer":"${provvMansutti}".padLeft(9),
                    "Provvigioni Mach1":"${provvMach1}".padLeft(9),
                    "Provvigioni società commerciale": "${provvSocComme}".padLeft(9),
                    "Totale caricamenti":"${totalecaricamenti}".padLeft(9),
                    "Imponibile": "${imponibile}".padLeft(9),
                    "Imposte":"${imposte}".padLeft(9),
                    "Pdu":  "".padLeft(9),
                    "Iban": "".padLeft(34),
                    "Collisione": "${(polizza.coperturaRichiesta) ? polizza.coperturaRichiesta.contains("COLLISIONE") ? "S" : "N":"N"}".padLeft(1),
                    "Data operazione": "".padLeft(8),
                    "Codice prodotto":  "${codProd}".padLeft(2),
                    "Tipologia veicolo": "${polizza.nuovo ? "S":"N"}".padLeft(1),
                    "Rischio": "".padLeft(1),
                    "Zona": "".padLeft(1),
                    "Email": "${polizza.email}".padLeft(50),
                    "Mini collisione": "".padLeft(2),
                    "Garanzia2": "".padLeft(2),
                    "Durata garanzia2": "".padLeft(2),
                    "Durata kasko": "${(polizza.coperturaRichiesta) ? (polizza.coperturaRichiesta.contains("KASKO")) ? polizza.durata.toString().trim() : "00" : "00"}".padLeft(2),
                    "Durata collisione": "${(polizza.coperturaRichiesta) ? (polizza.coperturaRichiesta.contains("COLLISIONE"))? polizza.durata.toString().trim() : "00" : "00"}".padLeft(2),
                    "Durata valore a nuovo": "00".padLeft(2),
                    "Codice società commerciale": "".padLeft(3),
                    "Codice venditore": "${polizza.dealer.codiceIASSICUR}".padLeft(3),
                    "Provvigioni venditore": "".padLeft(9),
                    "Codice segnalatore": "".padLeft(3),
                    "Provvigioni segnalatore": "".padLeft(9),
                    "Integra": "".padLeft(1),
                    "Numero polizza integrata": "${numpolInt}".padLeft(10),
                    "Durata integra": "".padLeft(2),
                    "Codice iban": "".padLeft(27),
                    "Intestatario iban": "".padLeft(30),
                    "Token": "".padLeft(16),
                    "Cognome nome assicurato": "".padLeft(40),
                    "Partita iva assicurato": "".padLeft(11),
                    "Indirizzo assicurato": "".padLeft(30),
                    "Citta assicurato": "".padLeft(30),
                    "Cap assicurato": "".padLeft(5),
                    "Provincia assicurato": "".padLeft(2),
                    "Codice fiscale assicurato": "".padLeft(16),
                    "Data nascita assicurato": "".padLeft(8),
                    "Sesso assicurato": "".padLeft(1),
                    "Seriale Lojack": "".padLeft(7)
            ]
            def dataInvioTracciato=new Date()
        /*if(polizza.codOperazione=="S" || polizza.codOperazione=="E"){
            return new Tracciato(polizza: polizza, tipoPolizza: polizza.tipoPolizza, tracciato: tracciato.values().join(), dataInvio: dataInvioTracciato, dataCaricamento:dataInvioTracciato,  cash: isCash, annullata: annullata)
        }else{*/
            return new Tracciato(polizza: polizza, tipoPolizza: polizza.tipoPolizza, tracciato: tracciato.values().join(), dataInvio: dataInvioTracciato, cash: isCash, annullata: annullata)

        //}
    }
    def generaTracciatoIAssicurRCA(PolizzaRCA polizza) {
        boolean isRCA=true
        boolean annullata=false

        if(polizza.codOperazione=="E" || polizza.codOperazione=="F"){
            annullata=true
        }
        def copertura=(polizza.coperturaRichiesta)? polizza.coperturaRichiesta.toUpperCase():""
        def kasko=(copertura)? copertura.contains("KASKO") ?"K":"I" :"I"
        def cognome= "${polizza.cognome.trim()} ${polizza.nome?.trim()}"
        def nome=""
        def sesso= ""
        def codFiscale= "${polizza.partitaIva?polizza.partitaIva.trim().toUpperCase():""}"
        def pcl= "${(polizza.premioLordo*100).round()}"
        def telCasa="${(polizza.telefono.trim())}"
        def imponibile=(polizza.premioImponibile*100).round()
        def imposte= ((polizza.imposte)*100).round()
        def provvMansutti=(polizza.rappel*0.50)
         provvMansutti=((provvMansutti.round(2))*100).round()
        def provvMach1=(polizza.rappel*0.50)
        provvMach1=((provvMach1.round(2))*100).round()
        def provvSocComme=((polizza.provvGmfi+polizza.provvDealer+polizza.provvVenditore)*100).round()
        def telefono=polizza.telefono? polizza.telefono.toString().trim().replaceAll("[^0-9]+",""):""
        //println "cellulare--> ${polizza.cellulare.toString().trim()}"
        def cellulare="${polizza.cellulare.toString().trim().replaceAll("[^0-9]+","")}"
        def totalecaricamenti=provvMansutti+provvMach1+provvSocComme
        if(polizza.codOperazione=="E" || polizza.codOperazione=="S" || polizza.codOperazione=="F"){
            pcl= "-${pcl}"
            imponibile= "-${imponibile}"
            imposte= "-${imposte}"
            provvMansutti= "-${provvMansutti}"
            provvMach1= "-${provvMach1}"
            provvSocComme= "-${provvSocComme}"
            totalecaricamenti= "-${totalecaricamenti}"
        }
        def marca
        if(Marche.findByMarca(polizza.marca.toString().trim())){
            marca=Marche.findByMarca(polizza.marca.toString().trim()).codice
        }else{marca=""}
        def codiceprod=""
        if(polizza.tipoPolizza==TipoPolizza.FINANZIATE){
            codiceprod="RR"
        }else{
            codiceprod="RT"
        }
        /**implementazione per campagna*/
        def numpolInt=""
        def codProd=''
        if(polizza.codCampagna!='0'){
            /*numpolInt=polizza.codCampagna
            if(codiceprod.codProdottoCampagna!='null' || codiceprod.codProdottoCampagna!=null || codiceprod.codProdottoCampagna!=''){
                codProd=codiceprod.codProdottoCampagna
            }else{
                codProd=''
            }*/
        }else{
           /* if(codiceprod && (polizza.tariffa==1)){
                codProd=codiceprod.codProdotto
            }else {
                codProd=''
                //codProd=codiceprod.codProdottoflex2
            }*/
        }

            def tracciato = [
                    "Kasko":"I".padLeft(1),
                    "Codice modello":"".padLeft(5),
                    "Modello veicolo":polizza.modello.trim().toUpperCase().padLeft(20),
                    "Targa":"${polizza.targa? polizza.targa.toUpperCase():""}".padLeft(15),
                    "Cognome":"${cognome.trim()}".padLeft(40),
                    "Nome":"".padLeft(40),
                    "Provincia":polizza.provincia.trim().toUpperCase().padLeft(2),
                    "Valuta":"E".padLeft(1),
                    "Valore assicurato":"${(polizza.valoreAssicurato *100).round()}".padLeft(9),
                    "Data immatricolazione":"${polizza.dataImmatricolazione? polizza.dataImmatricolazione.format('yyyyMMdd') : ""}".padLeft(8),
                    "Durata furto incendio":"".padLeft(3),
                    "Indirizzo":polizza.indirizzo.trim().toUpperCase().padLeft(30),
                    "Cap":polizza.cap.padLeft(5),
                    "Città":polizza.localita?.toUpperCase().padLeft(30),
                    "Data di nascita":"".padLeft(8),
                    "Provincia nascita":"".padLeft(2),
                    "Sesso":sesso.padLeft(1),
                    "Numero polizza":"${polizza.noPolizza.trim()}".padLeft(10),
                    "Codice operazione":"0".padLeft(3),
                    "Data apertura contratto":"".padLeft(8),
                    "Importo assicurato":"".padLeft(9),
                    "Codice fiscale / Partita IVA":"${codFiscale.trim()}".padLeft(16),
                    "Marca":  "53".padLeft(5),
                    "Autocarro": "${(polizza.tipoVeicolo==TipoVeicolo.AUTOCARRO)?'S':'N'}".padLeft(1),
                    "Uso": "".padLeft(1),
                    "Scadenza vincolo": "".padLeft(8),
                    "Codice dealer": "DU6".padLeft(5),
                    "Numero telaio": "${polizza.telaio? polizza.telaio.trim().toUpperCase():""}".padLeft(17),
                    "Numero rinnovo": "".padLeft(2),
                    "Vincolo": "".padLeft(1),
                    "Pcl":  "${pcl}".padLeft(9),
                    "Old contra": "".padLeft(10),
                    "Cv fisc": "".padLeft(4),
                    "Quintali": "".padLeft(4),
                    "Data decorrenza":"${(polizza.dataDecorrenza)? polizza.dataDecorrenza.format("yyyyMMdd"):""}".padLeft(8),
                    "Buyback": "".padLeft(1),
                    "Satellitare": "".padLeft(1),
                    "Data scadenza": "${(polizza.dataScadenza)? polizza.dataScadenza.format("yyyyMMdd") :""}".padLeft(8),
                    "Rc": "".padLeft(1),
                    "Telefono casa": "${telCasa}".padLeft(15),
                    "Telefono ufficio": "".padLeft(15),
                    "Telefono": "".padLeft(15),
                    "Cellulare":"${cellulare}".padLeft(15),
                    "Provvigioni dealer":"${provvMansutti}".padLeft(9),
                    "Provvigioni Mach1":"${provvMach1}".padLeft(9),
                    "Provvigioni società commerciale": "${provvSocComme}".padLeft(9),
                    "Totale caricamenti":"${totalecaricamenti}".padLeft(9),
                    "Imponibile": "${imponibile}".padLeft(9),
                    "Imposte":"${imposte}".padLeft(9),
                    "Pdu":  "".padLeft(9),
                    "Iban": "".padLeft(34),
                    "Collisione": "N".padLeft(1),
                    "Data operazione": "".padLeft(8),
                    "Codice prodotto":  "${codiceprod}".padLeft(2),
                    "Tipologia veicolo": "${polizza.nuovo ? "S":"N"}".padLeft(1),
                    "Rischio": "".padLeft(1),
                    "Zona": "".padLeft(1),
                    "Email": "${polizza.email.trim()}".padLeft(50),
                    "Mini collisione": "".padLeft(2),
                    "Garanzia2": "".padLeft(2),
                    "Durata garanzia2": "".padLeft(2),
                    "Durata kasko": "".padLeft(2),
                    "Durata collisione": "".padLeft(2),
                    "Durata valore a nuovo": "".padLeft(2),
                    "Codice società commerciale": "DU7".padLeft(3),
                    "Codice venditore": "".padLeft(3),
                    "Provvigioni venditore": "".padLeft(9),
                    "Codice segnalatore": "${polizza.dealer.codiceIASSICUR}".padLeft(3),
                    "Provvigioni segnalatore": "".padLeft(9),
                    "Integra": "".padLeft(1),
                    "Numero polizza integrata": "${numpolInt}".padLeft(10),
                    "Durata integra": "".padLeft(2),
                    "Codice iban": "".padLeft(27),
                    "Intestatario iban": "".padLeft(30),
                    "Token": "".padLeft(16),
                    "Cognome nome assicurato": "".padLeft(40),
                    "Partita iva assicurato": "".padLeft(11),
                    "Indirizzo assicurato": "".padLeft(30),
                    "Citta assicurato": "".padLeft(30),
                    "Cap assicurato": "".padLeft(5),
                    "Provincia assicurato": "".padLeft(2),
                    "Codice fiscale assicurato": "".padLeft(16),
                    "Data nascita assicurato": "".padLeft(8),
                    "Sesso assicurato": "".padLeft(1),
                    "Seriale Lojack": "".padLeft(7)
            ]
            def dataInvioTracciato=new Date()
        /*if(polizza.codOperazione=="S" || polizza.codOperazione=="E"){
            return new Tracciato(polizza: polizza, tipoPolizza: polizza.tipoPolizza, tracciato: tracciato.values().join(), dataInvio: dataInvioTracciato, dataCaricamento:dataInvioTracciato,  cash: isCash, annullata: annullata)
        }else{*/
            return new TracciatoIASSRCA(polizzaRCA: polizza, tipoPolizza: polizza.tipoPolizza, tracciato: tracciato.values().join(), dataInvio: dataInvioTracciato,  rca: isRCA, annullata: annullata)


        //}
    }
    def generaTracciatoPAIPAgamento(PolizzaPaiPag polizza, boolean leasing) {
        def codiceprod="PL"
        def tipo=TipoPai.PAGAMENTO
        if(!leasing){codiceprod="PF"}
        def kasko="I"
        def sesso=(polizza.tipoCliente==TipoCliente.M)? TipoCliente.M.value : (polizza.tipoCliente==TipoCliente.F )?TipoCliente.F.value:""
        def cognome=(polizza.nome)? "${polizza.cognome.trim()}" : "${polizza.cognome.trim()} ${polizza.nome?.trim()}"
        def nome=(polizza.nome)? "${polizza.nome?.trim()}":""
        def codFiscale= "${polizza.partitaIva?polizza.partitaIva.trim().toUpperCase():""}"
        def valoreAssicurato=(polizza.valoreAssicurato*100).round()
        def pcl= "${((polizza.premioLordo)*100).round()}"
        def telCasa= "${(polizza.telefono.trim())}"
        def imponibile=(polizza.premioImponibile*100).round()
        def imposte= ((polizza.imposte)*100).round()
        def provvDealer=(((polizza.premioImponibile)*0.13)*0.75)
        def provvMach1=(((polizza.premioImponibile)*0.13)*0.25)
        def provvSocComme=((polizza.premioImponibile)*0.66)
        def cellulare= "${polizza.cellulare.toString().trim().replaceAll("[^0-9]+","")}"
        def totalecaricamenti=provvDealer+provvMach1+provvSocComme
        def pdu=polizza.premioImponibile-totalecaricamenti
        provvDealer=((provvDealer.round(2))*100).round()
        provvMach1=((provvMach1.round(2))*100).round()
        provvSocComme=((provvSocComme.round(2))*100).round()
        totalecaricamenti=((totalecaricamenti.round(2))*100).round()
        pdu=((pdu.round(2))*100).round()

        def marca
        if(Marche.findByMarca(polizza.marca.toString().trim())){
            marca=Marche.findByMarca(polizza.marca.toString().trim()).codice
        }else{marca=""}

            def tracciato = [
                    "Kasko":"${kasko}".padLeft(1),
                    "Codice modello":"".padLeft(5),
                    "Modello veicolo":polizza.modello.trim().toUpperCase().padLeft(20),
                    "Targa":"${polizza.targa? polizza.targa.trim().toUpperCase():""}".padLeft(15),
                    "Cognome":cognome.toUpperCase().padLeft(40),
                    "Nome":nome.toUpperCase().padLeft(40),
                    "Provincia":polizza.provincia.toUpperCase().padLeft(2),
                    "Valuta":"E".padLeft(1),
                    "Valore assicurato":"${valoreAssicurato}".padLeft(9),
                    "Data immatricolazione":"".padLeft(8),
                    "Durata furto incendio":"${polizza.durata}".padLeft(3),
                    "Indirizzo":polizza.indirizzo.toUpperCase().padLeft(30),
                    "Cap":polizza.cap.padLeft(5),
                    "Città":polizza.localita?.toUpperCase().padLeft(30),
                    "Data di nascita":"".padLeft(8),
                    "Provincia nascita":"".padLeft(2),
                    "Sesso":"${sesso}".padLeft(1),
                    "Numero polizza":"${polizza.noPolizza}".padLeft(10),
                    "Codice operazione":"${polizza.codOperazione}".padLeft(3),
                    "Data apertura contratto":"".padLeft(8),
                    "Importo assicurato":"".padLeft(9),
                    "Codice fiscale / Partita IVA":"${codFiscale}".padLeft(16),
                    "Marca":  "${marca}".padLeft(5),
                    "Autocarro": "".padLeft(1),
                    "Uso": "".padLeft(1),
                    "Scadenza vincolo": "".padLeft(8),
                    "Codice dealer": "".padLeft(5),
                    "Numero telaio": "${polizza.telaio? polizza.telaio.trim().toUpperCase():""}".padLeft(17),
                    "Numero rinnovo": "".padLeft(2),
                    "Vincolo": "".padLeft(1),
                    "Pcl":  "${pcl}".padLeft(9),
                    "Old contra": "".padLeft(10),
                    "Cv fisc": "".padLeft(4),
                    "Quintali": "".padLeft(4),
                    "Data decorrenza":"${(polizza.dataDecorrenza)? polizza.dataDecorrenza.format("yyyyMMdd"):""}".padLeft(8),
                    "Buyback": "".padLeft(1),
                    "Satellitare": "".padLeft(1),
                    "Data scadenza": "${(polizza.dataScadenza)? polizza.dataScadenza.format("yyyyMMdd") :""}".padLeft(8),
                    "Rc": "N".padLeft(1),
                    "Telefono casa": "${telCasa}".padLeft(15),
                    "Telefono ufficio": "".padLeft(15),
                    "Telefono": "".padLeft(15),
                    "Cellulare":"${cellulare}".padLeft(15),
                    "Provvigioni dealer":"${provvDealer}".padLeft(9),
                    "Provvigioni Mach1":"${provvMach1}".padLeft(9),
                    "Provvigioni società commerciale": "${provvSocComme}".padLeft(9),
                    "Totale caricamenti":"${totalecaricamenti}".padLeft(9),
                    "Imponibile": "${imponibile}".padLeft(9),
                    "Imposte":"${imposte}".padLeft(9),
                    "Pdu":  "${pdu}".padLeft(9),
                    "Iban": "".padLeft(34),
                    "Collisione": "N".padLeft(1),
                    "Data operazione": "".padLeft(8),
                    "Codice prodotto":  "${codiceprod}".padLeft(2),
                    "Tipologia veicolo": "".padLeft(1),
                    "Rischio": "".padLeft(1),
                    "Zona": "".padLeft(1),
                    "Email": "${polizza.email}".padLeft(50),
                    "Mini collisione": "".padLeft(2),
                    "Garanzia2": "".padLeft(2),
                    "Durata garanzia2": "".padLeft(2),
                    "Durata kasko": "".padLeft(2),
                    "Durata collisione": "".padLeft(2),
                    "Durata valore a nuovo": "".padLeft(2),
                    "Codice società commerciale": "".padLeft(3),
                    "Codice venditore": "".padLeft(3),
                    "Provvigioni venditore": "".padLeft(9),
                    "Codice segnalatore": "${polizza.dealer.codiceIASSICUR}".padLeft(3),
                    "Provvigioni segnalatore": "".padLeft(9),
                    "Integra": "".padLeft(1),
                    "Numero polizza integrata": "".padLeft(10),
                    "Durata integra": "".padLeft(2),
                    "Codice iban": "".padLeft(27),
                    "Intestatario iban": "".padLeft(30),
                    "Token": "".padLeft(16),
                    "Cognome nome assicurato": "".padLeft(40),
                    "Partita iva assicurato": "".padLeft(11),
                    "Indirizzo assicurato": "".padLeft(30),
                    "Citta assicurato": "".padLeft(30),
                    "Cap assicurato": "".padLeft(5),
                    "Provincia assicurato": "".padLeft(2),
                    "Codice fiscale assicurato": "".padLeft(16),
                    "Data nascita assicurato": "".padLeft(8),
                    "Sesso assicurato": "".padLeft(1),
                    "Seriale Lojack": "".padLeft(7)
            ]
            def dataInvioTracciato=new Date()
            return new TracciatoPAIPag(polizza: polizza, tipoPolizza: polizza.tipoPolizza, tracciatoPAIPaga: tracciato.values().join(), tipo:tipo, dataInvio: dataInvioTracciato, cash: false)
    }
    def generaTracciatoIAssicurRinn(def polizza) {
        boolean isCash=false
        boolean annullata=false
        if(polizza.codOperazione=="E" || polizza.codOperazione=="F"){
            annullata=true
        }
        def datadecorrenza= polizza.dataDecorrenza
        def dataTariffa=Date.parse("yyyy-MM-dd","2017-06-16")
            if(!polizza.ramo.toString().trim().isEmpty()) {
                def ramo = polizza.ramo.toString().trim()

                if (datadecorrenza > dataTariffa) {
                    if (ramo.contains('D/02') || ramo.contains('D/12') || ramo.contains('D/30')) {
                        polizza.codPacchetto = "2RPKR"
                    } else if (ramo.toString().contains('D/11') || ramo.toString().contains('D/29')) {
                        polizza.codPacchetto = "2RPCR"
                    } else if (ramo.toString().contains('W/99') || ramo.toString().contains('D/10') || ramo.toString().contains('D/28')) {
                        polizza.codPacchetto = "2RP0R"
                    } else if (ramo.toString().contains('W/98') || ramo.toString().contains('D/09') || ramo.toString().contains('D/27')) {
                        polizza.codPacchetto = "2RG0R"
                    } else if (ramo.toString().contains('W/97') || ramo.toString().contains('D/08') || ramo.toString().contains('D/26')) {
                        polizza.codPacchetto = "2RS0R"
                    } else if (ramo.toString().contains('W/96') || ramo.toString().contains('D/07') || ramo.toString().contains('D/25')) {
                        polizza.codPacchetto = "2RPK0"
                    } else if (ramo.toString().contains('W/95') || ramo.toString().contains('D/06') || ramo.toString().contains('D/24')) {
                        polizza.codPacchetto = "2RPC0"
                    } else if (ramo.toString().contains('W/94') || ramo.toString().contains('D/05') || ramo.toString().contains('D/23')) {
                        polizza.codPacchetto = "2RP00"
                    } else if (ramo.toString().contains('W/93') || ramo.toString().contains('D/04') || ramo.toString().contains('D/22')) {
                        polizza.codPacchetto = "2RG00"
                    } else if (ramo.toString().contains('W/92') || ramo.toString().contains('D/03') || ramo.toString().contains('D/21')) {
                        polizza.codPacchetto = "2RS00"
                    }
                }
            }
            //println "polizza cod pachetto tracc compagnia ${polizza.codPacchetto}"
/*            "silver" => '2RS00',
            "silver+8" => '2RS0R',
            "gold" => '2RG00',
            "gold+8" => '2RG0R',
            "platinum" => '2RP00',
            "platinum+8" => '2RP0R',
            "platinum+collisione" => '2RPC0',
            "platinum+collisione+8" => '2RPCR',
            "platinum+kasko" => '2RPK0',
            "platinum+kasko+8" => '2RPKR',*/
        def copertura=(polizza.coperturaRichiesta)? polizza.coperturaRichiesta.toUpperCase():""
        def kasko=(copertura)? copertura.contains("KASKO") ?"K":"I" :"I"
        def cognome="${polizza.cliente.trim()}"
        def codFiscale= "${polizza.cFiscale}"
        def pcl="${(polizza.premioLordo*100).round()}"
        def telCasa= ""
        def imponibile=(polizza.premioImponibile*100).round()
        def imposte= ((polizza.imposte)*100).round()
        def provvMansutti=polizza.provvMansutti
         provvMansutti=((provvMansutti.round(2))*100).round()
        def provvMach1=(polizza.provvMach1)
        provvMach1=((provvMach1.round(2))*100).round()
        def provvSocComme=((polizza.provvGmfi+polizza.provvDealer+polizza.provvVenditore)*100).round()
        def telefono=""
        def cellulare=""
        def totalecaricamenti=provvMansutti+provvMach1+provvSocComme
        def marca
        if(Marche.findByMarca(polizza.marca.toString().trim())){
            marca=Marche.findByMarca(polizza.marca.toString().trim()).codice
        }else{marca=""}
        def codiceprod=""
            def tracciato = [
                    "Kasko":"${kasko}".padLeft(1),
                    "Codice modello":"".padLeft(5),
                    "Modello veicolo":polizza.modello.trim().toUpperCase().padLeft(20),
                    "Targa":"${polizza.targa? polizza.targa.trim().toUpperCase():""}".padLeft(15),
                    "Cognome":cognome.toUpperCase().padLeft(40),
                    "Nome":"".padLeft(40),
                    "Provincia":polizza.provincia.toUpperCase().padLeft(2),
                    "Valuta":"E".padLeft(1),
                    "Valore assicurato":"${(polizza.valoreAssicurato *100).round()}".padLeft(9),
                    "Data immatricolazione":"${polizza.dataImmatricolazione? polizza.dataImmatricolazione.format('yyyyMMdd') : ""}".padLeft(8),
                    "Durata furto incendio":"".padLeft(3),
                    "Indirizzo":polizza.indirizzo.toUpperCase().padLeft(30),
                    "Cap":polizza.cap.padLeft(5),
                    "Città":polizza.localita?.toUpperCase().padLeft(30),
                    "Data di nascita":"".padLeft(8),
                    "Provincia nascita":"".padLeft(2),
                    "Sesso":"".padLeft(1),
                    "Numero polizza":"${polizza.noPolizza}".padLeft(10),
                    "Codice operazione":"${polizza.codOperazione}".padLeft(3),
                    "Data apertura contratto":"".padLeft(8),
                    "Importo assicurato":"".padLeft(9),
                    "Codice fiscale / Partita IVA":"${codFiscale}".padLeft(16),
                    "Marca":  "${marca}".padLeft(5),
                    "Autocarro": "".padLeft(1),
                    "Uso": "".padLeft(1),
                    "Scadenza vincolo": "".padLeft(8),
                    "Codice dealer": "".padLeft(5),
                    "Numero telaio": "${polizza.telaio? polizza.telaio.trim().toUpperCase():""}".padLeft(17),
                    "Numero rinnovo": "".padLeft(2),
                    "Vincolo": "".padLeft(1),
                    "Pcl":  "${pcl}".padLeft(9),
                    "Old contra": "".padLeft(10),
                    "Cv fisc": "".padLeft(4),
                    "Quintali": "".padLeft(4),
                    "Data decorrenza":"${(polizza.dataDecorrenza)? polizza.dataDecorrenza.format("yyyyMMdd"):""}".padLeft(8),
                    "Buyback": "".padLeft(1),
                    "Satellitare":"".padLeft(1),
                    "Data scadenza": "${(polizza.dataScadenza)? polizza.dataScadenza.format("yyyyMMdd") :""}".padLeft(8),
                    "Rc": "".padLeft(1),
                    "Telefono casa": "${telCasa}".padLeft(15),
                    "Telefono ufficio": "".padLeft(15),
                    "Telefono": "".padLeft(15),
                    "Cellulare":"${cellulare}".padLeft(15),
                    "Provvigioni dealer":"${provvMansutti}".padLeft(9),
                    "Provvigioni Mach1":"${provvMach1}".padLeft(9),
                    "Provvigioni società commerciale": "${provvSocComme}".padLeft(9),
                    "Totale caricamenti":"${totalecaricamenti}".padLeft(9),
                    "Imponibile": "${imponibile}".padLeft(9),
                    "Imposte":"${imposte}".padLeft(9),
                    "Pdu":  "".padLeft(9),
                    "Iban": "".padLeft(34),
                    "Collisione": "${(polizza.copertura) ? polizza.copertura.contains("COLLISIONE") ? "S" : "N":"N"}".padLeft(1),
                    "Data operazione": "".padLeft(8),
                    "Codice prodotto":  "${(codiceprod) ? codiceprod.codProdotto :""}".padLeft(2),
                    "Tipologia veicolo": "${polizza.nuovo ? "S":"N"}".padLeft(1),
                    "Rischio": "".padLeft(1),
                    "Zona": "".padLeft(1),
                    "Email": "".padLeft(50),
                    "Mini collisione": "".padLeft(2),
                    "Garanzia2": "".padLeft(2),
                    "Durata garanzia2": "".padLeft(2),
                    "Durata kasko": "${(polizza.copertura) ? (polizza.copertura.contains("KASKO")) ? polizza.durata.toString().trim() : "00" : "00"}".padLeft(2),
                    "Durata collisione": "${(polizza.copertura) ? (polizza.copertura.contains("COLLISIONE"))? polizza.durata.toString().trim() : "00" : "00"}".padLeft(2),
                    "Durata valore a nuovo": "00".padLeft(2),
                    "Codice società commerciale": "".padLeft(3),
                    "Codice venditore": "".padLeft(3),
                    "Provvigioni venditore": "".padLeft(9),
                    "Codice segnalatore": "".padLeft(3),
                    "Provvigioni segnalatore": "".padLeft(9),
                    "Integra": "".padLeft(1),
                    "Numero polizza integrata": "".padLeft(10),
                    "Durata integra": "".padLeft(2),
                    "Codice iban": "".padLeft(27),
                    "Intestatario iban": "".padLeft(30),
                    "Token": "".padLeft(16),
                    "Cognome nome assicurato": "".padLeft(40),
                    "Partita iva assicurato": "".padLeft(11),
                    "Indirizzo assicurato": "".padLeft(30),
                    "Citta assicurato": "".padLeft(30),
                    "Cap assicurato": "".padLeft(5),
                    "Provincia assicurato": "".padLeft(2),
                    "Codice fiscale assicurato": "".padLeft(16),
                    "Data nascita assicurato": "".padLeft(8),
                    "Sesso assicurato": "".padLeft(1),
                    "Seriale Lojack": "".padLeft(7)
            ]
            def dataInvioTracciato=new Date()
        /*if(polizza.codOperazione=="S" || polizza.codOperazione=="E"){
            return new Tracciato( tracciato: tracciato.values().join(), tipoPolizza: TipoPolizza.RINNOVI, dataInvio: dataInvioTracciato, dataCaricamento: dataInvioTracciato, cash: isCash, annullata: annullata)

        }else{*/
            return new Tracciato( tracciato: tracciato.values().join(), tipoPolizza: TipoPolizza.RINNOVI, dataInvio: dataInvioTracciato, cash: isCash,  annullata: annullata)
        //}
    }
    def generaTracciatoCompagnia(Polizza polizza) {
        def logg
        def telefono=polizza.telefono.replaceAll('/\\D+/','')
        def cellulare=polizza.cellulare?.replaceAll('/\\D+/','')?:""
        def nome="${polizza.nome.toUpperCase()trim()}"
        def cognome="${polizza.cognome.toUpperCase()trim()}"
        def indirizzo="${polizza.indirizzo.toUpperCase()trim()}"
        def nuovo=polizza.nuovo
        def veicolonuovo
        if(nuovo) veicolonuovo="S"
        else veicolonuovo="N"
        def onStar=polizza.onStar
        def blackBox
        if(onStar) blackBox="S"
        else blackBox="N"
        def stepN=polizza.step
        def codStep=""
        if(stepN=="step 1"){ codStep="step 1"}
        else if (stepN=="step 2"){codStep="step 2"}
        else if (stepN=="step 3"){codStep="step 3"}
        boolean isCash=false
        boolean annullata=false
        def coperturaR=polizza.coperturaRichiesta
        def tipoOper="A"
        def premioImponibile
        def premioLordo
        def imposte
        def provvigione
        def dataOperazione
        premioImponibile="${(polizza.premioImponibile*100).round()}"
        premioLordo="${(polizza.premioLordo*100).round()}"
        provvigione="${((polizza.provvDealer+polizza.provvVenditore+polizza.provvGmfi+polizza.rappel)*100).round()}"
        imposte="${(polizza.imposte*100).round()}"
        dataOperazione=polizza.dateCreated.format("yyyyMMdd")
        if(polizza.codOperazione=="S"){
            tipoOper="S"
        }else if (polizza.codOperazione=="E"){
            annullata=true
            tipoOper="E"
            /*premioImponibile="-${(polizza.premioImponibile*100).round()}"
            provvigione="-${((polizza.provvDealer+polizza.provvVenditore+polizza.provvGmfi+polizza.rappel)*100).round()}"
            premioLordo="-${(polizza.premioLordo*100).round()}"*/
            imposte=0
            dataOperazione=polizza.dataAnnullamento.format("yyyyMMdd")
        }else if (polizza.codOperazione=="F"){
            annullata=true
            tipoOper="F"
            /*premioImponibile="-${(polizza.premioImponibile*100).round()}"
            provvigione="-${((polizza.provvDealer+polizza.provvVenditore+polizza.provvGmfi+polizza.rappel)*100).round()}"
            premioLordo="-${(polizza.premioLordo*100).round()}"*/
            imposte=0
            dataOperazione=polizza.dataAnnullamento.format("yyyyMMdd")
        }
        def codCoper=""
        if(coperturaR =="SILVER"){codCoper="S"}
        else if(coperturaR=="GOLD"){codCoper="G"}
        else if(coperturaR=="PLATINUM"){codCoper="P"}
        else if(coperturaR=="PLATINUM COLLISIONE"){codCoper="C"}
        else if(coperturaR=="PLATINUM KASKO"){codCoper="K"}
        def tipoPacchetto=polizza.tipoPolizza.toString()
        codCoper=CodProdotti.findByCoperturaAndStepAndPacchetto(coperturaR, stepN,tipoPacchetto)
        def tipoPolizza=polizza.tipoPolizza
        def pacchetto
        if (tipoPolizza==TipoPolizza.FINANZIATE) {
            pacchetto="R"
            isCash=false
        }else if (tipoPolizza==TipoPolizza.LEASING) {
            pacchetto="L"
            isCash=false
        }
        else {
            pacchetto="C"
            isCash=true
        }
        def targa
        if(polizza.targa){
            targa=polizza.targa.toString().trim().toUpperCase()
        }else{
            targa=""
        }
        def tipoCliente= polizza.tipoCliente.value
        def partitaIva=""
        def codFiscale=""
        if(tipoCliente=="M" || tipoCliente=="F"){
            codFiscale=polizza.partitaIva?.toUpperCase().trim().toUpperCase()
        }else{
            partitaIva=polizza.partitaIva?.toUpperCase().trim().toUpperCase()

        }

        /**nuova implementazione campagna**/
        def numPolizza="DLI950000001"
        def codModello=""
        def codPacchetto=codCoper.codPacchetto
        def codproduttore=""
        if(polizza.codCampagna!='0'){
            numPolizza="DLI950000005"
            if(codCoper.codPacchettoDLCampagna!=null || codCoper.codPacchettoDLCampagna!='null' || codCoper.codPacchettoDLCampagna!=''){
                codPacchetto=codCoper.codPacchettoDLCampagna
            }else{
                codPacchetto=''
            }
            if(polizza.codCampagna=='C003' || polizza.codCampagna=='C004'){
                codproduttore=polizza.dealer.codProdCampagna
            }
            if(polizza.codCampagna=='C001' || polizza.codCampagna=='C003'){
                codModello="1"
            }else if(polizza.codCampagna=='C002' || polizza.codCampagna=='C004'){
                if(polizza.modello.toString().trim().toUpperCase().contains('CORSA')){
                    codModello="2"
                }else if (polizza.modello.toString().trim().toUpperCase().contains('MOKKA')){
                    codModello="3"
                }else if (polizza.modello.toString().trim().toUpperCase().contains('ASTRA')){
                    codModello="4"
                }else if (polizza.modello.toString().trim().toUpperCase().contains('KARL')){
                    codModello="5"
                }else if (polizza.modello.toString().trim().toUpperCase().contains('ADAM')){
                    codModello="6"
                }
            }
        }else{
            if(polizza.tariffa == 1){
                numPolizza="DLI950000001"
                codPacchetto=  codCoper.codPacchetto

            }else{
                numPolizza="DLI950000006"
                codPacchetto= codCoper.codPacchettodlflex2
            }
        }
        logg =new Log(parametri: "parametri codificate per campagna no polizza ${polizza.noPolizza}-cod pacheto ${codPacchetto}-cod campagna ${polizza.codCampagna}-Copertura ${coperturaR}- tipopacchetto ${tipoPacchetto}-step ${stepN} " +
        //logg =new Log(parametri: "parametri codificate per campagna no polizza ${polizza.noPolizza}-cod pacheto ${codPacchetto}-Copertura ${coperturaR}- tipopacchetto ${tipoPacchetto}-step ${stepN} " +
                "codice copertura id  ${codCoper}", operazione: "chiamata  ws", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def tracciato = [
                "Numero Polizza":"${numPolizza}".padRight(20),
                "Id Applicazione":"".padRight(5),
                "Numero Contratto":polizza.noPolizza.padRight(20),
                "Tipo Operazione":"${tipoOper}",
                "Data Operazione":"${dataOperazione}".padRight(8),
                "Nominativo":"${cognome.toUpperCase().trim()} ${nome.toUpperCase()trim()}".padRight(100),
                "Indirizzo":"${indirizzo.toUpperCase().trim()}".padRight(120),
                "Comune":polizza.localita.toUpperCase().trim().padRight(60),
                "Cap":polizza.cap.trim().padRight(5,"0"),
                "Provincia":polizza.provincia.toUpperCase().trim().padRight(2),
                "Indirizzo Corrisp":"".padRight(40),
                "Comune Corrisp":"".padRight(30),
                "Cap Corrisp":"".padRight(5),
                "Provincia Corrisp":"".padRight(2),
                "Nazione":"".padRight(3),
                "Codice Fiscale":"${codFiscale}".padRight(16),
                "Partita Iva":"${partitaIva}".padRight(11),
                "Telefono":telefono.padRight(20),
                "Cellulare":cellulare.padRight(20),
                "Email":polizza.email.trim().padRight(100),
                "Targa":targa.padRight(8),
                "Telaio":"${polizza.telaio?polizza.telaio.trim().toUpperCase():""}".padRight(20),
                "Codice Modello":"${codModello}".padRight(7),
                "Modello":"${polizza.modello?polizza.modello.trim().toUpperCase():""}".padRight(100),
                "Codice Marca":"".padRight(2),
                "Marca":"${polizza.marca?polizza.marca.toUpperCase().trim():""}".padRight(30),
                "Cilindrata":"".padRight(4),
                "Alimentazione":"".padLeft(10),
                "Black Box":blackBox.padRight(1),
                "Tipo Veicolo":"".padRight(1),
                "Tipo Targa":"".padRight(10),
                "Cavalli":"".padRight(5),
                "Quintali":"".padRight(5),
                "Kw":"".padRight(5),
                "Codice Infocar":"".padRight(20),
                "Veicolo Nuovo":veicolonuovo.padRight(1),
                "Data Consegna":"".padRight(8),
                "Data Immatricolaz.":polizza.dataImmatricolazione? polizza.dataImmatricolazione.format("yyyyMMdd").padRight(8):"".padRight(8),
                "Codice Dealer":polizza.dealer.codice.padRight(7),
                "Dealer":polizza.dealer.ragioneSocialeD.trim().toUpperCase().padRight(40),
                "Venditore":"".padRight(50),
                "Pacchetto":"${codPacchetto}".padRight(5),
                "Capitale Assicurato":"${(polizza.valoreAssicurato*100).round()}".padLeft(13),
                "Data Decorrenza":"".padRight(8),
                "Data Ultima Scadenza":"".padRight(8),
                "Data Inizio Polizza":polizza.dataDecorrenza?polizza.dataDecorrenza.format("yyyyMMdd").padRight(8):"".padRight(8),
                "Data Fine Polizza":polizza.dataScadenza?polizza.dataScadenza.format("yyyyMMdd").padRight(8):"".padRight(8),
                "Ora Effetto":"00:01:00".padRight(8),
                "Ora Scadenza":"24:00:00".padRight(8),
                "Data Scad Rate":"".padRight(8),
                "Tipo Frazionamento":"".padRight(10),
                "Numero Rate":"".padLeft(3),
                "Durata":"".padRight(3),
                "Durata Copertura":polizza.durata.toString().padLeft(3),
                "Premio Netto":"${premioImponibile}".padLeft(13),
                "Imposte":"${imposte}".padLeft(13),
                "Importo SSN":"".padLeft(13),
                "Premio Lordo":"${premioLordo}".padLeft(13),
                "Provvigione":"${provvigione}".padLeft(13),
                "Filler":"".padRight(13),
                "Filler2":"".padRight(13),
                "Numero Flusso":"".padRight(6),
                "Tipo Pacchetto":"${pacchetto}".padRight(10),
                "Agenzia":" ".padRight(10),
                "Produttore":"${codproduttore}".padRight(10),
                "IBAN":"".padRight(27),
                "Data Incasso":"".padRight(8),
                "Vincolo":"".padRight(1),
                "Data Vincolo":"".padRight(8),
                "Ente Vincolatario":"".padRight(200),
                "Nominativo Int PRA":"".padRight(100),
                "Indirizzo Int. PRA":"".padRight(120),
                "Comune Int. PRA":"".padRight(60),
                "Cap Int. PRA":"".padRight(5),
                "Provincia Int. PRA":"".padRight(2),
                "Codice Fiscale Int. PRA":"".padRight(16),
                "Cognome Int PRA":"".padRight(50),
                "Nome Int PRA":"".padRight(50),
                "Cognome Assicurato":"".padRight(50),
                "nome Assicurato":"".padRight(50),
                "Filler3":"".padRight(13),
                "MotivoUscita":"".padRight(10)
        ]
        def dataInvioTracciato=new Date()
        /*if(polizza.codOperazione=="S" || polizza.codOperazione=="E"){
            return new TracciatoCompagnia(polizza: polizza, tracciatoCompagnia:tracciato.values().join(), tipoPolizza:polizza.tipoPolizza, dataInvio:  dataInvioTracciato, dataCaricamento: dataInvioTracciato, cash: isCash, annullata: annullata)

        }else{*/
            return new TracciatoCompagnia(polizza: polizza, tracciatoCompagnia:tracciato.values().join(), tipoPolizza:polizza.tipoPolizza, dataInvio:  dataInvioTracciato, cash: isCash, annullata: annullata)
        //}
    }
    def generaTracciatoCompagniaRinn(def polizza) {
        def telefono=""
        def cellulare=""
        def nome=""
        def cognome="${polizza.cliente.toUpperCase()trim()}"
        def indirizzo="${polizza.indirizzo.toUpperCase()trim()}"
        def nuovo=polizza.nuovo
        def veicolonuovo
        if(nuovo=="NUOVO") veicolonuovo="S"
        else veicolonuovo="N"
        def onStar=""
        def blackBox
        if(onStar) blackBox="S"
        else blackBox="N"
        def stepN=""
        def codStep=""
        if(stepN=="step 1"){ codStep="step 1"}
        else if (stepN=="step 2"){codStep="step 2"}
        else if (stepN=="step 3"){codStep="step 3"}
        boolean isCash=false
        boolean annullata=false
        def coperturaR=polizza.coperturaRichiesta
        def tipoOper="A"
        def premioImponibile
        def premioLordo
        def imposte
        def provvigione
        def dataOperazione
        premioImponibile="${(polizza.premioImponibile*100).round()}"
        premioLordo="${(polizza.premioLordo*100).round()}"
        provvigione="${((polizza.provvDealer+polizza.provvVenditore+polizza.provvGmfi+polizza.rappel)*100).round()}"
        imposte="${(polizza.imposte*100).round()}"
        dataOperazione=polizza.dataDecorrenza.format("yyyyMMdd")
        if(polizza.codOperazione=="S"){
            tipoOper="S"
        }else if (polizza.codOperazione=="E"){
            annullata=true
            tipoOper="E"
            /*premioImponibile="-${(polizza.premioImponibile*100).round()}"
            provvigione="-${((polizza.provvDealer+polizza.provvVenditore+polizza.provvGmfi+polizza.rappel)*100).round()}"
            premioLordo="-${(polizza.premioLordo*100).round()}"*/
            imposte=0
            dataOperazione=polizza.dataAnnullamento.format("yyyyMMdd")
        }
        def codCoper=""
       /* if(coperturaR =="SILVER"){codCoper="S"}
        else if(coperturaR=="GOLD"){codCoper="G"}
        else if(coperturaR=="PLATINUM"){codCoper="P"}
        else if(coperturaR=="PLATINUM COLLISIONE"){codCoper="C"}
        else if(coperturaR=="PLATINUM KASKO"){codCoper="K"}*/
        codCoper=polizza.codPacchetto
        def tipoPolizza=polizza.tipoPolizza.toString().toUpperCase()
        def pacchetto
        pacchetto="X"
        def targa
        if(polizza.targa){
            targa=polizza.targa.toString().trim().toUpperCase()
        }else{
            targa=""
        }
        def partitaIva=""
        def codFiscale=""
        partitaIva=polizza.cFiscale.toUpperCase().trim()
        def datadecorrenza= polizza.dataDecorrenza
        def dataTariffa=Date.parse("yyyy-MM-dd","2017-06-16")
        def numpolizza="DLI950000003"
        if(!polizza.ramo.toString().trim().isEmpty()) {
            def ramo = polizza.ramo.toString().trim()
            if (datadecorrenza > dataTariffa) {
                numpolizza="DLI950000007"
                if (ramo.contains('D/02') || ramo.contains('D/12') || ramo.contains('D/30')) {
                    polizza.codPacchetto = "2RPKR"
                } else if (ramo.toString().contains('D/11') || ramo.toString().contains('D/29')) {
                    polizza.codPacchetto = "2RPCR"
                } else if (ramo.toString().contains('W/99') || ramo.toString().contains('D/10') || ramo.toString().contains('D/28')) {
                    polizza.codPacchetto = "2RP0R"
                } else if (ramo.toString().contains('W/98') || ramo.toString().contains('D/09') || ramo.toString().contains('D/27')) {
                    polizza.codPacchetto = "2RG0R"
                } else if (ramo.toString().contains('W/97') || ramo.toString().contains('D/08') || ramo.toString().contains('D/26')) {
                    polizza.codPacchetto = "2RS0R"
                } else if (ramo.toString().contains('W/96') || ramo.toString().contains('D/07') || ramo.toString().contains('D/25')) {
                    polizza.codPacchetto = "2RPK0"
                } else if (ramo.toString().contains('W/95') || ramo.toString().contains('D/06') || ramo.toString().contains('D/24')) {
                    polizza.codPacchetto = "2RPC0"
                } else if (ramo.toString().contains('W/94') || ramo.toString().contains('D/05') || ramo.toString().contains('D/23')) {
                    polizza.codPacchetto = "2RP00"
                } else if (ramo.toString().contains('W/93') || ramo.toString().contains('D/04') || ramo.toString().contains('D/22')) {
                    polizza.codPacchetto = "2RG00"
                } else if (ramo.toString().contains('W/92') || ramo.toString().contains('D/03') || ramo.toString().contains('D/21')) {
                    polizza.codPacchetto = "2RS00"
                }
            }
        }



        //println "polizza cod pachetto ${polizza.codPacchetto}"
        def tracciato = [
                "Numero Polizza":"${numpolizza}".padRight(20),
                "Id Applicazione":"".padRight(5),
                "Numero Contratto":polizza.noPolizza.padRight(20),
                "Tipo Operazione":"${tipoOper}",
                "Data Operazione":"${dataOperazione}".padRight(8),
                "Nominativo":"${cognome.toUpperCase().trim()}".padRight(100),
                "Indirizzo":"${indirizzo.toUpperCase().trim()}".padRight(120),
                "Comune":polizza.localita.toUpperCase().trim().padRight(60),
                "Cap":polizza.cap.trim().padRight(5,"0"),
                "Provincia":polizza.provincia.toUpperCase().trim().padRight(2),
                "Indirizzo Corrisp":"".padRight(40),
                "Comune Corrisp":"".padRight(30),
                "Cap Corrisp":"".padRight(5),
                "Provincia Corrisp":"".padRight(2),
                "Nazione":"".padRight(3),
                "Codice Fiscale":"${codFiscale}".padRight(16),
                "Partita Iva":"${partitaIva}".padRight(11),
                "Telefono":telefono.padRight(20),
                "Cellulare":cellulare.padRight(20),
                "Email":"".padRight(100),
                "Targa":targa.padRight(8),
                "Telaio":"${polizza.telaio?polizza.telaio.trim().toUpperCase():""}".padRight(20),
                "Codice Modello":"".padRight(7),
                "Modello":"${polizza.modello?polizza.modello.trim().toUpperCase():""}".padRight(100),
                "Codice Marca":"".padRight(2),
                "Marca":"${polizza.marca?polizza.marca.toUpperCase().trim():""}".padRight(30),
                "Cilindrata":"".padRight(4),
                "Alimentazione":"".padLeft(10),
                "Black Box":blackBox.padRight(1),
                "Tipo Veicolo":"".padRight(1),
                "Tipo Targa":"".padRight(10),
                "Cavalli":"".padRight(5),
                "Quintali":"".padRight(5),
                "Kw":"".padRight(5),
                "Codice Infocar":"".padRight(20),
                "Veicolo Nuovo":veicolonuovo.padRight(1),
                "Data Consegna":"".padRight(8),
                "Data Immatricolaz.":polizza.dataImmatricolazione? polizza.dataImmatricolazione.format("yyyyMMdd").padRight(8):"".padRight(8),
                "Codice Dealer":"".padRight(7),
                "Dealer":"".padRight(40),
                "Venditore":"".padRight(50),
                "Pacchetto":"${codCoper}".padRight(5),
                "Capitale Assicurato":"${(polizza.valoreAssicurato*100).round()}".padLeft(13),
                "Data Decorrenza":"".padRight(8),
                "Data Ultima Scadenza":"".padRight(8),
                "Data Inizio Polizza":polizza.dataDecorrenza.format("yyyyMMdd").padRight(8).padRight(8),
                "Data Fine Polizza":polizza.dataScadenza.format("yyyyMMdd").padRight(8).padRight(8),
                "Ora Effetto":"00:01:00".padRight(8),
                "Ora Scadenza":"24:00:00".padRight(8),
                "Data Scad Rate":"".padRight(8),
                "Tipo Frazionamento":"".padRight(10),
                "Numero Rate":"".padLeft(3),
                "Durata":"".padRight(3),
                "Durata Copertura":polizza.durata.toString().padLeft(3),
                "Premio Netto":"${premioImponibile}".padLeft(13),
                "Imposte":"${imposte}".padLeft(13),
                "Importo SSN":"".padLeft(13),
                "Premio Lordo":"${premioLordo}".padLeft(13),
                "Provvigione":"${provvigione}".padLeft(13),
                "Filler":"".padRight(13),
                "Filler2":"".padRight(13),
                "Numero Flusso":"".padRight(6),
                "Tipo Pacchetto":"${pacchetto}".padRight(10),
                "Agenzia":" ".padRight(10),
                "Produttore":"".padRight(10),
                "IBAN":"".padRight(27),
                "Data Incasso":"".padRight(8),
                "Vincolo":"".padRight(1),
                "Data Vincolo":"".padRight(8),
                "Ente Vincolatario":"".padRight(200),
                "Nominativo Int PRA":"".padRight(100),
                "Indirizzo Int. PRA":"".padRight(120),
                "Comune Int. PRA":"".padRight(60),
                "Cap Int. PRA":"".padRight(5),
                "Provincia Int. PRA":"".padRight(2),
                "Codice Fiscale Int. PRA":"".padRight(16),
                "Cognome Int PRA":"".padRight(50),
                "Nome Int PRA":"".padRight(50),
                "Cognome Assicurato":"".padRight(50),
                "nome Assicurato":"".padRight(50),
                "Filler3":"".padRight(13),
                "MotivoUscita":"".padRight(10)
        ]
        def dataInvioTracciato=new Date()
        /*if(polizza.codOperazione=="S" || polizza.codOperazione=="E"){
            return new TracciatoCompagnia(tracciatoCompagnia:tracciato.values().join(), tipoPolizza: TipoPolizza.RINNOVI,dataInvio:dataInvioTracciato,dataCaricamento: dataInvioTracciato, cash: isCash, annullata: annullata)

        }else {*/
            return new TracciatoCompagnia(tracciatoCompagnia:tracciato.values().join(), tipoPolizza: TipoPolizza.RINNOVI,dataInvio:dataInvioTracciato, cash: isCash, annullata: annullata)

        //}
    }
    def generaTracciatoPAI(Polizza polizza, boolean speciale) {
        boolean isCash=false
        def copertura=(polizza.coperturaRichiesta)? polizza.coperturaRichiesta.toUpperCase():""
        def kasko=(copertura)? copertura.contains("KASKO") ?"K":"I" :"I"
        def cognome= "${polizza.cognome.trim().toUpperCase()} ${polizza.nome?.trim().toUpperCase()}"
        def nome=""
        def dataScadenza=use (TimeCategory){polizza.dataDecorrenza +3.months}
        def codFiscale="${polizza.partitaIva.trim().toUpperCase()}"
        def telCasa= "${(polizza.telefono.trim().replaceAll("[^0-9]+",""))}"
        def cellulare="${polizza.cellulare.toString().trim().replaceAll("[^0-9]+","")}"
        def marca
        if(Marche.findByMarca(polizza.marca.toString().trim())){
            marca=Marche.findByMarca(polizza.marca.toString().trim()).codice
        }else{marca=""}
        def nuovo=polizza.nuovo
        def veicolonuovo
        if(nuovo) veicolonuovo="S"
        else veicolonuovo="N"
        def tipoOper="0"
        if(polizza.codOperazione=="S"){
            tipoOper="I"
        }
        def tracciato = [
                "Kasko":"${kasko}".padLeft(1),
                "Codice modello":"".padLeft(5),
                "Modello veicolo":polizza.modello.trim().toUpperCase().padLeft(20),
                "Targa":"${polizza.targa ? polizza.targa.trim().toUpperCase():""}".padLeft(15),
                "Cognome":cognome.padLeft(40),
                "Nome":nome.padLeft(40),
                "Provincia":polizza.provincia.toUpperCase().padLeft(2),
                "Valuta":"E".padLeft(1),
                "Valore assicurato":"${(polizza.valoreAssicurato *100).round()}".padLeft(9),
                "Data immatricolazione":"".padLeft(8),
                "Durata furto incendio":"".padLeft(3),
                "Indirizzo":polizza.indirizzo.toUpperCase().padLeft(30),
                "Cap":polizza.cap.padLeft(5),
                "Città":polizza.localita?.toUpperCase().padLeft(30),
                "Data di nascita":"".padLeft(8),
                "Provincia nascita":"".padLeft(2),
                "Sesso":"".padLeft(1),
                "Numero polizza":"${polizza.noPolizza}".padLeft(10),
                "Codice operazione":"${tipoOper}".padLeft(3),
                "Data apertura contratto":"".padLeft(8),
                "Importo assicurato":"".padLeft(9),
                "Codice fiscale / Partita IVA":"${codFiscale}".padLeft(16),
                "Marca":  "${marca}".padLeft(5),
                "Autocarro": "".padLeft(1),
                "Uso": "".padLeft(1),
                "Scadenza vincolo": "".padLeft(8),
                "Codice dealer": "".padLeft(5),
                "Numero telaio": "${polizza.telaio?polizza.telaio.trim().toUpperCase():""}".padLeft(17),
                "Numero rinnovo": "".padLeft(2),
                "Vincolo": "".padLeft(1),
                "Pcl":  "090".padLeft(9),
                "Old contra": "".padLeft(10),
                "Cv fisc": "".padLeft(4),
                "Quintali": "".padLeft(4),
                "Data decorrenza":"${(polizza.dataDecorrenza)? polizza.dataDecorrenza.format("yyyyMMdd"):""}".padLeft(8),
                "Buyback": "".padLeft(1),
                "Satellitare": "".padLeft(1),
                "Data scadenza": "${(polizza.dataDecorrenza)? dataScadenza.format("yyyyMMdd") :""}".padLeft(8),
                "Rc": "N".padLeft(1),
                "Telefono casa": "${telCasa}".padLeft(15),
                "Telefono ufficio": "".padLeft(15),
                "Telefono": "".padLeft(15),
                "Cellulare":"${cellulare}".padLeft(15),
                "Provvigioni dealer":"009".padLeft(9),
                "Provvigioni Mach1":"004".padLeft(9),
                "Provvigioni società commerciale": "".padLeft(9),
                "Totale caricamenti":"013".padLeft(9),
                "Imponibile": "088".padLeft(9),
                "Imposte":"002".padLeft(9),
                "Pdu":  "075".padLeft(9),
                "Iban": "".padLeft(34),
                "Collisione": "N".padLeft(1),
                "Data operazione": "".padLeft(8),
                "Codice prodotto":  "OP".padLeft(2),
                "Tipologia veicolo": "${veicolonuovo}".padLeft(1),
                "Rischio": "".padLeft(1),
                "Zona": "".padLeft(1),
                "Email": "${polizza.email}".padLeft(50),
                "Mini collisione": "".padLeft(2),
                "Garanzia2": "".padLeft(2),
                "Durata garanzia2": "".padLeft(2),
                "Durata kasko": "".padLeft(2),
                "Durata collisione": "".padLeft(2),
                "Durata valore a nuovo": "".padLeft(2),
                "Codice società commerciale": "".padLeft(3),
                "Codice venditore": "".padLeft(3),
                "Provvigioni venditore": "".padLeft(9),
                "Codice segnalatore": "".padLeft(3),
                "Provvigioni segnalatore": "".padLeft(9),
                "Integra": "".padLeft(1),
                "Numero polizza integrata": "".padLeft(10),
                "Durata integra": "".padLeft(2),
                "Codice iban": "".padLeft(27),
                "Intestatario iban": "".padLeft(30),
                "Token": "".padLeft(16),
                "Cognome nome assicurato": "".padLeft(40),
                "Partita iva assicurato": "".padLeft(11),
                "Indirizzo assicurato": "".padLeft(30),
                "Citta assicurato": "".padLeft(30),
                "Cap assicurato": "".padLeft(5),
                "Provincia assicurato": "".padLeft(2),
                "Codice fiscale assicurato": "".padLeft(16),
                "Data nascita assicurato": "".padLeft(8),
                "Sesso assicurato": "".padLeft(1),
                "Seriale Lojack": "".padLeft(7)
        ]
        def dataInvioTracciato=new Date()
            return new TracciatoPAI(polizza: polizza, tracciatoPAI: tracciato.values().join(), dataInvio: dataInvioTracciato, speciale: speciale, tipo: TipoPai.SPECIALE, tipoPolizza: polizza.tipoPolizza, cash: isCash)

    }
    def generaTracciatoPAIOMaggio(Polizza polizza, def dataDecorrenza) {
        boolean isCash=false
        def copertura=(polizza.coperturaRichiesta)? polizza.coperturaRichiesta.toUpperCase():""
        def kasko=(copertura)? copertura.contains("KASKO") ?"K":"I" :"I"
        def cognome= "${polizza.cognome.trim().toUpperCase()} ${polizza.nome?.trim().toUpperCase()}"
        def nome=""
        def newdataDecorrenza=""
        def codFiscale="${polizza.partitaIva.trim().toUpperCase()}"
        def telCasa= "${(polizza.telefono.trim().replaceAll("[^0-9]+",""))}"
        def cellulare="${polizza.cellulare.toString().trim().replaceAll("[^0-9]+","")}"
        def marca
        def nuovo=polizza.nuovo
        def veicolonuovo
        def tipoOper="0"
        def codProd="OP"
        //println "data decorrenza $polizza.dataDecorrenza"
        def dataScadenza=use (TimeCategory){ Date.parse("dd/MM/yyyy", dataDecorrenza) +6.months}
        if(polizza.tipoPolizza!=TipoPolizza.CASH){
            dataScadenza=use (TimeCategory){Date.parse("dd/MM/yyyy", dataDecorrenza) +6.months}
            newdataDecorrenza=polizza.dataDecorrenza.format("yyyyMMdd")
            tipoOper="BB"
        }else{
            dataScadenza=use (TimeCategory){Date.parse("dd/MM/yyyy", dataDecorrenza) +3.months}
            newdataDecorrenza=new Date().parse("dd/MM/yyyy", dataDecorrenza).format("yyyyMMdd")
            isCash=true
            codProd="PM"
        }
        if(Marche.findByMarca(polizza.marca.toString().trim())){
            marca=Marche.findByMarca(polizza.marca.toString().trim()).codice
        }else{marca=""}
        if(nuovo) veicolonuovo="S"
        else veicolonuovo="N"
        def tracciato = [
                "Kasko":"${kasko}".padLeft(1),
                "Codice modello":"".padLeft(5),
                "Modello veicolo":polizza.modello.trim().toUpperCase().padLeft(20),
                "Targa":"${polizza.targa ? polizza.targa.trim().toUpperCase():""}".padLeft(15),
                "Cognome":cognome.padLeft(40),
                "Nome":nome.padLeft(40),
                "Provincia":polizza.provincia.toUpperCase().padLeft(2),
                "Valuta":"E".padLeft(1),
                "Valore assicurato":"${(polizza.valoreAssicurato *100).round()}".padLeft(9),
                "Data immatricolazione":"".padLeft(8),
                "Durata furto incendio":"".padLeft(3),
                "Indirizzo":polizza.indirizzo.toUpperCase().padLeft(30),
                "Cap":polizza.cap.padLeft(5),
                "Città":polizza.localita?.toUpperCase().padLeft(30),
                "Data di nascita":"".padLeft(8),
                "Provincia nascita":"".padLeft(2),
                "Sesso":"".padLeft(1),
                "Numero polizza":"${polizza.noPolizza}".padLeft(10),
                "Codice operazione":"${tipoOper}".padLeft(3),
                "Data apertura contratto":"".padLeft(8),
                "Importo assicurato":"".padLeft(9),
                "Codice fiscale / Partita IVA":"${codFiscale}".padLeft(16),
                "Marca":  "${marca}".padLeft(5),
                "Autocarro": "".padLeft(1),
                "Uso": "".padLeft(1),
                "Scadenza vincolo": "".padLeft(8),
                "Codice dealer": "".padLeft(5),
                "Numero telaio": "${polizza.telaio?polizza.telaio.trim().toUpperCase():""}".padLeft(17),
                "Numero rinnovo": "".padLeft(2),
                "Vincolo": "".padLeft(1),
                "Pcl":  "077".padLeft(9),
                "Old contra": "".padLeft(10),
                "Cv fisc": "".padLeft(4),
                "Quintali": "".padLeft(4),
                "Data decorrenza":"${newdataDecorrenza}".padLeft(8),
                "Buyback": "".padLeft(1),
                "Satellitare": "".padLeft(1),
                "Data scadenza": "${dataScadenza.format("yyyyMMdd")}".padLeft(8),
                "Rc": "N".padLeft(1),
                "Telefono casa": "${telCasa}".padLeft(15),
                "Telefono ufficio": "".padLeft(15),
                "Telefono": "".padLeft(15),
                "Cellulare":"${cellulare}".padLeft(15),
                "Provvigioni dealer":"000".padLeft(9),
                "Provvigioni Mach1":"000".padLeft(9),
                "Provvigioni società commerciale": "".padLeft(9),
                "Totale caricamenti":"000".padLeft(9),
                "Imponibile": "075".padLeft(9),
                "Imposte":"002".padLeft(9),
                "Pdu":  "075".padLeft(9),
                "Iban": "".padLeft(34),
                "Collisione": "N".padLeft(1),
                "Data operazione": "".padLeft(8),
                "Codice prodotto":  "${codProd}".padLeft(2),
                "Tipologia veicolo": "${veicolonuovo}".padLeft(1),
                "Rischio": "".padLeft(1),
                "Zona": "".padLeft(1),
                "Email": "${polizza.email}".padLeft(50),
                "Mini collisione": "".padLeft(2),
                "Garanzia2": "".padLeft(2),
                "Durata garanzia2": "".padLeft(2),
                "Durata kasko": "".padLeft(2),
                "Durata collisione": "".padLeft(2),
                "Durata valore a nuovo": "".padLeft(2),
                "Codice società commerciale": "".padLeft(3),
                "Codice venditore": "".padLeft(3),
                "Provvigioni venditore": "".padLeft(9),
                "Codice segnalatore": "".padLeft(3),
                "Provvigioni segnalatore": "".padLeft(9),
                "Integra": "".padLeft(1),
                "Numero polizza integrata": "".padLeft(10),
                "Durata integra": "".padLeft(2),
                "Codice iban": "".padLeft(27),
                "Intestatario iban": "".padLeft(30),
                "Token": "".padLeft(16),
                "Cognome nome assicurato": "".padLeft(40),
                "Partita iva assicurato": "".padLeft(11),
                "Indirizzo assicurato": "".padLeft(30),
                "Citta assicurato": "".padLeft(30),
                "Cap assicurato": "".padLeft(5),
                "Provincia assicurato": "".padLeft(2),
                "Codice fiscale assicurato": "".padLeft(16),
                "Data nascita assicurato": "".padLeft(8),
                "Sesso assicurato": "".padLeft(1),
                "Seriale Lojack": "".padLeft(7)
        ]
        def dataInvioTracciato=new Date()
        return new TracciatoPAI(polizza: polizza, tracciatoPAI: tracciato.values().join(), tipo: TipoPai.OMAGGIO, dataInvio: dataInvioTracciato,tipoPolizza: polizza.tipoPolizza, omaggio: true, cash: isCash)
    }
    def generaTracciatoPAIOmaggioRinn(def polizza) {
        boolean isCash=false
        def copertura=(polizza.coperturaRichiesta)? polizza.coperturaRichiesta.toUpperCase():""
        def kasko=(copertura)? copertura.contains("KASKO") ?"K":"I" :"I"
        def cognome="${polizza.cliente.trim()}"
        def codFiscale= "${polizza.cFiscale}"
        def telCasa= ""
        def cellulare=""
        def marca
        def tipoOper="0"
        def dataDecorrenza=polizza.dataDecorrenza.format("yyyyMMdd")
        def dataScadenza=polizza.dataScadenza.format("yyyyMMdd")
        if(Marche.findByMarca(polizza.marca.toString().trim())){
            marca=Marche.findByMarca(polizza.marca.toString().trim()).codice
        }else{marca=""}
        def nuovo=polizza.nuovo
        def veicolonuovo
        if(nuovo) veicolonuovo="S"
        else veicolonuovo="N"
        def tracciato = [
                "Kasko":"${kasko}".padLeft(1),
                "Codice modello":"".padLeft(5),
                "Modello veicolo":polizza.modello.trim().toUpperCase().padLeft(20),
                "Targa":"${polizza.targa ? polizza.targa.trim().toUpperCase():""}".padLeft(15),
                "Cognome":cognome.padLeft(40),
                "Nome":"".padLeft(40),
                "Provincia":polizza.provincia.toUpperCase().padLeft(2),
                "Valuta":"E".padLeft(1),
                "Valore assicurato":"${(polizza.valoreAssicurato *100).round()}".padLeft(9),
                "Data immatricolazione":"".padLeft(8),
                "Durata furto incendio":"".padLeft(3),
                "Indirizzo":polizza.indirizzo.toUpperCase().padLeft(30),
                "Cap":polizza.cap.padLeft(5),
                "Città":polizza.localita?.toUpperCase().padLeft(30),
                "Data di nascita":"".padLeft(8),
                "Provincia nascita":"".padLeft(2),
                "Sesso":"".padLeft(1),
                "Numero polizza":"${polizza.noPolizza}".padLeft(10),
                "Codice operazione":"${tipoOper}".padLeft(3),
                "Data apertura contratto":"".padLeft(8),
                "Importo assicurato":"".padLeft(9),
                "Codice fiscale / Partita IVA":"${codFiscale}".padLeft(16),
                "Marca":  "${marca}".padLeft(5),
                "Autocarro": "".padLeft(1),
                "Uso": "".padLeft(1),
                "Scadenza vincolo": "".padLeft(8),
                "Codice dealer": "".padLeft(5),
                "Numero telaio": "${polizza.telaio?polizza.telaio.trim().toUpperCase():""}".padLeft(17),
                "Numero rinnovo": "".padLeft(2),
                "Vincolo": "".padLeft(1),
                "Pcl":  "077".padLeft(9),
                "Old contra": "".padLeft(10),
                "Cv fisc": "".padLeft(4),
                "Quintali": "".padLeft(4),
                "Data decorrenza":"${(polizza.dataDecorrenza)? dataDecorrenza:""}".padLeft(8),
                "Buyback": "".padLeft(1),
                "Satellitare": "".padLeft(1),
                "Data scadenza": "${(polizza.dataDecorrenza)? dataScadenza:""}".padLeft(8),
                "Rc": "N".padLeft(1),
                "Telefono casa": "${telCasa}".padLeft(15),
                "Telefono ufficio": "".padLeft(15),
                "Telefono": "".padLeft(15),
                "Cellulare":"${cellulare}".padLeft(15),
                "Provvigioni dealer":"000".padLeft(9),
                "Provvigioni Mach1":"000".padLeft(9),
                "Provvigioni società commerciale": "".padLeft(9),
                "Totale caricamenti":"000".padLeft(9),
                "Imponibile": "075".padLeft(9),
                "Imposte":"002".padLeft(9),
                "Pdu":  "075".padLeft(9),
                "Iban": "".padLeft(34),
                "Collisione": "N".padLeft(1),
                "Data operazione": "".padLeft(8),
                "Codice prodotto":  "PM".padLeft(2),
                "Tipologia veicolo": "${veicolonuovo}".padLeft(1),
                "Rischio": "".padLeft(1),
                "Zona": "".padLeft(1),
                "Email": "${polizza.email}".padLeft(50),
                "Mini collisione": "".padLeft(2),
                "Garanzia2": "".padLeft(2),
                "Durata garanzia2": "".padLeft(2),
                "Durata kasko": "".padLeft(2),
                "Durata collisione": "".padLeft(2),
                "Durata valore a nuovo": "".padLeft(2),
                "Codice società commerciale": "".padLeft(3),
                "Codice venditore": "".padLeft(3),
                "Provvigioni venditore": "".padLeft(9),
                "Codice segnalatore": "".padLeft(3),
                "Provvigioni segnalatore": "".padLeft(9),
                "Integra": "".padLeft(1),
                "Numero polizza integrata": "".padLeft(10),
                "Durata integra": "".padLeft(2),
                "Codice iban": "".padLeft(27),
                "Intestatario iban": "".padLeft(30),
                "Token": "".padLeft(16),
                "Cognome nome assicurato": "".padLeft(40),
                "Partita iva assicurato": "".padLeft(11),
                "Indirizzo assicurato": "".padLeft(30),
                "Citta assicurato": "".padLeft(30),
                "Cap assicurato": "".padLeft(5),
                "Provincia assicurato": "".padLeft(2),
                "Codice fiscale assicurato": "".padLeft(16),
                "Data nascita assicurato": "".padLeft(8),
                "Sesso assicurato": "".padLeft(1),
                "Seriale Lojack": "".padLeft(7)
        ]
        def dataInvioTracciato=new Date()
        return new TracciatoPAI(tracciatoPAI: tracciato.values().join(), dataInvio: dataInvioTracciato, cash: isCash, omaggio: true, tipo: TipoPai.OMAGGIO, tipoPolizza: TipoPolizza.RINNOVI)
    }
    def generaFileDL(PolizzaRCA polizza){

        def regtarga="^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}\$"
        def rispostaCert
        def numPolizzagenerale=(polizza.tipoPolizza==TipoPolizza.FINANZIATE)?"DLI950000011":(polizza.tipoPolizza==TipoPolizza.LEASING)?"DLI950000010":"DLI950000012"
        def tipoPolizza=(polizza.tipoPolizza==TipoPolizza.FINANZIATE)?"OPR02":(polizza.tipoPolizza==TipoPolizza.LEASING)?"OPR01":"OPR03"
        def targa=polizza.targa!=null?polizza.targa.toString().toUpperCase():""
        def marca=polizza.marca.toString().toUpperCase()
        def modello=polizza.modello.toString().toUpperCase()
        def modellor=""
        if(modello.contains("ANTARA")){
            modellor="ANTARA"
        }else if(modello.contains("ADAM")){
            modellor="ADAM"
        }else if(modello.contains("ASTRA")){
            modellor="ASTRA"
        }else if(modello.contains("CASCADA")){
            modellor="CASCADA"
        }else if(modello.contains("COMBO")){
            modellor="COMBO"
        }else if(modello.contains("CORSA")){
            modellor="CORSA"
        }else if(modello.contains("CROSSLAND X")){
            modellor="CROSSLAND X"
        }else if(modello.contains("INSIGNIA")){
            modellor="INSIGNIA"
        }else if(modello.contains("KARL")){
            modellor="KARL"
        }else if(modello.contains("MERIVA")){
            modellor="MERIVA"
        }else if(modello.contains("MOKKA")){
            modellor="MOKKA X"
        }else if(modello.contains("ZAFIRA")){
            modellor="ZAFIRA"
        }else if(modello.contains("MOVANO")){
            modellor="MOVANO"
        }else if(modello.contains("VIVARO")){
            modellor="VIVARO"
        }else if(modello.contains("AGILA")){
            modellor="AGILA"
        }else if(modello.contains("GRANDLAND X")){
            modellor="GRANDLAND X"
        }
        def tipoveicolo=(polizza.tipoVeicolo==TipoVeicolo.AUTOVEICOLO)?"AUTOVETTURA":"AUTOCARRO"
        def cilindrata=(polizza.cilindrata)?:""
        def cavalli=(polizza.tipoVeicolo==TipoVeicolo.AUTOVEICOLO)?polizza.cavalli:""
        def quintali=(polizza.tipoVeicolo==TipoVeicolo.AUTOCARRO)?polizza.quintali:""
        def kw=polizza.kw
        def dataimmatricolazione=polizza.dataImmatricolazione.format("dd/MM/yyyy")
        def datadecorrenza=polizza.dataDecorrenza.format("dd/MM/yyyy")
        def provImmatricolazione=polizza.provImmatricolazione
        def codoper=(polizza.codOperazione=="0")?"I":"E"
        def dealercode=polizza.dealer.codice
        def valoreAssicurato=polizza.valoreAssicurato
        def tipotarga=(polizza.targa.matches(regtarga)?"N":"V")
        def emaildealer=polizza.emailDealer
        def partitaIva=polizza.partitaIva
        def cognome=polizza.cognome
        def nome=polizza.nome
        def indirizzo=polizza.indirizzo
        def localita=polizza.localita
        def provincia=polizza.provincia
        def cap=polizza.cap
        def email=polizza.email
        def targa1=""
        if(polizza.targa1a!=''&& polizza.targa1a!='null' && polizza.targa1a!=null){
            targa1=polizza.targa1a
        }
        def bonus=(polizza.targa1a && !polizza.primaImmatr)?polizza.bonus?:"S":"N"
        def nopratica=polizza.noPolizza
        def premioimponibile=polizza.premioImponibile
        def premiolordo=polizza.premioLordo

        def lines =  [numPolizzagenerale, tipoPolizza, "N","N","N","",targa,marca,modellor,tipoveicolo,cilindrata,cavalli,quintali,kw,dataimmatricolazione,datadecorrenza,provImmatricolazione,codoper,"",dealercode,valoreAssicurato,tipoPolizza,"N",tipotarga,emaildealer,"",partitaIva,cognome,nome,indirizzo,localita,provincia,cap,"",email,targa1,bonus,nopratica,premioimponibile,premiolordo].join(';')
        def stream = new ByteArrayOutputStream()
        stream.write(lines.getBytes())
        stream.close()
        /**qui faccio la modifica**/
        def filename="OPEL_RCA_${polizza.targa.toUpperCase()}_${new Date().format("YYYYMMddhhmmss")}.csv"
        //def filename="OPEL_RCA_${new Date().format("YYYYMMddhhmmss")}.csv"
        def caricamentoftp=ftpService.uploadFileDLMach1(filename, stream)
        rispostaCert=caricamentoftp
        if(Environment.current == Environment.PRODUCTION ){
            if(rispostaCert){
                rispostaCert= ftpService.uploadFileDLcompagnia(filename,stream)
            }
        }

        return rispostaCert
    }
    private Calendar getCalendar(Date date) {
        Calendar cal = Calendar.getInstance(Locale.ITALY)
        cal.setTime(date)
        return cal
    }
}
