package webservice

import cashopel.polizze.CodProdotti
import cashopel.polizze.Documenti
import cashopel.polizze.Polizza
import cashopel.polizze.TipoDoc
import cashopel.utenti.Log
import grails.converters.JSON
import grails.util.Environment
import groovy.time.TimeCategory
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import org.grails.web.json.JSONObject
import org.grails.web.servlet.mvc.GrailsWebRequest
import org.grails.web.util.WebUtils
import wslite.rest.RESTClientException

import javax.servlet.http.HttpSession
import java.text.SimpleDateFormat

class IAssicurWebService {
    static transactional = false
    def iAssicurClient
    def grailsApplication
    def DLWebService

    def query(String sql) throws RESTClientException {
        def logg
        def path = grailsApplication.config.iAssicur.azioni.query.path
        def url = iAssicurClient.url
        iAssicurClient.url = grailsApplication.config.iAssicur.urlProduzione
        log.debug "iAssicur Webservice: ${url}${path} -> ${sql}"
        logg =new Log(parametri: "iAssicur Webservice: ${url}${path} -> ${sql}", operazione: "query IASSCIUR", pagina: "CHIAMATA IASSICUR")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def response = null
        try {
            response = iAssicurClient.get(path: path, query: [SQL: sql])
        } finally {
            iAssicurClient.url = url
        }
        log.debug "Response status: ${response.statusCode} (${response.statusMessage})"
        logg =new Log(parametri: "Response status: ${response.statusCode} (${response.statusMessage})", operazione: "query IASSCIUR", pagina: "CHIAMATA IASSICUR")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        return response
    }

//select DRE:CODICERECORD,DRE:COMPAGNIA,DRE:RAMO,DRE:INIZIO,DRE:SCADENZA,DRE:CLIENTE,DRE:CODICEFISCALE,DRE:INDIRIZZO,DRE:CAP,DRE:CITTA',DRE:PROVINCIA,DRE:RISCHIO,DRE:RISCHIO2,DRE:NUMERO,DRE:PREMIO,DRE:TASSE,DRE:IPERTESTO,DRE:NOTA,DRE:NOTA2,STATO,DATACOMUNICAZ FROM DSLIP WHERE COMPAGNIA=A55 AND DATACOMUNICAZ inrange 01.07.2016:31.07.2016 AND STATO=3

    def getPolizzeFinLea( def dataGeneraCert) {
        def logg
        def select
        def dataOdierna=new Date().format("dd.MM.yyyy")
        def giornoOdierno=new Date().format("EEE")
        println dataOdierna
        def dataggprima
        if(giornoOdierno.contains("lun")){
            dataggprima= use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -3.day}.format("dd.MM.yyyy")
        }else{
            dataggprima= use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -1.day}.format("dd.MM.yyyy")
        }
        def polizzeScaricate=Documenti.createCriteria().list(){
            eq "tipo", TipoDoc.FINANZIATE
            projections {
                groupProperty "noPolizza"
            }
        }
        def polizze
        if(Environment.current == Environment.PRODUCTION) {
            select = "select DRE:CODICERECORD,DRE:COMPAGNIA,DRE:RAMO,DRE:INIZIO,DRE:SCADENZA,DRE:CLIENTE,DRE:CODICEFISCALE,DRE:INDIRIZZO,DRE:CAP,DRE:CITTA',DRE:PROVINCIA,DRE:RISCHIO,DRE:RISCHIO2,DRE:NUMERO,DRE:PREMIO,DRE:TASSE,DRE:IPERTESTO,DRE:NOTA3,DRE:NOTA2,DRE:DURATAANNI,STATO,DATA FROM DSLIP WHERE COMPAGNIA=A55 AND DATA = ${dataGeneraCert} AND STATO=3  " +
                    "AND DRE:RAMO  IN (U/29,U/30,U/31,U/32,U/33,U/34,U/35,U/36,U/37,U/38,U/39,U/40,U/41,U/42,U/43,U/44,U/45,U/46,U/47,U/48,U/49,U/50,U/51,U/52,U/53,U/54,U/55,U/56," +
                    "U/57,U/58,W/62,W/61,W/63,W/64,W/65,W/66,W/67,W/68,W/69,W/70,W/71,W/72,W/73,W/74,W/75,W/76,W/77,W/79,W/80,W/81,W/82,W/83,W/84,W/85,W/86,W/87,W/88,W/89,W/90,W/91" +
                    ")"
        }else{
            select = "select DRE:CODICERECORD,DRE:COMPAGNIA,DRE:RAMO,DRE:INIZIO,DRE:SCADENZA,DRE:CLIENTE,DRE:CODICEFISCALE,DRE:INDIRIZZO,DRE:CAP,DRE:CITTA',DRE:PROVINCIA,DRE:RISCHIO,"+
                    "DRE:RISCHIO2,DRE:NUMERO,DRE:PREMIO,DRE:TASSE,DRE:IPERTESTO,DRE:NOTA3,DRE:NOTA2,DRE:DURATAANNI,STATO,DATA FROM DSLIP WHERE COMPAGNIA=A55 "+
                    " AND DRE:NUMERO =516061 "
                   // " AND DRE:NUMERO =538880 "
                    /*"AND DATA = ${dataGeneraCert} AND STATO=3  " +
                    "AND DRE:RAMO  IN (U/29,U/30,U/31,U/32,U/33,U/34,U/35,U/36,U/37,U/38,U/39,U/40,U/41,U/42,U/43,U/44,U/45,U/46,U/47,U/48,U/49,U/50,U/51,U/52,U/53,U/54,U/55,U/56," +
                    "U/57,U/58,W/62,W/61,W/63,W/64,W/65,W/66,W/67,W/68,W/69,W/70,W/71,W/72,W/73,W/74,W/75,W/76,W/77,W/79,W/80,W/81,W/82,W/83,W/84,W/85,W/86,W/87,W/88,W/89,W/90,W/91" +
                    ")"*/
        }

        println select
        logg =new Log(parametri: "query chiamata IAssicur per polizze FIN/LEASING ${select} ", operazione: "getPolizzeFinLea", pagina: "GENERE CERTIFICATI POLIZZE FIN/LEASING")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def result = [polizze: [], error: null, iAssicurError: null]
            try {
                def res = query(select)
                if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuna polizza trovata"
                if (res.xml.Query.Record.Campo.size() > 0) {
                    res.xml.Query.Record.each { record ->
                        def polizza = [:]
                        record.Campo.eachWithIndex { campo, indice ->
                            switch (indice) {
                                case 0: polizza.codiceDSLIP = campo.text()
                                    break
                                case 1: polizza.codice = campo.text()
                                    break
                                case 2: def compagnia = campo.text()
                                    def index1 = compagnia.lastIndexOf("(")
                                    def index2 = compagnia.lastIndexOf(")")
                                    compagnia = compagnia[index1 + 1..<index2]
                                    polizza.compagnia = compagnia
                                    break
                                case 3: def ramo = campo.text()
                                    if(ramo.indexOf("(") > 0){
                                        def pos1 = ramo.indexOf("(")
                                        def pos2 = ramo.indexOf(")")
                                        ramo = ramo.substring(pos1+1, pos2)
                                    }
                                    polizza.ramo = ramo
                                    break
                                case 4: polizza.inizio = campo.text()
                                    break
                                case 5: polizza.fine = campo.text()
                                    break
                                case 6: def cliente  = campo.text()
                                    if(cliente.indexOf("(") > 0){
                                        def pos = cliente.indexOf("(")
                                        cliente = cliente.substring(0, pos)
                                    }
                                    polizza.cliente=cliente.trim()
                                    break
                                case 7: polizza.cFiscale = campo.text()
                                    break
                                case 8: polizza.indirizzo = campo.text()
                                    break
                                case 9:def cap=campo.text()
                                    cap=cap.toString().replaceFirst ("^[0]", "O");
                                    polizza.cap = cap
                                    break
                                case 10: polizza.localita = campo.text()
                                    break
                                case 11: polizza.provincia = campo.text()
                                    break
                                case 12: polizza.targa = campo.text()
                                    break
                                case 13: polizza.telaio = campo.text()
                                    break
                                case 14: polizza.noPolizza = campo.text()
                                    break
                                case 15: def premio = campo.text()
                                    if(premio.indexOf("\u20AC") > 0){
                                        def pos = premio.indexOf("\u20AC")
                                        premio = premio.substring(0, pos)
                                    }
                                    polizza.premio = premio
                                    break
                                case 16: def imposte= campo.text()
                                    if(imposte.indexOf("\u20AC") > 0){
                                        def pos = imposte.indexOf("\u20AC")
                                        imposte = imposte.substring(0, pos)
                                    }
                                    polizza.imposte = imposte
                                    break
                                case 17: polizza.ipertesto = campo.text()
                                        def ipertesto = campo.text()
                                        def inizio, fine
                                        def index = ipertesto.indexOf("VALORE NUOVO:")
                                        if(index > 0){
                                            ipertesto = ipertesto.substring(index)
                                            inizio = ipertesto.indexOf(":")
                                            fine = ipertesto.indexOf("<br/>")
                                            def valorenuovo = ipertesto.substring(inizio + 1, fine)
                                            polizza.valoreAssicurato = valorenuovo.replaceAll(",",".").toBigDecimal()
                                        } else polizza.valoreAssicurato = null
                                        ipertesto = campo.text()
                                        index = ipertesto.indexOf("IMMATRICOLAZIONE:")
                                        if(index > 0) {
                                            ipertesto = ipertesto.substring(index)
                                            inizio = ipertesto.indexOf(":")
                                            fine = ipertesto.indexOf("<br/>")
                                            def immatricolazione = ipertesto.substring(inizio + 1, fine)
                                            polizza.immatricolazione = immatricolazione
                                        } else polizza.immatricolazione = null
                                        ipertesto = campo.text()
                                        index = ipertesto.indexOf("VEICOLO NUOVO:")
                                        if(index > 0) {
                                            ipertesto = ipertesto.substring(index)
                                            inizio = ipertesto.indexOf(":")
                                            fine = ipertesto.indexOf("<br/>")
                                            def stato = ipertesto.substring(inizio + 1, fine)
                                            polizza.stato = stato.contains("SI") ? "NUOVO" : "USATO"
                                        } else polizza.stato = null
                                        ipertesto = campo.text()
                                        index = ipertesto.indexOf("TIPO VEICOLO:")
                                        if(index > 0) {
                                            ipertesto = ipertesto.substring(index)
                                            inizio = ipertesto.indexOf(":")
                                            fine = ipertesto.indexOf("<br/>")
                                            def tipoVeicolo = ipertesto.substring(inizio + 1, fine)
                                            tipoVeicolo = tipoVeicolo.trim() ?: null
                                            switch (tipoVeicolo){
                                                case "A": polizza.tipoVeicolo = "AUTOVEICOLO"
                                                    break
                                                case "G": polizza.tipoVeicolo = "VEICOLO_ELETTRICO"
                                                    break
                                                case "C": polizza.tipoVeicolo = -"AUTOCARRO"
                                                    break
                                            }
                                        }
                                    break
                                case 18: polizza.nota = campo.text()
                                    break
                                case 19: polizza.marca = campo.text()
                                    break
                                case 20: def durata = campo.text()
                                    if(durata){
                                        if(durata=="1,00"){polizza.durata = "12"}
                                        else if(durata=="1,50"){polizza.durata = "18"}
                                        else if(durata=="2,00"){polizza.durata = "24"}
                                        else if(durata=="2,50"){polizza.durata = "30"}
                                        else if(durata=="3,00"){polizza.durata = "36"}
                                        else if(durata=="3,50"){polizza.durata = "42"}
                                        else if(durata=="4,00"){polizza.durata = "48"}
                                        else if(durata=="4,50"){polizza.durata = "54"}
                                        else if(durata=="5,00"){polizza.durata = "60"}
                                        else if(durata=="5,50"){polizza.durata = "66"}
                                        else if(durata=="6,00"){polizza.durata = "72"}

                                        //polizza.durata = durata.toInteger()
                                    } else polizza.durata = diffDateInMonths(polizza.fine, polizza.inizio).abs() //correzione campo durata se non presente in iAssicur
                                    break
                                case 21: def stato = campo.text()
                                    def index = stato.indexOf("(")
                                    polizza.statoPolizza = stato.substring(0, index).trim()
                                    break
                                case 22: polizza.dataContae = campo.text()
                                    break
                            }
                        }
                        result.polizze << polizza
                    }
                }
            } catch(RESTClientException e) {
                if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
                else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
                else result.iAssicurError = e.response.statusMessage
                return result
            }
        return result
    }
    def getPolizzeCASH() {
        def logg
        def select
        def dataOdierna=new Date().format("dd.MM.yyyy")
        def giornoOdierno=new Date().format("EEE")
        def dataggprima
        def inrangex
        def anno=new Date().format("yyyy").toInteger()
        def mese=new Date().format("MM").toInteger()
        mese=mese.toString().padLeft(2,"0")
        def datainizio = "01.${(mese)}.${anno.toString()}"
        mese=Integer.parseInt(mese)
        logg =new Log(parametri: "questo è il mese dopo parse int ${mese} e questa è la data inizio ${datainizio}", operazione: "getPolizzeCASH", pagina: "JOB POLIZZE CASH")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def giorno=new Date().format("dd").toInteger()
        def calendar = Calendar.getInstance()
        calendar.set(anno, mese-1, giorno)
        def finemese=checkIt(calendar)
        if (finemese){
            inrangex="inrange ${datainizio}:${dataOdierna}"
        }
        else if(giornoOdierno.contains("lun")){
            dataggprima= use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -3.day}.format("dd.MM.yyyy")
            inrangex="inrange ${dataggprima}:${dataOdierna}"
        }else{
            dataggprima= use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -1.day}.format("dd.MM.yyyy")
            inrangex="= ${dataggprima}"
        }
        if(Environment.current == Environment.PRODUCTION) {
            select = "select DRE:CODICERECORD,DRE:COMPAGNIA,DRE:RAMO,DRE:INIZIO,DRE:SCADENZA,DRE:CLIENTE,DRE:CODICEFISCALE,DRE:INDIRIZZO,DRE:CAP,DRE:CITTA',DRE:PROVINCIA,DRE:RISCHIO,DRE:RISCHIO2," +
                    "DRE:NUMERO,DRE:PREMIO,DRE:TASSE,DRE:IPERTESTO,DRE:NOTA3,DRE:NOTA2,DRE:DURATAANNI,STATO,DATA FROM DSLIP WHERE COMPAGNIA=A55 AND DATACOMUNICAZ  ${inrangex} AND STATO=3  " +
                    "AND DRE:RAMO IN ('U/12','U/13','U/14','U/15','U/16','U/17','U/18','U/19','U/20','U/21','U/22','U/23','U/24','U/25','U/26','W/47','W/46','W/48','W/49','W/50','W/51','W/52','W/53'," +
                    "'W/54','W/55','W/56','W/57','W/58','W/59','W/60')"


        }else{
            select = "select DRE:CODICERECORD,DRE:COMPAGNIA,DRE:RAMO,DRE:INIZIO,DRE:SCADENZA,DRE:CLIENTE,DRE:CODICEFISCALE,DRE:INDIRIZZO,DRE:CAP,DRE:CITTA',DRE:PROVINCIA,DRE:RISCHIO,DRE:RISCHIO2,"+
                    "DRE:NUMERO,DRE:PREMIO,DRE:TASSE,DRE:IPERTESTO,DRE:NOTA3,DRE:NOTA2,DRE:DURATAANNI,STATO,DATA FROM DSLIP WHERE COMPAGNIA=A55  " +
                    " AND DRE:NUMERO =DLI0025729  "
                    /*"AND DATACOMUNICAZ = 27.11.2017 AND STATO=3 AND DRE:RAMO IN ('U/12','U/13','U/14','U/15','U/16','U/17','U/18','U/19','U/20','U/21','U/22','U/23','U/24','U/25','U/26','W/47','W/46','W/48','W/49'," +
                    "'W/50','W/51','W/52','W/53','W/54','W/55','W/56','W/57','W/58','W/59','W/60')"*/

        }
        println select
        println inrangex
        println finemese
        println giornoOdierno
        logg =new Log(parametri: "query chiamata IAssicur per polizze CASH ${select} ", operazione: "getPolizzeCASH", pagina: "JOB POLIZZE CASH")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def result = [polizze: [], error: null, iAssicurError: null]
            try {
                def res = query(select)
                if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuna polizza trovata"
                if (res.xml.Query.Record.Campo.size() > 0) {
                    res.xml.Query.Record.each { record ->
                        def polizza = [:]
                        record.Campo.eachWithIndex { campo, indice ->
                            switch (indice) {
                                case 0: polizza.codiceDSLIP = campo.text()
                                    break
                                case 1: polizza.codice = campo.text()
                                    break
                                case 2: def compagnia = campo.text()
                                    def index1 = compagnia.lastIndexOf("(")
                                    def index2 = compagnia.lastIndexOf(")")
                                    compagnia = compagnia[index1 + 1..<index2]
                                    polizza.compagnia = compagnia
                                    break
                                case 3: def ramo = campo.text()
                                    if(ramo.indexOf("(") > 0){
                                        def pos1 = ramo.indexOf("(")
                                        def pos2 = ramo.indexOf(")")
                                        ramo = ramo.substring(pos1+1, pos2)
                                    }
                                    polizza.ramo = ramo
                                    break
                                case 4: polizza.inizio = campo.text()
                                    break
                                case 5: polizza.fine = campo.text()
                                    break
                                case 6: def cliente  = campo.text()
                                    if(cliente.indexOf("(") > 0){
                                        def pos = cliente.indexOf("(")
                                        cliente = cliente.substring(0, pos)
                                    }
                                    polizza.cliente=cliente.trim()
                                    break
                                case 7: polizza.cFiscale = campo.text()
                                    break
                                case 8: polizza.indirizzo = campo.text()
                                    break
                                case 9:def cap=campo.text()
                                    cap=cap.toString().replaceFirst ("^[0]", "O");
                                    polizza.cap = cap
                                    break
                                case 10: polizza.localita = campo.text()
                                    break
                                case 11: polizza.provincia = campo.text()
                                    break
                                case 12: polizza.targa = campo.text()
                                    break
                                case 13: polizza.telaio = campo.text()
                                    break
                                case 14: polizza.noPolizza = campo.text()
                                    break
                                case 15: def premio = campo.text()
                                    if(premio.indexOf("\u20AC") > 0){
                                        def pos = premio.indexOf("\u20AC")
                                        premio = premio.substring(0, pos)
                                    }
                                    polizza.premio = premio
                                    break
                                case 16: def imposte= campo.text()
                                    if(imposte.indexOf("\u20AC") > 0){
                                        def pos = imposte.indexOf("\u20AC")
                                        imposte = imposte.substring(0, pos)
                                    }
                                    polizza.imposte = imposte
                                    break
                                case 17: polizza.ipertesto = campo.text()
                                        def ipertesto = campo.text()
                                        def inizio, fine
                                        def index = ipertesto.indexOf("VALORE NUOVO:")
                                        if(index > 0){
                                            ipertesto = ipertesto.substring(index)
                                            inizio = ipertesto.indexOf(":")
                                            fine = ipertesto.indexOf("<br/>")
                                            def valorenuovo = ipertesto.substring(inizio + 1, fine)
                                            polizza.valoreAssicurato = valorenuovo.replaceAll(",",".").toBigDecimal()
                                        } else polizza.valoreAssicurato = null
                                        ipertesto = campo.text()
                                        index = ipertesto.indexOf("IMMATRICOLAZIONE:")
                                        if(index > 0) {
                                            ipertesto = ipertesto.substring(index)
                                            inizio = ipertesto.indexOf(":")
                                            fine = ipertesto.indexOf("<br/>")
                                            def immatricolazione = ipertesto.substring(inizio + 1, fine)
                                            polizza.immatricolazione = immatricolazione
                                        } else polizza.immatricolazione = null
                                        ipertesto = campo.text()
                                        index = ipertesto.indexOf("VEICOLO NUOVO:")
                                        if(index > 0) {
                                            ipertesto = ipertesto.substring(index)
                                            inizio = ipertesto.indexOf(":")
                                            fine = ipertesto.indexOf("<br/>")
                                            def stato = ipertesto.substring(inizio + 1, fine)
                                            polizza.stato = stato.contains("SI") ? "NUOVO" : "USATO"
                                        } else polizza.stato = null
                                        ipertesto = campo.text()
                                        index = ipertesto.indexOf("TIPO VEICOLO:")
                                        if(index > 0) {
                                            ipertesto = ipertesto.substring(index)
                                            inizio = ipertesto.indexOf(":")
                                            fine = ipertesto.indexOf("<br/>")
                                            def tipoVeicolo = ipertesto.substring(inizio + 1, fine)
                                            tipoVeicolo = tipoVeicolo.trim() ?: null
                                            switch (tipoVeicolo){
                                                case "A": polizza.tipoVeicolo = "AUTOVEICOLO"
                                                    break
                                                case "G": polizza.tipoVeicolo = "VEICOLO_ELETTRICO"
                                                    break
                                                case "C": polizza.tipoVeicolo = -"AUTOCARRO"
                                                    break
                                            }
                                        }
                                    break
                                case 18: polizza.nota = campo.text()
                                    break
                                case 19: polizza.marca = campo.text()
                                    break
                                case 20: def durata = campo.text()
                                    if(durata){
                                        if(durata=="1,00"){polizza.durata = "12"}
                                        else if(durata=="1,50"){polizza.durata = "18"}
                                        else if(durata=="2,00"){polizza.durata = "24"}
                                        else if(durata=="2,50"){polizza.durata = "30"}
                                        else if(durata=="3,00"){polizza.durata = "36"}
                                        else if(durata=="3,50"){polizza.durata = "42"}
                                        else if(durata=="4,00"){polizza.durata = "48"}
                                        else if(durata=="4,50"){polizza.durata = "54"}
                                        else if(durata=="5,00"){polizza.durata = "60"}
                                        else if(durata=="5,50"){polizza.durata = "66"}
                                        else if(durata=="6,00"){polizza.durata = "72"}

                                        //polizza.durata = durata.toInteger()
                                    } else{
                                        def inizioPolizza=Date.parse(polizza.inizio)
                                        def finePolizza=Date.parse(polizza.fine)
                                        polizza.durata = diffDateInMonths(inizioPolizza, finePolizza).abs()
                                    } //correzione campo durata se non presente in iAssicur
                                    break
                                case 21: def stato = campo.text()
                                    def index = stato.indexOf("(")
                                    polizza.statoPolizza = stato.substring(0, index).trim()
                                    break
                                case 22: polizza.dataContae = campo.text()
                                    break
                            }
                        }
                        result.polizze << polizza
                    }
                }
            } catch(RESTClientException e) {
                if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
                else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
                else result.iAssicurError = e.response.statusMessage
                return result
            }
        return result
    }
    def getPolizzePAIPAG(def dataGeneraCert) {
        def logg
        def dataOdierna=Date.parse("dd.MM.yyyy", dataGeneraCert)
        def c = Calendar.getInstance()
        def cal = Calendar.getInstance()
        cal.setTime(dataOdierna)
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMinimum(Calendar.DAY_OF_MONTH))
        def firstDayOfTheMonth = cal.getTime().format("dd.MM.yyyy")
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DAY_OF_MONTH))
        def lastDayOfTheMonth = cal.getTime().format("dd.MM.yyyy")
        def select
        if(Environment.current == Environment.PRODUCTION) {
            select = "select DRE:CODICERECORD,DRE:COMPAGNIA,DRE:RAMO,DRE:INIZIO,DRE:SCADENZA,DRE:CLIENTE,DRE:CODICEFISCALE,DRE:INDIRIZZO,DRE:CAP,DRE:CITTA',DRE:PROVINCIA," +
                    "DRE:RISCHIO,DRE:RISCHIO2,DRE:NUMERO,DRE:PREMIO,DRE:TASSE,DRE:IPERTESTO,DRE:NOTA3,DRE:NOTA2,DRE:DURATAANNI,STATO,DATA FROM DSLIP WHERE COMPAGNIA=A56 " +
                    "AND DATA  inrange  ${firstDayOfTheMonth}:${lastDayOfTheMonth} AND STATO=3  AND DRE:RAMO  IN (K/51,K/52)"
            //"AND DRE:NUMERO =528863 AND STATO=3  AND DRE:RAMO  IN (K/51,K/52)"
        }else{
            select = "select DRE:CODICERECORD,DRE:COMPAGNIA,DRE:RAMO,DRE:INIZIO,DRE:SCADENZA,DRE:CLIENTE,DRE:CODICEFISCALE,DRE:INDIRIZZO,DRE:CAP,DRE:CITTA',DRE:PROVINCIA," +
                    "DRE:RISCHIO,DRE:RISCHIO2,DRE:NUMERO,DRE:PREMIO,DRE:TASSE,DRE:IPERTESTO,DRE:NOTA3,DRE:NOTA2,DRE:DURATAANNI,STATO,DATA FROM DSLIP WHERE COMPAGNIA=A56 " +
                    //"AND DATA  inrange  ${firstDayOfTheMonth}:${lastDayOfTheMonth} AND STATO=3  AND DRE:RAMO  IN (K/51,K/52)"
                    "AND DRE:NUMERO =516061 AND STATO=3  AND DRE:RAMO  IN (K/51,K/52)"
        }
        println "select--> $select"
        logg =new Log(parametri: "query chiamata IAssicur per polizze PAI PAG ${select} ", operazione: "getPolizzePAIPAG", pagina: "JOB POLIZZE CERT PAI")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def result = [polizze: [], error: null, iAssicurError: null]
            try {
                def res = query(select)
                if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuna polizza trovata"
                if (res.xml.Query.Record.Campo.size() > 0) {
                    res.xml.Query.Record.each { record ->
                        def polizza = [:]
                        record.Campo.eachWithIndex { campo, indice ->
                            switch (indice) {
                                case 0: polizza.codiceDSLIP = campo.text()
                                    break
                                case 1: polizza.codice = campo.text()
                                    break
                                case 2: def compagnia = campo.text()
                                    def index1 = compagnia.lastIndexOf("(")
                                    def index2 = compagnia.lastIndexOf(")")
                                    compagnia = compagnia[index1 + 1..<index2]
                                    polizza.compagnia = compagnia
                                    break
                                case 3: def ramo = campo.text()
                                    if(ramo.indexOf("(") > 0){
                                        def pos1 = ramo.indexOf("(")
                                        def pos2 = ramo.indexOf(")")
                                        ramo = ramo.substring(pos1+1, pos2)
                                    }
                                    polizza.ramo = ramo
                                    break
                                case 4: polizza.inizio = campo.text()
                                    break
                                case 5: polizza.fine = campo.text()
                                    break
                                case 6: def cliente  = campo.text()
                                    if(cliente.indexOf("(") > 0){
                                        def pos = cliente.indexOf("(")
                                        cliente = cliente.substring(0, pos)
                                    }
                                    polizza.cliente=cliente.trim()
                                    break
                                case 7: polizza.cFiscale = campo.text()
                                    break
                                case 8: polizza.indirizzo = campo.text()
                                    break
                                case 9:def cap=campo.text()
                                    cap=cap.toString().replaceFirst ("^[0]", "O");
                                    polizza.cap = cap
                                    break
                                case 10: polizza.localita = campo.text()
                                    break
                                case 11: polizza.provincia = campo.text()
                                    break
                                case 12: polizza.targa = campo.text()
                                    break
                                case 13: polizza.telaio = campo.text()
                                    break
                                case 14: polizza.noPolizza = campo.text()
                                    break
                                case 15: def premio = campo.text()
                                    if(premio.indexOf("\u20AC") > 0){
                                        def pos = premio.indexOf("\u20AC")
                                        premio = premio.substring(0, pos)
                                    }
                                    polizza.premio = premio
                                    break
                                case 16: def imposte= campo.text()
                                    if(imposte.indexOf("\u20AC") > 0){
                                        def pos = imposte.indexOf("\u20AC")
                                        imposte = imposte.substring(0, pos)
                                    }
                                    polizza.imposte = imposte
                                    break
                                case 17: polizza.ipertesto = campo.text()
                                        def ipertesto = campo.text()
                                        def inizio, fine
                                        def index = ipertesto.indexOf("VALORE NUOVO:")
                                        if(index > 0){
                                            ipertesto = ipertesto.substring(index)
                                            inizio = ipertesto.indexOf(":")
                                            fine = ipertesto.indexOf("<br/>")
                                            def valorenuovo = ipertesto.substring(inizio + 1, fine)
                                            polizza.valoreAssicurato = valorenuovo.replaceAll(",",".").toBigDecimal()
                                        } else polizza.valoreAssicurato = null
                                        ipertesto = campo.text()
                                        index = ipertesto.indexOf("IMMATRICOLAZIONE:")
                                        if(index > 0) {
                                            ipertesto = ipertesto.substring(index)
                                            inizio = ipertesto.indexOf(":")
                                            fine = ipertesto.indexOf("<br/>")
                                            def immatricolazione = ipertesto.substring(inizio + 1, fine)
                                            polizza.immatricolazione = immatricolazione
                                        } else polizza.immatricolazione = null
                                        ipertesto = campo.text()
                                        index = ipertesto.indexOf("VEICOLO NUOVO:")
                                        if(index > 0) {
                                            ipertesto = ipertesto.substring(index)
                                            inizio = ipertesto.indexOf(":")
                                            fine = ipertesto.indexOf("<br/>")
                                            def stato = ipertesto.substring(inizio + 1, fine)
                                            polizza.stato = stato.contains("SI") ? "NUOVO" : "USATO"
                                        } else polizza.stato = null
                                        ipertesto = campo.text()
                                        index = ipertesto.indexOf("TIPO VEICOLO:")
                                        if(index > 0) {
                                            ipertesto = ipertesto.substring(index)
                                            inizio = ipertesto.indexOf(":")
                                            fine = ipertesto.indexOf("<br/>")
                                            def tipoVeicolo = ipertesto.substring(inizio + 1, fine)
                                            tipoVeicolo = tipoVeicolo.trim() ?: null
                                            switch (tipoVeicolo){
                                                case "A": polizza.tipoVeicolo = "AUTOVEICOLO"
                                                    break
                                                case "G": polizza.tipoVeicolo = "VEICOLO_ELETTRICO"
                                                    break
                                                case "C": polizza.tipoVeicolo = -"AUTOCARRO"
                                                    break
                                            }
                                        }
                                    break
                                case 18: polizza.nota = campo.text()
                                    break
                                case 19: polizza.marca = campo.text()
                                    break
                                case 20: def durata = campo.text()
                                    if(durata){
                                        if(durata=="1,00"){polizza.durata = "12"}
                                        else if(durata=="1,50"){polizza.durata = "18"}
                                        else if(durata=="2,00"){polizza.durata = "24"}
                                        else if(durata=="2,50"){polizza.durata = "30"}
                                        else if(durata=="3,00"){polizza.durata = "36"}
                                        else if(durata=="3,50"){polizza.durata = "42"}
                                        else if(durata=="4,00"){polizza.durata = "48"}
                                        else if(durata=="4,50"){polizza.durata = "54"}
                                        else if(durata=="5,00"){polizza.durata = "60"}
                                        else if(durata=="5,50"){polizza.durata = "66"}
                                        else if(durata=="6,00"){polizza.durata = "72"}

                                        //polizza.durata = durata.toInteger()
                                    } else{
                                        def inizioPolizza=Date.parse(polizza.inizio)
                                        def finePolizza=Date.parse(polizza.fine)
                                        polizza.durata = diffDateInMonths(inizioPolizza, finePolizza).abs()
                                    } //correzione campo durata se non presente in iAssicur
                                    break
                                case 21: def stato = campo.text()
                                    def index = stato.indexOf("(")
                                    polizza.statoPolizza = stato.substring(0, index).trim()
                                    break
                                case 22: polizza.dataContae = campo.text()
                                    break
                            }
                        }
                        result.polizze << polizza
                    }
                }
            } catch(RESTClientException e) {
                if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
                else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
                else result.iAssicurError = e.response.statusMessage
                return result
            }
        return result
    }
    def getPolizzeRinnovi() {
        def logg
        def dataOdierna=new Date().format("dd.MM.yyyy")
        //def dataggprima= use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -1.day}.format("dd.MM.yyyy")
        def giornoOdierno=new Date().format("EEE")
        def dataggprima
        def inrangex
        def anno=new Date().format("yyyy").toInteger()
        def mese=new Date().format("MM").toInteger()
        mese=mese.toString().padLeft(2,"0")
        def datainizio = "01.${(mese).toString()}.${anno.toString()}"
        mese=Integer.parseInt(mese)
        logg =new Log(parametri: "questo è il mese dopo parse int ${mese} e questa è la data inizio ${datainizio}", operazione: "getPolizzeRINNOVI", pagina: "JOB POLIZZE RINNOVI")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def giorno=new Date().format("dd").toInteger()
        def calendar = Calendar.getInstance()
        calendar.set(anno, mese-1, giorno)
        def finemese=checkIt(calendar)
        if (finemese){
            inrangex="inrange ${datainizio}:${dataOdierna}"
        }
        else if(giornoOdierno.contains("lun")){
            dataggprima= use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -3.day}.format("dd.MM.yyyy")
            inrangex="inrange ${dataggprima}:${dataOdierna}"
        }else{
            dataggprima= use(TimeCategory) {Date.parse("dd.MM.yyyy", dataOdierna) -1.day}.format("dd.MM.yyyy")
            inrangex="= ${dataggprima}"
        }
        def select
        if(Environment.current == Environment.PRODUCTION) {
            select = "select DRE:CODICERECORD,DRE:COMPAGNIA,DRE:RAMO,DRE:INIZIO,DRE:SCADENZA,DRE:CLIENTE,DRE:CODICEFISCALE,DRE:INDIRIZZO,DRE:CAP,DRE:CITTA',DRE:PROVINCIA,DRE:RISCHIO,DRE:RISCHIO2," +
                    "DRE:NUMERO,DRE:PREMIO,DRE:TASSE,DRE:IPERTESTO,DRE:NOTA3,DRE:NOTA2,DRE:DURATAANNI,STATO,DATA FROM DSLIP WHERE COMPAGNIA=A55 AND DATACOMUNICAZ  ${inrangex} AND STATO=3  " +
                    "AND DRE:RAMO IN ('U/59','U/60','U/61','U/62','U/63','U/64','U/65','U/66','U/67','U/68'," +
                    "'W/92','W/93','W/94','W/95','W/96','W/97','W/98','W/99','D/01','D/02','D/03','D/04','D/05'," +
                    "'D/06','D/07','D/08','D/09','D/10','D/11','D/12','D/21','D/22','D/23','D/24','D/25','D/26','D/27','D/28','D/29','D/30')"
           /* select = "select DRE:CODICERECORD,DRE:COMPAGNIA,DRE:RAMO,DRE:INIZIO,DRE:SCADENZA,DRE:CLIENTE,DRE:CODICEFISCALE,DRE:INDIRIZZO,DRE:CAP,DRE:CITTA',DRE:PROVINCIA,DRE:RISCHIO,DRE:RISCHIO2," +
                    "DRE:NUMERO,DRE:PREMIO,DRE:TASSE,DRE:IPERTESTO,DRE:NOTA3,DRE:NOTA2,DRE:DURATAANNI,STATO,DATA FROM DSLIP WHERE COMPAGNIA=A55 AND DATACOMUNICAZ inrange 04.07.2017:19.07.2017 AND STATO=3  " +
                    "AND DRE:RAMO IN ('U/59','U/60','U/61','U/62','U/63','U/64','U/65','U/66','U/67','U/68','W/92','W/94','W/95','W/96','W/97','W/98','W/99','D/01','D/02','D/03','D/04','D/05'," +
                    "'D/06','D/07','D/08','D/09','D/10','D/11','D/12','D/21','D/22','D/23','D/24','D/25','D/26','D/27','D/28','D/29','D/30')"*/

        }else{

            /*select = "select DRE:CODICERECORD,DRE:COMPAGNIA,DRE:RAMO,DRE:INIZIO,DRE:SCADENZA,DRE:CLIENTE,DRE:CODICEFISCALE,DRE:INDIRIZZO,DRE:CAP,DRE:CITTA',DRE:PROVINCIA,DRE:RISCHIO,DRE:RISCHIO2," +
                    "DRE:NUMERO,DRE:PREMIO,DRE:TASSE,DRE:IPERTESTO,DRE:NOTA3,DRE:NOTA2,DRE:DURATAANNI,STATO,DATA FROM DSLIP WHERE COMPAGNIA=A55 AND DATACOMUNICAZ  inrange 17.04.2017:03.05.2017 " +
                    "AND STATO=3 AND DRE:RAMO IN ('U/59','U/60','U/61','U/62','U/63','U/64','U/65','U/66','U/67','U/68','W/92','W/94','W/95','W/96','W/97','W/98','W/99','D/01','D/02'," +
                    "'D/03','D/04','D/05','D/06','D/07','D/08','D/09','D/10','D/11','D/12','D/21','D/22','D/23','D/24','D/25','D/26','D/27','D/28','D/29','D/30')"*/

            select = "select DRE:CODICERECORD,DRE:COMPAGNIA,DRE:RAMO,DRE:INIZIO,DRE:SCADENZA,DRE:CLIENTE,DRE:CODICEFISCALE,DRE:INDIRIZZO,DRE:CAP,DRE:CITTA',DRE:PROVINCIA,DRE:RISCHIO,DRE:RISCHIO2," +
                    "DRE:NUMERO,DRE:PREMIO,DRE:TASSE,DRE:IPERTESTO,DRE:NOTA3,DRE:NOTA2,DRE:DURATAANNI,STATO,DATA, DATACOMUNICAZ FROM DSLIP WHERE COMPAGNIA=A55 " +
                    " AND DRE:NUMERO in ( 'DLR0002199' )"
                    //" AND DRE:CODICERECORD in ( 'ACDUDX/002' )"
                    /*"AND DATACOMUNICAZ = " +
                    "24.07.2017 " +
                    "AND STATO=3 AND DRE:RAMO IN " +
                    //"('U/59','U/60','U/61','U/62','U/63','U/64','U/65','U/66','U/67','U/68'," +
                    "('W/92','W/94','W/95','W/96','W/97','W/98','W/99','D/01','D/02','D/03'," +
                    "'D/04','D/05','D/06','D/07','D/08','D/09','D/10','D/11','D/12','D/21','D/22','D/23','D/24','D/25','D/26','D/27','D/28','D/29','D/30')"
                    "('D/21','D/22','D/23','D/24','D/25','D/26','D/27','D/28','D/29','D/30')"*/


        }
        println inrangex
        println finemese
        println giornoOdierno
        println select
        logg =new Log(parametri: "query chiamata IAssicur per RINNOVI ${select} ", operazione: "getPolizzeRinnovi", pagina: "JOB RINNOVI")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def result = [polizze: [], error: null, iAssicurError: null]
        try {
            def res = query(select)
            if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuna polizza trovata"
            if (res.xml.Query.Record.Campo.size() > 0) {
                res.xml.Query.Record.each { record ->
                    def polizza = [:]
                    record.Campo.eachWithIndex { campo, indice ->
                        switch (indice) {
                            case 0: polizza.codiceDSLIP = campo.text()
                                break
                            case 1: polizza.codice = campo.text()
                                break
                            case 2: def compagnia = campo.text()
                                def index1 = compagnia.lastIndexOf("(")
                                def index2 = compagnia.lastIndexOf(")")
                                compagnia = compagnia[index1 + 1..<index2]
                                polizza.compagnia = compagnia
                                break
                            case 3: def ramo = campo.text()
                                if(ramo.indexOf("(") > 0){
                                    def pos1 = ramo.indexOf("(")
                                    def pos2 = ramo.indexOf(")")
                                    ramo = ramo.substring(pos1+1, pos2)
                                }
                                polizza.ramo = ramo
                                break
                            case 4: polizza.inizio = campo.text()
                                break
                            case 5: polizza.fine = campo.text()
                                break
                            case 6: def cliente  = campo.text()
                                if(cliente.indexOf("(") > 0){
                                    def pos = cliente.indexOf("(")
                                    cliente = cliente.substring(0, pos)
                                }
                                polizza.cliente=cliente.trim()
                                break
                            case 7: polizza.cFiscale = campo.text()
                                break
                            case 8: polizza.indirizzo = campo.text()
                                break
                            case 9: def cap=campo.text()
                                cap=cap.toString().replaceFirst ("^[0]", "O");
                                polizza.cap = cap
                                break
                            case 10: polizza.localita = campo.text()
                                break
                            case 11: polizza.provincia = campo.text()
                                break
                            case 12: polizza.targa = campo.text()
                                break
                            case 13: polizza.telaio = campo.text()
                                break
                            case 14: polizza.noPolizza = campo.text()
                                break
                            case 15: def premio = campo.text()
                                if(premio.indexOf("\u20AC") > 0){
                                    def pos = premio.indexOf("\u20AC")
                                    premio = premio.substring(0, pos)
                                }
                                polizza.premio = premio
                                break
                            case 16: def imposte= campo.text()
                                if(imposte.indexOf("\u20AC") > 0){
                                    def pos = imposte.indexOf("\u20AC")
                                    imposte = imposte.substring(0, pos)
                                }
                                polizza.imposte = imposte
                                break
                            case 17: polizza.ipertesto = campo.text()
                                    def ipertesto = campo.text()
                                    def inizio, fine
                                    def index = ipertesto.indexOf("VALORE NUOVO:")
                                    if(index > 0){
                                        ipertesto = ipertesto.substring(index)
                                        inizio = ipertesto.indexOf(":")
                                        fine = ipertesto.indexOf("<br/>")
                                        def valorenuovo = ipertesto.substring(inizio + 1, fine)
                                        polizza.valoreAssicurato = valorenuovo.replaceAll(",",".").toBigDecimal()
                                    } else polizza.valoreAssicurato = null
                                    ipertesto = campo.text()
                                    index = ipertesto.indexOf("IMMATRICOLAZIONE:")
                                    if(index > 0) {
                                        ipertesto = ipertesto.substring(index)
                                        inizio = ipertesto.indexOf(":")
                                        fine = ipertesto.indexOf("<br/>")
                                        def immatricolazione = ipertesto.substring(inizio + 1, fine)
                                        polizza.immatricolazione = immatricolazione
                                    } else polizza.immatricolazione = null
                                    ipertesto = campo.text()
                                    index = ipertesto.indexOf("VEICOLO NUOVO:")
                                    if(index > 0) {
                                        ipertesto = ipertesto.substring(index)
                                        inizio = ipertesto.indexOf(":")
                                        fine = ipertesto.indexOf("<br/>")
                                        def stato = ipertesto.substring(inizio + 1, fine)
                                        polizza.stato = stato.contains("SI") ? "NUOVO" : "USATO"
                                    } else polizza.stato = null
                                    ipertesto = campo.text()
                                    index = ipertesto.indexOf("TIPO VEICOLO:")
                                    if(index > 0) {
                                        ipertesto = ipertesto.substring(index)
                                        inizio = ipertesto.indexOf(":")
                                        fine = ipertesto.indexOf("<br/>")
                                        def tipoVeicolo = ipertesto.substring(inizio + 1, fine)
                                        tipoVeicolo = tipoVeicolo.trim() ?: null
                                        switch (tipoVeicolo){
                                            case "A": polizza.tipoVeicolo = "AUTOVEICOLO"
                                                break
                                            case "G": polizza.tipoVeicolo = "VEICOLO_ELETTRICO"
                                                break
                                            case "C": polizza.tipoVeicolo = -"AUTOCARRO"
                                                break
                                        }
                                    }
                                break
                            case 18: polizza.nota = campo.text()
                                break
                            case 19: polizza.marca = campo.text()
                                break
                            case 20: def durata = campo.text()
                                if(durata){
                                    if(durata=="1,00"){polizza.durata = "12"}
                                    else if(durata=="1,50"){polizza.durata = "18"}
                                    else if(durata=="2,00"){polizza.durata = "24"}
                                    else if(durata=="2,50"){polizza.durata = "30"}
                                    else if(durata=="3,00"){polizza.durata = "36"}
                                    else if(durata=="3,50"){polizza.durata = "42"}
                                    else if(durata=="4,00"){polizza.durata = "48"}
                                    else if(durata=="4,50"){polizza.durata = "54"}
                                    else if(durata=="5,00"){polizza.durata = "60"}
                                    else if(durata=="5,50"){polizza.durata = "66"}
                                    else if(durata=="6,00"){polizza.durata = "72"}

                                    //polizza.durata = durata.toInteger()
                                } else polizza.durata = diffDateInMonths(polizza.fine, polizza.inizio).abs() //correzione campo durata se non presente in iAssicur
                                break
                            case 21: def stato = campo.text()
                                def index = stato.indexOf("(")
                                polizza.statoPolizza = stato.substring(0, index).trim()
                                break
                            case 22: polizza.dataContae = campo.text()
                                break
                        }
                    }
                    result.polizze << polizza
                }
            }
        } catch(RESTClientException e) {
            if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
            else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
            else result.iAssicurError = e.response.statusMessage
            return result
            }
        return result
    }
    def getPolizzeRinnoviperAnnulla(String numero) {
        def logg
        def select
        select = "select RAMO, CLIENTE,CLIENTE:EMAIL as email,INIZIO,SCADENZA,CAP,CITTA',PROVINCIA,NUMERO,IPERTESTO,DURATAANNI,STATO,CODICEFISCALE,RISCHIO,RISCHIO2,NOTA3,NOTA2,INDIRIZZO, garanzia2 as ANTIFURTO" +
                " FROM DRE WHERE COMPAGNIA=A55 AND RAMO IN ('U/59','U/60','U/61','U/62','U/63','U/64','U/65','U/66','U/67','U/68'," +
                "'W/92','W/93','W/94','W/95','W/96','W/97','W/98','W/99','D/01','D/02','D/03','D/04','D/05','D/06','D/07','D/08','D/09','D/10','D/11','D/12','D/21','D/22','D/23','D/24'," +
                "'D/25','D/26','D/27','D/28','D/29','D/30') AND NUMERO=${numero}"
        logg =new Log(parametri: "query chiamata IAssicur per RINNOVI ${select} ", operazione: "getPolizzeRinnovi", pagina: "ANNULLAMENTO/RECESSO RINNOVI")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def result = [polizze: [], error: null, iAssicurError: null]
        try {
            def res = query(select)
            if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuna polizza trovata"
            if (res.xml.Query.Record.Campo.size() > 0) {
                res.xml.Query.Record.each { record ->
                    def polizza = [:]
                    record.Campo.eachWithIndex { campo, indice ->
                        switch (indice) {
                            case 0: polizza.codiceDRE = campo.text()
                                break
                            case 1: def ramo = campo.text()
                                if(ramo.indexOf("(") > 0){
                                    def pos1 = ramo.indexOf("(")
                                    def pos2 = ramo.indexOf(")")
                                    ramo = ramo.substring(pos1+1, pos2)
                                }
                                if(ramo){
                                    if(ramo.toString().contains('U/59') || ramo.toString().contains('W/92') || ramo.toString().contains('D/03')|| ramo.toString().contains('D/21')){
                                        polizza.codPacchetto = "ORS00"
                                        polizza.copertura="SILVER"
                                    }
                                    else if(ramo.toString().contains('U/60')|| ramo.toString().contains('W/93')|| ramo.toString().contains('D/04')|| ramo.toString().contains('D/22')){
                                        polizza.codPacchetto = "ORG00"
                                        polizza.copertura="GOLD"
                                    }
                                    else if(ramo.toString().contains('U/61') || ramo.toString().contains('W/94') || ramo.toString().contains('D/05')|| ramo.toString().contains('D/23')){
                                        polizza.codPacchetto = "ORP00"
                                        polizza.copertura="PLATINUM"
                                    }
                                    else if(ramo.toString().contains('U/62') || ramo.toString().contains('W/95')|| ramo.toString().contains('D/06')|| ramo.toString().contains('D/24')){
                                        polizza.codPacchetto = "ORPC0"
                                        polizza.copertura="PLATINUM COLLISIONE"
                                    }
                                    else if(ramo.toString().contains('U/63')|| ramo.toString().contains('W/96')|| ramo.toString().contains('D/07')|| ramo.toString().contains('D/25')){
                                        polizza.codPacchetto = "ORPK0"
                                        polizza.copertura="PLATINUM KASKO"
                                    }
                                    else if(ramo.toString().contains('U/64')|| ramo.toString().contains('W/97')|| ramo.toString().contains('D/08')|| ramo.toString().contains('D/26')){
                                        polizza.codPacchetto = "ORS0R"
                                        polizza.copertura="SILVER"
                                    }
                                    else if(ramo.toString().contains('U/65')|| ramo.toString().contains('W/98')|| ramo.toString().contains('D/09')|| ramo.toString().contains('D/27')){
                                        polizza.codPacchetto = "ORG0R"
                                        polizza.copertura="GOLD"
                                    }
                                    else if(ramo.toString().contains('U/66')|| ramo.toString().contains('W/99')|| ramo.toString().contains('D/10')|| ramo.toString().contains('D/28')){
                                        polizza.codPacchetto = "ORP0R"
                                        polizza.copertura="PLATINUM"
                                    }
                                    else if(ramo.toString().contains('U/67')|| ramo.toString().contains('D/11')|| ramo.toString().contains('D/29')){
                                        polizza.codPacchetto = "ORPCR"
                                        polizza.copertura="PLATINUM COLLISIONE"
                                    }
                                    else if(ramo.toString().contains('U/68')|| ramo.toString().contains('D/02')|| ramo.toString().contains('D/12')|| ramo.toString().contains('D/30')){
                                        polizza.codPacchetto = "ORPKR"
                                        polizza.copertura="PLATINUM KASKO"
                                    }

                                }
                                polizza.ramo = ramo
                                break
                            case 2: def cliente  = campo.text()
                                if(cliente.indexOf("(") > 0){
                                    def pos = cliente.indexOf("(")
                                    cliente = cliente.substring(0, pos)
                                }
                                polizza.cliente=cliente.trim()
                                break
                            case 3: def email  = campo.text()
                                polizza.email=email.trim()
                                break
                            case 4: polizza.dataDecorrenza = campo.text()
                                break
                            case 5: polizza.dataScadenza = campo.text()
                                break
                            case 6: def cap=campo.text()
                                cap=cap.toString().replaceFirst ("^[0]", "O");
                                polizza.cap = cap
                                break
                            case 7: polizza.localita = campo.text()
                                break
                            case 8: polizza.provincia = campo.text()
                                break
                            case 9: polizza.noPolizza = campo.text()
                                break
                            case 10: polizza.ipertesto = campo.text()
                                def ipertesto = campo.text()
                                def inizio, fine, index
                                index = ipertesto.indexOf("EICOLO NUOVO:")
                                if(index > 0) {
                                    ipertesto = ipertesto.substring(index)
                                    inizio = ipertesto.indexOf(":")
                                    fine = ipertesto.indexOf("<br/>")
                                    def stato = ipertesto.substring(inizio + 1, fine)
                                    polizza.nuovo = stato.trim().contains("SI") ? "NUOVO" : "USATO"
                                } else polizza.nuovo = "USATO"
                                ipertesto = campo.text()
                                index = ipertesto.indexOf("VALORE NUOVO:")
                                if(index > 0){
                                    ipertesto = ipertesto.substring(index)
                                    inizio = ipertesto.indexOf(":")
                                    fine = ipertesto.indexOf("<br/>")
                                    def valorenuovo = ipertesto.substring(inizio + 1, fine)
                                    polizza.valoreAssicurato = valorenuovo.replaceAll(",",".").toBigDecimal()
                                } else polizza.valoreAssicurato = null
                                break
                            case 11: def durata = campo.text()
                                if(durata){
                                    if(durata=="1,00"){polizza.durata = "12"}
                                    else if(durata=="1,50"){polizza.durata = "18"}
                                    else if(durata=="2,00"){polizza.durata = "24"}
                                    else if(durata=="2,50"){polizza.durata = "30"}
                                    else if(durata=="3,00"){polizza.durata = "36"}
                                    else if(durata=="3,50"){polizza.durata = "42"}
                                    else if(durata=="4,00"){polizza.durata = "48"}
                                    else if(durata=="4,50"){polizza.durata = "54"}
                                    else if(durata=="5,00"){polizza.durata = "60"}
                                    else if(durata=="5,50"){polizza.durata = "66"}
                                    else if(durata=="6,00"){polizza.durata = "72"}
                                    //polizza.durata = durata.toInteger()
                                } else polizza.durata = diffDateInMonths(polizza.fine, polizza.inizio).abs() //correzione campo durata se non presente in iAssicur
                                break
                            case 12: def stato = campo.text()
                                def index = stato.indexOf("(")
                                polizza.stato = stato.substring(0, index).trim()
                                break
                            case 13: polizza.cFiscale = campo.text()
                                break
                            case 14: polizza.targa = campo.text()
                                break
                            case 15: polizza.telaio = campo.text()
                                break
                            case 16: polizza.modello = campo.text()
                                break
                            case 17: polizza.marca = campo.text()
                                break
                            case 18: polizza.indirizzo = campo.text()
                                break
                            case 19: polizza.antifurto = campo.text()
                                break
                        }
                    }
                    result.polizze << polizza
                }
            }
        } catch(RESTClientException e) {
            if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
            else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
            else result.iAssicurError = e.response.statusMessage
            return result
            }
        return result
    }
    def getGMFGENFINLEACASHRINN( def mese, def anno) {
        def logg
        /*def dataodierna = new Date()
        dataodierna=dataodierna.clearTime()*/
        def c = Calendar.getInstance()
        //c.add(Calendar.MONTH, -1)
        def cal = Calendar.getInstance()
        //cal.setTime(dataodierna)
        //cal.add(Calendar.MONTH,-1)
        cal.set(Calendar.MONTH,Integer.parseInt(mese))
        cal.set(Calendar.YEAR,Integer.parseInt(anno))
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMinimum(Calendar.DAY_OF_MONTH))
        def firstDayOfTheMonth = cal.getTime().format("dd.MM.yyyy")
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DAY_OF_MONTH))
        def lastDayOfTheMonth = cal.getTime().format("dd.MM.yyyy")
        //println firstDayOfTheMonth
        //println lastDayOfTheMonth
        def select
        def result = [polizze: [], error: null, iAssicurError: null]
        try {
            if(Environment.current == Environment.PRODUCTION) {
                select = "select nome,numero,dre:inizio,datacompagnia,datacomunicaz,dre:scadenza,dre:ipertesto,rischio as targa,dre:rischio2 as telaio,ramo,dre:garanzia2 as onstar," +
                        "dre:durataanni,premio,dre:imponibile as imponibile,stato,statis-dslip as statodslip, dre:dataannullo from dslip where compagnia=a55 and stato=3 and " +
                        "data inrange ${firstDayOfTheMonth}:${lastDayOfTheMonth} order by numero asc"

            }else{
                select = "select nome,numero,dre:inizio,datacompagnia,datacomunicaz,dre:scadenza,dre:ipertesto,rischio as targa,dre:rischio2 as telaio,ramo,dre:garanzia2 as onstar," +
                        "dre:durataanni,premio,dre:imponibile as imponibile,stato,statis-dslip as statodslip, dre:dataannullo from dslip where compagnia=a55 and stato=3 and " +
                        "data inrange ${firstDayOfTheMonth}:${lastDayOfTheMonth} " +
                        "order by numero asc"

            }
         //select = "select nome,numero,dre:inizio,datacompagnia,datacomunicaz,dre:scadenza,dre:ipertesto,rischio as targa,dre:rischio2 as telaio,ramo,dre:garanzia2 as onstar,dre:durataanni,premio,dre:imponibile as imponibile,stato,statis-dslip as statodslip, dre:dataannullo from dslip where compagnia=a55 and stato=3 and data inrange 29.09.2016:29.09.2016 order by numero asc"
            println select
            logg =new Log(parametri: "query chiamata IAssicur ${select} ", operazione: "generaFileGMFGEN", pagina: "ELENCO POLIZZE CASH")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def res = query(select)
            if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuna polizza trovata"
            if (res.xml.Query.Record.Campo.size() > 0) {
                res.xml.Query.Record.each { record ->
                    def polizza = [:]
                    record.Campo.eachWithIndex { campo, indice ->
                        switch (indice) {
                            case 0: polizza.codiceDSLIP = campo.text()
                                break
                            case 1: def cliente  = campo.text()
                                if(cliente.indexOf("(") > 0){
                                    def pos = cliente.indexOf("(")
                                    cliente = cliente.substring(0, pos)
                                }
                                polizza.cliente=cliente.trim()
                                break
                            case 2: polizza.noPolizza = campo.text()
                                break
                            case 3: polizza.inizio = campo.text()
                                break
                            case 4: polizza.datacompagnia = campo.text()
                                break
                            case 5: polizza.datacomunicaz = campo.text()
                                break
                            case 6: polizza.fine = campo.text()
                                break
                            case 7: polizza.ipertesto = campo.text()
                                def ipertesto = campo.text()
                                def inizio, fine
                                def index = ipertesto.indexOf("VALORE NUOVO:")
                                if(index > 0){
                                    ipertesto = ipertesto.substring(index)
                                    inizio = ipertesto.indexOf(":")
                                    fine = ipertesto.indexOf("<br/>")
                                    def valorenuovo = ipertesto.substring(inizio + 1, fine)
                                    polizza.valoreAssicurato = valorenuovo.replaceAll(".","")
                                    polizza.valoreAssicurato = valorenuovo.replaceAll(",",".").toBigDecimal()
                                } else polizza.valoreAssicurato = 0.0
                                ipertesto = campo.text()
                                index = ipertesto.indexOf("IMMATRICOLAZIONE:")
                                if(index > 0) {
                                    ipertesto = ipertesto.substring(index)
                                    inizio = ipertesto.indexOf(":")
                                    fine = ipertesto.indexOf("<br/>")
                                    def immatricolazione = ipertesto.substring(inizio + 1, fine)
                                    polizza.immatricolazione = immatricolazione
                                } else polizza.immatricolazione = null
                                ipertesto = campo.text()
                                index = ipertesto.indexOf("VEICOLO NUOVO:")
                                if(index > 0) {
                                    ipertesto = ipertesto.substring(index)
                                    inizio = ipertesto.indexOf(":")
                                    fine = ipertesto.indexOf("<br/>")
                                    def stato = ipertesto.substring(inizio + 1, fine)
                                    polizza.stato = stato.contains("SI") ? "NUOVO" : "USATO"
                                } else polizza.stato = null
                                ipertesto = campo.text()
                                index = ipertesto.indexOf("TIPO VEICOLO:")
                                if(index > 0) {
                                    ipertesto = ipertesto.substring(index)
                                    inizio = ipertesto.indexOf(":")
                                    fine = ipertesto.indexOf("<br/>")
                                    def tipoVeicolo = ipertesto.substring(inizio + 1, fine)
                                    tipoVeicolo = tipoVeicolo.trim() ?: null
                                    switch (tipoVeicolo){
                                        case "A": polizza.tipoVeicolo = "AUTOVEICOLO"
                                            break
                                        case "G": polizza.tipoVeicolo = "VEICOLO_ELETTRICO"
                                            break
                                        case "C": polizza.tipoVeicolo = -"AUTOCARRO"
                                            break
                                    }
                                }
                                break
                            case 8: polizza.targa = campo.text()
                                break
                            case 9: polizza.telaio = campo.text()
                                break
                            case 10: def ramo = campo.text()
                                if(ramo.indexOf("(") > 0){
                                    def pos1 = ramo.indexOf("(")
                                    def pos2 = ramo.indexOf(")")
                                    ramo = ramo.substring(pos1+1, pos2)
                                }
                                polizza.ramo = ramo
                                break
                            case 11: polizza.onstar = campo.text()
                                break
                            case 12: def durata = campo.text()
                                if(durata){
                                    if(durata=="1,00"){
                                        polizza.durata = "12"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="1,50"){
                                        polizza.durata = "18"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="2,00"){
                                        polizza.durata = "24"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="2,50"){
                                        polizza.durata = "30"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="3,00"){
                                        polizza.durata = "36"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="3,50"){
                                        polizza.durata = "42"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="4,00"){
                                        polizza.durata = "48"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="4,50"){
                                        polizza.durata = "54"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="5,00"){
                                        polizza.durata = "60"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="5,50"){
                                        polizza.durata = "66"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="6,00"){
                                        polizza.durata = "72"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }

                                    //polizza.durata = durata.toInteger()
                                } else polizza.durata = diffDateInMonths(polizza.fine, polizza.inizio).abs() //correzione campo durata se non presente in iAssicur
                                break
                            case 13: def premio = campo.text()
                                if(premio.indexOf("\u20AC") > 0){
                                    def pos = premio.indexOf("\u20AC")
                                    premio = premio.substring(0, pos)
                                }
                                polizza.premio = premio
                                break
                            case 14: def imponibile= campo.text()
                                if(imponibile.indexOf("\u20AC") > 0){
                                    def pos = imponibile.indexOf("\u20AC")
                                    imponibile = imponibile.substring(0, pos)
                                }
                                polizza.imponibile = imponibile
                                break
                            case 15: polizza.stato = campo.text()
                                break
                            case 16: polizza.statodslip = campo.text()
                                break
                            case 17: polizza.dataannullo = campo.text()
                                break
                        }
                    }
                    result.polizze << polizza
                }
            }

        } catch(RESTClientException e) {
            if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
            else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
            else result.iAssicurError = e.response.statusMessage
            return result
        }
        return result
    }
    def getOFS( def mese, def anno) {
        def logg
        /*def dataodierna = new Date()
        dataodierna=dataodierna.clearTime()*/
        def c = Calendar.getInstance()
        //c.add(Calendar.MONTH, -1)
        def cal = Calendar.getInstance()
        //cal.setTime(dataodierna)
        //cal.add(Calendar.MONTH,-1)
        cal.set(Calendar.MONTH,Integer.parseInt(mese))
        cal.set(Calendar.YEAR,Integer.parseInt(anno))
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMinimum(Calendar.DAY_OF_MONTH))
        def firstDayOfTheMonth = cal.getTime().format("dd.MM.yyyy")
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DAY_OF_MONTH))
        def lastDayOfTheMonth = cal.getTime().format("dd.MM.yyyy")
        //println firstDayOfTheMonth
        //println lastDayOfTheMonth
        def select
        def result = [polizze: [], error: null, iAssicurError: null]
        try {
            if(Environment.current == Environment.PRODUCTION) {
                select = "select nome,numero,dre:inizio,datacompagnia,datacomunicaz,dre:scadenza,dre:ipertesto,rischio as targa,dre:rischio2 as telaio,ramo,dre:garanzia2 as onstar," +
                        "dre:durataanni,premio,dre:imponibile as imponibile,stato,statis-dslip as statodslip, dre:dataannullo from dslip where compagnia=a73 and stato=3 and " +
                        "data inrange ${firstDayOfTheMonth}:${lastDayOfTheMonth} order by numero asc"

            }else{
                select = "select nome,numero,dre:inizio,datacompagnia,datacomunicaz,dre:scadenza,dre:ipertesto,rischio as targa,dre:rischio2 as telaio,ramo,dre:garanzia2 as onstar," +
                        "dre:durataanni,premio,dre:imponibile as imponibile,stato,statis-dslip as statodslip, dre:dataannullo from dslip where compagnia=a73 and stato=3 and " +
                        "data inrange ${firstDayOfTheMonth}:${lastDayOfTheMonth} " +
                        "order by numero asc"

            }
         //select = "select nome,numero,dre:inizio,datacompagnia,datacomunicaz,dre:scadenza,dre:ipertesto,rischio as targa,dre:rischio2 as telaio,ramo,dre:garanzia2 as onstar,dre:durataanni,premio,dre:imponibile as imponibile,stato,statis-dslip as statodslip, dre:dataannullo from dslip where compagnia=a55 and stato=3 and data inrange 29.09.2016:29.09.2016 order by numero asc"
            println select
            logg =new Log(parametri: "query chiamata IAssicur ${select} ", operazione: "generaFileGMFGEN", pagina: "ELENCO POLIZZE RCA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def res = query(select)
            if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuna polizza trovata"
            if (res.xml.Query.Record.Campo.size() > 0) {
                res.xml.Query.Record.each { record ->
                    def polizza = [:]
                    record.Campo.eachWithIndex { campo, indice ->
                        switch (indice) {
                            case 0: polizza.codiceDSLIP = campo.text()
                                break
                            case 1: def cliente  = campo.text()
                                if(cliente.indexOf("(") > 0){
                                    def pos = cliente.indexOf("(")
                                    cliente = cliente.substring(0, pos)
                                }
                                polizza.cliente=cliente.trim()
                                break
                            case 2: polizza.noPolizza = campo.text()
                                break
                            case 3: polizza.inizio = campo.text()
                                break
                            case 4: polizza.datacompagnia = campo.text()
                                break
                            case 5: polizza.datacomunicaz = campo.text()
                                break
                            case 6: polizza.fine = campo.text()
                                break
                            case 7: polizza.ipertesto = campo.text()
                                def ipertesto = campo.text()
                                def inizio, fine
                                def index = ipertesto.indexOf("VALORE NUOVO:")
                                if(index > 0){
                                    ipertesto = ipertesto.substring(index)
                                    inizio = ipertesto.indexOf(":")
                                    fine = ipertesto.indexOf("<br/>")
                                    def valorenuovo = ipertesto.substring(inizio + 1, fine)
                                    polizza.valoreAssicurato = valorenuovo.replaceAll(".","")
                                    polizza.valoreAssicurato = valorenuovo.replaceAll(",",".").toBigDecimal()
                                } else polizza.valoreAssicurato = 0.0
                                ipertesto = campo.text()
                                index = ipertesto.indexOf("IMMATRICOLAZIONE:")
                                if(index > 0) {
                                    ipertesto = ipertesto.substring(index)
                                    inizio = ipertesto.indexOf(":")
                                    fine = ipertesto.indexOf("<br/>")
                                    def immatricolazione = ipertesto.substring(inizio + 1, fine)
                                    polizza.immatricolazione = immatricolazione
                                } else polizza.immatricolazione = null
                                ipertesto = campo.text()
                                index = ipertesto.indexOf("VEICOLO NUOVO:")
                                if(index > 0) {
                                    ipertesto = ipertesto.substring(index)
                                    inizio = ipertesto.indexOf(":")
                                    fine = ipertesto.indexOf("<br/>")
                                    def stato = ipertesto.substring(inizio + 1, fine)
                                    polizza.stato = stato.contains("SI") ? "NUOVO" : "USATO"
                                } else polizza.stato = null
                                ipertesto = campo.text()
                                index = ipertesto.indexOf("TIPO VEICOLO:")
                                if(index > 0) {
                                    ipertesto = ipertesto.substring(index)
                                    inizio = ipertesto.indexOf(":")
                                    fine = ipertesto.indexOf("<br/>")
                                    def tipoVeicolo = ipertesto.substring(inizio + 1, fine)
                                    tipoVeicolo = tipoVeicolo.trim() ?: null
                                    switch (tipoVeicolo){
                                        case "A": polizza.tipoVeicolo = "AUTOVEICOLO"
                                            break
                                        case "G": polizza.tipoVeicolo = "VEICOLO_ELETTRICO"
                                            break
                                        case "C": polizza.tipoVeicolo = -"AUTOCARRO"
                                            break
                                    }
                                }
                                break
                            case 8: polizza.targa = campo.text()
                                break
                            case 9: polizza.telaio = campo.text()
                                break
                            case 10: def ramo = campo.text()
                                if(ramo.indexOf("(") > 0){
                                    def pos1 = ramo.indexOf("(")
                                    def pos2 = ramo.indexOf(")")
                                    ramo = ramo.substring(pos1+1, pos2)
                                }
                                polizza.ramo = ramo
                                break
                            case 11: polizza.onstar = campo.text()
                                break
                            case 12: def durata = campo.text()
                                if(durata){
                                    if(durata=="1,00"){
                                        polizza.durata = "12"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="1,50"){
                                        polizza.durata = "18"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="2,00"){
                                        polizza.durata = "24"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="2,50"){
                                        polizza.durata = "30"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="3,00"){
                                        polizza.durata = "36"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="3,50"){
                                        polizza.durata = "42"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="4,00"){
                                        polizza.durata = "48"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="4,50"){
                                        polizza.durata = "54"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="5,00"){
                                        polizza.durata = "60"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="5,50"){
                                        polizza.durata = "66"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="6,00"){
                                        polizza.durata = "72"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }

                                    //polizza.durata = durata.toInteger()
                                } else polizza.durata = diffDateInMonths(polizza.fine, polizza.inizio).abs() //correzione campo durata se non presente in iAssicur
                                break
                            case 13: def premio = campo.text()
                                if(premio.indexOf("\u20AC") > 0){
                                    def pos = premio.indexOf("\u20AC")
                                    premio = premio.substring(0, pos)
                                }
                                polizza.premio = premio
                                break
                            case 14: def imponibile= campo.text()
                                if(imponibile.indexOf("\u20AC") > 0){
                                    def pos = imponibile.indexOf("\u20AC")
                                    imponibile = imponibile.substring(0, pos)
                                }
                                polizza.imponibile = imponibile
                                break
                            case 15: polizza.stato = campo.text()
                                break
                            case 16: polizza.statodslip = campo.text()
                                break
                            case 17: polizza.dataannullo = campo.text()
                                break
                        }
                    }
                    result.polizze << polizza
                }
            }

        } catch(RESTClientException e) {
            if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
            else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
            else result.iAssicurError = e.response.statusMessage
            return result
        }
        return result
    }
    def getGMFGENFINLEAPAI( def mese, def anno) {
        def logg
        /*def dataodierna = new Date()
        dataodierna=dataodierna.clearTime()*/
        def c = Calendar.getInstance()
        //c.add(Calendar.MONTH, -1)
        def cal = Calendar.getInstance()
        //cal.setTime(dataodierna)
        //cal.add(Calendar.MONTH,-1)
        cal.set(Calendar.MONTH,Integer.parseInt(mese))
        cal.set(Calendar.YEAR,Integer.parseInt(anno))
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMinimum(Calendar.DAY_OF_MONTH))
        def firstDayOfTheMonth = cal.getTime().format("dd.MM.yyyy")
        cal.set(Calendar.DAY_OF_MONTH,cal.getActualMaximum(Calendar.DAY_OF_MONTH))
        def lastDayOfTheMonth = cal.getTime().format("dd.MM.yyyy")
        //println firstDayOfTheMonth
        //println lastDayOfTheMonth
        def select
        def result = [polizze: [], error: null, iAssicurError: null]
        try {
            if(Environment.current == Environment.PRODUCTION) {
                select = "select nome,numero,dre:inizio,datacompagnia,datacomunicaz,dre:scadenza,dre:ipertesto,rischio as targa,dre:rischio2 as telaio,ramo,dre:garanzia2 " +
                        "as onstar,dre:durataanni,premio,dre:imponibile as imponibile,stato,statis-dslip as statodslip,dre:dataannullo from dslip where compagnia=a56 and stato=3 " +
                        "and ramo in (K/51,K/52, K/39,K/40) and data inrange  ${firstDayOfTheMonth}:${lastDayOfTheMonth}  order by numero asc"
            }else{
                select="select nome,numero,dre:inizio,datacompagnia,datacomunicaz,dre:scadenza,dre:ipertesto,rischio as targa,dre:rischio2 as telaio,ramo,dre:garanzia2 as onstar," +
                        "dre:durataanni,premio,dre:imponibile as imponibile,stato,statis-dslip as statodslip,dre:dataannullo from dslip where compagnia=a56 and stato=3 and ramo " +
                        "in (K/51,K/52,K/39,K/40) and data inrange  ${firstDayOfTheMonth}:${lastDayOfTheMonth}  order by numero asc"
            }
          //select = "select nome,numero,dre:inizio,datacompagnia,datacomunicaz,dre:scadenza,dre:ipertesto,rischio as targa,dre:rischio2 as telaio,ramo,dre:garanzia2 as onstar,dre:durataanni,premio,dre:imponibile as imponibile,stato,statis-dslip as statodslip,dre:dataannullo from dslip where compagnia=a56 and stato=3 and ramo in (k/51,k/52) and data inrange ${firstDayOfTheMonth}:${lastDayOfTheMonth} order by numero asc"
         //select = "select nome,numero,dre:inizio,datacompagnia,datacomunicaz,dre:scadenza,dre:ipertesto,rischio as targa,dre:rischio2 as telaio,ramo,dre:garanzia2 as onstar,dre:durataanni,premio,dre:imponibile as imponibile,stato,statis-dslip as statodslip,dre:dataannullo from dslip where compagnia=a56 and stato=3  and data inrange 01.11.2016:10.11.2016  order by numero asc"
            println select
            logg =new Log(parametri: "query chiamata IAssicur ${select} ", operazione: "generaFileGMFGEN", pagina: "POLIZZE PAI PAGAMENTO")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def res = query(select)
            if (res == null || res.xml.Query.Record.size() == 0) result.error = "Nessuna polizza trovata"
            if (res.xml.Query.Record.Campo.size() > 0) {
                res.xml.Query.Record.each { record ->
                    def polizza = [:]
                    record.Campo.eachWithIndex { campo, indice ->
                        switch (indice) {
                            case 0: polizza.codiceDSLIP = campo.text()
                                break
                            case 1: def cliente  = campo.text()
                                if(cliente.indexOf("(") > 0){
                                    def pos = cliente.indexOf("(")
                                    cliente = cliente.substring(0, pos)
                                }
                                polizza.cliente=cliente.trim()
                                break
                            case 2: polizza.noPolizza = campo.text()
                                break
                            case 3: polizza.inizio = campo.text()
                                break
                            case 4: polizza.datacompagnia = campo.text()
                                break
                            case 5: polizza.datacomunicaz = campo.text()
                                break
                            case 6: polizza.fine = campo.text()
                                break
                            case 7: polizza.ipertesto = campo.text()
                                def ipertesto = campo.text()
                                def inizio, fine
                                def index = ipertesto.indexOf("VALORE NUOVO:")
                                if(index > 0){
                                    ipertesto = ipertesto.substring(index)
                                    inizio = ipertesto.indexOf(":")
                                    fine = ipertesto.indexOf("<br/>")
                                    def valorenuovo = ipertesto.substring(inizio + 1, fine)
                                    polizza.valoreAssicurato = valorenuovo.replaceAll(".","")
                                    polizza.valoreAssicurato = valorenuovo.replaceAll(",",".").toBigDecimal()
                                } else polizza.valoreAssicurato = 0.0
                                ipertesto = campo.text()
                                index = ipertesto.indexOf("IMMATRICOLAZIONE:")
                                if(index > 0) {
                                    ipertesto = ipertesto.substring(index)
                                    inizio = ipertesto.indexOf(":")
                                    fine = ipertesto.indexOf("<br/>")
                                    def immatricolazione = ipertesto.substring(inizio + 1, fine)
                                    polizza.immatricolazione = immatricolazione
                                } else polizza.immatricolazione = null
                                ipertesto = campo.text()
                                index = ipertesto.indexOf("VEICOLO NUOVO:")
                                if(index > 0) {
                                    ipertesto = ipertesto.substring(index)
                                    inizio = ipertesto.indexOf(":")
                                    fine = ipertesto.indexOf("<br/>")
                                    def stato = ipertesto.substring(inizio + 1, fine)
                                    polizza.stato = stato.contains("SI") ? "NUOVO" : "USATO"
                                } else polizza.stato = null
                                ipertesto = campo.text()
                                index = ipertesto.indexOf("TIPO VEICOLO:")
                                if(index > 0) {
                                    ipertesto = ipertesto.substring(index)
                                    inizio = ipertesto.indexOf(":")
                                    fine = ipertesto.indexOf("<br/>")
                                    def tipoVeicolo = ipertesto.substring(inizio + 1, fine)
                                    tipoVeicolo = tipoVeicolo.trim() ?: null
                                    switch (tipoVeicolo){
                                        case "A": polizza.tipoVeicolo = "AUTOVEICOLO"
                                            break
                                        case "G": polizza.tipoVeicolo = "VEICOLO_ELETTRICO"
                                            break
                                        case "C": polizza.tipoVeicolo = -"AUTOCARRO"
                                            break
                                    }
                                }
                                break
                            case 8: polizza.targa = campo.text()
                                break
                            case 9: polizza.telaio = campo.text()
                                break
                            case 10: def ramo = campo.text()
                                if(ramo.indexOf("(") > 0){
                                    def pos1 = ramo.indexOf("(")
                                    def pos2 = ramo.indexOf(")")
                                    ramo = ramo.substring(pos1+1, pos2)
                                }
                                polizza.ramo = ramo
                                break
                            case 11: polizza.onstar = campo.text()
                                break
                            case 12: def durata = campo.text()
                                if(durata){
                                    if(durata=="1,00"){
                                        polizza.durata = "12"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="1,50"){
                                        polizza.durata = "18"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="2,00"){
                                        polizza.durata = "24"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="2,50"){
                                        polizza.durata = "30"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="3,00"){
                                        polizza.durata = "36"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="3,50"){
                                        polizza.durata = "42"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="4,00"){
                                        polizza.durata = "48"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="4,50"){
                                        polizza.durata = "54"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="5,00"){
                                        polizza.durata = "60"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="5,50"){
                                        polizza.durata = "66"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }
                                    else if(durata=="6,00"){
                                        polizza.durata = "72"
                                        polizza.anni=durata.substring(0,durata.indexOf(","))
                                    }

                                    //polizza.durata = durata.toInteger()
                                } else polizza.durata = diffDateInMonths(polizza.fine, polizza.inizio).abs() //correzione campo durata se non presente in iAssicur
                                break
                            case 13: def premio = campo.text()
                                if(premio.indexOf("\u20AC") > 0){
                                    def pos = premio.indexOf("\u20AC")
                                    premio = premio.substring(0, pos)
                                }
                                polizza.premio = premio
                                break
                            case 14: def imponibile= campo.text()
                                if(imponibile.indexOf("\u20AC") > 0){
                                    def pos = imponibile.indexOf("\u20AC")
                                    imponibile = imponibile.substring(0, pos)
                                }
                                polizza.imponibile = imponibile
                                break
                            case 15: polizza.stato = campo.text()
                                break
                            case 16: polizza.statodslip = campo.text()
                                break
                            case 17: polizza.dataannullo = campo.text()
                                break
                        }
                    }
                    result.polizze << polizza
                }
            }

        } catch(RESTClientException e) {
            if(e.response == null) result.iAssicurError = "Errore di connessione al portale iAssicur"
            else if(e.response.statusCode == 401) result.iAssicurError = "Errore di autenticazione sul portale iAssicur"
            else result.iAssicurError = e.response.statusMessage
            return result
        }
        return result
    }
    def chiamataWSRecessoAnnulla(def polizza, def polizzaWS, def coperturaR, boolean isRinnovo, def polizzaRinn, def codOperazione){
        def logg
        def antifurto,test,classeprovvi,pacchettoCod,valoreAssicurato,durata,provincia,localita,dealerType,percVenditore
        def newDataFine,dataeffettocopertura, datascadenzacopertura
        def operazione, pagina
        test="N"
        /*if(Environment.current == Environment.PRODUCTION) {
            test="N"
        }else{
            test="S"
        }*/
        if(codOperazione=="E"){
            if(Date.parse( 'dd/MM/yyyy', polizzaWS.dataannullo )){
                logg =new Log(parametri: "ha questo formato->${polizzaWS.dataannullo}", operazione: "annulla polizze CASH/FIN/LEASING", pagina: "ANNULLAMENTI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                newDataFine=Date.parse( 'dd/MM/yyyy', polizzaWS.dataannullo ).format( 'yyyy-MM-dd' )
            }else {
                newDataFine=polizzaWS.dataannullo
            }

            if(Date.parse( 'dd/MM/yyyy', polizzaWS.inizio )){
                logg =new Log(parametri: "ha questo formato->${polizzaWS.inizio}", operazione: "annulla polizze CASH/FIN/LEASING", pagina: "ANNULLAMENTI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                dataeffettocopertura=Date.parse( 'dd/MM/yyyy', polizzaWS.inizio ).format( 'yyyy-MM-dd' )
            }else {
                dataeffettocopertura=polizzaWS.inizio
            }
            if(Date.parse( 'dd/MM/yyyy', polizzaWS.fine )){
                logg =new Log(parametri: "ha questo formato->${polizzaWS.fine}", operazione: "annulla polizze CASH/FIN/LEASING", pagina: "ANNULLAMENTI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                datascadenzacopertura=Date.parse( 'dd/MM/yyyy', polizzaWS.fine ).format( 'yyyy-MM-dd' )
            }else {
                datascadenzacopertura=polizzaWS.fine
            }

        }else{
            if(polizzaRinn.dataStorno.toString().matches("^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d\$ ")) {
                logg =new Log(parametri: "ha questo formato->${polizzaRinn.dataStorno}", operazione: "recede polizze CASH/FIN/LEASING", pagina: "ANNULLAMENTI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                newDataFine=Date.parse( 'dd/MM/yyyy', polizzaRinn.dataStorno).format( 'yyyy-MM-dd' )
            }
            else {
                newDataFine=polizzaRinn.dataStorno
            }
            if(polizzaRinn.dataDecorrenza.toString().matches("^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d\$ ")) {
                logg =new Log(parametri: "ha questo formato->${polizzaRinn.dataDecorrenza}", operazione: "recede polizze CASH/FIN/LEASING", pagina: "ANNULLAMENTI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                dataeffettocopertura=Date.parse( 'dd/MM/yyyy', polizzaRinn.dataDecorrenza ).format( 'yyyy-MM-dd' )
            }else {
                dataeffettocopertura=polizzaRinn.dataDecorrenza
            }
            if(polizzaRinn.dataScadenza.toString().matches("^(0[1-9]|1[012])[- /.](0[1-9]|[12][0-9]|3[01])[- /.](19|20)\\d\\d\$ ")) {
                logg =new Log(parametri: "ha questo formato->${polizzaRinn.dataScadenza}", operazione: "recede polizze CASH/FIN/LEASING", pagina: "ANNULLAMENTI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                datascadenzacopertura=Date.parse( 'dd/MM/yyyy', polizzaRinn.dataScadenza).format( 'yyyy-MM-dd' )
            }else {
                datascadenzacopertura=polizzaRinn.dataScadenza
            }

        }
        if(!isRinnovo){
            operazione="annulla polizze CASH/FIN/LEASING"
            pagina="ANNULLAMENTI"
            if(polizza.onStar){
                antifurto="S"
            }else{antifurto="N"}

            if(Date.parse( 'dd/MM/yyyy', polizzaWS.inizio )){
                logg =new Log(parametri: "ha questo formato->${polizzaWS.inizio}", operazione: "annulla polizze CASH/FIN/LEASING", pagina: "ANNULLAMENTI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                dataeffettocopertura=Date.parse( 'dd/MM/yyyy', polizzaWS.inizio ).format( 'yyyy-MM-dd' )
            }else {
                dataeffettocopertura=polizzaWS.inizio
            }
            if(Date.parse( 'dd/MM/yyyy', polizzaWS.fine )){
                logg =new Log(parametri: "ha questo formato->${polizzaWS.fine}", operazione: "annulla polizze CASH/FIN/LEASING", pagina: "ANNULLAMENTI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                datascadenzacopertura=Date.parse( 'dd/MM/yyyy', polizzaWS.fine ).format( 'yyyy-MM-dd' )
            }else {
                datascadenzacopertura=polizzaWS.fine
            }

            pacchettoCod=CodProdotti.findByCoperturaAndStep(coperturaR, polizza.step).codPacchetto
            if(polizza.step.toString().equalsIgnoreCase("step 1")){
                classeprovvi="1"
            }else if(polizza.step.toString().equalsIgnoreCase("step 2")){
                classeprovvi="2"
            }else if(polizza.step.toString().equalsIgnoreCase("step 3")){
                classeprovvi="3"
            }
            valoreAssicurato=polizza.valoreAssicurato
            durata=polizza.durata
            provincia=polizza.provincia
            localita=polizza.localita
            dealerType=polizza.dealer.dealerType
            percVenditore= polizza.dealer.percVenditore
            if(percVenditore.toPlainString()=="0.0000000"){
                percVenditore=0.0
            }
        }else {
            antifurto="N"
            dealerType=""
            percVenditore="0"
            classeprovvi="0"
            if(codOperazione=="E"){
                operazione="annulla polizze RINNOVO"
                pagina="ANNULLAMENTI"
                pacchettoCod=polizzaRinn.codPacchetto.join(" ")
                //pacchettoCod="ORP0R"
                valoreAssicurato=polizzaRinn.valoreAssicurato.join(" ")
                durata=polizzaRinn.durata.join(" ")
                provincia=polizzaRinn.provincia.join(" ")
                localita=polizzaRinn.localita.join(" ")
            }else{
                operazione="recesso polizze RINNOVO"
                pagina="RECESSO"
                pacchettoCod=polizzaRinn.codPacchetto
                //pacchettoCod="ORP0R"
                valoreAssicurato=polizzaRinn.valoreAssicurato
                durata=polizzaRinn.durata
                provincia=polizzaRinn.provincia
                localita=polizzaRinn.localita
            }
        }
        def parameters  =[
                codicepacchetto: "${pacchettoCod}",
                valoreassicurato: valoreAssicurato,
                durata: durata,
                provincia: provincia,
                comune: localita,
                operazione: codOperazione,
                dataeffettocopertura: dataeffettocopertura,
                antifurto: antifurto,
                dealerType: dealerType,
                classeProvvigionale: classeprovvi,
                percentualeVenditore: percVenditore,
                test:test,
                datascadenzacopertura:datascadenzacopertura,
                dataeffettostorno:newDataFine
        ]
        println "parametri inviati: ${parameters}"
        logg =new Log(parametri: "parametri inviate al webservice per annullamento/recesso->${parameters}", operazione: "${operazione}", pagina: "${pagina}")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def url = "http://gmf.mach-1.it"
        def js = (parameters as JSON).toString()
        js = js.substring(1, js.length()-1)
        def path = "/calcprovvigioni/"
        /*if(Environment.current != Environment.PRODUCTION){
            path = "/test/calcprovvigioni/"
        }*/
        logg =new Log(parametri: "il path che viene chiamato->${path}", operazione: "chiamata  ws", pagina: "ELENCO POLIZZE CASH")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def query = "{"+js+"}"
        def errore=[]
        def response = DLWebService.postText(url, path, [p:query])
        def stringaresponse=""
        def esito,premioLordo,premioImpo,provvDealer,provvGMF,provvVendi,provvMansutti,provvMach1,rappel,codZonaTerr,imposte
        boolean risposta=false
        JSONObject userJson = JSON.parse(response)
        userJson.each { id, data ->
            logg = new Log(parametri: "dati webservice->${data}", operazione: "genera GMFGEN chiamata ws annullamenti", pagina: "ELENCO POLIZZE CASH")
            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if (data.Errors?.descErrore) {
                def desErrore = data.Errors?.descErrore?.toString().replaceAll("\\[", "").replaceAll("]", "")
                logg = new Log(parametri: "errore riportato dal webservice->${desErrore}", operazione: "genera GMFGEN chiamata ws annullamenti", pagina: "ELENCO POLIZZE CASH")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if (!(desErrore.contains("Comune non identificato"))) {
                    risposta = false
                    errore = data.Errors?.descErrore.toString().replaceAll("\\[", "").replaceAll("]", "")
                } else if ((desErrore.contains("Comune non identificato")) && !(desErrore.contains("Provincia non valida"))) {
                    risposta = true
                } else if ((desErrore.contains("Comune non identificato")) && desErrore.contains("Provincia non valida")) {
                    risposta = false
                    errore = data.Errors?.descErrore?.toString().replaceAll("\\[", "").replaceAll("]", "")
                }
            } else {
                risposta = true
            }
            logg = new Log(parametri: "valore parametro risposta->${risposta}", operazione: "genera GMFGEN chiamata ws annullamenti", pagina: "ELENCO POLIZZE CASH")
            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if (risposta) {
                esito = data.Esito ?: 1
                premioLordo = data.PremioLordo ?: 0.0
                premioImpo = data.Premio ?: 0.0
                provvDealer = data.provvigioneDealer ?: 0.0
                provvGMF = data.provvigioneGMF ?: 0.0
                provvVendi = data.provvigioneVenditore ?: 0.0
                provvMansutti = data.provvigioneMansutti ?: 0.0
                provvMach1 = data.provvigioneMach1 ?: 0.0
                rappel = data.rappel ?: 0.0
                codZonaTerr = data.codiceTipoZonaTerritoriale ?: 0.0
                imposte = data.imposte ?: 0.0
            }
        }
        if(risposta) stringaresponse=[risposta:risposta,esito:esito,premioLordo:premioLordo,provvDealer:provvDealer,provvGMF:provvGMF,provvVendi:provvVendi,premioImpo:premioImpo, provvMansutti:provvMansutti, provvMach1:provvMach1,rappel:rappel,codZonaTerr:codZonaTerr,imposte:imposte]
        else stringaresponse=[risposta:risposta,esito:esito,errore:errore,premioLordo:premioLordo,provvDealer:provvDealer,provvGMF:provvGMF,provvVendi:provvVendi,premioImpo:premioImpo, provvMansutti:provvMansutti, provvMach1:provvMach1, rappel:rappel,codZonaTerr:codZonaTerr,imposte:imposte]
        errore=[]
        logg = new Log(parametri: "valori restituiti dal WS->${stringaresponse}", operazione: "genera GMFGEN chiamata ws annullamenti", pagina: "ELENCO POLIZZE CASH")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        return stringaresponse
    }
    private int diffDateInMonths(Date start, Date end) {
        if(start > end) {
            def temp = end
            end = start
            start = temp
        }
        def diffMesi = (start[Calendar.MONTH] - end[Calendar.MONTH]) + 1
        def diffAnni = start[Calendar.YEAR] - end[Calendar.YEAR]
        return diffMesi + (diffAnni * 12)
    }
    private GrailsWebRequest getRequest() { WebUtils.retrieveGrailsWebRequest()}
    private HttpSession getSession() { WebUtils.retrieveGrailsWebRequest().getSession() }
    private static boolean checkIt(Calendar calendar) {
        def logg
        logg =new Log(parametri: "verifico se il giorno e' l'ultimo del mese", operazione: "check it", pagina: "IASSICUR WEB SERVICE")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def monthFormat = new SimpleDateFormat("MMMMM")
        def monthStr = monthFormat.format(calendar.getTime())
        logg =new Log(parametri: "ultimo giorno del mese ${monthStr} e' ${calendar.getActualMaximum(Calendar.DATE)}", operazione: "check it", pagina: "IASSICUR WEB SERVICE")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

        println("Maximum for " + monthStr +" is "
                + calendar.getActualMaximum(Calendar.DATE))

        def isLastDay = calendar.get(Calendar.DATE) == calendar.getActualMaximum(Calendar.DATE)
        logg =new Log(parametri: "Il giorno di oggi  ${(isLastDay ? "e' l'ultimo " : "non e' l'ultimo ")}  del mese", operazione: "check it", pagina: "IASSICUR WEB SERVICE")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        println("The calendar date " +
                (isLastDay ? "is " : "is not ") +
                "the last day of the month.")
        return isLastDay
    }
    /*static def postText(String baseUrl, String path, Map query, method = Method.GET) {
        def logg
        try {
            def ret = null
            def http = new HTTPBuilder(baseUrl)
            // perform a POST request, expecting TEXT response
            http.request(method, ContentType.TEXT) {
                uri.path = path
                uri.query = query
                //headers.'User-Agent' = 'Mozilla/5.0 Ubuntu/8.10 Firefox/3.0.4'
                // response handler for a success response code
                response.success = { resp, reader ->
                    logg =new Log(parametri: "response status: ${resp.statusLine}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizze")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println "response status: ${resp.statusLine}"
                    //println 'Headers: -----------'
                    resp.headers.each { h ->
                        // println " ${h.name} : ${h.value}"
                    }
                    ret = reader.getText()
                    *//*println 'Response data: -----'
                    println ret
                    println '--------------------'*//*
                }
            }
            return ret
        } catch (groovyx.net.http.HttpResponseException ex) {
            ex.printStackTrace()
            return null
        } catch (java.net.ConnectException ex) {
            ex.printStackTrace()
            return null
        }
    }*/
}