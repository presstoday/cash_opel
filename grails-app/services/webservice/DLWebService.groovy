package webservice

import cashopel.AssocTelaiDataFinLeasJob
import cashopel.polizze.CodProdotti
import cashopel.polizze.Polizza
import cashopel.polizze.StatoPolizza
import cashopel.polizze.TipoCliente
import cashopel.polizze.TipoPolizza
import cashopel.polizze.Tracciato
import cashopel.polizze.TracciatoCompagnia
import cashopel.polizze.ZoneTarif
import cashopel.utenti.Dealer
import cashopel.utenti.Log
import grails.converters.JSON
import grails.util.Environment
import groovy.time.TimeCategory
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import org.grails.web.json.JSONObject

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.util.regex.Pattern

class DLWebService {
    static transactional = false
    def tracciatiService
    def postText(String baseUrl, String path, Map query, method = Method.GET) {
        def logg
        logg =new Log(parametri: "path: ${path}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizze")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        logg =new Log(parametri: "query: ${query}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizze")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        logg =new Log(parametri: "baseUrl: ${baseUrl}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizze")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        try {
            def ret = null
            def http = new HTTPBuilder(baseUrl)
            // perform a POST request, expecting TEXT response
            http.request(method, ContentType.TEXT) {
                uri.path = path
                uri.query = query
                //headers.'User-Agent' = 'Mozilla/5.0 Ubuntu/8.10 Firefox/3.0.4'
                // response handler for a success response code
                response.success = { resp, reader ->
                    logg =new Log(parametri: "response status: ${resp.statusLine}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizze")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println "response status: ${resp.statusLine}"
                    //println 'Headers: -----------'
                    resp.headers.each { h ->
                        // println " ${h.name} : ${h.value}"
                    }
                    ret = reader.getText()
                    /*println 'Response data: -----'
                    println ret
                    println '--------------------'*/
                }
            }
            return ret
        } catch (groovyx.net.http.HttpResponseException ex) {
            ex.printStackTrace()
            return null
        } catch (java.net.ConnectException ex) {
            ex.printStackTrace()
            return null
        }
    }
    /*def riepilogoPolizze(){
           def dataodierna = new Date()
           dataodierna.clearTime()
           def document = new Document(PageSize.A4, 40, 40, 60, 50)
           def baosPDF= new ByteArrayOutputStream()
           PdfWriter docWriter = null
           docWriter = PdfWriter.getInstance(document, baosPDF)
           def font1 = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD)
           def font2 = new Font(Font.FontFamily.HELVETICA, 10, Font.UNDERLINE)
           def font3 = new Font(Font.FontFamily.HELVETICA, 10)
           document.open()
           def dealers=Polizza.createCriteria().list(){
               eq "dataInserimento", dataodierna
               //between "dateCreated", dataodierna-1, dataodierna
               projections {
                   groupProperty "dealer"
               }
           }
           def streams = []
           if (dealers.size() > 0) {
               dealers.each { dealer->
                   def polizzeFL = *//*SqlLogger.log {*//*Polizza.createCriteria().list() {
                       gt "dataInserimento", dataodierna-1
                       le "dataInserimento", dataodierna
                       ne "tipoPolizza", TipoPolizza.CASH
                       //between "dateCreated",  dataodierna-1, dataodierna
                       eq "dealer", dealer
                   }
                   def polizzeC = *//*SqlLogger.log {*//*Polizza.createCriteria().list() {
                       gt "dateCreated", dataodierna-1
                       le "dateCreated", dataodierna
                       eq "tipoPolizza", TipoPolizza.CASH
                       //between "dateCreated",  dataodierna-1, dataodierna
                       eq "dealer", dealer
                   }
                   document.add( new Paragraph( "Buongiorno,", font3 ) )
                   document.add( Chunk.NEWLINE )
                   document.add( new Paragraph( "In allegato le coperture attivate oggi.",font3 ) )
                   document.add( Chunk.NEWLINE )
                   if (polizzeFL.size() > 0) {
                       document.add( new Paragraph( "FINANZIATE E LEASING",font1 ) )
                       document.add( new Paragraph( " ",font3 ) )
                       float[] columnWidths = [6,4,6,4,4,2]
                       def table = new PdfPTable(columnWidths)
                       table.setHorizontalAlignment(Element.ALIGN_CENTER)
                       table.setWidthPercentage(100)
                       // the cell object
                       def cell
                       cell = new PdfPCell(new Phrase("Cliente", font3))
                       table.addCell(cell)
                       cell = new PdfPCell(new Phrase("Modello", font3))
                       table.addCell(cell)
                       cell = new PdfPCell(new Phrase("Telaio", font3))
                       table.addCell(cell)
                       cell = new PdfPCell(new Phrase("Pacchetto", font3))
                       table.addCell(cell)
                       cell = new PdfPCell(new Phrase("Data inizio copertura", font3))
                       table.addCell(cell)
                       cell = new PdfPCell(new Phrase("Durata", font3))
                       table.addCell(cell)
                       polizzeFL.each { polizza ->
                           table.addCell(new PdfPCell(new Phrase("${polizza.cognome} ${polizza.nome}", font3)))
                           table.addCell(new PdfPCell(new Phrase("${polizza.modello}", font3)))
                           table.addCell(new PdfPCell(new Phrase("${polizza.telaio}", font3)))
                           table.addCell(new PdfPCell(new Phrase("${polizza.coperturaRichiesta}", font3)))
                           table.addCell(new PdfPCell(new Phrase("${polizza.dataDecorrenza?.format("dd-MM-yyyy")}", font3)))
                           table.addCell(new PdfPCell(new Phrase("${polizza.durata}", font3)))
                       }
                       document.add(table)
                       document.add( Chunk.NEWLINE )
                   }
                   document.add( new Paragraph( "Le ricordiamo che procederemo entro fine mese a inviare al Cliente, al recapito indicato sul modulo di adesione, la lettera di benvenuto con l\u0027elenco delle coperture assicurative.", font3 ) )

                   if (polizzeC.size() > 0) {
                       document.add( Chunk.NEWLINE )
                       document.add( new Paragraph( "CASH", font1 ))
                       document.add( new Paragraph( " ",font3 ) )
                       float[] columnWidths = [6,4,6,4,4,2]
                       def table = new PdfPTable(columnWidths)
                       table.setHorizontalAlignment(Element.ALIGN_CENTER)
                       table.setWidthPercentage(100)
                       // the cell object
                       def cell
                       cell = new PdfPCell(new Phrase("Cliente", font3))
                       table.addCell(cell)
                       cell = new PdfPCell(new Phrase("Modello", font3))
                       table.addCell(cell)
                       cell = new PdfPCell(new Phrase("Telaio", font3))
                       table.addCell(cell)
                       cell = new PdfPCell(new Phrase("Pacchetto", font3))
                       table.addCell(cell)
                       cell = new PdfPCell(new Phrase("Data inizio copertura", font3))
                       table.addCell(cell)
                       cell = new PdfPCell(new Phrase("Durata", font3))
                       table.addCell(cell)
                       polizzeC.each { polizza ->
                           table.addCell(new PdfPCell(new Phrase("${polizza.cognome} ${polizza.nome}", font3)))
                           table.addCell(new PdfPCell(new Phrase("${polizza.modello}", font3)))
                           table.addCell(new PdfPCell(new Phrase("${polizza.telaio}", font3)))
                           table.addCell(new PdfPCell(new Phrase("${polizza.coperturaRichiesta}", font3)))
                           table.addCell(new PdfPCell(new Phrase("${polizza.dataDecorrenza?.format("dd-MM-yyyy")}", font3)))
                           table.addCell(new PdfPCell(new Phrase("${polizza.durata}", font3)))
                       }
                       document.add(table)
                   }
                   document.add( Chunk.NEWLINE )
                   def par1=new Chunk("Ricordiamo che il Programma prevede che la decorrenza della copertura, per il veicolo appena venduto, inizi dal giorno di immatricolazione (o data di voltura se usato), ", font3)
                   def par2=new Chunk("alla condizione che il premio venga pagato tramite bonifico bancario entro e non oltre 10 giorni da tale data.", font1)
                   def uniparagraph=new Paragraph()
                   uniparagraph.add(new Chunk(par1))
                   uniparagraph.add(new Chunk(par2))
                   document.add(uniparagraph)
                   document.add( Chunk.NEWLINE )
                   document.add(new Paragraph("I riferimenti del beneficiario e del conto corrente sul quale effettuare il versamento sono quelli sotto indicati:", font3))
                   document.add( Chunk.NEWLINE )
                   document.add(new Paragraph("Beneficiario:MACH1 s.r.l.- Via Vittor Pisani, 13 \u002D 20124 Milano", font3))
                   document.add(new Paragraph(" ", font3))
                   document.add(new Paragraph("IBAN: IT14R0623001629000043621881", font3))
                   document.add(new Paragraph(" ", font3))
                   document.add(new Paragraph("Causale: Nome e cognome dell'intestatario al P.R.A. - Targa o Telaio del veicolo",font3))
                   document.add( Chunk.NEWLINE )
                   def par4=new Chunk("sar\u00E1 nostra cura inviare direttamente all\u0027Assicurato la lettera di benvenuto unitamente all\u0027elenco delle garanzie attivate.", font3)
                   def par3=new Chunk("Non appena avremo ricevuto l\u0027accredito dell\u0027importo relativo al premio, ", font2)
                   def unipar34=new Paragraph()
                   unipar34.add(new Chunk(par3))
                   unipar34.add(new Chunk(par4))
                   document.add(unipar34)
                   document.add( Chunk.NEWLINE )
                   document.add(new Paragraph("Ricordiamo che il mancato accredito, cos\u00ED come il superamento del termine dei 10 giorni dall\u0027immatricolazione (o voltura) per l\u0027addebito del bonifico sul Suo conto, rendono nulla la copertura dall\u0027origine.", font3))
                   document.add( Chunk.NEWLINE )
                   document.add(new Paragraph("Per qualsiasi ulteriore informazione od eventuali altre necessit\u00E1, non esiti a contattarci ai seguenti recapiti:", font3))
                   document.add( Chunk.NEWLINE )
                   def par5=new Chunk("- Telefono:",font1).setBackground(BaseColor.LIGHT_GRAY)
                   def par6=new Chunk("02 30309020  tutti i giorni feriali dalle 9.00 alle 13.00 e dalle 14.00 alle 17.30", font3).setBackground(BaseColor.LIGHT_GRAY)
                   def par7=new Chunk("- E-mail:",font1).setBackground(BaseColor.LIGHT_GRAY)
                   def par8=new Chunk("attivazioni.ofs@mach-1.it",font3).setBackground(BaseColor.LIGHT_GRAY)
                   def par9=new Chunk("- Fax:",font1).setBackground(BaseColor.LIGHT_GRAY)
                   def par10=new Chunk("02 62694254",font3).setBackground(BaseColor.LIGHT_GRAY)
                   def par11=new Chunk("- Posta:",font1).setBackground(BaseColor.LIGHT_GRAY)
                   def par12=new Chunk(" MACH 1 s.r.l. \u002D Via Vittor Pisani, 13 \u002D 20124 Milano",font3).setBackground(BaseColor.LIGHT_GRAY)
                   def uniparindir=new Paragraph()
                   def uniparindir2=new Paragraph()
                   def uniparindir3=new Paragraph()
                   def uniparindir4=new Paragraph()
                   uniparindir.add(new Chunk(par5))
                   uniparindir.add(new Chunk(par6))
                   uniparindir2.add(new Chunk(par7))
                   uniparindir2.add(new Chunk(par8))
                   uniparindir3.add(new Chunk(par9))
                   uniparindir3.add(new Chunk(par10))
                   uniparindir4.add(new Chunk(par11))
                   uniparindir4.add(new Chunk(par12))
                   document.add(uniparindir)
                   document.add(uniparindir2)
                   document.add(uniparindir3)
                   document.add(uniparindir4)
                   //document.add( Chunk.NEWLINE )
                   document.add(new Paragraph(" ", font3))
                   document.add(new Paragraph(" ", font3))
                   document.add(new Paragraph(" ", font3))
                   document.add(new Paragraph("Cordiali saluti.", font3))
                   document.newPage()
                   //println polizze
               }
               streams += baosPDF
               document.close()
               docWriter.close()
               response.contentType = "application/pdf"
               response.addHeader "Content-disposition", "inline; filename=riepilogo_${dataodierna.format("dd-MM-yyyy")}.pdf"
               response.outputStream << mailService.mergePdf(streams).toByteArray()
           }else{
               document.add(new Paragraph("non ci sono polizze generate ieri", font3))
               document.close()
               docWriter.close()
               response.contentType = "application/pdf"
               response.addHeader "Content-disposition", "inline; filename=riepilogo_${dataodierna.format("dd-MM-yyyy")}.pdf"
               response.outputStream << mailService.mergePdf(streams).toByteArray()
           }


       }*/
    /*def elencoPolizzeSettimanale(){
        def dataodierna = new Date()
        dataodierna=dataodierna.clearTime()
        def wb
            def polizze = *//*SqlLogger.log {*//*Polizza.createCriteria().list() {
                gt "dataInserimento", dataodierna-8
                le "dataInserimento", dataodierna-3
                //between "dateCreated",  dataodierna-1, dataodierna
               // eq "dealer", dealer

            }
            wb = Stopwatch.log {
                ExcelBuilder.create {
                    style("header") {
                        background bisque
                        font {
                            bold(true)
                        }
                    }
                    sheet("Polizze cash_leasing_finanziate") {
                        row(style: "header") {
                            cell("Programma")
                            cell("Prodotto")
                            cell("Polizza")
                            cell("N/R")
                            cell("Liv.")
                            cell("Ass.")
                            cell("Prov. Ass.")
                            cell("Tel.")
                            cell("CF")
                            cell("Indirizzo")
                            cell("Modello")
                            cell("Targa")
                            cell("Val. Ass.")
                            cell("Premio")
                            cell("Pacchetto")
                            cell("Zona")
                            cell("Cop.")
                            cell("Scad.")
                            cell("Conc.")
                            cell("CodConc.")
                            cell("Provv. Conc.")
                            cell("Vend.")
                            cell("Provv. Vend.")
                            cell("Stato Pol.")
                            cell("Auto Usata")
                            cell("Account GMAC")
                            cell("Finanziata")
                            cell("On Star")

                        }
                        if (polizze.size() > 0) {
                            polizze.each { polizza ->
                                def durata =polizza.durata
                                def anni="0"
                                if(durata==12){
                                    anni="1"
                                }else if(durata==24){
                                    anni="2"
                                }else if(durata==36){
                                    anni="3"
                                }else if(durata==48){
                                    anni="4"
                                }else if(durata==60){
                                    anni="5"
                                }else if(durata==72){
                                    anni="6"
                                }else if(durata==84){
                                    anni="7"
                                }else if(durata==96){
                                    anni="8"
                                }
                                row {
                                    cell(polizza.tipoPolizza)
                                    cell("Flex Protection")
                                    cell(polizza.noPolizza)
                                    cell("Nuovi")
                                    cell("${anni}")
                                    cell("${polizza.cognome.toString().toUpperCase()} ${polizza.nome.toString().toUpperCase()}")
                                    cell(polizza.provincia.toString().toUpperCase())
                                    cell(polizza.telefono)
                                    cell(polizza.partitaIva.toString().toUpperCase())
                                    cell(polizza.indirizzo.toString().toUpperCase())
                                    cell(polizza.modello.toString().toUpperCase())
                                    cell(polizza.targa.toString().toUpperCase())
                                    cell(polizza.valoreAssicurato)
                                    cell(polizza.premioLordo)
                                    cell(polizza.coperturaRichiesta)
                                    cell(polizza.codiceZonaTerritoriale)
                                    cell(polizza.dataDecorrenza)
                                    cell(polizza.dataScadenza)
                                    cell(polizza.dealer.ragioneSocialeD.toString().toUpperCase())
                                    cell(polizza.dealer.codice)
                                    cell(polizza.provvDealer)
                                    cell(polizza.venditore.toString().toUpperCase())
                                    cell(polizza.provvVenditore)
                                    cell("")
                                    cell(polizza.nuovo? "N":"S")
                                    cell(polizza.provvGmfi)
                                    cell((polizza.tipoPolizza==TipoPolizza.FINANZIATE)?"S":"N")
                                    cell((polizza.onStar)?"S":"N")
                                }
                                polizza.discard()
                            }
                            for (int i = 0; i < 17; i++) {
                                sheet.autoSizeColumn(i);
                            }
                        }
                        else {
                            row {
                                cell("non ci sono polizze attivate")
                            }
                        }

                    }

                }
            }
                def stream = new ByteArrayOutputStream()
                stream.write(wb)
                stream.close()
                response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                response.addHeader "Content-disposition", "inline; filename=output_${new Date().format("ddMMyyyy")}_noRinnovi_salesSpecialist.xlsx"
                response.outputStream << stream.toByteArray()

    }
    def elencoPolizzeSDSLIP(){
        def dataodierna = new Date()
        dataodierna=dataodierna.clearTime()
        def wb
        def polizze = *//*SqlLogger.log {*//*Polizza.createCriteria().list() {
            gt "dataInserimento", dataodierna-8
            le "dataInserimento", dataodierna-3
            //between "dateCreated",  dataodierna-1, dataodierna
            // eq "dealer", dealer
        }
        wb = Stopwatch.log {
            ExcelBuilder.create {
                style("header") {
                    background bisque
                    font {
                        bold(true)
                    }
                }
                sheet("Polizze cash_leasing_finanziate") {
                    row(style: "header") {
                        cell("Programma")
                        cell("Prodotto")
                        cell("Polizza")
                        cell("N/R")
                        cell("Liv.")
                        cell("Ass.")
                        cell("Prov. Ass.")
                        cell("Tel.")
                        cell("CF")
                        cell("Indirizzo")
                        cell("Modello")
                        cell("Targa")
                        cell("Val. Ass.")
                        cell("Premio")
                        cell("Pacchetto")
                        cell("Zona")
                        cell("Cop.")
                        cell("Scad.")
                        cell("Conc.")
                        cell("CodConc.")
                        cell("Provv. Conc.")
                        cell("Vend.")
                        cell("Provv. Vend.")
                        cell("Stato Pol.")
                        cell("Auto Usata")
                        cell("Account GMAC")
                        cell("Finanziata")
                        cell("On Star")

                    }
                    if (polizze.size() > 0) {
                        polizze.each { polizza ->
                            def durata =polizza.durata
                            def anni="0"
                            if(durata==12){
                                anni="1"
                            }else if(durata==24){
                                anni="2"
                            }else if(durata==36){
                                anni="3"
                            }else if(durata==48){
                                anni="4"
                            }else if(durata==60){
                                anni="5"
                            }else if(durata==72){
                                anni="6"
                            }else if(durata==84){
                                anni="7"
                            }else if(durata==96){
                                anni="8"
                            }
                            row {
                                cell(polizza.tipoPolizza)
                                cell("Flex Protection")
                                cell(polizza.noPolizza)
                                cell("Nuovi")
                                cell("${anni}")
                                cell("${polizza.cognome.toString().toUpperCase()} ${polizza.nome.toString().toUpperCase()}")
                                cell(polizza.provincia.toString().toUpperCase())
                                cell(polizza.telefono)
                                cell(polizza.partitaIva.toString().toUpperCase())
                                cell(polizza.indirizzo.toString().toUpperCase())
                                cell(polizza.modello.toString().toUpperCase())
                                cell(polizza.targa.toString().toUpperCase())
                                cell(polizza.valoreAssicurato)
                                cell(polizza.premioLordo)
                                cell(polizza.coperturaRichiesta)
                                cell(polizza.codiceZonaTerritoriale)
                                cell(polizza.dataDecorrenza)
                                cell(polizza.dataScadenza)
                                cell(polizza.dealer.ragioneSocialeD.toString().toUpperCase())
                                cell(polizza.dealer.codice)
                                cell(polizza.provvDealer)
                                cell(polizza.venditore.toString().toUpperCase())
                                cell(polizza.provvVenditore)
                                cell("")
                                cell(polizza.nuovo? "N":"S")
                                cell(polizza.provvGmfi)
                                cell((polizza.tipoPolizza==TipoPolizza.FINANZIATE)?"S":"N")
                                cell((polizza.onStar)?"S":"N")
                            }
                            polizza.discard()
                        }
                        for (int i = 0; i < 17; i++) {
                            sheet.autoSizeColumn(i);
                        }
                    }
                    else {
                        row {
                            cell("non ci sono polizze senza DSLIP")
                        }
                    }

                }

            }
        }
        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        response.addHeader "Content-disposition", "inline; filename=output_${new Date().format("ddMMyyyy")}_polizzesenza DSLIP.xlsx"
        response.outputStream << stream.toByteArray()

    }
    def riepilogoPolizzeSettimanale(){
        def dataodierna = new Date()
        def fine = use(TimeCategory) { "${dataodierna.format("dd-MM-yyyy")}" }
        def inizio = use(TimeCategory) { dataodierna - 5.day }.format("dd/MM/yyyy")
        def document = new Document(PageSize.A4.rotate())
        def baosPDF= new ByteArrayOutputStream()
        PdfWriter docWriter = null
        docWriter = PdfWriter.getInstance(document, baosPDF)
        def font1 = new Font(Font.FontFamily.HELVETICA, 10, Font.BOLD)
        def font2 = new Font(Font.FontFamily.HELVETICA, 6)
        def font4 = new Font(Font.FontFamily.HELVETICA, 6, Font.NORMAL, BaseColor.RED)
        def font3 = new Font(Font.FontFamily.HELVETICA, 10)
        document.open()
        def inizioFormattato = inizio.format("dd MMMM").toString(), fineFormattato = fine.format("dd MMMM yyyy").toString(), inizioformBreve = inizio.format("dd/MM/yyyy"), fineformBreve = fine.format("dd/MM/yyyy"),noappendice=inizio.format("MM/yyyy"),nomefile=inizio.format("yyyy MMMM")
        inizioFormattato = new String(inizioFormattato.toString().substring(0, 4).toUpperCase() + inizioFormattato.toString().substring(4));
        fineFormattato = new String(fineFormattato.toString().substring(0, 4).toUpperCase() + fineFormattato.toString().substring(4));
        def dealers=Polizza.createCriteria().list(){
            gt "dateCreated", dataodierna-5
            le "dateCreated", dataodierna
            eq "coperturaRichiesta", "SILVER"
            //between "dateCreated", dataodierna-1, dataodierna
            projections {
                groupProperty "dealer"
                sum ("premioLordo")
            }
        }
        def streams = []
        dealers.each { dealer->

            def polizze = *//*SqlLogger.log {*//*Polizza.createCriteria().list() {
                gt "dateCreated", dataodierna-5
                le "dateCreated", dataodierna
                //between "dateCreated",  dataodierna-1, dataodierna
                eq "dealer", dealer[0]
                eq "coperturaRichiesta", "SILVER"
                projections {
                   // groupProperty "dealer"
                    property("coperturaRichiesta")
                    property("cognome")
                    property("nome")
                    property("premioLordo")
                    property("noPolizza")
                    property("targa")
                    property("dataDecorrenza")
                    property("dataScadenza")

                }

            }
            def data_da = inizioFormattato
            def data_al = fineFormattato
            def img= Image.getInstance(this.class.getResource("/img/mach1.png"))
            img.scaleAbsolute(70,50)
            if (polizze.size() > 0) {
                float[] columnWidth = [6,8,8,8,18,16]
                def tableIni = new PdfPTable(columnWidth)
                tableIni.setHorizontalAlignment(Element.ALIGN_CENTER)
                tableIni.setWidthPercentage(100)
                tableIni.setSpacingBefore(5)
                tableIni.setSpacingAfter(15)
                // the cell object
                def cellIni
                cellIni=new PdfPCell(img, true)
                cellIni.setBorder(PdfPCell.NO_BORDER)
                tableIni.addCell(cellIni)
                cellIni = new PdfPCell(new Phrase("  ",font3))
                cellIni.setBorder(PdfPCell.NO_BORDER)
                tableIni.addCell(cellIni)
                cellIni = new PdfPCell(new Phrase("  ",font3))
                cellIni.setBorder(PdfPCell.NO_BORDER)
                tableIni.addCell(cellIni)
                cellIni = new PdfPCell(new Phrase("  ",font3))
                cellIni.setBorder(PdfPCell.NO_BORDER)
                tableIni.addCell(cellIni)
                cellIni = new PdfPCell(new Phrase("  ",font3))
                cellIni.setBorder(PdfPCell.NO_BORDER)
                tableIni.addCell(cellIni)
                def ragSocDealer=new Chunk("Spett.le ${dealer[0].ragioneSocialeD}",font3)
                def dirDealer=new Chunk("${dealer[0].indirizzoD}",font3)
                def capLocDealer=new Chunk("${dealer[0].capD} ${dealer[0].localitaD} ${dealer[0].provinciaD}",font3)
                def datiDealer= new Paragraph()
                datiDealer.add(new Chunk(ragSocDealer))
                datiDealer.add(Chunk.NEWLINE)
                datiDealer.add(new Chunk(dirDealer))
                datiDealer.add(Chunk.NEWLINE)
                datiDealer.add(new Chunk(capLocDealer))
                cellIni = new PdfPCell(datiDealer)
                cellIni.setBorder(PdfPCell.NO_BORDER)
                tableIni.addCell(cellIni)
                document.add(tableIni)
                document.add( Chunk.NEWLINE )
                document.add( Chunk.NEWLINE )
                document.add( new Paragraph( "Con la presente, si riepilogano le posizioni attivate nella settimana dal ${inizio} al ${fine}, relative al prodotto Polizza Silver :",font3 ) )
                document.add( Chunk.NEWLINE )
                document.add( Chunk.NEWLINE )
                float[] columnWidths = [6,6,4,4,4,4,4]
                def table = new PdfPTable(columnWidths)
                table.setHorizontalAlignment(Element.ALIGN_CENTER)
                table.setWidthPercentage(100)
                // the cell object
                def cell
                cell = new PdfPCell(new Phrase("Prodotto", font1))
                table.addCell(cell)
                cell = new PdfPCell(new Phrase("Assicurato", font1))
                table.addCell(cell)
                cell = new PdfPCell(new Phrase("Premio\n Lordo", font1))
                table.addCell(cell)
                cell = new PdfPCell(new Phrase("Num. Polizza", font1))
                table.addCell(cell)
                cell = new PdfPCell(new Phrase("Targa", font1))
                table.addCell(cell)
                cell = new PdfPCell(new Phrase("Data\n Copertura", font1))
                table.addCell(cell)
                cell = new PdfPCell(new Phrase("Data\n Scadenza", font1))
                table.addCell(cell)
                polizze.each { polizza ->
                   table.addCell(new PdfPCell(new Phrase("${polizza[0]}", font3)))
                    table.addCell(new PdfPCell(new Phrase("${polizza[1]} ${polizza[2]}", font3)))
                    table.addCell(new PdfPCell(new Phrase("${polizza[3]}", font3)))
                    table.addCell(new PdfPCell(new Phrase("${polizza[4]}", font3)))
                    table.addCell(new PdfPCell(new Phrase("${polizza[5]?:""}", font3)))
                    table.addCell(new PdfPCell(new Phrase("${polizza[6]?.format("dd-MM-yyyy")}", font3)))
                    table.addCell(new PdfPCell(new Phrase("${polizza[7]?.format("dd-MM-yyyy")}", font3)))
                }
                cell = new PdfPCell(new Phrase("", font1))
                cell.setBorder(PdfPCell.NO_BORDER)
                table.addCell(cell)
                cell = new PdfPCell(new Phrase("IMPORTO BONIFICO", font1))
                table.addCell(cell)
                cell = new PdfPCell(new Phrase("${dealer[1]}", font3))
                table.addCell(cell)
                cell = new PdfPCell(new Phrase("", font1))
                cell.setBorder(PdfPCell.NO_BORDER)
                table.addCell(cell)
                cell = new PdfPCell(new Phrase("", font1))
                cell.setBorder(PdfPCell.NO_BORDER)
                table.addCell(cell)
                cell = new PdfPCell(new Phrase("", font1))
                cell.setBorder(PdfPCell.NO_BORDER)
                table.addCell(cell)
                cell = new PdfPCell(new Phrase("", font1))
                cell.setBorder(PdfPCell.NO_BORDER)
                table.addCell(cell)
                document.add(table)
                document.add( Chunk.NEWLINE )
                document.add(new Paragraph("Milano, ${dataodierna.format("dd/MM/yyyy")} ", font3))
                document.add( Chunk.NEWLINE )
                document.add(new Paragraph("MACH1 s.r.l.", font3))
                document.add(new Paragraph("Tel. 02 30465068", font3))
                document.add(new Paragraph("e-mail: attivazionigmf@mach-1.it",font3))
                document.add( Chunk.NEWLINE )
                document.add( Chunk.NEWLINE )
                document.add( Chunk.NEWLINE )
                document.add( Chunk.NEWLINE )
                document.add( Chunk.NEWLINE )
                document.add( Chunk.NEWLINE )
                document.add( Chunk.NEWLINE )
                document.add( Chunk.NEWLINE )
                document.add( Chunk.NEWLINE )
                document.add( Chunk.NEWLINE )
                def par1=new Paragraph("MACH 1 s.r.l.",font4)
                par1.setAlignment(Element.ALIGN_CENTER)
                document.add(par1)
                def par2=new Paragraph("Via Vittor Pisani, 13 \u002D 20124 Milano \u002D TEL. 02 30465068 \u002D FAX 02 62694254 \u002D E-mail PEC: mach1@registerpec.it \u002D",font2)
                par2.setAlignment(Element.ALIGN_CENTER)
                document.add(par2)
                def par3=new Paragraph("CCIAA Milano - REA MI 1908726C.F., \u002D  P. IVA e Reg. Imprese Milano 06680830962 \u002D Capitale sociale 100.000 EURO",font2)
                par3.setAlignment(Element.ALIGN_CENTER)
                document.add(par3)
                def par4=new Paragraph("Registro Unico Intermediari  A000317603 \u002D Banco Posta Sede di Milano, Piazza Cordusio, 1 \u002D IBAN IT 54 C 07601 01600 0000 99701393",font2)
                par4.setAlignment(Element.ALIGN_CENTER)
                document.add(par4)
                document.newPage()
            }
        }
        streams += baosPDF
        document.close()
        docWriter.close()
        response.contentType = "application/pdf"
        response.addHeader "Content-disposition", "inline; filename=riepilogo_${dataodierna.format("dd-MM-yyyy")}.pdf"
        response.outputStream << mergePdf(streams).toByteArray()

        //def polizze = *//*SqlLogger.log {*//*Polizza.createCriteria().list() {
        //  between "dateCreated", dataodierna-1, dataodierna
        //}*//*

        //creare il pdf



    }*/
    /*def elencoFlussiIassicurRecede() {
        def traccIassicur = *//*SqlLogger.log {*//*Tracciato.createCriteria().list() {
            isNull ("dataCaricamento")
            eq "annullata", false
            eq "tipoPolizza",TipoPolizza.RINNOVI
        }
        def data=new Date()
        if(traccIassicur) {
            byte[] b = traccIassicur.tracciato.join("\n").getBytes()
            response.contentType = "text/plain"
            response.addHeader "Content-disposition", "attachment; filename=flussi_RECEDUTI_${data.format("yyyyMMdd")}.txt"
            response.outputStream << b
        } //else response.sendError(404)
        else{
            flash.error = "Non ci sono flussi di recessi da mostrare"
            redirect action: "recederePolizze"
            return
        }
    }*/
    /* def elencoFlussiDLRecede() {
       def traccDL = *//*SqlLogger.log {*//*TracciatoCompagnia.createCriteria().list() {
           isNull ("dataCaricamento")
           eq "annullata", false
           eq "tipoPolizza",TipoPolizza.RINNOVI
       }
       def data=new Date()
       if(traccDL) {
           byte[] b = traccDL.tracciatoCompagnia.join("\n").getBytes()
           response.contentType = "text/plain"
           response.addHeader "Content-disposition", "attachment; filename=flussiDL_RECEDUTI_${data.format("yyyyMMdd")}.txt"
           response.outputStream << b
       } //else response.sendError(404)
       else{
           flash.error = "Non ci sono flussi DL di recessi da mostrare"
           redirect action: "recederePolizze"
           return
       }
   }*/
    /*def mergePdf(streams) {
       def pdf = new Document()
       def output = new ByteArrayOutputStream()
       def pdfWriter = PdfWriter.getInstance(pdf, output)
       pdf.open()
       def directContent = pdfWriter.getDirectContent()
       streams.each { stream ->
           def pdfReader = new PdfReader(stream.toByteArray())
           for(def i = 1; i <= pdfReader.getNumberOfPages(); i++) {
               pdf.newPage()
               def page = pdfWriter.getImportedPage(pdfReader, i)
               directContent.addTemplate(page, 0, 0)
           }
       }
       output.flush()
       pdf.close()
       output.close()
       return output
   }*/
    /*static def postText(String baseUrl, String path, Map query, method = Method.GET) {
       def logg
       try {
           def ret = null
           def http = new HTTPBuilder(baseUrl)
           // perform a POST request, expecting TEXT response
           http.request(method, ContentType.TEXT) {
               uri.path = path
               uri.query = query
               //headers.'User-Agent' = 'Mozilla/5.0 Ubuntu/8.10 Firefox/3.0.4'
               // response handler for a success response code
               response.success = { resp, reader ->
                   logg =new Log(parametri: "response status: ${resp.statusLine}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizze")
                   if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                   println "response status: ${resp.statusLine}"
                   //println 'Headers: -----------'
                   resp.headers.each { h ->
                       // println " ${h.name} : ${h.value}"
                   }
                   ret = reader.getText()
                   *//*println 'Response data: -----'
                   println ret
                   println '--------------------'*//*
               }
           }
           return ret
       } catch (groovyx.net.http.HttpResponseException ex) {
           ex.printStackTrace()
           return null
       } catch (java.net.ConnectException ex) {
           ex.printStackTrace()
           return null
       }
   }*/
    /*codice vecchio*/
    def caricaPraticaCSVOLD() {
        def logg
        if (request.post) {
            def speciale=params.speciale.toBoolean()
            def praticheNuove=[], errorePratica=[], flussiPAI=[]
            def dealerMancante=[]
            def file =params.excelPratica
            def filename=""
            def delimiter = ";"
            def fileReader = null
            def reader = null
            def erroreWS=""
            def dataPAIFin=new Date().parse("yyyyMMdd","20160930")
            def dataPAIIni=new Date().parse("yyyyMMdd","20160701")
            def listError =""
            def listPratiche=""
            def listFlussiPAI=""
            def listDeleted=""
            def totale=0
            def totaleErr=0
            boolean isPai=false
            if(file.bytes) {
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                reader = new InputStreamReader(myInputStream)
                fileReader = new BufferedReader(reader)
                fileReader.readLine()
                def risposta =[]
                def polizzeDeleted =[]
                Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$)/
                Pattern pattern = ~/(\w+)/
                String regex = "<(\\S+)[^>]+?mso-[^>]*>.*?</\\1>"
                def x_value=[]
                def dealer
                def dealers=[]
                try {
                    def line = ""
                    while ((line = fileReader.readLine()) != null) {
                        boolean sup75=false
                        boolean isDeleted=false
                        def tokens = []
                        tokens = line.split(delimiter)
                        def cellType = tokens[0].toString().trim().replaceAll ("'", "")
                        def cellnoPratica = tokens[1].toString().trim().replaceAll ("'", "")
                        def cellDataDecorrenza = tokens[2].toString().trim().replaceAll ("'", "")
                        def cellaDealer =tokens[3].toString().trim().replaceAll ("'", "")
                        def cellaNomeDealer =tokens[4].toString().trim().replaceAll ("'", "")
                        def cellaCognome  = tokens[5].toString().trim().replaceAll ("'", "")
                        def cellaNome  = tokens[6].toString().trim().replaceAll ("'", "")
                        def cellaBeneficiario=tokens[61].toString().trim().replaceAll ("'", "")
                        def cellaCF = tokens[7].toString().trim().replaceAll ("'", "")
                        def cellaTel = tokens[8].toString().trim().replaceAll ("'", "")
                        def cellaCel = tokens[9].toString().trim().replaceAll ("'", "")
                        def cellaLocalita = tokens[10].toString().trim().replaceAll ("'", "")
                        def cellaIndirizzo = tokens[11].toString().trim().replaceAll ("'", "")
                        def cellaProvincia = tokens[12].toString().trim().replaceAll ("'", "")
                        def cellaCap = tokens[13].toString().trim().replaceAll ("'", "")
                        def cellaPIva= tokens[15].toString().trim().replaceAll ("'", "")
                        def cellaEmail = tokens[16].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoCognome  = tokens[17].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoNome  = tokens[18].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoCF  = tokens[19].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoTEL  = tokens[20].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoCEL  = tokens[21].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoLOC  = tokens[22].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoIND  = tokens[23].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoPROV  = tokens[24].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoCAP  = tokens[25].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoemail  = tokens[28].toString().trim().replaceAll ("'", "")
                        def cellaDenominazione= tokens[29].toString().trim().replaceAll ("'", "")
                        def cellaTelefonoBusiness= tokens[31].toString().trim().replaceAll ("'", "")
                        def cellaIndirizzoBusiness= tokens[32].toString().trim().replaceAll ("'", "")
                        def cellaCellulareBusiness= tokens[33].toString().trim().replaceAll ("'", "")
                        def cellaProvinciaBusiness= tokens[34].toString().trim().replaceAll ("'", "")
                        def cellaLocalitaBusiness= tokens[22].toString().trim().replaceAll ("'", "")
                        def cellaCapBusiness= tokens[35].toString().trim().replaceAll ("'", "")
                        def cellaPIvaBusiness= tokens[36].toString().trim().replaceAll ("'", "")
                        def cellaCFBusiness= tokens[37].toString().trim().replaceAll ("'", "")
                        def cellaEmailBusiness= tokens[38].toString().trim().replaceAll ("'", "")
                        def cellaBeneFinanziato= tokens[39].toString().trim().replaceAll ("'", "")
                        def cellaDataImmatr= tokens[40].toString().trim().replaceAll ("'", "")
                        def cellaMarca= tokens[41].toString().trim().replaceAll ("'", "")
                        def cellaModello= tokens[42].toString().trim().replaceAll ("'", "")
                        def cellaVersione= tokens[43].toString().trim().replaceAll ("'", "")
                        def cellaTelaio= tokens[45].toString().trim().replaceAll ("'", "")
                        def cellaTarga= tokens[46].toString().trim().replaceAll ("'", "")
                        def cellaPrezzo= tokens[47].toString().trim().replaceAll ("'", "")
                        def cellaStep= tokens[49].toString().trim().replaceAll ("'", "")
                        def cellaProdotto= tokens[51].toString().trim().replaceAll ("'", "")
                        def cellaDurata= tokens[52].toString().trim().replaceAll ("'", "")
                        def cellaValoreAssi= tokens[53].toString().trim().replaceAll ("'", "")
                        def cellaPacchetto= tokens[54].toString().trim().replaceAll ("'", "")
                        def cellaPremioImpo= tokens[56].toString().trim().replaceAll ("'", "")
                        def cellaPremioLordo= tokens[57].toString().trim().replaceAll ("'", "")
                        def cellaProvvDealer= tokens[58].toString().trim().replaceAll ("'", "")
                        def cellaProvvVenditore= tokens[59].toString().trim().replaceAll ("'", "")
                        def cellaProvvGMFI= tokens[60].toString().trim().replaceAll ("'", "")
                        def cellaCertificato= tokens[62].toString().trim().replaceAll ("'", "")
                        def cellaVenditore= tokens[65].toString().trim().replaceAll ("'", "")
                        def cellatipoPolizza=tokens[68].toString().trim().replaceAll ("'", "")
                        def cellacodCampagna=tokens[69].toString().trim().replaceAll ("'", "")
                        def dataDecorrenza=null
                        if(cellDataDecorrenza.toString().trim()!='null' && cellDataDecorrenza.toString().length()>0){
                            def newDate = Date.parse( 'yyyyMMdd', cellDataDecorrenza )
                            dataDecorrenza = newDate
                            if((dataDecorrenza) && (dataDecorrenza>=dataPAIIni && dataDecorrenza<=dataPAIFin)){
                                isPai=true
                            }else{
                                isPai=false
                            }
                        }
                        if(cellnoPratica.toString().equals("null")){
                            cellnoPratica=""
                        }else{
                            cellnoPratica=cellnoPratica.toString()
                            cellnoPratica=cellnoPratica.toString().replaceFirst ("^0*", "")
                            def indexslash=cellnoPratica.indexOf("/")
                            if(indexslash>0){
                                cellnoPratica=cellnoPratica.toString().substring(0, indexslash)
                            }
                            if(cellnoPratica.toString().contains(".")){
                                def punto=cellnoPratica.toString().indexOf(".")
                                cellnoPratica=cellnoPratica.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                        }
                        def codOperazione="0"
                        if(cellType.toString().trim()!='null'){
                            if(cellType.toString().trim().equalsIgnoreCase("NEW")){
                                codOperazione="0"
                            }else if (cellType.toString().trim().equalsIgnoreCase("DELETED")){
                                isDeleted=true
                            }else if (cellType.toString().trim().equalsIgnoreCase("CLOSED")){
                                codOperazione="0"
                            }
                            else{
                                codOperazione="S"
                            }
                        }
                        if (isDeleted && cellnoPratica!=""){
                            polizzeDeleted.add( "${cellnoPratica},")
                            logg =new Log(parametri: "Errore creazione polizza: la polizza e' tipo DELETED", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            risposta.add("Errore creazione polizza: la polizza ${cellnoPratica} e' tipo DELETED,")
                            errorePratica.add("Errore creazione polizza: la polizza ${cellnoPratica} e' tipo DELETED,")
                            totaleErr++
                        }else if(!cellnoPratica){
                            logg =new Log(parametri: "Errore creazione polizza: non \u00E8 presente un numero di pratica", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            risposta.add("Errore creazione polizza: non \u00E8 presente un numero di pratica,")
                            errorePratica.add("Errore creazione polizza: non \u00E8 presente un numero di polizza,")
                            totaleErr++
                        }
                        else{
                            if(cellaNome.toString().equals("null")){
                                cellaNome="";
                            }else{
                                cellaNome=cellaNome.toString().trim()
                            }
                            if(cellaCognome.toString().equals("null")){
                                cellaCognome="";
                            }else{
                                cellaCognome=cellaCognome.toString().trim()
                            }
                            if(cellaCap.toString().equals("null")){
                                cellaCap="";
                            }else{
                                cellaCap=cellaCap.toString().replaceAll("'", "")
                                if(cellaCap.toString().contains(".")){
                                    def punto=cellaCap.toString().indexOf(".")
                                    cellaCap=cellaCap.toString().substring(0,punto)
                                }
                            }
                            if(cellaDealer.toString().equals("null")){
                                cellaDealer="";
                            }else{
                                cellaDealer=cellaDealer.toString().replaceAll ("'", "")
                                cellaDealer=cellaDealer.toString().replaceFirst ("^0*", "")
                                if(cellaDealer.toString().contains(".")){
                                    cellaDealer=Double.valueOf(cellaDealer).longValue().toString()
                                    //cellaDealer=cellaDealer.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                }
                                dealer=Dealer.findByCodice(cellaDealer.toString())
                            }
                            if(cellaCF.toString().equals("null") ){
                                cellaCF=cellaPIva.toString().replaceAll("[^a-z,A-Z,0-9]","")
                            }else{
                                cellaCF=cellaCF.toString().replaceAll("[^a-z,A-Z,0-9]","")
                            }
                            if(cellaMarca.toString().equals("null")){
                                cellaMarca=null
                            }else{
                                cellaMarca=cellaMarca.toString().trim().toUpperCase()
                            }
                            if(cellaModello.toString().equals("null")){
                                cellaModello=null
                            }else{
                                cellaModello=cellaModello.toString().trim().toUpperCase()
                            }
                            if(cellaTel.toString().trim()!='null'){
                                cellaTel=cellaTel.toString().trim()
                                if(cellaTel.contains(".")){
                                    int zero=cellaTel.toString().indexOf(".")
                                    cellaTel=cellaTel.toString().substring(0,zero)
                                }
                            }else{cellaTel=null}
                            if(cellaTelefonoBusiness.toString().trim()!='null'){
                                cellaTelefonoBusiness=cellaTelefonoBusiness.toString().trim()
                                if(cellaTelefonoBusiness.contains(".")){
                                    int zero=cellaTelefonoBusiness.toString().indexOf(".")
                                    cellaTelefonoBusiness=cellaTelefonoBusiness.toString().substring(0,zero)
                                }

                            }else{cellaTelefonoBusiness=null}
                            if(cellaCel.toString().trim()!='null'){
                                cellaCel=cellaCel.toString().trim().replaceAll("[^0-9]+","")
                                if(cellaCel.toString().contains(".")){
                                    int zero=cellaCel.toString().indexOf(".")
                                    cellaCel=cellaCel.toString().substring(0,zero)
                                }
                            }else{
                                cellaCel=null
                            }
                            if(cellaCellulareBusiness.toString().trim()!='null'){
                                cellaCellulareBusiness=cellaCellulareBusiness.toString().trim().replaceAll("[^0-9]+","")
                                if(cellaCellulareBusiness.toString().contains(".")){
                                    int zero=cellaCellulareBusiness.toString().indexOf(".")
                                    cellaCellulareBusiness=cellaCellulareBusiness.toString().substring(0,zero)
                                }
                            }else{
                                cellaCellulareBusiness=null
                            }
                            def coperturaR
                            if(cellaProdotto.toString().trim()!='null' && cellaPacchetto.toString().trim().length()>0){
                                def pacchetto=""
                                def indexdue=cellaProdotto.toString().trim().indexOf("-")
                                pacchetto=cellaProdotto.toString().trim().substring(0,indexdue)
                                pacchetto=pacchetto.toString().trim()
                                if(pacchetto=="A"){
                                    coperturaR="SILVER"
                                }else if (pacchetto=="B"){
                                    coperturaR="GOLD"
                                }else if(pacchetto=="C"){
                                    coperturaR="PLATINUM"
                                }else if(pacchetto=="D"){
                                    coperturaR="PLATINUM COLLISIONE"
                                }else if(pacchetto=="E"){
                                    coperturaR="PLATINUM KASKO"
                                }
                            }
                            def nuovo=false
                            if(cellaBeneFinanziato.toString().trim()!='null'){
                                if(cellaBeneFinanziato.toString().trim().equalsIgnoreCase("NEW")){
                                    nuovo=true
                                }else{
                                    nuovo=false
                                }
                            }
                            def polizzaTipo=""
                            if(cellatipoPolizza.toString().trim()!='null'){
                                if(cellatipoPolizza.toString().trim().equalsIgnoreCase("RETAIL")){
                                    polizzaTipo=TipoPolizza.FINANZIATE
                                }else{
                                    polizzaTipo=TipoPolizza.LEASING
                                }
                            }
                            def codCampagna=""
                            if(cellacodCampagna.toString().trim()!='null'){
                                if(cellacodCampagna.toString().trim().equalsIgnoreCase("0")){
                                    codCampagna="0"
                                }else{
                                    codCampagna=cellacodCampagna.toString().trim()
                                }
                            }
                            boolean onStar=false
                            boolean certificato=false
                            if(cellaCertificato.toString().trim()!=''){
                                if(cellaCertificato.toString().trim()!="N"){
                                    certificato=true
                                }else{
                                    certificato=false
                                }
                            }
                            if(cellaTelaio.toString().trim()!=''){
                                cellaTelaio=cellaTelaio.toString().trim().toUpperCase()
                            }else{
                                cellaTelaio=null
                            }
                            if(cellaTarga.toString().trim()!=''){
                                cellaTarga=cellaTarga.toString().trim().toUpperCase()
                            }else{
                                cellaTarga=null
                            }
                            if(cellaEmail.toString().trim()!=''){
                                cellaEmail=cellaEmail.toString().trim()
                            }else{
                                cellaEmail=null
                            }
                            def dataScadenza
                            def durata
                            if(cellaDurata.toString().trim()!='null' && cellaDurata.toString().trim().length()>0){
                                if(cellaDurata.toString().contains(".")){
                                    def punto=cellaDurata.toString().indexOf(".")
                                    cellaDurata=cellaDurata.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                }
                                durata=Integer.parseInt(cellaDurata.toString().trim())
                            }else{
                                durata=null
                            }
                            if(durata!=null && dataDecorrenza!=null){

                                if(durata==12){
                                    dataScadenza= use(TimeCategory) {dataDecorrenza +12.months}
                                }else if( durata==18){
                                    dataScadenza= use(TimeCategory) {dataDecorrenza +18.months}
                                }else if( durata==24){
                                    dataScadenza= use(TimeCategory) {dataDecorrenza +24.months}
                                }else if( durata==30){
                                    dataScadenza= use(TimeCategory) {dataDecorrenza +30.months}
                                }else if(durata==36){
                                    dataScadenza=use(TimeCategory) {dataDecorrenza +36.months}
                                }else if( durata==42){
                                    dataScadenza= use(TimeCategory) {dataDecorrenza +42.months}
                                }else if(durata==48){
                                    dataScadenza= use(TimeCategory) {dataDecorrenza +48.months}
                                }else if( durata==54){
                                    dataScadenza= use(TimeCategory) {dataDecorrenza +54.months}
                                }else if(durata==60){
                                    dataScadenza=use(TimeCategory) {dataDecorrenza +60.months}
                                }else if(durata==66){
                                    dataScadenza=use(TimeCategory) {dataDecorrenza +66.months}
                                }else if(durata==72){
                                    dataScadenza=use(TimeCategory) {dataDecorrenza +72.months}
                                }else{
                                    dataScadenza=null
                                    durata=null
                                }
                            }
                            def step, codStep
                            if(cellaStep.toString().trim()!='null'){
                                step=cellaStep.toString().trim()
                                if(step.contains(".")){
                                    int zero=step.toString().indexOf(".")
                                    step=step.toString().substring(0,zero).toLowerCase()
                                }
                                if(step=="1"){ codStep="step 1"}
                                else if (step=="2"){codStep="step 2"}
                                else if (step=="3"){codStep="step 3"}
                            }else{codStep=null}
                            def dataImmatricolazione=null
                            if(cellaDataImmatr.toString().trim()!='null' && cellaDataImmatr.toString().length()>0){
                                def newDate = Date.parse( 'yyyyMMdd', cellaDataImmatr )
                                dataImmatricolazione = newDate
                            }
                            def valoreAssicurato
                            if(cellaValoreAssi.toString().trim()!='null' && cellaValoreAssi.toString().length()>0){
                                valoreAssicurato=cellaValoreAssi.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                //valoreAssicurato=cellaValoreAssi.toString().replaceAll(",","\\.")
                                valoreAssicurato = new BigDecimal(valoreAssicurato)
                            }else{
                                valoreAssicurato=0.0
                            }
                            def prezzoVendita
                            if(cellaPrezzo.toString().trim()!='null' && cellaPrezzo.toString().length()>0){
                                prezzoVendita=cellaPrezzo.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                //prezzoVendita=prezzoVendita.toString().replaceAll(",","\\.")
                                prezzoVendita = new BigDecimal(prezzoVendita)
                            }else{
                                prezzoVendita=0.0
                            }
                            def premioImponibile
                            if(cellaPremioImpo.toString().trim()!='null' && cellaPremioImpo.toString().trim().length()>0){
                                premioImponibile=cellaPremioImpo.toString().trim().replaceAll("\\.","").replaceAll(",","\\.")
                                //premioImponibile=cellaPremioImpo.toString().trim().replaceAll(",","\\.")
                                premioImponibile = new BigDecimal(premioImponibile)
                            }else{
                                premioImponibile=0.0
                            }
                            def premioLordo
                            if(cellaPremioLordo.toString().trim()!='null' && cellaPremioLordo.toString().trim().length()>0){
                                premioLordo=cellaPremioLordo.toString().trim().replaceAll("\\.","").replaceAll(",","\\.")
                                // premioLordo=cellaPremioLordo.toString().trim().replaceAll(",","\\.")
                                premioLordo = new BigDecimal(premioLordo)
                            }else{
                                premioLordo=0.0
                            }
                            def provvDealer
                            if(cellaProvvDealer.toString().trim()!='null' && cellaProvvDealer.toString().trim().length()>0){
                                provvDealer=cellaProvvDealer.toString().trim().replaceAll("\\.","").replaceAll(",","\\.")
                                //provvDealer=cellaProvvDealer.toString().trim().replaceAll(",","\\.")
                                provvDealer = new BigDecimal(provvDealer)
                            }else{
                                provvDealer=0.0
                            }
                            def provvGmfi
                            if(cellaProvvGMFI.toString().trim()!='null' && cellaProvvGMFI.toString().trim().length()>0){
                                provvGmfi=cellaProvvGMFI.toString().trim().replaceAll("\\.","").replaceAll(",",".")
                                //provvGmfi=cellaProvvGMFI.toString().trim().replaceAll(",",".")
                                provvGmfi = new BigDecimal(provvGmfi)
                            }else{
                                provvGmfi=0.0
                            }
                            def provvVendi
                            if(cellaProvvVenditore.toString().trim()!='' && cellaProvvVenditore.toString().trim().length()>0 && cellaProvvVenditore.toString().trim()!='0'){
                                provvVendi=cellaProvvVenditore.toString().trim().replaceAll("\\.","").replaceAll(",","\\.")
                                //provvVendi=cellaProvvVenditore.toString().trim().replaceAll(",","\\.")
                                provvVendi=new BigDecimal(provvVendi)
                            }else{
                                provvVendi=0.0
                            }
                            if(cellaVenditore.toString()!='null'){
                                cellaVenditore=cellaVenditore.toString().trim()
                            }else{
                                cellaVenditore=""
                            }
                            def dataInserimento=new Date()
                            def premioLordoSAntifurto=""
                            def premioLordoAntifurto=""
                            dataInserimento=dataInserimento.clearTime()
                            def parole=[]
                            def nomeB="", cognomeB=""
                            def cognomeDef,nomeDef,provinciaDef,localitaDef,capDef,telDef,cellDef,cfDef,indirizzoDef,tipoClienteDef,annoCF
                            def emailDef=""
                            boolean denominazione=false
                            boolean coobligato=false
                            boolean cliente=false
                            boolean nonTrovato=false
                            if(cellaBeneficiario.toString()!=''){
                                cellaBeneficiario=cellaBeneficiario.toUpperCase()
                                logg =new Log(parametri: "beneficiario-->${cellaBeneficiario}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                if(cellaDenominazione) {
                                    cellaDenominazione=cellaDenominazione.toUpperCase().trim()
                                    if (cellaDenominazione.contains(cellaBeneficiario)) {
                                        logg =new Log(parametri: "denominazione-->${cellaDenominazione}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        denominazione = true

                                    } else {
                                        denominazione = false
                                    }
                                }
                                if(cellaCoobligatoNome){
                                    cellaCoobligatoNome=cellaCoobligatoNome.toUpperCase().trim()
                                    cellaCoobligatoCognome=cellaCoobligatoCognome.toUpperCase().trim()
                                    def nomeCooC=cellaCoobligatoNome+" "+cellaCoobligatoCognome
                                    if(nomeCooC.contains(cellaBeneficiario)){
                                        logg =new Log(parametri: "nome Coobligato-->${nomeCooC}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        coobligato=true
                                    }else{
                                        coobligato = false
                                    }
                                }
                                if(cellaNome){
                                    cellaNome=cellaNome.toUpperCase().trim()
                                    cellaCognome=cellaCognome.toUpperCase().trim()
                                    def nomeCom=cellaNome+" "+cellaCognome
                                    if(nomeCom.contains(cellaBeneficiario)){
                                        logg =new Log(parametri: "nome Cliente-->${nomeCom}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        cliente=true
                                    }else{
                                        cliente = false
                                    }
                                }

                                if(coobligato){
                                    logg =new Log(parametri: "prendo i dati del Coobligato-->${cellaCoobligatoNome}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    cognomeDef=cellaCoobligatoCognome.toString().trim().toUpperCase()
                                    nomeDef=cellaCoobligatoNome.toString().trim().toUpperCase()
                                    provinciaDef=cellaCoobligatoPROV.toString().trim().toUpperCase()
                                    localitaDef=cellaCoobligatoLOC.toString().trim().toUpperCase()
                                    capDef=cellaCoobligatoCAP.toString().trim().toUpperCase()
                                    telDef=cellaCoobligatoTEL.toString().trim().toUpperCase()
                                    cellDef=cellaCoobligatoCEL.toString().trim().toUpperCase()
                                    cfDef=cellaCoobligatoCF.toString().trim().toUpperCase()
                                    //prendo i dati dell'anno dal codice fiscale
                                    if(cfDef){
                                        annoCF=cfDef.substring(6,8)
                                        if(Integer.parseInt(annoCF) && Integer.parseInt(annoCF)<=40){
                                            sup75=true
                                        }
                                    }
                                    indirizzoDef=cellaCoobligatoIND.toString().trim().toUpperCase()
                                    tipoClienteDef=TipoCliente.M
                                    if(cellaCoobligatoemail.toString().trim().size()>4){
                                        emailDef=cellaCoobligatoemail.toString().trim()
                                    }
                                }else if(denominazione){
                                    logg =new Log(parametri: "prendo i dati della denominazione -->${cellaDenominazione}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    cognomeDef=cellaDenominazione.toString().trim().toUpperCase()
                                    nomeDef=""
                                    provinciaDef=cellaProvinciaBusiness.toString().trim().toUpperCase()
                                    localitaDef=cellaLocalitaBusiness.toString().trim().toUpperCase()
                                    capDef=cellaCapBusiness.toString().trim().toUpperCase()
                                    telDef=cellaTelefonoBusiness.toString().trim().toUpperCase()
                                    cellDef=cellaCellulareBusiness.toString().trim().toUpperCase()
                                    if(cellaPIvaBusiness){
                                        cfDef=cellaPIvaBusiness.toString().trim().toUpperCase()
                                        if(!cfDef.matches("[0-9]{11}")){
                                            cfDef=cfDef.padLeft(11,"0")
                                        }
                                    }


                                    indirizzoDef=cellaIndirizzoBusiness.toString().trim().toUpperCase()
                                    tipoClienteDef=TipoCliente.DITTA_INDIVIDUALE
                                    if(cellaEmailBusiness.toString().trim().size()>4){
                                        emailDef=cellaEmailBusiness.toString().trim()
                                    }
                                }else if(cliente){
                                    logg =new Log(parametri: "prendo i dati del cliente -->${cellaNome}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    cognomeDef=cellaCognome.toString().trim().toUpperCase()
                                    nomeDef=cellaNome.toString().trim().toUpperCase()
                                    provinciaDef=cellaProvincia.toString().trim().toUpperCase()
                                    localitaDef=cellaLocalita.toString().trim().toUpperCase()
                                    capDef=cellaCap.toString().trim().toUpperCase()
                                    telDef=cellaTel.toString().trim().toUpperCase()
                                    cellDef=cellaCel.toString().trim().toUpperCase()
                                    cfDef=cellaCF.toString().trim().toUpperCase()
                                    if(cfDef){
                                        annoCF=cfDef.substring(6,8)
                                        if(Integer.parseInt(annoCF) && Integer.parseInt(annoCF)<=40){
                                            sup75=true
                                        }
                                    }
                                    indirizzoDef=cellaIndirizzo.toString().trim().toUpperCase()
                                    tipoClienteDef=TipoCliente.M
                                    if(cellaEmail.toString().trim().size()>4){
                                        emailDef=cellaEmail.toString().trim()
                                    }

                                }else{
                                    nonTrovato=true
                                }
                            }
                            if(nonTrovato){
                                logg =new Log(parametri: "il beneficiario non appare nelle colonne dei dati coobligato/cliente/business, controllare file", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                errorePratica.add("per la pratica-->${cellnoPratica} il beneficiario non appare nelle colonne dei dati coobligato/cliente/business, controllare file,")
                                totaleErr++
                            }else{
                                //verifica tot provv rispetto a step assegnato
                                def provincia=provinciaDef.toString().trim()
                                def calcoloImp, totProvvi, diffImpoProvv
                                totProvvi=provvDealer+provvVendi+provvGmfi
                                if((provincia.trim()!="") && (provincia.trim() !=null) && (provincia.trim() !='null')){
                                    def zonaT=ZoneTarif.findByProvincia(provincia).zona
                                    if(zonaT!="8"){
                                        if(step=="1"){
                                            calcoloImp= premioImponibile*0.30
                                        }else if(step=="2"){
                                            calcoloImp= premioImponibile*0.40
                                        }else if(step=="3"){
                                            calcoloImp= premioImponibile*0.50
                                        }
                                    }else{
                                        if(step=="1"){
                                            calcoloImp= premioImponibile*0.27
                                        }else if(step=="2"){
                                            calcoloImp= premioImponibile*0.37
                                        }else if(step=="3"){
                                            calcoloImp= premioImponibile*0.47
                                        }
                                    }
                                    diffImpoProvv=Math.abs(Math.round((calcoloImp-totProvvi)*100)/100)
                                    logg =new Log(parametri: "differenza imponibile provvigioni -> ${diffImpoProvv}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    if(diffImpoProvv<=0.05 && diffImpoProvv!=""){
                                        if(dealer && cellnoPratica && cellaTelaio && cellaBeneficiario.toString()!='' ){
                                            def polizzaesistente= Polizza.findByNoPolizzaIlike(cellnoPratica)
                                            if (polizzaesistente){
                                                if(codOperazione=="S" && !speciale){
                                                    if(polizzaesistente.telaio.equalsIgnoreCase(cellaTelaio)){
                                                        polizzaesistente.codOperazione=codOperazione
                                                        if(polizzaesistente.save(flush:true)) {
                                                            def tracciatoPolizzaR= tracciatiService.generaTracciatoIAssicur(polizzaesistente)
                                                            if(!tracciatoPolizzaR.save(flush: true)){
                                                                logg =new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                risposta.add(noPratica:cellnoPratica, errore:"Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors}")
                                                                errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors},")
                                                                totaleErr++
                                                            }else{
                                                                def tracciatoComPolizzaR= tracciatiService.generaTracciatoCompagnia(polizzaesistente)
                                                                if(!tracciatoComPolizzaR.save(flush: true)){
                                                                    logg =new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    risposta.add(noPratica:cellnoPratica, errore:"Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors}")
                                                                    errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors},")
                                                                    totaleErr++
                                                                }else{
                                                                    if(polizzaesistente.tracciatoPAIId){
                                                                        def tracciatoPAIPolizzaR= tracciatiService.generaTracciatoPAI(polizzaesistente, speciale)
                                                                        if(!tracciatoPAIPolizzaR.save(flush: true)){
                                                                            logg =new Log(parametri: "Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            risposta.add(noPratica:cellnoPratica, errore:"Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors}")
                                                                            errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors},")
                                                                            totaleErr++
                                                                        } else{
                                                                            if(polizzaesistente.targa != null && polizzaesistente.targa != ''){
                                                                                polizzaesistente.targa=polizzaesistente.targa+"_R"
                                                                            }
                                                                            if(polizzaesistente.telaio != null && polizzaesistente.telaio!= ''){
                                                                                polizzaesistente.telaio=polizzaesistente.telaio+"_R"
                                                                            }
                                                                            if(polizzaesistente.save(flush:true)) {
                                                                                logg =new Log(parametri: "targa e telaio per la polizza ${polizzaesistente.noPolizza} aggiornati", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                praticheNuove.add("polizza stornata correttamente ${polizzaesistente.noPolizza},")
                                                                                totale++
                                                                            }else{
                                                                                logg =new Log(parametri: "non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                risposta.add(noPratica:cellnoPratica, errore:"non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}")
                                                                                errorePratica.add(" pratica-->${cellnoPratica} non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors},")
                                                                                totaleErr++
                                                                            }
                                                                        }
                                                                    }else{
                                                                        if(polizzaesistente.targa != null && polizzaesistente.targa != ''){
                                                                            polizzaesistente.targa=polizzaesistente.targa+"_R"
                                                                        }
                                                                        if(polizzaesistente.telaio != null && polizzaesistente.telaio!= ''){
                                                                            polizzaesistente.telaio=polizzaesistente.telaio+"_R"
                                                                        }
                                                                        if(polizzaesistente.save(flush:true)) {
                                                                            logg =new Log(parametri: "targa e telaio per la polizza ${polizzaesistente.noPolizza} aggiornati", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            praticheNuove.add("polizza stornata correttamente ${polizzaesistente.noPolizza},")
                                                                            totale++
                                                                        }else{
                                                                            logg =new Log(parametri: "non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            risposta.add(noPratica:cellnoPratica, errore:"non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}")
                                                                            errorePratica.add(" pratica-->${cellnoPratica} non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors},")
                                                                            totaleErr++
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    }else{
                                                        risposta.add(noPratica:cellnoPratica, errore:"il telaio associato a questa pratica non corrisponde a quello inserito nel DB")
                                                        errorePratica.add("per questa pratica-->${cellnoPratica} il telaio associato non corrisponde a quello inserito nel DB,")
                                                        totaleErr++
                                                    }
                                                }
                                                else if(speciale && !sup75){
                                                    logg =new Log(parametri: "genero tracciato PAI", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    def tracciatoPAI= tracciatiService.generaTracciatoPAI(polizzaesistente, speciale)
                                                    if(!tracciatoPAI.save(flush: true)){
                                                        logg =new Log(parametri: "Errore creazione tracciato PAI: ${tracciatoPAI.errors}", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        risposta.add(noPratica:cellnoPratica, errore:"Errore creazione tracciato PAI: ${tracciatoPAI.errors}")
                                                        errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAI.errors},")
                                                        totaleErr++
                                                    } else{
                                                        logg =new Log(parametri: "tracciato PAI generato correttamente per la polizza ${polizzaesistente.noPolizza}", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        flussiPAI.add("tracciato PAI generato correttamente per la polizza ${polizzaesistente.noPolizza},")
                                                        totale++
                                                    }
                                                }/*else if(sup75){
                                                    errorePratica.add("il beneficiario della pratica-->${cellnoPratica}  supera la eta' di 75 anni,")
                                                    totaleErr++
                                                }*/
                                                else{
                                                    errorePratica.add("la pratica-->${cellnoPratica} \u00E8 gia' stata caricata nel portale,")
                                                    totaleErr++
                                                }
                                            }else{
                                                if(codOperazione=="S"){
                                                    risposta.add(noPratica:cellnoPratica, errore:"la polizza che si sta cercando di stornare, non esiste, verificare no polizza")
                                                    errorePratica.add("la pratica-->${cellnoPratica} che si sta cercando di stornare non esiste. Verificare no polizza,")
                                                    totaleErr++
                                                }else if(speciale){
                                                    risposta.add(noPratica:cellnoPratica, errore:"la polizza non esiste e non e' possibile generare un flusso PAI... verificare no polizza")
                                                    errorePratica.add("la pratica-->${cellnoPratica} non esiste e non e' possibile generare un flusso PAI... verificare no polizza,")
                                                    totaleErr++
                                                }
                                                else{
                                                    def polizza = Polizza.findOrCreateWhere(
                                                            annullata:false,
                                                            cap: capDef.toString().trim().padLeft(5,"0"),
                                                            cellulare:  cellDef.toString().trim(),
                                                            cognome:  cognomeDef.toString().trim(),
                                                            coperturaRichiesta: coperturaR,
                                                            dataDecorrenza:dataDecorrenza,
                                                            dataImmatricolazione: dataImmatricolazione,
                                                            dataInserimento: dataInserimento,
                                                            dataScadenza: dataScadenza,
                                                            dealer: dealer,
                                                            durata:durata,
                                                            email: emailDef.toString().trim(),
                                                            indirizzo: indirizzoDef.toString().trim().toUpperCase(),
                                                            localita: localitaDef.toString().trim().toUpperCase(),
                                                            marca: cellaMarca,
                                                            modello: cellaModello,
                                                            noPolizza: cellnoPratica,
                                                            nome: nomeDef.toString().trim().toUpperCase(),
                                                            nuovo:nuovo,
                                                            onStar: onStar,
                                                            partitaIva: cfDef.toString().trim().toUpperCase(),
                                                            premioImponibile:premioImponibile,
                                                            premioLordo:premioLordo,
                                                            provincia: provinciaDef.toString().trim().toUpperCase(),
                                                            provvDealer:provvDealer,
                                                            provvGmfi:provvGmfi,
                                                            provvVenditore:provvVendi,
                                                            stato:StatoPolizza.PREVENTIVO,
                                                            tipoCliente:tipoClienteDef,
                                                            tipoPolizza:polizzaTipo,
                                                            step:codStep,
                                                            telaio:cellaTelaio,
                                                            targa:cellaTarga,
                                                            telefono:telDef.toString().trim(),
                                                            valoreAssicurato:valoreAssicurato,
                                                            valoreAssicuratoconiva:prezzoVendita,
                                                            certificatoMail:certificato,
                                                            codiceZonaTerritoriale: "1",
                                                            rappel:0.0,
                                                            codOperazione:codOperazione,
                                                            dSlip:"S",
                                                            venditore:cellaVenditore?cellaVenditore.toString().trim().toUpperCase():"",
                                                            imposte:0.0,
                                                            codCampagna: codCampagna
                                                    )
                                                    if(polizza.save(flush:true)) {
                                                        logg =new Log(parametri: "la polizza viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        praticheNuove.add("polizza caricata correttamente ${polizza.noPolizza},")
                                                        totale++
                                                    }
                                                    else{
                                                        logg =new Log(parametri: "Errore caricamento polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        risposta.add(noPratica:cellnoPratica, errore:"Errore caricamento polizza: ${polizza.errors}")
                                                        errorePratica.add(" pratica-->${cellnoPratica} Errore caricamento polizza: ${polizza.errors},")
                                                        totaleErr++
                                                    }
                                                }
                                            }
                                        }else if(!dealer){
                                            logg =new Log(parametri: "Errore caricamento polizza: il dealer non \u00E8 presente", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            risposta.add("Errore caricamento polizza: il dealer non \u00E8 presente")
                                            errorePratica.add("Errore caricamento polizza: il dealer ${cellaDealer}- ${cellaNomeDealer} non \u00E8 presente,")
                                            totaleErr++
                                        }else if(!cellnoPratica){
                                            logg =new Log(parametri: "Errore caricamento polizza: non \u00E8 presente un numero di pratica", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            risposta.add("Errore caricamento polizza: non \u00E8 presente un numero di pratica,")
                                            errorePratica.add("Errore caricamento polizza: non \u00E8 presente un numero di polizza,")
                                            totaleErr++
                                        }
                                    }else{
                                        logg =new Log(parametri: "Errore caricamento polizza:${cellnoPratica} la somma delle provviggioni-> ${totProvvi} non corrisponde con la percentuale imponibile dello step assegnato-> ${calcoloImp}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        risposta.add("Errore caricamento polizza:${cellnoPratica} la somma delle provviggioni-> ${totProvvi} non corrisponde con la percentuale imponibile dello step assegnato-> ${calcoloImp},")
                                        errorePratica.add("Errore caricamento polizza:${cellnoPratica} la somma delle provviggioni-> ${totProvvi} non corrisponde con la percentuale imponibile dello step assegnato-> ${calcoloImp},")
                                        totaleErr++
                                    }
                                }else{
                                    logg =new Log(parametri: "Errore caricamento polizza:${cellnoPratica} la provincia non \u00E8 stata dichiarata, controllare il file", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    risposta.add("Errore caricamento polizza:${cellnoPratica} la provincia non \u00E8 stata dichiarata, controllare il file,")
                                    errorePratica.add("Errore caricamento polizza:${cellnoPratica} la provincia non \u00E8 stata dichiarata, controllare il file,")
                                    totaleErr++
                                }
                            }
                        }
                    }

                }catch (e){
                    logg =new Log(parametri: "c\u00E8 un problema con il caricamento del file ${e.toString()}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    risposta.add("c\u00E8  un problema con il caricamento del file")
                    errorePratica.add("c\u00E8  un problema con il caricamento del file ")
                }

                if(polizzeDeleted.size()>0){
                    for(String s: polizzeDeleted)
                    {
                        listDeleted+=s+"\r\n\r\n"
                    }
                    def mailInviato= mailService.invioMailDeleted(listDeleted)
                    logg =new Log(parametri: "risposta dell'invio della mail POLIZZE DELETED  ${mailInviato}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    if((mailInviato.contains("queued")||mailInviato.contains("sent") ||mailInviato.contains("scheduled")) && !mailInviato.contains("errore->") ){
                        praticheNuove.add("mail riassunto polizze DELETED inviata correttamente a assunta.carnovale@gmfinancial.com, ")
                    }else{
                        risposta.add("Errore invio mail  polizza: ${mailInviato}")
                        errorePratica.add("Errore invio mail  polizza: ${mailInviato}")
                    }
                }
                if(errorePratica.size()>0){
                    for (String s : errorePratica)
                    {
                        listError += s + "\r\n\r\n"
                    }
                    //println(listError)
                }

                if(praticheNuove.size()>0){
                    for (String s : praticheNuove)
                    {
                        listPratiche += s + "\r\n\r\n"
                    }
                    //println(listPratiche)
                }
                if(errorePratica.size()>0){
                    flash.errorePratica=listError+="totale polizze non caricate ${totaleErr}"
                }
                if(praticheNuove.size()>0){
                    flash.praticheNuove=listPratiche+="totale  polizze caricate ${totale}"
                }
            } else {flash.error = "Specificare l'elenco csv"}

            redirect action: "listFinanziate"
        }else response.sendError(404)
    }
    def associatelaiodatatar() {
        def logg
        if (request.post) {
            def associa=[], erroreTelaio=[]
            def file =params.telaiodatatar
            if(file.bytes) {
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                def risposta =[]
                try {
                    logg =new Log(parametri: "chiamo il job per leggere il file excel, associare i telai con le date e chiamare il WS", operazione: "associazione telaio data tariffa", pagina: "Polizze FIN/LEASING")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    AssocTelaiDataFinLeasJob.triggerNow([myInputStream:myInputStream])
                    flash.associazione = "A breve arrivera' la mail con il riassunto delle polizze inserite"
                }catch (e){
                    println "c'e' un problema con il file, ${e.toString()}"
                    risposta.add("c'e' un problema con il file, ${e.toString()}")
                    logg =new Log(parametri: "errore try catch: ${e.toString()}", operazione: "associazione telaio data tariffa", pagina: "Polizze FIN/LEASING")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }
            } else flash.error = "Specificare l'elenco xlsx"
            redirect action: "listFinanziate"
        }else response.sendError(404)
    }

    /*finisce codice vecchio*/
    def chiamataWSFIN(Polizza polizza, def dataTarriffa){
        def logg
        def onStar=false
        def tipoPolizza=""
        if(polizza.tipoPolizza){
            tipoPolizza=polizza.tipoPolizza
        }

        def copertura=polizza.coperturaRichiesta
        def valoreAssicurato=polizza.valoreAssicurato
        def provincia=polizza.provincia
        def comune=polizza.localita
        comune=comune.toString().toUpperCase()
        def durata=polizza.durata
        def antifurtoval=polizza.onStar
        def antifurto
        if(antifurtoval=="1" || antifurtoval=="true") antifurto="S"
        else antifurto="N"
        def dealerType=polizza.dealer.dealerType
        def step=polizza.step
        def dataEffetto=dataTarriffa.format("yyyy-MM-dd")
        def modello=polizza.modello
        def tipoOperazione="A"

        if(polizza.codOperazione=="0"){
            tipoOperazione="A"
        }else{
            tipoOperazione=polizza.codOperazione
        }

        def classeprovvi=""
        def stepcodifica=""
        def coperturacodifica=""
        if(step.toString().equalsIgnoreCase("step 1")){
            classeprovvi="1"
        }else if(step.toString().equalsIgnoreCase("step 2")){
            classeprovvi="2"
        }else if(step.toString().equalsIgnoreCase("step 3")){
            classeprovvi="3"
        }
        /*if(copertura.toString().equalsIgnoreCase()=="silver"){
            coperturacodifica="SILVER"
        }else if(copertura.toString().equalsIgnoreCase()=="gold"){
            coperturacodifica="GOLD"
        }else if(copertura.toString().equalsIgnoreCase()=="platinum"){
            coperturacodifica="PLATINUM"
        }else if(copertura.toString().equalsIgnoreCase()=="platinumK"){
            coperturacodifica="PLATINUM KASKO"
        }else if(copertura.toString()=="platinumC"){
            coperturacodifica="PLATINUM COLLISIONE"
        }*/

        def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
        String pattern = "#,##0.0#"
        symbols.setGroupingSeparator((char) ',')
        symbols.setDecimalSeparator((char) '.')
        def  decimalFormat = new DecimalFormat(pattern, symbols)
        decimalFormat.setParseBigDecimal(true)
        def premioLordoSAntifurto=""
        def premioLordoAntifurto=""

        def pacchetto=CodProdotti.findByCoperturaAndStep(copertura, step).codPacchetto
        /*def dataTariffa=Date.parse("yyyy-MM-dd",dataEffetto)
        def dataTariffa2=Date.parse("yyyy-MM-dd","2017-04-18")
        if(dataTariffa>=dataTariffa2){
            pacchetto=CodProdotti.findByCoperturaAndStep(copertura, step).codPacchettodlflex2

        }else{
            pacchetto=CodProdotti.findByCoperturaAndStep(copertura, step).codPacchetto

        }*/
        pacchetto=CodProdotti.findByCoperturaAndStep(copertura, step).codPacchetto

        def percentualeVenditore=polizza.dealer.percVenditore
        if(percentualeVenditore.toPlainString()=="0.0000000"){
            percentualeVenditore=0.0
        }
        def test="N"
        /*if(Environment.current != Environment.DEVELOPMENT) {
            test="N"
        }else{
            test="S"
        }*/
        boolean risposta=false
        def parameters=[]
        if(tipoOperazione=="A"){
            antifurto="N"
            if(polizza.codCampagna!='0'){
                parameters  =[
                        codicepacchetto: "${pacchetto}",
                        valoreassicurato: valoreAssicurato,
                        durata: durata,
                        provincia: provincia,
                        comune: comune,
                        operazione: tipoOperazione,
                        dataeffettocopertura: dataEffetto,
                        antifurto: antifurto,
                        dealerType: dealerType,
                        classeProvvigionale: classeprovvi,
                        percentualeVenditore: percentualeVenditore,
                        test:test,
                        vehiclemodel:modello,
                        vapcampaignid:polizza.codCampagna
                ]
            }else{
                parameters  =[
                        codicepacchetto: "${pacchetto}",
                        valoreassicurato: valoreAssicurato,
                        durata: durata,
                        provincia: provincia,
                        comune: comune,
                        operazione: tipoOperazione,
                        dataeffettocopertura: dataEffetto,
                        antifurto: antifurto,
                        dealerType: dealerType,
                        classeProvvigionale: classeprovvi,
                        percentualeVenditore: percentualeVenditore,
                        test:test
                ]
            }

        }/*else{
            def dataScadenza=polizza.dataScadenza
            def dataStorno=polizza.dataStorno
            dataEffetto=dataTarriffa.format("yyyy-MM-dd")
            parameters  =[
                    codicepacchetto: "${pacchetto}",
                    valoreassicurato: valoreAssicurato,
                    durata: durata,
                    provincia: provincia,
                    comune: comune,
                    operazione: tipoOperazione,
                    dataeffettocopertura: dataEffetto,
                    antifurto: antifurto,
                    dealerType: dealerType,
                    classeProvvigionale: classeprovvi,
                    percentualeVenditore: percentualeVenditore,
                    test:test,
                    datascadenzacopertura:dataScadenza,
                    dataeffettostorno:dataStorno
            ]
        }*/
        logg =new Log(parametri: "parametri inviate al webservice->${parameters}", operazione: "chiamata  ws", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def url = "http://gmf.mach-1.it"
        def js = (parameters as JSON).toString()
        js = js.substring(1, js.length()-1)
        //println "js--> $js"
        def path = "/calcprovvigioni/"
        /*if(Environment.current != Environment.PRODUCTION){
            path = "/test/calcprovvigioni/"

        }*/
        logg =new Log(parametri: "il path che viene chiamato->${path}", operazione: "chiamata  ws", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def query = "{"+js+"}"
        def errore=[]
        def response = postText(url, path, [p:query])
        def stringaresponse=""
        def esito, codiceZonaTerritoriale,rappel,imposte,provvMansutti,provvMach1
        def provvDealerWS,provvgmfiWS,provvVendWS,premioLordoWS,premioImponibileWS,premioClienteWS,premioLordoClienteWS,premioTerziWS,premioLordoTerziWS


        JSONObject userJson = JSON.parse(response)
        userJson.each { id, data ->
            logg =new Log(parametri: "dati webservice->${data}", operazione: "chiamata  ws", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
               // if(data.Esito==0 ||data.Esito==1 || data.Esito==2){
                    if(data.Errors?.descErrore){
                        def desErrore=data.Errors?.descErrore?.toString().replaceAll("\\[","").replaceAll("]","")
                        logg =new Log(parametri: "errore riportato dal webservice->${desErrore}", operazione: "chiamata  ws", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        if(!((desErrore.contains("Comune non identificato") || (desErrore.contains("Data Effetto non coerente con"))))){
                            risposta=false
                            errore = data.Errors?.descErrore.toString().replaceAll("\\[","").replaceAll("]","")
                        }else if (((desErrore.contains("Comune non identificato"))|| (desErrore.contains("Data Effetto non coerente con"))) && !( desErrore.contains("Provincia non valida"))){
                            risposta=true
                        }
                        else if ( desErrore.contains("Provincia non valida")){
                            risposta=false
                            errore = data.Errors?.descErrore?.toString().replaceAll("\\[","").replaceAll("]","")
                        }
                    }
                    else{
                        risposta=true
                    }
               // }
            logg =new Log(parametri: "valore parametro risposta->${risposta}", operazione: "chiamata  ws", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if(risposta){
                esito=data.Esito ?:1
                premioLordoWS=data.PremioLordo?:0.0
                premioImponibileWS=data.Premio?:0.0
                provvDealerWS=data.provvigioneDealer?:0.0
                provvgmfiWS=data.provvigioneGMF?:0.0
                provvVendWS=data.provvigioneVenditore?:0.0
                provvMansutti=data.provvigioneMansutti?:0.0
                provvMach1=data.provvigioneMach1?:0.0
                codiceZonaTerritoriale=data.IPRPremioCalcolato.codiceZonaTerritoriale.toString().replaceAll("\\[","").replaceAll("]","")?:""
                rappel=data.IPRPremioCalcolato.rappel.toString().replaceAll("\\[","").replaceAll("]","")?:0.0
                imposte=data.IPRPremioCalcolato.imposte.toString().replaceAll("\\[","").replaceAll("]","")?:0.0
                premioClienteWS=data.PremioCliente?:0.0
                premioLordoClienteWS=data.PremioLordoCliente?:0.0
                premioTerziWS=data.PremioTerzi?:0.0
                premioLordoTerziWS=data.PremioLordoTerzi?:0.0
            }
        }
        if(risposta){
            logg =new Log(parametri: "premio lordo ws on star false -> ${premioLordoWS}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            premioLordoSAntifurto=premioLordoWS
            logg =new Log(parametri: "premio lordo csv ->${polizza.premioLordo}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if(premioLordoWS==polizza.premioLordo){
                if(rappel==0.0){
                    logg =new Log(parametri: "pratica-->${polizza.noPolizza} Attenzione per questa pratica il valore del rappel e' uguale a 0", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }
                logg =new Log(parametri: "il premio lordo è uguale on star false-> ${premioLordoWS}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                onStar=false
                /*polizza.premioLordo=(BigDecimal) premioLordoWS
                polizza.premioImponibile=(BigDecimal)premioImponibileWS
                polizza.provvDealer=(BigDecimal) provvDealerWS
                polizza.provvGmfi=(BigDecimal) provvgmfiWS
                polizza.provvVenditore=(BigDecimal) provvVendWS*/
                polizza.codiceZonaTerritoriale=codiceZonaTerritoriale
                polizza.rappel=(BigDecimal) decimalFormat.parse(rappel)
                polizza.imposte=(BigDecimal) decimalFormat.parse(imposte)
                polizza.stato= StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE
                polizza.onStar=onStar
                polizza.premioCliente=(BigDecimal) premioClienteWS
                polizza.premioLordoCliente=(BigDecimal) premioLordoClienteWS
                polizza.premioTerzi=(BigDecimal) premioTerziWS
                polizza.premioLordoTerzi=(BigDecimal) premioLordoTerziWS
                String datatariffa2 = '18-04-2017'
                def dataInizioTar2 = Date.parse( 'dd-MM-yyyy', datatariffa2 )
                if(dataTarriffa>=dataInizioTar2){
                    polizza.tariffa=2
                }else{
                    polizza.tariffa=1
                }
                //polizza.dataDecorrenza=dataTarriffa
                if(polizza.tracciatoId!=null){
                    def tracciatoId= polizza.tracciatoId
                    def tracciatoPolizza=Tracciato.get(tracciatoId)
                    if(tracciatoPolizza.dataCaricamento==null){tracciatoPolizza.dataCaricamento=new Date()}
                }
                if(polizza.tracciatoCompagniaId!=null){
                    def tracciatoCompagniaId= polizza.tracciatoCompagniaId
                    def tracciatoCompagniaPolizza=TracciatoCompagnia.get(tracciatoCompagniaId)
                    if(tracciatoCompagniaPolizza.dataCaricamento==null){tracciatoCompagniaPolizza.dataCaricamento=new Date()}
                }
                if(polizza.save(flush:true)) {
                    logg =new Log(parametri: "la polizza viene aggiornata nel DB", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                    if(!tracciatoPolizza.save(flush: true)){
                        logg =new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }else{
                        polizza.tracciato=tracciatoPolizza
                        logg =new Log(parametri: "genero tracciato IAssicur", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        def tracciatoComPolizza= tracciatiService.generaTracciatoCompagnia(polizza)
                        if(!tracciatoComPolizza.save(flush: true)){
                            logg =new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        } else{
                            polizza.tracciatoCompagnia=tracciatoComPolizza
                            logg =new Log(parametri: "genero tracciato Compagnia", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            if(polizza.save(flush:true)) {
                                logg =new Log(parametri: "polizza creata con i suoi tracciati correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }else{
                                //println "Errore creazione polizza: ${polizza.errors}"
                                risposta=false
                                esito="0"
                                errore="Errore creazione polizza: ${polizza.errors}"
                                logg =new Log(parametri: "Errore aggiornamento tracciati polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                                errore=[]

                            }
                            logg =new Log(parametri: "polizza creata correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }
                    }
                    stringaresponse=[risposta:risposta,esito:esito]

                }
                else{
                    risposta=false
                    esito="Errore creazione polizza: ${polizza.errors}"
                    logg =new Log(parametri: "Errore creazione polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                    errore=[]
                }

            }else{
                def diffPremio=Math.abs(Math.round((premioLordoWS-polizza.premioLordo)*100)/100)
                if(diffPremio<=0.05){
                    onStar=false
                    logg =new Log(parametri: "il premio lordo è uguale on star false per poca differenza-> ${diffPremio}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    onStar=false
                    /*polizza.premioLordo=(BigDecimal) premioLordoWS
                    polizza.premioImponibile=(BigDecimal)premioImponibileWS
                    polizza.provvDealer=(BigDecimal) provvDealerWS
                    polizza.provvGmfi=(BigDecimal) provvgmfiWS
                    polizza.provvVenditore=(BigDecimal) provvVendWS*/
                    polizza.codiceZonaTerritoriale=codiceZonaTerritoriale
                    polizza.rappel=(BigDecimal) decimalFormat.parse(rappel)
                    polizza.imposte=(BigDecimal) decimalFormat.parse(imposte)
                    polizza.stato= StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE
                    polizza.onStar=onStar
                    polizza.premioCliente=(BigDecimal) premioClienteWS
                    polizza.premioLordoCliente=(BigDecimal) premioLordoClienteWS
                    polizza.premioTerzi=(BigDecimal) premioTerziWS
                    polizza.premioLordoTerzi=(BigDecimal) premioLordoTerziWS
                    String datatariffa2 = '18-04-2017'
                    def dataInizioTar2 = Date.parse( 'dd-MM-yyyy', datatariffa2 )
                    if(dataTarriffa>=dataInizioTar2){
                        polizza.tariffa=2
                    }else{
                        polizza.tariffa=1
                    }
                    //polizza.dataDecorrenza=dataTarriffa
                    if(polizza.tracciatoId!=null){
                        logg =new Log(parametri: "la polizza ha un tracciato ${polizza.tracciatoId} ", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        println "tracciato $polizza.tracciatoId"
                        def tracciatoId= polizza.tracciatoId
                        def tracciatoPolizza=Tracciato.get(tracciatoId)
                        if(tracciatoPolizza.dataCaricamento==null){tracciatoPolizza.dataCaricamento=new Date()}
                    }
                    if(polizza.tracciatoCompagniaId!=null){
                        def tracciatoCompagniaId= polizza.tracciatoCompagniaId
                        def tracciatoCompagniaPolizza=TracciatoCompagnia.get(tracciatoCompagniaId)
                        if(tracciatoCompagniaPolizza.dataCaricamento==null){tracciatoCompagniaPolizza.dataCaricamento=new Date()}
                    }
                    if(polizza.save(flush:true)) {
                        logg =new Log(parametri: "la polizza viene aggiornata nel DB", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                        if(!tracciatoPolizza.save(flush: true)){
                            logg =new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }else{
                            polizza.tracciato=tracciatoPolizza
                            logg =new Log(parametri: "genero tracciato IAssicur", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            def tracciatoComPolizza= tracciatiService.generaTracciatoCompagnia(polizza)
                            if(!tracciatoComPolizza.save(flush: true)){
                                logg =new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            } else{
                                polizza.tracciatoCompagnia=tracciatoComPolizza
                                logg =new Log(parametri: "genero tracciato Compagnia", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                if(polizza.save(flush:true)) {
                                    logg =new Log(parametri: "polizza creata con i suoi tracciati correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }else{
                                    println "Errore creazione polizza: ${polizza.errors}"
                                    risposta=false
                                    esito="0"
                                    errore="Errore creazione polizza: ${polizza.errors}"
                                    logg =new Log(parametri: "Errore aggiornamento tracciati polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                                    errore=[]
                                }
                                logg =new Log(parametri: "polizza creata correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }
                        }
                        stringaresponse=[risposta:risposta,esito:esito]

                    }
                    else{
                        risposta=false
                        errore="Errore creazione polizza: ${polizza.errors}"
                        esito="0"
                        logg =new Log(parametri: "Errore creazione polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                        errore=[]
                    }
                }else{
                    antifurto="S"
                    if(polizza.codCampagna!='0'){
                        parameters  =[
                                codicepacchetto: "${pacchetto}",
                                valoreassicurato: valoreAssicurato,
                                durata: durata,
                                provincia: provincia,
                                comune: comune,
                                operazione: tipoOperazione,
                                dataeffettocopertura: dataEffetto,
                                antifurto: antifurto,
                                dealerType: dealerType,
                                classeProvvigionale: classeprovvi,
                                percentualeVenditore: percentualeVenditore,
                                test:test,
                                vehiclemodel:modello,
                                vapcampaignid:polizza.codCampagna
                        ]
                    }else{
                        parameters  =[
                                codicepacchetto: "${pacchetto}",
                                valoreassicurato: valoreAssicurato,
                                durata: durata,
                                provincia: provincia,
                                comune: comune,
                                operazione: tipoOperazione,
                                dataeffettocopertura: dataEffetto,
                                antifurto: antifurto,
                                dealerType: dealerType,
                                classeProvvigionale: classeprovvi,
                                percentualeVenditore: percentualeVenditore,
                                test:test
                        ]
                    }

                    logg =new Log(parametri: "star true -> parametri inviate al webservice->${parameters}", operazione: "carica polizze fin/lea chiamata ws", pagina: "INSERIMENTO/MODIFICA polizze FIN/LEA")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    url = "http://gmf.mach-1.it"
                    js = (parameters as JSON).toString()
                    js = js.substring(1, js.length()-1)
                    //println "js--> $js"
                    path = "/calcprovvigioni/"
                    /*if(Environment.current != Environment.PRODUCTION){
                        path = "/test/calcprovvigioni/"
                    }*/
                    logg =new Log(parametri: "il path che viene chiamato->${path}", operazione: "chiamata  ws", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    query = "{"+js+"}"
                    errore=[]
                    response = postText(url, path, [p:query])
                     stringaresponse=""
                    risposta=false
                    userJson = JSON.parse(response)
                    userJson.each { id, data ->
                        logg =new Log(parametri: "dati webservice->${data}", operazione: "chiamata  ws", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            //if(data.Esito==0 ||data.Esito==1 || data.Esito==2){
                                if(data.Errors?.descErrore){
                                    def desErrore=data.Errors?.descErrore?.toString().replaceAll("\\[","").replaceAll("]","")
                                    logg =new Log(parametri: "errore riportato dal webservice->${desErrore}", operazione: "chiamata  ws", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    if(!((desErrore.contains("Comune non identificato") || (desErrore.contains("Data Effetto non coerente con"))))){
                                        risposta=false
                                        errore = data.Errors?.descErrore.toString().replaceAll("\\[","").replaceAll("]","")
                                    }else if (((desErrore.contains("Comune non identificato"))|| (desErrore.contains("Data Effetto non coerente con"))) && !( desErrore.contains("Provincia non valida"))){
                                        risposta=true
                                    }
                                    else if ( desErrore.contains("Provincia non valida")){
                                        risposta=false
                                        errore = data.Errors?.descErrore?.toString().replaceAll("\\[","").replaceAll("]","")
                                    }
                                }
                                else{
                                    risposta=true
                                }
                            //}
                        logg =new Log(parametri: "valore parametro risposta->${risposta}", operazione: "chiamata  ws", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        if(risposta){
                            esito=data.Esito ?:1
                            premioLordoWS=data.PremioLordo?:0.0
                            premioImponibileWS=data.Premio?:0.0
                            provvDealerWS=data.provvigioneDealer?:0.0
                            provvgmfiWS=data.provvigioneGMF?:0.0
                            provvVendWS=data.provvigioneVenditore?:0.0
                            provvMansutti=data.provvigioneMansutti?:0.0
                            provvMach1=data.provvigioneMach1?:0.0
                            codiceZonaTerritoriale=data.IPRPremioCalcolato.codiceZonaTerritoriale.toString().replaceAll("\\[","").replaceAll("]","")?:""
                            rappel=data.IPRPremioCalcolato.rappel.toString().replaceAll("\\[","").replaceAll("]","")?:0.0
                            imposte=data.IPRPremioCalcolato.imposte.toString().replaceAll("\\[","").replaceAll("]","")?:0.0
                            premioClienteWS=data.PremioCliente?:0.0
                            premioLordoClienteWS=data.PremioLordoCliente?:0.0
                            premioTerziWS=data.PremioTerzi?:0.0
                            premioLordoTerziWS=data.PremioLordoTerzi?:0.0
                        }
                        else{
                            risposta=false
                            esito=esito
                            //if(data.Esito==0 ||data.Esito==1 || data.Esito==2){
                                errore="${data.Errors?.descErrore}"
                            /*}else{
                                errore="esito->${data.Esito}"
                            }*/

                            stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                            errore=[]
                        }
                    }
                    if(risposta){
                        logg =new Log(parametri: "premio lordo ws on star true -> ${premioLordoWS}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        premioLordoAntifurto=premioLordoWS
                        logg =new Log(parametri: "premio lordo csv ->${polizza.premioLordo}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        if(premioLordoWS==polizza.premioLordo){
                            //println "il premio lordo \u00E8 uguale on star true -> ${polizza.premioLordo}"
                            logg =new Log(parametri: "il premio lordo \u00E8 uguale on star true -> ${polizza.premioLordo}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            if(rappel==0.0){
                                logg =new Log(parametri: "pratica-->${polizza.noPolizza} Attenzione per questa pratica il valore del rappel e' uguale a 0", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }
                            onStar=true
                            /*polizza.premioLordo=(BigDecimal) premioLordoWS
                            polizza.premioImponibile=(BigDecimal)premioImponibileWS
                            polizza.provvDealer=(BigDecimal) provvDealerWS
                            polizza.provvGmfi=(BigDecimal) provvgmfiWS
                            polizza.provvVenditore=(BigDecimal) provvVendWS*/
                            polizza.codiceZonaTerritoriale=codiceZonaTerritoriale
                            polizza.rappel=(BigDecimal) decimalFormat.parse(rappel)
                            polizza.imposte=(BigDecimal) decimalFormat.parse(imposte)
                            polizza.stato= StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE
                            polizza.onStar=onStar
                            polizza.premioCliente=(BigDecimal) premioClienteWS
                            polizza.premioLordoCliente=(BigDecimal) premioLordoClienteWS
                            polizza.premioTerzi=(BigDecimal) premioTerziWS
                            polizza.premioLordoTerzi=(BigDecimal) premioLordoTerziWS
                            String datatariffa2 = '18-04-2017'
                            def dataInizioTar2 = Date.parse( 'dd-MM-yyyy', datatariffa2 )
                            if(dataTarriffa>=dataInizioTar2){
                                polizza.tariffa=2
                            }else{
                                polizza.tariffa=1
                            }
                            //polizza.dataDecorrenza=dataTarriffa
                            if(polizza.tracciatoId!=null){
                                println "tracciato $polizza.tracciatoId"
                                def tracciatoId= polizza.tracciatoId
                                def tracciatoPolizza=Tracciato.get(tracciatoId)
                                if(tracciatoPolizza.dataCaricamento==null){tracciatoPolizza.dataCaricamento=new Date()}
                            }
                            if(polizza.tracciatoCompagniaId!=null){
                                def tracciatoCompagniaId= polizza.tracciatoCompagniaId
                                def tracciatoCompagniaPolizza=TracciatoCompagnia.get(tracciatoCompagniaId)
                                if(tracciatoCompagniaPolizza.dataCaricamento==null){tracciatoCompagniaPolizza.dataCaricamento=new Date()}
                            }
                            if(polizza.save(flush:true)) {
                                logg =new Log(parametri: "la polizza viene aggiornata nel DB", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                                if(!tracciatoPolizza.save(flush: true)){
                                    logg =new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }else{
                                    polizza.tracciato=tracciatoPolizza
                                    logg =new Log(parametri: "genero tracciato IAssicur", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    def tracciatoComPolizza= tracciatiService.generaTracciatoCompagnia(polizza)
                                    if(!tracciatoComPolizza.save(flush: true)){
                                        logg =new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    } else{
                                        polizza.tracciatoCompagnia=tracciatoComPolizza
                                        logg =new Log(parametri: "genero tracciato Compagnia", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        if(polizza.save(flush:true)) {
                                            logg =new Log(parametri: "polizza creata con i suoi tracciati correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }else{
                                            println "Errore creazione polizza: ${polizza.errors}"
                                            risposta=false
                                            esito="0"
                                            errore="Errore creazione polizza: ${polizza.errors}"
                                            logg =new Log(parametri: "Errore aggiornamento tracciati polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                                            errore=[]
                                        }
                                        logg =new Log(parametri: "polizza creata correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }
                                }
                                stringaresponse=[risposta:risposta,esito:esito]

                            }
                            else{
                                println "Errore creazione polizza: ${polizza.errors}"
                                risposta=false
                                esito="0"
                                errore="Errore creazione polizza: ${polizza.errors}"
                                logg =new Log(parametri: "Errore creazione polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                                errore=[]
                            }
                        }else{
                            diffPremio=Math.abs(Math.round((premioLordoWS-polizza.premioLordo)*100)/100)
                            if(diffPremio<=0.05){
                                //println "il premio lordo \u00E8 uguale on star true con poca differenza -> ${diffPremio}"
                                logg =new Log(parametri: "il premio lordo \u00E8 uguale on star true con poca differenza -> ${diffPremio}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                onStar=true
                                /*polizza.premioLordo=(BigDecimal) premioLordoWS
                                polizza.premioImponibile=(BigDecimal)premioImponibileWS
                                polizza.provvDealer=(BigDecimal) provvDealerWS
                                polizza.provvGmfi=(BigDecimal) provvgmfiWS
                                polizza.provvVenditore=(BigDecimal) provvVendWS*/
                                polizza.codiceZonaTerritoriale=codiceZonaTerritoriale
                                polizza.rappel=(BigDecimal) decimalFormat.parse(rappel)
                                polizza.imposte=(BigDecimal) decimalFormat.parse(imposte)
                                polizza.stato= StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE
                                polizza.onStar=onStar
                                polizza.premioCliente=(BigDecimal) premioClienteWS
                                polizza.premioLordoCliente=(BigDecimal) premioLordoClienteWS
                                polizza.premioTerzi=(BigDecimal) premioTerziWS
                                polizza.premioLordoTerzi=(BigDecimal) premioLordoTerziWS
                                String datatariffa2 = '18-04-2017'
                                def dataInizioTar2 = Date.parse( 'dd-MM-yyyy', datatariffa2 )
                                if(dataTarriffa>=dataInizioTar2){
                                    polizza.tariffa=2
                                }else{
                                    polizza.tariffa=1
                                }
                                //polizza.dataDecorrenza=dataTarriffa
                                if(polizza.tracciatoId!=null){
                                    println "tracciato $polizza.tracciatoId"
                                    def tracciatoId= polizza.tracciatoId
                                    def tracciatoPolizza=Tracciato.get(tracciatoId)
                                    if(tracciatoPolizza.dataCaricamento==null){tracciatoPolizza.dataCaricamento=new Date()}
                                }
                                if(polizza.tracciatoCompagniaId!=null){
                                    def tracciatoCompagniaId= polizza.tracciatoCompagniaId
                                    def tracciatoCompagniaPolizza=TracciatoCompagnia.get(tracciatoCompagniaId)
                                    if(tracciatoCompagniaPolizza.dataCaricamento==null){tracciatoCompagniaPolizza.dataCaricamento=new Date()}
                                }
                                if(polizza.save(flush:true)) {
                                    logg =new Log(parametri: "la polizza viene aggiornata nel DB", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                                    if(!tracciatoPolizza.save(flush: true)){
                                        logg =new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }else{
                                        polizza.tracciato=tracciatoPolizza
                                        logg =new Log(parametri: "genero tracciato IAssicur", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        def tracciatoComPolizza= tracciatiService.generaTracciatoCompagnia(polizza)
                                        if(!tracciatoComPolizza.save(flush: true)){
                                            logg =new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        } else{
                                            polizza.tracciatoCompagnia=tracciatoComPolizza
                                            logg =new Log(parametri: "genero tracciato Compagnia", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            if(polizza.save(flush:true)) {
                                                logg =new Log(parametri: "polizza creata con i suoi tracciati correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            }else{
                                                println "Errore creazione polizza: ${polizza.errors}"
                                                risposta=false
                                                esito="0"
                                                errore="Errore creazione polizza: ${polizza.errors}"
                                                logg =new Log(parametri: "Errore aggiornamento tracciati polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                                                errore=[]
                                            }
                                            logg =new Log(parametri: "polizza creata correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }
                                    stringaresponse=[risposta:risposta,esito:esito]

                                }
                                else{
                                    println "Errore creazione polizza: ${polizza.errors}"
                                    risposta=false
                                    esito="0"
                                    errore="Errore creazione polizza: ${polizza.errors}"
                                    logg =new Log(parametri: "Errore creazione polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                                    errore=[]
                                }
                            }else{
                                println "Errore creazione polizza: ${polizza.errors}"
                                risposta=false
                                esito="0"
                                errore="pratica-->${polizza.noPolizza} Errore il premio lordo non corrisponde in nessuno dei casi con il premio lordo del webservice: con antifurto: ${premioLordoAntifurto ?: ""} senza antifurto: ${premioLordoSAntifurto ?: ""}"
                                logg =new Log(parametri: "pratica-->${polizza.noPolizza} Errore il premio lordo non corrisponde in nessuno dei casi con il premio lordo del webservice: con antifurto: ${premioLordoAntifurto ?: ""} senza antifurto: ${premioLordoSAntifurto ?: ""},", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                errore ="pratica-->${polizza.noPolizza} Errore il premio lordo non corrisponde in nessuno dei casi con il premio lordo del webservice: con antifurto: ${premioLordoAntifurto ?: ""} senza antifurto: ${premioLordoSAntifurto ?: ""}"
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                                errore=[]
                            }
                        }
                    }else{
                            risposta=false
                            esito=esito
                            stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                            errore=[]
                    }
                }
            }

        }else{
            risposta=false
            esito=esito

            stringaresponse=[risposta:risposta,esito:esito,errore:errore]
            errore=[]
        }
        //println stringaresponse
        return stringaresponse
    }
    def chiamataWSFINTAR2(def pacchetto,def valoreAssicurato,def provincia,def localitaDef,def durata,def dealerType, def step, def dataTarriffa, def tipoOperazione, def percentualeVenditore, def premioLordo, def noPolizza, def polizzaTipo){
       //println "paccheto->${pacchetto}\n valore ${valoreAssicurato}\n provincia ${provincia} \n"
        def logg
        def onStar=false
        def tipoPolizza=""
        def polizza = [:]
        /*if(polizza.tipoPolizza){
            tipoPolizza=polizza.tipoPolizza
        }*/

       // def copertura=polizza.coperturaRichiesta
       // def valoreAssicurato=polizza.valoreAssicurato
       // def provincia=polizza.provincia
        def comune=localitaDef
        comune=comune.toString().toUpperCase()
        //def durata=polizza.durata
        def antifurtoval=polizza.onStar
        def antifurto
        if(antifurtoval=="1" || antifurtoval=="true") antifurto="S"
        else antifurto="N"
        //def dealerType=polizza.dealer.dealerType
        //def step=polizza.step
        def dataEffetto=dataTarriffa
        //def tipoOperazione="A"

       /* if(polizza.codOperazione=="0"){
            tipoOperazione="A"
        }else{
            tipoOperazione=polizza.codOperazione
        }*/

        def classeprovvi=step
        def stepcodifica=""
        def coperturacodifica=""
        /*if(step.toString().equalsIgnoreCase("step 1")){
            classeprovvi="1"
        }else if(step.toString().equalsIgnoreCase("step 2")){
            classeprovvi="2"
        }else if(step.toString().equalsIgnoreCase("step 3")){
            classeprovvi="3"
        }*/
        def paymenttype="Leasing"
        if(polizzaTipo != TipoPolizza.LEASING.toString()){
            paymenttype="Funded"
        }
        def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
        String pattern = "#,##0.0#"
        symbols.setGroupingSeparator((char) ',')
        symbols.setDecimalSeparator((char) '.')
        def  decimalFormat = new DecimalFormat(pattern, symbols)
        decimalFormat.setParseBigDecimal(true)
        def premioLordoSAntifurto=""
        def premioLordoAntifurto=""

        //def pacchetto=CodProdotti.findByCoperturaAndStep(copertura, step).codPacchetto

       // pacchetto=CodProdotti.findByCoperturaAndStep(copertura, step).codPacchetto

        //def percentualeVenditore=polizza.dealer.percVenditore
        if(percentualeVenditore.toPlainString()=="0.0000000"){
            percentualeVenditore=0.0
        }
        def test="N"
        /*if(Environment.current != Environment.DEVELOPMENT) {
            test="N"
        }else{
            test="S"
        }*/
        boolean risposta=false
        def parameters=[]
        if(tipoOperazione=="A"){
            antifurto="N"
                parameters  =[
                        codicepacchetto: "${pacchetto}",
                        valoreassicurato: valoreAssicurato,
                        durata: durata,
                        provincia: provincia,
                        comune: comune,
                        operazione: tipoOperazione,
                        dataeffettocopertura: dataEffetto,
                        antifurto: antifurto,
                        dealerType: dealerType,
                        classeProvvigionale: classeprovvi,
                        percentualeVenditore: percentualeVenditore,
                        test:test,
                        paymenttype:paymenttype
                ]
        }
        //println "${parameters}"
        logg =new Log(parametri: "parametri inviate al webservice CVT->${parameters}", operazione: "chiamata  ws CVT", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def url = "http://gmf.mach-1.it"
        def js = (parameters as JSON).toString()
        js = js.substring(1, js.length()-1)
        //println "js--> $js"
        def path = "/calcprovvigioni/"
        /*if(Environment.current != Environment.PRODUCTION){
            path = "/test/calcprovvigioni/"

        }*/
        logg =new Log(parametri: "il path che viene chiamato WS CVT->${path}", operazione: "chiamata  ws CVT", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def query = "{"+js+"}"
        def errore=[]
        def response = postText(url, path, [p:query])
        def stringaresponse=""
        def esito, codiceZonaTerritoriale,rappel,imposte,provvMansutti,provvMach1
        def provvDealerWS,provvgmfiWS,provvVendWS,premioLordoWS,premioImponibileWS,premioClienteWS,premioLordoClienteWS,premioTerziWS,premioLordoTerziWS

        JSONObject userJson = JSON.parse(response)
        userJson.each { id, data ->
            //println "dati webservice->${data}"
            logg =new Log(parametri: "dati webservice->${data}", operazione: "chiamata  ws CVT", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
               // if(data.Esito==0 ||data.Esito==1 || data.Esito==2){
                    if(data.Errors?.descErrore){
                        def desErrore=data.Errors?.descErrore?.toString().replaceAll("\\[","").replaceAll("]","")
                        logg =new Log(parametri: "errore riportato dal webservice CVT->${desErrore}", operazione: "chiamata  ws", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        if(!((desErrore.contains("Comune non identificato") || (desErrore.contains("Data Effetto non coerente con"))))){
                            risposta=false
                            errore = data.Errors?.descErrore.toString().replaceAll("\\[","").replaceAll("]","")
                        }else if (((desErrore.contains("Comune non identificato"))|| (desErrore.contains("Data Effetto non coerente con"))) && !( desErrore.contains("Provincia non valida"))){
                            println "non trovo il comune"
                            risposta=true
                        }
                        else if ( desErrore.contains("Provincia non valida")){
                            risposta=false
                            errore = data.Errors?.descErrore?.toString().replaceAll("\\[","").replaceAll("]","")
                        }
                    }
                    else{
                        risposta=true
                    }
               // }
            logg =new Log(parametri: "valore parametro risposta WS CVT->${risposta}", operazione: "chiamata  ws CVT", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if(risposta){
                esito=data.Esito ?:1
                premioLordoWS=data.PremioLordo?:0.0
                premioImponibileWS=data.Premio?:0.0
                provvDealerWS=data.provvigioneDealer?:0.0
                provvgmfiWS=data.provvigioneGMF?:0.0
                provvVendWS=data.provvigioneVenditore?:0.0
                provvMansutti=data.provvigioneMansutti?:0.0
                provvMach1=data.provvigioneMach1?:0.0
                codiceZonaTerritoriale=data.IPRPremioCalcolato.codiceZonaTerritoriale.toString().replaceAll("\\[","").replaceAll("]","")?:""
                rappel=data.IPRPremioCalcolato.rappel.toString().replaceAll("\\[","").replaceAll("]","")?:0.0
                imposte=data.IPRPremioCalcolato.imposte.toString().replaceAll("\\[","").replaceAll("]","")?:0.0
                premioClienteWS=data.PremioCliente?:0.0
                premioLordoClienteWS=data.PremioLordoCliente?:0.0
                premioTerziWS=data.PremioTerzi?:0.0
                premioLordoTerziWS=data.PremioLordoTerzi?:0.0
            }
        }
        if(risposta){
            logg =new Log(parametri: "premio lordo ws on star false WS CVT-> ${premioLordoWS}", operazione: "caricamento polizze tramite csv WS CVT", pagina: "POLIZZE FINANZIATE")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            premioLordoSAntifurto=premioLordoWS
            logg =new Log(parametri: "premio lordo csv WS CVT->${premioLordo}", operazione: "caricamento polizze tramite csv WS CVT", pagina: "POLIZZE FINANZIATE")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if(premioLordoWS==premioLordo){
                if(rappel==0.0){
                    logg =new Log(parametri: "pratica-->${noPolizza} Attenzione per questa pratica il valore del rappel e' uguale a 0", operazione: "caricamento polizze tramite csv WS CVT", pagina: "POLIZZE FINANZIATE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }
                logg =new Log(parametri: "il premio lordo è uguale on star false-> ${premioLordoWS}", operazione: "caricamento polizze tramite csv WS CVT", pagina: "POLIZZE FINANZIATE")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                onStar=false
                risposta=true
                polizza.codiceZonaTerritoriale=codiceZonaTerritoriale
                polizza.rappel=(BigDecimal) decimalFormat.parse(rappel)
                polizza.imposte=(BigDecimal) decimalFormat.parse(imposte)
                polizza.stato= StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE
                polizza.onStar=onStar
                polizza.premioLordo=(BigDecimal) premioLordoWS
                polizza.premioImponibile=(BigDecimal) premioImponibileWS
                polizza.premioCliente=(BigDecimal) premioClienteWS
                polizza.premioLordoCliente=(BigDecimal) premioLordoClienteWS
                polizza.premioTerzi=(BigDecimal) premioTerziWS
                polizza.premioLordoTerzi=(BigDecimal) premioLordoTerziWS
                polizza.provvGmfi=(BigDecimal) provvgmfiWS
                polizza.tariffa=2

                /*if(polizza.tracciatoId!=null){
                    def tracciatoId= polizza.tracciatoId
                    def tracciatoPolizza=Tracciato.get(tracciatoId)
                    if(tracciatoPolizza.dataCaricamento==null){tracciatoPolizza.dataCaricamento=new Date()}
                }
                if(polizza.tracciatoCompagniaId!=null){
                    def tracciatoCompagniaId= polizza.tracciatoCompagniaId
                    def tracciatoCompagniaPolizza=TracciatoCompagnia.get(tracciatoCompagniaId)
                    if(tracciatoCompagniaPolizza.dataCaricamento==null){tracciatoCompagniaPolizza.dataCaricamento=new Date()}
                }*/
                /*if(polizza.save(flush:true)) {
                    logg =new Log(parametri: "la polizza viene aggiornata nel DB", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                    if(!tracciatoPolizza.save(flush: true)){
                        logg =new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }else{
                        polizza.tracciato=tracciatoPolizza
                        logg =new Log(parametri: "genero tracciato IAssicur", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        def tracciatoComPolizza= tracciatiService.generaTracciatoCompagnia(polizza)
                        if(!tracciatoComPolizza.save(flush: true)){
                            logg =new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        } else{
                            polizza.tracciatoCompagnia=tracciatoComPolizza
                            logg =new Log(parametri: "genero tracciato Compagnia", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            if(polizza.save(flush:true)) {
                                logg =new Log(parametri: "polizza creata con i suoi tracciati correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }else{
                                println "Errore creazione polizza: ${polizza.errors}"
                                risposta=false
                                esito="0"
                                errore="Errore creazione polizza: ${polizza.errors}"
                                logg =new Log(parametri: "Errore aggiornamento tracciati polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                                errore=[]

                            }
                            logg =new Log(parametri: "polizza creata correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }
                    }
                    stringaresponse=[risposta:risposta,esito:esito]

                }
                else{
                    risposta=false
                    esito="Errore creazione polizza: ${polizza.errors}"
                    logg =new Log(parametri: "Errore creazione polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                    errore=[]
                }*/
                esito=esito
                stringaresponse=[risposta:risposta,esito:esito,errore:null, polizza: polizza]
                errore=[]

            }else{
                def diffPremio=Math.abs(Math.round((premioLordoWS-premioLordo)*100)/100)
                if(diffPremio<=0.05){
                    logg =new Log(parametri: "il premio lordo è uguale on star false per poca differenza-> ${diffPremio}", operazione: "caricamento polizze tramite csv WS CVT", pagina: "POLIZZE FINANZIATE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    onStar=false
                    risposta=true
                    polizza.codiceZonaTerritoriale=codiceZonaTerritoriale
                    polizza.rappel=(BigDecimal) decimalFormat.parse(rappel)
                    polizza.imposte=(BigDecimal) decimalFormat.parse(imposte)
                    polizza.stato= StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE
                    polizza.onStar=onStar
                    polizza.premioLordo=(BigDecimal) premioLordoWS
                    polizza.premioImponibile=(BigDecimal) premioImponibileWS
                    polizza.premioCliente=(BigDecimal) premioClienteWS
                    polizza.premioLordoCliente=(BigDecimal) premioLordoClienteWS
                    polizza.premioTerzi=(BigDecimal) premioTerziWS
                    polizza.premioLordoTerzi=(BigDecimal) premioLordoTerziWS
                    polizza.provvGmfi=(BigDecimal) provvgmfiWS
                    polizza.tariffa=2
                    stringaresponse=[risposta:risposta,esito:esito,errore:null, polizza: polizza]
                    /*if(polizza.tracciatoId!=null){
                        println "tracciato $polizza.tracciatoId"
                        def tracciatoId= polizza.tracciatoId
                        def tracciatoPolizza=Tracciato.get(tracciatoId)
                        if(tracciatoPolizza.dataCaricamento==null){tracciatoPolizza.dataCaricamento=new Date()}
                    }
                    if(polizza.tracciatoCompagniaId!=null){
                        def tracciatoCompagniaId= polizza.tracciatoCompagniaId
                        def tracciatoCompagniaPolizza=TracciatoCompagnia.get(tracciatoCompagniaId)
                        if(tracciatoCompagniaPolizza.dataCaricamento==null){tracciatoCompagniaPolizza.dataCaricamento=new Date()}
                    }
                    if(polizza.save(flush:true)) {
                        logg =new Log(parametri: "la polizza viene aggiornata nel DB", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                        if(!tracciatoPolizza.save(flush: true)){
                            logg =new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }else{
                            polizza.tracciato=tracciatoPolizza
                            logg =new Log(parametri: "genero tracciato IAssicur", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            def tracciatoComPolizza= tracciatiService.generaTracciatoCompagnia(polizza)
                            if(!tracciatoComPolizza.save(flush: true)){
                                logg =new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            } else{
                                polizza.tracciatoCompagnia=tracciatoComPolizza
                                logg =new Log(parametri: "genero tracciato Compagnia", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                if(polizza.save(flush:true)) {
                                    logg =new Log(parametri: "polizza creata con i suoi tracciati correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }else{
                                    println "Errore creazione polizza: ${polizza.errors}"
                                    risposta=false
                                    esito="0"
                                    errore="Errore creazione polizza: ${polizza.errors}"
                                    logg =new Log(parametri: "Errore aggiornamento tracciati polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                                    errore=[]
                                }
                                logg =new Log(parametri: "polizza creata correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }
                        }
                        stringaresponse=[risposta:risposta,esito:esito]

                    }
                    else{
                        risposta=false
                        errore="Errore creazione polizza: ${polizza.errors}"
                        esito="0"
                        logg =new Log(parametri: "Errore creazione polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                        errore=[]
                    }*/
                }else{
                    antifurto="S"
                        parameters  =[
                                codicepacchetto: "${pacchetto}",
                                valoreassicurato: valoreAssicurato,
                                durata: durata,
                                provincia: provincia,
                                comune: comune,
                                operazione: tipoOperazione,
                                dataeffettocopertura: dataEffetto,
                                antifurto: antifurto,
                                dealerType: dealerType,
                                classeProvvigionale: classeprovvi,
                                percentualeVenditore: percentualeVenditore,
                                test:test,
                                paymenttype:paymenttype
                        ]
                    logg =new Log(parametri: "star true -> parametri inviate al webservice CVT->${parameters}", operazione: "carica polizze fin/lea chiamata ws CVT", pagina: "INSERIMENTO/MODIFICA polizze FIN/LEA")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    url = "http://gmf.mach-1.it"
                    js = (parameters as JSON).toString()
                    js = js.substring(1, js.length()-1)
                    //println "js--> $js"
                    path = "/calcprovvigioni/"
                    /*if(Environment.current != Environment.PRODUCTION){
                        path = "/test/calcprovvigioni/"
                    }*/
                    logg =new Log(parametri: "il path che viene chiamato WS CVT->${path}", operazione: "chiamata  ws CVT", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    query = "{"+js+"}"
                    errore=[]
                    response = postText(url, path, [p:query])
                    stringaresponse=""
                    risposta=false
                    userJson = JSON.parse(response)
                    userJson.each { id, data ->
                        logg =new Log(parametri: "dati webservice->${data}", operazione: "chiamata  ws", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            //if(data.Esito==0 ||data.Esito==1 || data.Esito==2){
                                if(data.Errors?.descErrore){
                                    def desErrore=data.Errors?.descErrore?.toString().replaceAll("\\[","").replaceAll("]","")
                                    logg =new Log(parametri: "errore riportato dal webservice->${desErrore}", operazione: "chiamata  ws", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    if(!((desErrore.contains("Comune non identificato") || (desErrore.contains("Data Effetto non coerente con"))))){
                                        risposta=false
                                        errore = data.Errors?.descErrore.toString().replaceAll("\\[","").replaceAll("]","")
                                    }else if (((desErrore.contains("Comune non identificato"))|| (desErrore.contains("Data Effetto non coerente con"))) && !( desErrore.contains("Provincia non valida"))){
                                        risposta=true
                                    }
                                    else if ( desErrore.contains("Provincia non valida")){
                                        risposta=false
                                        errore = data.Errors?.descErrore?.toString().replaceAll("\\[","").replaceAll("]","")
                                    }
                                }
                                else{
                                    risposta=true
                                }
                            //}
                        logg =new Log(parametri: "valore parametro risposta->${risposta}", operazione: "chiamata  ws", pagina: "CARICAMENTO POLIZZE FIN/LEASING")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        if(risposta){
                            esito=data.Esito ?:1
                            premioLordoWS=data.PremioLordo?:0.0
                            premioImponibileWS=data.Premio?:0.0
                            provvDealerWS=data.provvigioneDealer?:0.0
                            provvgmfiWS=data.provvigioneGMF?:0.0
                            provvVendWS=data.provvigioneVenditore?:0.0
                            provvMansutti=data.provvigioneMansutti?:0.0
                            provvMach1=data.provvigioneMach1?:0.0
                            codiceZonaTerritoriale=data.IPRPremioCalcolato.codiceZonaTerritoriale.toString().replaceAll("\\[","").replaceAll("]","")?:""
                            rappel=data.IPRPremioCalcolato.rappel.toString().replaceAll("\\[","").replaceAll("]","")?:0.0
                            imposte=data.IPRPremioCalcolato.imposte.toString().replaceAll("\\[","").replaceAll("]","")?:0.0
                            premioClienteWS=data.PremioCliente?:0.0
                            premioLordoClienteWS=data.PremioLordoCliente?:0.0
                            premioTerziWS=data.PremioTerzi?:0.0
                            premioLordoTerziWS=data.PremioLordoTerzi?:0.0
                        }
                        else{
                            risposta=false
                            esito=esito
                            //if(data.Esito==0 ||data.Esito==1 || data.Esito==2){
                                errore="${data.Errors?.descErrore}"
                            /*}else{
                                errore="esito->${data.Esito}"
                            }*/

                            stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                            errore=[]
                        }
                    }
                    if(risposta){
                        logg =new Log(parametri: "premio lordo ws CVT on star true -> ${premioLordoWS}", operazione: "caricamento polizze tramite csv WS CVT", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        premioLordoAntifurto=premioLordoWS
                        logg =new Log(parametri: "premio lordo csv ->${premioLordo}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        if(premioLordoWS==premioLordo){
                            //println "il premio lordo \u00E8 uguale on star true -> ${premioLordo}"
                            logg =new Log(parametri: "il premio lordo \u00E8 uguale on star true -> ${premioLordo}", operazione: "caricamento polizze tramite csv WS CVT", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            if(rappel==0.0){
                                logg =new Log(parametri: "pratica-->${noPolizza} Attenzione per questa pratica il valore del rappel e' uguale a 0", operazione: "caricamento polizze tramite csv WS CVT", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }
                            onStar=true
                            risposta=true
                            polizza.codiceZonaTerritoriale=codiceZonaTerritoriale
                            polizza.rappel=(BigDecimal) decimalFormat.parse(rappel)
                            polizza.imposte=(BigDecimal) decimalFormat.parse(imposte)
                            polizza.stato= StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE
                            polizza.onStar=onStar
                            polizza.premioLordo=(BigDecimal) premioLordoWS
                            polizza.premioImponibile=(BigDecimal) premioImponibileWS
                            polizza.premioCliente=(BigDecimal) premioClienteWS
                            polizza.premioLordoCliente=(BigDecimal) premioLordoClienteWS
                            polizza.premioTerzi=(BigDecimal) premioTerziWS
                            polizza.premioLordoTerzi=(BigDecimal) premioLordoTerziWS
                            polizza.provvGmfi=(BigDecimal) provvgmfiWS
                            polizza.tariffa=2
                            stringaresponse=[risposta:risposta,esito:esito,errore:null, polizza: polizza]
                            //polizza.dataDecorrenza=dataTarriffa
                            /*if(polizza.tracciatoId!=null){
                                println "tracciato $polizza.tracciatoId"
                                def tracciatoId= polizza.tracciatoId
                                def tracciatoPolizza=Tracciato.get(tracciatoId)
                                if(tracciatoPolizza.dataCaricamento==null){tracciatoPolizza.dataCaricamento=new Date()}
                            }
                            if(polizza.tracciatoCompagniaId!=null){
                                def tracciatoCompagniaId= polizza.tracciatoCompagniaId
                                def tracciatoCompagniaPolizza=TracciatoCompagnia.get(tracciatoCompagniaId)
                                if(tracciatoCompagniaPolizza.dataCaricamento==null){tracciatoCompagniaPolizza.dataCaricamento=new Date()}
                            }
                            if(polizza.save(flush:true)) {
                                logg =new Log(parametri: "la polizza viene aggiornata nel DB", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                                if(!tracciatoPolizza.save(flush: true)){
                                    logg =new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }else{
                                    polizza.tracciato=tracciatoPolizza
                                    logg =new Log(parametri: "genero tracciato IAssicur", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    def tracciatoComPolizza= tracciatiService.generaTracciatoCompagnia(polizza)
                                    if(!tracciatoComPolizza.save(flush: true)){
                                        logg =new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    } else{
                                        polizza.tracciatoCompagnia=tracciatoComPolizza
                                        logg =new Log(parametri: "genero tracciato Compagnia", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        if(polizza.save(flush:true)) {
                                            logg =new Log(parametri: "polizza creata con i suoi tracciati correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }else{
                                            println "Errore creazione polizza: ${polizza.errors}"
                                            risposta=false
                                            esito="0"
                                            errore="Errore creazione polizza: ${polizza.errors}"
                                            logg =new Log(parametri: "Errore aggiornamento tracciati polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                                            errore=[]
                                        }
                                        logg =new Log(parametri: "polizza creata correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }
                                }
                                stringaresponse=[risposta:risposta,esito:esito]

                            }
                            else{
                                println "Errore creazione polizza: ${polizza.errors}"
                                risposta=false
                                esito="0"
                                errore="Errore creazione polizza: ${polizza.errors}"
                                logg =new Log(parametri: "Errore creazione polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                                errore=[]
                            }*/
                        }else{
                            diffPremio=Math.abs(Math.round((premioLordoWS-premioLordo)*100)/100)
                            if(diffPremio<=0.05){
                                println "il premio lordo \u00E8 uguale on star true con poca differenza -> ${diffPremio}"
                                logg =new Log(parametri: "il premio lordo \u00E8 uguale on star true con poca differenza -> ${diffPremio}", operazione: "caricamento polizze tramite csv WS CVT", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                onStar=true
                                risposta=true
                                polizza.codiceZonaTerritoriale=codiceZonaTerritoriale
                                polizza.rappel=(BigDecimal) decimalFormat.parse(rappel)
                                polizza.imposte=(BigDecimal) decimalFormat.parse(imposte)
                                polizza.stato= StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE
                                polizza.onStar=onStar
                                polizza.premioLordo=(BigDecimal) premioLordoWS
                                polizza.premioImponibile=(BigDecimal) premioImponibileWS
                                polizza.premioCliente=(BigDecimal) premioClienteWS
                                polizza.premioLordoCliente=(BigDecimal) premioLordoClienteWS
                                polizza.premioTerzi=(BigDecimal) premioTerziWS
                                polizza.premioLordoTerzi=(BigDecimal) premioLordoTerziWS
                                polizza.provvGmfi=(BigDecimal) provvgmfiWS
                                polizza.tariffa=2
                                stringaresponse=[risposta:risposta,esito:esito,errore:null, polizza: polizza]
                                //polizza.dataDecorrenza=dataTarriffa
                                /*if(polizza.tracciatoId!=null){
                                    println "tracciato $polizza.tracciatoId"
                                    def tracciatoId= polizza.tracciatoId
                                    def tracciatoPolizza=Tracciato.get(tracciatoId)
                                    if(tracciatoPolizza.dataCaricamento==null){tracciatoPolizza.dataCaricamento=new Date()}
                                }
                                if(polizza.tracciatoCompagniaId!=null){
                                    def tracciatoCompagniaId= polizza.tracciatoCompagniaId
                                    def tracciatoCompagniaPolizza=TracciatoCompagnia.get(tracciatoCompagniaId)
                                    if(tracciatoCompagniaPolizza.dataCaricamento==null){tracciatoCompagniaPolizza.dataCaricamento=new Date()}
                                }
                                if(polizza.save(flush:true)) {
                                    logg =new Log(parametri: "la polizza viene aggiornata nel DB", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                                    if(!tracciatoPolizza.save(flush: true)){
                                        logg =new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }else{
                                        polizza.tracciato=tracciatoPolizza
                                        logg =new Log(parametri: "genero tracciato IAssicur", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        def tracciatoComPolizza= tracciatiService.generaTracciatoCompagnia(polizza)
                                        if(!tracciatoComPolizza.save(flush: true)){
                                            logg =new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizza.errors}", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        } else{
                                            polizza.tracciatoCompagnia=tracciatoComPolizza
                                            logg =new Log(parametri: "genero tracciato Compagnia", operazione: "associazione telaio data tariffa", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            if(polizza.save(flush:true)) {
                                                logg =new Log(parametri: "polizza creata con i suoi tracciati correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            }else{
                                                println "Errore creazione polizza: ${polizza.errors}"
                                                risposta=false
                                                esito="0"
                                                errore="Errore creazione polizza: ${polizza.errors}"
                                                logg =new Log(parametri: "Errore aggiornamento tracciati polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                                                errore=[]
                                            }
                                            logg =new Log(parametri: "polizza creata correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }
                                    stringaresponse=[risposta:risposta,esito:esito]

                                }
                                else{
                                    println "Errore creazione polizza: ${polizza.errors}"
                                    risposta=false
                                    esito="0"
                                    errore="Errore creazione polizza: ${polizza.errors}"
                                    logg =new Log(parametri: "Errore creazione polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                                    errore=[]
                                }*/
                            }else{
                                println "Errore creazione polizza: ${polizza.errors}"
                                risposta=false
                                esito="0"
                                errore="pratica-->${noPolizza} Errore il premio lordo non corrisponde in nessuno dei casi con il premio lordo del webservice: con antifurto: ${premioLordoAntifurto ?: ""} senza antifurto: ${premioLordoSAntifurto ?: ""}"
                                logg =new Log(parametri: "pratica-->${noPolizza} Errore il premio lordo non corrisponde in nessuno dei casi con il premio lordo del webservice: con antifurto: ${premioLordoAntifurto ?: ""} senza antifurto: ${premioLordoSAntifurto ?: ""},", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                errore ="pratica-->${noPolizza} Errore il premio lordo non corrisponde in nessuno dei casi con il premio lordo del webservice: con antifurto: ${premioLordoAntifurto ?: ""} senza antifurto: ${premioLordoSAntifurto ?: ""}"
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                stringaresponse=[risposta:risposta,esito:esito,errore:errore]
                                errore=[]
                            }
                        }
                    }else{
                            risposta=false
                            esito=esito
                            stringaresponse=[risposta:risposta,esito:esito,errore:errore, polizza: null]
                            errore=[]
                    }
                }
            }

        }else{
            risposta=false
            esito=esito
            stringaresponse=[risposta:risposta,esito:esito,errore:errore, polizza:null]
            errore=[]
        }
        logg =new Log(parametri: "stringa response->${stringaresponse}", operazione: "caricamento polizze tramite csv WS CVT", pagina: "POLIZZE FINANZIATE")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        //println stringaresponse
        return stringaresponse
    }
    def chiamataRCAWS(modello,dealerid,valoreAssicurato,provincia,localita,versione,dataEffetto){
        def bottoneChiamata="caricamento"
        def logg
        def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
        String pattern = "#,##0.0#"
        symbols.setGroupingSeparator((char) ',')
        symbols.setDecimalSeparator((char) '.')
        def  decimalFormat = new DecimalFormat(pattern, symbols)
        decimalFormat.setParseBigDecimal(true)
        localita=localita.toString().toUpperCase()
        def test="N"
        def pacchetto="RC000"
        def idPolizza, percVenditore
        def dealer=Dealer.get(dealerid)
        percVenditore=dealer.percVenditore
        if (modello.toString().trim().toUpperCase().contains("MOKKA X")){modello= modello.toString().replaceAll("X","")}
        if(dataEffetto.getClass()!=Date){
            dataEffetto=new Date().parse("dd-MM-yyyy", dataEffetto).format("yyyy-MM-dd")
        }
            else{
            dataEffetto=dataEffetto.format("yyyy-MM-dd")
        }

        /*if(Environment.current != Environment.DEVELOPMENT) {
            test="N"
        }else{
            test="S"
        }*/
        boolean risposta=true
        def stringaresponse=""
        def errore=[]
        stringaresponse=[risposta:risposta]
        def parametriaggiornamento=[]
        parametriaggiornamento=[
                modello:modello,
                percVenditore:dealerid,
                polizzaaggiornata:false,
                valoreAssicurato:valoreAssicurato,
                provincia:provincia,
                comune:localita
        ]
        def parameters=[]
        parameters  =[
                codicepacchetto: "${pacchetto}",
                valoreassicurato: valoreAssicurato,
                provincia: provincia,
                comune: localita,
                dataeffettocopertura: dataEffetto,
                vehiclemodel: modello,
                vehicleversion: versione,
                test:test,
                durata:12,
                percentualeVenditore:percVenditore
        ]
        logg =new Log(parametri: "parametri inviate al webservice RCA->${parameters}", operazione: "chiamata ws RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza RCA")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def url = "http://gmf.mach-1.it"

        def js = (parameters as JSON).toString()
        js = js.substring(1, js.length()-1)
        def path = "/calcprovvigioni/"
        if(Environment.current != Environment.PRODUCTION ){
            path = "/test/calcprovvigioni/"

        }else {
            path = "/calcprovvigioni/"
        }
        logg =new Log(parametri: "il path che viene chiamato ws rca->${path}", operazione: "chiamata  WS RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza RCA")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def query = "{"+js+"}"
        def esito, provvDealer,provvgmfi,provvVend,premioLordo,premioImponibile,codiceZonaTerritoriale,rappel,imposte,provvMansutti,provvMach1

        def response = postText(url, path, [p:query])
        JSONObject userJson = JSON.parse(response)
        userJson.each { id, data ->
            logg =new Log(parametri: "dati webservice->${data}", operazione: "chiamata WS RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza RCA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if(data.Errors?.descErrore){
                println "entro qui negli errori ${data.Errors.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")}"
                //def desErrore=data.Errors?.desc_Errore?.toString().replaceAll("\\[","").replaceAll("]","")
                def desErrore=data.Errors?.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")
                logg =new Log(parametri: "errore riportato dal webservice->${desErrore}", operazione: "chiamata  ws RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizze")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(!((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con")))){
                    risposta=false
                    errore = data.Errors?.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")
                }else if (((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con"))) &&!( desErrore.contains("Provincia non valida"))){
                    risposta=true
                }
                else if (((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con"))) && desErrore.contains("Provincia non valida")){
                    risposta=false
                    errore = data.Errors?.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")

                }
                else{
                    risposta=false
                    errore = data.Errors?.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")
                    esito=data.Esito
                }
            }
            else{
                //println "${data.Esito}"
                logg =new Log(parametri: "valore parametro data.esito->${data.Esito}", operazione: "chiamata  ws RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(data.Esito==0){
                    risposta=true
                }else{
                    risposta=false
                    errore=data.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")
                }
            }
            logg =new Log(parametri: "valore parametro risposta WS RCA->${risposta}", operazione: "chiamata  ws RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if(risposta){
                logg =new Log(parametri: "valori data WS RCA--> ${data.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")}", operazione: "chiamata  ws RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                esito=data.Esito? 0:1
                //println "questo è data-->>>>>>${data}"
                premioLordo=(BigDecimal) decimalFormat.parse(data?.PremioLordo.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
                premioImponibile=(BigDecimal) decimalFormat.parse(data?.Premio.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
                provvDealer=(BigDecimal) decimalFormat.parse(data?.provvigioneDealer.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
                provvgmfi=(BigDecimal) decimalFormat.parse(data?.provvigioneGMF.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
                provvVend=(BigDecimal) decimalFormat.parse(data?.provvigioneVenditore.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
                //provvMansutti=/*data.Tariffazione?.IPRPremioCalcolato.provvigioneMansutti ?:*/0.0
                //provvMach1=/*data.Tariffazione?.IPRPremioCalcolato.provvigioneMach1 ?:*/0.0
                //codiceZonaTerritoriale=data.IPRPremioCalcolato.codiceZonaTerritoriale.toString().replaceAll("\\[","").replaceAll("]","")?:""*/
                rappel=(BigDecimal) decimalFormat.parse(data?.rappel.toString().replaceAll("\\[","").replaceAll("]","")) ?:0.0
                imposte=(BigDecimal) decimalFormat.parse(data?.imposte.toString().replaceAll("\\[","").replaceAll("]","")) ?:0.0

                stringaresponse=[risposta:risposta,esito:esito,premioLordo:premioLordo,premioImponibile:premioImponibile, imposte:imposte, rappel:rappel, bottone:"caricamento"]
                errore=[]
            }else{
                stringaresponse=[risposta:risposta,esito:esito,errore:errore,premioLordo:premioLordo,premioImponibile:premioImponibile, imposte:imposte, rappel:rappel]
                errore=[errore:errore]
            }
        }

        /*if(risposta) stringaresponse=[risposta:risposta,esito:esito,premioLordo:premioLordo,provvDealer:provvDealer,provvgmfi:provvgmfi,provvMansutti:provvMansutti,provvMach1:provvMach1,provvVend:provvVend,premioImponibile:premioImponibile,codiceZonaTerritoriale:codiceZonaTerritoriale,rappel:rappel,imposte:imposte]
        else stringaresponse=[risposta:risposta,esito:esito,errore:errore,premioLordo:premioLordo,provvDealer:provvDealer,provvgmfi:provvgmfi,provvMansutti:provvMansutti,provvMach1:provvMach1,provvVend:provvVend,premioImponibile:premioImponibile,codiceZonaTerritoriale:codiceZonaTerritoriale,rappel:rappel,imposte:imposte]*/
        errore=[]
        logg =new Log(parametri: "stringa response ->${stringaresponse}", operazione: "chiamata  ws RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        return stringaresponse
    }
    def chiamataRCAWSCSV(pratica, modello,dealerid,valoreAssicurato,provincia,localita,versione,dataEffetto, premioLordo, polizzaTipo){
        def bottoneChiamata="caricamento"
        def logg
        def diffPremio
        def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
        String pattern = "#,##0.0#"
        symbols.setGroupingSeparator((char) ',')
        symbols.setDecimalSeparator((char) '.')
        def  decimalFormat = new DecimalFormat(pattern, symbols)
        decimalFormat.setParseBigDecimal(true)
        localita=localita.toString().toUpperCase()
        def test="N"
        def pacchetto="RC000"
        def idPolizza, percVenditore
        def paymenttype="Leasing"
        def dealer=Dealer.get(dealerid)
        if(polizzaTipo != TipoPolizza.LEASING.toString()){
            paymenttype="Funded"
        }
        println "paymente type $paymenttype"
        logg =new Log(parametri: "paymente type ${paymenttype}", operazione: "chiamata ws RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza RCA")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        if (modello.toString().trim().toUpperCase().contains("MOKKA X")){modello= modello.toString().replaceAll("X","").trim()}
        percVenditore=dealer.percVenditore
        if(dataEffetto.getClass()!=Date){
            dataEffetto=new Date().parse("dd-MM-yyyy", dataEffetto).format("yyyy-MM-dd")
        }
            else{
            dataEffetto=dataEffetto.format("yyyy-MM-dd")
        }
        /*if(Environment.current != Environment.DEVELOPMENT) {
            test="N"
        }else{
            test="S"
        }*/
        boolean risposta=true
        def stringaresponse=""
        def errore=[]
        stringaresponse=[risposta:risposta]
        def parametriaggiornamento=[]
        parametriaggiornamento=[
                modello:modello,
                percVenditore:dealerid,
                polizzaaggiornata:false,
                valoreAssicurato:valoreAssicurato,
                provincia:provincia,
                comune:localita
        ]
        def parameters=[]
        parameters  =[
                codicepacchetto: "${pacchetto}",
                valoreassicurato: valoreAssicurato,
                provincia: provincia,
                comune: localita,
                dataeffettocopertura: dataEffetto,
                vehiclemodel: modello,
                vehicleversion: versione,
                test:test,
                durata:12,
                percentualeVenditore:percVenditore,
                paymenttype:paymenttype
        ]
        logg =new Log(parametri: "parametri inviate al webservice RCA->${parameters}", operazione: "chiamata ws RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza RCA")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def url = "http://gmf.mach-1.it"

        def js = (parameters as JSON).toString()
        js = js.substring(1, js.length()-1)
        def path = "/calcprovvigioni/"
        /*if(Environment.current != Environment.PRODUCTION ){
            path = "/test/calcprovvigioni/"

        }else {
            path = "/calcprovvigioni/"
        }*/
        logg =new Log(parametri: "il path che viene chiamato ws rca->${path}", operazione: "chiamata  WS RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza RCA")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def query = "{"+js+"}"
        def esito, provvDealer,provvgmfi,provvVend,premioLordoWS,premioImponibile,zona,rappel,imposte,categoriaveicolo,provvMach1

        def response = postText(url, path, [p:query])
        JSONObject userJson = JSON.parse(response)
        userJson.each { id, data ->
            logg =new Log(parametri: "dati webservice->${data}", operazione: "chiamata WS RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza RCA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if(data.Errors?.descErrore){
                println "entro qui negli errori ${data.Errors.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")}"
                //def desErrore=data.Errors?.desc_Errore?.toString().replaceAll("\\[","").replaceAll("]","")
                def desErrore=data.Errors?.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")
                logg =new Log(parametri: "errore riportato dal webservice->${desErrore}", operazione: "chiamata  ws RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizze")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(!((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con")))){
                    risposta=false
                    errore = data.Errors?.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")
                }else if (((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con"))) &&!( desErrore.contains("Provincia non valida"))){
                    risposta=true
                }
                else if (((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con"))) && desErrore.contains("Provincia non valida")){
                    risposta=false
                    errore = data.Errors?.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")
                }
                else{
                    risposta=false
                    errore = data.Errors?.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")
                    esito=data.Esito
                }
            }
            else{
                println "${data.Esito}"
                if(data.Esito==0){
                    risposta=true
                }else{
                    risposta=false
                    errore=data.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")
                }
            }
            logg =new Log(parametri: "valore parametro risposta WS RCA->${risposta}", operazione: "chiamata  ws RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if(risposta){
                logg =new Log(parametri: "valori data WS RCA--> ${data.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")}", operazione: "chiamata  ws RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                esito=data.Esito? 0:1
                //println "questo è data-->>>>>>${data}"
                premioLordoWS=(BigDecimal) decimalFormat.parse(data?.PremioLordo.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
                premioImponibile=(BigDecimal) decimalFormat.parse(data?.Premio.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
                provvDealer=(BigDecimal) decimalFormat.parse(data?.provvigioneDealer.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
                provvgmfi=(BigDecimal) decimalFormat.parse(data?.provvigioneGMF.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
                provvVend=(BigDecimal) decimalFormat.parse(data?.provvigioneVenditore.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
                //provvMansutti=/*data.Tariffazione?.IPRPremioCalcolato.provvigioneMansutti ?:*/0.0
                //provvMach1=/*data.Tariffazione?.IPRPremioCalcolato.provvigioneMach1 ?:*/0.0
                zona=data.zona.toString().replaceAll("\\[","").replaceAll("]","")?:""
                categoriaveicolo=data.categoriaveicolo.toString().replaceAll("\\[","").replaceAll("]","")?:""
                rappel=(BigDecimal) decimalFormat.parse(data?.rappel.toString().replaceAll("\\[","").replaceAll("]","")) ?:0.0
                imposte=(BigDecimal) decimalFormat.parse(data?.imposte.toString().replaceAll("\\[","").replaceAll("]","")) ?:0.0
                if(premioLordoWS==premioLordo){
                    logg =new Log(parametri: "POLIZZA RCA CHIAMATA WS DA CSV il premio lordo e' uguale a quello del csv-> ${premioLordoWS}", operazione: "caricamento polizze tramite csv WS RCA", pagina: "POLIZZE FINANZIATE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    risposta=true
                    stringaresponse=[risposta:risposta,esito:esito,premioLordo:premioLordoWS,premioImponibile:premioImponibile, imposte:imposte, rappel:rappel,zona:zona, categoria:categoriaveicolo, bottone:"caricamento"]
                    errore=[]

                }else{
                    diffPremio=Math.abs(Math.round((premioLordoWS-premioLordo)*100)/100)
                    if(diffPremio<=0.05){
                       // println "POLIZZA RCA CHIAMATA WS DA CSV il premio lordo \u00E8 uguale  con poca differenza -> ${diffPremio}"
                        logg =new Log(parametri: "POLIZZA RCA CHIAMATA WS DA CSV il premio lordo \u00E8 uguale on star true con poca differenza -> ${diffPremio}", operazione: "caricamento polizze tramite csv WS CVT", pagina: "POLIZZE FINANZIATE")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        stringaresponse=[risposta:risposta,esito:esito,premioLordo:premioLordoWS,premioImponibile:premioImponibile, imposte:imposte, rappel:rappel, zona:zona, categoria:categoriaveicolo, bottone:"caricamento"]
                        errore=[]
                    }else{
                        risposta=false
                        esito="0"
                        errore="POLIZZA RCA CHIAMATA WS pratica RCA -->${pratica} Errore il premio lordo non corrisponde con il premio lordo del webservice: ${premioLordoWS ?: ""}"
                        logg =new Log(parametri: "POLIZZA RCA CHIAMATA WS pratica RCA -->${pratica} Errore il premio lordo non corrisponde con il premio lordo del webservice: ${premioLordoWS ?: ""},", operazione: "caricamento polizze RCA tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        stringaresponse=[risposta:risposta,esito:esito,errore:errore,premioLordo:premioLordoWS,premioImponibile:premioImponibile, imposte:imposte, rappel:rappel,zona:zona,categoria:categoriaveicolo]
                        errore=[]
                    }
                }
            }else{
                stringaresponse=[risposta:risposta,esito:esito,errore:errore,premioLordo:premioLordoWS,premioImponibile:premioImponibile, imposte:imposte, rappel:rappel,zona:zona, categoria:categoriaveicolo]
                errore=[errore:errore]
            }
        }

        /*if(risposta) stringaresponse=[risposta:risposta,esito:esito,premioLordo:premioLordo,provvDealer:provvDealer,provvgmfi:provvgmfi,provvMansutti:provvMansutti,provvMach1:provvMach1,provvVend:provvVend,premioImponibile:premioImponibile,codiceZonaTerritoriale:codiceZonaTerritoriale,rappel:rappel,imposte:imposte]
        else stringaresponse=[risposta:risposta,esito:esito,errore:errore,premioLordo:premioLordo,provvDealer:provvDealer,provvgmfi:provvgmfi,provvMansutti:provvMansutti,provvMach1:provvMach1,provvVend:provvVend,premioImponibile:premioImponibile,codiceZonaTerritoriale:codiceZonaTerritoriale,rappel:rappel,imposte:imposte]*/
        errore=[]
        logg =new Log(parametri: "stringa response->${stringaresponse}", operazione: "chiamata  ws RCA", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        return stringaresponse
    }
}
