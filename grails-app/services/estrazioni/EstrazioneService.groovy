package estrazioni

import cashopel.polizze.CodProdotti
import cashopel.polizze.Polizza
import cashopel.polizze.PolizzaPaiPag
import cashopel.polizze.StatoPolizza
import cashopel.polizze.TipoCliente
import cashopel.polizze.TipoPolizza
import cashopel.polizze.ZoneTarif
import cashopel.utenti.Admin
import cashopel.utenti.Dealer
import cashopel.utenti.Log
import cashopel.utenti.Ruolo
import com.itextpdf.text.BaseColor
import com.itextpdf.text.Element
import com.itextpdf.text.Font
import com.itextpdf.text.Paragraph
import com.itextpdf.text.pdf.PdfPCell
import com.itextpdf.text.pdf.PdfPTable
import com.itextpdf.text.pdf.PdfReader
import com.presstoday.groovy.Stopwatch
import excel.builder.ExcelBuilder
import excel.reader.ExcelReader
import grails.converters.JSON
import grails.core.GrailsApplication
import grails.transaction.Transactional
import grails.util.Environment
import groovy.time.TimeCategory
import org.apache.poi.ss.util.CellReference
import org.apache.poi.xssf.usermodel.XSSFWorkbook
import org.grails.web.json.JSONObject

import java.text.DecimalFormat
import java.util.regex.Pattern

@Transactional
class EstrazioneService {
    GrailsApplication grailsApplication
    def mailService
    def DLWebService
    def generaFilePolizzeAutoimport(def inizio, def fine){
        def fileName, fileContent
        def dealer=Dealer.get(2)
        def polizze=Polizza.createCriteria().list(){
            ge "dataInserimento", inizio
            le "dataInserimento", fine
            eq "dealer",dealer
            eq "tipoPolizza", TipoPolizza.CASH
            eq "codOperazione","0"
        }
        def wb = Stopwatch.log { ExcelBuilder.create {
            style("header") {
                background bisque
                font {
                    bold(true)
                }
            }
            sheet("Polizze AUTOIMPORT") {
                row(style: "header") {
                    cell("Prodotto")
                    cell("Assicurato")
                    cell("Premio \n Lordo")
                    cell("Num. \n Polizza")
                    cell("Targa o \n Telaio")
                    cell("Data Copertura")
                    cell("Data Scadenza")
                }
                if(polizze){
                    polizze.each { polizza ->
                        row {
                            def targa, telaio
                            if(polizza.targa){
                                targa=polizza.targa
                                if(targa.contains("_")){
                                    def under=targa.indexOf("_")
                                    targa=targa.substring(0,under)
                                }
                            }else if(polizza.telaio){
                                telaio=polizza.telaio
                                if(telaio.contains("_")){
                                    def under=telaio.indexOf("_")
                                    telaio=telaio.substring(0,under)
                                }
                            }
                            cell("Flex Protection")
                            cell("${polizza.cognome.toString().toUpperCase()} ${polizza.nome.toString().toUpperCase()}")
                            cell(polizza.premioLordo)
                            cell(polizza.noPolizza)
                            cell(targa!=null?targa.toString().toUpperCase():telaio!=null?telaio.toString().toUpperCase():"")
                            cell(polizza.dataDecorrenza.format("dd/MM/yyyy"))
                            cell(polizza.dataScadenza.format("dd/MM/yyyy"))
                        }
                        polizza.discard()
                    }
                    for (int i = 0; i < 17; i++) {
                        sheet.autoSizeColumn(i)
                    }
                }else{
                    row{
                        cell("non ci sono polizze inserite in questo periodo")
                    }
                }

            }

        }
        }

        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        fileName="polizze_Autoimport_da_${inizio.format("ddMMyyyy")}_a_${fine.format("ddMMyyyy")}.xlsx"
        fileContent=stream.toByteArray()
        return [(fileName): fileContent]
    }
    def elencoPolizzeSettimanaleAUTOIMPORT(def inizio, def fine){
        def dealer=Dealer.get(2)
        def polizze = /*SqlLogger.log {*/Polizza.createCriteria().list() {
            ge "dataInserimento", inizio
            le "dataInserimento", fine
            eq "dealer",dealer
            eq "tipoPolizza",TipoPolizza.CASH
            eq "codOperazione","0"
        }
        def src = grailsApplication.mainContext.getResource("/pdf/riepilogoAutoimport2.pdf").file
        def fileName
        def fileContent
        if (polizze.size() > 0) {
            def streams = []
            def df = new DecimalFormat("###,##0.00")
            def importoB = polizze.sum { it.premioLordo }
            def importoBonifico=df.format(importoB)
            //def numpag=Math.ceil(polizze.size()/14)
            //def pagcorr=0
            polizze.collate(14).each { elencoPolizze ->
                //pagcorr++
                 //importoB = elencoPolizze.sum { it.premioLordo }
                 //importoBonifico=df.format(importoB)
                def stream = new FileInputStream(src)
                PdfReader reader = new PdfReader(stream)
                def (canvas, output, writer) = mailService.createCanvasAndStreams(reader, 1)
                canvas.saveState()
                mailService.writeText(canvas, 34, 455, "Con la presente, si riepilogano le posizioni attivate nella settimana dal", 11, "nero", "normal")
                mailService.writeText(canvas, 377, 455, inizio.format("dd/MM/yyyy"), 11, "nero", "bold")
                mailService.writeText(canvas, 435, 455, "al", 11, "nero", "normal")
                mailService.writeText(canvas, 447, 455, fine.format("dd/MM/yyyy"), 11, "nero", "bold")
                mailService.writeText(canvas, 502, 455, ", relative al prodotto", 11, "nero", "normal")
                mailService.writeText(canvas, 603, 455, "Flex Protection :", 11, "nero", "bold")
                float[] columnWidths = [1f,2.5f,1f,1f,1.5f,1f,1f]
                def table = new PdfPTable(columnWidths)
                def cell = new PdfPCell()
                def font = new Font(Font.FontFamily.HELVETICA, 11, Font.BOLD)
                def fontN = new Font(Font.FontFamily.HELVETICA, 10, Font.NORMAL)
                def para = new Paragraph("Prodotto",font)
                para.setAlignment(Element.ALIGN_CENTER)
                cell.addElement(para)
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY)
                table.addCell(cell)
                cell = new PdfPCell()
                para = new Paragraph("Assicurato",font)
                para.setAlignment(Element.ALIGN_CENTER)
                cell.addElement(para)
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY)
                table.addCell(cell)
                cell = new PdfPCell()
                para = new Paragraph("Premio \n Lordo",font)
                para.setAlignment(Element.ALIGN_CENTER)
                cell.addElement(para)
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY)
                table.addCell(cell)
                cell = new PdfPCell()
                para = new Paragraph("Num. \n Polizza",font)
                para.setAlignment(Element.ALIGN_CENTER)
                cell.addElement(para)
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY)
                table.addCell(cell)
                cell = new PdfPCell()
                para = new Paragraph("Targa o \n Telaio",font)
                para.setAlignment(Element.ALIGN_CENTER)
                cell.addElement(para)
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY)
                table.addCell(cell)
                cell = new PdfPCell()
                para = new Paragraph("Data Copertura",font)
                para.setAlignment(Element.ALIGN_CENTER)
                cell.addElement(para)
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY)
                table.addCell(cell)
                cell = new PdfPCell()
                para = new Paragraph("Data Scadenza",font)
                para.setAlignment(Element.ALIGN_CENTER)
                cell.addElement(para)
                cell.setBackgroundColor(BaseColor.LIGHT_GRAY)
                table.addCell(cell)
                elencoPolizze.eachWithIndex { polizza, indice ->
                    def targa, telaio
                    if(polizza.targa){
                        targa=polizza.targa
                        if(targa.contains("_")){
                            def under=targa.indexOf("_")
                            targa=targa.substring(0,under)
                        }
                    }else if(polizza.telaio){
                        telaio=polizza.telaio
                        if(telaio.contains("_")){
                            def under=telaio.indexOf("_")
                            telaio=telaio.substring(0,under)
                        }
                    }
                    String assicurato = "${polizza.cognome}  ${polizza.nome}"
                    def premio=df.format(polizza.premioLordo)
                    def premioLordo = "${premio}"
                    String noPolizza = "${polizza.noPolizza}"
                    String targa_telaio = "${targa? targa:telaio?:null}"
                    String dataInizio = "${polizza.dataDecorrenza.format("dd/MM/yyyy")}"
                    String dataScadenza = "${polizza.dataScadenza.format("dd/MM/yyyy")}"

                    cell = new PdfPCell()
                    para = new Paragraph("Flex Protection",fontN)
                    para.setAlignment(Element.ALIGN_MIDDLE)
                    cell.addElement(para)
                    table.addCell(cell)
                    cell = new PdfPCell()
                    para = new Paragraph(assicurato,fontN)
                    para.setAlignment(Element.ALIGN_MIDDLE)
                    cell.addElement(para)
                    table.addCell(cell)
                    cell = new PdfPCell()
                    para = new Paragraph(premioLordo+" \u20AC  ",fontN)
                    para.setAlignment(Element.ALIGN_RIGHT)
                    cell.addElement(para)
                    table.addCell(cell)
                    cell = new PdfPCell()
                    para = new Paragraph(noPolizza,fontN)
                    para.setAlignment(Element.ALIGN_CENTER)
                    cell.addElement(para)
                    table.addCell(cell)
                    cell = new PdfPCell()
                    para = new Paragraph(targa_telaio,fontN)
                    para.setAlignment(Element.ALIGN_CENTER)
                    cell.addElement(para)
                    table.addCell(cell)
                    cell = new PdfPCell()
                    para = new Paragraph(dataInizio,fontN)
                    para.setAlignment(Element.ALIGN_CENTER)
                    cell.addElement(para)
                    table.addCell(cell)
                    cell = new PdfPCell()
                    para = new Paragraph(dataScadenza,fontN)
                    para.setAlignment(Element.ALIGN_CENTER)
                    cell.addElement(para)
                    table.addCell(cell)
                }
                //if(pagcorr==numpag){
                    table.addCell(" ")
                    cell = new PdfPCell()
                    cell.setVerticalAlignment(Element.ALIGN_MIDDLE)
                        para = new Paragraph("Importo Bonifico",font)
                        para.setAlignment(Element.ALIGN_RIGHT)
                        cell.addElement(para)
                        cell.setBackgroundColor(BaseColor.LIGHT_GRAY)
                        table.addCell(cell)
                        cell = new PdfPCell()
                        para = new Paragraph(importoBonifico+" \u20AC  ",font)
                        para.setAlignment(Element.ALIGN_RIGHT)
                        cell.addElement(para)
                        cell.setBackgroundColor(BaseColor.LIGHT_GRAY)
                        table.addCell(cell)
                        table.addCell(" ")
                        table.addCell(" ")
                        table.addCell(" ")
                        table.addCell(" ")
               // }

                table.setHorizontalAlignment(Element.ALIGN_MIDDLE)

                table.setTotalWidth(700f)
                table.writeSelectedRows(0, 17, 50, 425, canvas)
                canvas = writer.getOverContent(1)
                writer.close()
                reader.close()
                streams += output
            }
            fileName = "riepilogo_Autoimport_da_${inizio.format("ddMMyyyy")}_a_${fine.format("ddMMyyyy")}.pdf"
            fileContent = mailService.mergePdfAUTOIMPORT(streams).toByteArray()
            streams=[]
        }
        return [(fileName): fileContent]
    }
    def caricaExcelDealer() {
        def excel = this.class.getResource("excel/active_dealers.xlsx")
        println "Loading DEALERS.xlsx from: ${excel.path}"
        boolean attivo=false
        def wb = new XSSFWorkbook(excel.openStream())
        def decimalFormat = new DecimalFormat("#")
        def format = { dato ->
            if(dato instanceof String) return dato
            try {
                return decimalFormat.format(dato)
            } catch(e) {
                println "$dato: ${dato.getClass()} non convertibile"
                return null
            }
        }
        String special = "+!@#\$%^&*()_\\/[a-zA-Z] "
        String pattern = ".*[" + Pattern.quote(special) + "].*"
        def percVenditore
        Dealer.withTransaction {
            wb.getSheetAt(0).with {
                def cellaCodice = new CellReference("A2")
                def cellaRagioneSociale = new CellReference("D2")
                def cellPartitaIva = new CellReference("U2")
                def cellaSalesSpecialist = new CellReference("H2")
                def cellaDealerType = new CellReference("K2")
                def cellapercVenditore = new CellReference("M2")
                def cellTelefono = new  CellReference("O2")
                def cellFax = new  CellReference("P2")
                def cellmailD=new CellReference("N2")
                def cellmailSales=new CellReference("I2")
                def cellIndirizzo=new CellReference("R2")
                def cellCap=new CellReference("S2")
                def cellLocalita=new CellReference("T2")
                def cellProvincia=new CellReference("V2")
                def cellCodIassicur=new CellReference("X2")
                def ultimaRiga = new CellReference("A169")
                def firstRow = cellaCodice.row
                def lastRow = ultimaRiga.row
                (firstRow..lastRow).each { rowIndex ->
                    println "Analizzo riga: ${rowIndex}"
                    def row = getRow(rowIndex)
                    def dealerType=cell(row, cellaDealerType.col)
                    if(!(dealerType.toString().contains("NO FINLINK")) && !(dealerType.toString().contains("NO IVASS"))){
                        attivo=true
                        percVenditore=cell(row, cellapercVenditore.col)
                        if(percVenditore==null){
                            percVenditore=0.0
                        }
                    }else{
                        attivo=false
                        percVenditore=0.0
                    }
                    def codice=cell(row,cellaCodice.col)
                    codice=codice.toString()
                    if(codice.contains(".")){
                        /*def punto=codice.indexOf(".")
                        codice=codice.substring(0,punto).replaceAll("[^0-9]", "")*/
                        codice=Double.valueOf(codice).longValue().toString()
                    }
                    if (codice.toString().matches("[0].*")) {
                        //codice=codice.toString().replaceFirst("[^1-9]", "")
                        codice=codice.toString().replaceFirst("^0+(?!\\\$)", "")
                    }
                    def partitaIva = cell(row, cellPartitaIva.col)
                    partitaIva=partitaIva.toString()
                    if(partitaIva.trim()!='null'){
                        if(!partitaIva.toString().matches("^[a-z|A-Z]{6}[0-9]{2}[A-Z|a-z][0-9]{2}[A-Z|a-z][0-9]{3}[A-Z|a-z]\$") && !partitaIva.toString().matches("^[a-z|A-Z]{2}[0-9]{5}") && !partitaIva.matches("^0+\\d*(?:\\.\\d+)?\$")){
                            partitaIva=Double.valueOf(partitaIva).longValue().toString()
                        }
                    }
                    if(!partitaIva.matches("[0-9]{11}")){
                        partitaIva=partitaIva.padLeft(11,"0")
                    }
                    def indirizzo = cell(row, cellIndirizzo.col)
                    def localita = cell(row, cellLocalita.col).toString()
                    def provincia = cell(row, cellProvincia.col).toString()
                    def cap = cell(row, cellCap.col)
                    cap=cap.toString()
                    if(cap.contains(".")){
                        def puntocap=cap.indexOf(".")
                        cap=cap.substring(0,puntocap).replaceAll("[^0-9]", "")
                    }
                    if (!cap.matches("[0-9]{5}")) {
                        cap=cap.padLeft(5,"0")
                    }
                    def salesSpecialist= cell(row, cellaSalesSpecialist.col)


                    def ragioneSociale = cell(row, cellaRagioneSociale.col)
                    def telefono = cell(row, cellTelefono.col)
                    telefono=telefono.toString()
                    if(telefono.trim()!='null'){
                        telefono=telefono.trim().toString()
                        if(telefono.toString().contains(".")){
                            int zero=telefono.toString().indexOf(".")
                            telefono=telefono.toString().substring(0,zero)
                        }

                    }else{telefono=null}
                    def fax = cell(row, cellFax.col)
                    fax=fax.toString()
                    if(fax.trim()!='null'){
                        fax=fax.toString().trim()
                        if(fax.toString().contains(".")){
                            int zero=fax.toString().indexOf(".")
                            fax=fax.toString().substring(0,zero)
                        }
                    }else{
                        fax=null
                    }
                    def emaild=cell(row, cellmailD.col)
                    def emailsales=cell(row, cellmailSales.col)
                    def codIassicur=cell(row, cellCodIassicur.col)
                    def dealer = new Dealer(
                            codice: codice,
                            ragioneSocialeD: ragioneSociale,
                            pivaD: partitaIva,
                            indirizzoD: indirizzo,
                            localitaD: localita,
                            capD: cap,
                            provinciaD: provincia,
                            dealerType: dealerType,
                            telefonoD: telefono,
                            faxD: fax,
                            emailD: emaild,
                            emailSales: emailsales,
                            salesSpecialist: salesSpecialist,
                            percVenditore: percVenditore,
                            attivo: attivo,
                            codiceIASSICUR: codIassicur
                    )
                    if(dealer.save(flush: true)) println "Dealer creato correttamente ${codice}"
                    else println "Errore creazione Dealer: ${dealer.errors}"
                }
            }
            wb.close()
        }
        def userAdmin= new Admin()
        userAdmin.username= "ITADMIN"
        userAdmin.password="adminMach1"
        userAdmin.nome="admin"
        userAdmin.cognome="admin"
        userAdmin.passwordScaduta=false
        if(userAdmin.save(flush: true)) println "L'utente admin è stato creato"
        else println "Errore creazione utente admin: ${userAdmin.errors}"
        def userAdminPres= new Admin()
        userAdminPres.username= "ITADMINPRESS"
        userAdminPres.password="adminMach1"
        userAdminPres.nome="adminPressToday"
        userAdminPres.cognome="adminPressToday"
        userAdminPres.passwordScaduta=false

        if(userAdminPres.save(flush: true)) {
            userAdminPres.removeRuolo(Ruolo.ADMIN)
            userAdminPres.addRuolo(Ruolo.ADMINPRESSTODAY)
            println "L'utente admin presstoday è stato creato"
        }
        else println "Errore creazione utente admin posticipate: ${userAdminPres.errors}"

    }

    /**vecchio codice*/
    def caricaPraticaCSVOLD() {
        def logg
        if (request.post) {
            def speciale=params.speciale.toBoolean()
            def praticheNuove=[], errorePratica=[], flussiPAI=[]
            def dealerMancante=[]
            def file =params.excelPratica
            def filename=""
            def delimiter = ";"
            def fileReader = null
            def reader = null
            def erroreWS=""
            def dataPAIFin=new Date().parse("yyyyMMdd","20160930")
            def dataPAIIni=new Date().parse("yyyyMMdd","20160701")
            def listError =""
            def listPratiche=""
            def listFlussiPAI=""
            def listDeleted=""
            def totale=0
            def totaleErr=0
            boolean isPai=false
            if(file.bytes) {
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                reader = new InputStreamReader(myInputStream)
                fileReader = new BufferedReader(reader)
                fileReader.readLine()
                def risposta =[]
                def polizzeDeleted =[]
                Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$)/
                Pattern pattern = ~/(\w+)/
                String regex = "<(\\S+)[^>]+?mso-[^>]*>.*?</\\1>"
                def x_value=[]
                def dealer
                def dealers=[]
                try {
                    def line = ""
                    while ((line = fileReader.readLine()) != null) {
                        boolean sup75=false
                        boolean isDeleted=false
                        def tokens = []
                        tokens = line.split(delimiter)
                        def cellType = tokens[0].toString().trim().replaceAll ("'", "")
                        def cellnoPratica = tokens[1].toString().trim().replaceAll ("'", "")
                        def cellDataDecorrenza = tokens[2].toString().trim().replaceAll ("'", "")
                        def cellaDealer =tokens[3].toString().trim().replaceAll ("'", "")
                        def cellaNomeDealer =tokens[4].toString().trim().replaceAll ("'", "")
                        def cellaCognome  = tokens[5].toString().trim().replaceAll ("'", "")
                        def cellaNome  = tokens[6].toString().trim().replaceAll ("'", "")
                        def cellaBeneficiario=tokens[61].toString().trim().replaceAll ("'", "")
                        def cellaCF = tokens[7].toString().trim().replaceAll ("'", "")
                        def cellaTel = tokens[8].toString().trim().replaceAll ("'", "")
                        def cellaCel = tokens[9].toString().trim().replaceAll ("'", "")
                        def cellaLocalita = tokens[10].toString().trim().replaceAll ("'", "")
                        def cellaIndirizzo = tokens[11].toString().trim().replaceAll ("'", "")
                        def cellaProvincia = tokens[12].toString().trim().replaceAll ("'", "")
                        def cellaCap = tokens[13].toString().trim().replaceAll ("'", "")
                        def cellaPIva= tokens[15].toString().trim().replaceAll ("'", "")
                        def cellaEmail = tokens[16].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoCognome  = tokens[17].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoNome  = tokens[18].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoCF  = tokens[19].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoTEL  = tokens[20].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoCEL  = tokens[21].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoLOC  = tokens[22].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoIND  = tokens[23].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoPROV  = tokens[24].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoCAP  = tokens[25].toString().trim().replaceAll ("'", "")
                        def cellaCoobligatoemail  = tokens[28].toString().trim().replaceAll ("'", "")
                        def cellaDenominazione= tokens[29].toString().trim().replaceAll ("'", "")
                        def cellaTelefonoBusiness= tokens[31].toString().trim().replaceAll ("'", "")
                        def cellaIndirizzoBusiness= tokens[32].toString().trim().replaceAll ("'", "")
                        def cellaCellulareBusiness= tokens[33].toString().trim().replaceAll ("'", "")
                        def cellaProvinciaBusiness= tokens[34].toString().trim().replaceAll ("'", "")
                        def cellaLocalitaBusiness= tokens[22].toString().trim().replaceAll ("'", "")
                        def cellaCapBusiness= tokens[35].toString().trim().replaceAll ("'", "")
                        def cellaPIvaBusiness= tokens[36].toString().trim().replaceAll ("'", "")
                        def cellaCFBusiness= tokens[37].toString().trim().replaceAll ("'", "")
                        def cellaEmailBusiness= tokens[38].toString().trim().replaceAll ("'", "")
                        def cellaBeneFinanziato= tokens[39].toString().trim().replaceAll ("'", "")
                        def cellaDataImmatr= tokens[40].toString().trim().replaceAll ("'", "")
                        def cellaMarca= tokens[41].toString().trim().replaceAll ("'", "")
                        def cellaModello= tokens[42].toString().trim().replaceAll ("'", "")
                        def cellaVersione= tokens[43].toString().trim().replaceAll ("'", "")
                        def cellaTelaio= tokens[45].toString().trim().replaceAll ("'", "")
                        def cellaTarga= tokens[46].toString().trim().replaceAll ("'", "")
                        def cellaPrezzo= tokens[47].toString().trim().replaceAll ("'", "")
                        def cellaStep= tokens[49].toString().trim().replaceAll ("'", "")
                        def cellaProdotto= tokens[51].toString().trim().replaceAll ("'", "")
                        def cellaDurata= tokens[52].toString().trim().replaceAll ("'", "")
                        def cellaValoreAssi= tokens[53].toString().trim().replaceAll ("'", "")
                        def cellaPacchetto= tokens[54].toString().trim().replaceAll ("'", "")
                        def cellaPremioImpo= tokens[56].toString().trim().replaceAll ("'", "")
                        def cellaPremioLordo= tokens[57].toString().trim().replaceAll ("'", "")
                        def cellaProvvDealer= tokens[58].toString().trim().replaceAll ("'", "")
                        def cellaProvvVenditore= tokens[59].toString().trim().replaceAll ("'", "")
                        def cellaProvvGMFI= tokens[60].toString().trim().replaceAll ("'", "")
                        def cellaCertificato= tokens[62].toString().trim().replaceAll ("'", "")
                        def cellaVenditore= tokens[65].toString().trim().replaceAll ("'", "")
                        def cellatipoPolizza=tokens[68].toString().trim().replaceAll ("'", "")
                        def dataDecorrenza=null
                        if(cellDataDecorrenza.toString().trim()!='null' && cellDataDecorrenza.toString().length()>0){
                            def newDate = Date.parse( 'yyyyMMdd', cellDataDecorrenza )
                            dataDecorrenza = newDate
                            if((dataDecorrenza) && (dataDecorrenza>=dataPAIIni && dataDecorrenza<=dataPAIFin)){
                                isPai=true
                            }else{
                                isPai=false
                            }
                        }
                        if(cellnoPratica.toString().equals("null")){
                            cellnoPratica=""
                        }else{
                            cellnoPratica=cellnoPratica.toString()
                            cellnoPratica=cellnoPratica.toString().replaceFirst ("^0*", "")
                            def indexslash=cellnoPratica.indexOf("/")
                            if(indexslash>0){
                                cellnoPratica=cellnoPratica.toString().substring(0, indexslash)
                            }
                            if(cellnoPratica.toString().contains(".")){
                                def punto=cellnoPratica.toString().indexOf(".")
                                cellnoPratica=cellnoPratica.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                        }
                        def codOperazione="0"
                        if(cellType.toString().trim()!='null'){
                            if(cellType.toString().trim().equalsIgnoreCase("NEW")){
                                codOperazione="0"
                            }else if (cellType.toString().trim().equalsIgnoreCase("DELETED")){
                                isDeleted=true
                            }else if (cellType.toString().trim().equalsIgnoreCase("CLOSED")){
                                codOperazione="0"
                            }
                            else{
                                codOperazione="S"
                            }
                        }
                        if (isDeleted && cellnoPratica!=""){
                            polizzeDeleted.add( "${cellnoPratica},")
                            logg =new Log(parametri: "Errore creazione polizza: la polizza e' tipo DELETED", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            risposta.add("Errore creazione polizza: la polizza ${cellnoPratica} e' tipo DELETED,")
                            errorePratica.add("Errore creazione polizza: la polizza ${cellnoPratica} e' tipo DELETED,")
                            totaleErr++
                        }else if(!cellnoPratica){
                            logg =new Log(parametri: "Errore creazione polizza: non \u00E8 presente un numero di pratica", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            risposta.add("Errore creazione polizza: non \u00E8 presente un numero di pratica,")
                            errorePratica.add("Errore creazione polizza: non \u00E8 presente un numero di polizza,")
                            totaleErr++
                        }
                        else{
                            if(cellaNome.toString().equals("null")){
                                cellaNome="";
                            }else{
                                cellaNome=cellaNome.toString().trim()
                            }
                            if(cellaCognome.toString().equals("null")){
                                cellaCognome="";
                            }else{
                                cellaCognome=cellaCognome.toString().trim()
                            }
                            if(cellaCap.toString().equals("null")){
                                cellaCap="";
                            }else{
                                cellaCap=cellaCap.toString().replaceAll("'", "")
                                if(cellaCap.toString().contains(".")){
                                    def punto=cellaCap.toString().indexOf(".")
                                    cellaCap=cellaCap.toString().substring(0,punto)
                                }
                            }
                            if(cellaDealer.toString().equals("null")){
                                cellaDealer="";
                            }else{
                                cellaDealer=cellaDealer.toString().replaceAll ("'", "")
                                cellaDealer=cellaDealer.toString().replaceFirst ("^0*", "")
                                if(cellaDealer.toString().contains(".")){
                                    cellaDealer=Double.valueOf(cellaDealer).longValue().toString()
                                    //cellaDealer=cellaDealer.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                }
                                dealer=Dealer.findByCodice(cellaDealer.toString())
                            }
                            if(cellaCF.toString().equals("null") ){
                                cellaCF=cellaPIva.toString().replaceAll("[^a-z,A-Z,0-9]","")
                            }else{
                                cellaCF=cellaCF.toString().replaceAll("[^a-z,A-Z,0-9]","")
                            }
                            if(cellaMarca.toString().equals("null")){
                                cellaMarca=null;
                            }else{
                                cellaMarca=cellaMarca.toString().trim().toUpperCase()
                            }
                            if(cellaModello.toString().equals("null")){
                                cellaModello=null
                            }else{
                                cellaModello=cellaModello.toString().trim().toUpperCase()
                            }
                            if(cellaTel.toString().trim()!='null'){
                                cellaTel=cellaTel.toString().trim()
                                if(cellaTel.contains(".")){
                                    int zero=cellaTel.toString().indexOf(".")
                                    cellaTel=cellaTel.toString().substring(0,zero)
                                }
                            }else{cellaTel=null}
                            if(cellaTelefonoBusiness.toString().trim()!='null'){
                                cellaTelefonoBusiness=cellaTelefonoBusiness.toString().trim()
                                if(cellaTelefonoBusiness.contains(".")){
                                    int zero=cellaTelefonoBusiness.toString().indexOf(".")
                                    cellaTelefonoBusiness=cellaTelefonoBusiness.toString().substring(0,zero)
                                }

                            }else{cellaTelefonoBusiness=null}
                            if(cellaCel.toString().trim()!='null'){
                                cellaCel=cellaCel.toString().trim().replaceAll("[^0-9]+","")
                                if(cellaCel.toString().contains(".")){
                                    int zero=cellaCel.toString().indexOf(".")
                                    cellaCel=cellaCel.toString().substring(0,zero)
                                }
                            }else{
                                cellaCel=null
                            }
                            if(cellaCellulareBusiness.toString().trim()!='null'){
                                cellaCellulareBusiness=cellaCellulareBusiness.toString().trim().replaceAll("[^0-9]+","")
                                if(cellaCellulareBusiness.toString().contains(".")){
                                    int zero=cellaCellulareBusiness.toString().indexOf(".")
                                    cellaCellulareBusiness=cellaCellulareBusiness.toString().substring(0,zero)
                                }
                            }else{
                                cellaCellulareBusiness=null
                            }
                            def coperturaR
                            if(cellaProdotto.toString().trim()!='null' && cellaPacchetto.toString().trim().length()>0){
                                def pacchetto=""
                                def indexdue=cellaProdotto.toString().trim().indexOf("-")
                                pacchetto=cellaProdotto.toString().trim().substring(0,indexdue)
                                pacchetto=pacchetto.toString().trim()
                                if(pacchetto=="A"){
                                    coperturaR="SILVER"
                                }else if (pacchetto=="B"){
                                    coperturaR="GOLD"
                                }else if(pacchetto=="C"){
                                    coperturaR="PLATINUM"
                                }else if(pacchetto=="D"){
                                    coperturaR="PLATINUM COLLISIONE"
                                }else if(pacchetto=="E"){
                                    coperturaR="PLATINUM KASKO"
                                }
                            }
                            def nuovo=false
                            if(cellaBeneFinanziato.toString().trim()!='null'){
                                if(cellaBeneFinanziato.toString().trim().equalsIgnoreCase("NEW")){
                                    nuovo=true
                                }else{
                                    nuovo=false
                                }
                            }
                            def polizzaTipo=""
                            if(cellatipoPolizza.toString().trim()!='null'){
                                if(cellatipoPolizza.toString().trim().equalsIgnoreCase("RETAIL")){
                                    polizzaTipo=TipoPolizza.FINANZIATE
                                }else{
                                    polizzaTipo=TipoPolizza.LEASING
                                }
                            }
                            boolean onStar=false
                            boolean certificato=false
                            if(cellaCertificato.toString().trim()!=''){
                                if(cellaCertificato.toString().trim()!="N"){
                                    certificato=true
                                }else{
                                    certificato=false
                                }
                            }
                            if(cellaTelaio.toString().trim()!=''){
                                cellaTelaio=cellaTelaio.toString().trim().toUpperCase()
                            }else{
                                cellaTelaio=null
                            }
                            if(cellaTarga.toString().trim()!=''){
                                cellaTarga=cellaTarga.toString().trim().toUpperCase()
                            }else{
                                cellaTarga=null
                            }
                            if(cellaEmail.toString().trim()!=''){
                                cellaEmail=cellaEmail.toString().trim()
                            }else{
                                cellaEmail=null
                            }
                            def dataScadenza,dataEffetto
                            def durata
                            if(cellaDurata.toString().trim()!='null' && cellaDurata.toString().trim().length()>0){
                                if(cellaDurata.toString().contains(".")){
                                    def punto=cellaDurata.toString().indexOf(".")
                                    cellaDurata=cellaDurata.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                }
                                durata=Integer.parseInt(cellaDurata.toString().trim())
                            }else{
                                durata=null
                            }
                            if(durata!=null && dataDecorrenza!=null){

                                if(durata==12){
                                    dataScadenza= use(TimeCategory) {dataDecorrenza +12.months}
                                }else if( durata==18){
                                    dataScadenza= use(TimeCategory) {dataDecorrenza +18.months}
                                }else if( durata==24){
                                    dataScadenza= use(TimeCategory) {dataDecorrenza +24.months}
                                }else if( durata==30){
                                    dataScadenza= use(TimeCategory) {dataDecorrenza +30.months}
                                }else if(durata==36){
                                    dataScadenza=use(TimeCategory) {dataDecorrenza +36.months}
                                }else if( durata==42){
                                    dataScadenza= use(TimeCategory) {dataDecorrenza +42.months}
                                }else if(durata==48){
                                    dataScadenza= use(TimeCategory) {dataDecorrenza +48.months}
                                }else if( durata==54){
                                    dataScadenza= use(TimeCategory) {dataDecorrenza +54.months}
                                }else if(durata==60){
                                    dataScadenza=use(TimeCategory) {dataDecorrenza +60.months}
                                }else if(durata==66){
                                    dataScadenza=use(TimeCategory) {dataDecorrenza +66.months}
                                }else if(durata==72){
                                    dataScadenza=use(TimeCategory) {dataDecorrenza +72.months}
                                }else{
                                    dataScadenza=null
                                    durata=null
                                }
                            }
                            def step, codStep
                            if(cellaStep.toString().trim()!='null'){
                                step=cellaStep.toString().trim()
                                if(step.contains(".")){
                                    int zero=step.toString().indexOf(".")
                                    step=step.toString().substring(0,zero).toLowerCase()
                                }
                                if(step=="1"){ codStep="step 1"}
                                else if (step=="2"){codStep="step 2"}
                                else if (step=="3"){codStep="step 3"}
                            }else{codStep=null}
                            def dataImmatricolazione=null
                            if(cellaDataImmatr.toString().trim()!='null' && cellaDataImmatr.toString().length()>0){
                                def newDate = Date.parse( 'yyyyMMdd', cellaDataImmatr )
                                dataImmatricolazione = newDate
                            }
                            def valoreAssicurato
                            if(cellaValoreAssi.toString().trim()!='null' && cellaValoreAssi.toString().length()>0){
                                valoreAssicurato=cellaValoreAssi.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                //valoreAssicurato=cellaValoreAssi.toString().replaceAll(",","\\.")
                                valoreAssicurato = new BigDecimal(valoreAssicurato)
                            }else{
                                valoreAssicurato=0.0
                            }
                            def prezzoVendita
                            if(cellaPrezzo.toString().trim()!='null' && cellaPrezzo.toString().length()>0){
                                prezzoVendita=cellaPrezzo.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                //prezzoVendita=prezzoVendita.toString().replaceAll(",","\\.")
                                prezzoVendita = new BigDecimal(prezzoVendita)
                            }else{
                                prezzoVendita=0.0
                            }
                            def premioImponibile
                            if(cellaPremioImpo.toString().trim()!='null' && cellaPremioImpo.toString().trim().length()>0){
                                premioImponibile=cellaPremioImpo.toString().trim().replaceAll("\\.","").replaceAll(",","\\.")
                                //premioImponibile=cellaPremioImpo.toString().trim().replaceAll(",","\\.")
                                premioImponibile = new BigDecimal(premioImponibile)
                            }else{
                                premioImponibile=0.0
                            }
                            def premioLordo
                            if(cellaPremioLordo.toString().trim()!='null' && cellaPremioLordo.toString().trim().length()>0){
                                premioLordo=cellaPremioLordo.toString().trim().replaceAll("\\.","").replaceAll(",","\\.")
                                // premioLordo=cellaPremioLordo.toString().trim().replaceAll(",","\\.")
                                premioLordo = new BigDecimal(premioLordo)
                            }else{
                                premioLordo=0.0
                            }
                            def provvDealer
                            if(cellaProvvDealer.toString().trim()!='null' && cellaProvvDealer.toString().trim().length()>0){
                                provvDealer=cellaProvvDealer.toString().trim().replaceAll("\\.","").replaceAll(",","\\.")
                                //provvDealer=cellaProvvDealer.toString().trim().replaceAll(",","\\.")
                                provvDealer = new BigDecimal(provvDealer)
                            }else{
                                provvDealer=0.0
                            }
                            def provvGmfi
                            if(cellaProvvGMFI.toString().trim()!='null' && cellaProvvGMFI.toString().trim().length()>0){
                                provvGmfi=cellaProvvGMFI.toString().trim().replaceAll("\\.","").replaceAll(",",".")
                                //provvGmfi=cellaProvvGMFI.toString().trim().replaceAll(",",".")
                                provvGmfi = new BigDecimal(provvGmfi)
                            }else{
                                provvGmfi=0.0
                            }
                            def provvVendi
                            if(cellaProvvVenditore.toString().trim()!='' && cellaProvvVenditore.toString().trim().length()>0 && cellaProvvVenditore.toString().trim()!='0'){
                                provvVendi=cellaProvvVenditore.toString().trim().replaceAll("\\.","").replaceAll(",","\\.")
                                //provvVendi=cellaProvvVenditore.toString().trim().replaceAll(",","\\.")
                                provvVendi=new BigDecimal(provvVendi)
                            }else{
                                provvVendi=0.0
                            }
                            if(cellaVenditore.toString()!='null'){
                                cellaVenditore=cellaVenditore.toString().trim()
                            }else{
                                cellaVenditore=""
                            }
                            def dataInserimento=new Date()
                            def premioLordoSAntifurto=""
                            def premioLordoAntifurto=""
                            dataInserimento=dataInserimento.clearTime()
                            def parole=[]
                            def nomeB="", cognomeB=""
                            def cognomeDef,nomeDef,provinciaDef,localitaDef,capDef,telDef,cellDef,cfDef,indirizzoDef,tipoClienteDef,annoCF
                            def emailDef=""
                            boolean denominazione=false
                            boolean coobligato=false
                            boolean cliente=false
                            boolean nonTrovato=false
                            if(cellaBeneficiario.toString()!=''){
                                cellaBeneficiario=cellaBeneficiario.toUpperCase()
                                logg =new Log(parametri: "beneficiario-->${cellaBeneficiario}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                if(cellaDenominazione) {
                                    cellaDenominazione=cellaDenominazione.toUpperCase().trim()
                                    if (cellaDenominazione.contains(cellaBeneficiario)) {
                                        logg =new Log(parametri: "denominazione-->${cellaDenominazione}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        denominazione = true

                                    } else {
                                        denominazione = false
                                    }
                                }
                                if(cellaCoobligatoNome){
                                    cellaCoobligatoNome=cellaCoobligatoNome.toUpperCase().trim()
                                    cellaCoobligatoCognome=cellaCoobligatoCognome.toUpperCase().trim()
                                    def nomeCooC=cellaCoobligatoNome+" "+cellaCoobligatoCognome
                                    if(nomeCooC.contains(cellaBeneficiario)){
                                        logg =new Log(parametri: "nome Coobligato-->${nomeCooC}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        coobligato=true
                                    }else{
                                        coobligato = false
                                    }
                                }
                                if(cellaNome){
                                    cellaNome=cellaNome.toUpperCase().trim()
                                    cellaCognome=cellaCognome.toUpperCase().trim()
                                    def nomeCom=cellaNome+" "+cellaCognome
                                    if(nomeCom.contains(cellaBeneficiario)){
                                        logg =new Log(parametri: "nome Cliente-->${nomeCom}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        cliente=true
                                    }else{
                                        cliente = false
                                    }
                                }
                                // parole= cellaBeneficiario.split(" ")
                                //println parole
                                /*if(parole.size()==2){
                                    cognomeB=parole[1]
                                    cognomeB=cognomeB.trim()
                                    nomeB=parole[0]
                                    nomeB=nomeB.trim()
                                }else if(parole.size()>2){
                                    nomeB=parole[0]
                                    nomeB=nomeB.trim()
                                    for ( int ind=1; ind<parole.size();ind++) {
                                        cognomeB+=parole[ind]+" "
                                    }
                                    cognomeB=cognomeB.trim()
                                }else{
                                    nomeB=cellaBeneficiario.trim()
                                }*/
                                /*for(int i =0; i < parole.size(); i++)
                                {
                                    if(cellaDenominazione.contains(parole[i]))
                                    {
                                        denominazione= true
                                    }else{
                                        denominazione= false
                                    }
                                }
                                for(int i =0; i < parole.size(); i++)
                                {
                                    if(cellaCoobligatoNome.contains(parole[i]))
                                    {
                                        coobligato= true
                                    }else if(cellaCoobligatoCognome.contains(parole[i])){
                                        coobligato=true
                                    }else{
                                        coobligato = false
                                    }
                                }
                                for(int i =0; i < parole.size(); i++)
                                {

                                    if(cellaNome.contains(parole[i]))
                                    {
                                        cliente= true
                                    }else if(cellaCognome.contains(parole[i])){
                                        cliente=true
                                    }else{
                                        cliente = false
                                    }
                                }*/
                                if(coobligato){
                                    logg =new Log(parametri: "prendo i dati del Coobligato-->${cellaCoobligatoNome}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    cognomeDef=cellaCoobligatoCognome.toString().trim().toUpperCase()
                                    nomeDef=cellaCoobligatoNome.toString().trim().toUpperCase()
                                    provinciaDef=cellaCoobligatoPROV.toString().trim().toUpperCase()
                                    localitaDef=cellaCoobligatoLOC.toString().trim().toUpperCase()
                                    capDef=cellaCoobligatoCAP.toString().trim().toUpperCase()
                                    telDef=cellaCoobligatoTEL.toString().trim().toUpperCase()
                                    cellDef=cellaCoobligatoCEL.toString().trim().toUpperCase()
                                    cfDef=cellaCoobligatoCF.toString().trim().toUpperCase()
                                    //prendo i dati dell'anno dal codice fiscale
                                    if(cfDef){
                                        annoCF=cfDef.substring(6,8)
                                        if(Integer.parseInt(annoCF) && Integer.parseInt(annoCF)<=40){
                                            sup75=true
                                        }
                                    }
                                    indirizzoDef=cellaCoobligatoIND.toString().trim().toUpperCase()
                                    tipoClienteDef=TipoCliente.M
                                    if(cellaCoobligatoemail.toString().trim().size()>4){
                                        emailDef=cellaCoobligatoemail.toString().trim()
                                    }
                                }else if(denominazione){
                                    logg =new Log(parametri: "prendo i dati della denominazione -->${cellaDenominazione}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    cognomeDef=cellaDenominazione.toString().trim().toUpperCase()
                                    nomeDef=""
                                    provinciaDef=cellaProvinciaBusiness.toString().trim().toUpperCase()
                                    localitaDef=cellaLocalitaBusiness.toString().trim().toUpperCase()
                                    capDef=cellaCapBusiness.toString().trim().toUpperCase()
                                    telDef=cellaTelefonoBusiness.toString().trim().toUpperCase()
                                    cellDef=cellaCellulareBusiness.toString().trim().toUpperCase()
                                    if(cellaPIvaBusiness){
                                        cfDef=cellaPIvaBusiness.toString().trim().toUpperCase()
                                        if(!cfDef.matches("[0-9]{11}")){
                                            cfDef=cfDef.padLeft(11,"0")
                                        }
                                    }


                                    indirizzoDef=cellaIndirizzoBusiness.toString().trim().toUpperCase()
                                    tipoClienteDef=TipoCliente.DITTA_INDIVIDUALE
                                    if(cellaEmailBusiness.toString().trim().size()>4){
                                        emailDef=cellaEmailBusiness.toString().trim()
                                    }
                                }else if(cliente){
                                    logg =new Log(parametri: "prendo i dati del cliente -->${cellaNome}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    cognomeDef=cellaCognome.toString().trim().toUpperCase()
                                    nomeDef=cellaNome.toString().trim().toUpperCase()
                                    provinciaDef=cellaProvincia.toString().trim().toUpperCase()
                                    localitaDef=cellaLocalita.toString().trim().toUpperCase()
                                    capDef=cellaCap.toString().trim().toUpperCase()
                                    telDef=cellaTel.toString().trim().toUpperCase()
                                    cellDef=cellaCel.toString().trim().toUpperCase()
                                    cfDef=cellaCF.toString().trim().toUpperCase()
                                    if(cfDef){
                                        annoCF=cfDef.substring(6,8)
                                        if(Integer.parseInt(annoCF) && Integer.parseInt(annoCF)<=40){
                                            sup75=true
                                        }
                                    }
                                    indirizzoDef=cellaIndirizzo.toString().trim().toUpperCase()
                                    tipoClienteDef=TipoCliente.M
                                    if(cellaEmail.toString().trim().size()>4){
                                        emailDef=cellaEmail.toString().trim()
                                    }

                                }else{
                                    nonTrovato=true
                                }
                            }
                            if(nonTrovato){
                                logg =new Log(parametri: "il beneficiario non appare nelle colonne dei dati coobligato/cliente/business, controllare file", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                errorePratica.add("per la pratica-->${cellnoPratica} il beneficiario non appare nelle colonne dei dati coobligato/cliente/business, controllare file,")
                                totaleErr++
                            }else{
                                //verifica tot provv rispetto a step assegnato
                                def provincia=provinciaDef.toString().trim()
                                def calcoloImp, totProvvi, diffImpoProvv
                                totProvvi=provvDealer+provvVendi+provvGmfi
                                if((provincia.trim()!="") && (provincia.trim() !=null) && (provincia.trim() !='null')){
                                    def zonaT=ZoneTarif.findByProvincia(provincia).zona
                                    if(zonaT!="8"){
                                        if(step=="1"){
                                            calcoloImp= premioImponibile*0.30
                                        }else if(step=="2"){
                                            calcoloImp= premioImponibile*0.40
                                        }else if(step=="3"){
                                            calcoloImp= premioImponibile*0.50
                                        }
                                    }else{
                                        if(step=="1"){
                                            calcoloImp= premioImponibile*0.27
                                        }else if(step=="2"){
                                            calcoloImp= premioImponibile*0.37
                                        }else if(step=="3"){
                                            calcoloImp= premioImponibile*0.47
                                        }
                                    }

                                    logg =new Log(parametri: "parametri letti: premio imponibile-->${premioImponibile}, step --> ${step}, zona t  --> ${zonaT}, calcolo imponibile --> ${calcoloImp}, provvDealer   --> ${provvDealer}, provvVendi   --> ${provvVendi}, provvGmfi   --> ${provvGmfi}, totProvvi   --> ${totProvvi}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    diffImpoProvv=Math.abs(Math.round((calcoloImp-totProvvi)*100)/100)
                                    logg =new Log(parametri: "differenza imponibile provvigioni -> ${diffImpoProvv}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    if(diffImpoProvv<=0.05 && diffImpoProvv!=""){
                                        if(dealer && cellnoPratica && cellaTelaio && cellaBeneficiario.toString()!='' ){
                                            def polizzaesistente= Polizza.findByNoPolizzaIlike(cellnoPratica)
                                            if (polizzaesistente){
                                                if(codOperazione=="S" && !speciale){
                                                    if(polizzaesistente.telaio.equalsIgnoreCase(cellaTelaio)){
                                                        polizzaesistente.codOperazione=codOperazione
                                                        if(polizzaesistente.save(flush:true)) {
                                                            def tracciatoPolizzaR= tracciatiService.generaTracciatoIAssicur(polizzaesistente)
                                                            if(!tracciatoPolizzaR.save(flush: true)){
                                                                logg =new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                risposta.add(noPratica:cellnoPratica, errore:"Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors}")
                                                                errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors},")
                                                                totaleErr++
                                                            }else{
                                                                def tracciatoComPolizzaR= tracciatiService.generaTracciatoCompagnia(polizzaesistente)
                                                                if(!tracciatoComPolizzaR.save(flush: true)){
                                                                    logg =new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    risposta.add(noPratica:cellnoPratica, errore:"Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors}")
                                                                    errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors},")
                                                                    totaleErr++
                                                                }else{
                                                                    if(polizzaesistente.tracciatoPAIId){
                                                                        def tracciatoPAIPolizzaR= tracciatiService.generaTracciatoPAI(polizzaesistente, speciale)
                                                                        if(!tracciatoPAIPolizzaR.save(flush: true)){
                                                                            logg =new Log(parametri: "Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            risposta.add(noPratica:cellnoPratica, errore:"Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors}")
                                                                            errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors},")
                                                                            totaleErr++
                                                                        } else{
                                                                            if(polizzaesistente.targa != null && polizzaesistente.targa != ''){
                                                                                polizzaesistente.targa=polizzaesistente.targa+"_R"
                                                                            }
                                                                            if(polizzaesistente.telaio != null && polizzaesistente.telaio!= ''){
                                                                                polizzaesistente.telaio=polizzaesistente.telaio+"_R"
                                                                            }
                                                                            if(polizzaesistente.save(flush:true)) {
                                                                                logg =new Log(parametri: "targa e telaio per la polizza ${polizzaesistente.noPolizza} aggiornati", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                praticheNuove.add("polizza stornata correttamente ${polizzaesistente.noPolizza},")
                                                                                totale++
                                                                            }else{
                                                                                logg =new Log(parametri: "non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                risposta.add(noPratica:cellnoPratica, errore:"non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}")
                                                                                errorePratica.add(" pratica-->${cellnoPratica} non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors},")
                                                                                totaleErr++
                                                                            }
                                                                        }
                                                                    }else{
                                                                        if(polizzaesistente.targa != null && polizzaesistente.targa != ''){
                                                                            polizzaesistente.targa=polizzaesistente.targa+"_R"
                                                                        }
                                                                        if(polizzaesistente.telaio != null && polizzaesistente.telaio!= ''){
                                                                            polizzaesistente.telaio=polizzaesistente.telaio+"_R"
                                                                        }
                                                                        if(polizzaesistente.save(flush:true)) {
                                                                            logg =new Log(parametri: "targa e telaio per la polizza ${polizzaesistente.noPolizza} aggiornati", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            praticheNuove.add("polizza stornata correttamente ${polizzaesistente.noPolizza},")
                                                                            totale++
                                                                        }else{
                                                                            logg =new Log(parametri: "non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            risposta.add(noPratica:cellnoPratica, errore:"non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}")
                                                                            errorePratica.add(" pratica-->${cellnoPratica} non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors},")
                                                                            totaleErr++
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    }else{
                                                        risposta.add(noPratica:cellnoPratica, errore:"il telaio associato a questa pratica non corrisponde a quello inserito nel DB")
                                                        errorePratica.add("per questa pratica-->${cellnoPratica} il telaio associato non corrisponde a quello inserito nel DB,")
                                                        totaleErr++
                                                    }
                                                }
                                                else if(speciale && !sup75){
                                                    logg =new Log(parametri: "genero tracciato PAI", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    def tracciatoPAI= tracciatiService.generaTracciatoPAI(polizzaesistente, speciale)
                                                    if(!tracciatoPAI.save(flush: true)){
                                                        logg =new Log(parametri: "Errore creazione tracciato PAI: ${tracciatoPAI.errors}", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        risposta.add(noPratica:cellnoPratica, errore:"Errore creazione tracciato PAI: ${tracciatoPAI.errors}")
                                                        errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAI.errors},")
                                                        totaleErr++
                                                    } else{
                                                        logg =new Log(parametri: "tracciato PAI generato correttamente per la polizza ${polizzaesistente.noPolizza}", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        flussiPAI.add("tracciato PAI generato correttamente per la polizza ${polizzaesistente.noPolizza},")
                                                        totale++
                                                    }
                                                }else if(sup75){
                                                    errorePratica.add("il beneficiario della pratica-->${cellnoPratica}  supera la eta' di 75 anni,")
                                                    totaleErr++
                                                }
                                                else{
                                                    errorePratica.add("la pratica-->${cellnoPratica} \u00E8 gia' stata caricata nel portale,")
                                                    totaleErr++
                                                }
                                            }else{
                                                if(codOperazione=="S"){
                                                    risposta.add(noPratica:cellnoPratica, errore:"la polizza che si sta cercando di stornare, non esiste, verificare no polizza")
                                                    errorePratica.add("la pratica-->${cellnoPratica} che si sta cercando di stornare non esiste. Verificare no polizza,")
                                                    totaleErr++
                                                }else if(speciale){
                                                    risposta.add(noPratica:cellnoPratica, errore:"la polizza non esiste e non e' possibile generare un flusso PAI... verificare no polizza")
                                                    errorePratica.add("la pratica-->${cellnoPratica} non esiste e non e' possibile generare un flusso PAI... verificare no polizza,")
                                                    totaleErr++
                                                }
                                                else{
                                                    //chiamata webservice con onstar false
                                                    def comune=localitaDef.toString().trim()
                                                    comune=comune.toString().toUpperCase()
                                                    def antifurto="N"
                                                    def test="N"
                                                    /*if(Environment.current == Environment.PRODUCTION) {
                                                        test="N"
                                                    }else{
                                                        test="S"
                                                    }*/
                                                    def dealerType=dealer.dealerType
                                                    def dataeffettocopertura= dataDecorrenza?dataDecorrenza.format("yyyy-MM-dd"):null
                                                    def pacchetto=CodProdotti.findByCoperturaAndStep(coperturaR, codStep)
                                                    def percentualeVenditore=dealer.percVenditore
                                                    def esito, provvDealerWS,provvgmfiWS,provvVendWS,premioLordoWS,premioImponibileWS,codiceZonaTerritoriale,rappel,imposte

                                                    if((provincia.trim()!="") && (provincia.trim() !='null') && pacchetto.codPacchetto!="" && durata!=null && comune.trim()!="" && comune.trim()!='NULL' && dealerType!="" && step!=""&& percentualeVenditore!=""&& dataeffettocopertura!=null && valoreAssicurato!="" && valoreAssicurato > 0.0 ){
                                                        def parameters  =[
                                                                codicepacchetto: "${pacchetto.codPacchetto}",
                                                                valoreassicurato: valoreAssicurato,
                                                                durata: durata,
                                                                provincia: provincia,
                                                                comune: comune,
                                                                operazione: "A",
                                                                dataeffettocopertura: dataeffettocopertura,
                                                                antifurto: antifurto,
                                                                dealerType: dealerType,
                                                                classeProvvigionale: step,
                                                                percentualeVenditore: percentualeVenditore,
                                                                test:test/*,
                                                                vehiclemodel:cellaModello*/
                                                        ]
                                                        logg =new Log(parametri: "star false -> parametri inviate al webservice->${parameters}", operazione: "carica polizze fin/lea chiamata ws", pagina: "INSERIMENTO/MODIFICA polizze FIN/LEA")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        def url = "http://gmf.mach-1.it"
                                                        def js = (parameters as JSON).toString()
                                                        js = js.substring(1, js.length()-1)
                                                        def path = "/calcprovvigioni/"
                                                        /*if(Environment.current != Environment.PRODUCTION){
                                                            path = "/test/calcprovvigioni/"
                                                        }*/
                                                        def query = "{"+js+"}"
                                                        def errore=""
                                                        def resp = DLWebService.postText(url, path, [p:query])
                                                        boolean rispostaT=false
                                                        JSONObject userJson = JSON.parse(resp)
                                                        userJson.each { id, data ->
                                                            logg =new Log(parametri: "onstar false --> ${data}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            if(data.Errors){
                                                                def desErrore=data.Errors?.descErrore.toString().replaceAll("\\[","").replaceAll("]","")
                                                                if(!((desErrore.contains("Comune non identificato"))|| (desErrore.contains("Data Effetto non coerente con")))){
                                                                    rispostaT=false
                                                                    errore = data.Errors?.descErrore.toString().replaceAll("\\[","").replaceAll("]","")
                                                                    logg =new Log(parametri: "errore --> ${errore}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }else if (((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con"))) &&!( desErrore.contains("Provincia non valida"))){
                                                                    logg =new Log(parametri: "errore (comune non identificato) --> ${desErrore}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    rispostaT=true
                                                                }
                                                                else if (((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con")))&& desErrore.contains("Provincia non valida")){
                                                                    rispostaT=false
                                                                    errore = data.Errors?.descErrore.toString().replaceAll("\\[","").replaceAll("]","")
                                                                    logg =new Log(parametri: "errore (comune non identificato - provincia non valida) --> ${errore}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }
                                                            }else{
                                                                rispostaT=true
                                                            }
                                                            if(rispostaT){
                                                                esito=data.Esito ?:1
                                                                premioLordoWS=data.PremioLordo?:0.0
                                                                premioImponibileWS=data.Premio?:0.0
                                                                provvDealerWS=data.provvigioneDealer?:0.0
                                                                provvgmfiWS=data.provvigioneGMF?:0.0
                                                                provvVendWS=data.provvigioneVenditore?:0.0
                                                                codiceZonaTerritoriale=data.IPRPremioCalcolato.codiceZonaTerritoriale.toString().replaceAll("\\[","").replaceAll("]","")?:""
                                                                rappel=data.IPRPremioCalcolato.rappel.toString().replaceAll("\\[","").replaceAll("]","")?:0.0
                                                                imposte=data.IPRPremioCalcolato.imposte.toString().replaceAll("\\[","").replaceAll("]","")?:0.0
                                                                rappel = new BigDecimal(rappel)
                                                                imposte = new BigDecimal(imposte)
                                                            }
                                                        }
                                                        if(rispostaT){
                                                            logg =new Log(parametri: "premio lordo ws on star false -> ${premioLordoWS}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            premioLordoSAntifurto=premioLordoWS
                                                            logg =new Log(parametri: "premio lordo csv ->${premioLordo}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            if(premioLordoWS==premioLordo){
                                                                if(rappel==0.0){
                                                                    erroreWS=" pratica-->${cellnoPratica} Attenzione per questa pratica il valore del rappel e' uguale a 0, "
                                                                    totaleErr++
                                                                }
                                                                logg =new Log(parametri: "il premio lordo è uguale on star false-> ${premioLordo}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                onStar=false
                                                            }
                                                            else{
                                                                def diffPremio=Math.abs(Math.round((premioLordoWS-premioLordo)*100)/100)
                                                                if(diffPremio<=0.05){
                                                                    onStar=false
                                                                    logg =new Log(parametri: "il premio lordo è uguale on star false per poca differenza-> ${diffPremio}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }else{
                                                                    antifurto="S"
                                                                    parameters  =[
                                                                            codicepacchetto: "${pacchetto.codPacchetto}",
                                                                            valoreassicurato: valoreAssicurato,
                                                                            durata: durata,
                                                                            provincia: provincia,
                                                                            comune: comune,
                                                                            operazione: "A",
                                                                            dataeffettocopertura: dataeffettocopertura,
                                                                            antifurto: antifurto,
                                                                            dealerType: dealerType,
                                                                            classeProvvigionale: step,
                                                                            percentualeVenditore: percentualeVenditore,
                                                                            test:test/*,
                                                                            vehiclemodel:cellaModello*/
                                                                    ]
                                                                    logg =new Log(parametri: "star true -> parametri inviate al webservice->${parameters}", operazione: "carica polizze fin/lea chiamata ws", pagina: "INSERIMENTO/MODIFICA polizze FIN/LEA")
                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    url = "http://gmf.mach-1.it"
                                                                    js = (parameters as JSON).toString()
                                                                    js = js.substring(1, js.length()-1)
                                                                    path = "/calcprovvigioni/"
                                                                    /*if(Environment.current == Environment.DEVELOPMENT){
                                                                        path = "/test/calcprovvigioni/"
                                                                    }*/
                                                                    query = "{"+js+"}"
                                                                    errore=""
                                                                    resp = DLWebService.postText(url, path, [p:query])
                                                                    rispostaT=false
                                                                    userJson = JSON.parse(resp)
                                                                    userJson.each { id, data ->
                                                                        logg =new Log(parametri: "onstar false --> ${data}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        if(data.Errors){
                                                                            def desErrore=data.Errors?.descErrore.toString().replaceAll("\\[","").replaceAll("]","")
                                                                            if(!((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con")))){
                                                                                rispostaT=false
                                                                                errore = data.Errors?.descErrore.toString().replaceAll("\\[","").replaceAll("]","")
                                                                                logg =new Log(parametri: "errore --> ${errore}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            }else if (((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con"))) &&!( desErrore.contains("Provincia non valida"))){
                                                                                logg =new Log(parametri: "errore (comune non identificato) --> ${desErrore}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                rispostaT=true
                                                                            }
                                                                            else if (((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con")) )&& desErrore.contains("Provincia non valida")){
                                                                                rispostaT=false
                                                                                logg =new Log(parametri: "errore (comune non identificato - provincia non valida) --> ${desErrore}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            }
                                                                        }else{
                                                                            rispostaT=true
                                                                        }
                                                                        if(rispostaT){
                                                                            esito=data.Esito ?:1
                                                                            premioLordoWS=data.PremioLordo?:0.0
                                                                            premioImponibileWS=data.Premio?:0.0
                                                                            provvDealerWS=data.provvigioneDealer?:0.0
                                                                            provvgmfiWS=data.provvigioneGMF?:0.0
                                                                            provvVendWS=data.provvigioneVenditore?:0.0
                                                                            codiceZonaTerritoriale=data.IPRPremioCalcolato.codiceZonaTerritoriale.toString().replaceAll("\\[","").replaceAll("]","")?:""
                                                                            rappel=data.IPRPremioCalcolato.rappel.toString().replaceAll("\\[","").replaceAll("]","")?:0.0
                                                                            imposte=data.IPRPremioCalcolato.imposte.toString().replaceAll("\\[","").replaceAll("]","")?:0.0
                                                                            rappel = new BigDecimal(rappel)
                                                                            imposte = new BigDecimal(imposte)
                                                                        }

                                                                    }
                                                                    if(rispostaT){

                                                                        premioLordoAntifurto=premioLordoWS
                                                                        logg =new Log(parametri: "premio lordo ws on star true -> ${premioLordoWS}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        logg =new Log(parametri: "premio lordo csv ->${premioLordo}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        if(premioLordoWS==premioLordo){
                                                                            logg =new Log(parametri: "il premio lordo \u00E8 uguale on star true -> ${premioLordo}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            onStar=true
                                                                            if(rappel==0.0){
                                                                                erroreWS=" pratica-->${cellnoPratica} Attenzione per questa pratica il valore del rappel e' uguale a 0, "
                                                                                totaleErr++
                                                                            }
                                                                        }else{
                                                                            diffPremio=Math.abs(Math.round((premioLordoWS-premioLordo)*100)/100)
                                                                            if(diffPremio<=0.05){
                                                                                logg =new Log(parametri: "il premio lordo \u00E8 uguale on star true con poca differenza -> ${diffPremio}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                onStar=true
                                                                            }else{
                                                                                risposta.add(noPratica:cellnoPratica, errore:"Errore il premio lordo non corrisponde in nessuno dei casi con il premio lordo del webservice: con antifurto: ${premioLordoAntifurto?:""}/ senza antifurto: ${premioLordoSAntifurto?:""}, ")
                                                                                erroreWS=" pratica-->${cellnoPratica} Errore il premio lordo non corrisponde in nessuno dei casi con il premio lordo del webservice: con antifurto: ${premioLordoAntifurto ?:""}/ senza antifurto: ${premioLordoSAntifurto ?:""}, "
                                                                                totaleErr++
                                                                            }
                                                                        }
                                                                    }else{
                                                                        risposta.add(noPratica:cellnoPratica, errore:"Errore chiamata webservice ${errore}")
                                                                        erroreWS=" pratica-->${cellnoPratica} ha dato il seguente errore nella chiamata webservice ${errore}"
                                                                        totaleErr++
                                                                    }
                                                                }
                                                                //richiamo il ws con on star uguale a si
                                                            }
                                                        }else{
                                                            risposta.add(noPratica:cellnoPratica, errore:"Errore chiamata webservice ${errore},")
                                                            erroreWS="la pratica-->${cellnoPratica} ha dato il seguente errore nella chiamata webservice ${errore},"
                                                            totaleErr++
                                                        }
                                                    }else{
                                                        erroreWS="per la pratica-->${cellnoPratica} controllare i valori: -durata -provincia  -dealer  -comune -valore assicurato -data decorrenza,"
                                                        totaleErr++
                                                    }
                                                    if(erroreWS.length()>0){
                                                    }else{
                                                        def polizza = Polizza.findOrCreateWhere(
                                                                annullata:false,
                                                                cap: capDef.toString().trim().padLeft(5,"0"),
                                                                cellulare:  cellDef.toString().trim(),
                                                                cognome:  cognomeDef.toString().trim(),
                                                                coperturaRichiesta: coperturaR,
                                                                dataDecorrenza:dataDecorrenza,
                                                                dataImmatricolazione: dataImmatricolazione,
                                                                dataInserimento: dataInserimento,
                                                                dataScadenza: dataScadenza,
                                                                dealer: dealer,
                                                                durata:durata,
                                                                email: emailDef.toString().trim(),
                                                                indirizzo: indirizzoDef.toString().trim().toUpperCase(),
                                                                localita: localitaDef.toString().trim().toUpperCase(),
                                                                marca: cellaMarca,
                                                                modello: cellaModello,
                                                                noPolizza: cellnoPratica,
                                                                nome: nomeDef.toString().trim().toUpperCase(),
                                                                nuovo:nuovo,
                                                                onStar: onStar,
                                                                partitaIva: cfDef.toString().trim().toUpperCase(),
                                                                premioImponibile:premioImponibile,
                                                                premioLordo:premioLordo,
                                                                provincia: provinciaDef.toString().trim().toUpperCase(),
                                                                provvDealer:provvDealer,
                                                                provvGmfi:provvGmfi,
                                                                provvVenditore:provvVendi,
                                                                stato:StatoPolizza.POLIZZA,
                                                                tipoCliente:tipoClienteDef,
                                                                tipoPolizza:polizzaTipo,
                                                                step:codStep,
                                                                telaio:cellaTelaio,
                                                                targa:cellaTarga,
                                                                telefono:telDef.toString().trim(),
                                                                valoreAssicurato:valoreAssicurato,
                                                                valoreAssicuratoconiva:prezzoVendita,
                                                                certificatoMail:certificato,
                                                                codiceZonaTerritoriale: codiceZonaTerritoriale,
                                                                rappel:rappel,
                                                                codOperazione:codOperazione,
                                                                dSlip:"S",
                                                                venditore:cellaVenditore?cellaVenditore.toString().trim().toUpperCase():"",
                                                                imposte:imposte
                                                        )
                                                        if(polizza.save(flush:true)) {
                                                            logg =new Log(parametri: "la polizza viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                                                            if(!tracciatoPolizza.save(flush: true)){
                                                                risposta.add(noPratica:cellnoPratica, errore:"Errore creazione tracciato iassicur: ${tracciatoPolizza.errors}")
                                                                errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato iassicur: ${tracciatoPolizza.errors},")
                                                                totaleErr++
                                                            }else{
                                                                logg =new Log(parametri: "genero tracciato IAssicur", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                def tracciatoComPolizza= tracciatiService.generaTracciatoCompagnia(polizza)
                                                                if(!tracciatoComPolizza.save(flush: true)){
                                                                    risposta.add(noPratica:cellnoPratica, errore:"Errore creazione tracciato compagnia: ${tracciatoComPolizza.errors}")
                                                                    errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato compagnia: ${tracciatoComPolizza.errors},")
                                                                    totaleErr++
                                                                } else{
                                                                    logg =new Log(parametri: "genero tracciato Compagnia", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    if(isPai){
                                                                        logg =new Log(parametri: "genero tracciato PAI", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        def tracciatoPAI= tracciatiService.generaTracciatoPAI(polizza, speciale)
                                                                        if(!tracciatoPAI.save(flush: true)){
                                                                            logg =new Log(parametri: "Errore creazione tracciato PAI: ${tracciatoPAI.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            risposta.add(noPratica:cellnoPratica, errore:"Errore creazione tracciato PAI: ${tracciatoPAI.errors}")
                                                                            errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAI.errors},")
                                                                            totaleErr++
                                                                        } else{
                                                                            logg =new Log(parametri: "polizza creata correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            praticheNuove.add("polizza creata correttamente ${polizza.noPolizza},")
                                                                            totale++
                                                                            //risposta.add("per la polizza: ${polizza.noPolizza} la mail e' stata inviata al dealer ${polizza.dealer.ragioneSocialeD} e a ${polizza.dealer.salesSpecialist} ")
                                                                            //praticheNuove.add("per la polizza: ${polizza.noPolizza} la mail e' stata inviata al dealer ${polizza.dealer.ragioneSocialeD} e a ${polizza.dealer.salesSpecialist} ")
                                                                            /*def mailInviato= mailService.invioMailCertificato(dealer,polizza)
                                                                              println mailInviato
                                                                              if((mailInviato.contains("queued")||mailInviato.contains("sent") ||mailInviato.contains("scheduled")) && !mailInviato.contains("errore->") ){
                                                                                  praticheNuove.add("polizza creata correttamente ${polizza.noPolizza}")
                                                                                  risposta.add("per la polizza: ${polizza.noPolizza} la mail e' stata inviata al dealer ${polizza.dealer.ragioneSocialeD} e a ${polizza.dealer.salesSpecialist} ")
                                                                                  praticheNuove.add("per la polizza: ${polizza.noPolizza} la mail e' stata inviata al dealer ${polizza.dealer.ragioneSocialeD} e a ${polizza.dealer.salesSpecialist} ")
                                                                              }else{
                                                                                  println mailInviato
                                                                                  risposta.add("Errore invio mail  polizza: ${mailInviato}")
                                                                                  errorePratica.add("Errore invio mail  polizza: ${mailInviato}")
                                                                                  praticheNuove.add("polizza creata correttamente ${polizza.noPolizza}")
                                                                              }*/
                                                                        }
                                                                    }else{
                                                                        logg =new Log(parametri: "polizza creata correttamente ${polizza.noPolizza}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        praticheNuove.add("polizza creata correttamente ${polizza.noPolizza},")
                                                                        totale++
                                                                    }
                                                                }
                                                            }
                                                        }
                                                        else{
                                                            logg =new Log(parametri: "Errore creazione polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            risposta.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: ${polizza.errors}")
                                                            errorePratica.add(" pratica-->${cellnoPratica} Errore creazione polizza: ${polizza.errors},")
                                                            totaleErr++
                                                        }
                                                    }
                                                }
                                            }
                                        }else if(!dealer){
                                            logg =new Log(parametri: "Errore creazione polizza: il dealer non \u00E8 presente", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            risposta.add("Errore creazione polizza: il dealer non \u00E8 presente")
                                            errorePratica.add("Errore creazione polizza: il dealer ${cellaDealer}- ${cellaNomeDealer} non \u00E8 presente,")
                                            totaleErr++
                                        }else if(!cellnoPratica){
                                            logg =new Log(parametri: "Errore creazione polizza: non \u00E8 presente un numero di pratica", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            risposta.add("Errore creazione polizza: non \u00E8 presente un numero di pratica,")
                                            errorePratica.add("Errore creazione polizza: non \u00E8 presente un numero di polizza,")
                                            totaleErr++
                                        }
                                        if(erroreWS.length()>0){
                                            errorePratica.add(erroreWS)
                                            erroreWS=""
                                        }
                                    }else{
                                        logg =new Log(parametri: "Errore creazione polizza:${cellnoPratica} la somma delle provviggioni-> ${totProvvi} non corrisponde con la percentuale imponibile dello step assegnato-> ${calcoloImp}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        risposta.add("Errore creazione polizza:${cellnoPratica} la somma delle provviggioni-> ${totProvvi} non corrisponde con la percentuale imponibile dello step assegnato-> ${calcoloImp},")
                                        errorePratica.add("Errore creazione polizza:${cellnoPratica} la somma delle provviggioni-> ${totProvvi} non corrisponde con la percentuale imponibile dello step assegnato-> ${calcoloImp},")
                                        totaleErr++
                                    }
                                }else{
                                    logg =new Log(parametri: "Errore creazione polizza:${cellnoPratica} la provincia non \u00E8 stata dichiarata, controllare il file", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    risposta.add("Errore creazione polizza:${cellnoPratica} la provincia non \u00E8 stata dichiarata, controllare il file,")
                                    errorePratica.add("Errore creazione polizza:${cellnoPratica} la provincia non \u00E8 stata dichiarata, controllare il file,")
                                    totaleErr++
                                }
                            }
                        }
                    }

                }catch (e){
                    logg =new Log(parametri: "c\u00E8 un problema con il caricamento del file ${e.toString()}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    risposta.add("c\u00E8  un problema con il caricamento del file")
                    errorePratica.add("c\u00E8  un problema con il caricamento del file ")
                }

                if(polizzeDeleted.size()>0){
                    for(String s: polizzeDeleted)
                    {
                        listDeleted+=s+"\r\n\r\n"
                    }
                    def mailInviato= mailService.invioMailDeleted(listDeleted)
                    logg =new Log(parametri: "risposta dell'invio della mail POLIZZE DELETED  ${mailInviato}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    if((mailInviato.contains("queued")||mailInviato.contains("sent") ||mailInviato.contains("scheduled")) && !mailInviato.contains("errore->") ){
                        praticheNuove.add("mail riassunto polizze DELETED inviata correttamente a assunta.carnovale@gmfinancial.com, ")
                    }else{
                        risposta.add("Errore invio mail  polizza: ${mailInviato}")
                        errorePratica.add("Errore invio mail  polizza: ${mailInviato}")
                    }
                }
                if(errorePratica.size()>0){
                    for (String s : errorePratica)
                    {
                        listError += s + "\r\n\r\n"
                    }
                    //println(listError)
                }

                if(praticheNuove.size()>0){
                    for (String s : praticheNuove)
                    {
                        listPratiche += s + "\r\n\r\n"
                    }
                    //println(listPratiche)
                }
                if(flussiPAI.size()>0){
                    for (String s : flussiPAI)
                    {
                        listFlussiPAI += s + "\r\n\r\n"
                    }
                }

                if(errorePratica.size()>0){
                    flash.errorePratica=listError+="totale polizze non caricate ${totaleErr}"
                }
                if(praticheNuove.size()>0){
                    flash.praticheNuove=listPratiche+="totale  polizze caricate ${totale}"
                }
                if(flussiPAI.size()>0){
                    flash.flussiPAI=listFlussiPAI+="totale  flussi generati ${totale}"
                }
            } else {flash.error = "Specificare l'elenco csv"}

            redirect action: "listFinanziate"
        }else response.sendError(404)
    }
    def generaFileGMFGENold(){
        def logg
        if (request.post) {
            try {
                def mese=params.mese
                def anno=params.anno
                def resultwebS = IAssicurWebService.getGMFGENFINLEACASHRINN(mese, anno)
                def polizzeWS = resultwebS.polizze
                logg =new Log(parametri: "numero di polizze trovate su IAssciur nel periodo selezionato ${polizzeWS.size()} ", operazione: "generaFileGMFGEN", pagina: "LISTA POLIZZE CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                def  fmt_IT = NumberFormat.getNumberInstance(Locale.ITALIAN)
                //fmt_IT.setParseBigDecimal(true)
                fmt_IT.setMaximumFractionDigits(2)
                if (polizzeWS) {
                    def filegmfgen=GmfgenService.fileGMFGEN(polizzeWS)

                    response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    response.addHeader "Content-disposition", "inline; filename=GMFEN_${new Date().format("ddMMyyyy")}.xlsx"
                    response.outputStream << filegmfgen
                    //redirect action: "lista"
                }else{
                    logg =new Log(parametri: "non ci sono polizze generate nel mese/anno selezionato", operazione: "generazione GMFGEN", pagina: "Polizza CASH")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    flash.error="non ci sono polizze generate nel mese/anno selezionato"
                    redirect action: "lista"
                }
            }catch(e) {
                logg =new Log(parametri: "errore try catch ${e.toString()}", operazione: "generazione GMFGEN", pagina: "Polizza CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println e.toString() }
        }else response.sendError(404)


    }
    def generaFileGMFGENPAIOLD(){
        def logg
        if (request.post) {
            try {

                def mese=params.mese
                def anno=params.anno
                def resultwebS = IAssicurWebService.getGMFGENFINLEAPAI(mese, anno)
                def polizzeWS = resultwebS.polizze
                logg =new Log(parametri: "numero di polizze trovate su IAssciur nel periodo selezionato ${polizzeWS.size()} ", operazione: "generaFileGMFGEN", pagina: "POLIZZE PAI PAGAMENTO")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                /*def  fmt_IT = NumberFormat.getNumberInstance(Locale.ITALIAN)
                fmt_IT.setMaximumFractionDigits(2)*/
                if (polizzeWS.size()>0) {

                    def filegmfgen=GmfgenService.fileGMFGENPAI(polizzeWS)
                    response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
                    response.addHeader "Content-disposition", "inline; filename=GMFEN_${new Date().format("ddMMyyyy")}.xlsx"
                    response.outputStream << filegmfgen
                    //redirect action: "lista"
                }else{
                    logg =new Log(parametri: "non ci sono polizze generate nel mese/anno selezionato", operazione: "generazione GMFGEN", pagina: "POLIZZE PAI PAGAMENTO")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    flash.error="non ci sono polizze generate nel mese/anno selezionato"
                    redirect action: "listPaiPag"
                }
            }catch(e) {
                logg =new Log(parametri: "errore try catch ${e.toString()}", operazione: "generazione GMFGEN", pagina: "POLIZZE PAI PAGAMENTO")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println e.toString() }
        }else response.sendError(404)


    }
    def caricaPolizzaPAIPagamentoOLD() {
        def logg
        def listFlussiPAI=""
        def listErrori=""
        def totaleErr=0
        def totale=0
        def erroPolizza=0
        if (request.post) {
            def flussiPAI=[], errorePratica=[]
            def file =params.excelPratica
            def filename=""
            if(file.bytes) {
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                def risposta =[]
                Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$)/
                Pattern pattern = ~/(\w+)/
                String regex = "<(\\S+)[^>]+?mso-[^>]*>.*?</\\1>";
                def polizza
                try {
                    def wb = ExcelReader.readXlsx(myInputStream) {
                        sheet (0) {
                            def j=1
                            rows(from: 2) {
                                boolean sup75=false
                                def tipoClienteDef=TipoCliente.M
                                def cellnoPratica = cell("B")?.value?:""
                                def dataDecorrenza=""
                                if((cellnoPratica.toString().equals("null") || cellnoPratica.toString()=='')){
                                    logg =new Log(parametri: "il numero di polizza non e' stato inserito nella cella-- riga ${j}", operazione: "caricamento polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    errorePratica.add("il numero di polizza non e' stato inserito nella cella -- riga ${j} del file excel,")
                                }else{
                                    //cellaNoPolizza=cellaNoPolizza.toString().replaceFirst ("^0*", "")
                                    if(cellnoPratica.toString().contains(".")){
                                        def punto=cellnoPratica.toString().indexOf(".")
                                        //cellaNoPolizza=Double.valueOf(cellaNoPolizza).longValue().toString()
                                        cellnoPratica=cellnoPratica.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                    }
                                    logg =new Log(parametri: "numero di polizza inserito ${cellnoPratica}", operazione: "caricamento polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                                    polizza=PolizzaPaiPag.findByNoPolizza(cellnoPratica)
                                    if(polizza){
                                        logg = new Log(parametri: "la polizza con numero ${cellnoPratica} e' gia' stata caricata", operazione: "caricamento polizze PAI pagamento tramite xlsx", pagina: "POLIZZE FINANZIATE")
                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        errorePratica.add("la polizza con numero ${cellnoPratica}  e' gia' stata caricata,")
                                    }else{
                                        boolean denominazione=false
                                        boolean coobligato=false
                                        boolean cliente=false
                                        boolean nonTrovato=false
                                        def dealer
                                        def cognomeDef,nomeDef,provinciaDef,localitaDef,capDef,telDef,cellDef,cfDef,indirizzoDef,emailDef
                                        def celladatadecorrenza = cell("C")?.value?:""
                                        def cellaDealer =cell("D")?.value?:""
                                        def cellaBeneficiario=cell("BF")?.value?:""
                                        def cellaDenominazione= cell("AD")?.value?:""
                                        def cellaCoobligatoCognome= cell("R")?.value?:""
                                        def cellaCoobligatoNome= cell("S")?.value?:""
                                        def cellaCognome= cell("F")?.value?:""
                                        def cellaNome= cell("G")?.value?:""
                                        if((celladatadecorrenza.toString().equals("null") || celladatadecorrenza.toString()=='')) {
                                            logg = new Log(parametri: "la data di decorrenza non e' stata inserita", operazione: "caricamento polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            errorePratica.add("la data di decorrenza non e' stata inserita,")
                                            dataDecorrenza=null
                                        }else{
                                            if(celladatadecorrenza instanceof Date){
                                                dataDecorrenza=celladatadecorrenza
                                            }else {dataDecorrenza=Date.parse("dd/MM/yyyy",celladatadecorrenza)}

                                        }
                                        if(cellaDealer.toString().equals("null") || cellaDealer.toString().equals("")){
                                            cellaDealer=""
                                        }else{
                                            cellaDealer=cellaDealer.toString().replaceAll ("'", "")
                                            cellaDealer=cellaDealer.toString().replaceFirst ("^0*", "")
                                            if(cellaDealer.toString().contains(".")){
                                                cellaDealer=Double.valueOf(cellaDealer).longValue().toString()
                                                //cellaDealer=cellaDealer.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                            }
                                            dealer=Dealer.findByCodice(cellaDealer.toString())
                                        }
                                        if(cellaBeneficiario.toString()!=''){
                                            cellaBeneficiario=cellaBeneficiario.toUpperCase().trim()
                                            //println "benficiario ${cellaBeneficiario}"
                                            logg =new Log(parametri: "beneficiario-->${cellaBeneficiario}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            if(cellaDenominazione) {
                                                cellaDenominazione=cellaDenominazione.toUpperCase().trim()
                                                //println "cellaDenominazione ${cellaDenominazione}"
                                                if (cellaDenominazione.contains(cellaBeneficiario)) {
                                                    logg =new Log(parametri: "denominazione-->${cellaDenominazione}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    denominazione = true

                                                } else {
                                                    denominazione = false
                                                }
                                            }
                                            if(cellaCoobligatoNome){
                                                cellaCoobligatoNome=cellaCoobligatoNome.toUpperCase().trim()
                                                cellaCoobligatoCognome=cellaCoobligatoCognome.toUpperCase().trim()
                                                //def nomeCooC= cellaCoobligatoCognome+" "+cellaCoobligatoNome
                                                def nomeCooC= cellaCoobligatoNome+" "+cellaCoobligatoCognome
                                                //println "nomeCooC ${nomeCooC}"
                                                if(nomeCooC.trim().contains(cellaBeneficiario)){
                                                    logg =new Log(parametri: "nome Coobligato-->${nomeCooC}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    coobligato=true
                                                }else{
                                                    coobligato = false
                                                }
                                            }
                                            if(cellaNome){
                                                cellaNome=cellaNome.toUpperCase().trim()
                                                cellaCognome=cellaCognome.toUpperCase().trim()
                                                //def nomeCom=cellaCognome+" "+cellaNome
                                                def nomeCom=cellaNome+" "+cellaCognome
                                                //println "nomeCom ${nomeCom}"
                                                if(nomeCom.trim().contains(cellaBeneficiario)){
                                                    logg =new Log(parametri: "nome Cliente-->${nomeCom}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    cliente=true
                                                }else{
                                                    cliente = false
                                                }
                                            }
                                            if(coobligato){
                                                logg =new Log(parametri: "prendo i dati del Coobligato-->${cellaCoobligatoNome}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                def cellaCoobligatoPROV=cell("Y")?.value?:""
                                                if(cellaCoobligatoPROV.toString().contains("\'")) cellaCoobligatoPROV = cellaCoobligatoPROV.replaceAll("\'","")
                                                def cellaCoobligatoLOC=cell("W")?.value?:""
                                                if(cellaCoobligatoLOC.toString().contains("\'")) cellaCoobligatoLOC = cellaCoobligatoLOC.replaceAll("\'","")
                                                def cellaCoobligatoCAP=cell("Z")?.value?:""
                                                if(cellaCoobligatoCAP.toString().contains("\'")) cellaCoobligatoCAP = cellaCoobligatoCAP.replaceAll("\'","")
                                                def cellaCoobligatoTEL=cell("U")?.value?:""
                                                if(cellaCoobligatoTEL.toString().contains("\'")) cellaCoobligatoTEL = cellaCoobligatoTEL.replaceAll("\'","")
                                                def cellaCoobligatoCEL=cell("V")?.value?:""
                                                if(cellaCoobligatoCEL.toString().contains("\'")) cellaCoobligatoCEL = cellaCoobligatoCEL.replaceAll("\'","")
                                                def cellaCoobligatoCF=cell("T")?.value?:""
                                                if(cellaCoobligatoCF.toString().contains("\'")) cellaCoobligatoCF = cellaCoobligatoCF.replaceAll("\'","")
                                                def cellaCoobligatoIND=cell("X")?.value?:""
                                                if(cellaCoobligatoIND.toString().contains("\'")) cellaCoobligatoIND = cellaCoobligatoIND.replaceAll("\'","")
                                                def cellaCoobligatoemail=cell("AC")?.value?:""
                                                if(cellaCoobligatoIND.toString().contains("\'")) cellaCoobligatoIND = cellaCoobligatoIND.replaceAll("\'","")
                                                cognomeDef=cellaCoobligatoCognome.toString().trim().toUpperCase()
                                                nomeDef=cellaCoobligatoNome.toString().trim().toUpperCase()
                                                provinciaDef=cellaCoobligatoPROV.toString().trim().toUpperCase()
                                                localitaDef=cellaCoobligatoLOC.toString().trim().toUpperCase()
                                                capDef=cellaCoobligatoCAP.toString().trim().toUpperCase()
                                                telDef=cellaCoobligatoTEL.toString().trim().toUpperCase()
                                                cellDef=cellaCoobligatoCEL.toString().trim().toUpperCase()
                                                if(cellaCoobligatoCF !='null'){
                                                    cfDef=cellaCoobligatoCF.toString().trim().toUpperCase()
                                                    if(cfDef){
                                                        def annoCF=cfDef.substring(6,8)
                                                        if(Integer.parseInt(annoCF) && Integer.parseInt(annoCF)<=40){
                                                            sup75=true
                                                        }
                                                        def sessC=cfDef.substring(9,11)
                                                        if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                                            tipoClienteDef=TipoCliente.F
                                                        }
                                                    }
                                                    /*if(!cfDef.toString().matches("^[a-z|A-Z]{6}[0-9]{2}[A-Z|a-z][0-9]{2}[A-Z|a-z][0-9]{3}[A-Z|a-z]\$") && !cfDef.matches("^0+\\d*(?:\\.\\d+)?\$")){
                                                        cfDef=Double.valueOf(cfDef).longValue().toString()
                                                    }
                                                    if(!cfDef.matches("[0-9]{11}")){
                                                    cfDef=cfDef.padLeft(11,"0")
                                                    }
                                                    */
                                                }
                                                if(cellDef.toString().contains(".")){
                                                    def punto=cellDef.toString().indexOf(".")
                                                    cellDef=cellDef.toString().replaceAll("[^0-9]", "")
                                                }
                                                if(telDef.toString().contains(".")){
                                                    def punto=telDef.toString().indexOf(".")
                                                    telDef=telDef.toString().replaceAll("[^0-9]", "")
                                                }
                                                indirizzoDef=cellaCoobligatoIND.toString().trim().toUpperCase()

                                                if(cellaCoobligatoemail.toString().trim().size()>4){
                                                    emailDef=cellaCoobligatoemail.toString().trim()
                                                }

                                            }else if(denominazione){
                                                logg =new Log(parametri: "prendo i dati della denominazione -->${cellaDenominazione}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                                                def cellaProvinciaBusiness =cell("AI")?.value?:""
                                                if(cellaProvinciaBusiness.toString().contains("\'")) cellaProvinciaBusiness = cellaProvinciaBusiness.replaceAll("\'","")
                                                def cellaLocalitaBusiness =cell("W")?.value?:""
                                                if(cellaLocalitaBusiness.toString().contains("\'")) cellaLocalitaBusiness = cellaLocalitaBusiness.replaceAll("\'","")
                                                def cellaCapBusiness =cell("AJ")?.value?:""
                                                if(cellaCapBusiness.toString().contains("\'")) cellaCapBusiness = cellaCapBusiness.replaceAll("\'","")
                                                def cellaTelefonoBusiness =cell("AF")?.value?:""
                                                if(cellaTelefonoBusiness.toString().contains("\'")) cellaTelefonoBusiness = cellaTelefonoBusiness.replaceAll("\'","")
                                                def cellaCellulareBusiness =cell("AH")?.value?:""
                                                if(cellaCellulareBusiness.toString().contains("\'")) cellaCellulareBusiness = cellaCellulareBusiness.replaceAll("\'","")
                                                def cellaPIvaBusiness =cell("AK")?.value?:""
                                                if(cellaPIvaBusiness.toString().contains("\'")) cellaPIvaBusiness = cellaPIvaBusiness.replaceAll("\'","")
                                                def cellaIndirizzoBusiness =cell("AG")?.value?:""
                                                if(cellaIndirizzoBusiness.toString().contains("\'")) cellaIndirizzoBusiness = cellaIndirizzoBusiness.replaceAll("\'","")
                                                def cellaEmailBusiness =cell("AM")?.value?:""
                                                if(cellaEmailBusiness.toString().contains("\'")) cellaEmailBusiness = cellaEmailBusiness.replaceAll("\'","")
                                                cognomeDef=cellaDenominazione.toString().trim().toUpperCase()
                                                nomeDef=""

                                                provinciaDef=cellaProvinciaBusiness.toString().trim().toUpperCase()
                                                localitaDef=cellaLocalitaBusiness.toString().trim().toUpperCase()
                                                capDef=cellaCapBusiness.toString().trim()
                                                telDef=cellaTelefonoBusiness.toString().trim()
                                                cellDef=cellaCellulareBusiness.toString().trim()
                                                if(cellDef.toString().contains(".")){
                                                    def punto=cellDef.toString().indexOf(".")
                                                    //cellaNoPolizza=Double.valueOf(cellaNoPolizza).longValue().toString()
                                                    //cellDef=cellDef.toString().replaceAll("[^0-9]", "")
                                                }
                                                if(telDef.toString().contains(".")){
                                                    def punto=telDef.toString().indexOf(".")
                                                    //cellaNoPolizza=Double.valueOf(cellaNoPolizza).longValue().toString()
                                                    // telDef=telDef.toString().replaceAll("[^0-9]", "")
                                                }
                                                if(cellaPIvaBusiness !='null'){
                                                    cfDef=cellaPIvaBusiness.toString().trim().toUpperCase()
                                                    /*if(!cfDef.toString().matches("^[a-z|A-Z]{6}[0-9]{2}[A-Z|a-z][0-9]{2}[A-Z|a-z][0-9]{3}[A-Z|a-z]\$") && !cfDef.matches("^0+\\d*(?:\\.\\d+)?\$")){
                                                        cfDef=Double.valueOf(cfDef).longValue().toString()
                                                    }
                                                    if(!cfDef.matches("[0-9]{11}")){
                                                    cfDef=cfDef.padLeft(11,"0")
                                                    }
                                                    */
                                                }

                                                indirizzoDef=cellaIndirizzoBusiness.toString().trim().toUpperCase()
                                                tipoClienteDef=TipoCliente.DITTA_INDIVIDUALE
                                                if(cellaEmailBusiness.toString().trim().size()>4){
                                                    emailDef=cellaEmailBusiness.toString().trim()
                                                }
                                            }else if(cliente){
                                                logg =new Log(parametri: "prendo i dati del cliente -->${cellaNome}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                def cellaProvincia=cell("M")?.value?:""
                                                if(cellaProvincia.toString().contains("\'")) cellaProvincia = cellaProvincia.replaceAll("\'","")
                                                def cellaLocalita=cell("K")?.value?:""
                                                if(cellaLocalita.toString().contains("\'")) cellaLocalita = cellaLocalita.replaceAll("\'","")
                                                def cellaCap=cell("N")?.value?:""
                                                if(cellaCap.toString().contains("\'")) cellaCap = cellaCap.replaceAll("\'","")
                                                def cellaTel=cell("I")?.value?:""
                                                if(cellaTel.toString().contains("\'")) cellaTel = cellaTel.replaceAll("\'","")
                                                def cellaCel=cell("J")?.value?:""
                                                if(cellaCel.toString().contains("\'")) cellaCel = cellaCel.replaceAll("\'","")
                                                def cellaCF=cell("H")?.value?:""
                                                def cellaIndirizzo=cell("L")?.value?:""
                                                if(cellaIndirizzo.toString().contains("\'")) cellaIndirizzo = cellaIndirizzo.replaceAll("\'","")
                                                def cellaEmail=cell("Q")?.value?:""
                                                if(cellaEmail.toString().contains("\'")) cellaEmail = cellaEmail.replaceAll("\'","")
                                                cognomeDef=cellaCognome.toString().trim().toUpperCase()
                                                nomeDef=cellaNome.toString().trim().toUpperCase()
                                                provinciaDef=cellaProvincia.toString().trim().toUpperCase()
                                                localitaDef=cellaLocalita.toString().trim().toUpperCase()
                                                capDef=cellaCap.toString().trim()
                                                telDef=cellaTel.toString().trim()
                                                cellDef=cellaCel.toString().trim()
                                                if(cellDef.toString().contains(".")){
                                                    //def punto=cellDef.toString().indexOf(".")
                                                    cellDef=cellDef.toString().replaceAll("[^0-9]", "")
                                                }
                                                if(telDef.toString().contains(".")){
                                                    def punto=telDef.toString().indexOf(".")
                                                    telDef=telDef.toString().replaceAll("[^0-9]", "")
                                                }
                                                if(cellaCF !='null'){
                                                    cfDef=cellaCF.toString().trim().toUpperCase()
                                                    if(cfDef){
                                                        def annoCF=cfDef.substring(6,8)
                                                        if(Integer.parseInt(annoCF) && Integer.parseInt(annoCF)<=40){
                                                            sup75=true
                                                        }
                                                        def sessC=cfDef.substring(9,11)
                                                        if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                                            tipoClienteDef=TipoCliente.F
                                                        }
                                                    }
                                                    /*if(!cfDef.toString().matches("^[a-z|A-Z]{6}[0-9]{2}[A-Z|a-z][0-9]{2}[A-Z|a-z][0-9]{3}[A-Z|a-z]\$") && !cfDef.matches("^0+\\d*(?:\\.\\d+)?\$")){
                                                        cfDef=Double.valueOf(cfDef).longValue().toString()
                                                    }

                                                    if(!cellaCF.matches("[0-9]{11}")){
                                                    cfDef=cfDef.padLeft(11,"0")
                                                    }
                                                    */
                                                }

                                                indirizzoDef=cellaIndirizzo.toString().trim().toUpperCase()
                                                if(cellaEmail.toString().trim().size()>4){
                                                    emailDef=cellaEmail.toString().trim()
                                                }

                                            }else{
                                                nonTrovato=true
                                            }

                                            if(nonTrovato){
                                                logg =new Log(parametri: "il beneficiario ${cellaBeneficiario} non appare nelle colonne dei dati coobligato/cliente/business, controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                errorePratica.add("per la pratica-->${cellnoPratica} il beneficiario non appare nelle colonne dei dati coobligato/cliente/business controllare file,")
                                                erroPolizza++
                                            }else{
                                                def cellaNomeDealer=cell("E")?.value?:null
                                                if(cellaNomeDealer.toString().contains("\'")) cellaNomeDealer = cellaNomeDealer.replaceAll("\'","")
                                                def cellaProdotto=cell("AY")?.value?:null
                                                if(cellaProdotto.toString().contains("\'")) cellaProdotto = cellaProdotto.replaceAll("\'","")
                                                def cellaDataImmatr=cell("AO")?.value?:null
                                                if(cellaDataImmatr.toString().contains("\'")) cellaDataImmatr = cellaDataImmatr.replaceAll("\'","")
                                                def cellaDurata=cell("AZ")?.value?:null
                                                //println "cell(\"AZ\") ${cellaDurata}"
                                                if(cellaDurata.toString().contains("\'")) cellaDurata = cellaDurata.replaceAll("\'","")
                                                def cellaMarca=cell("AP")?.value?:null
                                                if(cellaMarca.toString().contains("\'")) cellaMarca = cellaMarca.replaceAll("\'","")
                                                def cellaModello=cell("AQ")?.value?:null
                                                if(cellaModello.toString().contains("\'")) cellaModello = cellaModello.replaceAll("\'","")
                                                def cellaBeneFinanziato=cell("AN")?.value?:null
                                                if(cellaBeneFinanziato.toString().contains("\'")) cellaBeneFinanziato = cellaBeneFinanziato.replaceAll("\'","")
                                                def cellapremioImponibile=cell("BA")?.value?:null
                                                if(cellapremioImponibile.toString().contains("\'")) cellapremioImponibile = cellapremioImponibile.replaceAll("\'","")
                                                /*def cellapremioLordo=cell("BB")?.value?:null*/
                                                def cellaprovvVendi=cell("BD")?.value?:null
                                                if(cellaprovvVendi.toString().contains("\'")) cellaprovvVendi = cellaprovvVendi.replaceAll("\'","")
                                                def cellaprovvDealer=cell("BC")?.value?:null
                                                if(cellaprovvDealer.toString().contains("\'")) cellaprovvDealer = cellaprovvDealer.replaceAll("\'","")
                                                def cellaprovvGmfi=cell("BE")?.value?:null
                                                if(cellaprovvGmfi.toString().contains("\'")) cellaprovvGmfi = cellaprovvGmfi.replaceAll("\'","")
                                                def cellaTelaio
                                                if(cell("AT")?.value !=null){
                                                    cellaTelaio=cell("AT")?.value
                                                    if(cellaTelaio.toString().contains("\'")) cellaTelaio = cellaTelaio.replaceAll("\'","")
                                                }
                                                def cellaTarga
                                                if(cell("AU")?.value !=null){
                                                    cellaTarga=cell("AU")?.value
                                                    if( cellaTarga.toString().contains("\'")) cellaTarga = cellaTarga.replaceAll("\'","")
                                                }
                                                def cellaValoreAssi=cell("AV")?.value?:null
                                                if(cellaValoreAssi.toString().contains("\'")) cellaValoreAssi = cellaValoreAssi.replaceAll("\'","")
                                                def cellaPrezzo=cell("AV")?.value?:null
                                                if(cellaPrezzo.toString().contains("\'")) cellaPrezzo = cellaPrezzo.replaceAll("\'","")
                                                def cellaVenditore=cell("BG")?.value?:null
                                                if(cellaVenditore.toString().contains("\'")) cellaVenditore = cellaVenditore.replaceAll("\'","")
                                                def cellaTipoPol=cell("BJ")?.value?:null
                                                if(cellaTipoPol.toString().contains("\'")) cellaTipoPol = cellaVenditore.replaceAll("\'","")
                                                def leasing=false
                                                def onStar=false
                                                def dataInserimento=new Date()
                                                dataInserimento=dataInserimento.clearTime()
                                                def dataScadenza
                                                def durata=null
                                                def premioImponibile=0.0
                                                def premioImponibileFoglio=0.0
                                                def provvDealerFoglio=0.0
                                                def provvVendFoglio=0.0
                                                def provvGMFFoglio=0.0
                                                def premioLordo=0.0
                                                def provvVendi=0.0
                                                def provvDealer=0.0
                                                def provvGmfi=0.0
                                                def premioFisso=126.83
                                                def imposte=0.0
                                                if(cellaTipoPol.toString().trim().toLowerCase().contains("leasing")){leasing=true}
                                                if((cellaDurata.toString().trim()!=null && cellaDurata.toString().trim()!='') && cellaDurata.toString().trim().length()>0){
                                                    if(cellaDurata.toString().contains(".")){
                                                        def punto=cellaDurata.toString().indexOf(".")
                                                        cellaDurata=cellaDurata.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                                    }
                                                    durata=Integer.parseInt(cellaDurata.toString().trim())
                                                    premioImponibile=premioFisso*durata
                                                    premioImponibile=Math.round(premioImponibile * 100) / 100
                                                    premioLordo = premioImponibile * (1.025)
                                                    premioLordo=Math.round(premioLordo * 100) / 100
                                                    provvGmfi=premioImponibile*0.38
                                                    provvGmfi=Math.round(provvGmfi * 100) / 100
                                                    if(dealer.percVenditore>0){
                                                        provvDealer= (premioImponibile*0.1867)
                                                        provvDealer=Math.round(provvDealer * 100) / 100
                                                        provvVendi= (premioImponibile*0.0933)
                                                        provvVendi=Math.round(provvVendi * 100) / 100
                                                    }else{
                                                        provvDealer= (premioImponibile*0.28)
                                                        provvDealer=Math.round(provvDealer * 100) / 100
                                                        provvVendi= (premioImponibile*0.0)
                                                        provvVendi=Math.round(provvVendi * 100) / 100
                                                    }
                                                    if(cellaprovvDealer !=null && cellaprovvDealer !=''){
                                                        if(cellaprovvDealer instanceof BigDecimal){
                                                            cellaprovvDealer=cellaprovvDealer.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                                            provvDealerFoglio = new BigDecimal(cellaprovvDealer)
                                                        }else if(cellaprovvDealer instanceof Double){
                                                            provvDealerFoglio = BigDecimal.valueOf(cellaprovvDealer)
                                                            provvDealerFoglio=Math.round(provvDealerFoglio * 100) / 100
                                                        }
                                                        else {provvDealerFoglio=0.0}
                                                        def diffProvvD=Math.abs(Math.round((provvDealer-provvDealerFoglio)*100)/100)
                                                        if(diffProvvD>=0.05 && diffProvvD!=""){
                                                            logg =new Log(parametri: "per la polizza ${cellnoPratica}  la provv. Dealer inserita nel foglio ${provvDealerFoglio} non corrisponde con la provv. Dealer calcolata : ${provvDealer} ", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            errorePratica.add("per la polizza ${cellnoPratica}  la provv. Dealer inserita nel foglio ${provvDealerFoglio} non corrisponde con la provv. Dealer calcolata : ${provvDealer},")
                                                            erroPolizza++
                                                        }
                                                    }else{
                                                        logg =new Log(parametri: "per la polizza ${cellnoPratica} il valore della provv. Dealer non e' stata inserita controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        errorePratica.add("per la polizza ${cellnoPratica} il valore della provv. Dealer non e' stata inserita controllare file,")
                                                        erroPolizza++
                                                        provvDealerFoglio=0.0
                                                    }
                                                    if(cellaprovvGmfi !=null && cellaprovvGmfi !=''){
                                                        if(cellaprovvGmfi instanceof BigDecimal){
                                                            cellaprovvGmfi=cellaprovvGmfi.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                                            provvGMFFoglio = new BigDecimal(cellaprovvGmfi)
                                                        }else if(cellaprovvGmfi instanceof Double){
                                                            provvGMFFoglio = BigDecimal.valueOf(cellaprovvGmfi)
                                                            provvGMFFoglio=Math.round(provvGMFFoglio * 100) / 100
                                                        }else {provvGMFFoglio=0.0}
                                                        def diffProvvGMF=Math.abs(Math.round((provvGmfi-provvGMFFoglio)*100)/100)
                                                        if(diffProvvGMF>=0.05 && diffProvvGMF!=""){
                                                            logg =new Log(parametri: "per la polizza ${cellnoPratica}  la provv. GMF inserita nel foglio ${provvGMFFoglio} non corrisponde con la provv. GMF calcolata : ${provvGmfi} ", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            errorePratica.add("per la polizza ${cellnoPratica}  la provv. GMF inserita nel foglio ${provvGMFFoglio} non corrisponde con la provv. GMF calcolata : ${provvGmfi},")
                                                            erroPolizza++
                                                        }

                                                    }else{
                                                        logg =new Log(parametri: "per la polizza ${cellnoPratica} il valore della provv. GMF non e' stata inserita controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        errorePratica.add("per la polizza ${cellnoPratica} il valore della provv. GMF non e' stata inserita controllare file,")
                                                        erroPolizza++
                                                        provvGMFFoglio=0.0
                                                    }
                                                    if(cellaprovvVendi !=null && cellaprovvVendi !=''){
                                                        if(cellaprovvVendi instanceof BigDecimal){
                                                            cellaprovvVendi=cellaprovvVendi.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                                            provvVendFoglio = new BigDecimal(cellaprovvVendi)
                                                        }else if(cellaprovvVendi instanceof Double){
                                                            provvVendFoglio = BigDecimal.valueOf(cellaprovvVendi)
                                                            provvVendFoglio=Math.round(provvVendFoglio * 100) / 100
                                                        }else {provvVendFoglio=0.0}
                                                        def diffProvvVend=Math.abs(Math.round((provvVendi-provvVendFoglio)*100)/100)
                                                        if(diffProvvVend>=0.05 && diffProvvVend!=""){
                                                            logg =new Log(parametri: "per la polizza ${cellnoPratica}  la provv. venditore inserita nel foglio ${provvVendFoglio} non corrisponde con la provv. venditore calcolata : ${provvVendi} ", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            errorePratica.add("per la polizza ${cellnoPratica}  la provv. venditore inserita nel foglio ${provvVendFoglio} non corrisponde con la provv. venditore calcolata : ${provvVendi},")
                                                            erroPolizza++
                                                        }

                                                    }else{
                                                        logg =new Log(parametri: "per la polizza ${cellnoPratica} il valore della provv. venditore non e' stata inserita controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        errorePratica.add("per la polizza ${cellnoPratica} il valore della provv. venditore non e' stata inserita controllare file,")
                                                        erroPolizza++
                                                        provvVendFoglio=0.0
                                                    }
                                                    imposte=premioImponibile *0.025
                                                    if( dataDecorrenza!=null){
                                                        if(durata==1){
                                                            dataScadenza= use(TimeCategory) {dataDecorrenza +12.months}
                                                        }else if( durata==2){
                                                            dataScadenza= use(TimeCategory) {dataDecorrenza +24.months}
                                                        }else if(durata==3){
                                                            dataScadenza=use(TimeCategory) {dataDecorrenza +36.months}
                                                        }else if(durata==4){
                                                            dataScadenza= use(TimeCategory) {dataDecorrenza +48.months}
                                                        }else if(durata==5){
                                                            dataScadenza=use(TimeCategory) {dataDecorrenza +60.months}
                                                        }else{
                                                            dataScadenza=null
                                                        }
                                                    }else{
                                                        logg =new Log(parametri: "la data decorrenza non e' stata inserita pertanto non e' possibile mettere una data scadenza, controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        errorePratica.add("la data decorrenza non e' stata inserita pertanto non e' possibile mettere una data scadenza controllare file,")
                                                        erroPolizza++
                                                        dataScadenza=null
                                                    }
                                                }else{
                                                    logg =new Log(parametri: "la durata non e' stata inserita, controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    errorePratica.add("la durata non e' stata inserita controllare file,")
                                                    erroPolizza++

                                                }
                                                def coperturaR
                                                if(cellaProdotto.toString().trim()!=null && cellaProdotto.toString().trim()!=''){
                                                    coperturaR=cellaProdotto

                                                }
                                                def dataImmatricolazione=null
                                                if(!(cellaDataImmatr.toString().trim().equals("null") || cellaDataImmatr.toString().trim()!='')){
                                                    def newDate = Date.parse( 'MM/yyyy', cellaDataImmatr )
                                                    dataImmatricolazione = newDate
                                                }else{
                                                    dataImmatricolazione = new Date()
                                                }
                                                if(cellaMarca.toString().equals("null") && cellaMarca.toString().trim()!=''){
                                                    cellaMarca=null;
                                                }else{
                                                    cellaMarca=cellaMarca.toString().trim().toUpperCase()
                                                }
                                                if(cellaModello.toString().equals("null")&& cellaModello.toString().equals("")){
                                                    cellaModello=null;
                                                }else{
                                                    cellaModello=cellaModello.toString().trim().toUpperCase()
                                                }
                                                def nuovo=false
                                                if(cellaBeneFinanziato.toString().trim()!=null && cellaBeneFinanziato.toString().trim()!=''){
                                                    if(cellaBeneFinanziato.toString().trim().toLowerCase().contains("seminuove")){
                                                        nuovo=false
                                                    }else{
                                                        nuovo=true
                                                    }
                                                }
                                                if(cellapremioImponibile !=null && cellapremioImponibile !=''){
                                                    if(cellapremioImponibile instanceof BigDecimal){
                                                        cellapremioImponibile=cellapremioImponibile.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                                        premioImponibileFoglio = new BigDecimal(cellapremioImponibile)
                                                    }else if(cellapremioImponibile instanceof Double){
                                                        premioImponibileFoglio = BigDecimal.valueOf(cellapremioImponibile)
                                                        premioImponibileFoglio=Math.round(premioImponibileFoglio * 100) / 100
                                                    }else {premioImponibileFoglio=0.0}
                                                    def diffPremioImpo=Math.abs(Math.round((premioImponibile-premioImponibileFoglio)*100)/100)
                                                    if(diffPremioImpo>=0.05 && diffPremioImpo!=""){
                                                        logg =new Log(parametri: "per la polizza ${cellnoPratica}  il premio imponibile inserito nel foglio ${premioImponibileFoglio} non corrisponde con il premio (imponibile: ${premioFisso} * la durata: ${durata}) -> ${premioImponibile}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        errorePratica.add("per la polizza ${cellnoPratica} il premio imponibile inserito nel foglio ${premioImponibileFoglio} non corrisponde con il premio (imponibile: ${premioFisso} * la durata: ${durata})--> ${premioImponibile},")
                                                        erroPolizza++
                                                    }

                                                }else{
                                                    logg =new Log(parametri: "per la polizza ${cellnoPratica} il valore del premio imponibile non e' stato inserito controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    errorePratica.add("per la polizza ${cellnoPratica} il valore del premio imponibile non e' stato inserito controllare file,")
                                                    erroPolizza++
                                                    premioImponibileFoglio=0.0
                                                }

                                                def valoreAssicurato
                                                if((cellaValoreAssi.toString().trim()!=null && cellaValoreAssi.toString().trim()!='' )&& cellaValoreAssi.toString().length()>0){
                                                    cellaValoreAssi=cellaValoreAssi.toString().trim()
                                                    def counter = 0
                                                    for( int i=0; i<cellaValoreAssi.length(); i++ ) {
                                                        if( cellaValoreAssi.charAt(i) == '.' ) {
                                                            counter++
                                                        }
                                                    }
                                                    if(counter==1){
                                                        def punto=cellaValoreAssi.indexOf(".")
                                                        valoreAssicurato=cellaValoreAssi.substring(0,punto).replaceAll("[^0-9]", "")
                                                    }else if(counter>1){
                                                        def sdopto=cellaValoreAssi.indexOf(".", cellaValoreAssi.indexOf(".") + 1)
                                                        valoreAssicurato=cellaValoreAssi.substring(0,sdopto).replaceAll("[^0-9]", "")
                                                    }
                                                    valoreAssicurato = new BigDecimal(valoreAssicurato)
                                                }else{
                                                    valoreAssicurato=0.0
                                                }
                                                def prezzoVendita
                                                if((cellaPrezzo.toString().trim()!=null || cellaPrezzo.toString().trim()!='' ) && cellaPrezzo.toString().length()>0){
                                                    prezzoVendita=cellaPrezzo.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                                    //prezzoVendita=prezzoVendita.toString().replaceAll(",","\\.")
                                                    prezzoVendita = new BigDecimal(prezzoVendita)
                                                }else{
                                                    prezzoVendita=0.0
                                                }
                                                if(cellaVenditore.toString()!=null || cellaVenditore.toString()!=''){
                                                    cellaVenditore=cellaVenditore.toString().trim()
                                                }else{
                                                    cellaVenditore=""
                                                }
                                                if(dealer && cellnoPratica && cellaTelaio && cellaBeneficiario.toString()!='' ){
                                                    if (!sup75){
                                                        def polizzaPAI = PolizzaPaiPag.findOrCreateWhere(
                                                                annullata:false,
                                                                cap: capDef.toString().trim().padLeft(5,"0"),
                                                                cellulare:  cellDef.toString().trim(),
                                                                cognome:  cognomeDef.toString().trim(),
                                                                coperturaRichiesta: coperturaR,
                                                                dataDecorrenza:dataDecorrenza,
                                                                dataImmatricolazione: dataImmatricolazione,
                                                                dataInserimento: dataInserimento,
                                                                dataScadenza: dataScadenza,
                                                                dealer: dealer,
                                                                durata:durata,
                                                                email: emailDef.toString().trim(),
                                                                indirizzo: indirizzoDef.toString().trim().toUpperCase(),
                                                                localita: localitaDef.toString().trim().toUpperCase(),
                                                                marca: cellaMarca.toString().trim(),
                                                                modello: cellaModello.toString().trim(),
                                                                noPolizza: cellnoPratica.toString().trim(),
                                                                nome: nomeDef.toString().trim().toUpperCase(),
                                                                nuovo:nuovo,
                                                                onStar: onStar,
                                                                partitaIva: cfDef.toString().trim().toUpperCase(),
                                                                premioImponibile:premioImponibile,
                                                                premioLordo:premioLordo,
                                                                provincia: provinciaDef.toString().trim().toUpperCase(),
                                                                provvDealer:provvDealer,
                                                                provvGmfi:provvGmfi,
                                                                provvVenditore:provvVendi,
                                                                stato:StatoPolizza.POLIZZA,
                                                                tipoCliente:tipoClienteDef,
                                                                tipoPolizza:TipoPolizza.PAIPAGAMENTO,
                                                                step:"0",
                                                                telaio:cellaTelaio.toString().trim(),
                                                                //targa:cellaTarga?:cellaTarga.toString().trim(),
                                                                telefono:telDef.toString().trim(),
                                                                valoreAssicurato:valoreAssicurato,
                                                                valoreAssicuratoconiva:prezzoVendita,
                                                                certificatoMail:false,
                                                                codiceZonaTerritoriale: "1",
                                                                rappel:0.0,
                                                                codOperazione:"0",
                                                                dSlip:"S",
                                                                venditore:cellaVenditore?cellaVenditore.toString().trim().toUpperCase():"",
                                                                imposte:imposte
                                                        )
                                                        if(cellaTarga){
                                                            polizzaPAI.targa=cellaTarga?:cellaTarga.toString().trim()
                                                        }
                                                        if(polizzaPAI.save(flush:true)) {
                                                            logg =new Log(parametri: "la polizza ${cellnoPratica} viene salvata nel DB", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            flussiPAI.add("pratica-->${cellnoPratica} salvata nel DB,")
                                                            def tracciatoPolizza= tracciatiService.generaTracciatoPAIPAgamento(polizzaPAI,leasing)
                                                            if(!tracciatoPolizza.save(flush: true)){
                                                                logg =new Log(parametri: "errore creazione tracciato PAI", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                risposta.add(noPratica:cellnoPratica, errore:"Errore creazione tracciato PAI: ${tracciatoPolizza.errors}")
                                                                errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPolizza.errors},")
                                                                erroPolizza++
                                                            }else{
                                                                flussiPAI.add("per la pratica-->${cellnoPratica} tracciato PAI PAGAMENTO generato,")
                                                                totale++
                                                                logg =new Log(parametri: "per la pratica-->${cellnoPratica} tracciato PAI PAGAMENTO generato", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }
                                                        }
                                                        else{
                                                            logg =new Log(parametri: "Errore creazione polizza:${cellnoPratica}--> ${polizzaPAI.errors}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            risposta.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: ${polizzaPAI.errors}")
                                                            errorePratica.add(" pratica-->${cellnoPratica} Errore creazione polizza: ${polizzaPAI.errors},")
                                                            erroPolizza++
                                                        }
                                                    }else{
                                                        errorePratica.add("il beneficiario della pratica-->${cellnoPratica}  supera la eta' di 75 anni,")
                                                        erroPolizza++
                                                    }

                                                }else if(!dealer){
                                                    logg =new Log(parametri: "Errore creazione polizza:${cellnoPratica} il dealer non e' presente", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    risposta.add("Errore creazione polizza:${cellnoPratica} il dealer non e' presente")
                                                    errorePratica.add("Errore creazione polizza:${cellnoPratica} il dealer ${cellaDealer}- ${cellaNomeDealer} non e' presente,")
                                                    erroPolizza++
                                                }else if(!cellnoPratica){
                                                    logg =new Log(parametri: "Errore creazione polizza: non e' presente un numero di pratica riga ${j}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    risposta.add("Errore creazione polizza: non e' presente un numero di pratica riga ${j},")
                                                    errorePratica.add("Errore creazione polizza: non e' presente un numero di polizza riga ${j},")
                                                    erroPolizza++
                                                }else if(!cellaTelaio){
                                                    logg =new Log(parametri: "Errore creazione polizza:${cellnoPratica} non e' presente un numero di telaio", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    risposta.add("Errore creazione polizza:${cellnoPratica} non e' presente un numero di telaio,")
                                                    errorePratica.add("Errore creazione polizza:${cellnoPratica} non e' presente un numero di telaio,")
                                                    erroPolizza++
                                                }
                                            }
                                        }else{
                                            logg =new Log(parametri: "per la polizza ${cellnoPratica}-->i dati del beneficiaro non sono stati inseriti", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            risposta.add("per la polizza ${cellnoPratica}-->i dati del beneficiaro non sono stati inseriti,")
                                            errorePratica.add("per la polizza ${cellnoPratica}-->i dati del beneficiaro non sono stati inseriti,")
                                            erroPolizza++
                                        }
                                    }
                                }
                                j++
                                if(erroPolizza>0){ totaleErr++}
                            }
                        }
                    }
                }catch (e){
                    logg =new Log(parametri: "c'e' un problema con il file, ${e.toString()}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println "c'e' un problema con il file, ${e.toString()}"
                    errorePratica.add("c'e' un problema con il file, ${e.toString()}")
                }
                if(errorePratica.size()>0){
                    for (String s : errorePratica)
                    {
                        listErrori += s + "\r\n\r\n"
                    }
                }
                if(flussiPAI.size()>0){
                    for (String s : flussiPAI)
                    {
                        listFlussiPAI += s + "\r\n\r\n"
                    }
                }
                if(flussiPAI.size()>0){
                    flash.flussiPAI=listFlussiPAI+="totale flussi generati ${totale}"
                }
                if(errorePratica.size()>0){
                    flash.errorePratica=listErrori+="totale flussi non generati ${totaleErr}"
                }
            } else flash.error = "Specificare l'elenco xlsx"
            redirect controller: "polizze", action: "listPaiPag"
        }else response.sendError(404)
    }
}
