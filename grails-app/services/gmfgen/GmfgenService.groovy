package gmfgen

import cashopel.polizze.Polizza
import cashopel.polizze.PolizzaPaiPag
import cashopel.polizze.PolizzaRCA
import cashopel.polizze.TipoPolizza
import cashopel.polizze.ZoneTarif
import cashopel.utenti.Log
import com.presstoday.groovy.Stopwatch
import excel.builder.ExcelBuilder
import groovy.sql.Sql

import java.text.NumberFormat
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream


class GmfgenService {
    static transactional = false
    def IAssicurWebService
    def dataSource_rinnovi
    def mailService
    def ftpService
    private def getRinnDb() {
        //println dataSource_rinnovi
        return new Sql(dataSource_rinnovi)
    }
    def fileGMFGENold(def polizzeWS) {
        def logg
        def db = rinnDb
        def  fmt_IT = NumberFormat.getNumberInstance(Locale.ITALIAN)
        fmt_IT.setMaximumFractionDigits(2)
        def programma, venditore, dealer, datapagamento, codDealer, pacchetto, satellitare, zonaTariffaria, tariffa, rinnovo, nuovo, finanziata, provvVendi, provvDealer, provvGMF, netPayable, rimborso, account, totProvv, ritenuta, coperturaR,codCampagna,premioLordoCampagna,premioNettoCampagna,premioLordoCliente,premioNettoCliente
        def polizzeCash = [polizze: []]
        def polizzeLeasing = [polizze: []]
        def polizzeFinanziate = [polizze: []]
        def polizzeRinn = [polizze: []]
        def allPolizze = [polizze: []]
        def polizzaTotali = [:]
        polizzeWS.each { polizzaWS ->
            if (polizzaWS.statodslip != "V") {
                if (polizzaWS.ramo.contains("U/12") || polizzaWS.ramo.contains("U/13") || polizzaWS.ramo.contains("U/14") || polizzaWS.ramo.contains("U/15") || polizzaWS.ramo.contains("U/16") || polizzaWS.ramo.contains("U/17") || polizzaWS.ramo.contains("U/18") || polizzaWS.ramo.contains("U/19") || polizzaWS.ramo.contains("U/20") || polizzaWS.ramo.contains("U/21") || polizzaWS.ramo.contains("U/22") || polizzaWS.ramo.contains("U/23") || polizzaWS.ramo.contains("U/24") || polizzaWS.ramo.contains("U/25") || polizzaWS.ramo.contains("U/26")
                        || polizzaWS.ramo.contains("W/46")|| polizzaWS.ramo.contains("W/47")|| polizzaWS.ramo.contains("W/48")|| polizzaWS.ramo.contains("W/49")|| polizzaWS.ramo.contains("W/50")|| polizzaWS.ramo.contains("W/51")|| polizzaWS.ramo.contains("W/52")|| polizzaWS.ramo.contains("W/53")|| polizzaWS.ramo.contains("W/54")|| polizzaWS.ramo.contains("W/55")|| polizzaWS.ramo.contains("W/56")|| polizzaWS.ramo.contains("W/57")|| polizzaWS.ramo.contains("W/58")|| polizzaWS.ramo.contains("W/59")|| polizzaWS.ramo.contains("W/60")) {
                    polizzeCash.polizze << polizzaWS
                } else if (polizzaWS.ramo.contains("U/29") || polizzaWS.ramo.contains("U/30") || polizzaWS.ramo.contains("U/31") || polizzaWS.ramo.contains("U/32") || polizzaWS.ramo.contains("U/33") || polizzaWS.ramo.contains("U/34") || polizzaWS.ramo.contains("U/35") || polizzaWS.ramo.contains("U/36") || polizzaWS.ramo.contains("U/37") || polizzaWS.ramo.contains("U/38") || polizzaWS.ramo.contains("U/39") || polizzaWS.ramo.contains("U/40") || polizzaWS.ramo.contains("U/41") || polizzaWS.ramo.contains("U/42") || polizzaWS.ramo.contains("U/43")
                        || polizzaWS.ramo.contains("W/61") || polizzaWS.ramo.contains("W/62")|| polizzaWS.ramo.contains("W/63")|| polizzaWS.ramo.contains("W/64")|| polizzaWS.ramo.contains("W/65")|| polizzaWS.ramo.contains("W/66")|| polizzaWS.ramo.contains("W/67")|| polizzaWS.ramo.contains("W/68")|| polizzaWS.ramo.contains("W/69")|| polizzaWS.ramo.contains("W/70")|| polizzaWS.ramo.contains("W/71")
                        || polizzaWS.ramo.contains("W/72") || polizzaWS.ramo.contains("W/73")|| polizzaWS.ramo.contains("W/74")|| polizzaWS.ramo.contains("W/75")) {
                    polizzeLeasing.polizze << polizzaWS
                } else if (polizzaWS.ramo.contains("U/44") || polizzaWS.ramo.contains("U/45") || polizzaWS.ramo.contains("U/46") || polizzaWS.ramo.contains("U/47") || polizzaWS.ramo.contains("U/48") || polizzaWS.ramo.contains("U/49") || polizzaWS.ramo.contains("U/50") || polizzaWS.ramo.contains("U/51") || polizzaWS.ramo.contains("U/52") || polizzaWS.ramo.contains("U/53") || polizzaWS.ramo.contains("U/54") || polizzaWS.ramo.contains("U/55") || polizzaWS.ramo.contains("U/56") || polizzaWS.ramo.contains("U/57")
                        || polizzaWS.ramo.contains("U/58")|| polizzaWS.ramo.contains("W/76") || polizzaWS.ramo.contains("W/77") || polizzaWS.ramo.contains("W/78") || polizzaWS.ramo.contains("W/79") || polizzaWS.ramo.contains("W/80") || polizzaWS.ramo.contains("W/81") || polizzaWS.ramo.contains("W/82") || polizzaWS.ramo.contains("W/83") || polizzaWS.ramo.contains("W/84") || polizzaWS.ramo.contains("W/85") || polizzaWS.ramo.contains("W/86")|| polizzaWS.ramo.contains("W/87")|| polizzaWS.ramo.contains("W/88")|| polizzaWS.ramo.contains("W/89")
                        || polizzaWS.ramo.contains("W/90") || polizzaWS.ramo.contains("W/91")) {
                    polizzeFinanziate.polizze << polizzaWS
                } else if (polizzaWS.ramo.contains("U/59") || polizzaWS.ramo.contains("U/60") || polizzaWS.ramo.contains("U/61") || polizzaWS.ramo.contains("U/62") || polizzaWS.ramo.contains("U/63") || polizzaWS.ramo.contains("U/64") || polizzaWS.ramo.contains("U/65") || polizzaWS.ramo.contains("U/66") || polizzaWS.ramo.contains("U/67") || polizzaWS.ramo.contains("U/68")
                        || polizzaWS.ramo.contains("W/92")|| polizzaWS.ramo.contains("W/94") || polizzaWS.ramo.contains("W/95")|| polizzaWS.ramo.contains("W/96")|| polizzaWS.ramo.contains("W/97")|| polizzaWS.ramo.contains("W/98")|| polizzaWS.ramo.contains("W/99")|| polizzaWS.ramo.contains("D/01")
                        || polizzaWS.ramo.contains("D/02")|| polizzaWS.ramo.contains("D/03") || polizzaWS.ramo.contains("D/04")  || polizzaWS.ramo.contains("D/05") || polizzaWS.ramo.contains("D/06") || polizzaWS.ramo.contains("D/07") || polizzaWS.ramo.contains("D/08") || polizzaWS.ramo.contains("D/09") || polizzaWS.ramo.contains("D/10") || polizzaWS.ramo.contains("D/11") || polizzaWS.ramo.contains("D/12")) {
                    polizzeRinn.polizze << polizzaWS
                }
            }
        }
        def polizzeF = polizzeFinanziate.polizze
        def polizzeL = polizzeLeasing.polizze
        def polizzeC = polizzeCash.polizze
        def polizzeRN = polizzeRinn.polizze
        boolean isRinnovo = false
        def polizzaRinn
        def wb = Stopwatch.log {
            ExcelBuilder.create {
                style("header") {
                    background bisque
                    font {
                        bold(true)
                    }
                }
                sheet("Retail - Finanziate") {
                    row(style: "header") {
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                        cell("CODICE CAMPAGNA")
                        cell("PREMIO LORDO CAMPAGNA")
                        cell("PREMIO NETTO CAMPAGNA")
                        cell("PREMIO LORDO CLIENTE")
                        cell("PREMIO NETTO CLIENTE")

                    }
                    if (polizzeF) {
                        polizzeF.each { polizzaWS ->
                            //per le polizze leasing / finanziate
                            if (polizzaWS.ramo.contains("U/50") || polizzaWS.ramo.contains("U/51") || polizzaWS.ramo.contains("U/52") || polizzaWS.ramo.contains("W/83") || polizzaWS.ramo.contains("W/84")|| polizzaWS.ramo.contains("W/85")) {
                                pacchetto = "PLATINUM"
                                coperturaR = "PLATINUM"
                            } else if (polizzaWS.ramo.contains("U/44") || polizzaWS.ramo.contains("U/45") || polizzaWS.ramo.contains("U/46") || polizzaWS.ramo.contains("W/76") || polizzaWS.ramo.contains("W/77") || polizzaWS.ramo.contains("W/79") ) {
                                pacchetto = "SILVER"
                                coperturaR = "SILVER"
                            } else if (polizzaWS.ramo.contains("U/47") || polizzaWS.ramo.contains("U/48") || polizzaWS.ramo.contains("U/49")  || polizzaWS.ramo.contains("W/80") || polizzaWS.ramo.contains("W/81") || polizzaWS.ramo.contains("W/82")) {
                                pacchetto = "GOLD"
                                coperturaR = "GOLD"
                            } else if (polizzaWS.ramo.contains("U/53") || polizzaWS.ramo.contains("U/54") || polizzaWS.ramo.contains("U/55") || polizzaWS.ramo.contains("W/86") || polizzaWS.ramo.contains("W/87")|| polizzaWS.ramo.contains("W/88")) {
                                pacchetto = "PLATINUM+COLLISIONE"
                                coperturaR = "PLATINUM COLLISIONE"
                            } else if (polizzaWS.ramo.contains("U/56") || polizzaWS.ramo.contains("U/57") || polizzaWS.ramo.contains("U/58")|| polizzaWS.ramo.contains("W/89")|| polizzaWS.ramo.contains("W/90") || polizzaWS.ramo.contains("W/91")) {
                                pacchetto = "PLATINUM+KASKO"
                                coperturaR = "PLATINUM KASKO"
                            }
                            def noPolizza = polizzaWS.noPolizza
                            if (polizzaWS.onstar.toString().trim().contains("SATELITTARE")) {
                                satellitare = "S"
                            } else {
                                satellitare = "N"
                            }
                            rimborso = "N"
                            def premioLordo, premioImpo, valoreAssicurato
                            def polizza = Polizza.findByNoPolizza(noPolizza.toString().trim())
                            def dataInizio = polizzaWS.inizio
                            if (dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            } else {
                                dataInizio = ""
                            }
                            def dataFine = polizzaWS.fine
                            if (dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            } else {
                                dataFine = ""
                            }
                            if (polizza) {
                                if(polizza.tariffa!=1){
                                    programma = "Flex Protection"
                                }else{
                                    programma = "Flex Protection 2017"
                                }
                                venditore = polizza.venditore.toUpperCase()
                                dealer = polizza.dealer.ragioneSocialeD.toUpperCase()
                                codDealer = polizza.dealer.codice.toString().trim().padLeft(4, "0")
                                zonaTariffaria = polizza.codiceZonaTerritoriale
                                tariffa = "${polizza.dealer.dealerType} - ${polizza.step}"
                                rinnovo = "0"
                                nuovo = "NUOVI"
                                premioLordo = polizza.premioLordo
                                premioImpo = polizza.premioImponibile
                                valoreAssicurato = polizza.valoreAssicurato
                                provvVendi = polizza.provvVenditore
                                provvDealer = polizza.provvDealer
                                provvGMF = polizza.provvGmfi
                                account = polizza.dealer.salesSpecialist.toUpperCase()
                                def nomeSales = account.split(" ")
                                def salesSpec
                                if (nomeSales.size() == 2) {
                                    salesSpec = nomeSales[1] + " " + nomeSales[0]
                                } else if (nomeSales.size() > 2) {
                                    for (int ind = 0; ind < nomeSales.size(); ind++) {
                                        salesSpec += nomeSales[ind] + " "
                                    }
                                }
                                datapagamento = polizzaWS.datacompagnia
                                if (datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                } else {
                                    datapagamento = ""
                                }
                                finanziata = "S"
                                totProvv = provvDealer + provvVendi + provvGMF
                                ritenuta = 0.046 * (provvDealer + provvVendi + provvGMF)
                                netPayable = premioLordo - (provvDealer + provvVendi + provvGMF)
                                codCampagna=polizza.codCampagna
                                premioLordoCampagna=polizza.premioLordoTerzi
                                premioNettoCampagna=polizza.premioTerzi
                                premioLordoCliente=polizza.premioLordoCliente
                                premioNettoCliente=polizza.premioCliente
                                if (polizzaWS.statodslip != "0" && polizzaWS.statodslip != "V" ) {
                                    rimborso = "Y"
                                    isRinnovo = false
                                    polizzaRinn = ""
                                    netPayable = netPayable.negate()
                                    ritenuta = ritenuta.negate()
                                    totProvv = totProvv.negate()
                                    premioImpo = premioImpo.negate()
                                    premioLordo = premioLordo.negate()
                                    provvDealer = provvDealer.negate()
                                    provvVendi = provvVendi.negate()
                                    provvGMF = provvGMF.negate()

                                    /*if(polizzaWS.statodslip=="E"){
                                        def codOperazione = "E"
                                        def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                        println "premio lordo ${premioLordo}"
                                        println "provvDealer ${provvDealer}"
                                        println "provvVendi ${provvVendi}"
                                        println "provvGMF ${provvGMF}"
                                    }else if(polizzaWS.statodslip=="F"){
                                        def codOperazione = "F"
                                        def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                        premioLordo = rispostawe.premioLordo
                                        premioImpo = rispostawe.premioImpo
                                        provvDealer = rispostawe.provvDealer
                                        provvVendi = rispostawe.provvVendi
                                        provvGMF = rispostawe.provvGMF
                                    }else if(polizzaWS.statodslip=="G"){
                                        def codOperazione = "G"
                                         def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                         premioLordo = rispostawe.premioLordo
                                         premioImpo = rispostawe.premioImpo
                                         provvDealer = rispostawe.provvDealer
                                         provvVendi = rispostawe.provvVendi
                                         provvGMF = rispostawe.provvGMF
                                    }
                                    else
                                    if(polizzaWS.statodslip=="S") {
                                        println "premio lordo ${premioLordo}"
                                        println "provvDealer ${provvDealer}"
                                        println "provvVendi ${provvVendi}"
                                        println "provvGMF ${provvGMF}"
                                        netPayable = netPayable.negate()
                                        ritenuta = ritenuta.negate()
                                        totProvv = totProvv.negate()
                                        premioImpo = premioImpo.negate()
                                        premioLordo = premioLordo.negate()
                                        provvDealer = provvDealer.negate()
                                        provvVendi = provvVendi.negate()
                                        provvGMF = provvGMF.negate()
                                    }*/
                                }
                                def numPolizza = ""
                                numPolizza = polizza.noPolizza
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza = TipoPolizza.FINANZIATE
                                polizzaTotali.noPolizza = numPolizza
                                polizzaTotali.programma = programma
                                polizzaTotali.assicurato = polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore = venditore
                                polizzaTotali.concessionario = dealer
                                polizzaTotali.dataPagamento = datapagamento
                                polizzaTotali.dataCopertura = dataInizio
                                polizzaTotali.dataImmisione = dataInizio
                                polizzaTotali.dataScadenza = dataFine
                                polizzaTotali.valoreAssicurato = valoreAssicurato
                                polizzaTotali.cod_Conc = codDealer
                                polizzaTotali.cod_vend = ""
                                polizzaTotali.targa = polizzaWS.targa
                                polizzaTotali.telaio = polizzaWS.telaio
                                polizzaTotali.pacchetto = pacchetto
                                polizzaTotali.satellitare = satellitare
                                polizzaTotali.livello = polizzaWS.anni
                                polizzaTotali.zonaTariffaria = zonaTariffaria
                                polizzaTotali.tariffa = tariffa
                                polizzaTotali.rinnovo = rinnovo
                                polizzaTotali.nuovo = nuovo
                                polizzaTotali.durata = polizzaWS.durata
                                polizzaTotali.premioLordo = premioLordo
                                polizzaTotali.premioImpo = premioImpo
                                polizzaTotali.provvVendi = provvVendi
                                polizzaTotali.provvDealer = provvDealer
                                polizzaTotali.provvGmfi = provvGMF
                                polizzaTotali.provvAddFee = ""
                                polizzaTotali.provvAgg = ""
                                polizzaTotali.account = salesSpec
                                polizzaTotali.finanziata = finanziata
                                polizzaTotali.rimborso = rimborso
                                polizzaTotali.noteRimb = ""
                                polizzaTotali.totProvv = totProvv
                                polizzaTotali.ritenuta = ritenuta
                                polizzaTotali.netPayable = netPayable
                                polizzaTotali.codCampagna = codCampagna
                                polizzaTotali.premioLordoCampagna = premioLordoCampagna
                                polizzaTotali.premioNettoCampagna = premioNettoCampagna
                                polizzaTotali.premioLordoCliente = premioLordoCliente
                                polizzaTotali.premioNettoCliente = premioNettoCliente

                                row {
                                    cell(numPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataInizio)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizzaWS.targa)
                                    cell(polizzaWS.telaio)
                                    cell(pacchetto)
                                    cell(satellitare)
                                    cell(polizzaWS.anni)
                                    cell(zonaTariffaria)
                                    cell(tariffa)
                                    cell(rinnovo)
                                    cell(nuovo)
                                    cell(polizzaWS.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(salesSpec)
                                    cell(finanziata)
                                    cell(rimborso)
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                    cell(codCampagna)
                                    cell(premioLordoCampagna)
                                    cell(premioNettoCampagna)
                                    cell(premioLordoCliente)
                                    cell(premioNettoCliente)
                                }
                                for (int i = 0; i < 36; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali = [:]
                            }
                        }
                    } else {
                        logg = new Log(parametri: "non ci sono polizze finanziate ", operazione: "generaFileGMFGEN", pagina: "LISTA POLIZZE CASH")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }
                sheet("Retail - Leasing") {
                    row(style: "header") {
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                        cell("CODICE CAMPAGNA")
                        cell("PREMIO LORDO CAMPAGNA")
                        cell("PREMIO NETTO CAMPAGNA")
                        cell("PREMIO LORDO CLIENTE")
                        cell("PREMIO NETTO CLIENTE")
                    }
                    if (polizzeL) {
                        polizzeL.each { polizzaWS ->
                            //per le polizze leasing / finanziate
                            if (polizzaWS.ramo.contains("U/35") || polizzaWS.ramo.contains("U/36") || polizzaWS.ramo.contains("U/37") || polizzaWS.ramo.contains("W/67") || polizzaWS.ramo.contains("W/68") || polizzaWS.ramo.contains("W/69")) {
                                pacchetto = "PLATINUM"
                            } else if (polizzaWS.ramo.contains("U/29") || polizzaWS.ramo.contains("U/30") || polizzaWS.ramo.contains("U/31") || polizzaWS.ramo.contains("W/61") || polizzaWS.ramo.contains("W/62") || polizzaWS.ramo.contains("W/63")) {
                                pacchetto = "SILVER"
                            } else if (polizzaWS.ramo.contains("U/32") || polizzaWS.ramo.contains("U/33") || polizzaWS.ramo.contains("U/34") || polizzaWS.ramo.contains("W/64") || polizzaWS.ramo.contains("W/65")|| polizzaWS.ramo.contains("W/66")) {
                                pacchetto = "GOLD"
                            } else if (polizzaWS.ramo.contains("U/38") || polizzaWS.ramo.contains("U/39") || polizzaWS.ramo.contains("U/40")|| polizzaWS.ramo.contains("W/70") || polizzaWS.ramo.contains("W/71")|| polizzaWS.ramo.contains("W/72")) {
                                pacchetto = "PLATINUM+COLLISIONE"
                            } else if (polizzaWS.ramo.contains("U/41") || polizzaWS.ramo.contains("U/42") || polizzaWS.ramo.contains("U/43") || polizzaWS.ramo.contains("W/73") || polizzaWS.ramo.contains("W/74") || polizzaWS.ramo.contains("W/75")) {
                                pacchetto = "PLATINUM+KASKO"
                            }
                            def noPolizza = polizzaWS.noPolizza
                            //println polizzaWS.codiceDSLIP
                            if (polizzaWS.onstar.toString().trim().contains("SATELITTARE")) {
                                satellitare = "S"
                            } else {
                                satellitare = "N"
                            }
                            rimborso = "N"
                            def premioLordo, premioImpo, valoreAssicurato

                            def polizza = Polizza.findByNoPolizza(noPolizza.toString().trim())
                            def dataInizio = polizzaWS.inizio
                            if (dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            } else {
                                dataInizio = ""
                            }
                            def dataFine = polizzaWS.fine
                            if (dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            } else {
                                dataFine = ""
                            }
                            if (polizza) {
                                if(polizza.tariffa!=1){
                                    programma = "Flex Protection"
                                }else{
                                    programma = "Flex Protection 2017"
                                }
                                venditore = polizza.venditore.toUpperCase()
                                dealer = polizza.dealer.ragioneSocialeD.toUpperCase()
                                codDealer = polizza.dealer.codice.toString().trim().padLeft(4, "0")
                                zonaTariffaria = polizza.codiceZonaTerritoriale
                                tariffa = "${polizza.dealer.dealerType} - ${polizza.step}"
                                rinnovo = "0"
                                nuovo = "NUOVI"
                                premioLordo = polizza.premioLordo
                                premioImpo = polizza.premioImponibile
                                valoreAssicurato = polizza.valoreAssicurato
                                provvVendi = polizza.provvVenditore
                                provvDealer = polizza.provvDealer
                                provvGMF = polizza.provvGmfi
                                account = polizza.dealer.salesSpecialist.toUpperCase()
                                def nomeSales = account.split(" ")
                                def salesSpec
                                //println nomeSales
                                if (nomeSales.size() == 2) {
                                    salesSpec = nomeSales[1] + " " + nomeSales[0]
                                } else if (nomeSales.size() > 2) {
                                    for (int ind = 0; ind < nomeSales.size(); ind++) {
                                        salesSpec += nomeSales[ind] + " "
                                    }
                                }
                                datapagamento = polizzaWS.datacompagnia
                                if (datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                } else {
                                    datapagamento = ""
                                }
                                finanziata = "S"
                                totProvv = provvDealer + provvVendi + provvGMF
                                ritenuta = 0.046 * (provvDealer + provvVendi + provvGMF)
                                netPayable = premioLordo - (provvDealer + provvVendi + provvGMF)
                                codCampagna=polizza.codCampagna
                                premioLordoCampagna=polizza.premioLordoTerzi
                                premioNettoCampagna=polizza.premioTerzi
                                premioLordoCliente=polizza.premioLordoCliente
                                premioNettoCliente=polizza.premioCliente
                                if (polizzaWS.statodslip != "0" && polizzaWS.statodslip != "V") {
                                    isRinnovo = false
                                    polizzaRinn = ""
                                    rimborso = "Y"
                                    ritenuta = ritenuta.negate()
                                    totProvv = totProvv.negate()
                                    premioLordo = premioLordo.negate()
                                    premioImpo = premioImpo.negate()
                                    provvDealer = provvDealer.negate()
                                    provvVendi = provvVendi.negate()
                                    provvGMF = provvGMF.negate()
                                    /*if (polizzaWS.statodslip != "S") {
                                        if(polizza.codOperazione=="E"){
                                            def codOperazione = "E"
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF

                                        }else if(polizza.codOperazione=="F"){
                                            def codOperazione = "F"
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF

                                        }else if(polizza.codOperazione=="G"){
                                            def codOperazione = "G"
                                            /*def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF

                                        }

                                    } else {

                                    }*/

                                }
                                def numPolizza = polizza.noPolizza
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza = TipoPolizza.LEASING
                                polizzaTotali.noPolizza = numPolizza
                                polizzaTotali.programma = programma
                                polizzaTotali.assicurato = polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore = venditore
                                polizzaTotali.concessionario = dealer
                                polizzaTotali.dataPagamento = datapagamento
                                polizzaTotali.dataCopertura = dataInizio
                                polizzaTotali.dataImmisione = dataInizio
                                polizzaTotali.dataScadenza = dataFine
                                polizzaTotali.valoreAssicurato = valoreAssicurato
                                polizzaTotali.cod_Conc = codDealer
                                polizzaTotali.cod_vend = ""
                                polizzaTotali.targa = polizzaWS.targa
                                polizzaTotali.telaio = polizzaWS.telaio
                                polizzaTotali.pacchetto = pacchetto
                                polizzaTotali.satellitare = satellitare
                                polizzaTotali.livello = polizzaWS.anni
                                polizzaTotali.zonaTariffaria = zonaTariffaria
                                polizzaTotali.tariffa = tariffa
                                polizzaTotali.rinnovo = rinnovo
                                polizzaTotali.nuovo = nuovo
                                polizzaTotali.durata = polizzaWS.durata
                                polizzaTotali.premioLordo = premioLordo
                                polizzaTotali.premioImpo = premioImpo
                                polizzaTotali.provvVendi = provvVendi
                                polizzaTotali.provvDealer = provvDealer
                                polizzaTotali.provvGmfi = provvGMF
                                polizzaTotali.provvAddFee = ""
                                polizzaTotali.provvAgg = ""
                                polizzaTotali.account = salesSpec
                                polizzaTotali.finanziata = finanziata
                                polizzaTotali.rimborso = rimborso
                                polizzaTotali.noteRimb = ""
                                polizzaTotali.totProvv = totProvv
                                polizzaTotali.ritenuta = ritenuta
                                polizzaTotali.netPayable = netPayable
                               polizzaTotali.codCampagna = codCampagna
                                polizzaTotali.premioLordoCampagna = premioLordoCampagna
                                polizzaTotali.premioNettoCampagna = premioNettoCampagna
                                polizzaTotali.premioLordoCliente = premioLordoCliente
                                polizzaTotali.premioNettoCliente = premioNettoCliente
                                row {
                                    cell(numPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataInizio)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizzaWS.targa)
                                    cell(polizzaWS.telaio)
                                    cell(pacchetto)
                                    cell(satellitare)
                                    cell(polizzaWS.anni)
                                    cell(zonaTariffaria)
                                    cell(tariffa)
                                    cell(rinnovo)
                                    cell(nuovo)
                                    cell(polizzaWS.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(salesSpec)
                                    cell(finanziata)
                                    cell(rimborso)
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                    cell(codCampagna)
                                    cell(premioLordoCampagna)
                                    cell(premioNettoCampagna)
                                    cell(premioLordoCliente)
                                    cell(premioNettoCliente)
                                }
                                for (int i = 0; i < 36; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali = [:]
                            }
                        }
                    } else {
                        logg = new Log(parametri: "non ci sono polizze leasing ", operazione: "generaFileGMFGEN", pagina: "LISTA POLIZZE CASH")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }
                sheet("Retail - Cash") {
                    row(style: "header") {
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                    }
                    if (polizzeC) {
                        polizzeC.each { polizzaWS ->
                            //per le polizze leasing / cash / finanziate
                            def noPolizza = polizzaWS.noPolizza
                            //println polizzaWS.codiceDSLIP
                            if (polizzaWS.ramo.contains("U/18") || polizzaWS.ramo.contains("U/19") || polizzaWS.ramo.contains("U/20")|| polizzaWS.ramo.contains("W/52")|| polizzaWS.ramo.contains("W/53")|| polizzaWS.ramo.contains("W/54")) {
                                pacchetto = "PLATINUM"
                            } else if (polizzaWS.ramo.contains("U/12") || polizzaWS.ramo.contains("U/13") || polizzaWS.ramo.contains("U/14") || polizzaWS.ramo.contains("W/46")|| polizzaWS.ramo.contains("W/47")|| polizzaWS.ramo.contains("W/48")) {
                                pacchetto = "SILVER"
                            } else if (polizzaWS.ramo.contains("U/15") || polizzaWS.ramo.contains("U/16") || polizzaWS.ramo.contains("U/17")|| polizzaWS.ramo.contains("W/49") || polizzaWS.ramo.contains("W/50")|| polizzaWS.ramo.contains("W/51")) {
                                pacchetto = "GOLD"
                            } else if (polizzaWS.ramo.contains("U/21") || polizzaWS.ramo.contains("U/22") || polizzaWS.ramo.contains("U/23")|| polizzaWS.ramo.contains("W/55")|| polizzaWS.ramo.contains("W/56")|| polizzaWS.ramo.contains("W/57")) {
                                pacchetto = "PLATINUM+COLLISIONE"
                            } else if (polizzaWS.ramo.contains("U/24") || polizzaWS.ramo.contains("U/25") || polizzaWS.ramo.contains("U/26")|| polizzaWS.ramo.contains("W/58")|| polizzaWS.ramo.contains("W/59")|| polizzaWS.ramo.contains("W/60")) {
                                pacchetto = "PLATINUM+KASKO"
                            }
                            if (polizzaWS.onstar.toString().trim().contains("SATELITTARE")) {
                                satellitare = "S"
                            } else {
                                satellitare = "N"
                            }
                            rimborso = "N"
                            def premioLordo, premioImpo, valoreAssicurato
                            def polizza = Polizza.findByNoPolizza(noPolizza.toString().trim())
                            def dataInizio = polizzaWS.inizio
                            if (dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            } else {
                                dataInizio = ""
                            }
                            def dataFine = polizzaWS.fine
                            if (dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            } else {
                                dataFine = ""
                            }
                            if (polizza) {
                                if(polizza.tariffa!=1){
                                    programma = "Flex Protection"
                                }else{
                                    programma = "Flex Protection 2017"
                                }
                                venditore = polizza.venditore.toUpperCase()
                                dealer = polizza.dealer.ragioneSocialeD.toUpperCase()
                                codDealer = polizza.dealer.codice.toString().trim().padLeft(4, "0")
                                zonaTariffaria = polizza.codiceZonaTerritoriale
                                tariffa = "${polizza.dealer.dealerType} - ${polizza.step}"
                                rinnovo = "0"
                                nuovo = "NUOVI"
                                premioLordo = polizza.premioLordo
                                premioImpo = polizza.premioImponibile
                                valoreAssicurato = polizza.valoreAssicurato
                                provvVendi = polizza.provvVenditore
                                provvDealer = polizza.provvDealer
                                provvGMF = polizza.provvGmfi
                                account = polizza.dealer.salesSpecialist.toUpperCase()
                                def nomeSales = account.split(" ")
                                def salesSpec
                                //println nomeSales
                                if (nomeSales.size() == 2) {
                                    salesSpec = nomeSales[1] + " " + nomeSales[0]
                                } else if (nomeSales.size() > 2) {
                                    for (int ind = 0; ind < nomeSales.size(); ind++) {
                                        salesSpec += nomeSales[ind] + " "
                                    }
                                }
                                datapagamento = polizzaWS.datacomunicaz
                                if (datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                } else {
                                    datapagamento = ""
                                }
                                finanziata = "N"
                                totProvv = provvDealer + provvVendi + provvGMF
                                ritenuta = 0.046 * (provvDealer + provvVendi + provvGMF)
                                netPayable = premioLordo - (provvDealer + provvVendi + provvGMF)
                                if (polizzaWS.statodslip != "0" && polizzaWS.statodslip != "V") {
                                    isRinnovo = false
                                    polizzaRinn = ""
                                    rimborso = "Y"
                                    ritenuta = ritenuta.negate()
                                    totProvv = totProvv.negate()
                                    premioLordo = premioLordo.negate()
                                    premioImpo = premioImpo.negate()
                                    provvDealer = provvDealer.negate()
                                    provvVendi = provvVendi.negate()
                                    provvGMF = provvGMF.negate()
                                    /*if (polizzaWS.statodslip != "S") {
                                        if(polizza.codOperazione=="E"){
                                            def codOperazione = "E"
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                        }else if(polizza.codOperazione=="F"){
                                            def codOperazione = "F"
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF
                                        }else if(polizza.codOperazione=="G"){
                                            def codOperazione = "G"
                                            /*def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF
                                        }

                                    } else {

                                    }*/
                                }
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza = TipoPolizza.CASH
                                polizzaTotali.noPolizza = polizza.noPolizza
                                polizzaTotali.programma = programma
                                polizzaTotali.assicurato = polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore = venditore
                                polizzaTotali.concessionario = dealer
                                polizzaTotali.dataPagamento = datapagamento
                                polizzaTotali.dataCopertura = dataInizio
                                polizzaTotali.dataImmisione = dataInizio
                                polizzaTotali.dataScadenza = dataFine
                                polizzaTotali.valoreAssicurato = valoreAssicurato
                                polizzaTotali.cod_Conc = codDealer
                                polizzaTotali.cod_vend = ""
                                polizzaTotali.targa = polizzaWS.targa
                                polizzaTotali.telaio = polizzaWS.telaio
                                polizzaTotali.pacchetto = pacchetto
                                polizzaTotali.satellitare = satellitare
                                polizzaTotali.livello = polizzaWS.anni
                                polizzaTotali.zonaTariffaria = zonaTariffaria
                                polizzaTotali.tariffa = tariffa
                                polizzaTotali.rinnovo = rinnovo
                                polizzaTotali.nuovo = nuovo
                                polizzaTotali.durata = polizzaWS.durata
                                polizzaTotali.premioLordo = premioLordo
                                polizzaTotali.premioImpo = premioImpo
                                polizzaTotali.provvVendi = provvVendi
                                polizzaTotali.provvDealer = provvDealer
                                polizzaTotali.provvGmfi = provvGMF
                                polizzaTotali.provvAddFee = ""
                                polizzaTotali.provvAgg = ""
                                polizzaTotali.account = salesSpec
                                polizzaTotali.finanziata = finanziata
                                polizzaTotali.rimborso = rimborso
                                polizzaTotali.noteRimb = ""
                                polizzaTotali.totProvv = totProvv
                                polizzaTotali.ritenuta = ritenuta
                                polizzaTotali.netPayable = netPayable

                                row {
                                    cell(polizza.noPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataInizio)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizzaWS.targa)
                                    cell(polizzaWS.telaio)
                                    cell(pacchetto)
                                    cell(satellitare)
                                    cell(polizzaWS.anni)
                                    cell(zonaTariffaria)
                                    cell(tariffa)
                                    cell(rinnovo)
                                    cell(nuovo)
                                    cell(polizzaWS.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(salesSpec)
                                    cell(finanziata)
                                    cell(rimborso)
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                }
                                for (int i = 0; i < 36; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali = [:]
                            }

                        }
                    }
                }
                sheet("Retail - Rinnovi") {
                    row(style: "header") {
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                    }
                    if (polizzeRN) {
                        polizzeRN.each { polizzaWS ->
                            def noPolizza = polizzaWS.noPolizza
                            if (polizzaWS.ramo.contains("U/61") || polizzaWS.ramo.contains("U/66")|| polizzaWS.ramo.contains("W/94") || polizzaWS.ramo.contains("W/99")|| polizzaWS.ramo.contains("D/05") || polizzaWS.ramo.contains("D/10")) {
                                pacchetto = "PLATINUM"
                            } else if (polizzaWS.ramo.contains("U/59") || polizzaWS.ramo.contains("U/64")|| polizzaWS.ramo.contains("W/92") || polizzaWS.ramo.contains("W/97") || polizzaWS.ramo.contains("D/03") || polizzaWS.ramo.contains("D/08")) {
                                pacchetto = "SILVER"
                            } else if (polizzaWS.ramo.contains("U/60") || polizzaWS.ramo.contains("U/65")|| polizzaWS.ramo.contains("W/93") || polizzaWS.ramo.contains("W/98") || polizzaWS.ramo.contains("D/04") || polizzaWS.ramo.contains("D/09")) {
                                pacchetto = "GOLD"
                            } else if (polizzaWS.ramo.contains("U/62") || polizzaWS.ramo.contains("U/67") || polizzaWS.ramo.contains("W/95")  || polizzaWS.ramo.contains("D/01")  || polizzaWS.ramo.contains("D/06") || polizzaWS.ramo.contains("D/11")) {
                                pacchetto = "PLATINUM+COLLISIONE"
                            } else if (polizzaWS.ramo.contains("U/63") || polizzaWS.ramo.contains("U/68")|| polizzaWS.ramo.contains("W/96")|| polizzaWS.ramo.contains("D/02") || polizzaWS.ramo.contains("D/07") || polizzaWS.ramo.contains("D/12")) {
                                pacchetto = "PLATINUM+KASKO"
                            }
                            if (polizzaWS.onstar.toString().trim().contains("SATELITTARE")) {
                                satellitare = "S"
                            } else {
                                satellitare = "N"
                            }
                            rimborso = "N"
                            def premioLordo, premioImpo, valoreAssicurato
                            if (polizzaWS.premio) {
                                premioLordo = (BigDecimal) fmt_IT.parseObject(polizzaWS.premio.toString())
                            } else {
                                premioLordo = 0.0
                            }
                            if (polizzaWS.imponibile) {
                                premioImpo = (BigDecimal) fmt_IT.parseObject(polizzaWS.imponibile.toString())
                            } else {
                                premioImpo = 0.0
                            }
                            if (polizzaWS.valoreAssicurato) {
                                valoreAssicurato = polizzaWS.valoreAssicurato
                            } else {
                                valoreAssicurato = 0.0
                            }
                            def dataInizio = polizzaWS.inizio
                            if (dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            } else {
                                dataInizio = ""
                            }
                            def dataFine = polizzaWS.fine
                            if (dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            } else {
                                dataFine = ""
                            }
                            def result = db.firstRow("select polizze.annualita, rinnovi.provincia, rinnovi.provv_gm, polizze.versione_tariffa from polizze join rinnovi on polizze.idpolizze=rinnovi.idpolizze where targa='${polizzaWS.targa}' or telaio='${polizzaWS.telaio}'")
                            if (result) {
                                def versione=result[3]
                                //println "versione $versione"
                                if(versione==1){
                                    programma = "Flex Protection Rinnovi"

                                }else{
                                    programma = "Flex Protection Rinnovi 2017"

                                }
                                venditore = ""
                                dealer = ""
                                codDealer = ""
                                satellitare = "N"
                                zonaTariffaria = ZoneTarif.findByProvincia(result[1]).zona
                                tariffa = ""
                                rinnovo = "1"
                                nuovo = "RINNOVO"
                                provvVendi = ""
                                provvDealer = ""
                                provvGMF = result[2]
                                //provvGMF=provvGMF.toString().replace(".",",")
                                provvGMF = new BigDecimal(provvGMF)
                                provvGMF = provvGMF.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                //provvGMF=fmt_IT.format(provvGMF)
                                account = ""
                                datapagamento = polizzaWS.datacomunicaz
                                if (datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                } else {
                                    datapagamento = ""
                                }
                                finanziata = "N"
                                totProvv = provvGMF
                                ritenuta = 0.046 * (totProvv)
                                netPayable = premioLordo - (provvGMF)
                                if (polizzaWS.statodslip != "0" && polizzaWS.statodslip != "V") {
                                    rimborso = "Y"
                                    if (polizzaWS.statodslip != "S") {
                                        if (polizzaWS.statodslip == "E") {
                                            def codOperazione = "E"
                                            polizzaRinn = []
                                            polizzaRinn = IAssicurWebService.getPolizzeRinnoviperAnnulla(polizzaWS.noPolizza).polizze
                                            isRinnovo = true
                                            def polizza = ""
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            //premioLordo = rispostawe.premioLordo
                                            //premioImpo = rispostawe.premioImpo
                                            //provvDealer = rispostawe.provvDealer
                                            //provvVendi = rispostawe.provvVendi
                                            //provvGMF = rispostawe.provvGMF
                                            if (rispostawe.premioLordo) {//(BigDecimal) decimalFormat.parse(
                                                rispostawe.premioLordo=rispostawe.premioLordo.toString().replace(".",",")
                                                premioLordo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioLordo.toString())
                                                premioLordo=premioLordo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioLordo = 0.0
                                            }
                                            if (rispostawe.premioImpo) {
                                                rispostawe.premioImpo=rispostawe.premioImpo.toString().replace(".",",")
                                                premioImpo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioImpo.toString())
                                                premioImpo=premioImpo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioImpo = 0.0
                                            }
                                            if (rispostawe.provvDealer) {
                                                rispostawe.provvDealer=rispostawe.provvDealer.toString().replace(".",",")
                                                provvDealer = (BigDecimal) fmt_IT.parseObject(rispostawe.provvDealer.toString())
                                                provvDealer=provvDealer.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvDealer = 0.0
                                            }
                                            if (rispostawe.provvVendi) {
                                                rispostawe.provvVendi=rispostawe.provvVendi.toString().replace(".",",")
                                                provvVendi = (BigDecimal) fmt_IT.parseObject(rispostawe.provvVendi.toString())
                                                provvVendi=provvVendi.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvVendi = 0.0
                                            }
                                            if (rispostawe.provvGMF) {
                                                rispostawe.provvGMF=rispostawe.provvGMF.toString().replace(".",",")
                                                provvGMF = (BigDecimal) fmt_IT.parseObject(rispostawe.provvGMF.toString())
                                                provvGMF=provvGMF.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvGMF = 0.0
                                            }
                                        }
                                        else if (polizzaWS.statodslip == "F") {
                                            def codOperazione = "F"
                                            polizzaRinn = []
                                            polizzaRinn = IAssicurWebService.getPolizzeRinnoviperAnnulla(polizzaWS.noPolizza).polizze
                                            isRinnovo = true
                                            def polizza = ""
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            //premioLordo = rispostawe.premioLordo
                                            //premioImpo = rispostawe.premioImpo
                                            //provvDealer = rispostawe.provvDealer
                                            //provvVendi = rispostawe.provvVendi
                                            //provvGMF = rispostawe.provvGMF
                                            if (rispostawe.premioLordo) {//(BigDecimal) decimalFormat.parse(
                                                rispostawe.premioLordo=rispostawe.premioLordo.toString().replace(".",",")
                                                premioLordo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioLordo.toString())
                                                premioLordo=premioLordo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioLordo = 0.0
                                            }
                                            if (rispostawe.premioImpo) {
                                                rispostawe.premioImpo=rispostawe.premioImpo.toString().replace(".",",")
                                                premioImpo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioImpo.toString())
                                                premioImpo=premioImpo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioImpo = 0.0
                                            }
                                            if (rispostawe.provvDealer) {
                                                rispostawe.provvDealer=rispostawe.provvDealer.toString().replace(".",",")
                                                provvDealer = (BigDecimal) fmt_IT.parseObject(rispostawe.provvDealer.toString())
                                                provvDealer=provvDealer.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvDealer = 0.0
                                            }
                                            if (rispostawe.provvVendi) {
                                                rispostawe.provvVendi=rispostawe.provvVendi.toString().replace(".",",")
                                                provvVendi = (BigDecimal) fmt_IT.parseObject(rispostawe.provvVendi.toString())
                                                provvVendi=provvVendi.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvVendi = 0.0
                                            }
                                            if (rispostawe.provvGMF) {
                                                rispostawe.provvGMF=rispostawe.provvGMF.toString().replace(".",",")
                                                provvGMF = (BigDecimal) fmt_IT.parseObject(rispostawe.provvGMF.toString())
                                                provvGMF=provvGMF.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvGMF = 0.0
                                            }
                                        }
                                        else if (polizzaWS.statodslip == "G") {
                                            def codOperazione = "G"
                                            polizzaRinn = []
                                            polizzaRinn = IAssicurWebService.getPolizzeRinnoviperAnnulla(polizzaWS.noPolizza).polizze
                                            isRinnovo = true
                                            def polizza = ""
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            //premioLordo = rispostawe.premioLordo
                                            //premioImpo = rispostawe.premioImpo
                                            //provvDealer = rispostawe.provvDealer
                                            //provvVendi = rispostawe.provvVendi
                                            //provvGMF = rispostawe.provvGMF
                                            if (rispostawe.premioLordo) {//(BigDecimal) decimalFormat.parse(
                                                rispostawe.premioLordo=rispostawe.premioLordo.toString().replace(".",",")
                                                premioLordo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioLordo.toString())
                                                premioLordo=premioLordo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioLordo = 0.0
                                            }
                                            if (rispostawe.premioImpo) {
                                                rispostawe.premioImpo=rispostawe.premioImpo.toString().replace(".",",")
                                                premioImpo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioImpo.toString())
                                                premioImpo=premioImpo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioImpo = 0.0
                                            }
                                            if (rispostawe.provvDealer) {
                                                rispostawe.provvDealer=rispostawe.provvDealer.toString().replace(".",",")
                                                provvDealer = (BigDecimal) fmt_IT.parseObject(rispostawe.provvDealer.toString())
                                                provvDealer=provvDealer.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvDealer = 0.0
                                            }
                                            if (rispostawe.provvVendi) {
                                                rispostawe.provvVendi=rispostawe.provvVendi.toString().replace(".",",")
                                                provvVendi = (BigDecimal) fmt_IT.parseObject(rispostawe.provvVendi.toString())
                                                provvVendi=provvVendi.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvVendi = 0.0
                                            }
                                            if (rispostawe.provvGMF) {
                                                rispostawe.provvGMF=rispostawe.provvGMF.toString().replace(".",",")
                                                provvGMF = (BigDecimal) fmt_IT.parseObject(rispostawe.provvGMF.toString())
                                                provvGMF=provvGMF.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvGMF = 0.0
                                            }
                                        }
                                        netPayable = premioLordo - (provvGMF)
                                        ritenuta = 0.046 * (provvGMF)
                                        totProvv = provvGMF
                                    } else if(polizzaWS.statodslip == "S") {
                                        netPayable = premioLordo + (provvGMF)
                                        ritenuta = ritenuta.negate()
                                        totProvv = totProvv.negate()
                                        premioLordo = premioLordo.negate()
                                        premioImpo = premioImpo.negate()
                                        //provvDealer = provvDealer.negate()
                                        //provvVendi = provvVendi.negate()
                                        provvGMF = provvGMF.negate()
                                    }
                                }
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza = TipoPolizza.RINNOVI
                                polizzaTotali.noPolizza = polizzaWS.noPolizza
                                polizzaTotali.programma = programma
                                polizzaTotali.assicurato = polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore = venditore
                                polizzaTotali.concessionario = dealer
                                polizzaTotali.dataPagamento = datapagamento
                                polizzaTotali.dataCopertura = dataInizio
                                polizzaTotali.dataImmisione = dataInizio
                                polizzaTotali.dataScadenza = dataFine
                                polizzaTotali.valoreAssicurato = valoreAssicurato
                                polizzaTotali.cod_Conc = codDealer
                                polizzaTotali.cod_vend = ""
                                polizzaTotali.targa = polizzaWS.targa
                                polizzaTotali.telaio = polizzaWS.telaio
                                polizzaTotali.pacchetto = pacchetto
                                polizzaTotali.satellitare = satellitare
                                polizzaTotali.livello = polizzaWS.anni
                                polizzaTotali.zonaTariffaria = zonaTariffaria
                                polizzaTotali.tariffa = tariffa
                                polizzaTotali.rinnovo = rinnovo
                                polizzaTotali.nuovo = nuovo
                                polizzaTotali.durata = polizzaWS.durata
                                polizzaTotali.premioLordo = premioLordo
                                polizzaTotali.premioImpo = premioImpo
                                polizzaTotali.provvVendi = provvVendi
                                polizzaTotali.provvDealer = provvDealer
                                polizzaTotali.provvGmfi = provvGMF
                                polizzaTotali.provvAddFee = ""
                                polizzaTotali.provvAgg = ""
                                polizzaTotali.account = account
                                polizzaTotali.finanziata = finanziata
                                polizzaTotali.rimborso = rimborso
                                polizzaTotali.noteRimb = ""
                                polizzaTotali.totProvv = totProvv
                                polizzaTotali.ritenuta = ritenuta
                                polizzaTotali.netPayable = netPayable

                                row {
                                    cell(polizzaWS.noPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataInizio)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizzaWS.targa)
                                    cell(polizzaWS.telaio)
                                    cell(pacchetto)
                                    cell(satellitare)
                                    cell(result[0])
                                    cell(zonaTariffaria)
                                    cell(tariffa)
                                    cell(rinnovo)
                                    cell(nuovo)
                                    cell(polizzaWS.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(account)
                                    cell(finanziata)
                                    cell(rimborso)
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                }
                                for (int i = 0; i < 36; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali = [:]
                            }

                        }
                    }

                }
                sheet("Retail - Polizze") {
                    row(style: "header") {
                        cell("Tipo Polizza")
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                        cell("CODICE CAMPAGNA")
                        cell("PREMIO LORDO CAMPAGNA")
                        cell("PREMIO NETTO CAMPAGNA")
                        cell("PREMIO LORDO CLIENTE")
                        cell("PREMIO NETTO CLIENTE")
                    }
                    if (allPolizze) {
                        def rPolizze = allPolizze.polizze
                        rPolizze.each { polizzaWS ->
                            row {
                                cell(polizzaWS.tipoPolizza)
                                cell(polizzaWS.noPolizza)
                                cell(polizzaWS.programma)
                                cell(polizzaWS.assicurato)
                                cell(polizzaWS.venditore)
                                cell(polizzaWS.concessionario)
                                cell(polizzaWS.datapagamento)
                                cell(polizzaWS.dataCopertura)
                                cell(polizzaWS.dataCopertura)
                                cell(polizzaWS.dataScadenza)
                                cell(polizzaWS.valoreAssicurato)
                                cell(polizzaWS.cod_Conc)
                                cell(polizzaWS.cod_Vend)
                                cell(polizzaWS.targa)
                                cell(polizzaWS.telaio)
                                cell(polizzaWS.pacchetto)
                                cell(polizzaWS.satellitare)
                                cell(polizzaWS.livello)
                                cell(polizzaWS.zonaTariffaria)
                                cell(polizzaWS.tariffa)
                                cell(polizzaWS.rinnovo)
                                cell(polizzaWS.nuovo)
                                cell(polizzaWS.durata)
                                cell(polizzaWS.premioLordo)
                                cell(polizzaWS.premioImpo)
                                cell(polizzaWS.provvVendi)
                                cell(polizzaWS.provvDealer)
                                cell(polizzaWS.provvGmfi)
                                cell(polizzaWS.provvAddFee)
                                cell(polizzaWS.provvAgg)
                                cell(polizzaWS.account)
                                cell(polizzaWS.finanziata)
                                cell(polizzaWS.rimborso)
                                cell(polizzaWS.noteRimb)
                                cell(polizzaWS.totProvv)
                                cell(polizzaWS.ritenuta)
                                cell(polizzaWS.netPayable)
                                cell(polizzaWS.codCampagna)
                                cell(polizzaWS.premioLordoCampagna)
                                cell(polizzaWS.premioNettoCampagna)
                                cell(polizzaWS.premioLordoCliente)
                                cell(polizzaWS.premioNettoCliente)
                            }
                            for (int i = 0; i < 36; i++) {
                                sheet.autoSizeColumn(i)
                            }
                        }
                    }

                }
            }
        }
        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()

        return stream.toByteArray()
    }
    def fileGMFGEN(def polizzeWS, def mese) {
        def rispostaCert=false
        def streamGMFGEN = new ByteArrayOutputStream()
        def zipGMFGEN = new ZipOutputStream(streamGMFGEN)
        def logg
        logg = new Log(parametri: "comincio a generare il file con i dati", operazione: "generazione file GMFGEN", pagina: "LISTA POLIZZE CASH")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def db = rinnDb
        def fileName="GMFEN_${new Date().format("ddMMyyyy")}.xlsx"
        def  fmt_IT = NumberFormat.getNumberInstance(Locale.ITALIAN)
        fmt_IT.setMaximumFractionDigits(2)
        def programma, venditore, dealer, datapagamento, codDealer, pacchetto, satellitare, zonaTariffaria, tariffa, rinnovo, nuovo, finanziata, provvVendi, provvDealer, provvGMF, netPayable, rimborso, account, totProvv, ritenuta, coperturaR,codCampagna,premioLordoCampagna,premioNettoCampagna,premioLordoCliente,premioNettoCliente
        def polizzeCash = [polizze: []]
        def polizzeLeasing = [polizze: []]
        def polizzeFinanziate = [polizze: []]
        def polizzeRinn = [polizze: []]
        def allPolizze = [polizze: []]
        def polizzaTotali = [:]
        polizzeWS.each { polizzaWS ->
            if (polizzaWS.statodslip != "V") {
                /*Set<String> ramiCash = ["U/12", "U/13", "U/14", "U/15", "U/16", "U/17", "U/18", "U/19", "U/20", "U/21", "U/22", "U/23", "U/24", "U/25", "U/26",
                                        "W/46", "W/47", "W/48", "W/49", "W/50", "W/51", "W/52", "W/53", "W/54", "W/55", "W/56", "W/57", "W/58", "W/59", "W/60"]
                Set<String> ramiLeasing = ["U/29","U/30","U/31","U/32","U/33","U/34","U/35","U/36","U/37","U/38","U/39","U/40","U/41","U/42","U/43",
                                           "W/61","W/62","W/63","W/64","W/65","W/66","W/67","W/68","W/69","W/70","W/71","W/72","W/73","W/74","W/75"]
                Set<String> ramiFinanziate = ["U/44", "U/45", "U/46", "U/47", "U/48", "U/49", "U/50", "U/51", "U/52", "U/53", "U/54", "U/55", "U/56", "U/57",
                                              "U/58", "W/76", "W/77", "W/78", "W/79", "W/80", "W/81", "W/82", "W/83", "W/84", "W/85", "W/86", "W/87", "W/88", "W/89", "W/90", "W/91"]
                Set<String> ramRinn = ["U/59", "U/60", "U/61", "U/62", "U/63", "U/64", "U/65", "U/66", "U/67", "U/68",
                                       "W/92", "W/94", "W/95", "W/96", "W/97", "W/98", "W/99",
                                       "D/01", "D/02", "D/03", "D/04", "D/05", "D/06", "D/07", "D/08", "D/09", "D/10", "D/11", "D/12",
                                       "D/21", "D/22", "D/23", "D/24", "D/25", "D/26", "D/27", "D/28", "D/29", "D/30"]*/
                // pippo (A/23)
               // def match = polizzaWS.ramo =~ /\(([^)]+)\)/
                    //def ramo = match[0][1]
                    //println "ramo qui"
                /*if(ramiCash.contains(ramo))
                        polizzeCash.polizze << polizzaWS
                    if(ramiLeasing.contains(ramo))
                        polizzeLeasing.polizze << polizzaWS
                    if(ramiFinanziate.contains(ramo))
                        polizzeFinanziate.polizze << polizzaWS
                    if(ramRinn.contains(ramo))
                        polizzeRinn.polizze << polizzaWS */
                if (polizzaWS.ramo.contains("U/12") || polizzaWS.ramo.contains("U/13") || polizzaWS.ramo.contains("U/14") || polizzaWS.ramo.contains("U/15") ||
                        polizzaWS.ramo.contains("U/16") || polizzaWS.ramo.contains("U/17") || polizzaWS.ramo.contains("U/18") || polizzaWS.ramo.contains("U/19") ||
                        polizzaWS.ramo.contains("U/20") || polizzaWS.ramo.contains("U/21") || polizzaWS.ramo.contains("U/22") || polizzaWS.ramo.contains("U/23") ||
                        polizzaWS.ramo.contains("U/24") || polizzaWS.ramo.contains("U/25") || polizzaWS.ramo.contains("U/26") || polizzaWS.ramo.contains("W/46")||
                        polizzaWS.ramo.contains("W/47")|| polizzaWS.ramo.contains("W/48")|| polizzaWS.ramo.contains("W/49")|| polizzaWS.ramo.contains("W/50")||
                        polizzaWS.ramo.contains("W/51")|| polizzaWS.ramo.contains("W/52")|| polizzaWS.ramo.contains("W/53")|| polizzaWS.ramo.contains("W/54")||
                        polizzaWS.ramo.contains("W/55")|| polizzaWS.ramo.contains("W/56")|| polizzaWS.ramo.contains("W/57")|| polizzaWS.ramo.contains("W/58")||
                        polizzaWS.ramo.contains("W/59")|| polizzaWS.ramo.contains("W/60")) {
                    polizzeCash.polizze << polizzaWS
                } else if (polizzaWS.ramo.contains("U/29") || polizzaWS.ramo.contains("U/30") || polizzaWS.ramo.contains("U/31") || polizzaWS.ramo.contains("U/32")
                        || polizzaWS.ramo.contains("U/33") || polizzaWS.ramo.contains("U/34") || polizzaWS.ramo.contains("U/35") || polizzaWS.ramo.contains("U/36")
                        || polizzaWS.ramo.contains("U/37") || polizzaWS.ramo.contains("U/38") || polizzaWS.ramo.contains("U/39") || polizzaWS.ramo.contains("U/40")
                        || polizzaWS.ramo.contains("U/41") || polizzaWS.ramo.contains("U/42") || polizzaWS.ramo.contains("U/43") || polizzaWS.ramo.contains("W/61")
                        || polizzaWS.ramo.contains("W/62")|| polizzaWS.ramo.contains("W/63")|| polizzaWS.ramo.contains("W/64") || polizzaWS.ramo.contains("W/65")
                        || polizzaWS.ramo.contains("W/66")|| polizzaWS.ramo.contains("W/67")|| polizzaWS.ramo.contains("W/68") || polizzaWS.ramo.contains("W/69")
                        || polizzaWS.ramo.contains("W/70")|| polizzaWS.ramo.contains("W/71") || polizzaWS.ramo.contains("W/72") || polizzaWS.ramo.contains("W/73")
                        || polizzaWS.ramo.contains("W/74")|| polizzaWS.ramo.contains("W/75")) {
                    polizzeLeasing.polizze << polizzaWS
                } else if (polizzaWS.ramo.contains("U/44") || polizzaWS.ramo.contains("U/45") || polizzaWS.ramo.contains("U/46") || polizzaWS.ramo.contains("U/47")
                        || polizzaWS.ramo.contains("U/48") || polizzaWS.ramo.contains("U/49") || polizzaWS.ramo.contains("U/50") || polizzaWS.ramo.contains("U/51")
                        || polizzaWS.ramo.contains("U/52") || polizzaWS.ramo.contains("U/53") || polizzaWS.ramo.contains("U/54") || polizzaWS.ramo.contains("U/55")
                        || polizzaWS.ramo.contains("U/56") || polizzaWS.ramo.contains("U/57") || polizzaWS.ramo.contains("U/58")|| polizzaWS.ramo.contains("W/76")
                        || polizzaWS.ramo.contains("W/77") || polizzaWS.ramo.contains("W/78") || polizzaWS.ramo.contains("W/79") || polizzaWS.ramo.contains("W/80")
                        || polizzaWS.ramo.contains("W/81") || polizzaWS.ramo.contains("W/82") || polizzaWS.ramo.contains("W/83") || polizzaWS.ramo.contains("W/84")
                        || polizzaWS.ramo.contains("W/85") || polizzaWS.ramo.contains("W/86")|| polizzaWS.ramo.contains("W/87")|| polizzaWS.ramo.contains("W/88")
                        || polizzaWS.ramo.contains("W/89") || polizzaWS.ramo.contains("W/90") || polizzaWS.ramo.contains("W/91") || polizzaWS.ramo.contains("D/46")) {
                    polizzeFinanziate.polizze << polizzaWS
                } else if (polizzaWS.ramo.contains("U/59") || polizzaWS.ramo.contains("U/60") || polizzaWS.ramo.contains("U/61") || polizzaWS.ramo.contains("U/62")
                        || polizzaWS.ramo.contains("U/63") || polizzaWS.ramo.contains("U/64") || polizzaWS.ramo.contains("U/65") || polizzaWS.ramo.contains("U/66")
                        || polizzaWS.ramo.contains("U/67") || polizzaWS.ramo.contains("U/68") || polizzaWS.ramo.contains("W/92") || polizzaWS.ramo.contains("W/94")
                        || polizzaWS.ramo.contains("W/95") || polizzaWS.ramo.contains("W/96") || polizzaWS.ramo.contains("W/97") || polizzaWS.ramo.contains("W/98")
                        || polizzaWS.ramo.contains("W/99") || polizzaWS.ramo.contains("D/01") || polizzaWS.ramo.contains("D/02") || polizzaWS.ramo.contains("D/03")
                        || polizzaWS.ramo.contains("D/04") || polizzaWS.ramo.contains("D/05") || polizzaWS.ramo.contains("D/06") || polizzaWS.ramo.contains("D/07")
                        || polizzaWS.ramo.contains("D/08") || polizzaWS.ramo.contains("D/09") || polizzaWS.ramo.contains("D/10") || polizzaWS.ramo.contains("D/11")
                        || polizzaWS.ramo.contains("D/12") || polizzaWS.ramo.contains("D/21") || polizzaWS.ramo.contains("D/22") || polizzaWS.ramo.contains("D/23")
                        || polizzaWS.ramo.contains("D/24") || polizzaWS.ramo.contains("D/25") || polizzaWS.ramo.contains("D/26") || polizzaWS.ramo.contains("D/27")
                        || polizzaWS.ramo.contains("D/28") || polizzaWS.ramo.contains("D/29") || polizzaWS.ramo.contains("D/30") ) {
                    polizzeRinn.polizze << polizzaWS
                }
            }
        }
        def polizzeF = polizzeFinanziate.polizze
        def polizzeL = polizzeLeasing.polizze
        def polizzeC = polizzeCash.polizze
        def polizzeRN = polizzeRinn.polizze
        boolean isRinnovo = false
        def polizzaRinn

        def wb = Stopwatch.log {
            ExcelBuilder.create {
                style("header") {
                    background bisque
                    font {
                        bold(true)
                    }
                }
                sheet("Retail - Finanziate") {
                    row(style: "header") {
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                        cell("CODICE CAMPAGNA")
                        cell("PREMIO LORDO CAMPAGNA")
                        cell("PREMIO NETTO CAMPAGNA")
                        cell("PREMIO LORDO CLIENTE")
                        cell("PREMIO NETTO CLIENTE")

                    }
                    if (polizzeF) {
                        polizzeF.each { polizzaWS ->
                            //per le polizze leasing / finanziate
                            if (polizzaWS.ramo.contains("U/50") || polizzaWS.ramo.contains("U/51") || polizzaWS.ramo.contains("U/52") || polizzaWS.ramo.contains("W/83") || polizzaWS.ramo.contains("W/84")|| polizzaWS.ramo.contains("W/85")) {
                                pacchetto = "PLATINUM"
                                coperturaR = "PLATINUM"
                            } else if (polizzaWS.ramo.contains("U/44") || polizzaWS.ramo.contains("U/45") || polizzaWS.ramo.contains("U/46") || polizzaWS.ramo.contains("W/76") || polizzaWS.ramo.contains("W/77") || polizzaWS.ramo.contains("W/79") ) {
                                pacchetto = "SILVER"
                                coperturaR = "SILVER"
                            } else if (polizzaWS.ramo.contains("U/47") || polizzaWS.ramo.contains("U/48") || polizzaWS.ramo.contains("U/49")  || polizzaWS.ramo.contains("W/80") || polizzaWS.ramo.contains("W/81") || polizzaWS.ramo.contains("W/82")) {
                                pacchetto = "GOLD"
                                coperturaR = "GOLD"
                            } else if (polizzaWS.ramo.contains("U/53") || polizzaWS.ramo.contains("U/54") || polizzaWS.ramo.contains("U/55") || polizzaWS.ramo.contains("W/86") || polizzaWS.ramo.contains("W/87")|| polizzaWS.ramo.contains("W/88")) {
                                pacchetto = "PLATINUM+COLLISIONE"
                                coperturaR = "PLATINUM COLLISIONE"
                            } else if (polizzaWS.ramo.contains("U/56") || polizzaWS.ramo.contains("U/57") || polizzaWS.ramo.contains("U/58")|| polizzaWS.ramo.contains("W/89")|| polizzaWS.ramo.contains("W/90") || polizzaWS.ramo.contains("W/91")) {
                                pacchetto = "PLATINUM+KASKO"
                                coperturaR = "PLATINUM KASKO"
                            }
                            def noPolizza = polizzaWS.noPolizza
                            if (polizzaWS.onstar.toString().trim().contains("SATELITTARE")) {
                                satellitare = "S"
                            } else {
                                satellitare = "N"
                            }
                            rimborso = "N"
                            def premioLordo, premioImpo, valoreAssicurato
                            def polizza = Polizza.findByNoPolizza(noPolizza.toString().trim())
                            def dataInizio = polizzaWS.inizio
                            if (dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            } else {
                                dataInizio = ""
                            }
                            def dataFine = polizzaWS.fine
                            if (dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            } else {
                                dataFine = ""
                            }
                            if (polizza) {
                                if(polizza.tariffa!=1){
                                    programma = "Flex Protection"
                                }else{
                                    programma = "Flex Protection 2017"
                                }
                                venditore = polizza.venditore.toUpperCase()
                                dealer = polizza.dealer.ragioneSocialeD.toUpperCase()
                                codDealer = polizza.dealer.codice.toString().trim().padLeft(4, "0")
                                zonaTariffaria = polizza.codiceZonaTerritoriale
                                tariffa = "${polizza.dealer.dealerType} - ${polizza.step}"
                                rinnovo = "0"
                                nuovo = "NUOVI"
                                premioLordo = polizza.premioLordo
                                premioImpo = polizza.premioImponibile
                                valoreAssicurato = polizza.valoreAssicurato
                                provvVendi = polizza.provvVenditore
                                provvDealer = polizza.provvDealer
                                provvGMF = polizza.provvGmfi
                                account = polizza.dealer.salesSpecialist.toUpperCase()
                                def nomeSales = account.split(" ")
                                def salesSpec
                                if (nomeSales.size() == 2) {
                                    salesSpec = nomeSales[1] + " " + nomeSales[0]
                                } else if (nomeSales.size() > 2) {
                                    for (int ind = 0; ind < nomeSales.size(); ind++) {
                                        salesSpec += nomeSales[ind] + " "
                                    }
                                }
                                datapagamento = polizzaWS.datacompagnia
                                if (datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                } else {
                                    datapagamento = ""
                                }
                                finanziata = "S"
                                totProvv = provvDealer + provvVendi + provvGMF
                                ritenuta = 0.046 * (provvDealer + provvVendi + provvGMF)
                                netPayable = premioLordo - (provvDealer + provvVendi + provvGMF)
                                codCampagna=polizza.codCampagna
                                premioLordoCampagna=polizza.premioLordoTerzi
                                premioNettoCampagna=polizza.premioTerzi
                                premioLordoCliente=polizza.premioLordoCliente
                                premioNettoCliente=polizza.premioCliente
                                if (polizzaWS.statodslip != "0" && polizzaWS.statodslip != "V" ) {
                                    rimborso = "Y"
                                    isRinnovo = false
                                    polizzaRinn = ""
                                    netPayable = netPayable.negate()
                                    ritenuta = ritenuta.negate()
                                    totProvv = totProvv.negate()
                                    premioImpo = premioImpo.negate()
                                    premioLordo = premioLordo.negate()
                                    provvDealer = provvDealer.negate()
                                    provvVendi = provvVendi.negate()
                                    provvGMF = provvGMF.negate()

                                    /*if(polizzaWS.statodslip=="E"){
                                        def codOperazione = "E"
                                        def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                        println "premio lordo ${premioLordo}"
                                        println "provvDealer ${provvDealer}"
                                        println "provvVendi ${provvVendi}"
                                        println "provvGMF ${provvGMF}"
                                    }else if(polizzaWS.statodslip=="F"){
                                        def codOperazione = "F"
                                        def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                        premioLordo = rispostawe.premioLordo
                                        premioImpo = rispostawe.premioImpo
                                        provvDealer = rispostawe.provvDealer
                                        provvVendi = rispostawe.provvVendi
                                        provvGMF = rispostawe.provvGMF
                                    }else if(polizzaWS.statodslip=="G"){
                                        def codOperazione = "G"
                                         def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                         premioLordo = rispostawe.premioLordo
                                         premioImpo = rispostawe.premioImpo
                                         provvDealer = rispostawe.provvDealer
                                         provvVendi = rispostawe.provvVendi
                                         provvGMF = rispostawe.provvGMF
                                    }
                                    else
                                    if(polizzaWS.statodslip=="S") {
                                        println "premio lordo ${premioLordo}"
                                        println "provvDealer ${provvDealer}"
                                        println "provvVendi ${provvVendi}"
                                        println "provvGMF ${provvGMF}"
                                        netPayable = netPayable.negate()
                                        ritenuta = ritenuta.negate()
                                        totProvv = totProvv.negate()
                                        premioImpo = premioImpo.negate()
                                        premioLordo = premioLordo.negate()
                                        provvDealer = provvDealer.negate()
                                        provvVendi = provvVendi.negate()
                                        provvGMF = provvGMF.negate()
                                    }*/
                                }
                                def numPolizza = ""
                                numPolizza = polizza.noPolizza
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza = TipoPolizza.FINANZIATE
                                polizzaTotali.noPolizza = numPolizza
                                polizzaTotali.programma = programma
                                polizzaTotali.assicurato = polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore = venditore
                                polizzaTotali.concessionario = dealer
                                polizzaTotali.dataPagamento = datapagamento
                                polizzaTotali.dataCopertura = dataInizio
                                polizzaTotali.dataImmisione = dataInizio
                                polizzaTotali.dataScadenza = dataFine
                                polizzaTotali.valoreAssicurato = valoreAssicurato
                                polizzaTotali.cod_Conc = codDealer
                                polizzaTotali.cod_vend = ""
                                polizzaTotali.targa = polizzaWS.targa
                                polizzaTotali.telaio = polizzaWS.telaio
                                polizzaTotali.pacchetto = pacchetto
                                polizzaTotali.satellitare = satellitare
                                polizzaTotali.livello = polizzaWS.anni
                                polizzaTotali.zonaTariffaria = zonaTariffaria
                                polizzaTotali.tariffa = tariffa
                                polizzaTotali.rinnovo = rinnovo
                                polizzaTotali.nuovo = nuovo
                                polizzaTotali.durata = polizzaWS.durata
                                polizzaTotali.premioLordo = premioLordo
                                polizzaTotali.premioImpo = premioImpo
                                polizzaTotali.provvVendi = provvVendi
                                polizzaTotali.provvDealer = provvDealer
                                polizzaTotali.provvGmfi = provvGMF
                                polizzaTotali.provvAddFee = ""
                                polizzaTotali.provvAgg = ""
                                polizzaTotali.account = salesSpec
                                polizzaTotali.finanziata = finanziata
                                polizzaTotali.rimborso = rimborso
                                polizzaTotali.noteRimb = ""
                                polizzaTotali.totProvv = totProvv
                                polizzaTotali.ritenuta = ritenuta
                                polizzaTotali.netPayable = netPayable
                                polizzaTotali.codCampagna = codCampagna
                                polizzaTotali.premioLordoCampagna = premioLordoCampagna
                                polizzaTotali.premioNettoCampagna = premioNettoCampagna
                                polizzaTotali.premioLordoCliente = premioLordoCliente
                                polizzaTotali.premioNettoCliente = premioNettoCliente

                                row {
                                    cell(numPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataInizio)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizzaWS.targa)
                                    cell(polizzaWS.telaio)
                                    cell(pacchetto)
                                    cell(satellitare)
                                    cell(polizzaWS.anni)
                                    cell(zonaTariffaria)
                                    cell(tariffa)
                                    cell(rinnovo)
                                    cell(nuovo)
                                    cell(polizzaWS.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(salesSpec)
                                    cell(finanziata)
                                    cell(rimborso)
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                    cell(codCampagna)
                                    cell(premioLordoCampagna)
                                    cell(premioNettoCampagna)
                                    cell(premioLordoCliente)
                                    cell(premioNettoCliente)
                                }
                                for (int i = 0; i < 36; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali = [:]
                            }
                        }
                    } else {
                        logg = new Log(parametri: "non ci sono polizze finanziate ", operazione: "generaFileGMFGEN", pagina: "LISTA POLIZZE CASH")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }
                sheet("Retail - Leasing") {
                    row(style: "header") {
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                        cell("CODICE CAMPAGNA")
                        cell("PREMIO LORDO CAMPAGNA")
                        cell("PREMIO NETTO CAMPAGNA")
                        cell("PREMIO LORDO CLIENTE")
                        cell("PREMIO NETTO CLIENTE")
                    }
                    if (polizzeL) {
                        polizzeL.each { polizzaWS ->
                            //per le polizze leasing / finanziate
                            if (polizzaWS.ramo.contains("U/35") || polizzaWS.ramo.contains("U/36") || polizzaWS.ramo.contains("U/37") || polizzaWS.ramo.contains("W/67") || polizzaWS.ramo.contains("W/68") || polizzaWS.ramo.contains("W/69")) {
                                pacchetto = "PLATINUM"
                            } else if (polizzaWS.ramo.contains("U/29") || polizzaWS.ramo.contains("U/30") || polizzaWS.ramo.contains("U/31") || polizzaWS.ramo.contains("W/61") || polizzaWS.ramo.contains("W/62") || polizzaWS.ramo.contains("W/63")) {
                                pacchetto = "SILVER"
                            } else if (polizzaWS.ramo.contains("U/32") || polizzaWS.ramo.contains("U/33") || polizzaWS.ramo.contains("U/34") || polizzaWS.ramo.contains("W/64") || polizzaWS.ramo.contains("W/65")|| polizzaWS.ramo.contains("W/66")) {
                                pacchetto = "GOLD"
                            } else if (polizzaWS.ramo.contains("U/38") || polizzaWS.ramo.contains("U/39") || polizzaWS.ramo.contains("U/40")|| polizzaWS.ramo.contains("W/70") || polizzaWS.ramo.contains("W/71")|| polizzaWS.ramo.contains("W/72")) {
                                pacchetto = "PLATINUM+COLLISIONE"
                            } else if (polizzaWS.ramo.contains("U/41") || polizzaWS.ramo.contains("U/42") || polizzaWS.ramo.contains("U/43") || polizzaWS.ramo.contains("W/73") || polizzaWS.ramo.contains("W/74") || polizzaWS.ramo.contains("W/75")) {
                                pacchetto = "PLATINUM+KASKO"
                            }
                            def noPolizza = polizzaWS.noPolizza
                            //println polizzaWS.codiceDSLIP
                            if (polizzaWS.onstar.toString().trim().contains("SATELITTARE")) {
                                satellitare = "S"
                            } else {
                                satellitare = "N"
                            }
                            rimborso = "N"
                            def premioLordo, premioImpo, valoreAssicurato

                            def polizza = Polizza.findByNoPolizza(noPolizza.toString().trim())
                            def dataInizio = polizzaWS.inizio
                            if (dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            } else {
                                dataInizio = ""
                            }
                            def dataFine = polizzaWS.fine
                            if (dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            } else {
                                dataFine = ""
                            }
                            if (polizza) {
                                if(polizza.tariffa!=1){
                                    programma = "Flex Protection"
                                }else{
                                    programma = "Flex Protection 2017"
                                }
                                venditore = polizza.venditore.toUpperCase()
                                dealer = polizza.dealer.ragioneSocialeD.toUpperCase()
                                codDealer = polizza.dealer.codice.toString().trim().padLeft(4, "0")
                                zonaTariffaria = polizza.codiceZonaTerritoriale
                                tariffa = "${polizza.dealer.dealerType} - ${polizza.step}"
                                rinnovo = "0"
                                nuovo = "NUOVI"
                                premioLordo = polizza.premioLordo
                                premioImpo = polizza.premioImponibile
                                valoreAssicurato = polizza.valoreAssicurato
                                provvVendi = polizza.provvVenditore
                                provvDealer = polizza.provvDealer
                                provvGMF = polizza.provvGmfi
                                account = polizza.dealer.salesSpecialist.toUpperCase()
                                def nomeSales = account.split(" ")
                                def salesSpec
                                //println nomeSales
                                if (nomeSales.size() == 2) {
                                    salesSpec = nomeSales[1] + " " + nomeSales[0]
                                } else if (nomeSales.size() > 2) {
                                    for (int ind = 0; ind < nomeSales.size(); ind++) {
                                        salesSpec += nomeSales[ind] + " "
                                    }
                                }
                                datapagamento = polizzaWS.datacompagnia
                                if (datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                } else {
                                    datapagamento = ""
                                }
                                finanziata = "S"
                                totProvv = provvDealer + provvVendi + provvGMF
                                ritenuta = 0.046 * (provvDealer + provvVendi + provvGMF)
                                netPayable = premioLordo - (provvDealer + provvVendi + provvGMF)
                                codCampagna=polizza.codCampagna
                                premioLordoCampagna=polizza.premioLordoTerzi
                                premioNettoCampagna=polizza.premioTerzi
                                premioLordoCliente=polizza.premioLordoCliente
                                premioNettoCliente=polizza.premioCliente
                                if (polizzaWS.statodslip != "0" && polizzaWS.statodslip != "V") {
                                    isRinnovo = false
                                    polizzaRinn = ""
                                    rimborso = "Y"
                                    ritenuta = ritenuta.negate()
                                    totProvv = totProvv.negate()
                                    premioLordo = premioLordo.negate()
                                    premioImpo = premioImpo.negate()
                                    provvDealer = provvDealer.negate()
                                    provvVendi = provvVendi.negate()
                                    provvGMF = provvGMF.negate()
                                    /*if (polizzaWS.statodslip != "S") {
                                        if(polizza.codOperazione=="E"){
                                            def codOperazione = "E"
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF

                                        }else if(polizza.codOperazione=="F"){
                                            def codOperazione = "F"
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF

                                        }else if(polizza.codOperazione=="G"){
                                            def codOperazione = "G"
                                            /*def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF

                                        }

                                    } else {

                                    }*/

                                }
                                def numPolizza = polizza.noPolizza
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza = TipoPolizza.LEASING
                                polizzaTotali.noPolizza = numPolizza
                                polizzaTotali.programma = programma
                                polizzaTotali.assicurato = polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore = venditore
                                polizzaTotali.concessionario = dealer
                                polizzaTotali.dataPagamento = datapagamento
                                polizzaTotali.dataCopertura = dataInizio
                                polizzaTotali.dataImmisione = dataInizio
                                polizzaTotali.dataScadenza = dataFine
                                polizzaTotali.valoreAssicurato = valoreAssicurato
                                polizzaTotali.cod_Conc = codDealer
                                polizzaTotali.cod_vend = ""
                                polizzaTotali.targa = polizzaWS.targa
                                polizzaTotali.telaio = polizzaWS.telaio
                                polizzaTotali.pacchetto = pacchetto
                                polizzaTotali.satellitare = satellitare
                                polizzaTotali.livello = polizzaWS.anni
                                polizzaTotali.zonaTariffaria = zonaTariffaria
                                polizzaTotali.tariffa = tariffa
                                polizzaTotali.rinnovo = rinnovo
                                polizzaTotali.nuovo = nuovo
                                polizzaTotali.durata = polizzaWS.durata
                                polizzaTotali.premioLordo = premioLordo
                                polizzaTotali.premioImpo = premioImpo
                                polizzaTotali.provvVendi = provvVendi
                                polizzaTotali.provvDealer = provvDealer
                                polizzaTotali.provvGmfi = provvGMF
                                polizzaTotali.provvAddFee = ""
                                polizzaTotali.provvAgg = ""
                                polizzaTotali.account = salesSpec
                                polizzaTotali.finanziata = finanziata
                                polizzaTotali.rimborso = rimborso
                                polizzaTotali.noteRimb = ""
                                polizzaTotali.totProvv = totProvv
                                polizzaTotali.ritenuta = ritenuta
                                polizzaTotali.netPayable = netPayable
                                polizzaTotali.codCampagna = codCampagna
                                polizzaTotali.premioLordoCampagna = premioLordoCampagna
                                polizzaTotali.premioNettoCampagna = premioNettoCampagna
                                polizzaTotali.premioLordoCliente = premioLordoCliente
                                polizzaTotali.premioNettoCliente = premioNettoCliente
                                row {
                                    cell(numPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataInizio)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizzaWS.targa)
                                    cell(polizzaWS.telaio)
                                    cell(pacchetto)
                                    cell(satellitare)
                                    cell(polizzaWS.anni)
                                    cell(zonaTariffaria)
                                    cell(tariffa)
                                    cell(rinnovo)
                                    cell(nuovo)
                                    cell(polizzaWS.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(salesSpec)
                                    cell(finanziata)
                                    cell(rimborso)
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                    cell(codCampagna)
                                    cell(premioLordoCampagna)
                                    cell(premioNettoCampagna)
                                    cell(premioLordoCliente)
                                    cell(premioNettoCliente)
                                }
                                for (int i = 0; i < 36; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali = [:]
                            }
                        }
                    } else {
                        logg = new Log(parametri: "non ci sono polizze leasing ", operazione: "generaFileGMFGEN", pagina: "LISTA POLIZZE CASH")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }
                sheet("Retail - Cash") {
                    row(style: "header") {
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                    }
                    if (polizzeC) {
                        polizzeC.each { polizzaWS ->
                            //per le polizze leasing / cash / finanziate
                            def noPolizza = polizzaWS.noPolizza
                            //println polizzaWS.codiceDSLIP
                            if (polizzaWS.ramo.contains("U/18") || polizzaWS.ramo.contains("U/19") || polizzaWS.ramo.contains("U/20")|| polizzaWS.ramo.contains("W/52")|| polizzaWS.ramo.contains("W/53")|| polizzaWS.ramo.contains("W/54")) {
                                pacchetto = "PLATINUM"
                            } else if (polizzaWS.ramo.contains("U/12") || polizzaWS.ramo.contains("U/13") || polizzaWS.ramo.contains("U/14") || polizzaWS.ramo.contains("W/46")|| polizzaWS.ramo.contains("W/47")|| polizzaWS.ramo.contains("W/48")) {
                                pacchetto = "SILVER"
                            } else if (polizzaWS.ramo.contains("U/15") || polizzaWS.ramo.contains("U/16") || polizzaWS.ramo.contains("U/17")|| polizzaWS.ramo.contains("W/49") || polizzaWS.ramo.contains("W/50")|| polizzaWS.ramo.contains("W/51")) {
                                pacchetto = "GOLD"
                            } else if (polizzaWS.ramo.contains("U/21") || polizzaWS.ramo.contains("U/22") || polizzaWS.ramo.contains("U/23")|| polizzaWS.ramo.contains("W/55")|| polizzaWS.ramo.contains("W/56")|| polizzaWS.ramo.contains("W/57")) {
                                pacchetto = "PLATINUM+COLLISIONE"
                            } else if (polizzaWS.ramo.contains("U/24") || polizzaWS.ramo.contains("U/25") || polizzaWS.ramo.contains("U/26")|| polizzaWS.ramo.contains("W/58")|| polizzaWS.ramo.contains("W/59")|| polizzaWS.ramo.contains("W/60")) {
                                pacchetto = "PLATINUM+KASKO"
                            }
                            if (polizzaWS.onstar.toString().trim().contains("SATELITTARE")) {
                                satellitare = "S"
                            } else {
                                satellitare = "N"
                            }
                            rimborso = "N"
                            def premioLordo, premioImpo, valoreAssicurato
                            def polizza = Polizza.findByNoPolizza(noPolizza.toString().trim())
                            def dataInizio = polizzaWS.inizio
                            if (dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            } else {
                                dataInizio = ""
                            }
                            def dataFine = polizzaWS.fine
                            if (dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            } else {
                                dataFine = ""
                            }
                            if (polizza) {
                                if(polizza.tariffa!=1){
                                    programma = "Flex Protection"
                                }else{
                                    programma = "Flex Protection 2017"
                                }
                                venditore = polizza.venditore.toUpperCase()
                                dealer = polizza.dealer.ragioneSocialeD.toUpperCase()
                                codDealer = polizza.dealer.codice.toString().trim().padLeft(4, "0")
                                zonaTariffaria = polizza.codiceZonaTerritoriale
                                tariffa = "${polizza.dealer.dealerType} - ${polizza.step}"
                                rinnovo = "0"
                                nuovo = "NUOVI"
                                premioLordo = polizza.premioLordo
                                premioImpo = polizza.premioImponibile
                                valoreAssicurato = polizza.valoreAssicurato
                                provvVendi = polizza.provvVenditore
                                provvDealer = polizza.provvDealer
                                provvGMF = polizza.provvGmfi
                                account = polizza.dealer.salesSpecialist.toUpperCase()
                                def nomeSales = account.split(" ")
                                def salesSpec
                                //println nomeSales
                                if (nomeSales.size() == 2) {
                                    salesSpec = nomeSales[1] + " " + nomeSales[0]
                                } else if (nomeSales.size() > 2) {
                                    for (int ind = 0; ind < nomeSales.size(); ind++) {
                                        salesSpec += nomeSales[ind] + " "
                                    }
                                }
                                datapagamento = polizzaWS.datacomunicaz
                                if (datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                } else {
                                    datapagamento = ""
                                }
                                finanziata = "N"
                                totProvv = provvDealer + provvVendi + provvGMF
                                ritenuta = 0.046 * (provvDealer + provvVendi + provvGMF)
                                netPayable = premioLordo - (provvDealer + provvVendi + provvGMF)
                                if (polizzaWS.statodslip != "0" && polizzaWS.statodslip != "V") {
                                    isRinnovo = false
                                    polizzaRinn = ""
                                    rimborso = "Y"
                                    ritenuta = ritenuta.negate()
                                    totProvv = totProvv.negate()
                                    premioLordo = premioLordo.negate()
                                    premioImpo = premioImpo.negate()
                                    provvDealer = provvDealer.negate()
                                    provvVendi = provvVendi.negate()
                                    provvGMF = provvGMF.negate()
                                    /*if (polizzaWS.statodslip != "S") {
                                        if(polizza.codOperazione=="E"){
                                            def codOperazione = "E"
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                        }else if(polizza.codOperazione=="F"){
                                            def codOperazione = "F"
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF
                                        }else if(polizza.codOperazione=="G"){
                                            def codOperazione = "G"
                                            /*def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF
                                        }

                                    } else {

                                    }*/
                                }
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza = TipoPolizza.CASH
                                polizzaTotali.noPolizza = polizza.noPolizza
                                polizzaTotali.programma = programma
                                polizzaTotali.assicurato = polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore = venditore
                                polizzaTotali.concessionario = dealer
                                polizzaTotali.dataPagamento = datapagamento
                                polizzaTotali.dataCopertura = dataInizio
                                polizzaTotali.dataImmisione = dataInizio
                                polizzaTotali.dataScadenza = dataFine
                                polizzaTotali.valoreAssicurato = valoreAssicurato
                                polizzaTotali.cod_Conc = codDealer
                                polizzaTotali.cod_vend = ""
                                polizzaTotali.targa = polizzaWS.targa
                                polizzaTotali.telaio = polizzaWS.telaio
                                polizzaTotali.pacchetto = pacchetto
                                polizzaTotali.satellitare = satellitare
                                polizzaTotali.livello = polizzaWS.anni
                                polizzaTotali.zonaTariffaria = zonaTariffaria
                                polizzaTotali.tariffa = tariffa
                                polizzaTotali.rinnovo = rinnovo
                                polizzaTotali.nuovo = nuovo
                                polizzaTotali.durata = polizzaWS.durata
                                polizzaTotali.premioLordo = premioLordo
                                polizzaTotali.premioImpo = premioImpo
                                polizzaTotali.provvVendi = provvVendi
                                polizzaTotali.provvDealer = provvDealer
                                polizzaTotali.provvGmfi = provvGMF
                                polizzaTotali.provvAddFee = ""
                                polizzaTotali.provvAgg = ""
                                polizzaTotali.account = salesSpec
                                polizzaTotali.finanziata = finanziata
                                polizzaTotali.rimborso = rimborso
                                polizzaTotali.noteRimb = ""
                                polizzaTotali.totProvv = totProvv
                                polizzaTotali.ritenuta = ritenuta
                                polizzaTotali.netPayable = netPayable

                                row {
                                    cell(polizza.noPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataInizio)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizzaWS.targa)
                                    cell(polizzaWS.telaio)
                                    cell(pacchetto)
                                    cell(satellitare)
                                    cell(polizzaWS.anni)
                                    cell(zonaTariffaria)
                                    cell(tariffa)
                                    cell(rinnovo)
                                    cell(nuovo)
                                    cell(polizzaWS.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(salesSpec)
                                    cell(finanziata)
                                    cell(rimborso)
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                }
                                for (int i = 0; i < 36; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali = [:]
                            }

                        }
                    }
                }
                sheet("Retail - Rinnovi") {
                    row(style: "header") {
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                    }
                    if (polizzeRN) {
                        polizzeRN.each { polizzaWS ->
                            def noPolizza = polizzaWS.noPolizza
                            if (polizzaWS.ramo.contains("U/61") || polizzaWS.ramo.contains("U/66")|| polizzaWS.ramo.contains("W/94") || polizzaWS.ramo.contains("W/99")|| polizzaWS.ramo.contains("D/05") || polizzaWS.ramo.contains("D/10") || polizzaWS.ramo.contains("D/23") || polizzaWS.ramo.contains("D/28")) {
                                pacchetto = "PLATINUM"
                            } else if (polizzaWS.ramo.contains("U/59") || polizzaWS.ramo.contains("U/64")|| polizzaWS.ramo.contains("W/92") || polizzaWS.ramo.contains("W/97") || polizzaWS.ramo.contains("D/03") || polizzaWS.ramo.contains("D/08") || polizzaWS.ramo.contains("D/21") || polizzaWS.ramo.contains("D/26")) {
                                pacchetto = "SILVER"
                            } else if (polizzaWS.ramo.contains("U/60") || polizzaWS.ramo.contains("U/65")|| polizzaWS.ramo.contains("W/93") || polizzaWS.ramo.contains("W/98") || polizzaWS.ramo.contains("D/04") || polizzaWS.ramo.contains("D/09") || polizzaWS.ramo.contains("D/22") || polizzaWS.ramo.contains("D/27")) {
                                pacchetto = "GOLD"
                            } else if (polizzaWS.ramo.contains("U/62") || polizzaWS.ramo.contains("U/67") || polizzaWS.ramo.contains("W/95")  || polizzaWS.ramo.contains("D/01")  || polizzaWS.ramo.contains("D/06") || polizzaWS.ramo.contains("D/11") || polizzaWS.ramo.contains("D/24") || polizzaWS.ramo.contains("D/29")) {
                                pacchetto = "PLATINUM+COLLISIONE"
                            } else if (polizzaWS.ramo.contains("U/63") || polizzaWS.ramo.contains("U/68")|| polizzaWS.ramo.contains("W/96")|| polizzaWS.ramo.contains("D/02") || polizzaWS.ramo.contains("D/07") || polizzaWS.ramo.contains("D/12") || polizzaWS.ramo.contains("D/25") || polizzaWS.ramo.contains("D/30")) {
                                pacchetto = "PLATINUM+KASKO"
                            }
                            if (polizzaWS.onstar.toString().trim().contains("SATELITTARE")) {
                                satellitare = "S"
                            } else {
                                satellitare = "N"
                            }
                            rimborso = "N"
                            def premioLordo, premioImpo, valoreAssicurato
                            if (polizzaWS.premio) {
                                premioLordo = (BigDecimal) fmt_IT.parseObject(polizzaWS.premio.toString())
                            } else {
                                premioLordo = 0.0
                            }
                            if (polizzaWS.imponibile) {
                                premioImpo = (BigDecimal) fmt_IT.parseObject(polizzaWS.imponibile.toString())
                            } else {
                                premioImpo = 0.0
                            }
                            if (polizzaWS.valoreAssicurato) {
                                valoreAssicurato = polizzaWS.valoreAssicurato
                            } else {
                                valoreAssicurato = 0.0
                            }
                            def dataInizio = polizzaWS.inizio
                            if (dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            } else {
                                dataInizio = ""
                            }
                            def dataFine = polizzaWS.fine
                            if (dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            } else {
                                dataFine = ""
                            }
                            def regtarga=/^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$/
                            def targa=polizzaWS.targa.toString().trim()
                            def result=""
                            if (targa.toString().trim()!='' && targa.toString().trim()!=null){
                                if(!targa.matches("^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}")){
                                    logg = new Log(parametri: "la targa non corrisponde  ${targa}", operazione: "invio GMFGEN", pagina: "LISTA POLIZZE CASH")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    result = db.firstRow("select polizze.annualita, rinnovi.provincia, rinnovi.provv_gm, polizze.versione_tariffa from polizze join rinnovi on polizze.idpolizze=rinnovi.idpolizze where telaio='${polizzaWS.telaio}'  order by rinnovi.idpolizze desc")

                                }else{
                                    logg = new Log(parametri: "la targa  corrisponde  ${targa}", operazione: "invio GMFGEN", pagina: "LISTA POLIZZE CASH")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    result = db.firstRow("select polizze.annualita, rinnovi.provincia, rinnovi.provv_gm, polizze.versione_tariffa from polizze join rinnovi on polizze.idpolizze=rinnovi.idpolizze where targa='${polizzaWS.targa}' and telaio='${polizzaWS.telaio}'  order by rinnovi.idpolizze desc")

                                }
                            }else{
                                logg = new Log(parametri: "non è stata inserita nessuna targa ", operazione: "invio GMFGEN", pagina: "LISTA POLIZZE CASH")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                result = db.firstRow("select polizze.annualita, rinnovi.provincia, rinnovi.provv_gm, polizze.versione_tariffa from polizze join rinnovi on polizze.idpolizze=rinnovi.idpolizze where telaio='${polizzaWS.telaio}'  order by rinnovi.idpolizze desc")
                            }
                            //println "sono arrivata qui"
                            //result = db.firstRow("select polizze.annualita, rinnovi.provincia, rinnovi.provv_gm, polizze.versione_tariffa from polizze join rinnovi on polizze.idpolizze=rinnovi.idpolizze where (targa='${polizzaWS.targa}' or telaio='${polizzaWS.telaio}') and cliente like '%${polizzaWS.cliente}%' order by rinnovi.idpolizze desc")
                            if (result) {
                                logg = new Log(parametri: "result ${result}", operazione: "invio GMFGEN", pagina: "LISTA POLIZZE CASH")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                def versione=result[3]
                                //println "versione $versione"
                                if(versione==1){
                                    programma = "Flex Protection Rinnovi"

                                }else{
                                    programma = "Flex Protection Rinnovi 2017"

                                }
                                venditore = ""
                                dealer = ""
                                codDealer = ""
                                satellitare = "N"
                                zonaTariffaria = ZoneTarif.findByProvincia(result[1]).zona
                                tariffa = ""
                                rinnovo = "1"
                                nuovo = "RINNOVO"
                                provvVendi = ""
                                provvDealer = ""
                                provvGMF = result[2]
                                logg = new Log(parametri: "provv gmf  ${provvGMF} --> $noPolizza", operazione: "invio GMFGEN", pagina: "LISTA POLIZZE CASH")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                //provvGMF=provvGMF.toString().replace(".",",")
                                provvGMF = new BigDecimal(provvGMF)
                                provvGMF = provvGMF.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                logg = new Log(parametri: "provv gmf dopo conversione -->  ${provvGMF} --> $noPolizza", operazione: "invio GMFGEN", pagina: "LISTA POLIZZE CASH")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                //provvGMF=fmt_IT.format(provvGMF)
                                account = ""
                                datapagamento = polizzaWS.datacomunicaz
                                if (datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                } else {
                                    datapagamento = ""
                                }
                                finanziata = "N"
                                totProvv = provvGMF
                                ritenuta = 0.046 * (totProvv)
                                netPayable = premioLordo - (provvGMF)
                                if (/*polizzaWS.statodslip != "0" &&*/ polizzaWS.statodslip != "V") {

                                    if (polizzaWS.statodslip != "S") {
                                        if (polizzaWS.statodslip == "E") {
                                            rimborso = "Y"
                                            def codOperazione = "E"
                                            polizzaRinn = []
                                            polizzaRinn = IAssicurWebService.getPolizzeRinnoviperAnnulla(polizzaWS.noPolizza).polizze
                                            isRinnovo = true
                                            def polizza = ""
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            //premioLordo = rispostawe.premioLordo
                                            //premioImpo = rispostawe.premioImpo
                                            //provvDealer = rispostawe.provvDealer
                                            //provvVendi = rispostawe.provvVendi
                                            //provvGMF = rispostawe.provvGMF
                                            if (rispostawe.premioLordo) {//(BigDecimal) decimalFormat.parse(
                                                rispostawe.premioLordo=rispostawe.premioLordo.toString().replace(".",",")
                                                premioLordo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioLordo.toString())
                                                premioLordo=premioLordo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioLordo = 0.0
                                            }
                                            if (rispostawe.premioImpo) {
                                                rispostawe.premioImpo=rispostawe.premioImpo.toString().replace(".",",")
                                                premioImpo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioImpo.toString())
                                                premioImpo=premioImpo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioImpo = 0.0
                                            }
                                            if (rispostawe.provvDealer) {
                                                rispostawe.provvDealer=rispostawe.provvDealer.toString().replace(".",",")
                                                provvDealer = (BigDecimal) fmt_IT.parseObject(rispostawe.provvDealer.toString())
                                                provvDealer=provvDealer.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvDealer = 0.0
                                            }
                                            if (rispostawe.provvVendi) {
                                                rispostawe.provvVendi=rispostawe.provvVendi.toString().replace(".",",")
                                                provvVendi = (BigDecimal) fmt_IT.parseObject(rispostawe.provvVendi.toString())
                                                provvVendi=provvVendi.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvVendi = 0.0
                                            }
                                            if (rispostawe.provvGMF) {
                                                rispostawe.provvGMF=rispostawe.provvGMF.toString().replace(".",",")
                                                provvGMF = (BigDecimal) fmt_IT.parseObject(rispostawe.provvGMF.toString())
                                                provvGMF=provvGMF.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvGMF = 0.0
                                            }
                                        }
                                        else if (polizzaWS.statodslip == "F") {
                                            rimborso = "Y"
                                            def codOperazione = "F"
                                            polizzaRinn = []
                                            polizzaRinn = IAssicurWebService.getPolizzeRinnoviperAnnulla(polizzaWS.noPolizza).polizze
                                            isRinnovo = true
                                            def polizza = ""
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            //premioLordo = rispostawe.premioLordo
                                            //premioImpo = rispostawe.premioImpo
                                            //provvDealer = rispostawe.provvDealer
                                            //provvVendi = rispostawe.provvVendi
                                            //provvGMF = rispostawe.provvGMF
                                            if (rispostawe.premioLordo) {//(BigDecimal) decimalFormat.parse(
                                                rispostawe.premioLordo=rispostawe.premioLordo.toString().replace(".",",")
                                                premioLordo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioLordo.toString())
                                                premioLordo=premioLordo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioLordo = 0.0
                                            }
                                            if (rispostawe.premioImpo) {
                                                rispostawe.premioImpo=rispostawe.premioImpo.toString().replace(".",",")
                                                premioImpo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioImpo.toString())
                                                premioImpo=premioImpo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioImpo = 0.0
                                            }
                                            if (rispostawe.provvDealer) {
                                                rispostawe.provvDealer=rispostawe.provvDealer.toString().replace(".",",")
                                                provvDealer = (BigDecimal) fmt_IT.parseObject(rispostawe.provvDealer.toString())
                                                provvDealer=provvDealer.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvDealer = 0.0
                                            }
                                            if (rispostawe.provvVendi) {
                                                rispostawe.provvVendi=rispostawe.provvVendi.toString().replace(".",",")
                                                provvVendi = (BigDecimal) fmt_IT.parseObject(rispostawe.provvVendi.toString())
                                                provvVendi=provvVendi.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvVendi = 0.0
                                            }
                                            if (rispostawe.provvGMF) {
                                                rispostawe.provvGMF=rispostawe.provvGMF.toString().replace(".",",")
                                                provvGMF = (BigDecimal) fmt_IT.parseObject(rispostawe.provvGMF.toString())
                                                provvGMF=provvGMF.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvGMF = 0.0
                                            }
                                        }
                                        else if (polizzaWS.statodslip == "G") {
                                            rimborso = "Y"
                                            def codOperazione = "G"
                                            polizzaRinn = []
                                            polizzaRinn = IAssicurWebService.getPolizzeRinnoviperAnnulla(polizzaWS.noPolizza).polizze
                                            isRinnovo = true
                                            def polizza = ""
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            //premioLordo = rispostawe.premioLordo
                                            //premioImpo = rispostawe.premioImpo
                                            //provvDealer = rispostawe.provvDealer
                                            //provvVendi = rispostawe.provvVendi
                                            //provvGMF = rispostawe.provvGMF
                                            if (rispostawe.premioLordo) {//(BigDecimal) decimalFormat.parse(
                                                rispostawe.premioLordo=rispostawe.premioLordo.toString().replace(".",",")
                                                premioLordo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioLordo.toString())
                                                premioLordo=premioLordo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioLordo = 0.0
                                            }
                                            if (rispostawe.premioImpo) {
                                                rispostawe.premioImpo=rispostawe.premioImpo.toString().replace(".",",")
                                                premioImpo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioImpo.toString())
                                                premioImpo=premioImpo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioImpo = 0.0
                                            }
                                            if (rispostawe.provvDealer) {
                                                rispostawe.provvDealer=rispostawe.provvDealer.toString().replace(".",",")
                                                provvDealer = (BigDecimal) fmt_IT.parseObject(rispostawe.provvDealer.toString())
                                                provvDealer=provvDealer.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvDealer = 0.0
                                            }
                                            if (rispostawe.provvVendi) {
                                                rispostawe.provvVendi=rispostawe.provvVendi.toString().replace(".",",")
                                                provvVendi = (BigDecimal) fmt_IT.parseObject(rispostawe.provvVendi.toString())
                                                provvVendi=provvVendi.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvVendi = 0.0
                                            }
                                            if (rispostawe.provvGMF) {
                                                rispostawe.provvGMF=rispostawe.provvGMF.toString().replace(".",",")
                                                provvGMF = (BigDecimal) fmt_IT.parseObject(rispostawe.provvGMF.toString())
                                                provvGMF=provvGMF.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvGMF = 0.0
                                            }
                                        }
                                        netPayable = premioLordo - (provvGMF)
                                        ritenuta = 0.046 * (provvGMF)
                                        totProvv = provvGMF
                                    } else if(polizzaWS.statodslip == "S") {
                                        rimborso = "Y"
                                        netPayable = premioLordo + (provvGMF)
                                        ritenuta = ritenuta.negate()
                                        totProvv = totProvv.negate()
                                        premioLordo = premioLordo.negate()
                                        premioImpo = premioImpo.negate()
                                        //provvDealer = provvDealer.negate()
                                        //provvVendi = provvVendi.negate()
                                        provvGMF = provvGMF.negate()
                                    }
                                }
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza = TipoPolizza.RINNOVI
                                polizzaTotali.noPolizza = polizzaWS.noPolizza
                                polizzaTotali.programma = programma
                                polizzaTotali.assicurato = polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore = venditore
                                polizzaTotali.concessionario = dealer
                                polizzaTotali.dataPagamento = datapagamento
                                polizzaTotali.dataCopertura = dataInizio
                                polizzaTotali.dataImmisione = dataInizio
                                polizzaTotali.dataScadenza = dataFine
                                polizzaTotali.valoreAssicurato = valoreAssicurato
                                polizzaTotali.cod_Conc = codDealer
                                polizzaTotali.cod_vend = ""
                                polizzaTotali.targa = polizzaWS.targa
                                polizzaTotali.telaio = polizzaWS.telaio
                                polizzaTotali.pacchetto = pacchetto
                                polizzaTotali.satellitare = satellitare
                                polizzaTotali.livello = polizzaWS.anni
                                polizzaTotali.zonaTariffaria = zonaTariffaria
                                polizzaTotali.tariffa = tariffa
                                polizzaTotali.rinnovo = rinnovo
                                polizzaTotali.nuovo = nuovo
                                polizzaTotali.durata = polizzaWS.durata
                                polizzaTotali.premioLordo = premioLordo
                                polizzaTotali.premioImpo = premioImpo
                                polizzaTotali.provvVendi = provvVendi
                                polizzaTotali.provvDealer = provvDealer
                                polizzaTotali.provvGmfi = provvGMF
                                polizzaTotali.provvAddFee = ""
                                polizzaTotali.provvAgg = ""
                                polizzaTotali.account = account
                                polizzaTotali.finanziata = finanziata
                                polizzaTotali.rimborso = rimborso
                                polizzaTotali.noteRimb = ""
                                polizzaTotali.totProvv = totProvv
                                polizzaTotali.ritenuta = ritenuta
                                polizzaTotali.netPayable = netPayable

                                row {
                                    cell(polizzaWS.noPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataInizio)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizzaWS.targa)
                                    cell(polizzaWS.telaio)
                                    cell(pacchetto)
                                    cell(satellitare)
                                    cell(result[0])
                                    cell(zonaTariffaria)
                                    cell(tariffa)
                                    cell(rinnovo)
                                    cell(nuovo)
                                    cell(polizzaWS.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(account)
                                    cell(finanziata)
                                    cell(rimborso)
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                }
                                for (int i = 0; i < 36; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali = [:]
                            }

                        }
                    }

                }
                sheet("Retail - Polizze") {
                    row(style: "header") {
                        cell("Tipo Polizza")
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                        cell("CODICE CAMPAGNA")
                        cell("PREMIO LORDO CAMPAGNA")
                        cell("PREMIO NETTO CAMPAGNA")
                        cell("PREMIO LORDO CLIENTE")
                        cell("PREMIO NETTO CLIENTE")
                    }
                    if (allPolizze) {
                        def rPolizze = allPolizze.polizze
                        rPolizze.each { polizzaWS ->
                            row {
                                cell(polizzaWS.tipoPolizza)
                                cell(polizzaWS.noPolizza)
                                cell(polizzaWS.programma)
                                cell(polizzaWS.assicurato)
                                cell(polizzaWS.venditore)
                                cell(polizzaWS.concessionario)
                                cell(polizzaWS.datapagamento)
                                cell(polizzaWS.dataCopertura)
                                cell(polizzaWS.dataCopertura)
                                cell(polizzaWS.dataScadenza)
                                cell(polizzaWS.valoreAssicurato)
                                cell(polizzaWS.cod_Conc)
                                cell(polizzaWS.cod_Vend)
                                cell(polizzaWS.targa)
                                cell(polizzaWS.telaio)
                                cell(polizzaWS.pacchetto)
                                cell(polizzaWS.satellitare)
                                cell(polizzaWS.livello)
                                cell(polizzaWS.zonaTariffaria)
                                cell(polizzaWS.tariffa)
                                cell(polizzaWS.rinnovo)
                                cell(polizzaWS.nuovo)
                                cell(polizzaWS.durata)
                                cell(polizzaWS.premioLordo)
                                cell(polizzaWS.premioImpo)
                                cell(polizzaWS.provvVendi)
                                cell(polizzaWS.provvDealer)
                                cell(polizzaWS.provvGmfi)
                                cell(polizzaWS.provvAddFee)
                                cell(polizzaWS.provvAgg)
                                cell(polizzaWS.account)
                                cell(polizzaWS.finanziata)
                                cell(polizzaWS.rimborso)
                                cell(polizzaWS.noteRimb)
                                cell(polizzaWS.totProvv)
                                cell(polizzaWS.ritenuta)
                                cell(polizzaWS.netPayable)
                                cell(polizzaWS.codCampagna)
                                cell(polizzaWS.premioLordoCampagna)
                                cell(polizzaWS.premioNettoCampagna)
                                cell(polizzaWS.premioLordoCliente)
                                cell(polizzaWS.premioNettoCliente)
                            }
                            for (int i = 0; i < 36; i++) {
                                sheet.autoSizeColumn(i)
                            }
                        }
                    }

                }
            }
        }
        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        /**qui faccio la modifica**/
        def caricamentoftp=ftpService.uploadFileGMFGEN(fileName, stream)
            /***INVIO LA MAIL A mach1 con il riepilogo dei certificati generati e i campioni dei certificati*/
            def inviaMailCaricamento = mailService.invioMailGMFGENMach1(caricamentoftp, fileName)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto della creazione del report GMFGEN \u00E8 stata inviata", operazione: "invio GMFGEN", pagina: "LISTA POLIZZE CASH")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                rispostaCert=true
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail del report GMFGEN", operazione: "invio GMFGEN", pagina: "LISTA POLIZZE CASH")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Non e' stato possibile inviare la mail di certificati falliti \n ${inviaMailCaricamento}"
                rispostaCert=true

            }

        //return stream.toByteArray()
        return rispostaCert
    }
    def fileOFSGEN(def polizzeWS, def mese) {
        def rispostaCert=false
        def streamGMFGEN = new ByteArrayOutputStream()
        def zipGMFGEN = new ZipOutputStream(streamGMFGEN)
        def logg
        logg = new Log(parametri: "comincio a generare il file con i dati", operazione: "generazione file OFSGEN", pagina: "LISTA POLIZZE RCA")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def db = rinnDb
        def fileName="OFSGEN_${new Date().format("ddMMyyyy")}.xlsx"
        def  fmt_IT = NumberFormat.getNumberInstance(Locale.ITALIAN)
        fmt_IT.setMaximumFractionDigits(2)
        def programma, venditore, dealer, datapagamento, codDealer, pacchetto, categoriaveicolo, zonaTariffaria, tariffa, rinnovo, nuovo, finanziata, provvVendi, provvDealer, provvGMF, netPayable, rimborso, account, totProvv, ritenuta, coperturaR,codCampagna,premioLordoCampagna,premioNettoCampagna,premioLordoCliente,premioNettoCliente
        def polizzeCash = [polizze: []]
        def polizzeLeasing = [polizze: []]
        def polizzeFinanziate = [polizze: []]
        def polizzeRinn = [polizze: []]
        def allPolizze = [polizze: []]
        def polizzaTotali = [:]
        def catveicoliP=['KARL','ADAM','CORSA 5 SERIE','COMBO TOUR','COMBO 4 SERIE','AGILA 1 SERIE','AGILA 2 SERIE']
        def catveicoliM=['ASTRA 3 SERIE','ASTRA 4 SERIE','ASTRA 5 SERIE','MERIVA 2 S.','MOKKA','MOKKA X','VIVARO 3 SERIE','CROSSLAND X','GRANDLAND X']
        def catveicoliG=['INSIGNIA','INSIGNIA 2 SERIE','ZAFIRA 3 SERIE','ANTARA','MOVANO 4 SERIE','CASCADA']
        def codzonaA=['CE','BN','NA','AV','SA','FG','BA','TA','BR','LE','BT']
        def codzonaM=['TO','VC','NO','CN','AT','AL','BI','VB','VA','CO','SO','MI','BG','BS','PV','CR','MN','LC','LO','MB','IM','SV','GE','SP','MS','LU','PT','FI','LI','PI','AR','SI','GR','PO','VT','RI','RM','LT','FR','AQ','TE','PE','CH','CB','IS','PZ','MT','CS','CZ','RC','KR','VV','TP','PA','ME','AG','CL','EN','CT','RG','SR']
        def codzonaB=['AO','BZ','TN','VR','VI','BL','TV','VE','PD','RO','UD','GO','TS','PN','PC','PR','RE','MO','BO','FE','RA','FC','RN','PG','TR','PU','AN','MC','AP','FM','SS','NU','CA','OR','OT','OG','VS','CI']
        polizzeWS.each { polizzaWS ->
            if (polizzaWS.statodslip != "V") {
                /*if (polizzaWS.ramo.contains("U/12") || polizzaWS.ramo.contains("U/13") || polizzaWS.ramo.contains("U/14") || polizzaWS.ramo.contains("U/15") ||
                        polizzaWS.ramo.contains("U/16") || polizzaWS.ramo.contains("U/17") || polizzaWS.ramo.contains("U/18") || polizzaWS.ramo.contains("U/19") ||
                        polizzaWS.ramo.contains("U/20") || polizzaWS.ramo.contains("U/21") || polizzaWS.ramo.contains("U/22") || polizzaWS.ramo.contains("U/23") ||
                        polizzaWS.ramo.contains("U/24") || polizzaWS.ramo.contains("U/25") || polizzaWS.ramo.contains("U/26") || polizzaWS.ramo.contains("W/46")||
                        polizzaWS.ramo.contains("W/47")|| polizzaWS.ramo.contains("W/48")|| polizzaWS.ramo.contains("W/49")|| polizzaWS.ramo.contains("W/50")||
                        polizzaWS.ramo.contains("W/51")|| polizzaWS.ramo.contains("W/52")|| polizzaWS.ramo.contains("W/53")|| polizzaWS.ramo.contains("W/54")||
                        polizzaWS.ramo.contains("W/55")|| polizzaWS.ramo.contains("W/56")|| polizzaWS.ramo.contains("W/57")|| polizzaWS.ramo.contains("W/58")||
                        polizzaWS.ramo.contains("W/59")|| polizzaWS.ramo.contains("W/60")) {
                    polizzeCash.polizze << polizzaWS
                } else*/ if (polizzaWS.ramo.contains("D/68") ) {
                    polizzeLeasing.polizze << polizzaWS
                } else if (polizzaWS.ramo.contains("D/67")) {
                    polizzeFinanziate.polizze << polizzaWS
                }/* else if (polizzaWS.ramo.contains("U/59") || polizzaWS.ramo.contains("U/60") || polizzaWS.ramo.contains("U/61") || polizzaWS.ramo.contains("U/62")
                        || polizzaWS.ramo.contains("U/63") || polizzaWS.ramo.contains("U/64") || polizzaWS.ramo.contains("U/65") || polizzaWS.ramo.contains("U/66")
                        || polizzaWS.ramo.contains("U/67") || polizzaWS.ramo.contains("U/68") || polizzaWS.ramo.contains("W/92") || polizzaWS.ramo.contains("W/94")
                        || polizzaWS.ramo.contains("W/95") || polizzaWS.ramo.contains("W/96") || polizzaWS.ramo.contains("W/97") || polizzaWS.ramo.contains("W/98")
                        || polizzaWS.ramo.contains("W/99") || polizzaWS.ramo.contains("D/01") || polizzaWS.ramo.contains("D/02") || polizzaWS.ramo.contains("D/03")
                        || polizzaWS.ramo.contains("D/04") || polizzaWS.ramo.contains("D/05") || polizzaWS.ramo.contains("D/06") || polizzaWS.ramo.contains("D/07")
                        || polizzaWS.ramo.contains("D/08") || polizzaWS.ramo.contains("D/09") || polizzaWS.ramo.contains("D/10") || polizzaWS.ramo.contains("D/11")
                        || polizzaWS.ramo.contains("D/12") || polizzaWS.ramo.contains("D/21") || polizzaWS.ramo.contains("D/22") || polizzaWS.ramo.contains("D/23")
                        || polizzaWS.ramo.contains("D/24") || polizzaWS.ramo.contains("D/25") || polizzaWS.ramo.contains("D/26") || polizzaWS.ramo.contains("D/27")
                        || polizzaWS.ramo.contains("D/28") || polizzaWS.ramo.contains("D/29") || polizzaWS.ramo.contains("D/30") ) {
                    polizzeRinn.polizze << polizzaWS
                }*/
            }
        }
        def polizzeF = polizzeFinanziate.polizze
        def polizzeL = polizzeLeasing.polizze
        def polizzeC = polizzeCash.polizze
        def polizzeRN = polizzeRinn.polizze
        boolean isRinnovo = false
        def polizzaRinn
        programma = "FLEX RCA"
        def piccola, media, grande,alto,medio,basso
        def wb = Stopwatch.log {
            ExcelBuilder.create {
                style("header") {
                    background bisque
                    font {
                        bold(true)
                    }
                }
                sheet("Retail - Finanziate") {
                    row(style: "header") {
                        cell("Numero Polizza")
                        //cell("Tipo Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Modello")
                        cell("Categoria Modello")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")

                    }
                    if (polizzeF) {
                        polizzeF.each { polizzaWS ->
                            //per le polizze leasing / finanziate
                            def noPolizza = polizzaWS.noPolizza
                            rimborso = "N"
                            def premioLordo, premioImpo, valoreAssicurato
                            def polizza = PolizzaRCA.findByNoPolizza(noPolizza.toString().trim())
                            def dataInizio, dataFine
                            /*if (dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            } else {
                                dataInizio = ""
                            }*/
                            /*if (dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            } else {
                                dataFine = ""
                            }*/
                            if (polizza) {
                                dataInizio = polizza.dataDecorrenza?.format("dd/MM/yyyy")
                                dataFine = polizza.dataScadenza?.format("dd/MM/yyyy")
                                venditore = polizza.venditore?.toUpperCase()
                                dealer = polizza.dealer.ragioneSocialeD?.toUpperCase()
                                codDealer = polizza.dealer.codice.toString().trim().padLeft(4, "0")
                                if(polizza.codiceZonaTerritoriale=="0"){
                                    alto = codzonaA.find { item ->
                                        item.contains(polizza.provincia?.toUpperCase())
                                    }
                                    medio = codzonaM.find { item ->
                                        item.contains(polizza.provincia?.toUpperCase())
                                    }
                                    basso = codzonaB.find { item ->
                                        item.contains(polizza.provincia?.toUpperCase())
                                    }

                                    if(alto){
                                        polizza.codiceZonaTerritoriale="A"
                                        zonaTariffaria = "ALTA"
                                        if(!polizza.save(flush:true)){
                                            logg = new Log(parametri: "cod zona territoriale piccolo per polizza RCA ${polizza.noPolizza} non aggiornata", operazione: "generaFile OFSGEN", pagina: "LISTA POLIZZE RCA")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }
                                    if(medio){
                                        polizza.codiceZonaTerritoriale="M"
                                        zonaTariffaria = "MEDIA"
                                        if(!polizza.save(flush:true)){
                                            logg = new Log(parametri: "cod zona territoriale per polizza RCA ${polizza.noPolizza} non aggiornata", operazione: "generaFile OFSGEN", pagina: "LISTA POLIZZE RCA")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }
                                    if(basso){
                                        polizza.codiceZonaTerritoriale="B"
                                        zonaTariffaria = "BASSA"
                                        if(!polizza.save(flush:true)){
                                            logg = new Log(parametri: "cod zona territoriale per polizza RCA ${polizza.noPolizza} non aggiornata", operazione: "generaFile OFSGEN", pagina: "LISTA POLIZZE RCA")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }
                                    alto=""
                                    medio=""
                                    basso=""
                                }else{
                                    if(polizza.codiceZonaTerritoriale.equalsIgnoreCase('A')){
                                        zonaTariffaria = "ALTA"
                                    }else if(polizza.codiceZonaTerritoriale.equalsIgnoreCase('B')){
                                        zonaTariffaria = "BASSA"
                                    }else if(polizza.codiceZonaTerritoriale.equalsIgnoreCase('M')){
                                        zonaTariffaria = "MEDIA"
                                    }else{zonaTariffaria = ""}
                                }
                                if(!polizza.categoriaveicolo){
                                    piccola = catveicoliP.find { item ->
                                        item.contains(polizza.modello?.toUpperCase())
                                    }
                                    media = catveicoliM.find { item ->
                                        item.contains(polizza.modello?.toUpperCase())
                                    }
                                    grande = catveicoliG.find { item ->
                                        item.contains(polizza.modello?.toUpperCase())
                                    }

                                    if(piccola){
                                        polizza.categoriaveicolo="P"
                                        categoriaveicolo = "PICCOLE"
                                        if(!polizza.save(flush:true)){
                                            logg = new Log(parametri: "cat veicolo piccolo per polizza RCA ${polizza.noPolizza} non aggiornata", operazione: "generaFile OFSGEN", pagina: "LISTA POLIZZE RCA")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }
                                    if(media){
                                        polizza.categoriaveicolo="M"
                                        categoriaveicolo = "MEDIE"
                                        if(!polizza.save(flush:true)){
                                            logg = new Log(parametri: "cat veicolo medio per polizza RCA ${polizza.noPolizza} non aggiornata", operazione: "generaFile OFSGEN", pagina: "LISTA POLIZZE RCA")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }
                                    if(grande){
                                        polizza.categoriaveicolo="G"
                                        categoriaveicolo = "GRANDI"
                                        if(!polizza.save(flush:true)){
                                            logg = new Log(parametri: "cat veicolo grande per polizza RCA ${polizza.noPolizza} non aggiornata", operazione: "generaFile OFSGEN", pagina: "LISTA POLIZZE RCA")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }
                                    piccola=""
                                    media=""
                                    grande=""
                                }else{
                                    if(polizza.categoriaveicolo?.equalsIgnoreCase("G")){
                                        categoriaveicolo = "GRANDI"
                                    }else if(polizza.categoriaveicolo?.equalsIgnoreCase("M")){
                                        categoriaveicolo = "MEDIE"
                                    }else if(polizza.categoriaveicolo?.equalsIgnoreCase("P")){
                                        categoriaveicolo = "PICCOLE"
                                    }else{categoriaveicolo = ""}

                                }
                                //println "no.polizza ${noPolizza} categoria vei ${categoriaveicolo}"
                                //println "no. polizza ${noPolizza} cod. zona  ${zonaTariffaria}"
                                tariffa = "${polizza.dealer.dealerType} - step 1"
                                rinnovo = "0"
                                nuovo = "NUOVI"
                                premioLordo = polizza.premioLordo
                                premioImpo = polizza.premioImponibile
                                valoreAssicurato = polizza.valoreAssicurato
                                provvVendi = polizza.provvVenditore
                                provvDealer = polizza.provvDealer
                                provvGMF = polizza.provvGmfi
                                account = polizza.dealer.salesSpecialist?.toUpperCase()
                                def nomeSales = account.split(" ")
                                def salesSpec
                                if (nomeSales.size() == 2) {
                                    salesSpec = nomeSales[1] + " " + nomeSales[0]
                                } else if (nomeSales.size() > 2) {
                                    for (int ind = 0; ind < nomeSales.size(); ind++) {
                                        salesSpec += nomeSales[ind] + " "
                                    }
                                }
                                datapagamento = polizzaWS.datacompagnia
                                if (datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                } else {
                                    datapagamento = ""
                                }

                                def dataImmisione=polizza.dataImmatricolazione?.format("dd/MM/yyyy")
                                finanziata = "S"
                                totProvv = provvDealer + provvVendi + provvGMF
                                ritenuta = 0.046 * (provvDealer + provvVendi + provvGMF)
                                netPayable = premioLordo - (provvDealer + provvVendi + provvGMF)
                                if (polizzaWS.statodslip != "0" && polizzaWS.statodslip != "V" ) {
                                    rimborso = "Y"
                                    isRinnovo = false
                                    polizzaRinn = ""
                                    netPayable = netPayable.negate()
                                    ritenuta = ritenuta.negate()
                                    totProvv = totProvv.negate()
                                    premioImpo = premioImpo.negate()
                                    premioLordo = premioLordo.negate()
                                    provvDealer = provvDealer.negate()
                                    provvVendi = provvVendi.negate()
                                    provvGMF = provvGMF.negate()

                                    /*if(polizzaWS.statodslip=="E"){
                                        def codOperazione = "E"
                                        def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                        println "premio lordo ${premioLordo}"
                                        println "provvDealer ${provvDealer}"
                                        println "provvVendi ${provvVendi}"
                                        println "provvGMF ${provvGMF}"
                                    }else if(polizzaWS.statodslip=="F"){
                                        def codOperazione = "F"
                                        def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                        premioLordo = rispostawe.premioLordo
                                        premioImpo = rispostawe.premioImpo
                                        provvDealer = rispostawe.provvDealer
                                        provvVendi = rispostawe.provvVendi
                                        provvGMF = rispostawe.provvGMF
                                    }else if(polizzaWS.statodslip=="G"){
                                        def codOperazione = "G"
                                         def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                         premioLordo = rispostawe.premioLordo
                                         premioImpo = rispostawe.premioImpo
                                         provvDealer = rispostawe.provvDealer
                                         provvVendi = rispostawe.provvVendi
                                         provvGMF = rispostawe.provvGMF
                                    }
                                    else
                                    if(polizzaWS.statodslip=="S") {
                                        println "premio lordo ${premioLordo}"
                                        println "provvDealer ${provvDealer}"
                                        println "provvVendi ${provvVendi}"
                                        println "provvGMF ${provvGMF}"
                                        netPayable = netPayable.negate()
                                        ritenuta = ritenuta.negate()
                                        totProvv = totProvv.negate()
                                        premioImpo = premioImpo.negate()
                                        premioLordo = premioLordo.negate()
                                        provvDealer = provvDealer.negate()
                                        provvVendi = provvVendi.negate()
                                        provvGMF = provvGMF.negate()
                                    }*/
                                }
                                def modello=polizza.modello.toString().toUpperCase()
                                def modellor=polizza.modello.toString().toUpperCase()
                                if(modello.contains("ANTARA") && !polizza.dacsv){
                                    modellor="ANTARA"
                                }else if(modello.contains("ADAM") && !polizza.dacsv){
                                    modellor="ADAM"
                                }else if(modello.contains("ASTRA") && !polizza.dacsv){
                                    modellor="ASTRA"
                                }else if(modello.contains("CASCADA")&& !polizza.dacsv){
                                    modellor="CASCADA"
                                }else if(modello.contains("COMBO")&& !polizza.dacsv){
                                    modellor="COMBO"
                                }else if(modello.contains("CORSA")&& !polizza.dacsv){
                                    modellor="CORSA"
                                }else if(modello.contains("CROSSLAND X") && !polizza.dacsv){
                                    modellor="CROSSLAND X"
                                }else if(modello.contains("INSIGNIA")&& !polizza.dacsv){
                                    modellor="INSIGNIA"
                                }else if(modello.contains("KARL")&& !polizza.dacsv){
                                    modellor="KARL"
                                }else if(modello.contains("MERIVA")&& !polizza.dacsv){
                                    modellor="MERIVA"
                                }else if(modello.contains("MOKKA")&& !polizza.dacsv){
                                    modellor="MOKKA"
                                }else if(modello.contains("ZAFIRA") && !polizza.dacsv){
                                    modellor="ZAFIRA"
                                }else if(modello.contains("MOVANO") && !polizza.dacsv){
                                    modellor="MOVANO"
                                }else if(modello.contains("VIVARO") && !polizza.dacsv){
                                    modellor="VIVARO"
                                }else if(modello.contains("AGILA") && !polizza.dacsv){
                                    modellor="AGILA"
                                }else if(modello.contains("GRANDLAND X")&& !polizza.dacsv){
                                    modellor="GRANDLAND X"
                                }
                                def numPolizza = ""
                                numPolizza = polizza.noPolizza
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza = polizza.tipoPolizza
                                polizzaTotali.noPolizza = numPolizza
                                polizzaTotali.programma = programma
                                polizzaTotali.assicurato = polizzaWS.cliente.toString()?.toUpperCase()
                                polizzaTotali.venditore = venditore
                                polizzaTotali.concessionario = dealer
                                polizzaTotali.dataPagamento = datapagamento
                                polizzaTotali.dataCopertura = dataInizio
                                polizzaTotali.dataImmisione = dataImmisione
                                polizzaTotali.dataScadenza = dataFine
                                polizzaTotali.valoreAssicurato = valoreAssicurato
                                polizzaTotali.cod_Conc = codDealer
                                polizzaTotali.cod_vend = ""
                                polizzaTotali.targa = polizza.targa?.toUpperCase()?:""
                                polizzaTotali.modello = modellor
                                polizzaTotali.categoriaveicolo = categoriaveicolo
                                polizzaTotali.telaio = polizza.telaio?.toUpperCase()?:""
                                polizzaTotali.pacchetto = "RCA"
                                polizzaTotali.satellitare = ""
                                polizzaTotali.livello = 1
                                polizzaTotali.zonaTariffaria = zonaTariffaria
                                polizzaTotali.tariffa = tariffa
                                polizzaTotali.rinnovo = 0
                                polizzaTotali.nuovo = nuovo
                                polizzaTotali.durata = polizza.durata
                                polizzaTotali.premioLordo = premioLordo
                                polizzaTotali.premioImpo = premioImpo
                                polizzaTotali.provvVendi = provvVendi
                                polizzaTotali.provvDealer = provvDealer
                                polizzaTotali.provvGmfi = provvGMF
                                polizzaTotali.provvAddFee = ""
                                polizzaTotali.provvAgg = ""
                                polizzaTotali.account = salesSpec
                                polizzaTotali.finanziata = finanziata
                                polizzaTotali.rimborso = rimborso
                                polizzaTotali.noteRimb = ""
                                polizzaTotali.totProvv = totProvv
                                polizzaTotali.ritenuta = ritenuta
                                polizzaTotali.netPayable = netPayable
                                row {
                                    cell(numPolizza)
                                    //cell(polizza.tipoPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataImmisione)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizza.targa?.toUpperCase()?:"")
                                    cell(modellor)
                                    cell(categoriaveicolo)
                                    cell(polizza.telaio?.toUpperCase()?:"")
                                    cell("RCA")
                                    cell("")
                                    cell(1)
                                    cell(zonaTariffaria)
                                    cell(tariffa)
                                    cell(0)
                                    cell(nuovo)
                                    cell(polizza.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(salesSpec)
                                    cell(finanziata)
                                    cell(rimborso)
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)

                                }
                                for (int i = 0; i < 40; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali = [:]
                            }
                        }
                    } else {
                        logg = new Log(parametri: "non ci sono polizze finanziate ", operazione: "generaFile OFSGEN", pagina: "LISTA POLIZZE RCA")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }
                sheet("Retail - Leasing") {
                    row(style: "header") {
                        cell("Numero Polizza")
                       // cell("Tipo Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Modello")
                        cell("Categoria Modello")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                    }
                    if (polizzeL) {
                        polizzeL.each { polizzaWS ->
                            //per le polizze leasing / finanziate
                            def noPolizza = polizzaWS.noPolizza
                            //println polizzaWS.codiceDSLIP
                            rimborso = "N"
                            def premioLordo, premioImpo, valoreAssicurato

                            def polizza = PolizzaRCA.findByNoPolizza(noPolizza.toString().trim())
                            def dataInizio, dataFine,dataImmisione
                            /*if (dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            } else {
                                dataInizio = ""
                            }*/
                            /*if (dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            } else {
                                dataFine = ""
                            }*/
                            if (polizza) {
                                dataInizio = polizza.dataDecorrenza?.format("dd/MM/yyyy")
                                dataFine = polizza.dataScadenza?.format("dd/MM/yyyy")
                                dataImmisione=polizza.dataImmatricolazione?.format("dd/MM/yyyy")
                                venditore = polizza.venditore?.toUpperCase()
                                dealer = polizza.dealer?.ragioneSocialeD.toUpperCase()
                                codDealer = polizza.dealer?.codice.toString().trim().padLeft(4, "0")
                                if(polizza.codiceZonaTerritoriale=="0"){
                                    alto = codzonaA.find { item ->
                                        item.contains(polizza.provincia?.toUpperCase())
                                    }
                                    medio = codzonaM.find { item ->
                                        item.contains(polizza.provincia?.toUpperCase())
                                    }
                                    basso = codzonaB.find { item ->
                                        item.contains(polizza.provincia?.toUpperCase())
                                    }

                                    if(alto){
                                        polizza.codiceZonaTerritoriale="A"
                                        zonaTariffaria = "ALTA"
                                        if(!polizza.save(flush:true)){
                                            logg = new Log(parametri: "cat veicolo piccolo per polizza RCA ${polizza.noPolizza} non aggiornata", operazione: "generaFile OFSGEN", pagina: "LISTA POLIZZE RCA")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }
                                    if(medio){
                                        polizza.codiceZonaTerritoriale="M"
                                        zonaTariffaria = "MEDIA"
                                        if(!polizza.save(flush:true)){
                                            logg = new Log(parametri: "cat veicolo medio per polizza RCA ${polizza.noPolizza} non aggiornata", operazione: "generaFile OFSGEN", pagina: "LISTA POLIZZE RCA")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }
                                    if(basso){
                                        polizza.codiceZonaTerritoriale="B"
                                        zonaTariffaria = "BASSA"
                                        if(!polizza.save(flush:true)){
                                            logg = new Log(parametri: "cat veicolo grande per polizza RCA ${polizza.noPolizza} non aggiornata", operazione: "generaFile OFSGEN", pagina: "LISTA POLIZZE RCA")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }
                                    alto=""
                                    medio=""
                                    basso=""
                                }else{
                                    if(polizza.codiceZonaTerritoriale.equalsIgnoreCase('A')){
                                        zonaTariffaria = "ALTA"
                                    }else if(polizza.codiceZonaTerritoriale.equalsIgnoreCase('B')){
                                        zonaTariffaria = "BASSA"
                                    }else if(polizza.codiceZonaTerritoriale.equalsIgnoreCase('M')){
                                        zonaTariffaria = "MEDIA"
                                    }else{zonaTariffaria = ""}
                                }
                                //println "modello $polizza.modello"
                                if(!polizza.categoriaveicolo){
                                    piccola = catveicoliP.find { item ->
                                        item.contains(polizza.modello?.toUpperCase())
                                    }
                                    media = catveicoliM.find { item ->
                                        item.contains(polizza.modello?.toUpperCase())
                                    }
                                    grande = catveicoliG.find { item ->
                                        item.contains(polizza.modello?.toUpperCase())
                                    }

                                    if(piccola){
                                        polizza.categoriaveicolo="P"
                                        categoriaveicolo = "PICCOLE"
                                        if(!polizza.save(flush:true)){
                                            logg = new Log(parametri: "cat veicolo piccolo per polizza RCA ${polizza.noPolizza} non aggiornata", operazione: "generaFile OFSGEN", pagina: "LISTA POLIZZE RCA")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }
                                    if(media){
                                        polizza.categoriaveicolo="M"
                                        categoriaveicolo = "MEDIE"
                                        if(!polizza.save(flush:true)){
                                            logg = new Log(parametri: "cat veicolo medio per polizza RCA ${polizza.noPolizza} non aggiornata", operazione: "generaFile OFSGEN", pagina: "LISTA POLIZZE RCA")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }
                                    if(grande){
                                        polizza.categoriaveicolo="G"
                                        categoriaveicolo = "GRANDI"
                                        if(!polizza.save(flush:true)){
                                            logg = new Log(parametri: "cat veicolo grande per polizza RCA ${polizza.noPolizza} non aggiornata", operazione: "generaFile OFSGEN", pagina: "LISTA POLIZZE RCA")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }
                                    piccola=""
                                    media=""
                                    grande=""
                                }else{
                                    if(polizza.categoriaveicolo?.equalsIgnoreCase("G")){
                                        categoriaveicolo = "GRANDI"
                                    }else if(polizza.categoriaveicolo?.equalsIgnoreCase("M")){
                                        categoriaveicolo = "MEDIE"
                                    }else if(polizza.categoriaveicolo?.equalsIgnoreCase("P")){
                                        categoriaveicolo = "PICCOLE"
                                    }else{categoriaveicolo = ""}
                                }
                                tariffa = "${polizza.dealer?.dealerType} - step 1"
                                rinnovo = "0"
                                nuovo = "NUOVI"
                                premioLordo = polizza.premioLordo
                                premioImpo = polizza.premioImponibile
                                valoreAssicurato = polizza.valoreAssicurato
                                provvVendi = polizza.provvVenditore
                                provvDealer = polizza.provvDealer
                                provvGMF = polizza.provvGmfi
                                account = polizza.dealer.salesSpecialist.toUpperCase()
                                def nomeSales = account.split(" ")
                                def salesSpec
                                //println nomeSales
                                if (nomeSales.size() == 2) {
                                    salesSpec = nomeSales[1] + " " + nomeSales[0]
                                } else if (nomeSales.size() > 2) {
                                    for (int ind = 0; ind < nomeSales.size(); ind++) {
                                        salesSpec += nomeSales[ind] + " "
                                    }
                                }
                                datapagamento = polizzaWS.datacompagnia
                                if (datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                } else {
                                    datapagamento = ""
                                }
                                finanziata = "S"
                                totProvv = provvDealer + provvVendi + provvGMF
                                ritenuta = 0.046 * (provvDealer + provvVendi + provvGMF)
                                netPayable = premioLordo - (provvDealer + provvVendi + provvGMF)

                                if (polizzaWS.statodslip != "0" && polizzaWS.statodslip != "V") {
                                    isRinnovo = false
                                    polizzaRinn = ""
                                    rimborso = "Y"
                                    ritenuta = ritenuta.negate()
                                    totProvv = totProvv.negate()
                                    premioLordo = premioLordo.negate()
                                    premioImpo = premioImpo.negate()
                                    provvDealer = provvDealer.negate()
                                    provvVendi = provvVendi.negate()
                                    provvGMF = provvGMF.negate()
                                    /*if (polizzaWS.statodslip != "S") {
                                        if(polizza.codOperazione=="E"){
                                            def codOperazione = "E"
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF

                                        }else if(polizza.codOperazione=="F"){
                                            def codOperazione = "F"
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF

                                        }else if(polizza.codOperazione=="G"){
                                            def codOperazione = "G"
                                            /*def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF

                                        }

                                    } else {

                                    }*/

                                }
                                def numPolizza = polizza.noPolizza
                                def modello=polizza.modello.toString().toUpperCase()
                                def modellor=polizza.modello.toString().toUpperCase()
                                if(modello.contains("ANTARA") && !polizza.dacsv){
                                    modellor="ANTARA"
                                }else if(modello.contains("ADAM") && !polizza.dacsv){
                                    modellor="ADAM"
                                }else if(modello.contains("ASTRA") && !polizza.dacsv){
                                    modellor="ASTRA"
                                }else if(modello.contains("CASCADA")&& !polizza.dacsv){
                                    modellor="CASCADA"
                                }else if(modello.contains("COMBO")&& !polizza.dacsv){
                                    modellor="COMBO"
                                }else if(modello.contains("CORSA")&& !polizza.dacsv){
                                    modellor="CORSA"
                                }else if(modello.contains("CROSSLAND X") && !polizza.dacsv){
                                    modellor="CROSSLAND X"
                                }else if(modello.contains("INSIGNIA")&& !polizza.dacsv){
                                    modellor="INSIGNIA"
                                }else if(modello.contains("KARL")&& !polizza.dacsv){
                                    modellor="KARL"
                                }else if(modello.contains("MERIVA")&& !polizza.dacsv){
                                    modellor="MERIVA"
                                }else if(modello.contains("MOKKA")&& !polizza.dacsv){
                                    modellor="MOKKA"
                                }else if(modello.contains("ZAFIRA") && !polizza.dacsv){
                                    modellor="ZAFIRA"
                                }else if(modello.contains("MOVANO") && !polizza.dacsv){
                                    modellor="MOVANO"
                                }else if(modello.contains("VIVARO") && !polizza.dacsv){
                                    modellor="VIVARO"
                                }else if(modello.contains("AGILA") && !polizza.dacsv){
                                    modellor="AGILA"
                                }else if(modello.contains("GRANDLAND X")&& !polizza.dacsv){
                                    modellor="GRANDLAND X"
                                }
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza = polizza.tipoPolizza
                                polizzaTotali.noPolizza = numPolizza
                                polizzaTotali.programma = programma
                                polizzaTotali.assicurato = polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore = venditore
                                polizzaTotali.concessionario = dealer
                                polizzaTotali.dataPagamento = datapagamento
                                polizzaTotali.dataCopertura = dataInizio
                                polizzaTotali.dataImmisione = dataImmisione
                                polizzaTotali.dataScadenza = dataFine
                                polizzaTotali.valoreAssicurato = valoreAssicurato
                                polizzaTotali.cod_Conc = codDealer
                                polizzaTotali.cod_vend = ""
                                polizzaTotali.targa = polizza.targa?.toUpperCase()?:""
                                polizzaTotali.modello = modellor
                                polizzaTotali.categoriaveicolo = categoriaveicolo
                                polizzaTotali.telaio = polizzaWS.telaio
                                polizzaTotali.pacchetto = "RCA"
                                polizzaTotali.satellitare = ""
                                polizzaTotali.livello = 1
                                polizzaTotali.zonaTariffaria = zonaTariffaria
                                polizzaTotali.tariffa = tariffa
                                polizzaTotali.rinnovo = 0
                                polizzaTotali.nuovo = nuovo
                                polizzaTotali.durata = polizza.durata
                                polizzaTotali.premioLordo = premioLordo
                                polizzaTotali.premioImpo = premioImpo
                                polizzaTotali.provvVendi = provvVendi
                                polizzaTotali.provvDealer = provvDealer
                                polizzaTotali.provvGmfi = provvGMF
                                polizzaTotali.provvAddFee = ""
                                polizzaTotali.provvAgg = ""
                                polizzaTotali.account = salesSpec
                                polizzaTotali.finanziata = finanziata
                                polizzaTotali.rimborso = rimborso
                                polizzaTotali.noteRimb = ""
                                polizzaTotali.totProvv = totProvv
                                polizzaTotali.ritenuta = ritenuta
                                polizzaTotali.netPayable = netPayable

                                row {
                                    cell(numPolizza)
                                    //cell(polizza.tipoPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataImmisione)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizza.targa?.toUpperCase()?:"")
                                    cell(modellor)
                                    cell(categoriaveicolo)
                                    cell(polizzaWS.telaio)
                                    cell("RCA")
                                    cell("")
                                    cell(1)
                                    cell(zonaTariffaria)
                                    cell(tariffa)
                                    cell(0)
                                    cell(nuovo)
                                    cell(polizza.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(salesSpec)
                                    cell(finanziata)
                                    cell(rimborso)
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                }
                                for (int i = 0; i < 40; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali = [:]
                            }
                        }
                    } else {
                        logg = new Log(parametri: "non ci sono polizze leasing ", operazione: "generaFile OFSGEN", pagina: "LISTA POLIZZE RCA")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }
                /*sheet("Retail - Cash") {
                    row(style: "header") {
                        cell("Numero Polizza")
                       // cell("Tipo Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Modello")
                        cell("Categoria Modello")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                    }
                    if (polizzeC) {
                        polizzeC.each { polizzaWS ->
                            def noPolizza = polizzaWS.noPolizza
                            rimborso = "N"
                            def premioLordo, premioImpo, valoreAssicurato
                            def polizza = PolizzaRCA.findByNoPolizza(noPolizza.toString().trim())
                            def dataInizio = polizzaWS.inizio
                            if (dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            } else {
                                dataInizio = ""
                            }
                            def dataFine = polizzaWS.fine
                            if (dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            } else {
                                dataFine = ""
                            }
                            if (polizza) {
                                if(polizza.tariffa!=1){
                                    programma = "Flex Protection"
                                }else{
                                    programma = "Flex Protection 2017"
                                }
                                venditore = polizza.venditore.toUpperCase()
                                dealer = polizza.dealer.ragioneSocialeD.toUpperCase()
                                codDealer = polizza.dealer.codice.toString().trim().padLeft(4, "0")
                                zonaTariffaria = polizza.codiceZonaTerritoriale
                                tariffa = "${polizza.dealer.dealerType} - step 1"
                                rinnovo = "0"
                                nuovo = "NUOVI"
                                premioLordo = polizza.premioLordo
                                premioImpo = polizza.premioImponibile
                                valoreAssicurato = polizza.valoreAssicurato
                                provvVendi = polizza.provvVenditore
                                provvDealer = polizza.provvDealer
                                provvGMF = polizza.provvGmfi
                                account = polizza.dealer.salesSpecialist.toUpperCase()
                                def nomeSales = account.split(" ")
                                def salesSpec
                                //println nomeSales
                                if (nomeSales.size() == 2) {
                                    salesSpec = nomeSales[1] + " " + nomeSales[0]
                                } else if (nomeSales.size() > 2) {
                                    for (int ind = 0; ind < nomeSales.size(); ind++) {
                                        salesSpec += nomeSales[ind] + " "
                                    }
                                }
                                datapagamento = polizzaWS.datacomunicaz
                                if (datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                } else {
                                    datapagamento = ""
                                }
                                finanziata = "N"
                                totProvv = provvDealer + provvVendi + provvGMF
                                ritenuta = 0.046 * (provvDealer + provvVendi + provvGMF)
                                netPayable = premioLordo - (provvDealer + provvVendi + provvGMF)
                                if (polizzaWS.statodslip != "0" && polizzaWS.statodslip != "V") {
                                    isRinnovo = false
                                    polizzaRinn = ""
                                    rimborso = "Y"
                                    ritenuta = ritenuta.negate()
                                    totProvv = totProvv.negate()
                                    premioLordo = premioLordo.negate()
                                    premioImpo = premioImpo.negate()
                                    provvDealer = provvDealer.negate()
                                    provvVendi = provvVendi.negate()
                                    provvGMF = provvGMF.negate()
                                    *//*if (polizzaWS.statodslip != "S") {
                                        if(polizza.codOperazione=="E"){
                                            def codOperazione = "E"
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                        }else if(polizza.codOperazione=="F"){
                                            def codOperazione = "F"
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF
                                        }else if(polizza.codOperazione=="G"){
                                            def codOperazione = "G"
                                            /*def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            premioLordo = rispostawe.premioLordo
                                            premioImpo = rispostawe.premioImpo
                                            provvDealer = rispostawe.provvDealer
                                            provvVendi = rispostawe.provvVendi
                                            provvGMF = rispostawe.provvGMF
                                        }

                                    } else {

                                    }*//*
                                }
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                def modello=polizza.modello.toString().toUpperCase()
                                def modellor=polizza.modello.toString().toUpperCase()
                                if(modello.contains("ANTARA") && !polizza.dacsv){
                                    modellor="ANTARA"
                                }else if(modello.contains("ADAM") && !polizza.dacsv){
                                    modellor="ADAM"
                                }else if(modello.contains("ASTRA") && !polizza.dacsv){
                                    modellor="ASTRA"
                                }else if(modello.contains("CASCADA")&& !polizza.dacsv){
                                    modellor="CASCADA"
                                }else if(modello.contains("COMBO")&& !polizza.dacsv){
                                    modellor="COMBO"
                                }else if(modello.contains("CORSA")&& !polizza.dacsv){
                                    modellor="CORSA"
                                }else if(modello.contains("CROSSLAND X") && !polizza.dacsv){
                                    modellor="CROSSLAND X"
                                }else if(modello.contains("INSIGNIA")&& !polizza.dacsv){
                                    modellor="INSIGNIA"
                                }else if(modello.contains("KARL")&& !polizza.dacsv){
                                    modellor="KARL"
                                }else if(modello.contains("MERIVA")&& !polizza.dacsv){
                                    modellor="MERIVA"
                                }else if(modello.contains("MOKKA")&& !polizza.dacsv){
                                    modellor="MOKKA"
                                }else if(modello.contains("ZAFIRA") && !polizza.dacsv){
                                    modellor="ZAFIRA"
                                }else if(modello.contains("MOVANO") && !polizza.dacsv){
                                    modellor="MOVANO"
                                }else if(modello.contains("VIVARO") && !polizza.dacsv){
                                    modellor="VIVARO"
                                }else if(modello.contains("AGILA") && !polizza.dacsv){
                                    modellor="AGILA"
                                }else if(modello.contains("GRANDLAND X")&& !polizza.dacsv){
                                    modellor="GRANDLAND X"
                                }
                                polizzaTotali.tipoPolizza = polizza.tipoPolizza
                                polizzaTotali.noPolizza = numPolizza
                                polizzaTotali.programma = programma
                                polizzaTotali.assicurato = polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore = venditore
                                polizzaTotali.concessionario = dealer
                                polizzaTotali.dataPagamento = datapagamento
                                polizzaTotali.dataCopertura = dataInizio
                                polizzaTotali.dataImmisione = dataImmisione
                                polizzaTotali.dataScadenza = dataFine
                                polizzaTotali.valoreAssicurato = valoreAssicurato
                                polizzaTotali.cod_Conc = codDealer
                                polizzaTotali.cod_vend = ""
                                polizzaTotali.targa = polizza.targa?.toUpperCase()?:""
                                polizzaTotali.modello = modellor
                                polizzaTotali.categoriaveicolo = categoriaveicolo
                                polizzaTotali.telaio = polizzaWS.telaio
                                polizzaTotali.pacchetto = "RCA"
                                polizzaTotali.satellitare = ""
                                polizzaTotali.livello = 1
                                polizzaTotali.zonaTariffaria = zonaTariffaria
                                polizzaTotali.tariffa = tariffa
                                polizzaTotali.rinnovo = 0
                                polizzaTotali.nuovo = nuovo
                                polizzaTotali.durata = polizza.durata
                                polizzaTotali.premioLordo = premioLordo
                                polizzaTotali.premioImpo = premioImpo
                                polizzaTotali.provvVendi = provvVendi
                                polizzaTotali.provvDealer = provvDealer
                                polizzaTotali.provvGmfi = provvGMF
                                polizzaTotali.provvAddFee = ""
                                polizzaTotali.provvAgg = ""
                                polizzaTotali.account = salesSpec
                                polizzaTotali.finanziata = finanziata
                                polizzaTotali.rimborso = rimborso
                                polizzaTotali.noteRimb = ""
                                polizzaTotali.totProvv = totProvv
                                polizzaTotali.ritenuta = ritenuta
                                polizzaTotali.netPayable = netPayable

                                row {
                                    cell(polizza.noPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataInizio)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizzaWS.targa)
                                    cell(polizzaWS.telaio)
                                    cell(pacchetto)
                                    cell(satellitare)
                                    cell(polizzaWS.anni)
                                    cell(zonaTariffaria)
                                    cell(tariffa)
                                    cell(rinnovo)
                                    cell(nuovo)
                                    cell(polizzaWS.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(salesSpec)
                                    cell(finanziata)
                                    cell(rimborso)
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                }
                                for (int i = 0; i < 36; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali = [:]
                            }

                        }
                    }
                }*/
                /*sheet("Retail - Rinnovi") {
                    row(style: "header") {
                        cell("Numero Polizza")
                       // cell("Tipo Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Modello")
                        cell("Categoria Modello")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                    }
                    if (polizzeRN) {
                        polizzeRN.each { polizzaWS ->
                            def noPolizza = polizzaWS.noPolizza
                            if (polizzaWS.ramo.contains("U/61") || polizzaWS.ramo.contains("U/66")|| polizzaWS.ramo.contains("W/94") || polizzaWS.ramo.contains("W/99")|| polizzaWS.ramo.contains("D/05") || polizzaWS.ramo.contains("D/10") || polizzaWS.ramo.contains("D/23") || polizzaWS.ramo.contains("D/28")) {
                                pacchetto = "PLATINUM"
                            } else if (polizzaWS.ramo.contains("U/59") || polizzaWS.ramo.contains("U/64")|| polizzaWS.ramo.contains("W/92") || polizzaWS.ramo.contains("W/97") || polizzaWS.ramo.contains("D/03") || polizzaWS.ramo.contains("D/08") || polizzaWS.ramo.contains("D/21") || polizzaWS.ramo.contains("D/26")) {
                                pacchetto = "SILVER"
                            } else if (polizzaWS.ramo.contains("U/60") || polizzaWS.ramo.contains("U/65")|| polizzaWS.ramo.contains("W/93") || polizzaWS.ramo.contains("W/98") || polizzaWS.ramo.contains("D/04") || polizzaWS.ramo.contains("D/09") || polizzaWS.ramo.contains("D/22") || polizzaWS.ramo.contains("D/27")) {
                                pacchetto = "GOLD"
                            } else if (polizzaWS.ramo.contains("U/62") || polizzaWS.ramo.contains("U/67") || polizzaWS.ramo.contains("W/95")  || polizzaWS.ramo.contains("D/01")  || polizzaWS.ramo.contains("D/06") || polizzaWS.ramo.contains("D/11") || polizzaWS.ramo.contains("D/24") || polizzaWS.ramo.contains("D/29")) {
                                pacchetto = "PLATINUM+COLLISIONE"
                            } else if (polizzaWS.ramo.contains("U/63") || polizzaWS.ramo.contains("U/68")|| polizzaWS.ramo.contains("W/96")|| polizzaWS.ramo.contains("D/02") || polizzaWS.ramo.contains("D/07") || polizzaWS.ramo.contains("D/12") || polizzaWS.ramo.contains("D/25") || polizzaWS.ramo.contains("D/30")) {
                                pacchetto = "PLATINUM+KASKO"
                            }
                            if (polizzaWS.onstar.toString().trim().contains("SATELITTARE")) {
                                satellitare = "S"
                            } else {
                                satellitare = "N"
                            }
                            rimborso = "N"
                            def premioLordo, premioImpo, valoreAssicurato
                            if (polizzaWS.premio) {
                                premioLordo = (BigDecimal) fmt_IT.parseObject(polizzaWS.premio.toString())
                            } else {
                                premioLordo = 0.0
                            }
                            if (polizzaWS.imponibile) {
                                premioImpo = (BigDecimal) fmt_IT.parseObject(polizzaWS.imponibile.toString())
                            } else {
                                premioImpo = 0.0
                            }
                            if (polizzaWS.valoreAssicurato) {
                                valoreAssicurato = polizzaWS.valoreAssicurato
                            } else {
                                valoreAssicurato = 0.0
                            }
                            def dataInizio = polizzaWS.inizio
                            if (dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            } else {
                                dataInizio = ""
                            }
                            def dataFine = polizzaWS.fine
                            if (dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            } else {
                                dataFine = ""
                            }
                            def regtarga=/^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$/
                            def targa=polizzaWS.targa.toString().trim()
                            def result=""
                            if (targa.toString().trim()!='' && targa.toString().trim()!=null){
                                if(!targa.matches("^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}")){
                                    logg = new Log(parametri: "la targa non corrisponde  ${targa}", operazione: "invio OFSGEN", pagina: "LISTA POLIZZE RCA")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    result = db.firstRow("select polizze.annualita, rinnovi.provincia, rinnovi.provv_gm, polizze.versione_tariffa from polizze join rinnovi on polizze.idpolizze=rinnovi.idpolizze where telaio='${polizzaWS.telaio}'  order by rinnovi.idpolizze desc")

                                }else{
                                    logg = new Log(parametri: "la targa  corrisponde  ${targa}", operazione: "invio OFSGEN", pagina: "LISTA POLIZZE RCA")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    result = db.firstRow("select polizze.annualita, rinnovi.provincia, rinnovi.provv_gm, polizze.versione_tariffa from polizze join rinnovi on polizze.idpolizze=rinnovi.idpolizze where targa='${polizzaWS.targa}' and telaio='${polizzaWS.telaio}'  order by rinnovi.idpolizze desc")

                                }
                            }else{
                                logg = new Log(parametri: "non è stata inserita nessuna targa ", operazione: "invio OFSGEN", pagina: "LISTA POLIZZE RCA")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                result = db.firstRow("select polizze.annualita, rinnovi.provincia, rinnovi.provv_gm, polizze.versione_tariffa from polizze join rinnovi on polizze.idpolizze=rinnovi.idpolizze where telaio='${polizzaWS.telaio}'  order by rinnovi.idpolizze desc")
                            }
                            //println "sono arrivata qui"
                            //result = db.firstRow("select polizze.annualita, rinnovi.provincia, rinnovi.provv_gm, polizze.versione_tariffa from polizze join rinnovi on polizze.idpolizze=rinnovi.idpolizze where (targa='${polizzaWS.targa}' or telaio='${polizzaWS.telaio}') and cliente like '%${polizzaWS.cliente}%' order by rinnovi.idpolizze desc")
                            if (result) {
                                logg = new Log(parametri: "result ${result}", operazione: "invio OFSGEN", pagina: "LISTA POLIZZE RCA")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                def versione=result[3]
                                //println "versione $versione"
                                if(versione==1){
                                    programma = "Flex Protection Rinnovi"

                                }else{
                                    programma = "Flex Protection Rinnovi 2017"

                                }
                                venditore = ""
                                dealer = ""
                                codDealer = ""
                                satellitare = "N"
                                zonaTariffaria = ZoneTarif.findByProvincia(result[1]).zona
                                tariffa = ""
                                rinnovo = "1"
                                nuovo = "RINNOVO"
                                provvVendi = ""
                                provvDealer = ""
                                provvGMF = result[2]
                                logg = new Log(parametri: "provv gmf  ${provvGMF} --> $noPolizza", operazione: "invio GMFGEN", pagina: "LISTA POLIZZE CASH")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                //provvGMF=provvGMF.toString().replace(".",",")
                                provvGMF = new BigDecimal(provvGMF)
                                provvGMF = provvGMF.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                logg = new Log(parametri: "provv gmf dopo conversione -->  ${provvGMF} --> $noPolizza", operazione: "invio GMFGEN", pagina: "LISTA POLIZZE CASH")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                //provvGMF=fmt_IT.format(provvGMF)
                                account = ""
                                datapagamento = polizzaWS.datacomunicaz
                                if (datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                } else {
                                    datapagamento = ""
                                }
                                finanziata = "N"
                                totProvv = provvGMF
                                ritenuta = 0.046 * (totProvv)
                                netPayable = premioLordo - (provvGMF)
                                if (*//*polizzaWS.statodslip != "0" &&*//* polizzaWS.statodslip != "V") {

                                    if (polizzaWS.statodslip != "S") {
                                        if (polizzaWS.statodslip == "E") {
                                            rimborso = "Y"
                                            def codOperazione = "E"
                                            polizzaRinn = []
                                            polizzaRinn = IAssicurWebService.getPolizzeRinnoviperAnnulla(polizzaWS.noPolizza).polizze
                                            isRinnovo = true
                                            def polizza = ""
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            //premioLordo = rispostawe.premioLordo
                                            //premioImpo = rispostawe.premioImpo
                                            //provvDealer = rispostawe.provvDealer
                                            //provvVendi = rispostawe.provvVendi
                                            //provvGMF = rispostawe.provvGMF
                                            if (rispostawe.premioLordo) {//(BigDecimal) decimalFormat.parse(
                                                rispostawe.premioLordo=rispostawe.premioLordo.toString().replace(".",",")
                                                premioLordo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioLordo.toString())
                                                premioLordo=premioLordo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioLordo = 0.0
                                            }
                                            if (rispostawe.premioImpo) {
                                                rispostawe.premioImpo=rispostawe.premioImpo.toString().replace(".",",")
                                                premioImpo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioImpo.toString())
                                                premioImpo=premioImpo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioImpo = 0.0
                                            }
                                            if (rispostawe.provvDealer) {
                                                rispostawe.provvDealer=rispostawe.provvDealer.toString().replace(".",",")
                                                provvDealer = (BigDecimal) fmt_IT.parseObject(rispostawe.provvDealer.toString())
                                                provvDealer=provvDealer.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvDealer = 0.0
                                            }
                                            if (rispostawe.provvVendi) {
                                                rispostawe.provvVendi=rispostawe.provvVendi.toString().replace(".",",")
                                                provvVendi = (BigDecimal) fmt_IT.parseObject(rispostawe.provvVendi.toString())
                                                provvVendi=provvVendi.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvVendi = 0.0
                                            }
                                            if (rispostawe.provvGMF) {
                                                rispostawe.provvGMF=rispostawe.provvGMF.toString().replace(".",",")
                                                provvGMF = (BigDecimal) fmt_IT.parseObject(rispostawe.provvGMF.toString())
                                                provvGMF=provvGMF.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvGMF = 0.0
                                            }
                                        }
                                        else if (polizzaWS.statodslip == "F") {
                                            rimborso = "Y"
                                            def codOperazione = "F"
                                            polizzaRinn = []
                                            polizzaRinn = IAssicurWebService.getPolizzeRinnoviperAnnulla(polizzaWS.noPolizza).polizze
                                            isRinnovo = true
                                            def polizza = ""
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            //premioLordo = rispostawe.premioLordo
                                            //premioImpo = rispostawe.premioImpo
                                            //provvDealer = rispostawe.provvDealer
                                            //provvVendi = rispostawe.provvVendi
                                            //provvGMF = rispostawe.provvGMF
                                            if (rispostawe.premioLordo) {//(BigDecimal) decimalFormat.parse(
                                                rispostawe.premioLordo=rispostawe.premioLordo.toString().replace(".",",")
                                                premioLordo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioLordo.toString())
                                                premioLordo=premioLordo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioLordo = 0.0
                                            }
                                            if (rispostawe.premioImpo) {
                                                rispostawe.premioImpo=rispostawe.premioImpo.toString().replace(".",",")
                                                premioImpo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioImpo.toString())
                                                premioImpo=premioImpo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioImpo = 0.0
                                            }
                                            if (rispostawe.provvDealer) {
                                                rispostawe.provvDealer=rispostawe.provvDealer.toString().replace(".",",")
                                                provvDealer = (BigDecimal) fmt_IT.parseObject(rispostawe.provvDealer.toString())
                                                provvDealer=provvDealer.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvDealer = 0.0
                                            }
                                            if (rispostawe.provvVendi) {
                                                rispostawe.provvVendi=rispostawe.provvVendi.toString().replace(".",",")
                                                provvVendi = (BigDecimal) fmt_IT.parseObject(rispostawe.provvVendi.toString())
                                                provvVendi=provvVendi.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvVendi = 0.0
                                            }
                                            if (rispostawe.provvGMF) {
                                                rispostawe.provvGMF=rispostawe.provvGMF.toString().replace(".",",")
                                                provvGMF = (BigDecimal) fmt_IT.parseObject(rispostawe.provvGMF.toString())
                                                provvGMF=provvGMF.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvGMF = 0.0
                                            }
                                        }
                                        else if (polizzaWS.statodslip == "G") {
                                            rimborso = "Y"
                                            def codOperazione = "G"
                                            polizzaRinn = []
                                            polizzaRinn = IAssicurWebService.getPolizzeRinnoviperAnnulla(polizzaWS.noPolizza).polizze
                                            isRinnovo = true
                                            def polizza = ""
                                            def rispostawe = IAssicurWebService.chiamataWSRecessoAnnulla(polizza, polizzaWS, coperturaR, isRinnovo, polizzaRinn, codOperazione)
                                            //premioLordo = rispostawe.premioLordo
                                            //premioImpo = rispostawe.premioImpo
                                            //provvDealer = rispostawe.provvDealer
                                            //provvVendi = rispostawe.provvVendi
                                            //provvGMF = rispostawe.provvGMF
                                            if (rispostawe.premioLordo) {//(BigDecimal) decimalFormat.parse(
                                                rispostawe.premioLordo=rispostawe.premioLordo.toString().replace(".",",")
                                                premioLordo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioLordo.toString())
                                                premioLordo=premioLordo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioLordo = 0.0
                                            }
                                            if (rispostawe.premioImpo) {
                                                rispostawe.premioImpo=rispostawe.premioImpo.toString().replace(".",",")
                                                premioImpo = (BigDecimal) fmt_IT.parseObject(rispostawe.premioImpo.toString())
                                                premioImpo=premioImpo.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                premioImpo = 0.0
                                            }
                                            if (rispostawe.provvDealer) {
                                                rispostawe.provvDealer=rispostawe.provvDealer.toString().replace(".",",")
                                                provvDealer = (BigDecimal) fmt_IT.parseObject(rispostawe.provvDealer.toString())
                                                provvDealer=provvDealer.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvDealer = 0.0
                                            }
                                            if (rispostawe.provvVendi) {
                                                rispostawe.provvVendi=rispostawe.provvVendi.toString().replace(".",",")
                                                provvVendi = (BigDecimal) fmt_IT.parseObject(rispostawe.provvVendi.toString())
                                                provvVendi=provvVendi.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvVendi = 0.0
                                            }
                                            if (rispostawe.provvGMF) {
                                                rispostawe.provvGMF=rispostawe.provvGMF.toString().replace(".",",")
                                                provvGMF = (BigDecimal) fmt_IT.parseObject(rispostawe.provvGMF.toString())
                                                provvGMF=provvGMF.setScale(2, BigDecimal.ROUND_HALF_EVEN)
                                            } else {
                                                provvGMF = 0.0
                                            }
                                        }
                                        netPayable = premioLordo - (provvGMF)
                                        ritenuta = 0.046 * (provvGMF)
                                        totProvv = provvGMF
                                    } else if(polizzaWS.statodslip == "S") {
                                        rimborso = "Y"
                                        netPayable = premioLordo + (provvGMF)
                                        ritenuta = ritenuta.negate()
                                        totProvv = totProvv.negate()
                                        premioLordo = premioLordo.negate()
                                        premioImpo = premioImpo.negate()
                                        //provvDealer = provvDealer.negate()
                                        //provvVendi = provvVendi.negate()
                                        provvGMF = provvGMF.negate()
                                    }
                                }
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza = TipoPolizza.RINNOVI
                                polizzaTotali.noPolizza = polizzaWS.noPolizza
                                polizzaTotali.programma = programma
                                polizzaTotali.assicurato = polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore = venditore
                                polizzaTotali.concessionario = dealer
                                polizzaTotali.dataPagamento = datapagamento
                                polizzaTotali.dataCopertura = dataInizio
                                polizzaTotali.dataImmisione = dataInizio
                                polizzaTotali.dataScadenza = dataFine
                                polizzaTotali.valoreAssicurato = valoreAssicurato
                                polizzaTotali.cod_Conc = codDealer
                                polizzaTotali.cod_vend = ""
                                polizzaTotali.targa = polizzaWS.targa
                                polizzaTotali.telaio = polizzaWS.telaio
                                polizzaTotali.pacchetto = pacchetto
                                polizzaTotali.satellitare = satellitare
                                polizzaTotali.livello = polizzaWS.anni
                                polizzaTotali.zonaTariffaria = zonaTariffaria
                                polizzaTotali.tariffa = tariffa
                                polizzaTotali.rinnovo = rinnovo
                                polizzaTotali.nuovo = nuovo
                                polizzaTotali.durata = polizzaWS.durata
                                polizzaTotali.premioLordo = premioLordo
                                polizzaTotali.premioImpo = premioImpo
                                polizzaTotali.provvVendi = provvVendi
                                polizzaTotali.provvDealer = provvDealer
                                polizzaTotali.provvGmfi = provvGMF
                                polizzaTotali.provvAddFee = ""
                                polizzaTotali.provvAgg = ""
                                polizzaTotali.account = account
                                polizzaTotali.finanziata = finanziata
                                polizzaTotali.rimborso = rimborso
                                polizzaTotali.noteRimb = ""
                                polizzaTotali.totProvv = totProvv
                                polizzaTotali.ritenuta = ritenuta
                                polizzaTotali.netPayable = netPayable

                                row {
                                    cell(polizzaWS.noPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataInizio)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizzaWS.targa)
                                    cell(polizzaWS.telaio)
                                    cell(pacchetto)
                                    cell(satellitare)
                                    cell(result[0])
                                    cell(zonaTariffaria)
                                    cell(tariffa)
                                    cell(rinnovo)
                                    cell(nuovo)
                                    cell(polizzaWS.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(account)
                                    cell(finanziata)
                                    cell(rimborso)
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                }
                                for (int i = 0; i < 36; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali = [:]
                            }

                        }
                    }

                }*/
                sheet("Retail - Polizze") {
                    row(style: "header") {
                        cell("Tipo Polizza")
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Modello")
                        cell("Categoria Modello")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                    }
                    if (allPolizze) {
                        def rPolizze = allPolizze.polizze
                        rPolizze.each { polizzaWS ->
                            row {
                                cell(polizzaWS.tipoPolizza)
                                cell(polizzaWS.noPolizza)
                                cell(polizzaWS.programma)
                                cell(polizzaWS.assicurato)
                                cell(polizzaWS.venditore)
                                cell(polizzaWS.concessionario)
                                cell(polizzaWS.datapagamento)
                                cell(polizzaWS.dataCopertura)
                                cell(polizzaWS.dataImmisione)
                                cell(polizzaWS.dataScadenza)
                                cell(polizzaWS.valoreAssicurato)
                                cell(polizzaWS.cod_Conc)
                                cell(polizzaWS.cod_Vend)
                                cell(polizzaWS.targa)
                                cell(polizzaWS.modello)
                                cell(polizzaWS.categoriaveicolo)
                                cell(polizzaWS.telaio)
                                cell(polizzaWS.pacchetto)
                                cell(polizzaWS.satellitare)
                                cell(polizzaWS.livello)
                                cell(polizzaWS.zonaTariffaria)
                                cell(polizzaWS.tariffa)
                                cell(polizzaWS.rinnovo)
                                cell(polizzaWS.nuovo)
                                cell(polizzaWS.durata)
                                cell(polizzaWS.premioLordo)
                                cell(polizzaWS.premioImpo)
                                cell(polizzaWS.provvVendi)
                                cell(polizzaWS.provvDealer)
                                cell(polizzaWS.provvGmfi)
                                cell(polizzaWS.provvAddFee)
                                cell(polizzaWS.provvAgg)
                                cell(polizzaWS.account)
                                cell(polizzaWS.finanziata)
                                cell(polizzaWS.rimborso)
                                cell(polizzaWS.noteRimb)
                                cell(polizzaWS.totProvv)
                                cell(polizzaWS.ritenuta)
                                cell(polizzaWS.netPayable)

                            }
                            for (int i = 0; i < 40; i++) {
                                sheet.autoSizeColumn(i)
                            }
                        }
                    }

                }
            }
        }
        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        /**qui faccio la modifica**/
        if(stream){
            def caricamentoftp=ftpService.uploadFileOFSGEN(fileName, stream)
            /***INVIO LA MAIL A mach1 con il riepilogo dei certificati generati e i campioni dei certificati*/
            def inviaMailCaricamento = mailService.invioMailOFSGENMach1(caricamentoftp, fileName)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto della creazione del report OFSGEN \u00E8 stata inviata", operazione: "invio OFSGEN", pagina: "LISTA POLIZZE RCA")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                rispostaCert=true
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail del report OFSGEN", operazione: "invio OFSGEN", pagina: "LISTA POLIZZE RCA")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Non e' stato possibile inviare la mail di certificati falliti \n ${inviaMailCaricamento}"
                rispostaCert=true

            }
        }else{
            rispostaCert=false
        }


        //return stream.toByteArray()
        //println rispostaCert
        return rispostaCert
    }
    def fileGMFGENPAI_(def polizzeWS) {
        def logg
        def  fmt_IT = NumberFormat.getNumberInstance(Locale.ITALIAN)
        //fmt_IT.setParseBigDecimal(true)
        fmt_IT.setMaximumFractionDigits(2)
        def programma, venditore, dealer, datapagamento, codDealer, pacchetto, satellitare, zonaTariffaria, tariffa, rinnovo,nuovo, finanziata, provvVendi, provvDealer, provvGMF, netPayable,rimborso,account, totProvv, ritenuta, coperturaR
        def polizzeLeasing= [polizze: []]
        def polizzeFinanziate= [polizze: []]
        def allPolizze = [polizze: []]
        def polizzaTotali=[:]
        polizzeWS.each { polizzaWS ->
            if(polizzaWS.statodslip !="V"){
                if( polizzaWS.ramo.contains("K/52")  || polizzaWS.ramo.contains("K/39")  ){
                    polizzeLeasing.polizze << polizzaWS
                } else if( polizzaWS.ramo.contains("K/51") || polizzaWS.ramo.contains("K/40") ){
                    polizzeFinanziate.polizze << polizzaWS
                }
            }
        }
        def polizzeF=polizzeFinanziate.polizze
        def polizzeL=polizzeLeasing.polizze

        def wb = Stopwatch.log {
            ExcelBuilder.create {
                style("header") {
                    background bisque
                    font {
                        bold(true)
                    }
                }
                sheet("Retail - Finanziate") {
                    row(style: "header") {
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")

                    }
                    if(polizzeF){
                        polizzeF.each { polizzaWS ->
                            def noPolizza = polizzaWS.noPolizza
                            def telaio = polizzaWS.telaio
                            telaio=telaio.toString().trim()
                            satellitare="N"
                            def premioLordo, premioImpo,valoreAssicurato

                            def polizza = PolizzaPaiPag.findByTelaio(telaio)
                            def dataInizio = polizzaWS.inizio
                            if(dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            }else{dataInizio=""}
                            def dataFine = polizzaWS.fine
                            if(dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            }else{dataFine=""}
                            if (polizza) {
                                valoreAssicurato= polizza.valoreAssicurato
                                pacchetto=polizza.coperturaRichiesta
                                programma = "SempreaBordo"
                                venditore = polizza.venditore.toUpperCase()
                                dealer = polizza.dealer.ragioneSocialeD.toUpperCase()
                                codDealer = polizza.dealer.codice.toString().trim().padLeft(4,"0")
                                valoreAssicurato= polizza.valoreAssicurato
                                premioImpo=polizza.premioImponibile
                                premioLordo=polizza.premioLordo
                                provvVendi=polizza.provvVenditore
                                provvDealer=polizza.provvDealer
                                provvGMF=polizza.provvGmfi
                                account=polizza.dealer.salesSpecialist.toUpperCase()
                                def nomeSales= account.split(" ")
                                def salesSpec
                                if(nomeSales.size()==2){
                                    salesSpec=nomeSales[1] + " "+nomeSales[0]
                                }else if(nomeSales.size()>2){
                                    for ( int ind=0; ind<nomeSales.size();ind++) {
                                        salesSpec+=nomeSales[ind]+" "
                                    }
                                }
                                datapagamento = polizzaWS.datacompagnia
                                if(datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                }else{datapagamento=""}
                                finanziata="S"
                                totProvv=provvDealer+provvVendi+provvGMF
                                ritenuta=0.046*(provvDealer+provvVendi+provvGMF)
                                netPayable=premioLordo-(provvDealer+provvVendi+provvGMF)

                                def numPolizza=""
                                numPolizza=polizza.noPolizza
                                def tipoPolizza=TipoPolizza.FINANZIATE
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza=TipoPolizza.FINANZIATE
                                polizzaTotali.noPolizza=numPolizza
                                polizzaTotali.programma=programma
                                polizzaTotali.assicurato=polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore=venditore
                                polizzaTotali.concessionario=dealer
                                polizzaTotali.dataPagamento=datapagamento
                                polizzaTotali.dataCopertura=dataInizio
                                polizzaTotali.dataImmisione=dataInizio
                                polizzaTotali.dataScadenza=dataFine
                                polizzaTotali.valoreAssicurato=valoreAssicurato
                                polizzaTotali.cod_Conc=codDealer
                                polizzaTotali.cod_vend=""
                                polizzaTotali.targa=polizzaWS.targa
                                polizzaTotali.telaio=polizzaWS.telaio
                                polizzaTotali.pacchetto=pacchetto
                                polizzaTotali.satellitare=satellitare
                                polizzaTotali.livello=polizzaWS.anni
                                polizzaTotali.zonaTariffaria=""
                                polizzaTotali.tariffa=""
                                polizzaTotali.rinnovo=""
                                polizzaTotali.nuovo=""
                                polizzaTotali.durata=polizzaWS.durata
                                polizzaTotali.premioLordo=premioLordo
                                polizzaTotali.premioImpo=premioImpo
                                polizzaTotali.provvVendi=provvVendi
                                polizzaTotali.provvDealer= provvDealer
                                polizzaTotali.provvGmfi= provvGMF
                                polizzaTotali.provvAddFee= ""
                                polizzaTotali.provvAgg= ""
                                polizzaTotali.account=salesSpec
                                polizzaTotali.finanziata=finanziata
                                polizzaTotali.rimborso=""
                                polizzaTotali.noteRimb=""
                                polizzaTotali.totProvv=totProvv
                                polizzaTotali.ritenuta=ritenuta
                                polizzaTotali.netPayable=netPayable
                                row {
                                    cell(numPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataInizio)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizzaWS.targa)
                                    cell(polizzaWS.telaio)
                                    cell(pacchetto)
                                    cell(satellitare)
                                    cell(polizzaWS.anni)
                                    cell("")
                                    cell("")
                                    cell("")
                                    cell("")
                                    cell(polizzaWS.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(salesSpec)
                                    cell(finanziata)
                                    cell("")
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                }
                                for (int i = 0; i < 36; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali=[:]
                            }
                        }
                    }else{
                        logg =new Log(parametri: "non ci sono polizze finanziate ", operazione: "generaFileGMFGEN", pagina: "POLIZZE PAI PAGAMENTO")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }
                sheet("Retail - Leasing") {
                    row(style: "header") {
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")

                    }
                    if(polizzeL){
                        polizzeL.each { polizzaWS ->
                            def noPolizza = polizzaWS.noPolizza
                            def telaio = polizzaWS.telaio
                            satellitare="N"
                            def premioLordo, premioImpo,valoreAssicurato

                            def polizza = PolizzaPaiPag.findByTelaio(telaio.toString().trim())
                            def dataInizio = polizzaWS.inizio
                            if(dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            }else{dataInizio=""}
                            def dataFine = polizzaWS.fine
                            if(dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            }else{dataFine=""}
                            if (polizza) {
                                pacchetto=polizza.coperturaRichiesta
                                programma = "SempreaBordo"
                                venditore = polizza.venditore.toUpperCase()
                                dealer = polizza.dealer.ragioneSocialeD.toUpperCase()
                                codDealer = polizza.dealer.codice.toString().trim().padLeft(4,"0")
                                premioLordo=polizza.premioLordo
                                premioImpo=polizza.premioImponibile
                                provvVendi=polizza.provvVenditore
                                provvDealer=polizza.provvDealer
                                provvGMF=polizza.provvGmfi
                                valoreAssicurato= polizza.valoreAssicurato
                                account=polizza.dealer.salesSpecialist.toUpperCase()
                                def nomeSales= account.split(" ")
                                def salesSpec
                                //println nomeSales
                                if(nomeSales.size()==2){
                                    salesSpec=nomeSales[1] + " "+nomeSales[0]
                                }else if(nomeSales.size()>2){
                                    for ( int ind=0; ind<nomeSales.size();ind++) {
                                        salesSpec+=nomeSales[ind]+" "
                                    }
                                }
                                datapagamento = polizzaWS.datacompagnia
                                if(datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                }else{datapagamento=""}
                                finanziata="S"
                                totProvv=provvDealer+provvVendi+provvGMF
                                ritenuta=0.046*(provvDealer+provvVendi+provvGMF)
                                netPayable=premioLordo-(provvDealer+provvVendi+provvGMF)
                                valoreAssicurato= polizza.valoreAssicurato
                                def numPolizza=polizza.noPolizza
                                def tipoPolizza=TipoPolizza.LEASING
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza=TipoPolizza.LEASING
                                polizzaTotali.noPolizza=numPolizza
                                polizzaTotali.programma=programma
                                polizzaTotali.assicurato=polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore=venditore
                                polizzaTotali.concessionario=dealer
                                polizzaTotali.dataPagamento=datapagamento
                                polizzaTotali.dataCopertura=dataInizio
                                polizzaTotali.dataImmisione=dataInizio
                                polizzaTotali.dataScadenza=dataFine
                                polizzaTotali.valoreAssicurato=valoreAssicurato
                                polizzaTotali.cod_Conc=codDealer
                                polizzaTotali.cod_vend=""
                                polizzaTotali.targa=polizzaWS.targa
                                polizzaTotali.telaio=polizzaWS.telaio
                                polizzaTotali.pacchetto=pacchetto
                                polizzaTotali.satellitare=satellitare
                                polizzaTotali.livello=polizzaWS.anni
                                polizzaTotali.zonaTariffaria=""
                                polizzaTotali.tariffa=""
                                polizzaTotali.rinnovo=""
                                polizzaTotali.nuovo=""
                                polizzaTotali.durata=polizzaWS.durata
                                polizzaTotali.premioLordo=premioLordo
                                polizzaTotali.premioImpo=premioImpo
                                polizzaTotali.provvVendi=provvVendi
                                polizzaTotali.provvDealer= provvDealer
                                polizzaTotali.provvGmfi= provvGMF
                                polizzaTotali.provvAddFee= ""
                                polizzaTotali.provvAgg= ""
                                polizzaTotali.account=salesSpec
                                polizzaTotali.finanziata=finanziata
                                polizzaTotali.rimborso=""
                                polizzaTotali.noteRimb=""
                                polizzaTotali.totProvv=totProvv
                                polizzaTotali.ritenuta=ritenuta
                                polizzaTotali.netPayable=netPayable

                                row {
                                    cell(numPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataInizio)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizzaWS.targa)
                                    cell(polizzaWS.telaio)
                                    cell(pacchetto)
                                    cell(satellitare)
                                    cell(polizzaWS.anni)
                                    cell("")
                                    cell("")
                                    cell("")
                                    cell("")
                                    cell(polizzaWS.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(salesSpec)
                                    cell(finanziata)
                                    cell("")
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                }
                                for (int i = 0; i < 36; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali=[:]
                            }

                        }
                    }else{
                        logg =new Log(parametri: "non ci sono polizze leasing ", operazione: "generaFileGMFGEN", pagina: "POLIZZE PAI PAGAMENTO")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }
                sheet("Retail - Polizze") {
                    row(style: "header") {
                        cell("Tipo Polizza")
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                    }
                    if(allPolizze){
                        def rPolizze=allPolizze.polizze
                        rPolizze.each { polizzaWS ->
                            row {
                                cell(polizzaWS.tipoPolizza)
                                cell(polizzaWS.noPolizza)
                                cell(polizzaWS.programma)
                                cell(polizzaWS.assicurato)
                                cell(polizzaWS.venditore)
                                cell(polizzaWS.concessionario)
                                cell(polizzaWS.datapagamento)
                                cell(polizzaWS.dataCopertura)
                                cell(polizzaWS.dataCopertura)
                                cell(polizzaWS.dataScadenza)
                                cell(polizzaWS.valoreAssicurato)
                                cell(polizzaWS.cod_Conc)
                                cell(polizzaWS.cod_Vend)
                                cell(polizzaWS.targa)
                                cell(polizzaWS.telaio)
                                cell(polizzaWS.pacchetto)
                                cell(polizzaWS.satellitare)
                                cell(polizzaWS.livello)
                                cell(polizzaWS.zonaTariffaria)
                                cell(polizzaWS.tariffa)
                                cell(polizzaWS.rinnovo)
                                cell(polizzaWS.nuovo)
                                cell(polizzaWS.durata)
                                cell(polizzaWS.premioLordo)
                                cell(polizzaWS.premioImpo)
                                cell(polizzaWS.provvVendi)
                                cell(polizzaWS.provvDealer)
                                cell(polizzaWS.provvGmfi)
                                cell(polizzaWS.provvAddFee)
                                cell(polizzaWS.provvAgg)
                                cell(polizzaWS.account)
                                cell(polizzaWS.finanziata)
                                cell(polizzaWS.rimborso)
                                cell(polizzaWS.noteRimb)
                                cell(polizzaWS.totProvv)
                                cell(polizzaWS.ritenuta)
                                cell(polizzaWS.netPayable)

                            }
                            for (int i = 0; i < 36; i++) {
                                sheet.autoSizeColumn(i)
                            }
                        }
                    }

                }
            }
        }
        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        return stream.toByteArray()
    }
    def fileGMFGENPAIJOB(def polizzeWS, def mese) {
        def rispostaCert=false
        def logg
        def  fmt_IT = NumberFormat.getNumberInstance(Locale.ITALIAN)
        def fileName="GMFEN_PAI${new Date().format("ddMMyyyy")}.xlsx"
        //fmt_IT.setParseBigDecimal(true)
        fmt_IT.setMaximumFractionDigits(2)
        def programma, venditore, dealer, datapagamento, codDealer, pacchetto, satellitare, zonaTariffaria, tariffa, rinnovo,nuovo, finanziata, provvVendi, provvDealer, provvGMF, netPayable,rimborso,account, totProvv, ritenuta, coperturaR
        def polizzeLeasing= [polizze: []]
        def polizzeFinanziate= [polizze: []]
        def allPolizze = [polizze: []]
        def polizzaTotali=[:]
        polizzeWS.each { polizzaWS ->
            if(polizzaWS.statodslip !="V"){
                if( polizzaWS.ramo.contains("K/52")  || polizzaWS.ramo.contains("K/39")  ){
                    polizzeLeasing.polizze << polizzaWS
                } else if( polizzaWS.ramo.contains("K/51") || polizzaWS.ramo.contains("K/40") ){
                    polizzeFinanziate.polizze << polizzaWS
                }
            }
        }
        def polizzeF=polizzeFinanziate.polizze
        def polizzeL=polizzeLeasing.polizze

        def wb = Stopwatch.log {
            ExcelBuilder.create {
                style("header") {
                    background bisque
                    font {
                        bold(true)
                    }
                }
                sheet("Retail - Finanziate") {
                    row(style: "header") {
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")

                    }
                    if(polizzeF){
                        polizzeF.each { polizzaWS ->
                            def noPolizza = polizzaWS.noPolizza
                            def telaio = polizzaWS.telaio
                            telaio=telaio.toString().trim()
                            satellitare="N"
                            def premioLordo, premioImpo,valoreAssicurato

                            def polizza = PolizzaPaiPag.findByTelaio(telaio)
                            def dataInizio = polizzaWS.inizio
                            if(dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            }else{dataInizio=""}
                            def dataFine = polizzaWS.fine
                            if(dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            }else{dataFine=""}
                            if (polizza) {
                                valoreAssicurato= polizza.valoreAssicurato
                                pacchetto=polizza.coperturaRichiesta
                                programma = "SempreaBordo"
                                venditore = polizza.venditore.toUpperCase()
                                dealer = polizza.dealer.ragioneSocialeD.toUpperCase()
                                codDealer = polizza.dealer.codice.toString().trim().padLeft(4,"0")
                                valoreAssicurato= polizza.valoreAssicurato
                                premioImpo=polizza.premioImponibile
                                premioLordo=polizza.premioLordo
                                provvVendi=polizza.provvVenditore
                                provvDealer=polizza.provvDealer
                                provvGMF=polizza.provvGmfi
                                account=polizza.dealer.salesSpecialist.toUpperCase()
                                def nomeSales= account.split(" ")
                                def salesSpec
                                if(nomeSales.size()==2){
                                    salesSpec=nomeSales[1] + " "+nomeSales[0]
                                }else if(nomeSales.size()>2){
                                    for ( int ind=0; ind<nomeSales.size();ind++) {
                                        salesSpec+=nomeSales[ind]+" "
                                    }
                                }
                                datapagamento = polizzaWS.datacompagnia
                                if(datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                }else{datapagamento=""}
                                finanziata="S"
                                totProvv=provvDealer+provvVendi+provvGMF
                                ritenuta=0.046*(provvDealer+provvVendi+provvGMF)
                                netPayable=premioLordo-(provvDealer+provvVendi+provvGMF)

                                def numPolizza=""
                                numPolizza=polizza.noPolizza
                                def tipoPolizza=TipoPolizza.FINANZIATE
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza=TipoPolizza.FINANZIATE
                                polizzaTotali.noPolizza=numPolizza
                                polizzaTotali.programma=programma
                                polizzaTotali.assicurato=polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore=venditore
                                polizzaTotali.concessionario=dealer
                                polizzaTotali.dataPagamento=datapagamento
                                polizzaTotali.dataCopertura=dataInizio
                                polizzaTotali.dataImmisione=dataInizio
                                polizzaTotali.dataScadenza=dataFine
                                polizzaTotali.valoreAssicurato=valoreAssicurato
                                polizzaTotali.cod_Conc=codDealer
                                polizzaTotali.cod_vend=""
                                polizzaTotali.targa=polizzaWS.targa
                                polizzaTotali.telaio=polizzaWS.telaio
                                polizzaTotali.pacchetto=pacchetto
                                polizzaTotali.satellitare=satellitare
                                polizzaTotali.livello=polizzaWS.anni
                                polizzaTotali.zonaTariffaria=""
                                polizzaTotali.tariffa=""
                                polizzaTotali.rinnovo=""
                                polizzaTotali.nuovo=""
                                polizzaTotali.durata=polizzaWS.durata
                                polizzaTotali.premioLordo=premioLordo
                                polizzaTotali.premioImpo=premioImpo
                                polizzaTotali.provvVendi=provvVendi
                                polizzaTotali.provvDealer= provvDealer
                                polizzaTotali.provvGmfi= provvGMF
                                polizzaTotali.provvAddFee= ""
                                polizzaTotali.provvAgg= ""
                                polizzaTotali.account=salesSpec
                                polizzaTotali.finanziata=finanziata
                                polizzaTotali.rimborso=""
                                polizzaTotali.noteRimb=""
                                polizzaTotali.totProvv=totProvv
                                polizzaTotali.ritenuta=ritenuta
                                polizzaTotali.netPayable=netPayable
                                row {
                                    cell(numPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataInizio)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizzaWS.targa)
                                    cell(polizzaWS.telaio)
                                    cell(pacchetto)
                                    cell(satellitare)
                                    cell(polizzaWS.anni)
                                    cell("")
                                    cell("")
                                    cell("")
                                    cell("")
                                    cell(polizzaWS.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(salesSpec)
                                    cell(finanziata)
                                    cell("")
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                }
                                for (int i = 0; i < 36; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali=[:]
                            }
                        }
                    }else{
                        logg =new Log(parametri: "non ci sono polizze finanziate ", operazione: "generaFileGMFGEN", pagina: "POLIZZE PAI PAGAMENTO")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }
                sheet("Retail - Leasing") {
                    row(style: "header") {
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")

                    }
                    if(polizzeL){
                        polizzeL.each { polizzaWS ->
                            def noPolizza = polizzaWS.noPolizza
                            def telaio = polizzaWS.telaio
                            satellitare="N"
                            def premioLordo, premioImpo,valoreAssicurato

                            def polizza = PolizzaPaiPag.findByTelaio(telaio.toString().trim())
                            def dataInizio = polizzaWS.inizio
                            if(dataInizio) {
                                new Date().parse("dd/MM/yyyy", dataInizio)
                            }else{dataInizio=""}
                            def dataFine = polizzaWS.fine
                            if(dataFine) {
                                new Date().parse("dd/MM/yyyy", dataFine)
                            }else{dataFine=""}
                            if (polizza) {
                                pacchetto=polizza.coperturaRichiesta
                                programma = "SempreaBordo"
                                venditore = polizza.venditore.toUpperCase()
                                dealer = polizza.dealer.ragioneSocialeD.toUpperCase()
                                codDealer = polizza.dealer.codice.toString().trim().padLeft(4,"0")
                                premioLordo=polizza.premioLordo
                                premioImpo=polizza.premioImponibile
                                provvVendi=polizza.provvVenditore
                                provvDealer=polizza.provvDealer
                                provvGMF=polizza.provvGmfi
                                valoreAssicurato= polizza.valoreAssicurato
                                account=polizza.dealer.salesSpecialist.toUpperCase()
                                def nomeSales= account.split(" ")
                                def salesSpec
                                //println nomeSales
                                if(nomeSales.size()==2){
                                    salesSpec=nomeSales[1] + " "+nomeSales[0]
                                }else if(nomeSales.size()>2){
                                    for ( int ind=0; ind<nomeSales.size();ind++) {
                                        salesSpec+=nomeSales[ind]+" "
                                    }
                                }
                                datapagamento = polizzaWS.datacompagnia
                                if(datapagamento) {
                                    new Date().parse("dd/MM/yyyy", datapagamento)
                                }else{datapagamento=""}
                                finanziata="S"
                                totProvv=provvDealer+provvVendi+provvGMF
                                ritenuta=0.046*(provvDealer+provvVendi+provvGMF)
                                netPayable=premioLordo-(provvDealer+provvVendi+provvGMF)
                                valoreAssicurato= polizza.valoreAssicurato
                                def numPolizza=polizza.noPolizza
                                def tipoPolizza=TipoPolizza.LEASING
                                //salvo la polizza trovata per poi riportarla nell'ultimo foglio
                                polizzaTotali.tipoPolizza=TipoPolizza.LEASING
                                polizzaTotali.noPolizza=numPolizza
                                polizzaTotali.programma=programma
                                polizzaTotali.assicurato=polizzaWS.cliente.toString().toUpperCase()
                                polizzaTotali.venditore=venditore
                                polizzaTotali.concessionario=dealer
                                polizzaTotali.dataPagamento=datapagamento
                                polizzaTotali.dataCopertura=dataInizio
                                polizzaTotali.dataImmisione=dataInizio
                                polizzaTotali.dataScadenza=dataFine
                                polizzaTotali.valoreAssicurato=valoreAssicurato
                                polizzaTotali.cod_Conc=codDealer
                                polizzaTotali.cod_vend=""
                                polizzaTotali.targa=polizzaWS.targa
                                polizzaTotali.telaio=polizzaWS.telaio
                                polizzaTotali.pacchetto=pacchetto
                                polizzaTotali.satellitare=satellitare
                                polizzaTotali.livello=polizzaWS.anni
                                polizzaTotali.zonaTariffaria=""
                                polizzaTotali.tariffa=""
                                polizzaTotali.rinnovo=""
                                polizzaTotali.nuovo=""
                                polizzaTotali.durata=polizzaWS.durata
                                polizzaTotali.premioLordo=premioLordo
                                polizzaTotali.premioImpo=premioImpo
                                polizzaTotali.provvVendi=provvVendi
                                polizzaTotali.provvDealer= provvDealer
                                polizzaTotali.provvGmfi= provvGMF
                                polizzaTotali.provvAddFee= ""
                                polizzaTotali.provvAgg= ""
                                polizzaTotali.account=salesSpec
                                polizzaTotali.finanziata=finanziata
                                polizzaTotali.rimborso=""
                                polizzaTotali.noteRimb=""
                                polizzaTotali.totProvv=totProvv
                                polizzaTotali.ritenuta=ritenuta
                                polizzaTotali.netPayable=netPayable

                                row {
                                    cell(numPolizza)
                                    cell(programma)
                                    cell(polizzaWS.cliente.toString().toUpperCase())
                                    cell(venditore)
                                    cell(dealer)
                                    cell(datapagamento)
                                    cell(dataInizio)
                                    cell(dataInizio)
                                    cell(dataFine)
                                    cell(valoreAssicurato)
                                    cell(codDealer)
                                    cell("")
                                    cell(polizzaWS.targa)
                                    cell(polizzaWS.telaio)
                                    cell(pacchetto)
                                    cell(satellitare)
                                    cell(polizzaWS.anni)
                                    cell("")
                                    cell("")
                                    cell("")
                                    cell("")
                                    cell(polizzaWS.durata)
                                    cell(premioLordo)
                                    cell(premioImpo)
                                    cell(provvVendi)
                                    cell(provvDealer)
                                    cell(provvGMF)
                                    cell("")
                                    cell("")
                                    cell(salesSpec)
                                    cell(finanziata)
                                    cell("")
                                    cell("")
                                    cell(totProvv)
                                    cell(ritenuta)
                                    cell(netPayable)
                                }
                                for (int i = 0; i < 36; i++) {
                                    sheet.autoSizeColumn(i)
                                }
                                allPolizze.polizze << polizzaTotali
                                polizzaTotali=[:]
                            }

                        }
                    }else{
                        logg =new Log(parametri: "non ci sono polizze leasing ", operazione: "generaFileGMFGEN", pagina: "POLIZZE PAI PAGAMENTO")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }
                sheet("Retail - Polizze") {
                    row(style: "header") {
                        cell("Tipo Polizza")
                        cell("Numero Polizza")
                        cell("Programma")
                        cell("Assicurato")
                        cell("Venditore")
                        cell("Concessionario")
                        cell("Data_Pagamento")
                        cell("DataCopertura")
                        cell("DataImmissione")
                        cell("DataScadenza")
                        cell("ValoreAssicurato")
                        cell("Cod_Conc")
                        cell("Cod_Vend")
                        cell("Targa")
                        cell("Telaio")
                        cell("Pacchetto")
                        cell("Satellitare")
                        cell("Livello")
                        cell("ZonaTariffaria")
                        cell("Tariffa")
                        cell("Rinnovo")
                        cell("Nuovo/Rinnovo")
                        cell("Durata")
                        cell("PremioLordo")
                        cell("PremioImponibile")
                        cell("Provv_Venditori")
                        cell("Provv_Concessionari")
                        cell("Provv_GMFI")
                        cell("Provv_AdditionalFee")
                        cell("Provv_Aggiuntive")
                        cell("Account")
                        cell("Finanziata")
                        cell("Rimborso (Y/N)")
                        cell("Note rimborso")
                        cell("TOTALE PROVVIGIONI")
                        cell("RITENUTA")
                        cell("NET PAYABLE")
                    }
                    if(allPolizze){
                        def rPolizze=allPolizze.polizze
                        rPolizze.each { polizzaWS ->
                            row {
                                cell(polizzaWS.tipoPolizza)
                                cell(polizzaWS.noPolizza)
                                cell(polizzaWS.programma)
                                cell(polizzaWS.assicurato)
                                cell(polizzaWS.venditore)
                                cell(polizzaWS.concessionario)
                                cell(polizzaWS.datapagamento)
                                cell(polizzaWS.dataCopertura)
                                cell(polizzaWS.dataCopertura)
                                cell(polizzaWS.dataScadenza)
                                cell(polizzaWS.valoreAssicurato)
                                cell(polizzaWS.cod_Conc)
                                cell(polizzaWS.cod_Vend)
                                cell(polizzaWS.targa)
                                cell(polizzaWS.telaio)
                                cell(polizzaWS.pacchetto)
                                cell(polizzaWS.satellitare)
                                cell(polizzaWS.livello)
                                cell(polizzaWS.zonaTariffaria)
                                cell(polizzaWS.tariffa)
                                cell(polizzaWS.rinnovo)
                                cell(polizzaWS.nuovo)
                                cell(polizzaWS.durata)
                                cell(polizzaWS.premioLordo)
                                cell(polizzaWS.premioImpo)
                                cell(polizzaWS.provvVendi)
                                cell(polizzaWS.provvDealer)
                                cell(polizzaWS.provvGmfi)
                                cell(polizzaWS.provvAddFee)
                                cell(polizzaWS.provvAgg)
                                cell(polizzaWS.account)
                                cell(polizzaWS.finanziata)
                                cell(polizzaWS.rimborso)
                                cell(polizzaWS.noteRimb)
                                cell(polizzaWS.totProvv)
                                cell(polizzaWS.ritenuta)
                                cell(polizzaWS.netPayable)

                            }
                            for (int i = 0; i < 36; i++) {
                                sheet.autoSizeColumn(i)
                            }
                        }
                    }

                }
            }
        }
        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        def caricamentoftp=ftpService.uploadFileGMFGEN(fileName, stream)
        /***INVIO LA MAIL A mach1 con il riepilogo dei certificati generati e i campioni dei certificati*/
        def inviaMailCaricamento = mailService.invioMailGMFGENMach1(caricamentoftp, fileName)
        if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
            logg = new Log(parametri: "la mail con il riassunto della creazione del report GMFGEN \u00E8 stata inviata", operazione: "invio GMFGEN", pagina: "LISTA POLIZZE PAI")
            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            rispostaCert=true
        } else {
            logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail del report GMFGEN", operazione: "invio GMFGEN", pagina: "LISTA POLIZZE PAI")
            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "Non e' stato possibile inviare la mail di certificati falliti \n ${inviaMailCaricamento}"
            rispostaCert=true

        }

        //return stream.toByteArray()
        return rispostaCert
    }
}
