package certificati

import cashopel.polizze.Documenti
import cashopel.polizze.DocumentiSalesSpecialist
import cashopel.polizze.Polizza
import cashopel.polizze.TipoDoc
import cashopel.polizze.TipoPolizza
import cashopel.utenti.Log
import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.PdfStamper

import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream

class generaCertService {
    def mailService
    def ftpService
    def IAssicurWebService
    void generaCertificato( def dataGeneraCert){
        def logg
        def riassunto = [], errore=[]
        def fileNameCert, fileContentCert, fileNameWelcome, fileContentWelcome, documentofinale
        def totale = 0
        def contDocs = 0
        def rispostaMail = "Riassunto invio certificati polizze :\r\n"
        def fileNameRisposta = "riassunto_Cert_FinLeasing_${new Date().format("yyyyMMddHms")}_.txt"
        def fileNameAllCert = "FINANZIATE_${new Date().format("ddMMyyyyHms")}.zip"
        def streamCertMach1 = new ByteArrayOutputStream()
        def zipCertificati = new ZipOutputStream(streamCertMach1)
        def streamallCert = new ByteArrayOutputStream()
        def zipAllCertificati = new ZipOutputStream(streamallCert)
        def resultwebS = IAssicurWebService.getPolizzeFinLea(dataGeneraCert)
        def polizze = resultwebS.polizze
        if (polizze) {
            logg =new Log(parametri: "No. polizzze trovate: ${polizze.size()}", operazione: "generazione certificati Fin/Leasing", pagina: "Polizze Fin/Leasing")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            polizze.each { polizza ->
                def codPolizza = polizza.codice
                codPolizza = codPolizza.toString().replaceAll("/", "-")
                def noPolizza = polizza.noPolizza
                def documentiCaricati = Documenti.findByNoPolizza(noPolizza.toString().trim())
                def polizzaPortale = Polizza.findByNoPolizza(noPolizza)
                if (!documentiCaricati && polizzaPortale) {
                    mailService.generaCertIASSPDF(polizza).each { fileName, fileContent ->
                        fileNameCert = fileName
                        fileContentCert = fileContent
                    }
                    mailService.generaWelcomeIASSPDF(polizza).each { fileName, fileContent ->
                        fileNameWelcome = fileName
                        fileContentWelcome = fileContent
                    }
                    //creo un unico pdf
                    def streams = []
                    def baos2 = new ByteArrayOutputStream(fileContentCert.length)
                    baos2.write(fileContentCert, 0, fileContentCert.length)
                    def baos1 = new ByteArrayOutputStream(fileContentWelcome.length);
                    baos1.write(fileContentWelcome, 0, fileContentWelcome.length)
                    streams += baos1
                    streams += baos2
                    documentofinale =mailService.mergePdf(streams).toByteArray()
                    //salvo il file nel BD
                    def documenti = new Documenti()
                    if (polizzaPortale.tipoPolizza == TipoPolizza.FINANZIATE) {
                        documenti.tipo = TipoDoc.FINANZIATE
                    } else if (polizzaPortale.tipoPolizza == TipoPolizza.LEASING) {
                        documenti.tipo = TipoDoc.LEASING
                    }
                    documenti.fileName = "${fileNameCert}"
                    documenti.fileContent = documentofinale
                    documenti.noPolizza = "${noPolizza}"
                    documenti.codIAssicur = "${codPolizza}"
                    documenti.cliente = "${polizza.cliente}"
                    documenti.indirizzo = "${polizza.indirizzo}"
                    documenti.cap = "${polizza.cap}"
                    documenti.localita = "${polizza.localita}"
                    documenti.provincia = "${polizza.provincia}"
                    if (documenti.save(flush: true)) {
                        logg =new Log(parametri: "${documenti.fileName} generato correttamente", operazione: "generazione certificati Fin/Leasing", pagina: "Polizza Fin/Leasing")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        riassunto.add( " ${documenti.fileName} generato correttamente")
                        totale++
                        zipAllCertificati.putNextEntry(new ZipEntry(documenti.fileName))
                        zipAllCertificati.write(documenti.fileContent)
                        if (contDocs <= 3) {
                            contDocs++
                            zipCertificati.putNextEntry(new ZipEntry(documenti.fileName))
                            zipCertificati.write(documenti.fileContent)
                            logg = new Log(parametri: "il certificato della polizza ${polizza.noPolizza} e' stato generato e inviato per controllo a MACH1", operazione: "generazione certificati polizze  Polizza Fin/Leasing", pagina: "Polizza  Fin/Leasing")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }

                    } else {
                        logg =new Log(parametri: "Errore generazione certificato ${documenti.errors}", operazione: "generazione certificati Fin/Leasing", pagina: "Polizza Fin/Leasing")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        errore.add(" Errore generazione certificato  ${documenti.errors}")
                    }

                } else if (documentiCaricati) {
                    logg = new Log(parametri: "il documento ${noPolizza.toString().trim()} esiste gia' nel sistema", operazione: "generazione certificati Fin/Leasing", pagina: "Polizza  Fin/Leasing")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    errore.add("il documento ${noPolizza.toString().trim()} esiste gia' nel sistema")
                } else if (!polizzaPortale) {
                    logg = new Log(parametri: "la polizza non esiste nel portale ${noPolizza.toString().trim()} controllare", operazione: "generazione certificati Fin/Leasing", pagina: "Polizza  Fin/Leasing")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    errore.add("la polizza non esiste nel portale ${noPolizza.toString().trim()} controllare")
                }
            }
        }else{
            logg = new Log(parametri: "non ho trovato la polizza ", operazione: "generazione certificati Fin/Leasing", pagina: "Polizza  Fin/Leasing")
            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        }
        zipCertificati.close()
        streamCertMach1.close()
        zipAllCertificati.close()
        streamallCert.close()
        if (totale > 0) {
            logg = new Log(parametri: "sono stati generati ${totale} certificati", operazione: "generazione certificati Fin/Leasing", pagina: "Polizza Fin/Leasing")
            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            riassunto.add("sono stati generati  ${totale} certificati")
        }
        if (riassunto.size() > 0) {
            riassunto.collect { commento ->
                rispostaMail = rispostaMail + " " + commento + "\r\n"
            }.grep().join("\n")
            /**CARICO I CERTIFICATI NELLA CARTELLA ftp**/
            def inputzipCert = new ByteArrayInputStream(streamallCert.toByteArray())
            ftpService.uploadFileCertMACH1(fileNameAllCert, inputzipCert)
        }
        if(errore.size()>0){
            errore.collect { commento ->
                rispostaMail = rispostaMail + " " + commento + "\r\n"
            }.grep().join("\n")

        }
        if(errore.size()>0 && !(riassunto.size()>0)){
            /***INVIO LA MAIL A mach1 con il riepilogo dei certificati generati e i campioni dei certificati*/
            def inviaMailCaricamento = mailService.invioMailCaricamentoMach1(rispostaMail, fileNameRisposta, false, null)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto della creazione dei certificati \u00E8 stata inviata", operazione: "invio riassunto certificati Fin/Leasing generati", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                //flash.certificati = ["la mail con il riassunto della creazione dei certificati \u00E8 stata inviata"]
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail di certificati falliti", operazione: "invio riassunto certificati Fin/Leasing generati", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                //flash.certificati = ["Non \u00E8 stato possibile inviare la mail di certificati falliti \n"]
                println "Non e' stato possibile inviare la mail di certificati falliti \n ${inviaMailCaricamento}"
            }
        }else {
            /***INVIO LA MAIL A mach1 con il riepilogo dei certificati generati e i campioni dei certificati*/
            def inviaMailCaricamento = mailService.invioMailCaricamentoMach1(rispostaMail, fileNameRisposta, false, streamCertMach1)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto della creazione dei certificati \u00E8 stata inviata", operazione: "invio riassunto certificati Fin/Leasing generati", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail di certificati falliti", operazione: "invio riassunto certificati Fin/Leasing generati", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Non e' stato possibile inviare la mail di certificati falliti \n ${inviaMailCaricamento}"
            }
        }

       // return riassunto
    }
    def inviaMailDefinitivo(boolean cash){
        def logg
        def riassunto = [], errore=[]
        def streamRiassunti = new ByteArrayOutputStream()
        def zipRiassunti = new ZipOutputStream(streamRiassunti)
        def rispostaMail="Riassunto invio certificati polizze :\r\n"
        def fileNameRisposta
        def documenti
        if(cash){
            logg =new Log(parametri: "lancio l'invio dei certificati CASH", operazione: "invio certificati CASH ai dealer e venditori", pagina: "Polizza CASH")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            documenti=Documenti.createCriteria().list(){
                eq "inviatoMail", false
                eq "tipo", TipoDoc.CASH
            }
            fileNameRisposta = "risposta_Invio_MailCertCASH_${new Date().format("yyyyMMddHms")}_.txt"
        }else{
            logg =new Log(parametri: "lancio l'invio dei certificati FIN/LEASING", operazione: "invio certificati FIN/LEASING ai dealer e venditori", pagina: "Polizza FIN/LEASING")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            documenti=Documenti.createCriteria().list(){
                eq "inviatoMail", false
                or{
                    eq "tipo", TipoDoc.FINANZIATE
                    eq "tipo", TipoDoc.LEASING
                }

            }
            cash=false
            fileNameRisposta = "risposta_Invio_MailCertFINLEASING_${new Date().format("yyyyMMddHms")}_.txt"
        }
        if (documenti.size() > 0) {
            documenti.each { documento ->
                def polizza=Polizza.findByNoPolizza(documento.noPolizza)
                if (polizza) {
                    def inviaMailCertificati= mailService.invioMailCertificato(polizza,documento)
                    if(inviaMailCertificati.contains("queued")||inviaMailCertificati.contains("sent") ||inviaMailCertificati.contains("scheduled") ){
                        logg =new Log(parametri: "il certificato della polizza ${polizza.noPolizza} e' stato inviato al dealer ${polizza.dealer.ragioneSocialeD} e al sales specialist ${polizza.dealer.salesSpecialist}", operazione: "invio certificati CASH ai delaer e venditori", pagina: "Polizza CASH/FINLEASING")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        riassunto.add("il certificato della polizza ${polizza.noPolizza} e' stato inviato al dealer ${polizza.dealer.ragioneSocialeD} e al sales specialist ${polizza.dealer.salesSpecialist}")
                        documento.inviatoMail=true
                        if(!documento.save(flush: true)){
                            logg =new Log(parametri: "Non e' stato possibile aggiornare lo stato del documento relativo alla polizza ${polizza.noPolizza}", operazione: "invio certificati CASH ai delaer e venditori", pagina: "Polizza CASH/FINLEASING")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            riassunto.add("Non e' stato possibile aggiornare lo stato del documento relativo alla polizza ${polizza.noPolizza}")
                        }else{
                            // println "il documento relativo alla polizza ${polizza.noPolizza} e' stato aggiornato"
                        }
                    }else{
                        logg =new Log(parametri: "Non e' stato possibile inviare il certificato per la polizza ${polizza.noPolizza} al dealer ${polizza.dealer.ragioneSocialeD} e al sales specialist ${polizza.dealer.salesSpecialist} \n ${inviaMailCertificati}", operazione: "invio certificati CASH ai delaer e venditori", pagina: "Polizza CASH/FINLEASING")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        riassunto.add("Non e' stato possibile inviare il certificato per la polizza ${polizza.noPolizza} al dealer ${polizza.dealer.ragioneSocialeD} e al sales specialist ${polizza.dealer.salesSpecialist} ")
                    }
                }else{
                    logg =new Log(parametri: "Non ci sono polizze con certificati da inviare per email", operazione: "invio certificati CASH ai delaer e venditori", pagina: "Polizza CASH")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    errore.add("Non ci sono polizze con certificati da inviare per email")
                }
            }
        }

        if (riassunto.size()>0){
            riassunto.collect{ commento ->
                rispostaMail =rispostaMail +" "+ commento+"\r\n"
            }.grep().join("\n")
            zipRiassunti.putNextEntry(new ZipEntry(fileNameRisposta))
            zipRiassunti.write(rispostaMail.bytes)
            def inviaMailCaricamento=mailService.invioMailCaricamento(rispostaMail, fileNameRisposta,cash)
            if(inviaMailCaricamento.contains("queued")||inviaMailCaricamento.contains("sent") ||inviaMailCaricamento.contains("scheduled") ){
                logg =new Log(parametri: "la mail con il riassunto dell'invio delle mail dei certificati e' stata inviata a MACH1", operazione: "invio mail riassunto dei certificati CASH inviati ai delaer e venditori", pagina: "Polizza CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }else{
                logg =new Log(parametri: "Non e' stato possibile inviare la mail di carimenti falliti \n ${inviaMailCaricamento}", operazione: "invio mail riassunto dei certificati CASH inviati ai delaer e venditori", pagina: "CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
        }
    }
    def genCertPaiPAG(def dataGeneraCert){
        def logg
        try {
            def resultwebS = IAssicurWebService.getPolizzePAIPAG(dataGeneraCert)
            def polizze = resultwebS.polizze
            def fileNameCert, fileContentCert, fileNameWelcome, fileContentWelcome, documentofinale, codPolizza, fileNameAxa,fileContentAxa, documentofinaleAxa
            if(polizze){
                def totale=0
                def totaleErr=0
                polizze.each { polizza ->
                    def ramo= polizza.ramo
                    codPolizza=polizza.codice
                    codPolizza=codPolizza.toString().replaceAll("/","-")
                    def noPolizza=polizza.noPolizza
                    def telaio=polizza.telaio
                    def piva=polizza.cFiscale
                    def certificatoAXA=DocumentiSalesSpecialist.findByPIvaAndTelaio("${piva}","${telaio}")
                    if(certificatoAXA){
                        fileNameAxa="NR POLIZZA ${polizza.noPolizza} - ${polizza.cliente}.pdf"
                        fileContentAxa=certificatoAXA.fileContent
                        //CERCO IL CERTIFICATO GIà GENERATO per polizza normale
                        def polizzaNoPAI=Polizza.findByTelaio(telaio.toString().trim())
                        def numeroPoli=null
                        def documentoCVT=null
                        def documentiPoli=Documenti.findByTipoAndNoPolizza("PAIPAG","${noPolizza}")
                        if(polizzaNoPAI){
                            numeroPoli=polizzaNoPAI.noPolizza
                            documentoCVT=Documenti.findByNoPolizza(numeroPoli)
                            if (!documentiPoli  && documentoCVT){
                                logg =new Log(parametri: "ho trovato il documento da modificare ${documentoCVT.fileName}", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                fileNameCert = documentoCVT.fileName
                                documentofinale = documentoCVT.fileContent
                                InputStream myInputStream = new ByteArrayInputStream(documentofinale)
                                def reader = new PdfReader(myInputStream)
                                def output = new ByteArrayOutputStream()
                                def stamper = new PdfStamper(reader, output)
                                reader.selectPages("3,4")
                                stamper.close()
                                //creo un unico pdf
                                def streams = []
                                //creo nuova welcome letter
                                mailService.generaWelcomeIASSPAIFINPDF(polizza).each {fileName, fileContent ->
                                    fileNameWelcome = fileName
                                    fileContentWelcome = fileContent
                                }
                                def baos1 =  new ByteArrayOutputStream(fileContentWelcome.length);
                                baos1.write(fileContentWelcome, 0, fileContentWelcome.length)
                                def baos2 = output
                                def baos3 = new ByteArrayOutputStream(fileContentAxa.length)
                                baos3.write(fileContentAxa, 0, fileContentAxa.length)
                                streams += baos1
                                streams += baos2
                                streams += baos3
                                documentofinaleAxa=mailService.mergePdf(streams).toByteArray()
                                def documenti=new Documenti()
                                documenti.tipo=TipoDoc.PAIPAG
                                documenti.inviatoMail=false
                                documenti.fileName="${fileNameAxa}"
                                documenti.noPolizza="${noPolizza}"
                                documenti.codIAssicur="${codPolizza}"
                                documenti.cliente="${polizza.cliente}"
                                documenti.indirizzo="${polizza.indirizzo}"
                                documenti.cap="${polizza.cap}"
                                documenti.localita="${polizza.localita}"
                                documenti.provincia="${polizza.provincia}"
                                documenti.fileContent=documentofinaleAxa
                                documentoCVT.caricatoRotoMail=true

                                if(documenti.save(flush:true)){
                                    logg =new Log(parametri: "il documento apartenente alla polizza ${noPolizza.toString().trim()} e' stato generato", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    certificatoAXA.dataCaricamento=new Date()
                                    if(!certificatoAXA.save(flush: true)){
                                        logg =new Log(parametri: "non e' stato possibile aggiornare lo stato del documento con telaio ${certificatoAXA.telaio}", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        totaleErr++
                                    }else{
                                        logg =new Log(parametri: "documento con telaio ${certificatoAXA.telaio} aggiornato", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        if(!documentoCVT.save(flush: true)){
                                            logg =new Log(parametri: "non e' stato possibile aggiornare il documento  ${documentoCVT.noPolizza} per non essere inviato per posta", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            totaleErr++
                                        }else{
                                            logg =new Log(parametri: "certificato per polizza CVT ${documentoCVT.noPolizza} messo per non essere inviato per posta  e aggiornato", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            totale++
                                        }
                                    }
                                }else{
                                    logg =new Log(parametri: "Errore salvataggio documento ${documenti.errors}", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    totaleErr++
                                }

                            }else if (documentiPoli){
                                logg =new Log(parametri: "essite gia' il documento per questa polizza ${noPolizza}", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                totaleErr++
                            }else if(!documentoCVT){
                                //println "DATI POLIZZA $polizza"
                                /*def polizzaPAI=PolizzaPaiPag.findByNoPolizza(polizza.noPolizza)
                                if(polizzaPAI){
                                    polizza.valoreAssicurato=polizzaPAI.valoreAssicurato
                                    polizza.immatricolazione=polizzaPAI.dataImmatricolazione
                                }*/
                                logg =new Log(parametri: "non ho trovato il documento per la polizza CVT ${numeroPoli} controllare", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                /*polizza.valoreAssicurato=polizzaNoPAI.valoreAssicurato
                                polizza.immatricolazione=polizzaNoPAI.dataImmatricolazione
                                mailService.generaCertIASSPDF(polizza).each{fileName, fileContent ->
                                    fileNameCert = fileName
                                    fileContentCert = fileContent
                                }
                                mailService.generaWelcomeIASSPAIFINPDF(polizza).each {fileName, fileContent ->
                                    fileNameWelcome = fileName
                                    fileContentWelcome = fileContent
                                }
                                println fileNameCert
                                println fileNameWelcome
                                //creo un unico pdf
                                def streams = []
                                def baos2 = new ByteArrayOutputStream(fileContentCert.length)
                                baos2.write(fileContentCert, 0, fileContentCert.length)
                                def baos1 = new ByteArrayOutputStream(fileContentWelcome.length);
                                baos1.write(fileContentWelcome, 0, fileContentWelcome.length)
                                streams += baos1
                                streams += baos2
                                documentofinale=mailService.mergePdf(streams).toByteArray()
                                def documenti=new Documenti()
                                documenti.tipo=TipoDoc.PAIPAG
                                documenti.inviatoMail=false
                                documenti.fileName="${fileNameAxa}"
                                documenti.noPolizza="${noPolizza}"
                                documenti.codIAssicur="${codPolizza}"
                                documenti.cliente="${polizza.cliente}"
                                documenti.indirizzo="${polizza.indirizzo}"
                                documenti.cap="${polizza.cap}"
                                documenti.localita="${polizza.localita}"
                                documenti.provincia="${polizza.provincia}"
                                documenti.fileContent=documentofinale

                                if(documenti.save(flush:true)){
                                    logg =new Log(parametri: "il documento della polizza ${noPolizza.toString().trim()} cvt +  pai  e' stato generato", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    certificatoAXA.dataCaricamento=new Date()
                                    if(!certificatoAXA.save(flush: true)){
                                        logg =new Log(parametri: "non e' stato possibile aggiornare lo stato del documento con telaio ${certificatoAXA.telaio}", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }else{
                                        logg =new Log(parametri: "documento con telaio ${certificatoAXA.telaio} aggiornato", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }
                                    //creo un unico pdf
                                     streams = []
                                    baos1 = new ByteArrayOutputStream(documentofinale.length)
                                    baos1.write(documentofinale, 0, documentofinale.length)
                                    baos2 = new ByteArrayOutputStream(fileContentAxa.length)
                                    baos2.write(fileContentAxa, 0, fileContentAxa.length)
                                    streams += baos1
                                    streams += baos2
                                    documentofinaleAxa=mailService.mergePdf(streams).toByteArray()
                                    documenti.fileContent=documentofinaleAxa

                                    if(documenti.save(flush:true)){
                                        logg =new Log(parametri: "il documento apartenente alla polizza ${noPolizza.toString().trim()} e' stato generato", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        certificatoAXA.dataCaricamento=new Date()
                                        if(!certificatoAXA.save(flush: true)){
                                            logg =new Log(parametri: "non e' stato possibile aggiornare lo stato del documento con telaio ${certificatoAXA.telaio}", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }else{
                                            logg =new Log(parametri: "documento con telaio ${certificatoAXA.telaio} aggiornato", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        }
                                    }else{
                                        logg =new Log(parametri: "Errore salvataggio documento ${documenti.errors}", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        //errorePratica.add(" Errore salvataggio documento  ${documenti.errors}")
                                    }
                                    totale++
                                }else{
                                    logg =new Log(parametri: "Errore salvataggio documento ${documenti.errors}", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    //errorePratica.add(" Errore salvataggio documento  ${documenti.errors}")
                                }*/
                            }
                        }else{
                            logg =new Log(parametri: "la polizza CVT per questo telaio  ${telaio} non essiste nel sistema", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            if(!documentiPoli){
                                mailService.generaWelcomeIASSPAIPDF(polizza).each {fileName, fileContent ->
                                    fileNameWelcome = fileName
                                    fileContentWelcome = fileContent
                                }
                                //creo un unico pdf
                                def streams = []
                                def baos2 = new ByteArrayOutputStream(fileContentAxa.length)
                                baos2.write(fileContentAxa, 0, fileContentAxa.length)
                                def baos1 = new ByteArrayOutputStream(fileContentWelcome.length);
                                baos1.write(fileContentWelcome, 0, fileContentWelcome.length)
                                streams += baos1
                                streams += baos2
                                documentofinale=mailService.mergePdf(streams).toByteArray()
                                def documenti=new Documenti()
                                documenti.tipo=TipoDoc.PAIPAG
                                documenti.inviatoMail=false
                                documenti.fileName="${fileNameAxa}"
                                documenti.noPolizza="${noPolizza}"
                                documenti.codIAssicur="${codPolizza}"
                                documenti.cliente="${polizza.cliente}"
                                documenti.indirizzo="${polizza.indirizzo}"
                                documenti.cap="${polizza.cap}"
                                documenti.localita="${polizza.localita}"
                                documenti.provincia="${polizza.provincia}"
                                documenti.fileContent=documentofinale
                                if(documenti.save(flush:true)){
                                    logg =new Log(parametri: "il documento apartenente alla polizza ${noPolizza.toString().trim()} e' stato generato", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    certificatoAXA.dataCaricamento=new Date()
                                    if(!certificatoAXA.save(flush: true)){
                                        logg =new Log(parametri: "non e' stato possibile aggiornare lo stato del documento con telaio ${certificatoAXA.telaio}", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }else{
                                        logg =new Log(parametri: "documento con telaio ${certificatoAXA.telaio} aggiornato", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        totale++
                                    }
                                }else{
                                    logg =new Log(parametri: "Errore salvataggio documento ${documenti.errors}", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }
                            }else{
                                logg =new Log(parametri: "essiste gia' il documento per questa polizza ${noPolizza}", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            }

                        }
                    }else{
                        logg =new Log(parametri: "il documento per questa polizza ${noPolizza.toString().trim()} non e' stato ancora caricato nel sistema", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }
                }
                if(totale>0){
                    logg =new Log(parametri: "sono stati generati  ${totale} certificati", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }
            }else{
                logg =new Log(parametri: "non ci sono polizze da scaricare", operazione: "generazione certificati integrati con AXA", pagina: "JOB CERT AXA")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }

        }catch(e) {
            logg =new Log(parametri: "errore try catch ${e.toString()}", operazione: "generazione certificati PAI PAGAMENTO", pagina: "Polizza PAI PAGAMENTO")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println e.toString()
        }

    }
}
