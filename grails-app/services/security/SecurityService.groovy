package security

import grails.transaction.Transactional
import org.grails.web.util.WebUtils
import cashopel.utenti.Utente

import javax.servlet.http.HttpServletRequest

@Transactional
class SecurityService {

    def passwordEncoder

    Utente getUtente() {
        if(SecurityUtils.anonymous) {
            def username = request.session.getAttribute(CashopelAuthenticationFailureHandler.LAST_USERNAME_KEY)
            if(username) return Utente.findByUsername(username)
        } else {
            def principal = SecurityUtils.principal
            if (principal) return Utente.read(principal.id)
        }
        return null
    }

    Utente getSwitchedUtente() {
        def principal = SecurityUtils.switchedUser
        if(principal) return Utente.read(principal.id)
        else return null
    }

    boolean isLoggedIn() { return SecurityUtils.isLoggedIn() }

    boolean passwordMatches(String password) { return passwordEncoder.matches(password, utente?.password) }

    void reauthenticate(Utente utente) { SecurityUtils.reauthenticate(utente.username, utente.password) }

    private HttpServletRequest getRequest() { return WebUtils.retrieveGrailsWebRequest().currentRequest }
}
