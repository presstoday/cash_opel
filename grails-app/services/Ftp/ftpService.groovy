package Ftp

import cashopel.utenti.Log
import com.jcraft.jsch.ChannelSftp
import com.jcraft.jsch.JSch
import grails.util.Environment
import org.apache.commons.net.ftp.*
import org.apache.commons.net.util.*
import org.grails.web.util.WebUtils
import javax.net.ssl.SSLSocket
import javax.servlet.http.HttpSession

class ftpService {
    //static transactional = false

    def grailsApplication

    def uploadFile(from, String to) {
        def input = null
        if(from instanceof byte[]) input = new ByteArrayInputStream(from)
        else if(input instanceof InputStream) input = from
        else if(from instanceof String) input = new ByteArrayInputStream(from.bytes)
        def client = new FTPSClient("TLS", false) {
            @Override
            protected void _prepareDataSocket_(final Socket socket) throws IOException {
                if(socket instanceof SSLSocket) {
                    def session = ((SSLSocket)_socket_).session
                    def context = session.sessionContext
                    context.sessionCacheSize = 1000
                    try {
                        def sessionHostPortCache = context.getClass().getDeclaredField("sessionHostPortCache")
                        sessionHostPortCache.setAccessible(true)
                        def cache = sessionHostPortCache.get(context)
                        def key = String.format("%s:%s", socket.inetAddress.hostName, String.valueOf(socket.port)).toLowerCase(Locale.ROOT)
                        cache.put(key, session)
                    } catch(e) {println e.toString()}
                }
            }
        }
        client.setTrustManager(TrustManagerUtils.getAcceptAllTrustManager())
        def config = grailsApplication.config.ftp
        try {
            client.connect(config.host, config.port)
            def replyString = client.replyString[0..-2]
            client.login(config.username, config.password)
            def ftpReply = FTPReply.isPositiveCompletion(client.replyCode)
            if(FTPReply.isPositiveCompletion(client.replyCode)) {
                client.changeWorkingDirectory(config.cartella)
                client.enterLocalPassiveMode()
                client.setEnabledSessionCreation(false)
                client.setFileType(FTP.BINARY_FILE_TYPE)
                client.execPBSZ(0)
                client.execPROT("P")
                client.setBufferSize(1024)
                client.storeFile(to, input)
                def replyStore=client.replyString[0..-2]
            } else client.disconnect()
        } catch(e) { println e.toString() }
        finally {
            if(client.isConnected()) {
                try {
                    client.logout()
                    client.disconnect()
                } catch(e) { println "Error disconnecting: ${e.toString()}" }
            }
        }
    }
    def uploadFileRotoDef(String fileName, InputStream fileContent, String fileNameT, InputStream fileTCont) {
        def logg
        try {
            def sftpConfig = grailsApplication.config.ftpRoto
            def client = new JSch()
            def session = client.getSession(sftpConfig.username, sftpConfig.host, sftpConfig.port)
            session.setConfig("StrictHostKeyChecking", "no")
            session.password = sftpConfig.password
            session.connect()
            logg =new Log(parametri: "JSch Session connected to ${session.userName}@${session.host}:${session.port}", operazione: "collegamento server ftp", pagina: "ftp upload CERTIFICATI POSTA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def channel = session.openChannel("sftp") as ChannelSftp
            channel.connect()
            logg =new Log(parametri: "JSch Channel connected", operazione: "collegamento server ftp", pagina: "ftp upload CERTIFICATI POSTA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            try {
                channel.cd("..")
                channel.cd("/home/flussi-nais")
                logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: "modifica cartella", pagina: "ftp upload CERTIFICATI POSTA")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                channel.cd(sftpConfig.cartella)
            }
            catch (e) {
                logg =new Log(parametri: "errore try catch ->${e.toString()}", operazione: "modifica cartella", pagina: "ftp upload CERTIFICATI POSTA")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                //channel.mkdir(sftpConfig.cartella)
                //channel.cd(sftpConfig.cartella)
            }
            logg =new Log(parametri: "change directory to ${channel.pwd()}", operazione: "modifica cartella", pagina: "ftp upload CERTIFICATI POSTA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            channel.put(fileContent, fileName)
            logg =new Log(parametri: "try uploading ${fileName}...", operazione: "caricamento file zip", pagina: "ftp upload CERTIFICATI POSTA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            logg =new Log(parametri: "${fileName} uploaded!", operazione: "caricamento file zip", pagina: "ftp upload CERTIFICATI POSTA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            //creo il nuovo file tipo T
            channel.put(fileTCont, fileNameT)
            logg =new Log(parametri: "try uploading ${fileNameT}...", operazione: "caricamento file T", pagina: "ftp upload CERTIFICATI POSTA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            logg =new Log(parametri: "${fileNameT} uploaded!", operazione: "caricamento file T", pagina: "ftp upload CERTIFICATI POSTA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            channel.disconnect()
            session.disconnect()
            return true
        } catch(e) {
            logg =new Log(parametri: "Error uploading file ${fileName} to sftp: ${e.toString()}", operazione: "FTP UPLOAD", pagina: "ftp upload CERTIFICATI POSTA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            /*mailService.sendErrors(
                    "Errore caricamento ${to}",
                    "Si è verificato un errore durante il caricamento del file ${to} sull'sftp.\\nIn allegato troverete il relativo file.\\n\\nBuona giornata.",
                    [file: from.bytes, fileName: to]
            )*/
            return false
        }
    }
    def uploadFileCertAxa(String fileName, InputStream fileContent) {
        def logg
        try {
            def sftpConfig = grailsApplication.config.ftpCertpai
            def client = new JSch()
            def session = client.getSession(sftpConfig.username, sftpConfig.host, sftpConfig.port)
            session.setConfig("StrictHostKeyChecking", "no")
            session.password = sftpConfig.password
            session.connect()
            logg =new Log(parametri: "JSch Session connected to ${session.userName}@${session.host}:${session.port}", operazione: "collegamento server ftp", pagina: "ftp upload CERTIFICATI PAI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def channel = session.openChannel("sftp") as ChannelSftp
            channel.connect()
            logg =new Log(parametri: "JSch Channel connected", operazione: "collegamento server ftp", pagina: "ftp upload CERTIFICATI PAI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            try {
                channel.cd("..")
                channel.cd("/home/flussi-nais")
                logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: "modifica cartella", pagina: "ftp upload CERTIFICATI PAI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                channel.cd(sftpConfig.cartella)
            }
            catch (e) {
                logg =new Log(parametri: "errore try catch ->${e.toString()}", operazione: "modifica cartella", pagina: "ftp upload CERTIFICATI PAI")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                //channel.mkdir(sftpConfig.cartella)
                //channel.cd(sftpConfig.cartella)
            }
            logg =new Log(parametri: "change directory to ${channel.pwd()}", operazione: "modifica cartella", pagina: "ftp upload CERTIFICATI PAI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            channel.put(fileContent, fileName)
            logg =new Log(parametri: "try uploading ${fileName}...", operazione: "caricamento file zip", pagina: "ftp upload CERTIFICATI PAI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            logg =new Log(parametri: "${fileName} uploaded!", operazione: "caricamento file zip", pagina: "ftp upload CERTIFICATI PAI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            channel.disconnect()
            session.disconnect()
            return true
        } catch(e) {
            logg =new Log(parametri: "Error uploading file ${fileName} to sftp: ${e.toString()}", operazione: "FTP UPLOAD", pagina: "ftp upload CERTIFICATI PAI")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            /*mailService.sendErrors(
                    "Errore caricamento ${to}",
                    "Si è verificato un errore durante il caricamento del file ${to} sull'sftp.\\nIn allegato troverete il relativo file.\\n\\nBuona giornata.",
                    [file: from.bytes, fileName: to]
            )*/
            return false
        }
    }
    def uploadFileCertMACH1(String fileName, InputStream fileContent) {
        def logg
        try {
            def sftpConfig = grailsApplication.config.ftpCertMACH1
            def client = new JSch()
            def session = client.getSession(sftpConfig.username, sftpConfig.host, sftpConfig.port)
            session.setConfig("StrictHostKeyChecking", "no")
            session.password = sftpConfig.password
            session.connect()
            logg =new Log(parametri: "JSch Session connected to ${session.userName}@${session.host}:${session.port}", operazione: "collegamento server ftp", pagina: "ftp upload CERTIFICATI MACH1")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def channel = session.openChannel("sftp") as ChannelSftp
            channel.connect()
            logg =new Log(parametri: "JSch Channel connected", operazione: "collegamento server ftp", pagina: "ftp upload CERTIFICATI MACH1")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            try {
                channel.cd("..")
                channel.cd("/home/flussi-nais")
                logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: "modifica cartella", pagina: "ftp upload CERTIFICATI MACH1")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                channel.cd(sftpConfig.cartella)
            }
            catch (e) {
                logg =new Log(parametri: "errore try catch ->${e.toString()}", operazione: "modifica cartella", pagina: "ftp upload CERTIFICATI MACH1")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                //channel.mkdir(sftpConfig.cartella)
                //channel.cd(sftpConfig.cartella)
            }
            logg =new Log(parametri: "change directory to ${channel.pwd()}", operazione: "modifica cartella", pagina: "ftp upload CERTIFICATI MACH1")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            channel.put(fileContent, fileName)
            logg =new Log(parametri: "try uploading ${fileName}...", operazione: "caricamento file zip", pagina: "ftp upload CERTIFICATI MACH1")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            logg =new Log(parametri: "${fileName} uploaded!", operazione: "caricamento file zip", pagina: "ftp upload CERTIFICATI MACH1")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            channel.disconnect()
            session.disconnect()
            return true
        } catch(e) {
            logg =new Log(parametri: "Error uploading file ${fileName} to sftp: ${e.toString()}", operazione: "FTP UPLOAD", pagina: "ftp upload CERTIFICATI MACH1")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            /*mailService.sendErrors(
                    "Errore caricamento ${to}",
                    "Si è verificato un errore durante il caricamento del file ${to} sull'sftp.\\nIn allegato troverete il relativo file.\\n\\nBuona giornata.",
                    [file: from.bytes, fileName: to]
            )*/
            return false
        }
    }
    def uploadFileGMFGEN(String fileName, def fileContent) {
        def logg
        try {
            def inputzipGMFGEN=new ByteArrayInputStream(fileContent.toByteArray())
            def sftpConfig = grailsApplication.config.ftpGMFGEN
            def client = new JSch()
            def session = client.getSession(sftpConfig.username, sftpConfig.host, sftpConfig.port)
            session.setConfig("StrictHostKeyChecking", "no")
            session.password = sftpConfig.password
            session.connect()
            logg =new Log(parametri: "JSch Session connected to ${session.userName}@${session.host}:${session.port}", operazione: "collegamento server ftp", pagina: "ftp upload GMFGEN")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def channel = session.openChannel("sftp") as ChannelSftp
            channel.connect()
            logg =new Log(parametri: "JSch Channel connected", operazione: "collegamento server ftp", pagina: "ftp upload GMFGEN")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            try {
                channel.cd("..")
                channel.cd("/home/flussi-nais")
                logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: "modifica cartella", pagina: "ftp upload GMFGEN")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(Environment.current == Environment.PRODUCTION) {
                    channel.cd(sftpConfig.cartella)
                }else{
                    channel.cd("FLUSSI cashOpel/TESTPDF")
                }
            }
            catch (e) {
                logg =new Log(parametri: "errore try catch ->${e.toString()}", operazione: "modifica cartella", pagina: "ftp upload GMFGEN")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                //channel.mkdir(sftpConfig.cartella)
                //channel.cd(sftpConfig.cartella)
            }
            logg =new Log(parametri: "change directory to ${channel.pwd()}", operazione: "modifica cartella", pagina: "ftp upload GMFGEN")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            channel.put(inputzipGMFGEN, fileName)
            logg =new Log(parametri: "try uploading ${fileName}...", operazione: "caricamento file zip", pagina: "ftp upload GMFGEN")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            logg =new Log(parametri: "${fileName} uploaded!", operazione: "caricamento file zip", pagina: "ftp upload GMFGEN")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            channel.disconnect()
            session.disconnect()
            return true
        } catch(e) {
            logg =new Log(parametri: "Error uploading file ${fileName} to sftp: ${e.toString()}", operazione: "FTP UPLOAD", pagina: "ftp upload GMFGEN")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            /*mailService.sendErrors(
                    "Errore caricamento ${to}",
                    "Si è verificato un errore durante il caricamento del file ${to} sull'sftp.\\nIn allegato troverete il relativo file.\\n\\nBuona giornata.",
                    [file: from.bytes, fileName: to]
            )*/
            return false
        }
    }
    def uploadFileOFSGEN(String fileName, def fileContent) {
        def logg
        try {
            def inputzipGMFGEN=new ByteArrayInputStream(fileContent.toByteArray())
            def sftpConfig = grailsApplication.config.ftpGMFGEN
            def client = new JSch()
            def session = client.getSession(sftpConfig.username, sftpConfig.host, sftpConfig.port)
            session.setConfig("StrictHostKeyChecking", "no")
            session.password = sftpConfig.password
            session.connect()
            logg =new Log(parametri: "JSch Session connected to ${session.userName}@${session.host}:${session.port}", operazione: "collegamento server ftp", pagina: "ftp upload OFSGEN")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def channel = session.openChannel("sftp") as ChannelSftp
            channel.connect()
            logg =new Log(parametri: "JSch Channel connected", operazione: "collegamento server ftp", pagina: "ftp upload OFSGEN")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            try {
                channel.cd("..")
                channel.cd("/home/flussi-nais")
                logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: "modifica cartella", pagina: "ftp upload OFSGEN")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                if(Environment.current == Environment.PRODUCTION) {
                    channel.cd("RCA-OPEL/OFSGEN")
                }else{
                    channel.cd("RCA-OPEL/old")
                }
            }
            catch (e) {
                logg =new Log(parametri: "errore try catch ->${e.toString()}", operazione: "modifica cartella", pagina: "ftp upload OFSGEN")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                //channel.mkdir(sftpConfig.cartella)
                //channel.cd(sftpConfig.cartella)
            }
            logg =new Log(parametri: "change directory to ${channel.pwd()}", operazione: "modifica cartella", pagina: "ftp upload OFSGEN")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            channel.put(inputzipGMFGEN, fileName)
            logg =new Log(parametri: "try uploading ${fileName}...", operazione: "caricamento file zip", pagina: "ftp upload OFSGEN")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            logg =new Log(parametri: "${fileName} uploaded!", operazione: "caricamento file zip", pagina: "ftp upload OFSGEN")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            channel.disconnect()
            session.disconnect()
            return true
        } catch(e) {
            logg =new Log(parametri: "Error uploading file ${fileName} to sftp: ${e.toString()}", operazione: "FTP UPLOAD", pagina: "ftp upload OFSGEN")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            /*mailService.sendErrors(
                    "Errore caricamento ${to}",
                    "Si è verificato un errore durante il caricamento del file ${to} sull'sftp.\\nIn allegato troverete il relativo file.\\n\\nBuona giornata.",
                    [file: from.bytes, fileName: to]
            )*/
            return false
        }
    }
    def uploadFileDLMach1(String fileName, def fileContent) {
        def logg
        try {
            def inputCSV=new ByteArrayInputStream(fileContent.toByteArray())
            def sftpConfig = grailsApplication.config.ftpGMFGEN
            def client = new JSch()


            def session = client.getSession(sftpConfig.username, sftpConfig.host, sftpConfig.port)
            session.setConfig("StrictHostKeyChecking", "no")
            session.password = sftpConfig.password
            session.connect()
            logg =new Log(parametri: "JSch Session connected to ${session.userName}@${session.host}:${session.port}", operazione: "collegamento server ftp", pagina: "ftp upload CSV RCA DL")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def channel = session.openChannel("sftp") as ChannelSftp
            channel.connect()
            logg =new Log(parametri: "JSch Channel connected", operazione: "collegamento server ftp", pagina: "ftp upload CSV RCA DL")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            try {
                channel.cd("..")
                channel.cd("/home/flussi-nais")
                logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: "modifica cartella", pagina: "ftp upload CSV RCA DL")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(Environment.current == Environment.PRODUCTION) {
                    channel.cd("RCA-OPEL")

                }else{
                    channel.cd("RCA-OPEL/old")

                }

            }
            catch (e) {
                logg =new Log(parametri: "errore try catch ->${e.toString()}", operazione: "modifica cartella", pagina: "ftp upload CSV RCA DL")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                //channel.mkdir(sftpConfig.cartella)
                //channel.cd(sftpConfig.cartella)
            }
            logg =new Log(parametri: "change directory to ${channel.pwd()}", operazione: "modifica cartella", pagina: "ftp upload CSV RCA DL")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            channel.put(inputCSV, fileName)
            logg =new Log(parametri: "try uploading ${fileName}...", operazione: "caricamento file zip", pagina: "ftp upload CSV RCA DL")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            logg =new Log(parametri: "${fileName} uploaded!", operazione: "caricamento file zip", pagina: "ftp upload CSV RCA DL")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            channel.disconnect()
            session.disconnect()


            return true
        } catch(e) {
            logg =new Log(parametri: "Error uploading file ${fileName} to sftp: ${e.toString()}", operazione: "FTP UPLOAD", pagina: "ftp upload CSV RCA DL")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            /*mailService.sendErrors(
                    "Errore caricamento ${to}",
                    "Si è verificato un errore durante il caricamento del file ${to} sull'sftp.\\nIn allegato troverete il relativo file.\\n\\nBuona giornata.",
                    [file: from.bytes, fileName: to]
            )*/
            return false
        }
    }
    def uploadFileDLcompagnia(String fileName, def fileContent) {
        def logg
        try {
            def inputCSV=new ByteArrayInputStream(fileContent.toByteArray())
            def sftpConfig = grailsApplication.config.ftpGMFGEN
            def client = new JSch()
                def username='SftpUserOPEL'
                def password='Op.Fpbs.2016!'
                def host='sftp.prontosinistro.it'
                //session.password = sftpConfig.password
                def session = client.getSession(username, host, 22)
                session.setConfig("StrictHostKeyChecking", "no")
                session.password = "Op.Fpbs.2016!"
                session.connect()
                logg =new Log(parametri: "JSch Session connected to ${session.userName}@${session.host}:${session.port}", operazione: "collegamento server ftp compagnia", pagina: "ftp upload CSV RCA DL")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                def channel = session.openChannel("sftp") as ChannelSftp
                channel.connect()
                logg =new Log(parametri: "JSch Channel connected", operazione: "collegamento server ftp compagnia", pagina: "ftp upload CSV RCA DL")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                try {
                    if(Environment.current == Environment.PRODUCTION) {
                        channel.cd("/FLOTTE")
                    }else{
                        channel.cd("/FLOTTETEST")

                    }
                    logg =new Log(parametri: "Current sftp directory: ${channel.pwd()}", operazione: "modifica cartella", pagina: "ftp upload CSV RCA DL")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    //channel.cd("RCA-OPEL")
                }
                catch (e) {
                    logg =new Log(parametri: "errore try catch ->${e.toString()}", operazione: "modifica cartella", pagina: "ftp upload CSV RCA DL")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    //channel.mkdir(sftpConfig.cartella)
                    //channel.cd(sftpConfig.cartella)
                }
                channel.put(inputCSV, fileName)
                logg =new Log(parametri: "try uploading ${fileName}...", operazione: "caricamento file zip", pagina: "ftp upload CSV RCA DL")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                logg =new Log(parametri: "${fileName} uploaded!", operazione: "caricamento file zip", pagina: "ftp upload CSV RCA DL")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                channel.disconnect()
                session.disconnect()


            return true
        } catch(e) {
            logg =new Log(parametri: "Error uploading file ${fileName} to sftp: ${e.toString()}", operazione: "FTP UPLOAD", pagina: "ftp upload CSV RCA DL")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            /*mailService.sendErrors(
                    "Errore caricamento ${to}",
                    "Si è verificato un errore durante il caricamento del file ${to} sull'sftp.\\nIn allegato troverete il relativo file.\\n\\nBuona giornata.",
                    [file: from.bytes, fileName: to]
            )*/
            return false
        }
    }
    private HttpSession getSession() { WebUtils.retrieveGrailsWebRequest().getSession() }
}
