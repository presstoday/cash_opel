package caricamento

import cashopel.polizze.CodProdotti
import cashopel.polizze.Polizza
import cashopel.polizze.PolizzaRCA
import cashopel.polizze.StatoPolizza
import cashopel.polizze.TipoCliente
import cashopel.polizze.TipoPolizza
import cashopel.polizze.TipoVeicolo
import cashopel.polizze.ZoneTarif
import cashopel.utenti.Dealer
import cashopel.utenti.Log
import groovy.time.TimeCategory

import java.util.regex.Pattern


class CaricamentiService {
    static transactional = false
    def tracciatiService
    def DLWebService
    def mailService

    def caricapratiche(def myInputStream,  def polizzeRCACVT){

        def speciale=false
        def errorepraticheCVT=[], errorePraticheRCA=[], errorePraticheCVTRCA=[], flussiPAI=[]
        def praticheRCA=[], praticheCVT=[], praticheCVT_RCA=[]
        def dealerMancante=[]
        def filename=""
        def delimiter = ";"
        def fileReader = null
        def reader = null
        def erroreWS=""
        def dataPAIFin=new Date().parse("yyyyMMdd","20160930")
        def dataPAIIni=new Date().parse("yyyyMMdd","20160701")
        def listError =""
        def listPratiche=""
        def listFlussiPAI=""
        def listDeleted=""
        def totale=0
        def totaleCVTRCA=0
        def totaleCVTRCA2=0
        def totaleRCA=0
        def totaleErr=0
        def totaleErrCVTRCA=0
        def totaleErrCVTRCA2=0
        def totaleErrRCA=0
        def logg
        boolean isPai=false
        def rispostaMail = "Riassunto caricamento polizze FIN LEASING :\r\n"
        def fileNameRisposta = "riassunto_Caricamento_FinLeasing_${new Date().format("yyyyMMddHms")}_.txt"
        //def polizza
        def risposta =[]
        def polizzeDeleted =[]
        Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$)/
        Pattern pattern = ~/(\w+)/
        String regex = "<(\\S+)[^>]+?mso-[^>]*>.*?</\\1>"
        def x_value=[]
        def dealer
        def dealers=[]

       // try {

        logg = new Log(parametri: " polizze CVT E RCA --> $polizzeRCACVT", operazione: "caricamento polizze csv", pagina: "POLIZZE FIN/LEASING")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

        //println "polizze CVT E RCA --> $polizzeRCACVT"

        //def line = ""
        myInputStream.eachLine { String line , count ->
            if(count!=0){
                def rispostaWebservice
                boolean sup75 = false
                boolean isDeleted = false
                def tokens = line.split(";")
                def cellnoPratica = tokens[1].toString().trim().replaceAll("'", "")

                def numeropacchetti= polizzeRCACVT["${cellnoPratica}"].collect().size()

                //println "num pacchet $numeropacchetti --> polizza $cellnoPratica"
                /*println "cvt $cvt"
                println "rca $rca"
                println "rcacvt $rcacvt"*/
                if(numeropacchetti>0){

                    def cellType = tokens[0].toString().trim()
                    def cellDataDecorrenza = tokens[2].toString().trim()
                    def cellaDealer = tokens[3].toString().trim()
                    def cellaNomeDealer = tokens[4].toString().trim()
                    def cellaCognome = tokens[5].toString().trim()
                    def cellaNome = tokens[6].toString().trim()
                    def cellaBeneficiario = tokens[61].toString().trim().replaceAll("'", "")
                    def cellaCF = tokens[7].toString().trim().replaceAll("'", "")
                    def cellaTel = tokens[8].toString().trim().replaceAll("'", "")
                    def cellaCel = tokens[9].toString().trim().replaceAll("'", "")
                    def cellaLocalita = tokens[10].toString().trim().replaceAll("'", "")
                    def cellaIndirizzo = tokens[11].toString().trim().replaceAll("'", "")
                    def cellaProvincia = tokens[12].toString().trim().replaceAll("'", "")
                    def cellaCap = tokens[13].toString().trim().replaceAll("'", "")
                    def cellaPIva = tokens[15].toString().trim().replaceAll("'", "")
                    def cellaEmail = tokens[16].toString().trim().replaceAll("'", "")
                    def cellaCoobligatoCognome = tokens[17].toString().trim().replaceAll("'", "")
                    def cellaCoobligatoNome = tokens[18].toString().trim().replaceAll("'", "")
                    def cellaCoobligatoCF = tokens[19].toString().trim().replaceAll("'", "")
                    def cellaCoobligatoTEL = tokens[20].toString().trim().replaceAll("'", "")
                    def cellaCoobligatoCEL = tokens[21].toString().trim().replaceAll("'", "")
                    def cellaCoobligatoLOC = tokens[22].toString().trim().replaceAll("'", "")
                    def cellaCoobligatoIND = tokens[23].toString().trim().replaceAll("'", "")
                    def cellaCoobligatoPROV = tokens[24].toString().trim().replaceAll("'", "")
                    def cellaCoobligatoCAP = tokens[25].toString().trim().replaceAll("'", "")
                    def cellaCoobligatoemail = tokens[28].toString().trim().replaceAll("'", "")
                    def cellaDenominazione = tokens[29].toString().trim().replaceAll("'", "")
                    def cellaTelefonoBusiness = tokens[31].toString().trim().replaceAll("'", "")
                    def cellaIndirizzoBusiness = tokens[32].toString().trim().replaceAll("'", "")
                    def cellaCellulareBusiness = tokens[33].toString().trim().replaceAll("'", "")
                    def cellaProvinciaBusiness = tokens[34].toString().trim().replaceAll("'", "")
                    def cellaLocalitaBusiness = tokens[22].toString().trim().replaceAll("'", "")
                    def cellaCapBusiness = tokens[35].toString().trim().replaceAll("'", "")
                    def cellaPIvaBusiness = tokens[36].toString().trim().replaceAll("'", "")
                    def cellaCFBusiness = tokens[37].toString().trim().replaceAll("'", "")
                    def cellaEmailBusiness = tokens[38].toString().trim().replaceAll("'", "")
                    def cellaBeneFinanziato = tokens[39].toString().trim().replaceAll("'", "")
                    def cellaDataImmatr = tokens[40].toString().trim().replaceAll("'", "")
                    def cellaMarca = tokens[41].toString().trim().replaceAll("'", "")
                    def cellaModello = tokens[42].toString().trim().replaceAll("'", "")
                    def cellaVersione = tokens[43].toString().trim().replaceAll("'", "")
                    def cellaTelaio = tokens[45].toString().trim().replaceAll("'", "")
                    def cellaTarga = tokens[46].toString().trim().replaceAll("'", "")
                    def cellaPrezzo = tokens[47].toString().trim().replaceAll("'", "")
                    def cellaStep = tokens[49].toString().trim().replaceAll("'", "")
                    def cellaProdotto = tokens[51].toString().trim().replaceAll("'", "")
                    def cellaDurata = tokens[52].toString().trim().replaceAll("'", "")
                    def cellaValoreAssi = tokens[53].toString().trim().replaceAll("'", "")
                    def cellaPacchetto = tokens[54].toString().trim().replaceAll("'", "")
                    def cellaPremioImpo = tokens[56].toString().trim().replaceAll("'", "")
                    def cellaPremioLordo = tokens[57].toString().trim().replaceAll("'", "")
                    def cellaProvvDealer = tokens[58].toString().trim().replaceAll("'", "")
                    def cellaProvvVenditore = tokens[59].toString().trim().replaceAll("'", "")
                    def cellaProvvGMFI = tokens[60].toString().trim().replaceAll("'", "")
                    def cellaCertificato = tokens[62].toString().trim().replaceAll("'", "")
                    def cellaVenditore = tokens[65].toString().trim().replaceAll("'", "")
                    def cellatipoPolizza = tokens[68].toString().trim().replaceAll("'", "")
                    def cellacodCampagna = tokens[69].toString().trim().replaceAll("'", "")
                    def dataDecorrenza = null
                    if (cellDataDecorrenza.toString().trim() != 'null' && cellDataDecorrenza.toString().length() > 0) {
                        def newDate = Date.parse('yyyyMMdd', cellDataDecorrenza)
                        dataDecorrenza = newDate
                        if ((dataDecorrenza) && (dataDecorrenza >= dataPAIIni && dataDecorrenza <= dataPAIFin)) {
                            isPai = true
                        } else {
                            isPai = false
                        }
                    }
                    if (cellnoPratica.toString().equals("null")) {
                        cellnoPratica = ""
                    } else {
                        cellnoPratica = cellnoPratica.toString()
                        cellnoPratica = cellnoPratica.toString().replaceFirst("^0*", "")
                        def indexslash = cellnoPratica.indexOf("/")
                        if (indexslash > 0) {
                            cellnoPratica = cellnoPratica.toString().substring(0, indexslash)
                        }
                        if (cellnoPratica.toString().contains(".")) {
                            def punto = cellnoPratica.toString().indexOf(".")
                            cellnoPratica = cellnoPratica.toString().substring(0, punto).replaceAll("[^0-9]", "")
                        }
                    }
                    def codOperazione = "0"
                    if (cellType.toString().trim() != 'null') {
                        if (cellType.toString().trim().equalsIgnoreCase("NEW")) {
                            codOperazione = "0"
                        } else if (cellType.toString().trim().equalsIgnoreCase("DELETED")) {
                            isDeleted = true
                        } else if (cellType.toString().trim().equalsIgnoreCase("CLOSED")) {
                            codOperazione = "0"
                        } else {
                            codOperazione = "S"
                        }
                    }
                    def coperturaR
                    def pacchettoP = ""
                    if (cellaProdotto.toString().trim() != 'null' && cellaPacchetto.toString().trim().length() > 0) {
                        def indexdue = cellaProdotto.toString().trim().indexOf("-")
                        pacchettoP = cellaProdotto.toString().trim().substring(0, indexdue)
                        pacchettoP = pacchettoP.toString().trim()
                        if (pacchettoP == "A") {
                            coperturaR = "SILVER"
                        } else if (pacchettoP == "B") {
                            coperturaR = "GOLD"
                        } else if (pacchettoP == "C") {
                            coperturaR = "PLATINUM"
                        } else if (pacchettoP == "D") {
                            coperturaR = "PLATINUM COLLISIONE"
                        } else if (pacchettoP == "E") {
                            coperturaR = "PLATINUM KASKO"
                        }else if (pacchettoP == "F") {
                            coperturaR = "F"
                        }
                    }
                    if (isDeleted && cellnoPratica != "") {
                        polizzeDeleted.add("${cellnoPratica},")
                        logg = new Log(parametri: "Errore creazione polizza: la polizza e' tipo DELETED", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        if(pacchettoP=="F"){
                            errorePraticheRCA.add("Errore creazione polizza: la polizza ${cellnoPratica} e' tipo DELETED,")
                            totaleErrRCA++
                        }else{
                            errorepraticheCVT.add("Errore creazione polizza: la polizza ${cellnoPratica} e' tipo DELETED,")
                            totaleErr++
                        }
                    } else if (!cellnoPratica) {
                        logg = new Log(parametri: "Errore creazione polizza: non \u00E8 presente un numero di pratica", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        if(pacchettoP=="F"){
                            errorePraticheRCA.add("Errore creazione polizza: non \u00E8 presente un numero di polizza,")
                            totaleErrRCA++
                        }else{
                            errorepraticheCVT.add("Errore creazione polizza: non \u00E8 presente un numero di polizza,")
                            totaleErr++
                        }
                    } else {
                        if (cellaNome.toString().equals("null")) {
                            cellaNome = ""
                        } else {
                            cellaNome = cellaNome.toString().trim()
                        }
                        if (cellaCognome.toString().equals("null")) {
                            cellaCognome = ""
                        } else {
                            cellaCognome = cellaCognome.toString().trim()
                        }
                        if (cellaCap.toString().equals("null")) {
                            cellaCap = ""
                        } else {
                            cellaCap = cellaCap.toString().replaceAll("'", "")
                            if (cellaCap.toString().contains(".")) {
                                def punto = cellaCap.toString().indexOf(".")
                                cellaCap = cellaCap.toString().substring(0, punto)
                            }
                        }
                        if (cellaDealer.toString().equals("null")) {
                            cellaDealer = ""
                        } else {
                            cellaDealer = cellaDealer.toString().replaceAll("'", "")
                            cellaDealer = cellaDealer.toString().replaceFirst("^0*", "")
                            if (cellaDealer.toString().contains(".")) {
                                cellaDealer = Double.valueOf(cellaDealer).longValue().toString()
                                //cellaDealer=cellaDealer.toString().substring(0,punto).replaceAll("[^0-9]", "")
                            }
                            dealer = Dealer.findByCodice(cellaDealer.toString())
                        }
                        if (cellaCF.toString().equals("null")) {
                            cellaCF = cellaPIva.toString().replaceAll("[^a-z,A-Z,0-9]", "")
                        } else {
                            cellaCF = cellaCF.toString().replaceAll("[^a-z,A-Z,0-9]", "")
                        }
                        if (cellaMarca.toString().equals("null")) {
                            cellaMarca = null
                        } else {
                            cellaMarca = cellaMarca.toString().trim().toUpperCase()
                        }
                        if (cellaModello.toString().equals("null")) {
                            cellaModello = null
                        } else {
                            cellaModello = cellaModello.toString().trim().toUpperCase()
                        }
                        if (cellaTel.toString().trim() != 'null') {
                            cellaTel = cellaTel.toString().trim()
                            if (cellaTel.contains(".")) {
                                int zero = cellaTel.toString().indexOf(".")
                                cellaTel = cellaTel.toString().substring(0, zero)
                            }
                        } else {
                            cellaTel = null
                        }
                        if (cellaTelefonoBusiness.toString().trim() != 'null') {
                            cellaTelefonoBusiness = cellaTelefonoBusiness.toString().trim()
                            if (cellaTelefonoBusiness.contains(".")) {
                                int zero = cellaTelefonoBusiness.toString().indexOf(".")
                                cellaTelefonoBusiness = cellaTelefonoBusiness.toString().substring(0, zero)
                            }

                        } else {
                            cellaTelefonoBusiness = null
                        }
                        if (cellaCel.toString().trim() != 'null') {
                            cellaCel = cellaCel.toString().trim().replaceAll("[^0-9]+", "")
                            if (cellaCel.toString().contains(".")) {
                                int zero = cellaCel.toString().indexOf(".")
                                cellaCel = cellaCel.toString().substring(0, zero)
                            }
                        } else {
                            cellaCel = null
                        }
                        if (cellaCellulareBusiness.toString().trim() != 'null') {
                            cellaCellulareBusiness = cellaCellulareBusiness.toString().trim().replaceAll("[^0-9]+", "")
                            if (cellaCellulareBusiness.toString().contains(".")) {
                                int zero = cellaCellulareBusiness.toString().indexOf(".")
                                cellaCellulareBusiness = cellaCellulareBusiness.toString().substring(0, zero)
                            }
                        } else {
                            cellaCellulareBusiness = null
                        }

                        def nuovo = false
                        if (cellaBeneFinanziato.toString().trim() != 'null') {
                            if (cellaBeneFinanziato.toString().trim().equalsIgnoreCase("NEW")) {
                                nuovo = true
                            } else {
                                nuovo = false
                            }
                        }
                        def polizzaTipo = ""
                        def provinciaimma=""
                        if (cellatipoPolizza.toString().trim() != 'null') {
                            if (cellatipoPolizza.toString().trim().equalsIgnoreCase("RETAIL")) {
                                polizzaTipo = TipoPolizza.FINANZIATE
                            } else {
                                polizzaTipo = TipoPolizza.LEASING
                                provinciaimma="RM"
                            }
                        }
                        def codCampagna = ""
                        if (cellacodCampagna.toString().trim() != 'null') {
                            if (cellacodCampagna.toString().trim().equalsIgnoreCase("0")) {
                                codCampagna = "0"
                            } else {
                                codCampagna = cellacodCampagna.toString().trim()
                            }
                        }
                        boolean onStar = false
                        boolean certificato = false
                        if (cellaCertificato.toString().trim() != '') {
                            if (cellaCertificato.toString().trim() != "N") {
                                certificato = true
                            } else {
                                certificato = false
                            }
                        }
                        if (cellaTelaio.toString().trim() != '') {
                            cellaTelaio = cellaTelaio.toString().trim().toUpperCase()
                        } else {
                            cellaTelaio = null
                        }
                        if (cellaTarga.toString().trim() != '') {
                            cellaTarga = cellaTarga.toString().trim().toUpperCase()
                        } else {
                            cellaTarga = null
                        }
                        if (cellaEmail.toString().trim() != '') {
                            cellaEmail = cellaEmail.toString().trim()
                        } else {
                            cellaEmail = null
                        }
                        def dataScadenza
                        def durata
                        if (cellaDurata.toString().trim() != 'null' && cellaDurata.toString().trim().length() > 0) {
                            if (cellaDurata.toString().contains(".")) {
                                def punto = cellaDurata.toString().indexOf(".")
                                cellaDurata = cellaDurata.toString().substring(0, punto).replaceAll("[^0-9]", "")
                            }
                            durata = Integer.parseInt(cellaDurata.toString().trim())
                        } else {
                            durata = null
                        }
                        if(numeropacchetti==1 && pacchettoP=="F"){
                            if (durata != null && dataDecorrenza != null) {
                                dataScadenza = use(TimeCategory) { dataDecorrenza + 12.months }
                            }
                        }else{
                            if (durata != null && dataDecorrenza != null) {
                                dataScadenza= assegnascadenza(durata, dataDecorrenza)
                            }
                        }
                        def step, codStep
                        if (cellaStep.toString().trim() != 'null') {
                            step = cellaStep.toString().trim()
                            if (step.contains(".")) {
                                int zero = step.toString().indexOf(".")
                                step = step.toString().substring(0, zero).toLowerCase()
                            }
                            if(pacchettoP=="F"){
                                codStep = "step 1"
                            }else{
                                if (step == "1") {
                                    codStep = "step 1"
                                } else if (step == "2") {
                                    codStep = "step 2"
                                } else if (step == "3") {
                                    codStep = "step 3"
                                }
                            }
                        } else {
                            codStep = null
                        }
                        def dataImmatricolazione = null
                        if (cellaDataImmatr.toString().trim() != 'null' && cellaDataImmatr.toString().length() > 0) {
                            def newDate = Date.parse('yyyyMMdd', cellaDataImmatr)
                            dataImmatricolazione = newDate
                        }
                        def valoreAssicurato
                        if (cellaValoreAssi.toString().trim() != 'null' && cellaValoreAssi.toString().length() > 0) {
                            valoreAssicurato = cellaValoreAssi.toString().replaceAll("\\.", "").replaceAll(",", "\\.")
                            //valoreAssicurato=cellaValoreAssi.toString().replaceAll(",","\\.")
                            valoreAssicurato = new BigDecimal(valoreAssicurato)
                        } else {
                            valoreAssicurato = 0.0
                        }
                        def prezzoVendita
                        if (cellaPrezzo.toString().trim() != 'null' && cellaPrezzo.toString().length() > 0) {
                            prezzoVendita = cellaPrezzo.toString().replaceAll("\\.", "").replaceAll(",", "\\.")
                            //prezzoVendita=prezzoVendita.toString().replaceAll(",","\\.")
                            prezzoVendita = new BigDecimal(prezzoVendita)
                        } else {
                            prezzoVendita = 0.0
                        }
                        def premioImponibile
                        if (cellaPremioImpo.toString().trim() != 'null' && cellaPremioImpo.toString().trim().length() > 0) {
                            premioImponibile = cellaPremioImpo.toString().trim().replaceAll("\\.", "").replaceAll(",", "\\.")
                            //premioImponibile=cellaPremioImpo.toString().trim().replaceAll(",","\\.")
                            premioImponibile = new BigDecimal(premioImponibile)
                        } else {
                            premioImponibile = 0.0
                        }
                        def premioLordo
                        if (cellaPremioLordo.toString().trim() != 'null' && cellaPremioLordo.toString().trim().length() > 0) {
                            premioLordo = cellaPremioLordo.toString().trim().replaceAll("\\.", "").replaceAll(",", "\\.")
                            // premioLordo=cellaPremioLordo.toString().trim().replaceAll(",","\\.")
                            premioLordo = new BigDecimal(premioLordo)
                        } else {
                            premioLordo = 0.0
                        }
                        def provvDealer
                        if (cellaProvvDealer.toString().trim() != 'null' && cellaProvvDealer.toString().trim().length() > 0) {
                            provvDealer = cellaProvvDealer.toString().trim().replaceAll("\\.", "").replaceAll(",", "\\.")
                            //provvDealer=cellaProvvDealer.toString().trim().replaceAll(",","\\.")
                            provvDealer = new BigDecimal(provvDealer)
                        } else {
                            provvDealer = 0.0
                        }
                        def provvGmfi
                        if (cellaProvvGMFI.toString().trim() != 'null' && cellaProvvGMFI.toString().trim().length() > 0) {
                            provvGmfi = cellaProvvGMFI.toString().trim().replaceAll("\\.", "").replaceAll(",", ".")
                            //provvGmfi=cellaProvvGMFI.toString().trim().replaceAll(",",".")
                            provvGmfi = new BigDecimal(provvGmfi)
                        } else {
                            provvGmfi = 0.0
                        }
                        def provvVendi
                        if (cellaProvvVenditore.toString().trim() != '' && cellaProvvVenditore.toString().trim().length() > 0 && cellaProvvVenditore.toString().trim() != '0') {
                            provvVendi = cellaProvvVenditore.toString().trim().replaceAll("\\.", "").replaceAll(",", "\\.")
                            //provvVendi=cellaProvvVenditore.toString().trim().replaceAll(",","\\.")
                            provvVendi = new BigDecimal(provvVendi)
                        } else {
                            provvVendi = 0.0
                        }
                        if (cellaVenditore.toString() != 'null') {
                            cellaVenditore = cellaVenditore.toString().trim()
                        } else {
                            cellaVenditore = ""
                        }
                        def dataInserimento = new Date()
                        def premioLordoSAntifurto = ""
                        def premioLordoAntifurto = ""
                        dataInserimento = dataInserimento.clearTime()
                        def parole = []
                        def nomeB = "", cognomeB = ""
                        def cognomeDef, nomeDef, provinciaDef, localitaDef, capDef, telDef, cellDef, cfDef, indirizzoDef, tipoClienteDef, annoCF
                        def emailDef = ""
                        boolean denominazione = false
                        boolean coobligato = false
                        boolean cliente = false
                        boolean nonTrovato = false
                        if (cellaBeneficiario.toString() != '' && pacchettoP!="F") {
                            denominazione= isbeneficiario(cellaBeneficiario,cellaDenominazione)

                            if (cellaCoobligatoNome) {
                                cellaCoobligatoNome = cellaCoobligatoNome.toUpperCase().trim()
                                cellaCoobligatoCognome = cellaCoobligatoCognome.toUpperCase().trim()
                                def nomeCooC = cellaCoobligatoNome + " " + cellaCoobligatoCognome
                                coobligato= isbeneficiario(cellaBeneficiario,nomeCooC)
                            }
                            if (cellaNome) {
                                cellaNome = cellaNome.toUpperCase().trim()
                                cellaCognome = cellaCognome.toUpperCase().trim()
                                def nomeCom = cellaNome + " " + cellaCognome
                                cliente=isbeneficiario(cellaBeneficiario,nomeCom)
                            }
                            if (coobligato) {
                                logg = new Log(parametri: "prendo i dati del Coobligato-->${cellaCoobligatoNome}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                cognomeDef = cellaCoobligatoCognome.toString().trim().toUpperCase()
                                nomeDef = cellaCoobligatoNome.toString().trim().toUpperCase()
                                provinciaDef = cellaCoobligatoPROV.toString().trim().toUpperCase()
                                localitaDef = cellaCoobligatoLOC.toString().trim().toUpperCase()
                                capDef = cellaCoobligatoCAP.toString().trim().toUpperCase()
                                telDef = cellaCoobligatoTEL.toString().trim().toUpperCase()
                                cellDef = cellaCoobligatoCEL.toString().trim().toUpperCase()
                                cfDef = cellaCoobligatoCF.toString().trim().toUpperCase()
                                //prendo i dati dell'anno dal codice fiscale
                                if (cfDef) {
                                    annoCF = cfDef.substring(6, 8)
                                    if (Integer.parseInt(annoCF) && Integer.parseInt(annoCF) <= 40) {
                                        sup75 = true
                                    }
                                    def sessC=cfDef.substring(9,11)
                                    if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                        tipoClienteDef=TipoCliente.F
                                    }else{
                                        tipoClienteDef=TipoCliente.M
                                    }
                                }
                                indirizzoDef = cellaCoobligatoIND.toString().trim().toUpperCase()
                                if (cellaCoobligatoemail.toString().trim().size() > 4) {
                                    emailDef = cellaCoobligatoemail.toString().trim()
                                }
                            } else if (denominazione) {
                                logg = new Log(parametri: "prendo i dati della denominazione -->${cellaDenominazione}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                cognomeDef = cellaDenominazione.toString().trim().toUpperCase()
                                nomeDef = ""
                                provinciaDef = cellaProvinciaBusiness.toString().trim().toUpperCase()
                                localitaDef = cellaLocalitaBusiness.toString().trim().toUpperCase()
                                capDef = cellaCapBusiness.toString().trim().toUpperCase()
                                telDef = cellaTelefonoBusiness.toString().trim().toUpperCase()
                                cellDef = cellaCellulareBusiness.toString().trim().toUpperCase()
                                if (cellaPIvaBusiness) {
                                    cfDef = cellaPIvaBusiness.toString().trim().toUpperCase()
                                    if (!cfDef.matches("[0-9]{11}")) {
                                        cfDef = cfDef.padLeft(11, "0")
                                    }

                                }
                                indirizzoDef = cellaIndirizzoBusiness.toString().trim().toUpperCase()
                                tipoClienteDef = TipoCliente.DITTA_INDIVIDUALE
                                if (cellaEmailBusiness.toString().trim().size() > 4) {
                                    emailDef = cellaEmailBusiness.toString().trim()
                                }
                            } else if (cliente) {
                                logg = new Log(parametri: "prendo i dati del cliente -->${cellaNome}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                cognomeDef = cellaCognome.toString().trim().toUpperCase()
                                nomeDef = cellaNome.toString().trim().toUpperCase()
                                provinciaDef = cellaProvincia.toString().trim().toUpperCase()
                                localitaDef = cellaLocalita.toString().trim().toUpperCase()
                                capDef = cellaCap.toString().trim().toUpperCase()
                                telDef = cellaTel.toString().trim().toUpperCase()
                                cellDef = cellaCel.toString().trim().toUpperCase()
                                cfDef = cellaCF.toString().trim().toUpperCase()
                                if (cfDef) {
                                    annoCF = cfDef.substring(6, 8)
                                    if (Integer.parseInt(annoCF) && Integer.parseInt(annoCF) <= 40) {
                                        sup75 = true
                                    }
                                    def sessC=cfDef.substring(9,11)
                                    if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                        tipoClienteDef=TipoCliente.F
                                    }else{
                                        tipoClienteDef=TipoCliente.M
                                    }
                                }
                                indirizzoDef = cellaIndirizzo.toString().trim().toUpperCase()
                                if (cellaEmail.toString().trim().size() > 4) {
                                    emailDef = cellaEmail.toString().trim()
                                }

                            } else {
                                nonTrovato = true
                            }
                        }else{
                            logg = new Log(parametri: "e' una polizza RCA prendo i dati del cliente -->${cellaNome}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            if(cellaCognome!="" && cellaNome!=""){
                                cognomeDef = cellaCognome.toString().trim().toUpperCase()
                                nomeDef = cellaNome.toString().trim().toUpperCase()
                                provinciaDef = cellaProvincia.toString().trim().toUpperCase()
                                localitaDef = cellaLocalita.toString().trim().toUpperCase()
                                capDef = cellaCap.toString().trim().toUpperCase()
                                telDef = cellaTel.toString().trim().toUpperCase()
                                cellDef = cellaCel.toString().trim().toUpperCase()
                                cfDef = cellaCF.toString().trim().toUpperCase()
                                //prendo i dati dell'anno dal codice fiscale
                                if (cfDef) {
                                    annoCF = cfDef.substring(6, 8)
                                    if (Integer.parseInt(annoCF) && Integer.parseInt(annoCF) <= 40) {
                                        sup75 = true
                                    }
                                    def sessC=cfDef.substring(9,11)
                                    if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                        tipoClienteDef=TipoCliente.F
                                    }else{
                                        tipoClienteDef=TipoCliente.M
                                    }
                                }
                            }else if (!(cellaDenominazione.toString().equals("null"))){
                                logg = new Log(parametri: "e' una polizza RCA prendo i dati della denominazione -->${cellaDenominazione}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                cognomeDef = cellaDenominazione.toString().trim().toUpperCase()
                                nomeDef = ""
                                provinciaDef = cellaProvinciaBusiness.toString().trim().toUpperCase()
                                localitaDef = cellaLocalitaBusiness.toString().trim().toUpperCase()
                                capDef = cellaCapBusiness.toString().trim().toUpperCase()
                                telDef = cellaTelefonoBusiness.toString().trim().toUpperCase()
                                cellDef = cellaCellulareBusiness.toString().trim().toUpperCase()
                                if (cellaPIvaBusiness) {
                                    cfDef = cellaPIvaBusiness.toString().trim().toUpperCase()
                                    if (!cfDef.matches("[0-9]{11}")) {
                                        cfDef = cfDef.padLeft(11, "0")
                                    }

                                }
                                tipoClienteDef = TipoCliente.DITTA_INDIVIDUALE
                                if (cellaEmailBusiness.toString().trim().size() > 4) {
                                    emailDef = cellaEmailBusiness.toString().trim()
                                }
                            }
                            if (cfDef) {
                                annoCF = cfDef.substring(6, 8)
                                if (Integer.parseInt(annoCF) && Integer.parseInt(annoCF) <= 40) {
                                    sup75 = true
                                }
                                def sessC=cfDef.substring(9,11)
                                if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                    tipoClienteDef=TipoCliente.F
                                }else{
                                    tipoClienteDef=TipoCliente.M
                                }
                            }
                            indirizzoDef = cellaIndirizzo.toString().trim().toUpperCase()
                            if (cellaEmail.toString().trim().size() > 4) {
                                emailDef = cellaEmail.toString().trim()
                            }
                        }
                        if (nonTrovato  && pacchettoP!='F') {
                            logg = new Log(parametri: "per la pratica CVT -->${cellnoPratica} il beneficiario ${cellaBeneficiario} non appare nelle colonne dei dati coobligato/cliente/business, controllare file", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            errorepraticheCVT.add("per la pratica CVT -->${cellnoPratica} il beneficiario ${cellaBeneficiario} non appare nelle colonne dei dati coobligato/cliente/business, controllare file,")
                            totaleErr++
                        } else {
                            def calcoloImp, totProvvi, diffImpoProvv
                            totProvvi = provvDealer + provvVendi + provvGmfi
                            def provincia = provinciaDef.toString().trim()

                            //verifica tot provv rispetto a step assegnato
                            if ((provincia.trim() != "") && (provincia.trim() != null) && (provincia.trim() != 'null')) {
                                def zonaT = ZoneTarif.findByProvincia(provincia).zona
                                if(pacchettoP!='F'){
                                    if (zonaT != "8") {
                                        if (step == "1") {
                                            calcoloImp = premioImponibile * 0.30
                                        } else if (step == "2") {
                                            calcoloImp = premioImponibile * 0.40
                                        } else if (step == "3") {
                                            calcoloImp = premioImponibile * 0.50
                                        }
                                    } else {
                                        if (step == "1") {
                                            calcoloImp = premioImponibile * 0.27
                                        } else if (step == "2") {
                                            calcoloImp = premioImponibile * 0.37
                                        } else if (step == "3") {
                                            calcoloImp = premioImponibile * 0.47
                                        }
                                    }
                                }else{
                                    calcoloImp = premioImponibile * 0.10
                                }

                                if (dealer && cellnoPratica && cellaTelaio && cellaBeneficiario.toString() != '') {
                                   // println "cella $cellnoPratica paccheto $pacchettoP numero $numeropacchetti"
                                        diffImpoProvv = Math.abs(Math.round((calcoloImp - totProvvi) * 100) / 100)
                                        logg = new Log(parametri: "differenza imponibile provvigioni -> ${diffImpoProvv}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        if (diffImpoProvv <= 0.05 && diffImpoProvv != "") {
                                            if(numeropacchetti==1 && pacchettoP!='F'){
                                               // println "e' una cvt--> ${cellnoPratica}"
                                                def polizzaesistente = Polizza.findByNoPolizzaIlike(cellnoPratica)
                                                if (polizzaesistente) {
                                                    if (codOperazione == "S" && !speciale) {
                                                        if (polizzaesistente.telaio.equalsIgnoreCase(cellaTelaio)) {
                                                            polizzaesistente.codOperazione = codOperazione
                                                            if (polizzaesistente.save(flush: true)) {
                                                                def tracciatoPolizzaR = tracciatiService.generaTracciatoIAssicur(polizzaesistente)
                                                                if (!tracciatoPolizzaR.save(flush: true)) {
                                                                    logg = new Log(parametri: "POLIZZE CVT Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    errorepraticheCVT.add("POLIZZE CVT  pratica-->${cellnoPratica} Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors},")
                                                                    totaleErr++
                                                                } else {
                                                                    def tracciatoComPolizzaR = tracciatiService.generaTracciatoCompagnia(polizzaesistente)
                                                                    if (!tracciatoComPolizzaR.save(flush: true)) {
                                                                        logg = new Log(parametri: "POLIZZE CVT Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        errorepraticheCVT.add("POLIZZE CVT pratica-->${cellnoPratica} Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors},")
                                                                        totaleErr++
                                                                    } else {
                                                                        if (polizzaesistente.tracciatoPAIId) {
                                                                            def tracciatoPAIPolizzaR = tracciatiService.generaTracciatoPAI(polizzaesistente, speciale)
                                                                            if (!tracciatoPAIPolizzaR.save(flush: true)) {
                                                                                logg = new Log(parametri: "POLIZZE CVT Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                errorepraticheCVT.add("POLIZZE CVT pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors},")
                                                                                totaleErr++
                                                                            } else {
                                                                                if (polizzaesistente.targa != null && polizzaesistente.targa != '') {
                                                                                    polizzaesistente.targa = polizzaesistente.targa + "_R"
                                                                                }
                                                                                if (polizzaesistente.telaio != null && polizzaesistente.telaio != '') {
                                                                                    polizzaesistente.telaio = polizzaesistente.telaio + "_R"
                                                                                }
                                                                                if (polizzaesistente.save(flush: true)) {
                                                                                    logg = new Log(parametri: "POLIZZE CVT targa e telaio per la polizza ${polizzaesistente.noPolizza} aggiornati", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                    praticheCVT.add("POLIZZE CVT polizza stornata correttamente ${polizzaesistente.noPolizza},")
                                                                                    totale++
                                                                                } else {
                                                                                    logg = new Log(parametri: "non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                    errorepraticheCVT.add(" POLIZZE CVT pratica-->${cellnoPratica} non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors},")
                                                                                    totaleErr++
                                                                                }
                                                                            }
                                                                        } else {
                                                                            if (polizzaesistente.targa != null && polizzaesistente.targa != '') {
                                                                                polizzaesistente.targa = polizzaesistente.targa + "_R"
                                                                            }
                                                                            if (polizzaesistente.telaio != null && polizzaesistente.telaio != '') {
                                                                                polizzaesistente.telaio = polizzaesistente.telaio + "_R"
                                                                            }
                                                                            if (polizzaesistente.save(flush: true)) {
                                                                                logg = new Log(parametri: "POLIZZE CVT targa e telaio per la polizza ${polizzaesistente.noPolizza} aggiornati", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                praticheCVT.add("POLIZZE CVT polizza stornata correttamente ${polizzaesistente.noPolizza},")
                                                                                totale++
                                                                            } else {
                                                                                logg = new Log(parametri: "POLIZZE CVT non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                errorepraticheCVT.add("POLIZZE CVT  pratica-->${cellnoPratica} non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors},")
                                                                                totaleErr++
                                                                            }
                                                                        }

                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            errorepraticheCVT.add("POLIZZE CVT per questa pratica-->${cellnoPratica} il telaio associato non corrisponde a quello inserito nel DB,")
                                                            totaleErr++
                                                        }
                                                    } else if (speciale && !sup75) {
                                                        logg = new Log(parametri: "POLIZZE CVT genero tracciato PAI", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        def tracciatoPAI = tracciatiService.generaTracciatoPAI(polizzaesistente, speciale)
                                                        if (!tracciatoPAI.save(flush: true)) {
                                                            logg = new Log(parametri: "POLIZZE CVT Errore creazione tracciato PAI: ${tracciatoPAI.errors}", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            errorepraticheCVT.add("POLIZZE CVT pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAI.errors},")
                                                            totaleErr++
                                                        } else {
                                                            logg = new Log(parametri: "POLIZZE CVT tracciato PAI generato correttamente per la polizza ${polizzaesistente.noPolizza}", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            flussiPAI.add("POLIZZE CVT tracciato PAI generato correttamente per la polizza ${polizzaesistente.noPolizza},")
                                                            totale++
                                                        }
                                                    }else if(sup75){
                                                        errorepraticheCVT.add("POLIZZE CVT il beneficiario della pratica CVT-->${cellnoPratica}  supera la eta' di 75 anni,")
                                                        totaleErr++
                                                    }
                                                    else {
                                                        errorepraticheCVT.add("POLIZZE CVT la pratica CVT -->${cellnoPratica} \u00E8 gia' stata caricata nel portale,")
                                                        totaleErr++
                                                    }
                                                } else {
                                                    if (codOperazione == "S") {
                                                        errorepraticheCVT.add("POLIZZE CVT la pratica CVT-->${cellnoPratica} che si sta cercando di stornare non esiste. Verificare no polizza,")
                                                        totaleErr++
                                                    } else if (speciale) {
                                                        errorepraticheCVT.add("POLIZZE CVT la pratica-->${cellnoPratica} non esiste e non e' possibile generare un flusso PAI... verificare no polizza,")
                                                        totaleErr++
                                                    } else {
                                                        /****hace llamada al ws**/
                                                        def pacchetto=CodProdotti.findByCoperturaAndStep(coperturaR, codStep).codPacchetto
                                                        def percentualeVenditore=dealer.percVenditore
                                                        if(percentualeVenditore.toPlainString()=="0.0000000"){
                                                            percentualeVenditore=0.0
                                                        }
                                                        def dealerType=dealer.dealerType
                                                        if((provincia.trim()!="") && (provincia.trim() !='null') && pacchetto!="" && durata!=null && localitaDef.trim()!="" && localitaDef.trim()!='NULL' && dealerType!="" && step!=""&& percentualeVenditore!=""&& valoreAssicurato!="" && valoreAssicurato > 0.0 ){
                                                            def dataTarriffa='2017-04-21'
                                                            def tipoOperazione
                                                            if(codOperazione=="0"){
                                                                tipoOperazione="A"
                                                            }else{
                                                                tipoOperazione=codOperazione
                                                            }
                                                            rispostaWebservice=DLWebService.chiamataWSFINTAR2(pacchetto,valoreAssicurato,provincia,localitaDef,durata,dealer.dealerType,step, dataTarriffa,tipoOperazione,percentualeVenditore,premioLordo, cellnoPratica,polizzaTipo)
                                                           // println "risposta chiamata WS CVT-->: ${rispostaWebservice}"
                                                            logg = new Log(parametri: "POLIZZE CVT risposta chiamata WS CVT-->: ${rispostaWebservice}", operazione: "chiamata WS CVT", pagina: "POLIZZE FIN/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            if(rispostaWebservice.risposta==true){
                                                                def polizza = Polizza.findOrCreateWhere(
                                                                        annullata: false,
                                                                        cap: capDef.toString().trim().padLeft(5, "0"),
                                                                        cellulare: cellDef.toString().trim(),
                                                                        cognome: cognomeDef.toString().trim(),
                                                                        coperturaRichiesta: coperturaR,
                                                                        dataDecorrenza: dataDecorrenza,
                                                                        dataImmatricolazione: dataImmatricolazione,
                                                                        dataInserimento: dataInserimento,
                                                                        dataScadenza: dataScadenza,
                                                                        dealer: dealer,
                                                                        durata: durata,
                                                                        email: emailDef.toString().trim(),
                                                                        indirizzo: indirizzoDef.toString().trim().toUpperCase(),
                                                                        localita: localitaDef.toString().trim().toUpperCase(),
                                                                        marca: cellaMarca,
                                                                        modello: cellaModello,
                                                                        noPolizza: cellnoPratica,
                                                                        nome: nomeDef.toString().trim().toUpperCase(),
                                                                        nuovo: nuovo,
                                                                        onStar: rispostaWebservice.polizza.onStar,
                                                                        partitaIva: cfDef.toString().trim().toUpperCase(),
                                                                        premioImponibile: premioImponibile,
                                                                        premioLordo: premioLordo,
                                                                        provincia: provinciaDef.toString().trim().toUpperCase(),
                                                                        provvDealer: provvDealer,
                                                                        provvGmfi: provvGmfi,
                                                                        provvVenditore: provvVendi,
                                                                        stato: StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE,
                                                                        tipoCliente: tipoClienteDef,
                                                                        tipoPolizza: polizzaTipo,
                                                                        step: codStep,
                                                                        telaio: cellaTelaio,
                                                                        targa: cellaTarga,
                                                                        telefono: telDef.toString().trim(),
                                                                        valoreAssicurato: valoreAssicurato,
                                                                        valoreAssicuratoconiva: prezzoVendita,
                                                                        certificatoMail: certificato,
                                                                        codiceZonaTerritoriale: rispostaWebservice.polizza.codiceZonaTerritoriale,
                                                                        rappel: rispostaWebservice.polizza.rappel,
                                                                        codOperazione: codOperazione,
                                                                        dSlip: "S",
                                                                        venditore: cellaVenditore ? cellaVenditore.toString().trim().toUpperCase() : "",
                                                                        imposte: rispostaWebservice.polizza.imposte,
                                                                        codCampagna: codCampagna,
                                                                        tariffa: 2,
                                                                        premioCliente: rispostaWebservice.polizza.premioCliente,
                                                                        premioLordoCliente: rispostaWebservice.polizza.premioLordoCliente,
                                                                        premioLordoTerzi: rispostaWebservice.polizza.premioLordoTerzi,
                                                                        premioTerzi: rispostaWebservice.polizza.premioTerzi,
                                                                        hasRCA: false
                                                                )
                                                                if (polizza.save(flush: true)) {
                                                                    logg = new Log(parametri: "POLIZZE CVT la polizza viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                                                                    if(!tracciatoPolizza.save(flush: true)){
                                                                        logg =new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizza.errors}", operazione: "generazione tracciato iassicur", pagina: "POLIZZE FINANZIATE")
                                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    }else{
                                                                        polizza.tracciato=tracciatoPolizza
                                                                        logg =new Log(parametri: "POLIZZE CVT genero tracciato IAssicur", operazione: "generazione tracciato iassicur", pagina: "POLIZZE FINANZIATE")
                                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        def tracciatoComPolizza= tracciatiService.generaTracciatoCompagnia(polizza)
                                                                        if(!tracciatoComPolizza.save(flush: true)){
                                                                            logg =new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizza.errors}", operazione: "generazione tracciato DL", pagina: "POLIZZE FINANZIATE")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        } else{
                                                                            polizza.tracciatoCompagnia=tracciatoComPolizza
                                                                            logg =new Log(parametri: "genero tracciato Compagnia", operazione: "generazione tracciato DL", pagina: "POLIZZE FINANZIATE")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            if(polizza.save(flush:true)) {
                                                                                logg =new Log(parametri: "polizza ${polizza.noPolizza} creata con i suoi tracciati correttamente ", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                praticheCVT.add("POLIZZA CVT-- polizza ${polizza.noPolizza} caricata correttamente ,")
                                                                                totale++
                                                                            }else{
                                                                               // println "Errore creazione polizza: ${polizza.errors}"
                                                                                errorepraticheCVT.add("POLIZZE CVT Errore creazione polizza: ${polizza.errors}")
                                                                                logg =new Log(parametri: "Errore aggiornamento tracciati polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            }
                                                                        }
                                                                    }

                                                                } else {
                                                                    //println "Errore caricamento polizza: ${polizza.errors}"
                                                                    logg = new Log(parametri: "Errore caricamento polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    errorepraticheCVT.add("POLIZZE CVT pratica-->${cellnoPratica} Errore caricamento polizza: ${polizza.errors},")
                                                                    totaleErr++
                                                                }

                                                            }else{
                                                                if(rispostaWebservice.errore!=''&& rispostaWebservice.errore && rispostaWebservice.errore.toString()!="null" ){
                                                                    //println "questa e' la risposta--> ${rispostaWebservice}"
                                                                    errorepraticheCVT.add( "POLIZZE CVT errore chiamata al web service per la polizza--> ${cellnoPratica} -->errore : ${rispostaWebservice.errore}")
                                                                    logg = new Log(parametri: "POLIZZE CVT -->errore chiamata al web service per la polizza--> ${cellnoPratica} --> ${rispostaWebservice.errore}", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }

                                                            }
                                                        }else{
                                                            if((provincia.trim()=="") && (provincia.trim() =='null')){
                                                                errorepraticheCVT.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stata compilata la provincia,")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stata compilata la provincia", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else if(pacchetto==""){
                                                                errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stato trovato il pacchetto controllare,")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stato trovato il pacchetto controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else if(durata==""){
                                                                errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stata inserita la durata,")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stata inserita la durata controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else if(localitaDef==""){
                                                                errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stata inserita la localita',")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stata inserita la localita' controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else if(dealerType==""){
                                                                errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stato inserito il dealer type,")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stato inserito il dealer type controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else if(step==""){
                                                                errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stato inserito lo step,")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non è stato inserito lo step controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else if(percentualeVenditore==""){
                                                                errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}   non e' stata inserita la percentuale venditore,")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stata inserita la percentuale venditore", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else if(valoreAssicurato=="" && valoreAssicurato <= 0.0){
                                                                errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}   non e' stato inserito il valore assicurato,")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stato inserito il valore assicurato", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }
                                                        }
                                                    }
                                                }
                                            }else if(numeropacchetti==1 && pacchettoP=='F'){
                                                //println "e' una rca-->${cellnoPratica}"
                                                def polizzaesistente = PolizzaRCA.findByNoPolizzaIlike(cellnoPratica)
                                                if (polizzaesistente) {
                                                    if (codOperazione == "S" ) {
                                                        if (polizzaesistente.telaio.equalsIgnoreCase(cellaTelaio)) {
                                                            polizzaesistente.codOperazione = codOperazione
                                                            if (polizzaesistente.save(flush: true)) {
                                                                logg = new Log(parametri: "POLIZZE RCA rigenerato", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                                if (!logg.save(flush: true)) println "POLIZZE RCA Errori salvataggio log reponse: ${logg.errors}"
                                                            }
                                                        } else {
                                                            errorePraticheRCA.add("POLIZZE RCA per questa pratica-->${cellnoPratica} il telaio associato non corrisponde a quello inserito nel DB,")
                                                            totaleErrRCA++
                                                        }
                                                    } else if(sup75){
                                                        errorePraticheRCA.add("POLIZZE RCA il beneficiario della pratica-->${cellnoPratica}  supera la eta' di 75 anni,")
                                                        totaleErrRCA++
                                                    }
                                                    else {
                                                        errorePraticheRCA.add("POLIZZE RCA la pratica-->${cellnoPratica} \u00E8 gia' stata caricata nel portale,")
                                                        totaleErrRCA++
                                                    }
                                                } else {
                                                    if (codOperazione == "S") {
                                                        errorePraticheRCA.add("POLIZZE RCA la pratica rca-->${cellnoPratica} che si sta cercando di stornare non esiste. Verificare no polizza,")
                                                        totaleErrRCA++
                                                    }  else {
                                                        /**chiamo il webservice*/
                                                        def rappel, imposte
                                                        def chiamataws=DLWebService.chiamataRCAWSCSV(cellnoPratica, cellaModello,dealer.id,valoreAssicurato,provincia,localitaDef.toString().trim().toUpperCase(),cellaVersione.toUpperCase(),dataDecorrenza, premioLordo,polizzaTipo)
                                                        if(chiamataws){
                                                            //println "risposta chiamata ws rca ${chiamataws}"
                                                            logg = new Log(parametri: "risposta chiamata ws rca ${chiamataws}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            if(chiamataws.risposta==false){
                                                                errorePraticheCVTRCA.add(" POLIZZA CVT/RCA  pratica-->${cellnoPratica} ha dati il seguente Errore dal WEBSERVICE: ${chiamataws.errore},")
                                                                logg = new Log(parametri: "POLIZZA CVT/RCA  pratica-->${cellnoPratica} ha dati il seguente Errore dal WEBSERVICE: ${chiamataws.errore}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                totaleErrCVTRCA2++
                                                                rappel=null
                                                                imposte=null
                                                            }else{
                                                                rappel=chiamataws.rappel
                                                                imposte=chiamataws.imposte
                                                            }

                                                        }
                                                        if(polizzaTipo != TipoPolizza.LEASING.toString()){
                                                            provinciaimma=provinciaDef.toString().trim().toUpperCase()
                                                        }
                                                        if(rappel != null && imposte != null){
                                                            def polizza = PolizzaRCA.findOrCreateWhere(
                                                                    annullata: false,
                                                                    cap: capDef.toString().trim().padLeft(5, "0"),
                                                                    cellulare: cellDef.toString().trim(),
                                                                    cognome: cognomeDef.toString().trim(),
                                                                    coperturaRichiesta: coperturaR,
                                                                    dataDecorrenza: dataDecorrenza,
                                                                    dataImmatricolazione: dataImmatricolazione,
                                                                    dataInserimento: dataInserimento,
                                                                    dataScadenza: dataScadenza,
                                                                    dealer: dealer,
                                                                    durata: 12,
                                                                    email: emailDef.toString().trim(),
                                                                    indirizzo: indirizzoDef.toString().trim().toUpperCase(),
                                                                    localita: localitaDef.toString().trim().toUpperCase(),
                                                                    marca: cellaMarca,
                                                                    modello: cellaModello,
                                                                    noPolizza: cellnoPratica,
                                                                    nome: nomeDef.toString().trim().toUpperCase(),
                                                                    nuovo: nuovo,
                                                                    onStar: false,
                                                                    partitaIva: cfDef.toString().trim().toUpperCase(),
                                                                    premioImponibile: premioImponibile,
                                                                    premioLordo: premioLordo,
                                                                    provincia: provinciaDef.toString().trim().toUpperCase(),
                                                                    provImmatricolazione: provinciaimma.toString().trim().toUpperCase(),
                                                                    provvDealer: provvDealer,
                                                                    provvGmfi: provvGmfi,
                                                                    provvVenditore: provvVendi,
                                                                    stato: StatoPolizza.PREVENTIVO,
                                                                    tipoCliente: tipoClienteDef,
                                                                    tipoPolizza: polizzaTipo,
                                                                    step: "step 1",
                                                                    telaio: cellaTelaio,
                                                                    targa: cellaTarga,
                                                                    telefono: telDef.toString().trim(),
                                                                    valoreAssicurato: valoreAssicurato,
                                                                    codiceZonaTerritoriale: chiamataws.zona,
                                                                    categoriaveicolo: chiamataws.categoria,
                                                                    rappel: rappel,
                                                                    codOperazione: codOperazione,
                                                                    dSlip: "S",
                                                                    venditore: cellaVenditore ? cellaVenditore.toString().trim().toUpperCase() : "",
                                                                    imposte: imposte,
                                                                    codCampagna: codCampagna,
                                                                    tariffa: 2,
                                                                    hasCVT: false,
                                                                    versione: cellaVersione.toUpperCase(),
                                                                    destUso: "PROPRIO",
                                                                    isCash: false,
                                                                    dacsv: true
                                                            )
                                                            if (polizza.save(flush: true)) {
                                                                logg = new Log(parametri: "POLIZZE RCA la polizza viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                def iden=generaIdentificativo(polizza)
                                                                //println "POLIZZE RCA la polizza viene salvata nel DB ${generaIdentificativo(polizza)}"
                                                                polizza.identificativo=  generaIdentificativo(polizza)
                                                                if (polizza.save(flush: true)) {
                                                                    // println "la polizza viene aggiornata nel DB"
                                                                    logg = new Log(parametri: "POLIZZE RCA la polizza viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    praticheRCA.add("POLIZZA RCA la pratica-->${cellnoPratica} e' stata caricata,")
                                                                }
                                                            } else {
                                                                logg = new Log(parametri: "POLIZZE RCA Errore caricamento polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                errorePraticheRCA.add(" POLIZZE RCA pratica-->${cellnoPratica} Errore caricamento polizza: ${polizza.errors},")
                                                                totaleErrRCA++
                                                            }
                                                        }

                                                    }
                                                }
                                            }else if(numeropacchetti>1){
                                               // println "e' una cvt rca--> ${cellnoPratica}"
                                                if(pacchettoP=="F"){
                                                    logg = new Log(parametri: "POLIZZa RCA la polizza ${cellnoPratica} e' una rca", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING/RCA")
                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    def polizzaesistente = PolizzaRCA.findByNoPolizzaIlike(cellnoPratica)
                                                    if (polizzaesistente) {
                                                        if (codOperazione == "S" ) {
                                                            if (polizzaesistente.telaio.equalsIgnoreCase(cellaTelaio)) {
                                                                polizzaesistente.codOperazione = codOperazione
                                                                if (polizzaesistente.save(flush: true)) {
                                                                    logg = new Log(parametri: "POLIZZA CVT/RCA POLIZZA RCA tracciato compagnia rigenerato", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                                    if (!logg.save(flush: true)) println "POLIZZA CVT/RCA  Errori salvataggio log reponse: ${logg.errors}"
                                                                }
                                                            } else {
                                                                errorePraticheCVTRCA.add("POLIZZA CVT/RCA  per questa pratica RCA-->${cellnoPratica} il telaio associato non corrisponde a quello inserito nel DB,")
                                                                totaleErrCVTRCA2++
                                                            }
                                                        } else if(sup75){
                                                            errorePraticheCVTRCA.add("POLIZZA CVT/RCA  il beneficiario della pratica RCA-->${cellnoPratica}  supera la eta' di 75 anni,")
                                                            totaleErrCVTRCA2++
                                                        }
                                                        else {
                                                            errorePraticheCVTRCA.add("POLIZZA CVT/RCA  la pratica RCA-->${cellnoPratica} \u00E8 gia' stata caricata nel portale,")
                                                            totaleErrCVTRCA2++
                                                        }
                                                    } else {
                                                        if (codOperazione == "S") {
                                                            errorePraticheCVTRCA.add("POLIZZA CVT/RCA  la pratica RCA-->${cellnoPratica} che si sta cercando di stornare non esiste. Verificare no polizza,")
                                                            totaleErrCVTRCA2++
                                                        }  else {
                                                            /**chiamo il webservice*/
                                                            def rappel, imposte

                                                            def chiamataws=DLWebService.chiamataRCAWSCSV(cellnoPratica,cellaModello,dealer.id,valoreAssicurato,provincia,localitaDef.toString().trim().toUpperCase(),cellaVersione.toUpperCase(),dataDecorrenza, premioLordo, polizzaTipo)
                                                            if(chiamataws){
                                                                //println "risposta chiamata ws rca ${chiamataws}"
                                                                logg = new Log(parametri: "risposta chiamata ws rca ${chiamataws}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                if(chiamataws.risposta==false){
                                                                    errorePraticheCVTRCA.add(" POLIZZA CVT/RCA  pratica-->${cellnoPratica} ha dati il seguente Errore dal WEBSERVICE: ${chiamataws.errore},")
                                                                    logg = new Log(parametri: "POLIZZA CVT/RCA  pratica-->${cellnoPratica} ha dati il seguente Errore dal WEBSERVICE: ${chiamataws.errore}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    totaleErrCVTRCA2++
                                                                    rappel=null
                                                                    imposte=null
                                                                }else{
                                                                    rappel=chiamataws.rappel
                                                                    imposte=chiamataws.imposte
                                                                }

                                                            }
                                                            if(polizzaTipo != TipoPolizza.LEASING.toString()){
                                                                provinciaimma=provinciaDef.toString().trim().toUpperCase()
                                                            }
                                                            if(rappel != null && imposte != null){
                                                                def polizza = PolizzaRCA.findOrCreateWhere(
                                                                        annullata: false,
                                                                        cap: capDef.toString().trim().padLeft(5, "0"),
                                                                        cellulare: cellDef.toString().trim(),
                                                                        cognome: cognomeDef.toString().trim(),
                                                                        coperturaRichiesta: coperturaR,
                                                                        dataDecorrenza: dataDecorrenza,
                                                                        dataImmatricolazione: dataImmatricolazione,
                                                                        dataInserimento: dataInserimento,
                                                                        dataScadenza: dataScadenza,
                                                                        dealer: dealer,
                                                                        durata: 12,
                                                                        email: emailDef.toString().trim(),
                                                                        indirizzo: indirizzoDef.toString().trim().toUpperCase(),
                                                                        localita: localitaDef.toString().trim().toUpperCase(),
                                                                        marca: cellaMarca,
                                                                        modello: cellaModello,
                                                                        noPolizza: cellnoPratica,
                                                                        nome: nomeDef.toString().trim().toUpperCase(),
                                                                        nuovo: nuovo,
                                                                        onStar: false,
                                                                        partitaIva: cfDef.toString().trim().toUpperCase(),
                                                                        premioImponibile: premioImponibile,
                                                                        premioLordo: premioLordo,
                                                                        provincia: provinciaDef.toString().trim().toUpperCase(),
                                                                        provImmatricolazione: provinciaimma.toString().trim().toUpperCase(),
                                                                        provvDealer: provvDealer,
                                                                        provvGmfi: provvGmfi,
                                                                        provvVenditore: provvVendi,
                                                                        stato: StatoPolizza.PREVENTIVO,
                                                                        tipoCliente: tipoClienteDef,
                                                                        tipoPolizza: polizzaTipo,
                                                                        step: "step 1",
                                                                        telaio: cellaTelaio,
                                                                        targa: cellaTarga,
                                                                        telefono: telDef.toString().trim(),
                                                                        valoreAssicurato: valoreAssicurato,
                                                                        codiceZonaTerritoriale: chiamataws.zona,
                                                                        categoriaveicolo: chiamataws.categoria,
                                                                        rappel: rappel,
                                                                        codOperazione: codOperazione,
                                                                        dSlip: "S",
                                                                        venditore: cellaVenditore ? cellaVenditore.toString().trim().toUpperCase() : "",
                                                                        imposte: imposte,
                                                                        codCampagna: codCampagna,
                                                                        tariffa: 2,
                                                                        hasCVT: true,
                                                                        versione: cellaVersione.toUpperCase(),
                                                                        destUso: "PROPRIO",
                                                                        isCash: false,
                                                                        dacsv: true
                                                                )
                                                                if (polizza.save(flush: true)) {
                                                                    logg = new Log(parametri: "POLIZZA CVT/RCA  la polizza RCA viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    def iden=generaIdentificativo(polizza)
                                                                    //println "la polizza viene salvata nel DB identificativo-> ${generaIdentificativo(polizza)}"
                                                                    polizza.identificativo=  generaIdentificativo(polizza)
                                                                    if (polizza.save(flush: true)) {
                                                                        // println "la polizza viene aggiornata nel DB"
                                                                        logg = new Log(parametri: "POLIZZA CVT/RCA  la polizza RCA viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        praticheCVT_RCA.add("POLIZZA CVT/RCA la pratica RCA-->${cellnoPratica} e' stata caricata,")
                                                                        totaleCVTRCA++
                                                                    }
                                                                } else {
                                                                    logg = new Log(parametri: "POLIZZA CVT/RCA  Errore caricamento polizza RCA: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    errorePraticheCVTRCA.add(" POLIZZA CVT/RCA  pratica-->${cellnoPratica} Errore caricamento polizza: ${polizza.errors},")
                                                                    totaleErrCVTRCA2++
                                                                }
                                                            }

                                                        }
                                                    }
                                                }else{
                                                   // println "e' una cvt-->${cellnoPratica}"
                                                    logg = new Log(parametri: "e' una cvt", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/RCA")
                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    def polizzaesistente = Polizza.findByNoPolizzaIlike(cellnoPratica)
                                                    if (polizzaesistente) {
                                                        if (codOperazione == "S" && !speciale) {
                                                            if (polizzaesistente.telaio.equalsIgnoreCase(cellaTelaio)) {
                                                                polizzaesistente.codOperazione = codOperazione
                                                                if (polizzaesistente.save(flush: true)) {
                                                                    def tracciatoPolizzaR = tracciatiService.generaTracciatoIAssicur(polizzaesistente)
                                                                    if (!tracciatoPolizzaR.save(flush: true)) {
                                                                        logg = new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        errorePraticheCVTRCA.add(" pratica-->${cellnoPratica} Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors},")
                                                                        totaleErrCVTRCA++
                                                                    } else {
                                                                        def tracciatoComPolizzaR = tracciatiService.generaTracciatoCompagnia(polizzaesistente)
                                                                        if (!tracciatoComPolizzaR.save(flush: true)) {
                                                                            logg = new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            errorePraticheCVTRCA.add(" pratica-->${cellnoPratica} Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors},")
                                                                            totaleErrCVTRCA++
                                                                        } else {
                                                                            if (polizzaesistente.tracciatoPAIId) {
                                                                                def tracciatoPAIPolizzaR = tracciatiService.generaTracciatoPAI(polizzaesistente, speciale)
                                                                                if (!tracciatoPAIPolizzaR.save(flush: true)) {
                                                                                    logg = new Log(parametri: "Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                    errorePraticheCVTRCA.add(" pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors},")
                                                                                    totaleErrCVTRCA++
                                                                                } else {
                                                                                    if (polizzaesistente.targa != null && polizzaesistente.targa != '') {
                                                                                        polizzaesistente.targa = polizzaesistente.targa + "_R"
                                                                                    }
                                                                                    if (polizzaesistente.telaio != null && polizzaesistente.telaio != '') {
                                                                                        polizzaesistente.telaio = polizzaesistente.telaio + "_R"
                                                                                    }
                                                                                    if (polizzaesistente.save(flush: true)) {
                                                                                        logg = new Log(parametri: "targa e telaio per la polizza ${polizzaesistente.noPolizza} aggiornati", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                        praticheCVT_RCA.add("polizza stornata correttamente ${polizzaesistente.noPolizza},")
                                                                                        totaleCVTRCA++
                                                                                    } else {
                                                                                        logg = new Log(parametri: "non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                        errorePraticheCVTRCA.add(" pratica-->${cellnoPratica} non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors},")
                                                                                        totaleErrCVTRCA++
                                                                                    }
                                                                                }
                                                                            } else {
                                                                                if (polizzaesistente.targa != null && polizzaesistente.targa != '') {
                                                                                    polizzaesistente.targa = polizzaesistente.targa + "_R"
                                                                                }
                                                                                if (polizzaesistente.telaio != null && polizzaesistente.telaio != '') {
                                                                                    polizzaesistente.telaio = polizzaesistente.telaio + "_R"
                                                                                }
                                                                                if (polizzaesistente.save(flush: true)) {
                                                                                    logg = new Log(parametri: "targa e telaio per la polizza ${polizzaesistente.noPolizza} aggiornati", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                    praticheCVT_RCA.add("polizza stornata correttamente ${polizzaesistente.noPolizza},")
                                                                                    totaleCVTRCA++
                                                                                } else {
                                                                                    logg = new Log(parametri: "non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                    errorePraticheCVTRCA.add(" pratica-->${cellnoPratica} non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors},")
                                                                                    totaleErrCVTRCA++
                                                                                }
                                                                            }

                                                                        }
                                                                    }
                                                                }
                                                            } else {
                                                                errorePraticheCVTRCA.add("per questa pratica-->${cellnoPratica} il telaio associato non corrisponde a quello inserito nel DB,")
                                                                totaleErrCVTRCA++
                                                            }
                                                        } else if (speciale && !sup75) {
                                                            logg = new Log(parametri: "POLIZZA CVT/RCA   genero tracciato PAI", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            def tracciatoPAI = tracciatiService.generaTracciatoPAI(polizzaesistente, speciale)
                                                            if (!tracciatoPAI.save(flush: true)) {
                                                                logg = new Log(parametri: "POLIZZA CVT/RCA   Errore creazione tracciato PAI: ${tracciatoPAI.errors}", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                errorePraticheCVTRCA.add(" pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAI.errors},")
                                                                totaleErrCVTRCA++
                                                            } else {
                                                                logg = new Log(parametri: "POLIZZA CVT/RCA  tracciato PAI generato correttamente per la polizza ${polizzaesistente.noPolizza}", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                flussiPAI.add("tracciato PAI generato correttamente per la polizza ${polizzaesistente.noPolizza},")
                                                                totaleCVTRCA++
                                                            }
                                                        }else if(sup75){
                                                            errorePraticheCVTRCA.add("POLIZZA CVT/RCA il beneficiario della pratica CVT-->${cellnoPratica}  supera la eta' di 75 anni,")
                                                            totaleErrCVTRCA++
                                                        }
                                                        else {
                                                            errorePraticheCVTRCA.add("POLIZZA CVT/RCA la pratica CVT -->${cellnoPratica} \u00E8 gia' stata caricata nel portale,")
                                                            totaleErrCVTRCA++
                                                        }
                                                    } else {
                                                        if (codOperazione == "S") {
                                                            errorePraticheCVTRCA.add("POLIZZA CVT/RCA la pratica CVT-->${cellnoPratica} che si sta cercando di stornare non esiste. Verificare no polizza,")
                                                            totaleErr++
                                                        } else if (speciale) {
                                                            errorePraticheCVTRCA.add("la pratica-->${cellnoPratica} non esiste e non e' possibile generare un flusso PAI... verificare no polizza,")
                                                            totaleErrCVTRCA++
                                                        } else {
                                                            /****hace llamada al ws**/
                                                            def pacchetto=CodProdotti.findByCoperturaAndStep(coperturaR, codStep).codPacchetto
                                                            def percentualeVenditore=dealer.percVenditore
                                                            if(percentualeVenditore.toPlainString()=="0.0000000"){
                                                                percentualeVenditore=0.0
                                                            }
                                                            def dealerType=dealer.dealerType
                                                            if((provincia.trim()!="") && (provincia.trim() !='null') && pacchetto!="" && durata!=null && localitaDef.trim()!="" && localitaDef.trim()!='NULL' && dealerType!="" && step!=""&& percentualeVenditore!=""&& valoreAssicurato!="" && valoreAssicurato > 0.0 ){
                                                                def dataTarriffa='2017-04-21'
                                                                def tipoOperazione
                                                                if(codOperazione=="0"){
                                                                    tipoOperazione="A"
                                                                }else{
                                                                    tipoOperazione=codOperazione
                                                                }
                                                                rispostaWebservice=DLWebService.chiamataWSFINTAR2(pacchetto,valoreAssicurato,provincia,localitaDef,durata,dealer.dealerType,step, dataTarriffa,tipoOperazione,percentualeVenditore,premioLordo, cellnoPratica,polizzaTipo)
                                                               // println "POLIZZA CVT/RCA risposta chiamata WS CVT-->: ${rispostaWebservice}"
                                                                logg = new Log(parametri: "POLIZZA CVT/RCA  risposta chiamata WS CVT-->: ${rispostaWebservice}", operazione: "chiamata WS CVT", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                                                                def polizza = Polizza.findOrCreateWhere(
                                                                        annullata: false,
                                                                        cap: capDef.toString().trim().padLeft(5, "0"),
                                                                        cellulare: cellDef.toString().trim(),
                                                                        cognome: cognomeDef.toString().trim(),
                                                                        coperturaRichiesta: coperturaR,
                                                                        dataDecorrenza: dataDecorrenza,
                                                                        dataImmatricolazione: dataImmatricolazione,
                                                                        dataInserimento: dataInserimento,
                                                                        dataScadenza: dataScadenza,
                                                                        dealer: dealer,
                                                                        durata: durata,
                                                                        email: emailDef.toString().trim(),
                                                                        indirizzo: indirizzoDef.toString().trim().toUpperCase(),
                                                                        localita: localitaDef.toString().trim().toUpperCase(),
                                                                        marca: cellaMarca,
                                                                        modello: cellaModello,
                                                                        noPolizza: cellnoPratica,
                                                                        nome: nomeDef.toString().trim().toUpperCase(),
                                                                        nuovo: nuovo,
                                                                        onStar: rispostaWebservice.polizza.onStar,
                                                                        partitaIva: cfDef.toString().trim().toUpperCase(),
                                                                        premioImponibile: premioImponibile,
                                                                        premioLordo: premioLordo,
                                                                        provincia: provinciaDef.toString().trim().toUpperCase(),
                                                                        provvDealer: provvDealer,
                                                                        provvGmfi: provvGmfi,
                                                                        provvVenditore: provvVendi,
                                                                        stato: StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE,
                                                                        tipoCliente: tipoClienteDef,
                                                                        tipoPolizza: polizzaTipo,
                                                                        step: codStep,
                                                                        telaio: cellaTelaio,
                                                                        targa: cellaTarga,
                                                                        telefono: telDef.toString().trim(),
                                                                        valoreAssicurato: valoreAssicurato,
                                                                        valoreAssicuratoconiva: prezzoVendita,
                                                                        certificatoMail: certificato,
                                                                        codiceZonaTerritoriale: rispostaWebservice.polizza.codiceZonaTerritoriale,
                                                                        rappel: rispostaWebservice.polizza.rappel,
                                                                        codOperazione: codOperazione,
                                                                        dSlip: "S",
                                                                        venditore: cellaVenditore ? cellaVenditore.toString().trim().toUpperCase() : "",
                                                                        imposte: rispostaWebservice.polizza.imposte,
                                                                        codCampagna: codCampagna,
                                                                        tariffa: 2,
                                                                        premioCliente: rispostaWebservice.polizza.premioCliente,
                                                                        premioLordoCliente: rispostaWebservice.polizza.premioLordoCliente,
                                                                        premioLordoTerzi: rispostaWebservice.polizza.premioLordoTerzi,
                                                                        premioTerzi: rispostaWebservice.polizza.premioTerzi,
                                                                        hasRCA: true
                                                                )
                                                                if (polizza.save(flush: true)) {
                                                                    logg = new Log(parametri: "POLIZZA CVT/RCA  la polizza CVT  viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    praticheCVT_RCA.add("POLIZZA CVT/RCA  la pratica CVT-->${cellnoPratica} e' stata caricata,")
                                                                    totaleCVTRCA++
                                                                } else {
                                                                    logg = new Log(parametri: "Errore caricamento polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    errorePraticheCVTRCA.add("POLIZZA CVT/RCA la pratica CVT-->${cellnoPratica} Errore caricamento polizza: ${polizza.errors},")
                                                                    totaleErrCVTRCA++
                                                                }
                                                            }else{
                                                                if((provincia.trim()=="") && (provincia.trim() =='null')){
                                                                    errorepraticheCVT.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stata compilata la provincia,")
                                                                    logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata compilata la provincia", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }else if(pacchetto==""){
                                                                    errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stato trovato il pacchetto controllare,")
                                                                    logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stato trovato il pacchetto controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }else if(durata==""){
                                                                    errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stata inserita la durata,")
                                                                    logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stata inserita la durata controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }else if(localitaDef==""){
                                                                    errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stata inserita la localita',")
                                                                    logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stata inserita la localita' controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }else if(dealerType==""){
                                                                    errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stato inserito il dealer type,")
                                                                    logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stato inserito il dealer type controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }else if(step==""){
                                                                    errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stato inserito lo step,")
                                                                    logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non è stato inserito lo step controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }else if(percentualeVenditore==""){
                                                                    errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}   non e' stata inserita la percentuale venditore,")
                                                                    logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stata inserita la percentuale venditore", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }else if(valoreAssicurato=="" && valoreAssicurato <= 0.0){
                                                                    errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}   non e' stato inserito il valore assicurato,")
                                                                    logg = new Log(parametri: "polizze CVT: PER LA POLIZZA  --> ${cellnoPratica}  non e' stato inserito il valore assicurato", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        } else {
                                            if(premioLordo==0.0){
                                                def rispostacaricazero=caricapratichePremioZero(tokens,numeropacchetti)
                                                //println "entro qui valore rispostacaricazero: ${rispostacaricazero.polizzezero}"
                                                if(rispostacaricazero.polizzezero!=null  && rispostacaricazero.polizzezero!=''){
                                                    praticheCVT.add("LA POLIZZA con premio zero-->${cellnoPratica}-- ${rispostacaricazero.polizzezero}")
                                                    totaleCVTRCA+=rispostacaricazero.totaleCVTRCA
                                                    totaleRCA+=rispostacaricazero.totaleRCA
                                                    totale+=rispostacaricazero.totale
                                                    //println "totale--> $totale"
                                                }
                                                if(rispostacaricazero.errorePratiche!=null && rispostacaricazero.errorePratiche!=''){
                                                    println "risposta errore: ${rispostacaricazero.errorePratiche}"
                                                    errorepraticheCVT.add("LA POLIZZA con premio zero:${cellnoPratica} ha dato i seguenti errori-> ${rispostacaricazero.errorePratiche}")
                                                    totaleErr+=rispostacaricazero.totaleErr
                                                    totaleErrRCA+=rispostacaricazero.totaleErrRCA
                                                    totaleErrCVTRCA+=rispostacaricazero.totaleErrCVTRCA
                                                    println "totaleErr--> $totaleErr"
                                                    println "totaleErrRCA--> $totaleErrRCA"
                                                    println "totaleErr--> $totaleErr"
                                                    println "totaleErrCVTRCA--> $totaleErrCVTRCA"
                                                }
                                                if(numeropacchetti==1 && pacchettoP!='F'){

                                                }else{

                                                }


                                            }else{
                                                if(pacchettoP!='F'){
                                                    logg = new Log(parametri: "Errore caricamento polizza:${cellnoPratica} la somma delle provviggioni-> ${totProvvi} non corrisponde con la percentuale imponibile dello step assegnato-> ${calcoloImp}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    errorepraticheCVT.add("POLIZZA CVT->Errore caricamento polizza:${cellnoPratica} la somma delle provviggioni-> ${totProvvi} non corrisponde con la percentuale imponibile dello step assegnato-> ${calcoloImp},")
                                                    totaleErr++
                                                }else{
                                                    logg = new Log(parametri: "Errore caricamento polizza:${cellnoPratica} la somma delle provviggioni-> ${totProvvi} non corrisponde con la percentuale imponibile dello step assegnato-> ${calcoloImp}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    errorepraticheCVT.add("POLIZZA RCA->Errore caricamento polizza:${cellnoPratica} la somma delle provviggioni-> ${totProvvi} non corrisponde con la percentuale imponibile assegnato-> ${calcoloImp},")
                                                    totaleErrRCA++
                                                }
                                            }


                                        }
                                } else if (!dealer) {
                                    logg = new Log(parametri: "Errore caricamento polizza: il dealer non \u00E8 presente", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    errorePraticheCVTRCA.add("Errore caricamento polizza: il dealer ${cellaDealer}- ${cellaNomeDealer} non \u00E8 presente,")
                                    totaleErrCVTRCA++
                                } else if (!cellnoPratica) {
                                    logg = new Log(parametri: "Errore caricamento polizza: non \u00E8 presente un numero di pratica", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    errorePraticheCVTRCA.add("Errore caricamento polizza: non \u00E8 presente un numero di polizza,")
                                    totaleErr++
                                }
                            } else {
                                logg = new Log(parametri: "POLIZZE CVT/RCA Errore caricamento polizza:${cellnoPratica} la provincia non \u00E8 stata dichiarata, controllare il file", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                errorePraticheCVTRCA.add("POLIZZE CVT/RCA Errore caricamento polizza:${cellnoPratica} la provincia non \u00E8 stata dichiarata, controllare il file,")
                                totaleErrCVTRCA++
                            }
                        }
                    }
                }
            }
        }

        /*}catch (e){
            logg = new Log(parametri: "c'e' un problema con il file, ${e.toString()}", operazione: "caricamento polizze FIN/LEASING tramite csv", pagina: "POLIZZE FIN/LEASING")
            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "c'e' un problema con il file, ${e.toString()}"
            errorePraticheCVTRCA.add( "c'e' un problema con il file, ${e.toString()}")
        }*/
        if (praticheCVT.size() > 0) {
            praticheCVT.collect { commento ->
                rispostaMail = rispostaMail + "\n polizze CVT: " + commento + "\r\n"
            }.grep().join("\n")
        }
        if (praticheCVT_RCA.size() > 0) {
            praticheCVT_RCA.collect { commento ->
                rispostaMail = rispostaMail + "\n polizze CVT abbinate a RCA: " + commento + "\r\n"
            }.grep().join("\n")
        }
        if (praticheRCA.size() > 0) {
            praticheRCA.collect { commento ->
                rispostaMail = rispostaMail + "\n polizze RCA: " + commento + "\r\n"
            }.grep().join("\n")
        }
        if(errorepraticheCVT.size()>0){
            errorepraticheCVT.collect { commento ->
                rispostaMail = rispostaMail + " " + commento + "\r\n"
            }.grep().join("\n")

        }
        if(errorePraticheCVTRCA.size()>0){
            errorePraticheCVTRCA.collect { commento ->
                rispostaMail = rispostaMail + " " + commento + "\r\n"
            }.grep().join("\n")

        }
        if(errorePraticheRCA.size()>0){
            errorePraticheRCA.collect { commento ->
                rispostaMail = rispostaMail + " " + commento + "\r\n"
            }.grep().join("\n")

        }
       /* if(praticheCVT.size()>0){
            rispostaMail=rispostaMail+=" totale polizze CVT caricate: ${totale} \n "
        }
        if(praticheCVT_RCA.size()>0){
            rispostaMail=rispostaMail+=" totale polizze CVT con RCA caricate: ${totaleCVTRCA/2} \n "
        }
        if(praticheRCA.size()>0){
            rispostaMail=rispostaMail+=" totale polizze RCA caricate: ${totaleRCA} \n "
        }
        if(errorepraticheCVT.size()>0){
            rispostaMail=rispostaMail+=" totale polizze CVT  non caricate: ${totaleErr} \n "
        }
        if(errorePraticheCVTRCA.size()>0){
            rispostaMail=rispostaMail+=" totale polizze CVT con RCA non caricate: ${totaleErrCVTRCA/2} \n "
        }
        if(errorePraticheRCA.size()>0){
            rispostaMail=rispostaMail+=" totale polizze RCA non caricate: ${totaleErrRCA} \n "
        }*/
        if(errorepraticheCVT.size()>0 || errorePraticheRCA.size()>0 || errorePraticheCVTRCA.size()>0 || (praticheCVT.size()>0) || (praticheCVT_RCA.size()>0) || (praticheRCA.size()>0) ){
            /***INVIO LA MAIL A mach1 con il riepilogo dei certificati generati e i campioni dei certificati*/
            def inviaMailCaricamento = mailService.invioMailCaricamentoPolizzaFinLeas(rispostaMail, fileNameRisposta)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto dello stato del caricamento delle polizze \u00E8 stata inviata", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail con il riassunto dello stato del caricamento delle polizze", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Non e' stato possibile inviare la mail con il riassunto dello stato del caricamento delle polizze \n ${inviaMailCaricamento}"
            }
        }else {
            def inviaMailCaricamento = mailService.invioMailCaricamentoPolizzaFinLeas(rispostaMail, fileNameRisposta)
            if (inviaMailCaricamento.contains("queued") || inviaMailCaricamento.contains("sent") || inviaMailCaricamento.contains("scheduled")) {
                logg = new Log(parametri: "la mail con il riassunto del caricamento delle polizze \u00E8 stata inviata", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            } else {
                logg = new Log(parametri: "Non \u00E8 stato possibile inviare la mail la mail con il riassunto del caricamento delle polizze", operazione: "invio riassunto polizze Fin/Leasing caricate", pagina: "Polizza Fin/Leasing")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "Non e' stato possibile inviare la mail la mail con il riassunto del caricamento delle polizze \n ${inviaMailCaricamento}"
            }
        }
    }
    def caricapratichePremioZero(tokens, numeropacchetti){

        def speciale=false
        def errorepraticheCVT=[], errorePraticheRCA=[], errorePraticheCVTRCA=[], flussiPAI=[]
        def praticheRCA=[], praticheCVT=[], praticheCVT_RCA=[]
        def dataPAIFin=new Date().parse("yyyyMMdd","20160930")
        def dataPAIIni=new Date().parse("yyyyMMdd","20160701")
        def totale=0
        def totaleCVTRCA=0
        def totaleCVTRCA2=0
        def totaleRCA=0
        def totaleErr=0
        def totaleErrCVTRCA=0
        def totaleErrCVTRCA2=0
        def totaleErrRCA=0
        def logg
        boolean isPai=false
        def rispostaMail = ""
        def rispostaErrori = ""
        def polizzeDeleted =[]
        def dealer
       // try {

        logg = new Log(parametri: " polizze CVT E RCA con premio zero--> ", operazione: "caricamento polizze premio zero csv", pagina: "POLIZZE FIN/LEASING")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def rispostaWebservice
            boolean sup75 = false
            boolean isDeleted = false
            def cellnoPratica = tokens[1].toString().trim().replaceAll("'", "")
            if(numeropacchetti>0){
                def cellType = tokens[0].toString().trim()
                def cellDataDecorrenza = tokens[2].toString().trim()
                def cellaDealer = tokens[3].toString().trim()
                def cellaNomeDealer = tokens[4].toString().trim()
                def cellaCognome = tokens[5].toString().trim()
                def cellaNome = tokens[6].toString().trim()
                def cellaBeneficiario = tokens[61].toString().trim().replaceAll("'", "")
                def cellaCF = tokens[7].toString().trim().replaceAll("'", "")
                def cellaTel = tokens[8].toString().trim().replaceAll("'", "")
                def cellaCel = tokens[9].toString().trim().replaceAll("'", "")
                def cellaLocalita = tokens[10].toString().trim().replaceAll("'", "")
                def cellaIndirizzo = tokens[11].toString().trim().replaceAll("'", "")
                def cellaProvincia = tokens[12].toString().trim().replaceAll("'", "")
                def cellaCap = tokens[13].toString().trim().replaceAll("'", "")
                def cellaPIva = tokens[15].toString().trim().replaceAll("'", "")
                def cellaEmail = tokens[16].toString().trim().replaceAll("'", "")
                def cellaCoobligatoCognome = tokens[17].toString().trim().replaceAll("'", "")
                def cellaCoobligatoNome = tokens[18].toString().trim().replaceAll("'", "")
                def cellaCoobligatoCF = tokens[19].toString().trim().replaceAll("'", "")
                def cellaCoobligatoTEL = tokens[20].toString().trim().replaceAll("'", "")
                def cellaCoobligatoCEL = tokens[21].toString().trim().replaceAll("'", "")
                def cellaCoobligatoLOC = tokens[22].toString().trim().replaceAll("'", "")
                def cellaCoobligatoIND = tokens[23].toString().trim().replaceAll("'", "")
                def cellaCoobligatoPROV = tokens[24].toString().trim().replaceAll("'", "")
                def cellaCoobligatoCAP = tokens[25].toString().trim().replaceAll("'", "")
                def cellaCoobligatoemail = tokens[28].toString().trim().replaceAll("'", "")
                def cellaDenominazione = tokens[29].toString().trim().replaceAll("'", "")
                def cellaTelefonoBusiness = tokens[31].toString().trim().replaceAll("'", "")
                def cellaIndirizzoBusiness = tokens[32].toString().trim().replaceAll("'", "")
                def cellaCellulareBusiness = tokens[33].toString().trim().replaceAll("'", "")
                def cellaProvinciaBusiness = tokens[34].toString().trim().replaceAll("'", "")
                def cellaLocalitaBusiness = tokens[22].toString().trim().replaceAll("'", "")
                def cellaCapBusiness = tokens[35].toString().trim().replaceAll("'", "")
                def cellaPIvaBusiness = tokens[36].toString().trim().replaceAll("'", "")
                def cellaCFBusiness = tokens[37].toString().trim().replaceAll("'", "")
                def cellaEmailBusiness = tokens[38].toString().trim().replaceAll("'", "")
                def cellaBeneFinanziato = tokens[39].toString().trim().replaceAll("'", "")
                def cellaDataImmatr = tokens[40].toString().trim().replaceAll("'", "")
                def cellaMarca = tokens[41].toString().trim().replaceAll("'", "")
                def cellaModello = tokens[42].toString().trim().replaceAll("'", "")
                def cellaVersione = tokens[43].toString().trim().replaceAll("'", "")
                def cellaTelaio = tokens[45].toString().trim().replaceAll("'", "")
                def cellaTarga = tokens[46].toString().trim().replaceAll("'", "")
                def cellaPrezzo = tokens[47].toString().trim().replaceAll("'", "")
                def cellacvtinsurance = tokens[48].toString().trim().replaceAll("'", "")
                def cellaStep = tokens[49].toString().trim().replaceAll("'", "")
                def cellaProdotto = tokens[51].toString().trim().replaceAll("'", "")
                def cellaDurata = tokens[52].toString().trim().replaceAll("'", "")
                def cellaValoreAssi = tokens[53].toString().trim().replaceAll("'", "")
                def cellaPacchetto = tokens[54].toString().trim().replaceAll("'", "")
                def cellaPremioImpo = tokens[56].toString().trim().replaceAll("'", "")
                def cellaPremioLordo = tokens[57].toString().trim().replaceAll("'", "")
                def cellaProvvDealer = tokens[58].toString().trim().replaceAll("'", "")
                def cellaProvvVenditore = tokens[59].toString().trim().replaceAll("'", "")
                def cellaProvvGMFI = tokens[60].toString().trim().replaceAll("'", "")
                def cellaCertificato = tokens[62].toString().trim().replaceAll("'", "")
                def cellaVenditore = tokens[65].toString().trim().replaceAll("'", "")
                def cellatipoPolizza = tokens[68].toString().trim().replaceAll("'", "")
                def cellacodCampagna = tokens[69].toString().trim().replaceAll("'", "")
                def dataDecorrenza = null
                if (cellDataDecorrenza.toString().trim() != 'null' && cellDataDecorrenza.toString().length() > 0) {
                    def newDate = Date.parse('yyyyMMdd', cellDataDecorrenza)
                    dataDecorrenza = newDate
                    if ((dataDecorrenza) && (dataDecorrenza >= dataPAIIni && dataDecorrenza <= dataPAIFin)) {
                        isPai = true
                    } else {
                        isPai = false
                    }
                }
                if (cellnoPratica.toString().equals("null")) {
                    cellnoPratica = ""
                } else {
                    cellnoPratica = cellnoPratica.toString()
                    cellnoPratica = cellnoPratica.toString().replaceFirst("^0*", "")
                    def indexslash = cellnoPratica.indexOf("/")
                    if (indexslash > 0) {
                        cellnoPratica = cellnoPratica.toString().substring(0, indexslash)
                    }
                    if (cellnoPratica.toString().contains(".")) {
                        def punto = cellnoPratica.toString().indexOf(".")
                        cellnoPratica = cellnoPratica.toString().substring(0, punto).replaceAll("[^0-9]", "")
                    }
                }
                def codOperazione = "0"
                if (cellType.toString().trim() != 'null') {
                    if (cellType.toString().trim().equalsIgnoreCase("NEW")) {
                        codOperazione = "0"
                    } else if (cellType.toString().trim().equalsIgnoreCase("DELETED")) {
                        isDeleted = true
                    } else if (cellType.toString().trim().equalsIgnoreCase("CLOSED")) {
                        codOperazione = "0"
                    } else {
                        codOperazione = "S"
                    }
                }
                def coperturaR
                def pacchettoP = ""
                if (cellaProdotto.toString().trim() != 'null' && cellaPacchetto.toString().trim().length() > 0) {
                    def indexdue = cellaProdotto.toString().trim().indexOf("-")
                    pacchettoP = cellaProdotto.toString().trim().substring(0, indexdue)
                    pacchettoP = pacchettoP.toString().trim()
                    if (pacchettoP == "A") {
                        coperturaR = "SILVER"
                    } else if (pacchettoP == "B") {
                        coperturaR = "GOLD"
                    } else if (pacchettoP == "C") {
                        coperturaR = "PLATINUM"
                    } else if (pacchettoP == "D") {
                        coperturaR = "PLATINUM COLLISIONE"
                    } else if (pacchettoP == "E") {
                        coperturaR = "PLATINUM KASKO"
                    }else if (pacchettoP == "F") {
                        coperturaR = "F"
                    }
                }
                if (isDeleted && cellnoPratica != "") {
                    polizzeDeleted.add("${cellnoPratica},")
                    logg = new Log(parametri: "Errore creazione polizza con premio zero: la polizza e' tipo DELETED", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    if(pacchettoP=="F"){
                        errorePraticheRCA.add("Errore creazione polizza con premio zero: la polizza ${cellnoPratica} e' tipo DELETED,")
                        totaleErrRCA++


                    }else{
                        errorepraticheCVT.add("Errore creazione polizza con premio zero: la polizza ${cellnoPratica} e' tipo DELETED,")
                        totaleErr++


                    }
                } else if (!cellnoPratica) {
                    logg = new Log(parametri: "Errore creazione polizza con premio zero: non \u00E8 presente un numero di pratica", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    if(pacchettoP=="F"){
                        errorePraticheRCA.add("Errore creazione polizza con premio zero: non \u00E8 presente un numero di polizza,")
                        totaleErrRCA++
                    }else{
                        errorepraticheCVT.add("Errore creazione polizza con premio zero: non \u00E8 presente un numero di polizza,")
                        totaleErr++
                    }
                } else {
                    if (cellaNome.toString().equals("null")) {
                        cellaNome = ""
                    } else {
                        cellaNome = cellaNome.toString().trim()
                    }
                    if (cellaCognome.toString().equals("null")) {
                        cellaCognome = ""
                    } else {
                        cellaCognome = cellaCognome.toString().trim()
                    }
                    if (cellaCap.toString().equals("null")) {
                        cellaCap = ""
                    } else {
                        cellaCap = cellaCap.toString().replaceAll("'", "")
                        if (cellaCap.toString().contains(".")) {
                            def punto = cellaCap.toString().indexOf(".")
                            cellaCap = cellaCap.toString().substring(0, punto)
                        }
                    }
                    if (cellaDealer.toString().equals("null")) {
                        cellaDealer = ""
                    } else {
                        cellaDealer = cellaDealer.toString().replaceAll("'", "")
                        cellaDealer = cellaDealer.toString().replaceFirst("^0*", "")
                        if (cellaDealer.toString().contains(".")) {
                            cellaDealer = Double.valueOf(cellaDealer).longValue().toString()
                            //cellaDealer=cellaDealer.toString().substring(0,punto).replaceAll("[^0-9]", "")
                        }
                        dealer = Dealer.findByCodice(cellaDealer.toString())
                    }
                    if (cellaCF.toString().equals("null")) {
                        cellaCF = cellaPIva.toString().replaceAll("[^a-z,A-Z,0-9]", "")
                    } else {
                        cellaCF = cellaCF.toString().replaceAll("[^a-z,A-Z,0-9]", "")
                    }
                    if (cellaMarca.toString().equals("null")) {
                        cellaMarca = null
                    } else {
                        cellaMarca = cellaMarca.toString().trim().toUpperCase()
                    }
                    if (cellaModello.toString().equals("null")) {
                        cellaModello = null
                    } else {
                        cellaModello = cellaModello.toString().trim().toUpperCase()
                    }
                    if (cellaTel.toString().trim() != 'null') {
                        cellaTel = cellaTel.toString().trim()
                        if (cellaTel.contains(".")) {
                            int zero = cellaTel.toString().indexOf(".")
                            cellaTel = cellaTel.toString().substring(0, zero)
                        }
                    } else {
                        cellaTel = null
                    }
                    if (cellaTelefonoBusiness.toString().trim() != 'null') {
                        cellaTelefonoBusiness = cellaTelefonoBusiness.toString().trim()
                        if (cellaTelefonoBusiness.contains(".")) {
                            int zero = cellaTelefonoBusiness.toString().indexOf(".")
                            cellaTelefonoBusiness = cellaTelefonoBusiness.toString().substring(0, zero)
                        }

                    } else {
                        cellaTelefonoBusiness = null
                    }
                    if (cellaCel.toString().trim() != 'null') {
                        cellaCel = cellaCel.toString().trim().replaceAll("[^0-9]+", "")
                        if (cellaCel.toString().contains(".")) {
                            int zero = cellaCel.toString().indexOf(".")
                            cellaCel = cellaCel.toString().substring(0, zero)
                        }
                    } else {
                        cellaCel = null
                    }
                    if (cellaCellulareBusiness.toString().trim() != 'null') {
                        cellaCellulareBusiness = cellaCellulareBusiness.toString().trim().replaceAll("[^0-9]+", "")
                        if (cellaCellulareBusiness.toString().contains(".")) {
                            int zero = cellaCellulareBusiness.toString().indexOf(".")
                            cellaCellulareBusiness = cellaCellulareBusiness.toString().substring(0, zero)
                        }
                    } else {
                        cellaCellulareBusiness = null
                    }

                    def nuovo = false
                    if (cellaBeneFinanziato.toString().trim() != 'null') {
                        if (cellaBeneFinanziato.toString().trim().equalsIgnoreCase("NEW")) {
                            nuovo = true
                        } else {
                            nuovo = false
                        }
                    }
                    def polizzaTipo = ""
                    def provinciaimma=""
                    if (cellatipoPolizza.toString().trim() != 'null') {
                        if (cellatipoPolizza.toString().trim().equalsIgnoreCase("RETAIL")) {
                            polizzaTipo = TipoPolizza.FINANZIATE
                        } else {
                            polizzaTipo = TipoPolizza.LEASING
                            provinciaimma="RM"
                        }
                    }
                    def codCampagna = ""
                    if (cellacodCampagna.toString().trim() != 'null') {
                        if (cellacodCampagna.toString().trim().equalsIgnoreCase("0")) {
                            codCampagna = "0"
                        } else {
                            codCampagna = cellacodCampagna.toString().trim()
                        }
                    }
                    boolean onStar = false
                    boolean certificato = false
                    if (cellaCertificato.toString().trim() != '') {
                        if (cellaCertificato.toString().trim() != "N") {
                            certificato = true
                        } else {
                            certificato = false
                        }
                    }
                    if (cellaTelaio.toString().trim() != '') {
                        cellaTelaio = cellaTelaio.toString().trim().toUpperCase()
                    } else {
                        cellaTelaio = null
                    }
                    if (cellaTarga.toString().trim() != '') {
                        cellaTarga = cellaTarga.toString().trim().toUpperCase()
                    } else {
                        cellaTarga = null
                    }
                    if (cellaEmail.toString().trim() != '') {
                        cellaEmail = cellaEmail.toString().trim()
                    } else {
                        cellaEmail = null
                    }
                    def dataScadenza
                    def durata
                    if (cellaDurata.toString().trim() != 'null' && cellaDurata.toString().trim().length() > 0) {
                        if (cellaDurata.toString().contains(".")) {
                            def punto = cellaDurata.toString().indexOf(".")
                            cellaDurata = cellaDurata.toString().substring(0, punto).replaceAll("[^0-9]", "")
                        }
                        durata = Integer.parseInt(cellaDurata.toString().trim())
                    } else {
                        durata = null
                    }

                    if(numeropacchetti==1 && pacchettoP=="F"){
                        if (durata != null && dataDecorrenza != null) {
                            dataScadenza = use(TimeCategory) { dataDecorrenza + 12.months }
                        }
                    }else{
                        if (durata != null && dataDecorrenza != null) {
                            dataScadenza=assegnascadenza(durata, dataDecorrenza)
                        }
                    }

                    def step, codStep
                    if (cellaStep.toString().trim() != 'null') {
                        step = cellaStep.toString().trim()
                        if (step.contains(".")) {
                            int zero = step.toString().indexOf(".")
                            step = step.toString().substring(0, zero).toLowerCase()
                        }
                        if (step == "1") {
                            codStep = "step 1"
                        } else if (step == "2") {
                            codStep = "step 2"
                        } else if (step == "3") {
                            codStep = "step 3"
                        }
                    } else {
                        codStep = null
                    }
                    def dataImmatricolazione = null
                    if (cellaDataImmatr.toString().trim() != 'null' && cellaDataImmatr.toString().length() > 0) {
                        def newDate = Date.parse('yyyyMMdd', cellaDataImmatr)
                        dataImmatricolazione = newDate
                    }
                    def valoreAssicurato
                    if (cellaValoreAssi.toString().trim() != 'null' && cellaValoreAssi.toString().length() > 0) {
                        valoreAssicurato = cellaValoreAssi.toString().replaceAll("\\.", "").replaceAll(",", "\\.")
                        //valoreAssicurato=cellaValoreAssi.toString().replaceAll(",","\\.")
                        valoreAssicurato = new BigDecimal(valoreAssicurato)
                    } else {
                        valoreAssicurato = 0.0
                    }
                    def prezzoVendita
                    if (cellaPrezzo.toString().trim() != 'null' && cellaPrezzo.toString().length() > 0) {
                        prezzoVendita = cellaPrezzo.toString().replaceAll("\\.", "").replaceAll(",", "\\.")
                        //prezzoVendita=prezzoVendita.toString().replaceAll(",","\\.")
                        prezzoVendita = new BigDecimal(prezzoVendita)
                    } else {
                        prezzoVendita = 0.0
                    }
                    def premioImponibile
                    if (cellaPremioImpo.toString().trim() != 'null' && cellaPremioImpo.toString().trim().length() > 0) {
                        premioImponibile = cellaPremioImpo.toString().trim().replaceAll("\\.", "").replaceAll(",", "\\.")
                        //premioImponibile=cellaPremioImpo.toString().trim().replaceAll(",","\\.")
                        premioImponibile = new BigDecimal(premioImponibile)
                    } else {
                        premioImponibile = 0.0
                    }
                    def premioLordo
                    if (cellaPremioLordo.toString().trim() != 'null' && cellaPremioLordo.toString().trim().length() > 0) {
                        premioLordo = cellaPremioLordo.toString().trim().replaceAll("\\.", "").replaceAll(",", "\\.")
                        // premioLordo=cellaPremioLordo.toString().trim().replaceAll(",","\\.")
                        premioLordo = new BigDecimal(premioLordo)
                    } else {
                        premioLordo = 0.0
                    }
                    def cvtinsurance
                    if (cellacvtinsurance.toString().trim() != 'null' && cellacvtinsurance.toString().trim().length() > 0) {
                        cvtinsurance = cellacvtinsurance.toString().trim().replaceAll("\\.", "").replaceAll(",", "\\.")
                        // premioLordo=cellaPremioLordo.toString().trim().replaceAll(",","\\.")
                        cvtinsurance = new BigDecimal(cvtinsurance)
                    } else {
                        cvtinsurance = 0.0
                    }
                    def provvDealer
                    if (cellaProvvDealer.toString().trim() != 'null' && cellaProvvDealer.toString().trim().length() > 0) {
                        provvDealer = cellaProvvDealer.toString().trim().replaceAll("\\.", "").replaceAll(",", "\\.")
                        //provvDealer=cellaProvvDealer.toString().trim().replaceAll(",","\\.")
                        provvDealer = new BigDecimal(provvDealer)
                    } else {
                        provvDealer = 0.0
                    }
                    def provvGmfi
                    if (cellaProvvGMFI.toString().trim() != 'null' && cellaProvvGMFI.toString().trim().length() > 0) {
                        provvGmfi = cellaProvvGMFI.toString().trim().replaceAll("\\.", "").replaceAll(",", ".")
                        //provvGmfi=cellaProvvGMFI.toString().trim().replaceAll(",",".")
                        provvGmfi = new BigDecimal(provvGmfi)
                    } else {
                        provvGmfi = 0.0
                    }
                    def provvVendi
                    if (cellaProvvVenditore.toString().trim() != '' && cellaProvvVenditore.toString().trim().length() > 0 && cellaProvvVenditore.toString().trim() != '0') {
                        provvVendi = cellaProvvVenditore.toString().trim().replaceAll("\\.", "").replaceAll(",", "\\.")
                        //provvVendi=cellaProvvVenditore.toString().trim().replaceAll(",","\\.")
                        provvVendi = new BigDecimal(provvVendi)
                    } else {
                        provvVendi = 0.0
                    }
                    if (cellaVenditore.toString() != 'null') {
                        cellaVenditore = cellaVenditore.toString().trim()
                    } else {
                        cellaVenditore = ""
                    }
                    def dataInserimento = new Date()
                    def premioLordoSAntifurto = ""
                    def premioLordoAntifurto = ""
                    dataInserimento = dataInserimento.clearTime()
                    def parole = []
                    def nomeB = "", cognomeB = ""
                    def cognomeDef, nomeDef, provinciaDef, localitaDef, capDef, telDef, cellDef, cfDef, indirizzoDef, tipoClienteDef, annoCF
                    def emailDef = ""
                    boolean denominazione = false
                    boolean coobligato = false
                    boolean cliente = false
                    boolean nonTrovato = false


                    if (cellaBeneficiario.toString() != '' && pacchettoP!="F") {
                        cellaBeneficiario = cellaBeneficiario.toUpperCase()
                        logg = new Log(parametri: "polizza ${cellnoPratica} con premio zero beneficiario-->${cellaBeneficiario}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        if (cellaDenominazione) {
                            cellaDenominazione = cellaDenominazione.toUpperCase().trim()
                            if (cellaDenominazione.contains(cellaBeneficiario)) {
                                logg = new Log(parametri: "polizza ${cellnoPratica} con premio zero denominazione-->${cellaDenominazione}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                denominazione = true

                            } else {
                                denominazione = false
                            }
                        }
                        if (cellaCoobligatoNome) {
                            cellaCoobligatoNome = cellaCoobligatoNome.toUpperCase().trim()
                            cellaCoobligatoCognome = cellaCoobligatoCognome.toUpperCase().trim()
                            def nomeCooC = cellaCoobligatoNome + " " + cellaCoobligatoCognome
                            if (nomeCooC.contains(cellaBeneficiario)) {
                                logg = new Log(parametri: "polizza ${cellnoPratica} con premio zero nome Coobligato-->${nomeCooC}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                coobligato = true
                            } else {
                                coobligato = false
                            }
                        }
                        if (cellaNome) {
                            cellaNome = cellaNome.toUpperCase().trim()
                            cellaCognome = cellaCognome.toUpperCase().trim()
                            def nomeCom = cellaNome + " " + cellaCognome
                            //println "nome Cliente-->${nomeCom} beneficiario--> ${cellaBeneficiario}"

                            if (nomeCom.toString().replaceAll("'","").contains(cellaBeneficiario)) {
                                //println "nome Cliente-->${nomeCom} beneficiario--> ${cellaBeneficiario}"
                                logg = new Log(parametri: "polizza ${cellnoPratica} con premio zero nome Cliente-->${nomeCom} beneficiario--> ${cellaBeneficiario}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                cliente = true
                            } else {
                                cliente = false
                            }
                        }
                        if (coobligato) {
                            logg = new Log(parametri: "polizza ${cellnoPratica} con premio zero prendo i dati del Coobligato-->${cellaCoobligatoNome}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            cognomeDef = cellaCoobligatoCognome.toString().trim().toUpperCase()
                            nomeDef = cellaCoobligatoNome.toString().trim().toUpperCase()
                            provinciaDef = cellaCoobligatoPROV.toString().trim().toUpperCase()
                            localitaDef = cellaCoobligatoLOC.toString().trim().toUpperCase()
                            capDef = cellaCoobligatoCAP.toString().trim().toUpperCase()
                            telDef = cellaCoobligatoTEL.toString().trim().toUpperCase()
                            cellDef = cellaCoobligatoCEL.toString().trim().toUpperCase()
                            cfDef = cellaCoobligatoCF.toString().trim().toUpperCase()
                            //prendo i dati dell'anno dal codice fiscale
                            if (cfDef) {
                                annoCF = cfDef.substring(6, 8)
                                if (Integer.parseInt(annoCF) && Integer.parseInt(annoCF) <= 40) {
                                    sup75 = true
                                }
                                def sessC=cfDef.substring(9,11)
                                if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                    tipoClienteDef=TipoCliente.F
                                }else{
                                    tipoClienteDef=TipoCliente.M
                                }
                            }
                            indirizzoDef = cellaCoobligatoIND.toString().trim().toUpperCase()
                            if (cellaCoobligatoemail.toString().trim().size() > 4) {
                                emailDef = cellaCoobligatoemail.toString().trim()
                            }
                        } else if (denominazione) {
                            logg = new Log(parametri: "polizza ${cellnoPratica} con premio zero prendo i dati della denominazione -->${cellaDenominazione}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            cognomeDef = cellaDenominazione.toString().trim().toUpperCase()
                            nomeDef = ""
                            provinciaDef = cellaProvinciaBusiness.toString().trim().toUpperCase()
                            localitaDef = cellaLocalitaBusiness.toString().trim().toUpperCase()
                            capDef = cellaCapBusiness.toString().trim().toUpperCase()
                            telDef = cellaTelefonoBusiness.toString().trim().toUpperCase()
                            cellDef = cellaCellulareBusiness.toString().trim().toUpperCase()
                            if (cellaPIvaBusiness) {
                                cfDef = cellaPIvaBusiness.toString().trim().toUpperCase()
                                if (!cfDef.matches("[0-9]{11}")) {
                                    cfDef = cfDef.padLeft(11, "0")
                                }

                            }
                            indirizzoDef = cellaIndirizzoBusiness.toString().trim().toUpperCase()
                            tipoClienteDef = TipoCliente.DITTA_INDIVIDUALE
                            if (cellaEmailBusiness.toString().trim().size() > 4) {
                                emailDef = cellaEmailBusiness.toString().trim()
                            }
                        } else if (cliente) {
                            logg = new Log(parametri: "polizza ${cellnoPratica} con premio zero prendo i dati del cliente -->${cellaNome}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            cognomeDef = cellaCognome.toString().trim().toUpperCase()
                            nomeDef = cellaNome.toString().trim().toUpperCase()
                            provinciaDef = cellaProvincia.toString().trim().toUpperCase()
                            localitaDef = cellaLocalita.toString().trim().toUpperCase()
                            capDef = cellaCap.toString().trim().toUpperCase()
                            telDef = cellaTel.toString().trim().toUpperCase()
                            cellDef = cellaCel.toString().trim().toUpperCase()
                            cfDef = cellaCF.toString().trim().toUpperCase()
                            if (cfDef) {
                                annoCF = cfDef.substring(6, 8)
                                if (Integer.parseInt(annoCF) && Integer.parseInt(annoCF) <= 40) {
                                    sup75 = true
                                }
                                def sessC=cfDef.substring(9,11)
                                if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                    tipoClienteDef=TipoCliente.F
                                }else{
                                    tipoClienteDef=TipoCliente.M
                                }
                            }
                            indirizzoDef = cellaIndirizzo.toString().trim().toUpperCase()
                            if (cellaEmail.toString().trim().size() > 4) {
                                emailDef = cellaEmail.toString().trim()
                            }

                        } else {
                            nonTrovato = true
                        }
                    }else{
                        logg = new Log(parametri: "polizza ${cellnoPratica} con premio zero e' una polizza RCA prendo i dati del cliente -->${cellaNome}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        if(cellaCognome!="" && cellaNome!=""){
                            cognomeDef = cellaCognome.toString().trim().toUpperCase()
                            nomeDef = cellaNome.toString().trim().toUpperCase()
                            provinciaDef = cellaProvincia.toString().trim().toUpperCase()
                            localitaDef = cellaLocalita.toString().trim().toUpperCase()
                            capDef = cellaCap.toString().trim().toUpperCase()
                            telDef = cellaTel.toString().trim().toUpperCase()
                            cellDef = cellaCel.toString().trim().toUpperCase()
                            cfDef = cellaCF.toString().trim().toUpperCase()
                            //prendo i dati dell'anno dal codice fiscale
                            if (cfDef) {
                                annoCF = cfDef.substring(6, 8)
                                if (Integer.parseInt(annoCF) && Integer.parseInt(annoCF) <= 40) {
                                    sup75 = true
                                }
                                def sessC=cfDef.substring(9,11)
                                if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                    tipoClienteDef=TipoCliente.F
                                }else{
                                    tipoClienteDef=TipoCliente.M
                                }
                            }
                        }else if (!(cellaDenominazione.toString().equals("null"))){
                            logg = new Log(parametri: "POLIZZA CON PREMIO ZERO e' una polizza RCA prendo i dati della denominazione -->${cellaDenominazione}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            cognomeDef = cellaDenominazione.toString().trim().toUpperCase()
                            nomeDef = ""
                            provinciaDef = cellaProvinciaBusiness.toString().trim().toUpperCase()
                            localitaDef = cellaLocalitaBusiness.toString().trim().toUpperCase()
                            capDef = cellaCapBusiness.toString().trim().toUpperCase()
                            telDef = cellaTelefonoBusiness.toString().trim().toUpperCase()
                            cellDef = cellaCellulareBusiness.toString().trim().toUpperCase()
                            if (cellaPIvaBusiness) {
                                cfDef = cellaPIvaBusiness.toString().trim().toUpperCase()
                                if (!cfDef.matches("[0-9]{11}")) {
                                    cfDef = cfDef.padLeft(11, "0")
                                }

                            }
                            tipoClienteDef = TipoCliente.DITTA_INDIVIDUALE
                            if (cellaEmailBusiness.toString().trim().size() > 4) {
                                emailDef = cellaEmailBusiness.toString().trim()
                            }
                        }
                        if (cfDef) {
                            annoCF = cfDef.substring(6, 8)
                            if (Integer.parseInt(annoCF) && Integer.parseInt(annoCF) <= 40) {
                                sup75 = true
                            }
                            def sessC=cfDef.substring(9,11)
                            if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                tipoClienteDef=TipoCliente.F
                            }else{
                                tipoClienteDef=TipoCliente.M
                            }
                        }
                        indirizzoDef = cellaIndirizzo.toString().trim().toUpperCase()
                        if (cellaEmail.toString().trim().size() > 4) {
                            emailDef = cellaEmail.toString().trim()
                        }
                    }
                    if (nonTrovato  && pacchettoP!='F') {
                        logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero -->${cellnoPratica} il beneficiario non appare nelle colonne dei dati coobligato/cliente/business, controllare file", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        errorepraticheCVT.add("polizze CVT: PER LA POLIZZA con premio zero -->${cellnoPratica} il beneficiario non appare nelle colonne dei dati coobligato/cliente/business, controllare file,")
                        totaleErr++
                    } else {
                        def calcoloImp, totProvvi, diffImpoProvv
                        totProvvi = provvDealer + provvVendi + provvGmfi
                        def provincia = provinciaDef.toString().trim()

                        //verifica tot provv rispetto a step assegnato
                        if ((provincia.trim() != "") && (provincia.trim() != null) && (provincia.trim() != 'null')) {
                            if (dealer && cellnoPratica && cellaTelaio && cellaBeneficiario.toString() != '') {
                               // println "cella $cellnoPratica paccheto $pacchettoP numero $numeropacchetti"
                                        if(numeropacchetti==1 && pacchettoP!='F'){
                                           // println "e' una cvt--> ${cellnoPratica}"
                                            def polizzaesistente = Polizza.findByNoPolizzaIlike(cellnoPratica)
                                            if (polizzaesistente) {
                                                if (codOperazione == "S" && !speciale) {
                                                    if (polizzaesistente.telaio.equalsIgnoreCase(cellaTelaio)) {
                                                        polizzaesistente.codOperazione = codOperazione
                                                        if (polizzaesistente.save(flush: true)) {
                                                            def tracciatoPolizzaR = tracciatiService.generaTracciatoIAssicur(polizzaesistente)
                                                            if (!tracciatoPolizzaR.save(flush: true)) {
                                                                logg = new Log(parametri: "POLIZZE CVT Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                errorepraticheCVT.add("POLIZZE CVT  pratica-->${cellnoPratica} Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors},")
                                                                totaleErr++
                                                            } else {
                                                                def tracciatoComPolizzaR = tracciatiService.generaTracciatoCompagnia(polizzaesistente)
                                                                if (!tracciatoComPolizzaR.save(flush: true)) {
                                                                    logg = new Log(parametri: "POLIZZE CVT Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    errorepraticheCVT.add("POLIZZE CVT pratica-->${cellnoPratica} Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors},")
                                                                    totaleErr++
                                                                } else {
                                                                    if (polizzaesistente.tracciatoPAIId) {
                                                                        def tracciatoPAIPolizzaR = tracciatiService.generaTracciatoPAI(polizzaesistente, speciale)
                                                                        if (!tracciatoPAIPolizzaR.save(flush: true)) {
                                                                            logg = new Log(parametri: "POLIZZE CVT Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            errorepraticheCVT.add("POLIZZE CVT pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors},")
                                                                            totaleErr++
                                                                        } else {
                                                                            if (polizzaesistente.targa != null && polizzaesistente.targa != '') {
                                                                                polizzaesistente.targa = polizzaesistente.targa + "_R"
                                                                            }
                                                                            if (polizzaesistente.telaio != null && polizzaesistente.telaio != '') {
                                                                                polizzaesistente.telaio = polizzaesistente.telaio + "_R"
                                                                            }
                                                                            if (polizzaesistente.save(flush: true)) {
                                                                                logg = new Log(parametri: "POLIZZE CVT targa e telaio per la polizza ${polizzaesistente.noPolizza} aggiornati", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                praticheCVT.add("POLIZZE CVT polizza stornata correttamente ${polizzaesistente.noPolizza},")
                                                                                totale++
                                                                            } else {
                                                                                logg = new Log(parametri: "non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                errorepraticheCVT.add(" POLIZZE CVT pratica-->${cellnoPratica} non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors},")
                                                                                totaleErr++
                                                                            }
                                                                        }
                                                                    } else {
                                                                        if (polizzaesistente.targa != null && polizzaesistente.targa != '') {
                                                                            polizzaesistente.targa = polizzaesistente.targa + "_R"
                                                                        }
                                                                        if (polizzaesistente.telaio != null && polizzaesistente.telaio != '') {
                                                                            polizzaesistente.telaio = polizzaesistente.telaio + "_R"
                                                                        }
                                                                        if (polizzaesistente.save(flush: true)) {
                                                                            logg = new Log(parametri: "POLIZZE CVT targa e telaio per la polizza ${polizzaesistente.noPolizza} aggiornati", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            praticheCVT.add("POLIZZE CVT polizza stornata correttamente ${polizzaesistente.noPolizza},")
                                                                            totale++
                                                                        } else {
                                                                            logg = new Log(parametri: "POLIZZE CVT non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            errorepraticheCVT.add("POLIZZE CVT  pratica-->${cellnoPratica} non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors},")
                                                                            totaleErr++
                                                                        }
                                                                    }

                                                                }
                                                            }
                                                        }
                                                    } else {
                                                        errorepraticheCVT.add("polizze CVT: PER LA POLIZZA con premio zero-->${cellnoPratica} il telaio associato non corrisponde a quello inserito nel DB,")
                                                        totaleErr++
                                                    }
                                                } else if (speciale && !sup75) {
                                                    logg = new Log(parametri: "POLIZZE CVT genero tracciato PAI", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    def tracciatoPAI = tracciatiService.generaTracciatoPAI(polizzaesistente, speciale)
                                                    if (!tracciatoPAI.save(flush: true)) {
                                                        logg = new Log(parametri: "POLIZZE CVT Errore creazione tracciato PAI: ${tracciatoPAI.errors}", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        errorepraticheCVT.add("POLIZZE CVT pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAI.errors},")
                                                        totaleErr++
                                                    } else {
                                                        logg = new Log(parametri: "POLIZZE CVT tracciato PAI generato correttamente per la polizza ${polizzaesistente.noPolizza}", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        flussiPAI.add("POLIZZE CVT tracciato PAI generato correttamente per la polizza ${polizzaesistente.noPolizza},")
                                                        totale++
                                                    }
                                                }else if(sup75){
                                                    errorepraticheCVT.add("polizze CVT: PER LA POLIZZA con premio zero-->${cellnoPratica}  supera la eta' di 75 anni,")
                                                    totaleErr++
                                                }
                                                else {
                                                    errorepraticheCVT.add("polizze CVT: PER LA POLIZZA con premio zero -->${cellnoPratica} \u00E8 gia' stata caricata nel portale,")
                                                    totaleErr++
                                                }
                                            } else {
                                                if (codOperazione == "S") {
                                                    errorepraticheCVT.add("POLIZZE CVT la pratica CVT-->${cellnoPratica} che si sta cercando di stornare non esiste. Verificare no polizza,")
                                                    totaleErr++
                                                } else if (speciale) {
                                                    errorepraticheCVT.add("polizze CVT: PER LA POLIZZA con premio zero-->${cellnoPratica} non esiste e non e' possibile generare un flusso PAI... verificare no polizza,")
                                                    totaleErr++
                                                } else {
                                                    /****hace llamada al ws**/
                                                    def pacchetto=CodProdotti.findByCoperturaAndStep(coperturaR, codStep).codPacchetto
                                                    def percentualeVenditore=dealer.percVenditore
                                                    if(percentualeVenditore.toPlainString()=="0.0000000"){
                                                        percentualeVenditore=0.0
                                                    }
                                                    def dealerType=dealer.dealerType
                                                    if((provincia.trim()!="") && (provincia.trim() !='null') && pacchetto!="" && durata!=null && localitaDef.trim()!="" && localitaDef.trim()!='NULL' && dealerType!="" && step!=""&& percentualeVenditore!=""&& valoreAssicurato!="" && valoreAssicurato > 0.0 ){
                                                        def dataTarriffa='2017-04-21'
                                                        def tipoOperazione
                                                        if(codOperazione=="0"){
                                                            tipoOperazione="A"
                                                        }else{
                                                            tipoOperazione=codOperazione
                                                        }
                                                        rispostaWebservice=DLWebService.chiamataWSFINTAR2(pacchetto,valoreAssicurato,provincia,localitaDef,durata,dealer.dealerType,step, dataTarriffa,tipoOperazione,percentualeVenditore,cvtinsurance, cellnoPratica,polizzaTipo)
                                                       //println "risposta chiamata WS CVT-->: ${rispostaWebservice}"
                                                        logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero risposta chiamata WS CVT-->: ${rispostaWebservice}", operazione: "chiamata WS CVT", pagina: "POLIZZE FIN/LEASING")
                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        if(rispostaWebservice.risposta==true){
                                                            def polizza = Polizza.findOrCreateWhere(
                                                                    annullata: false,
                                                                    cap: capDef.toString().trim().padLeft(5, "0"),
                                                                    cellulare: cellDef.toString().trim(),
                                                                    cognome: cognomeDef.toString().trim(),
                                                                    coperturaRichiesta: coperturaR,
                                                                    dataDecorrenza: dataDecorrenza,
                                                                    dataImmatricolazione: dataImmatricolazione,
                                                                    dataInserimento: dataInserimento,
                                                                    dataScadenza: dataScadenza,
                                                                    dealer: dealer,
                                                                    durata: durata,
                                                                    email: emailDef.toString().trim(),
                                                                    indirizzo: indirizzoDef.toString().trim().toUpperCase(),
                                                                    localita: localitaDef.toString().trim().toUpperCase(),
                                                                    marca: cellaMarca,
                                                                    modello: cellaModello,
                                                                    noPolizza: cellnoPratica,
                                                                    nome: nomeDef.toString().trim().toUpperCase(),
                                                                    nuovo: nuovo,
                                                                    onStar: rispostaWebservice.polizza.onStar,
                                                                    partitaIva: cfDef.toString().trim().toUpperCase(),
                                                                    premioImponibile: rispostaWebservice.polizza.premioImponibile,
                                                                    premioLordo: rispostaWebservice.polizza.premioLordo,
                                                                    provincia: provinciaDef.toString().trim().toUpperCase(),
                                                                    provvDealer: provvDealer,
                                                                    provvGmfi: rispostaWebservice.polizza.provvGmfi,
                                                                    provvVenditore: provvVendi,
                                                                    stato: StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE,
                                                                    tipoCliente: tipoClienteDef,
                                                                    tipoPolizza: polizzaTipo,
                                                                    step: codStep,
                                                                    telaio: cellaTelaio,
                                                                    targa: cellaTarga,
                                                                    telefono: telDef.toString().trim(),
                                                                    valoreAssicurato: valoreAssicurato,
                                                                    valoreAssicuratoconiva: prezzoVendita,
                                                                    certificatoMail: certificato,
                                                                    codiceZonaTerritoriale: rispostaWebservice.polizza.codiceZonaTerritoriale,
                                                                    rappel: rispostaWebservice.polizza.rappel,
                                                                    codOperazione: codOperazione,
                                                                    dSlip: "S",
                                                                    venditore: cellaVenditore ? cellaVenditore.toString().trim().toUpperCase() : "",
                                                                    imposte: rispostaWebservice.polizza.imposte,
                                                                    codCampagna: codCampagna,
                                                                    tariffa: 2,
                                                                    premioCliente: rispostaWebservice.polizza.premioCliente,
                                                                    premioLordoCliente: rispostaWebservice.polizza.premioLordoCliente,
                                                                    premioLordoTerzi: rispostaWebservice.polizza.premioLordoTerzi,
                                                                    premioTerzi: rispostaWebservice.polizza.premioTerzi,
                                                                    hasRCA: false
                                                            )
                                                            if (polizza.save(flush: true)) {
                                                                logg = new Log(parametri: "POLIZZE CVT la polizza viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                                                                if(!tracciatoPolizza.save(flush: true)){
                                                                    logg =new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizza.errors}", operazione: "generazione tracciato iassicur", pagina: "POLIZZE FINANZIATE")
                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                }else{
                                                                    polizza.tracciato=tracciatoPolizza
                                                                    logg =new Log(parametri: "POLIZZE CVT genero tracciato IAssicur", operazione: "generazione tracciato iassicur", pagina: "POLIZZE FINANZIATE")
                                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    def tracciatoComPolizza= tracciatiService.generaTracciatoCompagnia(polizza)
                                                                    if(!tracciatoComPolizza.save(flush: true)){
                                                                        logg =new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizza.errors}", operazione: "generazione tracciato DL", pagina: "POLIZZE FINANZIATE")
                                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    } else{
                                                                        polizza.tracciatoCompagnia=tracciatoComPolizza
                                                                        logg =new Log(parametri: "genero tracciato Compagnia", operazione: "generazione tracciato DL", pagina: "POLIZZE FINANZIATE")
                                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        if(polizza.save(flush:true)) {
                                                                            logg =new Log(parametri: "polizza ${polizza.noPolizza} creata con i suoi tracciati correttamente ", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                            praticheCVT.add("POLIZZA CVT-- polizza ${polizza.noPolizza} caricata correttamente ,")
                                                                            totale++
                                                                        }else{
                                                                           // println "Errore creazione polizza: ${polizza.errors}"
                                                                            errorepraticheCVT.add("POLIZZE CVT Errore creazione polizza: ${polizza.errors}")
                                                                            logg =new Log(parametri: "Errore aggiornamento tracciati polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        }
                                                                    }
                                                                }

                                                            } else {
                                                                //println "Errore caricamento polizza: ${polizza.errors}"
                                                                logg = new Log(parametri: "Errore caricamento polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                errorepraticheCVT.add("polizze CVT: PER LA POLIZZA con premio zero-->${cellnoPratica} Errore caricamento polizza: ${polizza.errors},")
                                                                totaleErr++
                                                            }

                                                        }else{
                                                            if(rispostaWebservice.errore!=''&& rispostaWebservice.errore && rispostaWebservice.errore.toString()!="null" ){
                                                                //println "questa e' la risposta--> ${rispostaWebservice}"
                                                                errorepraticheCVT.add( "polizze CVT: PER LA POLIZZA con premio zero errore chiamata al web service per la polizza--> ${cellnoPratica} -->errore : ${rispostaWebservice.errore}")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero -->errore chiamata al web service per la polizza--> ${cellnoPratica} --> ${rispostaWebservice.errore}", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }

                                                        }
                                                    }else{
                                                        if((provincia.trim()=="") && (provincia.trim() =='null')){
                                                            errorepraticheCVT.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata compilata la provincia,")
                                                            logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata compilata la provincia", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        }else if(pacchetto==""){
                                                            errorepraticheCVT.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stato trovato il pacchetto controllare,")
                                                            logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stato trovato il pacchetto controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        }else if(durata==""){
                                                            errorepraticheCVT.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata inserita la durata,")
                                                            logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata inserita la durata controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        }else if(localitaDef==""){
                                                            errorepraticheCVT.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata inserita la localita',")
                                                            logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata inserita la localita' controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        }else if(dealerType==""){
                                                            errorepraticheCVT.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stato inserito il dealer type,")
                                                            logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stato inserito il dealer type controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        }else if(step==""){
                                                            errorepraticheCVT.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stato inserito lo step,")
                                                            logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non è stato inserito lo step controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        }else if(percentualeVenditore==""){
                                                            errorepraticheCVT.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}   non e' stata inserita la percentuale venditore,")
                                                            logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata inserita la percentuale venditore", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        }else if(valoreAssicurato=="" && valoreAssicurato <= 0.0){
                                                            errorepraticheCVT.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}   non e' stato inserito il valore assicurato,")
                                                            logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stato inserito il valore assicurato", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        }
                                                    }
                                                }
                                            }
                                        }else if(numeropacchetti==1 && pacchettoP=='F'){
                                            //println "e' una rca-->${cellnoPratica}"
                                            def polizzaesistente = PolizzaRCA.findByNoPolizzaIlike(cellnoPratica)
                                            if (polizzaesistente) {
                                                if (codOperazione == "S" ) {
                                                    if (polizzaesistente.telaio.equalsIgnoreCase(cellaTelaio)) {
                                                        polizzaesistente.codOperazione = codOperazione
                                                        if (polizzaesistente.save(flush: true)) {
                                                            logg = new Log(parametri: "PER LA POLIZZA RCA con premio zero  rigenerato", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                            if (!logg.save(flush: true)) println "POLIZZE RCA Errori salvataggio log reponse: ${logg.errors}"
                                                        }
                                                    } else {
                                                        errorePraticheRCA.add("POLIZZE RCA con premio zero per questa pratica-->${cellnoPratica} il telaio associato non corrisponde a quello inserito nel DB,")
                                                        totaleErrRCA++
                                                    }
                                                } else if(sup75){
                                                    errorePraticheRCA.add("POLIZZE RCA con premio zero il beneficiario della pratica-->${cellnoPratica}  supera la eta' di 75 anni,")
                                                    totaleErrRCA++
                                                }
                                                else {
                                                    errorePraticheRCA.add("POLIZZE RCA con premio zero la pratica-->${cellnoPratica} \u00E8 gia' stata caricata nel portale,")
                                                    totaleErrRCA++
                                                }
                                            } else {
                                                if (codOperazione == "S") {
                                                    errorePraticheRCA.add("POLIZZE RCA con premio zero la pratica rca-->${cellnoPratica} che si sta cercando di stornare non esiste. Verificare no polizza,")
                                                    totaleErrRCA++
                                                }  else {
                                                    /**chiamo il webservice*/
                                                    def rappel, imposte
                                                    def chiamataws=DLWebService.chiamataRCAWSCSV(cellnoPratica, cellaModello,dealer.id,valoreAssicurato,provincia,localitaDef.toString().trim().toUpperCase(),cellaVersione.toUpperCase(),dataDecorrenza, cvtinsurance, polizzaTipo)
                                                    if(chiamataws){
                                                        //println "risposta chiamata ws rca ${chiamataws}"
                                                        logg = new Log(parametri: "POLIZZE RCA con premio zero risposta chiamata ws rca ${chiamataws}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        if(chiamataws.risposta==false){
                                                            errorePraticheCVTRCA.add(" POLIZZA CVT/RCA  pratica-->${cellnoPratica} ha dati il seguente Errore dal WEBSERVICE: ${chiamataws.errore},")
                                                            logg = new Log(parametri: "POLIZZA CVT/RCA  pratica-->${cellnoPratica} ha dati il seguente Errore dal WEBSERVICE: ${chiamataws.errore}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            totaleErrCVTRCA2++
                                                            rappel=null
                                                            imposte=null
                                                        }else{
                                                            rappel=chiamataws.rappel
                                                            imposte=chiamataws.imposte
                                                        }

                                                    }
                                                    if(polizzaTipo != TipoPolizza.LEASING.toString()){
                                                        provinciaimma=provinciaDef.toString().trim().toUpperCase()
                                                    }
                                                    if(rappel != null && imposte != null){
                                                        def polizza = PolizzaRCA.findOrCreateWhere(
                                                                annullata: false,
                                                                cap: capDef.toString().trim().padLeft(5, "0"),
                                                                cellulare: cellDef.toString().trim(),
                                                                cognome: cognomeDef.toString().trim(),
                                                                coperturaRichiesta: coperturaR,
                                                                dataDecorrenza: dataDecorrenza,
                                                                dataImmatricolazione: dataImmatricolazione,
                                                                dataInserimento: dataInserimento,
                                                                dataScadenza: dataScadenza,
                                                                dealer: dealer,
                                                                durata: 12,
                                                                email: emailDef.toString().trim(),
                                                                indirizzo: indirizzoDef.toString().trim().toUpperCase(),
                                                                localita: localitaDef.toString().trim().toUpperCase(),
                                                                marca: cellaMarca,
                                                                modello: cellaModello,
                                                                noPolizza: cellnoPratica,
                                                                nome: nomeDef.toString().trim().toUpperCase(),
                                                                nuovo: nuovo,
                                                                onStar: false,
                                                                partitaIva: cfDef.toString().trim().toUpperCase(),
                                                                premioImponibile: chiamataws.premioImponibile,
                                                                premioLordo: chiamataws.premioLordo,
                                                                provincia: provinciaDef.toString().trim().toUpperCase(),
                                                                provImmatricolazione: provinciaimma.toString().trim().toUpperCase(),
                                                                provvDealer: provvDealer,
                                                                provvGmfi: chiamataws.provvGmfi,
                                                                provvVenditore: provvVendi,
                                                                stato: StatoPolizza.PREVENTIVO,
                                                                tipoCliente: tipoClienteDef,
                                                                tipoPolizza: polizzaTipo,
                                                                step: codStep,
                                                                telaio: cellaTelaio,
                                                                targa: cellaTarga,
                                                                telefono: telDef.toString().trim(),
                                                                valoreAssicurato: valoreAssicurato,
                                                                codiceZonaTerritoriale: chiamataws.zona,
                                                                categoriaveicolo: chiamataws.categoria,
                                                                rappel: rappel,
                                                                codOperazione: codOperazione,
                                                                dSlip: "S",
                                                                venditore: cellaVenditore ? cellaVenditore.toString().trim().toUpperCase() : "",
                                                                imposte: imposte,
                                                                codCampagna: codCampagna,
                                                                tariffa: 2,
                                                                hasCVT: false,
                                                                versione: cellaVersione.toUpperCase(),
                                                                destUso: "PROPRIO",
                                                                isCash: false,
                                                                dacsv: true
                                                        )
                                                        if (polizza.save(flush: true)) {
                                                            logg = new Log(parametri: "POLIZZE RCA POLIZZE RCA con premio zero la polizza viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            def iden=generaIdentificativo(polizza)
                                                            //println "POLIZZE RCA la polizza viene salvata nel DB ${generaIdentificativo(polizza)}"
                                                            polizza.identificativo=  generaIdentificativo(polizza)
                                                            if (polizza.save(flush: true)) {
                                                                // println "la polizza viene aggiornata nel DB"
                                                                logg = new Log(parametri: "POLIZZE RCA POLIZZE RCA con premio zero la polizza viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                praticheRCA.add("POLIZZA RCA la pratica-->${cellnoPratica} e' stata caricata,")
                                                            }
                                                        } else {
                                                            logg = new Log(parametri: "POLIZZE RCA POLIZZE RCA con premio zero Errore caricamento polizza: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            errorePraticheRCA.add(" POLIZZE RCA pratica-->${cellnoPratica} Errore caricamento polizza: ${polizza.errors},")
                                                            totaleErrRCA++
                                                        }
                                                    }

                                                }
                                            }
                                        }else if(numeropacchetti>1){
                                           // println "e' una cvt rca--> ${cellnoPratica}"
                                            if(pacchettoP=="F"){
                                                logg = new Log(parametri: "POLIZZa RCA POLIZZE RCA con premio zero la polizza ${cellnoPratica} e' una rca", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING/RCA")
                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                def polizzaesistente = PolizzaRCA.findByNoPolizzaIlike(cellnoPratica)
                                                if (polizzaesistente) {
                                                    if (codOperazione == "S" ) {
                                                        if (polizzaesistente.telaio.equalsIgnoreCase(cellaTelaio)) {
                                                            polizzaesistente.codOperazione = codOperazione
                                                            if (polizzaesistente.save(flush: true)) {
                                                                logg = new Log(parametri: "POLIZZA CVT/RCA POLIZZA RCA con premio zero tracciato compagnia rigenerato", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                                if (!logg.save(flush: true)) println "POLIZZA CVT/RCA  Errori salvataggio log reponse: ${logg.errors}"
                                                            }
                                                        } else {
                                                            errorePraticheCVTRCA.add("POLIZZA CVT/RCA con premio zero per questa pratica RCA-->${cellnoPratica} il telaio associato non corrisponde a quello inserito nel DB,")
                                                            totaleErrCVTRCA2++
                                                        }
                                                    } else if(sup75){
                                                        errorePraticheCVTRCA.add("POLIZZA CVT/RCA con premio zero il beneficiario della pratica RCA-->${cellnoPratica}  supera la eta' di 75 anni,")
                                                        totaleErrCVTRCA2++
                                                    }
                                                    else {
                                                        errorePraticheCVTRCA.add("POLIZZA CVT/RCA con premio zero la pratica RCA-->${cellnoPratica} \u00E8 gia' stata caricata nel portale,")
                                                        totaleErrCVTRCA2++
                                                    }
                                                } else {
                                                    if (codOperazione == "S") {
                                                        errorePraticheCVTRCA.add("POLIZZA CVT/RCA con premio zero la pratica RCA-->${cellnoPratica} che si sta cercando di stornare non esiste. Verificare no polizza,")
                                                        totaleErrCVTRCA2++
                                                    }  else {
                                                        /**chiamo il webservice*/
                                                        def rappel, imposte

                                                        def chiamataws=DLWebService.chiamataRCAWSCSV(cellnoPratica,cellaModello,dealer.id,valoreAssicurato,provincia,localitaDef.toString().trim().toUpperCase(),cellaVersione.toUpperCase(),dataDecorrenza, cvtinsurance,polizzaTipo)
                                                        if(chiamataws){
                                                            //println "risposta chiamata ws rca ${chiamataws}"
                                                            logg = new Log(parametri: "risposta chiamata ws rca con premio zero ${chiamataws}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            if(chiamataws.risposta==false){
                                                                errorePraticheCVTRCA.add(" POLIZZA CVT/RCA  pratica-->${cellnoPratica} ha dati il seguente Errore dal WEBSERVICE: ${chiamataws.errore},")
                                                                logg = new Log(parametri: "POLIZZA CVT/RCA  pratica-->${cellnoPratica} ha dati il seguente Errore dal WEBSERVICE: ${chiamataws.errore}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                totaleErrCVTRCA2++
                                                                rappel=null
                                                                imposte=null
                                                            }else{
                                                                rappel=chiamataws.rappel
                                                                imposte=chiamataws.imposte
                                                            }
                                                        }
                                                        if(polizzaTipo != TipoPolizza.LEASING.toString()){
                                                            provinciaimma=provinciaDef.toString().trim().toUpperCase()
                                                        }
                                                        if(rappel != null && imposte != null){
                                                            def polizza = PolizzaRCA.findOrCreateWhere(
                                                                    annullata: false,
                                                                    cap: capDef.toString().trim().padLeft(5, "0"),
                                                                    cellulare: cellDef.toString().trim(),
                                                                    cognome: cognomeDef.toString().trim(),
                                                                    coperturaRichiesta: coperturaR,
                                                                    dataDecorrenza: dataDecorrenza,
                                                                    dataImmatricolazione: dataImmatricolazione,
                                                                    dataInserimento: dataInserimento,
                                                                    dataScadenza: dataScadenza,
                                                                    dealer: dealer,
                                                                    durata: 12,
                                                                    email: emailDef.toString().trim(),
                                                                    indirizzo: indirizzoDef.toString().trim().toUpperCase(),
                                                                    localita: localitaDef.toString().trim().toUpperCase(),
                                                                    marca: cellaMarca,
                                                                    modello: cellaModello,
                                                                    noPolizza: cellnoPratica,
                                                                    nome: nomeDef.toString().trim().toUpperCase(),
                                                                    nuovo: nuovo,
                                                                    onStar: false,
                                                                    partitaIva: cfDef.toString().trim().toUpperCase(),
                                                                    premioImponibile: chiamataws.premioImponibile,
                                                                    premioLordo: chiamataws.premioLordo,
                                                                    provincia: provinciaDef.toString().trim().toUpperCase(),
                                                                    provImmatricolazione: provinciaimma.toString().trim().toUpperCase(),
                                                                    provvDealer: provvDealer,
                                                                    provvGmfi: chiamataws.provvGmfi,
                                                                    provvVenditore: provvVendi,
                                                                    stato: StatoPolizza.PREVENTIVO,
                                                                    tipoCliente: tipoClienteDef,
                                                                    tipoPolizza: polizzaTipo,
                                                                    step: "step 1",
                                                                    telaio: cellaTelaio,
                                                                    targa: cellaTarga,
                                                                    telefono: telDef.toString().trim(),
                                                                    valoreAssicurato: valoreAssicurato,
                                                                    codiceZonaTerritoriale: chiamataws.zona,
                                                                    categoriaveicolo: chiamataws.categoria,
                                                                    rappel: rappel,
                                                                    codOperazione: codOperazione,
                                                                    dSlip: "S",
                                                                    venditore: cellaVenditore ? cellaVenditore.toString().trim().toUpperCase() : "",
                                                                    imposte: imposte,
                                                                    codCampagna: codCampagna,
                                                                    tariffa: 2,
                                                                    hasCVT: true,
                                                                    versione: cellaVersione.toUpperCase(),
                                                                    destUso: "PROPRIO",
                                                                    isCash: false,
                                                                    dacsv: true
                                                            )
                                                            if (polizza.save(flush: true)) {
                                                                logg = new Log(parametri: "POLIZZA CVT/RCA con premio zero la polizza RCA viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                def iden=generaIdentificativo(polizza)
                                                                //println "la polizza viene salvata nel DB identificativo-> ${generaIdentificativo(polizza)}"
                                                                polizza.identificativo=  generaIdentificativo(polizza)
                                                                if (polizza.save(flush: true)) {
                                                                    // println "la polizza viene aggiornata nel DB"
                                                                    logg = new Log(parametri: "POLIZZA CVT/RCA con premio zero la polizza RCA viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    praticheCVT_RCA.add("POLIZZA CVT/RCA con premio zero la pratica RCA-->${cellnoPratica} e' stata caricata,")
                                                                    totaleCVTRCA2++
                                                                }
                                                            } else {
                                                                logg = new Log(parametri: "POLIZZA CVT/RCA con premio zero Errore caricamento polizza RCA: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                errorePraticheCVTRCA.add(" POLIZZA CVT/RCA con premio zero pratica-->${cellnoPratica} Errore caricamento polizza: ${polizza.errors},")
                                                                totaleErrCVTRCA2++
                                                            }
                                                        }

                                                    }
                                                }
                                            }else{
                                               // println "e' una cvt-->${cellnoPratica}"
                                                logg = new Log(parametri: "e' una cvt con premio zero", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE/RCA")
                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                def polizzaesistente = Polizza.findByNoPolizzaIlike(cellnoPratica)
                                                if (polizzaesistente) {
                                                    if (codOperazione == "S" && !speciale) {
                                                        if (polizzaesistente.telaio.equalsIgnoreCase(cellaTelaio)) {
                                                            polizzaesistente.codOperazione = codOperazione
                                                            if (polizzaesistente.save(flush: true)) {
                                                                def tracciatoPolizzaR = tracciatiService.generaTracciatoIAssicur(polizzaesistente)
                                                                if (!tracciatoPolizzaR.save(flush: true)) {
                                                                    logg = new Log(parametri: "Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                    errorePraticheCVTRCA.add(" pratica-->${cellnoPratica} Errore creazione tracciato iassicur: ${tracciatoPolizzaR.errors},")
                                                                    totaleErrCVTRCA++
                                                                } else {
                                                                    def tracciatoComPolizzaR = tracciatiService.generaTracciatoCompagnia(polizzaesistente)
                                                                    if (!tracciatoComPolizzaR.save(flush: true)) {
                                                                        logg = new Log(parametri: "Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                        errorePraticheCVTRCA.add(" pratica-->${cellnoPratica} Errore creazione tracciato compagnia: ${tracciatoComPolizzaR.errors},")
                                                                        totaleErrCVTRCA++
                                                                    } else {
                                                                        if (polizzaesistente.tracciatoPAIId) {
                                                                            def tracciatoPAIPolizzaR = tracciatiService.generaTracciatoPAI(polizzaesistente, speciale)
                                                                            if (!tracciatoPAIPolizzaR.save(flush: true)) {
                                                                                logg = new Log(parametri: "Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                errorePraticheCVTRCA.add(" pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors},")
                                                                                totaleErrCVTRCA++
                                                                            } else {
                                                                                if (polizzaesistente.targa != null && polizzaesistente.targa != '') {
                                                                                    polizzaesistente.targa = polizzaesistente.targa + "_R"
                                                                                }
                                                                                if (polizzaesistente.telaio != null && polizzaesistente.telaio != '') {
                                                                                    polizzaesistente.telaio = polizzaesistente.telaio + "_R"
                                                                                }
                                                                                if (polizzaesistente.save(flush: true)) {
                                                                                    logg = new Log(parametri: "targa e telaio per la polizza ${polizzaesistente.noPolizza} aggiornati", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                    praticheCVT_RCA.add("polizza stornata correttamente ${polizzaesistente.noPolizza},")
                                                                                    totaleCVTRCA++
                                                                                } else {
                                                                                    logg = new Log(parametri: "non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                    errorePraticheCVTRCA.add(" pratica-->${cellnoPratica} non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors},")
                                                                                    totaleErrCVTRCA++
                                                                                }
                                                                            }
                                                                        } else {
                                                                            if (polizzaesistente.targa != null && polizzaesistente.targa != '') {
                                                                                polizzaesistente.targa = polizzaesistente.targa + "_R"
                                                                            }
                                                                            if (polizzaesistente.telaio != null && polizzaesistente.telaio != '') {
                                                                                polizzaesistente.telaio = polizzaesistente.telaio + "_R"
                                                                            }
                                                                            if (polizzaesistente.save(flush: true)) {
                                                                                logg = new Log(parametri: "targa e telaio per la polizza ${polizzaesistente.noPolizza} aggiornati", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                praticheCVT_RCA.add("polizza stornata correttamente ${polizzaesistente.noPolizza},")
                                                                                totaleCVTRCA++
                                                                            } else {
                                                                                logg = new Log(parametri: "non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors}", operazione: "Storno nel caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                                errorePraticheCVTRCA.add(" pratica-->${cellnoPratica} non e' stato possibile aggiornare targa e telaio: ${polizzaesistente.errors},")
                                                                                totaleErrCVTRCA++
                                                                            }
                                                                        }

                                                                    }
                                                                }
                                                            }
                                                        } else {
                                                            errorePraticheCVTRCA.add("per questa pratica con premio zero-->${cellnoPratica} il telaio associato non corrisponde a quello inserito nel DB,")
                                                            totaleErrCVTRCA++
                                                        }
                                                    } else if (speciale && !sup75) {
                                                        logg = new Log(parametri: "POLIZZA CVT/RCA con premio zero  genero tracciato PAI", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        def tracciatoPAI = tracciatiService.generaTracciatoPAI(polizzaesistente, speciale)
                                                        if (!tracciatoPAI.save(flush: true)) {
                                                            logg = new Log(parametri: "POLIZZA CVT/RCA   Errore creazione tracciato PAI: ${tracciatoPAI.errors}", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            errorePraticheCVTRCA.add(" pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPAI.errors},")
                                                            totaleErrCVTRCA++
                                                        } else {
                                                            logg = new Log(parametri: "POLIZZA CVT/RCA con premio zero tracciato PAI generato correttamente per la polizza ${polizzaesistente.noPolizza}", operazione: "generazione flussi pai tramite csv", pagina: "POLIZZE FINANZIATE")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            flussiPAI.add("tracciato PAI con premio zero generato correttamente per la polizza ${polizzaesistente.noPolizza},")
                                                            totaleCVTRCA++
                                                        }
                                                    }else if(sup75){
                                                        errorePraticheCVTRCA.add("POLIZZA CVT/RCA con premio zero il beneficiario della pratica CVT-->${cellnoPratica}  supera la eta' di 75 anni,")
                                                        totaleErrCVTRCA++
                                                    }
                                                    else {
                                                        errorePraticheCVTRCA.add("POLIZZA CVT/RCA con premio zero la pratica CVT -->${cellnoPratica} \u00E8 gia' stata caricata nel portale,")
                                                        totaleErrCVTRCA++
                                                    }
                                                } else {
                                                    if (codOperazione == "S") {
                                                        errorePraticheCVTRCA.add("POLIZZA CVT/RCA con premio zero la pratica CVT-->${cellnoPratica} che si sta cercando di stornare non esiste. Verificare no polizza,")
                                                        totaleErr++
                                                    } else if (speciale) {
                                                        errorePraticheCVTRCA.add("POLIZZA CVT/RCA con premio zero la pratica-->${cellnoPratica} non esiste e non e' possibile generare un flusso PAI... verificare no polizza,")
                                                        totaleErrCVTRCA++
                                                    } else {
                                                        /****hace llamada al ws**/
                                                        def pacchetto=CodProdotti.findByCoperturaAndStep(coperturaR, codStep).codPacchetto
                                                        def percentualeVenditore=dealer.percVenditore
                                                        if(percentualeVenditore.toPlainString()=="0.0000000"){
                                                            percentualeVenditore=0.0
                                                        }
                                                        def dealerType=dealer.dealerType
                                                        if((provincia.trim()!="") && (provincia.trim() !='null') && pacchetto!="" && durata!=null && localitaDef.trim()!="" && localitaDef.trim()!='NULL' && dealerType!="" && step!=""&& percentualeVenditore!=""&& valoreAssicurato!="" && valoreAssicurato > 0.0 ){
                                                            def dataTarriffa='2017-04-21'
                                                            def tipoOperazione
                                                            if(codOperazione=="0"){
                                                                tipoOperazione="A"
                                                            }else{
                                                                tipoOperazione=codOperazione
                                                            }
                                                            rispostaWebservice=DLWebService.chiamataWSFINTAR2(pacchetto,valoreAssicurato,provincia,localitaDef,durata,dealer.dealerType,step, dataTarriffa,tipoOperazione,percentualeVenditore,cvtinsurance, cellnoPratica,polizzaTipo)
                                                           // println "POLIZZA CVT/RCA risposta chiamata WS CVT-->: ${rispostaWebservice}"
                                                            logg = new Log(parametri: "POLIZZA CVT/RCA con premio zero risposta chiamata WS CVT-->: ${rispostaWebservice}", operazione: "chiamata WS CVT", pagina: "POLIZZE FIN/LEASING")
                                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                                                            def polizza = Polizza.findOrCreateWhere(
                                                                    annullata: false,
                                                                    cap: capDef.toString().trim().padLeft(5, "0"),
                                                                    cellulare: cellDef.toString().trim(),
                                                                    cognome: cognomeDef.toString().trim(),
                                                                    coperturaRichiesta: coperturaR,
                                                                    dataDecorrenza: dataDecorrenza,
                                                                    dataImmatricolazione: dataImmatricolazione,
                                                                    dataInserimento: dataInserimento,
                                                                    dataScadenza: dataScadenza,
                                                                    dealer: dealer,
                                                                    durata: durata,
                                                                    email: emailDef.toString().trim(),
                                                                    indirizzo: indirizzoDef.toString().trim().toUpperCase(),
                                                                    localita: localitaDef.toString().trim().toUpperCase(),
                                                                    marca: cellaMarca,
                                                                    modello: cellaModello,
                                                                    noPolizza: cellnoPratica,
                                                                    nome: nomeDef.toString().trim().toUpperCase(),
                                                                    nuovo: nuovo,
                                                                    onStar: rispostaWebservice.polizza.onStar,
                                                                    partitaIva: cfDef.toString().trim().toUpperCase(),
                                                                    premioImponibile: rispostaWebservice.polizza.premioImponibile,
                                                                    premioLordo: rispostaWebservice.polizza.premioLordo,
                                                                    provincia: provinciaDef.toString().trim().toUpperCase(),
                                                                    provvDealer: provvDealer,
                                                                    provvGmfi: rispostaWebservice.polizza.provvGmfi,
                                                                    provvVenditore: provvVendi,
                                                                    stato: StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE,
                                                                    tipoCliente: tipoClienteDef,
                                                                    tipoPolizza: polizzaTipo,
                                                                    step: codStep,
                                                                    telaio: cellaTelaio,
                                                                    targa: cellaTarga,
                                                                    telefono: telDef.toString().trim(),
                                                                    valoreAssicurato: valoreAssicurato,
                                                                    valoreAssicuratoconiva: prezzoVendita,
                                                                    certificatoMail: certificato,
                                                                    codiceZonaTerritoriale: rispostaWebservice.polizza.codiceZonaTerritoriale,
                                                                    rappel: rispostaWebservice.polizza.rappel,
                                                                    codOperazione: codOperazione,
                                                                    dSlip: "S",
                                                                    venditore: cellaVenditore ? cellaVenditore.toString().trim().toUpperCase() : "",
                                                                    imposte: rispostaWebservice.polizza.imposte,
                                                                    codCampagna: codCampagna,
                                                                    tariffa: 2,
                                                                    premioCliente: rispostaWebservice.polizza.premioCliente,
                                                                    premioLordoCliente: rispostaWebservice.polizza.premioLordoCliente,
                                                                    premioLordoTerzi: rispostaWebservice.polizza.premioLordoTerzi,
                                                                    premioTerzi: rispostaWebservice.polizza.premioTerzi,
                                                                    hasRCA: true
                                                            )
                                                            if (polizza.save(flush: true)) {
                                                                logg = new Log(parametri: "POLIZZA CVT/RCA con premio zero la polizza CVT  viene salvata nel DB", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                praticheCVT_RCA.add("POLIZZA CVT/RCA con premio zero la pratica CVT-->${cellnoPratica} e' stata caricata,")
                                                                totaleCVTRCA++
                                                            } else {
                                                                logg = new Log(parametri: "Errore caricamento polizza con premio zero: ${polizza.errors}", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                errorePraticheCVTRCA.add("POLIZZA CVT con premio zero la pratica CVT-->${cellnoPratica} Errore caricamento polizza: ${polizza.errors},")
                                                                totaleErrCVTRCA++
                                                            }
                                                        }else{
                                                            if((provincia.trim()=="") && (provincia.trim() =='null')){
                                                                errorepraticheCVT.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata compilata la provincia,")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata compilata la provincia", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else if(pacchetto==""){
                                                                errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stato trovato il pacchetto controllare,")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stato trovato il pacchetto controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else if(durata==""){
                                                                errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata inserita la durata,")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata inserita la durata controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else if(localitaDef==""){
                                                                errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata inserita la localita',")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata inserita la localita' controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else if(dealerType==""){
                                                                errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stato inserito il dealer type,")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stato inserito il dealer type controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else if(step==""){
                                                                errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stato inserito lo step,")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non è stato inserito lo step controllare", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else if(percentualeVenditore==""){
                                                                errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}   non e' stata inserita la percentuale venditore,")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stata inserita la percentuale venditore", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }else if(valoreAssicurato=="" && valoreAssicurato <= 0.0){
                                                                errorePraticheCVTRCA.add( "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}   non e' stato inserito il valore assicurato,")
                                                                logg = new Log(parametri: "polizze CVT: PER LA POLIZZA con premio zero --> ${cellnoPratica}  non e' stato inserito il valore assicurato", operazione: "creazione polizze FIN/LEASING tramite xls", pagina: "POLIZZE FIN/LEASING")
                                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }
                                                        }
                                                    }
                                                }
                                            }
                                        }

                            } else if (!dealer) {
                                logg = new Log(parametri: "Errore caricamento polizza con premio zero: il dealer non \u00E8 presente", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                errorePraticheCVTRCA.add("Errore caricamento polizza con premio zero: il dealer ${cellaDealer}- ${cellaNomeDealer} non \u00E8 presente,")
                                totaleErrCVTRCA++
                            } else if (!cellnoPratica) {
                                logg = new Log(parametri: "Errore caricamento polizza con premio zero: non \u00E8 presente un numero di pratica", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                errorePraticheCVTRCA.add("Errore caricamento polizza con premio zero: non \u00E8 presente un numero di polizza,")
                                totaleErr++
                            }
                        } else {
                            logg = new Log(parametri: "POLIZZE CVT/RCA con premio zero Errore caricamento polizza:${cellnoPratica} la provincia non \u00E8 stata dichiarata, controllare il file", operazione: "caricamento polizze tramite csv", pagina: "POLIZZE FINANZIATE")
                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            errorePraticheCVTRCA.add("POLIZZE CVT/RCA con premio zero Errore caricamento polizza:${cellnoPratica} la provincia non \u00E8 stata dichiarata, controllare il file,")
                            totaleErrCVTRCA++
                        }
                    }
                }
            }


        /*}catch (e){
            logg = new Log(parametri: "c'e' un problema con il file, ${e.toString()}", operazione: "caricamento polizze FIN/LEASING tramite csv", pagina: "POLIZZE FIN/LEASING")
            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            println "c'e' un problema con il file, ${e.toString()}"
            errorePraticheCVTRCA.add( "c'e' un problema con il file, ${e.toString()}")
        }*/
        if (praticheCVT.size() > 0) {
            praticheCVT.collect { commento ->
                rispostaMail = rispostaMail + "\n  " + commento + "\r\n"
            }.grep().join("\n")
        }
        if (praticheCVT_RCA.size() > 0) {
            praticheCVT_RCA.collect { commento ->
                rispostaMail = rispostaMail + "\n  " + commento + "\r\n"
            }.grep().join("\n")
        }
        if (praticheRCA.size() > 0) {
            praticheRCA.collect { commento ->
                rispostaMail = rispostaMail + "\n  " + commento + "\r\n"
            }.grep().join("\n")
        }
        if(errorepraticheCVT.size()>0){
            errorepraticheCVT.collect { commento ->
                rispostaErrori = rispostaErrori + "\n   " + commento + "\r\n"
            }.grep().join("\n")

        }
        if(errorePraticheCVTRCA.size()>0){
            errorePraticheCVTRCA.collect { commento ->
                rispostaMail = rispostaMail + "\n  " + commento + "\r\n"
            }.grep().join("\n")

        }
        if(errorePraticheRCA.size()>0){
            errorePraticheRCA.collect { commento ->
                rispostaErrori = rispostaErrori + "\n  " + commento + "\r\n"
            }.grep().join("\n")

        }
        /*if(praticheCVT.size()>0){
            rispostaMail=rispostaMail+=" totale polizze CVT caricate: ${totale} \n "
        }
        if(praticheCVT_RCA.size()>0){
            rispostaMail=rispostaMail+=" totale polizze CVT con RCA caricate: ${totaleCVTRCA/2} \n "
        }
        if(praticheRCA.size()>0){
            rispostaMail=rispostaMail+=" totale polizze RCA caricate: ${totaleRCA} \n "
        }
        if(errorepraticheCVT.size()>0){
            rispostaMail=rispostaMail+=" totale polizze CVT  non caricate: ${totaleErr} \n "
        }
        if(errorePraticheCVTRCA.size()>0){
            rispostaMail=rispostaMail+=" totale polizze CVT con RCA non caricate: ${totaleErrCVTRCA/2} \n "
        }
        if(errorePraticheRCA.size()>0){
            rispostaMail=rispostaMail+=" totale polizze RCA non caricate: ${totaleErrRCA} \n "
        }*/
        def rispostacarica=[:]
        rispostacarica.polizzezero=rispostaMail
        rispostacarica.errorePratiche=rispostaErrori
        rispostacarica.totaleErrRCA=totaleErrRCA
        rispostacarica.totaleErr=totaleErr
        rispostacarica.totaleErrCVTRCA=totaleErrCVTRCA+totaleErrCVTRCA2
        rispostacarica.totaleRCA=totaleRCA
        rispostacarica.totaleCVTRCA=totaleCVTRCA+totaleCVTRCA2
        rispostacarica.totale=totale
        logg = new Log(parametri: "risposta caricata--> ${rispostacarica}", operazione: "caricamento polizze FIN/LEASING tramite csv IMPORTO ZERO", pagina: "POLIZZE FIN/LEASING")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        //println "risposta caricata--> ${rispostacarica}\n"
        return rispostacarica
    }
    def generaIdentificativo(polizza) {
        def numero = polizza.id
        //dealer.save()
        if(!polizza.save()) {
            println "Errori aggiornamento numerazione polizza"
            return  "$numero".padLeft(6, '0')

        }else{
            return "$numero".padLeft(6, '0')

        }
    }
    def isbeneficiario(beneficiario,intestatario){
        def denominazione=false
        def logg
        beneficiario = beneficiario.toUpperCase()
        logg = new Log(parametri: "beneficiario-->${beneficiario}", operazione: "is beneficiario", pagina: "POLIZZE FINANZIATE")
        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            intestatario = intestatario.toUpperCase().trim().replaceAll("'", "")
            if (intestatario.contains(beneficiario)) {
                logg = new Log(parametri: "intestatario-->${intestatario}", operazione: "is beneficiario", pagina: "POLIZZE FINANZIATE")
                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                denominazione = true

            }
        return denominazione
    }
    def assegnascadenza(durata, dataDecorrenza){
        def dataScadenza
        if (durata == 12) {
            dataScadenza = use(TimeCategory) { dataDecorrenza + 12.months }
        } else if (durata == 18) {
            dataScadenza = use(TimeCategory) { dataDecorrenza + 18.months }
        } else if (durata == 24) {
            dataScadenza = use(TimeCategory) { dataDecorrenza + 24.months }
        } else if (durata == 30) {
            dataScadenza = use(TimeCategory) { dataDecorrenza + 30.months }
        } else if (durata == 36) {
            dataScadenza = use(TimeCategory) { dataDecorrenza + 36.months }
        } else if (durata == 42) {
            dataScadenza = use(TimeCategory) { dataDecorrenza + 42.months }
        } else if (durata == 48) {
            dataScadenza = use(TimeCategory) { dataDecorrenza + 48.months }
        } else if (durata == 54) {
            dataScadenza = use(TimeCategory) { dataDecorrenza + 54.months }
        } else if (durata == 60) {
            dataScadenza = use(TimeCategory) { dataDecorrenza + 60.months }
        } else if (durata == 66) {
            dataScadenza = use(TimeCategory) { dataDecorrenza + 66.months }
        } else if (durata == 72) {
            dataScadenza = use(TimeCategory) { dataDecorrenza + 72.months }
        } else {
            dataScadenza = null
            durata = null
        }

        return dataScadenza

    }

}
