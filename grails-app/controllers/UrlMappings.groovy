import org.springframework.security.access.AccessDeniedException

class UrlMappings {

    static mappings = {
        "/$controller/$action?/$id?(.$format)?"{
            constraints {
                // apply constraints here
            }
        }
        name list: "/$controller/polizze/$page" {
            action = "lista"
        }
        name listAnn: "/$controller/annullamenti/$page" {
            action = "annullamenti"
        }
        name listRCA: "/$controller/listaRCA/$page" {
            action = "listaRCA"
        }
        name listattesa: "/$controller/inattesa/$page" {
            action = "inattesa"
        }
        name listdacontt: "/$controller/dacontattare/$page" {
            action = "dacontattare"
        }
        name listannullate: "/$controller/annullate/$page" {
            action = "annullate"
        }
        "/login"(view: "/login")
        "/passwordScaduta"(controller: "security", action: "passwordScaduta")
        "/notAuthorized"(view: "/notAuthorized")
        //"/"(controller: "polizze", action: "lista")
        "/"(controller: "polizze", action: "menu")

        "/grails"(view: "/grails")
        //"/"(view:"/index")
        "500"(view:'/error')
        "500"(view: "/notAuthorized", exception: AccessDeniedException)
        "403"(view:'/notAuthorized')
        "404"(view:'/notFound')
    }
}
