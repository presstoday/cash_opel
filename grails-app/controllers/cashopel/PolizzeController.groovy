
package cashopel

import cashopel.polizze.CodProdotti
import cashopel.polizze.Documenti
import cashopel.polizze.DocumentiSalesSpecialist
import cashopel.polizze.Polizza
import cashopel.polizze.PolizzaPaiPag
import cashopel.polizze.StatoPolizza
import cashopel.polizze.TipoCliente
import cashopel.polizze.TipoDoc
import cashopel.polizze.TipoPai
import cashopel.polizze.TipoPolizza
import cashopel.polizze.Tracciato
import cashopel.polizze.TracciatoCompagnia
import cashopel.polizze.TracciatoPAI
import cashopel.polizze.TracciatoPAIPag
import cashopel.polizze.ZoneTarif
import cashopel.utenti.Admin
import cashopel.utenti.Dealer
import cashopel.utenti.Log
import com.itextpdf.text.Document
import com.itextpdf.text.pdf.PdfReader
import com.itextpdf.text.pdf.PdfWriter
import com.itextpdf.text.pdf.parser.PdfTextExtractor
import com.presstoday.groovy.Stopwatch
import excel.builder.ExcelBuilder
import excel.reader.ExcelReader
import grails.converters.JSON
import grails.util.Environment
import groovy.sql.Sql
import groovy.time.TimeCategory
import groovyx.net.http.ContentType
import groovyx.net.http.HTTPBuilder
import groovyx.net.http.Method
import org.grails.web.json.JSONObject
import security.SecurityUtils
import java.text.DecimalFormat
import java.text.DecimalFormatSymbols
import java.text.NumberFormat
import java.util.regex.Pattern
import java.util.zip.ZipEntry
import java.util.zip.ZipOutputStream
import static grails.async.Promises.*

class PolizzeController {
    static scope = "prototype"
    def mailService
    def estrazioneService
    def tracciatiService
    def IAssicurWebService
    def DLWebService
    def dataSource_rinnovi
    def ftpService
    def generaCertService
    def GmfgenService
    def quartzScheduler
    def index() {}
    def menu(){}
    def lista(){
        //InvioMailCertificatiCashFinLea14Job.triggerNow()
        //CertificatiIASSICURJob.triggerNow()
        //CertificatiRinnoviIASSICURJob.triggerNow()
        //CaricaRotoMailJob.triggerNow()
        //CaricasuIAssicurJob.triggerNow()
        //CertPAIJob.triggerNow()
        //ElencoMensileJob.triggerNow()
       //ElencoSemestraleGennaioMACH1Job.triggerNow()
       //ElencoSettimanaleMACH1Job.triggerNow()
        def principal = SecurityUtils.principal
        def utente,admin
        if(SecurityUtils.hasRole("ADMIN")){
            admin=Admin.findById(principal.id)
            utente=admin
        }
        def max = 15, pagesPerPage = 10
        def page = (params.page ?: 1) as int
        def offset = (page - 1) * max
        def cerca = params.search
        def polizze = Polizza.createCriteria().list(max: max, offset: offset) {
            eq "tipoPolizza",TipoPolizza.CASH
            projections {
                createAlias ('dealer', 'd')
            }
            if(cerca) {
                or {
                    ilike "noPolizza","${cerca}"
                    ilike "cognome", "%${cerca}%"
                    ilike "partitaIva", "%${cerca}%"
                    ilike "coperturaRichiesta", "%${cerca}%"
                    ilike "d.ragioneSocialeD", "%${cerca}%"
                }
            }
                and {
                    order('dataInserimento', 'desc')
                    order('stato', 'asc')
                    order('d.ragioneSocialeD', 'asc')
                }
        }

        def pages = Math.ceil(polizze.totalCount / max) as int
        def totalPages=pages
        if(pages <= 1) pages = []
        else if(pages <= pagesPerPage) pages = 1..pages
        else if(page < (pagesPerPage / 2))  pages = 1..pagesPerPage
        else {
            def minPage = page - ((pagesPerPage / 2) as int)
            def maxPage = page + ((pagesPerPage / 2) as int)
            if(maxPage > pages) pages = (maxPage - pagesPerPage)..pages
            else pages = minPage..maxPage
        }
        if(!pages && page > 1) {
            redirect action: "lista", params: params.search ? [search: params.search] : null
        }
        else if(pages && page > pages.max()){
            redirect action: "lista", params: [page: 1, search: params.search]
        }
        [polizze: polizze, pages: pages, page: page,utente:utente,totalPages:totalPages ]
    }
    def listFinanziate(){
        //GenCertFinLeasJob.triggerNow()
        //ElencoSettimanaleAUTOIMPORTJob.triggerNow()
        def principal = SecurityUtils.principal
        def utente,admin
        if(SecurityUtils.hasRole("ADMINPRESSTODAY")){
            admin=Admin.findById(principal.id)
            utente=admin
        }
        def max = 15, pagesPerPage = 7
        def page = (params.page ?: 1) as int
        def offset = (page - 1) * max
        def cerca = params.search
        def polizze = Polizza.createCriteria().list(max: max, offset: offset) {
            ne "tipoPolizza",TipoPolizza.CASH
            projections {
                createAlias ('dealer', 'd')
            }
            if(cerca) {
                or {
                    ilike "noPolizza","${cerca}"
                    ilike "cognome", "%${cerca}%"
                    ilike "partitaIva", "%${cerca}%"
                    ilike "coperturaRichiesta", "%${cerca}%"
                    ilike "d.ragioneSocialeD", "%${cerca}%"
                }
            }
            and {
                order('noPolizza', 'desc')
                order('d.ragioneSocialeD', 'asc')
                order('dateCreated', 'desc')

            }
        }

        def pages = Math.ceil(polizze.totalCount / max) as int
        if(pages <= 1) pages = []
        else if(pages <= pagesPerPage) pages = 1..pages
        else if(page < (pagesPerPage / 2))  pages = 1..pagesPerPage
        else {
            def minPage = page - ((pagesPerPage / 2) as int)
            def maxPage = page + ((pagesPerPage / 2) as int)
            if(maxPage > pages) pages = (maxPage - pagesPerPage)..pages
            else pages = minPage..maxPage
        }
        if(!pages && page > 1) {
            redirect action: "listFinanziate", params: params.search ? [search: params.search] : null
        }
        else if(pages && page > pages.max()){
            redirect action: "listFinanziate", params: [page: 1, search: params.search]
        }
        [polizze: polizze, pages: pages, page: page,utente:utente]
    }
    def listPaiPag(){
        //CertPAIJob.triggerNow()
        //CaricaCertPAIJob.triggerNow()
        def principal = SecurityUtils.principal
        def utente,admin
        if(SecurityUtils.hasRole("ADMINPRESSTODAY")){
            admin=Admin.findById(principal.id)
            utente=admin
        }
        def max = 15, pagesPerPage = 7
        def page = (params.page ?: 1) as int
        def offset = (page - 1) * max
        def cerca = params.search
        def polizze = PolizzaPaiPag.createCriteria().list(max: max, offset: offset) {
            eq "tipoPolizza",TipoPolizza.PAIPAGAMENTO
            projections {
                createAlias ('dealer', 'd')
            }
            if(cerca) {
                or {
                    ilike "noPolizza","${cerca}"
                    ilike "cognome", "%${cerca}%"
                    ilike "partitaIva", "%${cerca}%"
                    ilike "coperturaRichiesta", "%${cerca}%"
                    ilike "d.ragioneSocialeD", "%${cerca}%"
                }
            }
            and {
                order('noPolizza', 'desc')
                order('d.ragioneSocialeD', 'asc')
                order('dateCreated', 'desc')

            }
        }

        def pages = Math.ceil(polizze.totalCount / max) as int
        if(pages <= 1) pages = []
        else if(pages <= pagesPerPage) pages = 1..pages
        else if(page < (pagesPerPage / 2))  pages = 1..pagesPerPage
        else {
            def minPage = page - ((pagesPerPage / 2) as int)
            def maxPage = page + ((pagesPerPage / 2) as int)
            if(maxPage > pages) pages = (maxPage - pagesPerPage)..pages
            else pages = minPage..maxPage
        }
        if(!pages && page > 1) {
            redirect action: "listPaiPag", params: params.search ? [search: params.search] : null
        }
        else if(pages && page > pages.max()){
            redirect action: "listPaiPag", params: [page: 1, search: params.search]
        }
        [polizze: polizze, pages: pages, page: page,utente:utente]
    }
    def annullamenti(){
        def logg
        def polizze
        def principal = SecurityUtils.principal
        def utente,admin
        boolean rinnovi=false
        if(request.post){
            def db = rinnDb
            if(SecurityUtils.hasRole("ADMIN")){
                admin=Admin.findById(principal.id)
                utente=admin
            }
            def cerca = params.searchNo.toString().trim() ? params.searchNo.toString().trim() : params.searchCognome.toString().trim()
            /*if(params.searchNome){
                def parole, compParole
                parole= params.searchNome
                compParole= parole.split(" ")
                println parole
                if(compParole.size()==2){
                    cognomeB=parole[1]
                    cognomeB=cognomeB.trim()
                    nomeB=parole[0]
                    nomeB=nomeB.trim()
                }else if(parole.size()>2){
                    nomeB=parole[0]
                    nomeB=nomeB.trim()
                    for ( int ind=1; ind<parole.size();ind++) {
                        cognomeB+=parole[ind]+" "
                    }
                    cognomeB=cognomeB.trim()
                }else{
                    nomeB=cellaBeneficiario.trim()
                }

                for(int i =0; i < parole.size(); i++)
                {
                    if(cellaDenominazione.contains(parole[i]))
                    {
                        denominazione= true
                    }
                }
            }*/
            polizze = Polizza.createCriteria().list() {
                if(cerca) {
                    ne ("codOperazione","S")
                    ne "tipoPolizza",TipoPolizza.PAIPAGAMENTO
                    //eq ("annullata", false)
                    or {
                        ilike "noPolizza","%${cerca}%"
                        ilike "cognome", "%${cerca}%"
                       /* ilike "nome", "%${cerca}%"*/
                    }
                }
                and {
                    order('noPolizza', 'desc')
                    order('dataDecorrenza', 'desc')

                }
            }
            if(!polizze){
                if(cerca!=''){
                    def resultwebS = IAssicurWebService.getPolizzeRinnoviperAnnulla(cerca)
                    polizze=resultwebS.polizze
                    def erroreiAssicur=resultwebS.iAssicurError
                    def errore=resultwebS.error
                    if(errore==null && erroreiAssicur==null){
                        rinnovi=true
                    }
                    logg =new Log(parametri: "contenuto array result IAssicur per annullamenti/recesso RINNOVI ${resultwebS} ", operazione: "getPolizzeRinnovi", pagina: "ANNULLAMENTO RINNOVI")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }
            }
        }

        [polizze: polizze, utente:utente, rinnovi:rinnovi]
    }
    def nuovaPolizza(){
        def logg
        def polizza=new Polizza()
        def province=[Agrigento:"AG",Alessandria:"AL",Ancona:"AN",Aosta:"AO",Arezzo:"AR",AscoliPiceno:"AP",Asti:"AT",Avellino:"AV",Bari:"BA",BarlettaAndria:"BT",Belluno:"BL",Benevento:"BN",Bergamo:"BG",Biella:"BI",Bologna:"BO",Bolzano:"BZ",Brescia:"BS",Brindisi:"BR",Cagliari:"CA",Caltanissetta:"CL",Campobasso:"CB",Carbonia_Iglesia:"CI",Caserta:"CE",Catania:"CT",Catanzaro:"CZ",Chieti:"CH",Como:"CO",Cosenza:"CS",Cremona:"CR",Crotone:"KR",Cuneo:"CN",Enna:"EN",Fermo:"FM",Ferrara:"FE",Firenze:"FI",Foggia:"FG",Forli_Cesena:"FC",Frosinone:"FR",Genova:"GE",Gorizia:"GO",Grosseto:"GR",Imperia:"IM",Isernia:"IS",La_Spezia:"SP",L_Aquila:"AQ",Latina:"LT",Lecce:"LE",Lecco:"LC",Livorno:"LI",Lodi:"LO",Lucca:"LU",Macerata:"MC",Mantova:"MN",Massa_Carrara:"MS",Matera:"MT",Messina:"ME",Milano:"MI",Modena:"MO",Monza_e_della_Brianza:"MB",Napoli:"NA",Novara:"NO",Nuoro:"NU",Olbia_Tempio:"OT",Oristano:"OR",Padova:"PD",Palermo:"PA",Parma:"PR",Pavia:"PV",Perugia:"PG",Pesaro_e_Urbino:"PU",Pescara:"PE",Piacenza:"PC",Pisa:"PI",Pistoia:"PT",Pordenone:"PN",Potenza:"PZ",Prato:"PO",Ragusa:"RG",Ravenna:"RA",Reggio_Calabria:"RC",Reggio_Emilia:"RE",Rieti:"RI",Rimini:"RN",Roma:"RM",Rovigo:"RO",Repubblica_di_San_Marino:"RSM",Salerno:"SA",Medio_Campidano:"VS",Sassari:"SS",Savona:"SV",Siena:"SI",Siracusa:"SR",Sondrio:"SO",Taranto:"TA",Teramo:"TE",Terni:"TR",Torino:"TO",Ogliastra:"OG",Trapani:"TP",Trento:"TN",Treviso:"TV",Trieste:"TS",Udine:"UD",Varese:"VA",Venezia:"VE",Verbano_Cusio_Ossola:"VB",Vercelli:"VC",Verona:"VR",Vibo_Valentia:"VV",Vicenza:"VI",Viterbo:"VT"]
        if(request.post) {
            def dealerId=params.dealer.id ?: params.codice.id
            def dealer =  Dealer.get(dealerId)
            if(dealer){
                if((params.telefonoD.toString().trim()!='' && params.telefonoD.toString().trim() != dealer.telefonoD ))dealer.telefonoD=params.telefonoD.toString().trim()
                if((params.faxD.toString().trim()!='' && params.faxD.toString()!= dealer.faxD))dealer.faxD=params.faxD.toString().trim()
                if((params.emailD.toString().trim()!='' && params.emailD.toString()!= dealer.emailD)) dealer.emailD=params.emailD.toString().trim()
            }
            if(!dealer.save(flush:true)){
                flash.error = renderErrors(bean: dealer)
                logg =new Log(parametri: "Si è verificato un errore durante l'aggiornamento dei dati del dealer ${renderErrors(bean: dealer)} ", operazione: "creazione nuova polizza cash", pagina: "INSERISCE POLIZZA CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }else{
                params.nome=params.nome.toString().toUpperCase()
                params.cognome=params.cognome.toString().toUpperCase()
                params.partitaIva=params.partitaIva.toString().toUpperCase()
                logg =new Log(parametri: "parametri inserimento:[dealer:${params.dealer.id},dealerCensiti.ragioneSocialeD:${params.dealerCensiti.ragioneSocialeD}, percVenditore:${params.percVenditore}, premioImponibile:${params.premioImponibile}, codiceZonaTerritoriale:${params.codiceZonaTerritoriale}, rappel:${params.rappel}, imposte:${params.imposte}, cognome:${params.cognome} , nome:${params.nome}, partitaIva:${params.partitaIva}, indirizzo:${params.indirizzo}, localita:${params.localita}, cap:${params.cap}, tipoCliente:${params.tipoCliente}, provincia:${params.provincia}, telefono:${params.telefono?params.telefono:""}, email:${params.email ? params.email :  ""}, certificatoMail: ${params.certificatoMail}, marca: ${params.marca}, modello: ${params.modello}, targa: ${params.targa? params.targa:""}, nuovo:${params.nuovo}, onStar:${params.onStar}, telaio: ${params.telaio? params.telaio:""}, valoreAssicuratoconiva:${params.valoreAssicuratoconiva}, iva:${params.iva}, valoreAssicurato:${params.valoreAssicurato}, dataImmatricolazione:${params.dataImmatricolazione}, step:${params.step}, coperturaRichiesta:${params.coperturaRichiesta}, durata:${params.durata}, dataDecorrenza:${params.dataDecorrenza}, dataInserimento:${params.dataInserimento}, premioLordo:${params.premioLordo}, provvDealer:${params.provvDealer}, provvGmfi:${params.provvGmfi}, provvVenditore:${params.provvVenditore}  ]", operazione: "creazione nuova polizza cash", pagina: "INSERISCE POLIZZA CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                bindData(polizza, params, [exclude: ['dealer','coperturaRichiesta','dataScadenza','valoreAssicurato','valoreAssicuratoconiva','premioImponibile','premioLordo','provvDealer','provvGmfi','provvVenditore','tipoPolizza','telefono','targa','telaio','rappel','imposte','codCampagna']])
                def durata=params.durata
                def self=params.nome.toString()
                def dataScadenza
                if(durata=="12"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +12.months}:""
                }else if( durata=="18"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +18.months}:""
                }else if( durata=="24"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +24.months}:""
                }else if(durata=="30"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +30.months}:""
                }else if(durata=="36"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +36.months}:""
                }else if(durata=="42"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +42.months}:""
                }else if(durata=="48"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +48.months}:""
                }else if(durata=="54"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +54.months}:""
                }else if(durata=="60"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +60.months}:""
                }else if(durata=="66"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +66.months}:""
                }else if(durata=="72"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +72.months}:""
                }
                def copertura=params.coperturaRichiesta
                def coperturaRichiesta=""
                if(copertura=='silver'){
                    coperturaRichiesta="SILVER"
                }else if(copertura=='gold'){
                    coperturaRichiesta="GOLD"
                }else if(copertura=='platinum'){
                    coperturaRichiesta="PLATINUM"
                }else if(copertura=='platinumK'){
                    coperturaRichiesta="PLATINUM KASKO"
                }else if(copertura=='platinumC'){
                    coperturaRichiesta="PLATINUM COLLISIONE"
                }
                def telefono= params.telefono.toString().replaceAll("[^0-9]+","")
                //conversione valori decimali
                def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
                String pattern = "#,##0.0#"
                symbols.setGroupingSeparator((char) ',')
                symbols.setDecimalSeparator((char) '.')
                def  decimalFormat = new DecimalFormat(pattern, symbols)
                decimalFormat.setParseBigDecimal(true)
                def valoreAssicurato = (BigDecimal) decimalFormat.parse(params.valoreAssicurato)
                def valoreAssicuratoconiva = (BigDecimal) decimalFormat.parse(params.valoreAssicuratoconiva)
                def premioImponibile=(BigDecimal) decimalFormat.parse(params.premioImponibile)
                def premioLordo=(BigDecimal) decimalFormat.parse(params.premioLordo)
                def provvDealer=(BigDecimal) decimalFormat.parse(params.provvDealer)
                def provvGmfi=(BigDecimal) decimalFormat.parse(params.provvGmfi)
                def provvVenditore=(BigDecimal) decimalFormat.parse(params.provvVenditore)
                def rappel=(BigDecimal) decimalFormat.parse(params.rappel)
                def imposte=(BigDecimal) decimalFormat.parse(params.imposte)
                polizza.dealer=dealer
                polizza.dataScadenza=dataScadenza
                polizza.coperturaRichiesta=coperturaRichiesta
                polizza.valoreAssicurato=valoreAssicurato
                polizza.valoreAssicuratoconiva=valoreAssicuratoconiva
                polizza.premioImponibile=premioImponibile
                polizza.provvDealer=provvDealer
                polizza.provvGmfi=provvGmfi
                polizza.provvVenditore=provvVenditore
                polizza.premioLordo=premioLordo
                polizza.tipoPolizza=TipoPolizza.CASH
                polizza.telefono=telefono
                polizza.targa=params.targa?params.targa.toString().toUpperCase():null
                polizza.telaio=params.telaio?params.telaio.toString().toUpperCase():null
                polizza.rappel=rappel
                polizza.imposte=imposte
                polizza.codOperazione="0"
                polizza.codCampagna="0"
            }
            //aggiornamenti dati dealer
            if(polizza.save(flush:true)) {
                def tracciatoIassicur= tracciatiService.generaTracciatoIAssicur(polizza)
                if(!tracciatoIassicur.save(flush: true)){
                    logg =new Log(parametri: "Si è verificato un errore durante la creazione del tracciato IAssicur ${renderErrors(bean: tracciatoIassicur)} ", operazione: "creazione nuova polizza cash", pagina: "INSERISCE POLIZZA CASH")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    flash.error = renderErrors(bean: tracciatoIassicur)
                    redirect action: "lista", params: params.search ? [search: params.search] : [:]
                } else{
                    def tracciatoCompagnia= tracciatiService.generaTracciatoCompagnia(polizza)
                    if(!tracciatoCompagnia.save(flush: true)){
                        logg =new Log(parametri: "Si è verificato un errore durante la creazione del tracciato DL ${renderErrors(bean: tracciatoCompagnia)} ", operazione: "creazione nuova polizza cash", pagina: "INSERISCE POLIZZA CASH")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flash.error = renderErrors(bean: tracciatoCompagnia)
                        redirect action: "lista", params: params.search ? [search: params.search] : [:]
                    } else{
                       /* def invioMail=mailService.invioMail(polizza)
                        if(invioMail){
                            flash.message="Richiesta inviata e mail inviata al dealer ${polizza.dealer.ragioneSocialeD} \n e al sales specialist ${polizza.dealer.emailSales}"
                            redirect action: "lista", params: params.search ? [search: params.search] : [:]
                        }else{
                            flash.error = "c\u00E8 stato un errore con l\u00E8 invio della mail al dealer"
                            redirect action: "lista", params: params.search ? [search: params.search] : [:]
                        }*/
                        logg =new Log(parametri: "Richiesta inviata ", operazione: "creazione nuova polizza cash", pagina: "INSERISCE POLIZZA CASH")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flash.message="Richiesta inviata"
                        redirect action: "lista", params: params.search ? [search: params.search] : [:]
                    }
                }
            } else {
                flash.error = renderErrors(bean: polizza)
                logg =new Log(parametri: "Errore creazione polizza-> ${renderErrors(bean: polizza)}", operazione: "creazione nuova polizza cash", pagina: "INSERISCE POLIZZA CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
        }
        [polizza: polizza, province: province ]
    }
    def modificaPolizza(long id){
        def logg
        def polizza=Polizza.get(id)
        def province=[Agrigento:"AG",Alessandria:"AL",Ancona:"AN",Aosta:"AO",Arezzo:"AR",AscoliPiceno:"AP",Asti:"AT",Avellino:"AV",Bari:"BA",BarlettaAndria:"BT",Belluno:"BL",Benevento:"BN",Bergamo:"BG",Biella:"BI",Bologna:"BO",Bolzano:"BZ",Brescia:"BS",Brindisi:"BR",Cagliari:"CA",Caltanissetta:"CL",Campobasso:"CB",Carbonia_Iglesia:"CI",Caserta:"CE",Catania:"CT",Catanzaro:"CZ",Chieti:"CH",Como:"CO",Cosenza:"CS",Cremona:"CR",Crotone:"KR",Cuneo:"CN",Enna:"EN",Fermo:"FM",Ferrara:"FE",Firenze:"FI",Foggia:"FG",Forli_Cesena:"FC",Frosinone:"FR",Genova:"GE",Gorizia:"GO",Grosseto:"GR",Imperia:"IM",Isernia:"IS",La_Spezia:"SP",L_Aquila:"AQ",Latina:"LT",Lecce:"LE",Lecco:"LC",Livorno:"LI",Lodi:"LO",Lucca:"LU",Macerata:"MC",Mantova:"MN",Massa_Carrara:"MS",Matera:"MT",Messina:"ME",Milano:"MI",Modena:"MO",Monza_e_della_Brianza:"MB",Napoli:"NA",Novara:"NO",Nuoro:"NU",Olbia_Tempio:"OT",Oristano:"OR",Padova:"PD",Palermo:"PA",Parma:"PR",Pavia:"PV",Perugia:"PG",Pesaro_e_Urbino:"PU",Pescara:"PE",Piacenza:"PC",Pisa:"PI",Pistoia:"PT",Pordenone:"PN",Potenza:"PZ",Prato:"PO",Ragusa:"RG",Ravenna:"RA",Reggio_Calabria:"RC",Reggio_Emilia:"RE",Rieti:"RI",Rimini:"RN",Roma:"RM",Rovigo:"RO",Repubblica_di_San_Marino:"RSM",Salerno:"SA",Medio_Campidano:"VS",Sassari:"SS",Savona:"SV",Siena:"SI",Siracusa:"SR",Sondrio:"SO",Taranto:"TA",Teramo:"TE",Terni:"TR",Torino:"TO",Ogliastra:"OG",Trapani:"TP",Trento:"TN",Treviso:"TV",Trieste:"TS",Udine:"UD",Varese:"VA",Venezia:"VE",Verbano_Cusio_Ossola:"VB",Vercelli:"VC",Verona:"VR",Vibo_Valentia:"VV",Vicenza:"VI",Viterbo:"VT"]
        if(request.post) {
            def dealerId=params.dealer.id ?: params.codice.id
            def dealer =  Dealer.get(dealerId)
            if(dealer){
                if((params.telefonoD.toString().trim()!='' && params.telefonoD.toString().trim() != dealer.telefonoD ))dealer.telefonoD=params.telefonoD.toString().trim()
                if((params.faxD.toString().trim()!='' && params.faxD.toString()!= dealer.faxD))dealer.faxD=params.faxD.toString().trim()
                if((params.emailD.toString().trim()!='' && params.emailD.toString()!= dealer.emailD)) dealer.emailD=params.emailD.toString().trim()
            }
            if(!dealer.save(flush:true)){
                flash.error = renderErrors(bean: dealer)
                logg =new Log(parametri: "Errore modifica dealer-> ${renderErrors(bean: dealer)}", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

            }else{
                logg =new Log(parametri: "parametri modifica:[dealer:${params.dealer}, percVenditore:${params.percVenditore}, premioImponibile:${params.premioImponibile}, codiceZonaTerritoriale:${params.codiceZonaTerritoriale}, rappel:${params.rappel}, imposte:${params.imposte}, cognome:${params.cognome} , nome:${params.nome}, partitaIva:${params.partitaIva}, indirizzo:${params.indirizzo}, localita:${params.localita}, cap:${params.cap}, tipoCliente:${params.tipoCliente}, provincia:${params.provincia}, telefono:${params.telefono?params.telefono:""}, email:${params.email ? params.email :  ""}, certificatoMail: ${params.certificatoMail}, marca: ${params.marca}, modello: ${params.modello}, targa: ${params.targa? params.targa:""}, nuovo:${params.nuovo}, onStar:${params.onStar}, telaio: ${params.telaio? params.telaio:""}, valoreAssicuratoconiva:${params.valoreAssicuratoconiva}, iva:${params.iva}, valoreAssicurato:${params.valoreAssicurato}, dataImmatricolazione:${params.dataImmatricolazione}, step:${params.step}, coperturaRichiesta:${params.coperturaRichiesta}, durata:${params.durata}, dataDecorrenza:${params.dataDecorrenza}, dataInserimento:${params.dataInserimento}, premioLordo:${params.premioLordo}, provvDealer:${params.provvDealer}, provvGmfi:${params.provvGmfi}, provvVenditore:${params.provvVenditore}  ]", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                bindData(polizza, params, [exclude: ['dealer','coperturaRichiesta','dataScadenza','valoreAssicurato','valoreAssicuratoconiva','premioImponibile','premioLordo','provvDealer','provvGmfi','provvVenditore','telefono','targa','telaio','rappel','imposte']])
                def durata=params.durata
                def dataScadenza
                if(durata=="12"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +12.months}:""
                }else if( durata=="18"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +18.months}:""
                }else if( durata=="24"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +24.months}:""
                }else if(durata=="30"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +30.months}:""
                }else if(durata=="36"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +36.months}:""
                }else if(durata=="42"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +42.months}:""
                }else if(durata=="48"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +48.months}:""
                }else if(durata=="54"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +54.months}:""
                }else if(durata=="60"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +60.months}:""
                }else if(durata=="66"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +66.months}:""
                }else if(durata=="72"){
                    dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +72.months}:""
                }
                def copertura=params.coperturaRichiesta
                def coperturaRichiesta=""
                if(copertura=='silver'){
                    coperturaRichiesta="SILVER"
                }else if(copertura=='gold'){
                    coperturaRichiesta="GOLD"
                }else if(copertura=='platinum'){
                    coperturaRichiesta="PLATINUM"
                }else if(copertura=='platinumK'){
                    coperturaRichiesta="PLATINUM KASKO"
                }else if(copertura=='platinumC'){
                    coperturaRichiesta="PLATINUM COLLISIONE"
                }
                def telefono= params.telefono.toString().replaceAll("[^0-9]+","")

                //conversione valori decimali
                def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
                String pattern = "#,##0.0#"
                symbols.setGroupingSeparator((char) ',')
                symbols.setDecimalSeparator((char) '.')
                def  decimalFormat = new DecimalFormat(pattern, symbols)
                decimalFormat.setParseBigDecimal(true)

                def valoreAssicurato = (BigDecimal) decimalFormat.parse(params.valoreAssicurato)
                def valoreAssicuratoconiva = (BigDecimal) decimalFormat.parse(params.valoreAssicuratoconiva)
                def premioImponibile=(BigDecimal) decimalFormat.parse(params.premioImponibile)
                def premioLordo=(BigDecimal) decimalFormat.parse(params.premioLordo)
                def provvDealer=(BigDecimal) decimalFormat.parse(params.provvDealer)
                def provvGmfi=(BigDecimal) decimalFormat.parse(params.provvGmfi)
                def provvVenditore=(BigDecimal) decimalFormat.parse(params.provvVenditore)
                def rappel=(BigDecimal) decimalFormat.parse(params.rappel)
                def imposte=(BigDecimal) decimalFormat.parse(params.imposte)
                polizza.dealer=dealer
                polizza.dataScadenza=dataScadenza
                polizza.coperturaRichiesta=coperturaRichiesta
                polizza.valoreAssicurato=valoreAssicurato
                polizza.valoreAssicuratoconiva=valoreAssicuratoconiva
                polizza.premioImponibile=premioImponibile
                polizza.provvDealer=provvDealer
                polizza.provvGmfi=provvGmfi
                polizza.provvVenditore=provvVenditore
                polizza.premioLordo=premioLordo
                polizza.telefono=telefono
                polizza.targa=params.targa?:null
                polizza.telaio=params.telaio?:null
                polizza.rappel=rappel
                polizza.imposte=imposte
            }

            /*if(dataScadenza!=""){
                GregorianCalendar calendar = new GregorianCalendar()
                def yearInt = Integer.parseInt(dataScadenza.format("yyyy"))
                def monthInt = Integer.parseInt(dataScadenza.format("MM"))
                monthInt = monthInt - 1
                calendar.set(yearInt, monthInt, 1)
                def dayInt = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)
                dataScadenza=new GregorianCalendar(yearInt,monthInt,dayInt).format("dd-MM-yyyy")
                Date dataFormatata = Date.parse( "dd-MM-yyyy", dataScadenza )
                polizza.dataScadenza=dataFormatata
            }*/
            //aggiornamenti dati dealer
            if(polizza.tracciatoId!=null){
                def tracciatoId= polizza.tracciatoId
                def tracciatoPol=Tracciato.get(tracciatoId)
                if(tracciatoPol.dataCaricamento==null){tracciatoPol.dataCaricamento=new Date()}
            }
            if(polizza.tracciatoCompagniaId!=null){
                def tracciatoId= polizza.tracciatoCompagniaId
                def tracciatoPol=TracciatoCompagnia.get(tracciatoId)
                if(tracciatoPol.dataCaricamento==null){tracciatoPol.dataCaricamento=new Date()}
            }
            if(polizza.save(flush:true)) {
                logg =new Log(parametri: "polizza modificata", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                //def invioMail=mailService.invioMail(polizza)
                //println invioMail
                if(polizza.tracciato!=null){
                    def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                    if(!tracciatoPolizza.save(flush: true)){
                        flash.error = renderErrors(bean: tracciatoPolizza)
                        logg =new Log(parametri: "Errore modifica tracciato IAssicur Polizza-> ${renderErrors(bean: tracciatoPolizza)}", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    } else{
                        logg =new Log(parametri: "tracciato IAssicur salvato", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        if(polizza.tracciatoCompagnia!=null){
                            def tracciatoComp= tracciatiService.generaTracciatoCompagnia(polizza)
                            if(!tracciatoComp.save(flush: true)){
                                flash.error = renderErrors(bean: tracciatoComp)
                                logg =new Log(parametri: "Errore modifica tracciato DL Polizza-> ${renderErrors(bean: tracciatoComp)}", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            } else{
                                logg =new Log(parametri: "tracciato compagnia salvato", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                flash.message="RICHIESTA INVIATA"
                                redirect action: "lista", params: params.search ? [search: params.search] : [:]
                            }
                        }else{
                            def tracciatoComp= tracciatiService.generaTracciatoCompagnia(polizza)
                            if(!tracciatoComp.save(flush: true)){
                                logg =new Log(parametri: "Errore modifica tracciato DL Polizza-> ${renderErrors(bean: tracciatoComp)}", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                flash.error = renderErrors(bean: tracciatoComp)
                            } else{
                                logg =new Log(parametri: "tracciato compagnia salvato", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                flash.message="RICHIESTA INVIATA"
                                redirect action: "lista", params: params.search ? [search: params.search] : [:]
                            }
                        }

                    }
                }else{
                    def tracciatoPolizza= tracciatiService.generaTracciatoIAssicur(polizza)
                    if(!tracciatoPolizza.save(flush: true)){
                        logg =new Log(parametri: "Errore modifica tracciato IAssicur Polizza-> ${renderErrors(bean: tracciatoPolizza)}", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flash.error = renderErrors(bean: tracciatoPolizza)
                    } else{
                        logg =new Log(parametri: "tracciato IAssicur salvato", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        if(polizza.tracciatoCompagnia!=null){
                            def tracciatoComp= tracciatiService.generaTracciatoCompagnia(polizza)
                            if(!tracciatoComp.save(flush: true)){
                                logg =new Log(parametri: "Errore modifica tracciato DL Polizza-> ${renderErrors(bean: tracciatoComp)}", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                flash.error = renderErrors(bean: tracciatoComp)
                            } else{
                                logg =new Log(parametri: "tracciato compagnia salvato", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                flash.message="RICHIESTA INVIATA"
                                redirect action: "lista", params: params.search ? [search: params.search] : [:]
                            }
                        }else{
                            def tracciatoComp= tracciatiService.generaTracciatoCompagnia(polizza)
                            if(!tracciatoComp.save(flush: true)){
                                logg =new Log(parametri: "Errore modifica tracciato DL Polizza-> ${renderErrors(bean: tracciatoComp)}", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                flash.error = renderErrors(bean: tracciatoComp)
                            } else{
                                logg =new Log(parametri: "tracciato compagnia salvato", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                flash.message="RICHIESTA INVIATA"
                                redirect action: "lista", params: params.search ? [search: params.search] : [:]
                            }
                        }
                    }
                }

            } else {
                logg =new Log(parametri: "Errore modifica Polizza-> ${renderErrors(bean: polizza)}", operazione: "modifica polizza cash", pagina: "MODIFICA POLIZZA CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                flash.error = renderErrors(bean: polizza)
            }
        }
        [polizza: polizza, province: province ]
    }
    def dealerCensiti(String ragioneSocialeD){
        def dealerCensiti = Dealer.withCriteria {
            resultTransformer org.hibernate.criterion.CriteriaSpecification.ALIAS_TO_ENTITY_MAP
            ilike "ragioneSocialeD", "%${ragioneSocialeD}%"
            eq "attivo",true
            projections {
                property "id", "id"
                property "ragioneSocialeD", "ragioneSocialeD"
            }
        }
        if(dealerCensiti){
            render dealerCensiti as JSON
        }else{
            dealerCensiti=[id:0]
            render dealerCensiti as JSON
        }

    }
    def datiDealerT(String id) {
        def datiDealerT
        def dealer
        if(request.post) {
            if(id!=0){
                dealer= Dealer.findById(id)
                datiDealerT = [id:dealer.id, username:dealer.codice,ragioneSociale:dealer.ragioneSocialeD,indirizzo:dealer.indirizzoD,piva:dealer.pivaD,localita:dealer.localitaD,cap:dealer.capD,provincia:dealer.provinciaD,email:dealer.emailD?:"",telefono:dealer.telefonoD?:"",fax:dealer.faxD?:"",provvVenditore:dealer.provvVenditore]
            }else{
                datiDealerT = [id:0,username:"",ragioneSociale:"",indirizzo:"",piva:"",localita:"",cap:"",provincia:"",email:"",telefono:"",fax:"",provvVenditore:""]
            }
            render datiDealerT as JSON
        }
        else response.sendError(404)
    }
    def getDealer(long nodealer){
        def dealer=Dealer.findById(nodealer)
        def risposta
        if(dealer){
            risposta=[id:dealer.id, codice:dealer.codice,ragioneSocialeD:dealer.ragioneSocialeD,indirizzoD:dealer.indirizzoD,pivaD:dealer.pivaD,localitaD:dealer.localitaD,capD:dealer.capD,provinciaD:dealer.provinciaD,emailD:dealer.emailD?:"",telefonoD:dealer.telefonoD?:"",faxD:dealer.faxD?:"",dealerType:dealer.dealerType,percVenditore:dealer.percVenditore,risposta:true]
        }else{risposta=[risposta:"nonEsistenessuna"]}
        render risposta as JSON
    }
    def dealerCensitiCodici(String codice){
        def dealerCensitiCodici = Dealer.withCriteria {
            resultTransformer org.hibernate.criterion.CriteriaSpecification.ALIAS_TO_ENTITY_MAP
            eq "codice", "${codice}"
            eq "attivo",true
            projections {
                property "id", "id"
                property "codice", "codice"
            }
        }
        if(dealerCensitiCodici){
            render dealerCensitiCodici as JSON
        }else{
            dealerCensitiCodici=[id:0]
            render dealerCensitiCodici as JSON
        }

    }
    def datiDealerTCodice(String id) {
        def datiDealerTCodice
        def dealer
        if(request.post) {
            if(id!=0){
                dealer= Dealer.findById(id)
                datiDealerTCodice = [id:dealer.id, username:dealer.codice,ragioneSociale:dealer.ragioneSocialeD,indirizzo:dealer.indirizzoD,piva:dealer.pivaD,localita:dealer.localitaD,cap:dealer.capD,provincia:dealer.provinciaD,email:dealer.emailD?:"",telefono:dealer.telefonoD?:"",fax:dealer.faxD?:"",percVenditore:dealer.percVenditore]
            }else{
                datiDealerTCodice = [id:0,username:"",ragioneSociale:"",indirizzo:"",piva:"",localita:"",cap:"",provincia:"",email:"",telefono:"",fax:"",provvVenditore:""]
            }
            render datiDealerTCodice as JSON
        }
        else response.sendError(404)
    }
    def getDealerCodice(long nodealer){
        def dealer=Dealer.findById(nodealer)
        def risposta
        if(dealer){
            risposta=[id:dealer.id, codice:dealer.codice,ragioneSocialeD:dealer.ragioneSocialeD,indirizzoD:dealer.indirizzoD,pivaD:dealer.pivaD,localitaD:dealer.localitaD,capD:dealer.capD,provinciaD:dealer.provinciaD,emailD:dealer.emailD?:"",telefonoD:dealer.telefonoD?:"",faxD:dealer.faxD?:"",dealerType:dealer.dealerType,percVenditore:dealer.percVenditore,risposta:true]
        }else{risposta=[risposta:"nonEsistenessuna"]}
        render risposta as JSON
    }
    def chiamataWS(){
        def logg
        def tipoPolizza=""
        if(params.tipoPolizza){
            tipoPolizza=params.tipoPolizza
        }
        def paymenttype="Funded"

        /*implementazione nuova tariffa*/
        def tariffa=params.tariffa
        /*def modello=params.modello
        println "modello $modello\n"*/
        def copertura=params.copertura
        def valoreAssicurato=params.valoreAssicurato
        def provincia=params.provincia
        def comune=params.comune
        comune=comune.toString().toUpperCase()
        def durata=params.durata
        def antifurtoval=params.onStar
        def antifurto
        println "${antifurtoval}"
        if(antifurtoval=="1" || antifurtoval=="true") antifurto="S"
        else antifurto="N"
        println antifurto
        def dealerType=params.dealerType
        def step=params.step
        def dataEffetto=new Date().parse("dd-MM-yyyy", params.dataDecorrenza).format("yyyy-MM-dd")
        if(tariffa != '' && tariffa!= null){
            println "tariffa--> $tariffa"
            if(tariffa=="2"){
                dataEffetto="2017-04-18"
            }else if (tariffa=="1"){
                dataEffetto="2017-04-17"
            }
        }
        def tipoOperazione=params.operazione
        def classeprovvi=""
        def stepcodifica=""
        def coperturacodifica=""
        if(step.toString().equalsIgnoreCase("step 1")){
            classeprovvi="1"
        }else if(step.toString().equalsIgnoreCase("step 2")){
            classeprovvi="2"
        }else if(step.toString().equalsIgnoreCase("step 3")){
            classeprovvi="3"
        }
        if(copertura.toString()=="silver"){
            coperturacodifica="SILVER"
        }else if(copertura.toString()=="gold"){
            coperturacodifica="GOLD"
        }else if(copertura.toString()=="platinum"){
            coperturacodifica="PLATINUM"
        }else if(copertura.toString()=="platinumK"){
            coperturacodifica="PLATINUM KASKO"
        }else if(copertura.toString()=="platinumC"){
            coperturacodifica="PLATINUM COLLISIONE"
        }
        def pacchetto=""
        if(tipoPolizza!='RINN'){
            /*if(tariffa=="2"){
                pacchetto=CodProdotti.findByCoperturaAndStep(coperturacodifica, step).codPacchettodlflex2

            }else{*/
                pacchetto=CodProdotti.findByCoperturaAndStep(coperturacodifica, step).codPacchetto

            //}
        }else{
            pacchetto=params.codPacchetto
        }

        def percentualeVenditore=params.percVenditore
        if(percentualeVenditore=="0E-7"){
            percentualeVenditore=0.0
        }

        def test="N"
        /*if(Environment.current != Environment.DEVELOPMENT) {
            test="N"
        }else{
            test="S"
        }*/
        def parameters=[]
        if(tipoOperazione=="A"){
            parameters  =[
                    codicepacchetto: "${pacchetto}",
                    valoreassicurato: valoreAssicurato,
                    durata: durata,
                    provincia: provincia,
                    comune: comune,
                    operazione: tipoOperazione,
                    dataeffettocopertura: dataEffetto,
                    antifurto: antifurto,
                    dealerType: dealerType,
                    classeProvvigionale: classeprovvi,
                    percentualeVenditore: percentualeVenditore,
                    test:test,
                    paymenttype:paymenttype
            ]
        }else{
            def dataScadenza=params.dataScadenza
            def dataStorno=params.dataStorno
            dataEffetto=params.dataDecorrenza
            parameters  =[
                    codicepacchetto: "${pacchetto}",
                    valoreassicurato: valoreAssicurato,
                    durata: durata,
                    provincia: provincia,
                    comune: comune,
                    operazione: tipoOperazione,
                    dataeffettocopertura: dataEffetto,
                    antifurto: antifurto,
                    dealerType: dealerType,
                    classeProvvigionale: classeprovvi,
                    percentualeVenditore: percentualeVenditore,
                    test:test,
                    datascadenzacopertura:dataScadenza,
                    dataeffettostorno:dataStorno
            ]
        }
        logg =new Log(parametri: "parametri inviate al webservice->${parameters}", operazione: "chiamata ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza CASH")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def url = "http://gmf.mach-1.it"

        def js = (parameters as JSON).toString()
        js = js.substring(1, js.length()-1)
        def path = "/calcprovvigioni/"
       /* if(Environment.current != Environment.PRODUCTION && tipoOperazione!="E"){
            path = "/test/calcprovvigioni/"

        }else if(tipoOperazione=="E"){
            path = "/calcprovvigioni/"
        }*/
        logg =new Log(parametri: "il path che viene chiamato->${path}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza CASH")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def query = "{"+js+"}"
        def errore=[]
        boolean risposta=false
        def stringaresponse=""
        def esito, provvDealer,provvgmfi,provvVend,premioLordo,premioImponibile,codiceZonaTerritoriale,rappel,imposte,provvMansutti,provvMach1
        def response = DLWebService.postText(url, path, [p:query])


        JSONObject userJson = JSON.parse(response)
        userJson.each { id, data ->
            logg =new Log(parametri: "dati webservice->${data}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza CASH")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if(data.Errors?.descErrore){
                def desErrore=data.Errors?.descErrore?.toString().replaceAll("\\[","").replaceAll("]","")
                logg =new Log(parametri: "errore riportato dal webservice->${desErrore}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(!((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con")))){
                    risposta=false
                    errore = data.Errors?.descErrore.toString().replaceAll("\\[","").replaceAll("]","")
                }else if (((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con"))) &&!( desErrore.contains("Provincia non valida"))){
                    risposta=true
                }
                else if (((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con"))) && desErrore.contains("Provincia non valida")){
                    risposta=false
                    errore = data.Errors?.descErrore?.toString().replaceAll("\\[","").replaceAll("]","")
                }
            }
            else{
                risposta=true
            }
            logg =new Log(parametri: "valore parametro risposta->${risposta}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza CASH")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if(risposta){
                esito=data.Esito ?:1
                premioLordo=data.PremioLordo?:0.0
                premioImponibile=data.Premio?:0.0
                provvDealer=data.provvigioneDealer?:0.0
                provvgmfi=data.provvigioneGMF?:0.0
                provvVend=data.provvigioneVenditore?:0.0
                provvMansutti=data.provvigioneMansutti?:0.0
                provvMach1=data.provvigioneMach1?:0.0
                codiceZonaTerritoriale=data.IPRPremioCalcolato.codiceZonaTerritoriale.toString().replaceAll("\\[","").replaceAll("]","")?:""
                rappel=data.IPRPremioCalcolato.rappel.toString().replaceAll("\\[","").replaceAll("]","")?:0.0
                imposte=data.IPRPremioCalcolato.imposte.toString().replaceAll("\\[","").replaceAll("]","")?:0.0

            }
            if(rappel==0.0){
                risposta=false
                errore="il rappel ha dato come risultato 0.0"
            }
        }
        if(risposta) stringaresponse=[risposta:risposta,esito:esito,premioLordo:premioLordo,provvDealer:provvDealer,provvgmfi:provvgmfi,provvMansutti:provvMansutti,provvMach1:provvMach1,provvVend:provvVend,premioImponibile:premioImponibile,codiceZonaTerritoriale:codiceZonaTerritoriale,rappel:rappel,imposte:imposte]
        else stringaresponse=[risposta:risposta,esito:esito,errore:errore,premioLordo:premioLordo,provvDealer:provvDealer,provvgmfi:provvgmfi,provvMansutti:provvMansutti,provvMach1:provvMach1,provvVend:provvVend,premioImponibile:premioImponibile,codiceZonaTerritoriale:codiceZonaTerritoriale,rappel:rappel,imposte:imposte]
        errore=[]
        render stringaresponse as JSON
    }
    def generaFilePolizzeattivate(){

        def polizze=Polizza.createCriteria().list(){
            eq "tipoPolizza", TipoPolizza.CASH
        }


        def wb = Stopwatch.log { ExcelBuilder.create {
            style("header") {
                background bisque
                font {
                    bold(true)
                }
            }
            sheet("Polizze") {
                row(style: "header") {
                    cell("Numero Polizza")
                    cell("Dealer")
                    cell("Nome Contraente")
                    cell("Cognome Contraente")
                    cell("Targa")
                    cell("Marca")
                    cell("Modello")
                    cell("Copertura")
                    cell("Durata")
                    cell("On Star")
                    cell("Data Inserimento")
                }
                if(polizze){
                    polizze.each { polizza ->
                        row {
                            cell(polizza.noPolizza)
                            cell(polizza.dealer.ragioneSocialeD.toString().toUpperCase())
                            cell(polizza.nome.toString().toUpperCase())
                            cell(polizza.cognome.toString().toUpperCase())
                            cell(polizza.targa!=null?polizza.targa.toString().toUpperCase():"")
                            cell(polizza.marca.toString().toUpperCase())
                            cell(polizza.modello.toString().toUpperCase())
                            cell(polizza.coperturaRichiesta.toString().toUpperCase())
                            cell(polizza.durata)
                            cell((polizza.onStar==true)?"S":"N")
                            cell(polizza.dataInserimento)
                        }
                        polizza.discard()
                    }
                    for (int i = 0; i < 17; i++) {
                        sheet.autoSizeColumn(i)
                    }
                }else{
                    row{
                        cell("non ci sono polizze attivate")
                    }
                }

            }

         }
        }

        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        response.addHeader "Content-disposition", "inline; filename=polizze_${new Date().format("ddMMyyyy")}.xlsx"
        response.outputStream << stream.toByteArray()
    }
    def flussoIassicur(long id) {
        if(Polizza.exists(id)) {
            def flussoIassicur
            def polizza = Polizza.get(id)
            if(polizza.tracciato) {
                flussoIassicur=polizza.tracciato
            }else{
                flussoIassicur = tracciatiService.generaTracciatoIAssicur(polizza)
                if (!flussoIassicur.save(flush: true)) {
                    flash.errors = "Si è verificato un errore durante la generazione del flusso iAssicur"
                    log.error renderErrors(bean: flussoIassicur)
                    redirect action: "lista"
                    return
                }
            }
            byte[] b = flussoIassicur.tracciato.getBytes()
            response.contentType = "text/plain"
            response.addHeader "Content-disposition", "attachment; filename=tracciatoIASSICUR_${polizza.noPolizza}.txt"
            response.outputStream << b
        } else response.sendError(404)
    }
    def generaFlussiAnnulla() {
        def logg
        def id=Long.parseLong(params.idPolizzasel)
        if(Polizza.exists(id)) {
            def flussoIassicur
            def polizza = Polizza.get(id)
            polizza.annullata=true
            polizza.codOperazione="E"
            if (!polizza.save(flush: true)) {
                logg =new Log(parametri: "Si è verificato un errore durante l'annullamento della polizza-->${renderErrors(bean: polizza)}", operazione: "genera flussi Annulla", pagina: "ANNULLAMENTI polizza CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                flash.errors = "Si è verificato un errore durante l'annullamento della polizza"
                log.error renderErrors(bean: polizza)
                redirect action: "annullamenti"
            }else{
                logg =new Log(parametri: "La polizza è stata aggiornata come annullata", operazione: "genera flussi Annulla", pagina: "ANNULLAMENTI polizza CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                flussoIassicur = tracciatiService.generaTracciatoIAssicur(polizza)
                if (!flussoIassicur.save(flush: true)) {
                    logg =new Log(parametri: "Si è verificato un errore durante la generazione del tracciato IAssicur per l'annullamento della polizza-->${renderErrors(bean: flussoIassicur)}", operazione: "genera flussi Annulla", pagina: "ANNULLAMENTI polizza CASH")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    flash.errors = "Si è verificato un errore durante la generazione del flusso iAssicur"
                    log.error renderErrors(bean: flussoIassicur)
                    redirect action: "annullamenti"
                }else{
                    logg =new Log(parametri: "la polizza \u00E8 stata messa come annullata e il tracciato IAssicur generato", operazione: "genera flussi Annulla", pagina: "ANNULLAMENTI polizza CASH")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    redirect action: "annullamenti"
                }
            }
        } else response.sendError(404)
    }
    def elencoFlussiIassicur() {
        def traccIassicur = /*SqlLogger.log {*/Tracciato.createCriteria().list() {
            isNull ("dataCaricamento")
            eq "cash", false
            eq "annullata", false
        }
        def data=new Date()
        if(traccIassicur) {
            byte[] b = traccIassicur.tracciato.join("\n").getBytes()
            response.contentType = "text/plain"
            response.addHeader "Content-disposition", "attachment; filename=flussiFINANZIATE_LEASING_${data.format("yyyyMMdd")}.txt"
            response.outputStream << b
        } //else response.sendError(404)
        else{
            flash.error = "Non ci sono flussi FINANZIATE / LEASING da mostrare"
            redirect action: "listFinanziate"
            return
        }
    }
    def elencoFlussiIassicurAnnulla() {
        def dataoggi=new Date().clearTime()
        def traccIassicur = /*SqlLogger.log {*/Tracciato.createCriteria().list() {
            isNull ("dataCaricamento")
           // ge "dataCaricamento", dataoggi
            projections {
                createAlias ('polizza', 'p')
            }
            or {
                eq "p.codOperazione", "E"
                eq "p.codOperazione", "S"
                eq "annullata", true
            }

        }
        def traccIASSR = /*SqlLogger.log {*/Tracciato.createCriteria().list() {
            isNull ("dataCaricamento")
            and{
                or {
                    eq "tipoPolizza",TipoPolizza.RINNOVI

                }
            }
        }
        def data=new Date()

        if(traccIassicur || traccIASSR ) {
            byte[] b = traccIassicur.tracciato.join("\n").getBytes()
            byte[] c = traccIASSR.tracciato.join("\n").getBytes()
            byte[] d  = "\n".getBytes()
            def outputStream = new ByteArrayOutputStream()
            outputStream.write(b)
            outputStream.write(d)
            outputStream.write(c)
            response.contentType = "text/plain"
            response.addHeader "Content-disposition", "attachment; filename=flussi_IASSICUR_ANNULLATI_${data.format("yyyyMMdd")}.txt"
            response.outputStream << outputStream.toByteArray()
        } //else response.sendError(404)
        else{
            flash.error = "Non ci sono flussi di ANNULLAMENTI da mostrare"
            redirect action: "annullamenti"
            return
        }
    }
    def elencoFlussiDL() {
        def traccDL = /*SqlLogger.log {*/TracciatoCompagnia.createCriteria().list() {
            isNull ("dataCaricamento")
            eq "cash", false
            eq "annullata", false
        }
        def data=new Date()
        if(traccDL) {
            byte[] b = traccDL.tracciatoCompagnia.join("\n").getBytes()
            response.contentType = "text/plain"
            response.addHeader "Content-disposition", "attachment; filename=flussiDL_FINANZIATE_LEASING_${data.format("yyyyMMdd")}.txt"
            response.outputStream << b
        } //else response.sendError(404)
        else{
            flash.error = "Non ci sono flussi DL FINANZIATE / LEASING da mostrare"
            redirect action: "listFinanziate"
            return
        }
    }
    def elencotuttiFlussiDL() {
        def traccDL = /*SqlLogger.log {*/TracciatoCompagnia.createCriteria().list() {
            isNull ("dataCaricamento")
            eq "annullata", false
        }
        def data=new Date()
        if(traccDL) {
            byte[] b = traccDL.tracciatoCompagnia.join("\n").getBytes()
            response.contentType = "text/plain"
            response.addHeader "Content-disposition", "attachment; filename=flussiDL_${data.format("yyyyMMdd")}.txt"
            response.outputStream << b
        } //else response.sendError(404)
        else{
            flash.error = "Non ci sono flussi DL da mostrare"
            redirect action: "listFinanziate"
            return
        }
    }
    def elencoFlussiDLAnnulla() {
        def dataoggi=new Date().clearTime()
        def traccDL = /*SqlLogger.log {*/TracciatoCompagnia.createCriteria().list() {
            isNull ("dataCaricamento")
            and{
                or {
                    projections {
                        createAlias ('polizza', 'p')
                    }
                    eq "p.codOperazione", "E"
                    eq "p.codOperazione", "S"
                    eq "annullata", true
                    //eq "tipoPolizza",TipoPolizza.RINNOVI

                }
            }
        }
        def traccDLR = /*SqlLogger.log {*/TracciatoCompagnia.createCriteria().list() {
            isNull ("dataCaricamento")
            and{
                or {
                    eq "tipoPolizza",TipoPolizza.RINNOVI

                }
            }
        }
        def data=new Date()
        if(traccDL || traccDLR) {
            byte[] b = traccDL.tracciatoCompagnia.join("\n").getBytes()
            byte[] c  = traccDLR.tracciatoCompagnia.join("\n").getBytes()
            byte[] d  = "\n".getBytes()
            def outputStream = new ByteArrayOutputStream()
            outputStream.write(b)
            outputStream.write(d)
            outputStream.write(c)
            response.contentType = "text/plain"
            response.addHeader "Content-disposition", "attachment; filename=flussi_DL_ANNULLATI_${data.format("yyyyMMdd")}.txt"
            response.outputStream << outputStream.toByteArray()
        } //else response.sendError(404)
        else{
            flash.error = "Non ci sono flussi DL di ANNULLAMENTI da mostrare"
            redirect action: "annullamenti"
            return
        }
    }
    def elencoFlussiPAI() {
        def traccPAI = /*SqlLogger.log {*/TracciatoPAI.createCriteria().list() {
            isNull ("dataCaricamento")
            eq "cash", false
            eq "omaggio", false
            ne "tipo", TipoPai.PAGAMENTO
        }
        def data=new Date()
        if(traccPAI) {
            byte[] b = traccPAI.tracciatoPAI.join("\n").getBytes()
            response.contentType = "text/plain"
            response.addHeader "Content-disposition", "attachment; filename=flussiPAI_${data.format("yyyyMMdd")}.txt"
            response.outputStream << b
        } //else response.sendError(404)
        else{
            flash.error = "Non ci sono flussi PAI da mostrare"
            redirect action: "listFinanziate"
            return
        }
    }
    def elencoFlussiPAISpe() {
        def dataOdierna=new Date()
        dataOdierna= dataOdierna.clearTime()
        def traccPAI = /*SqlLogger.log {*/TracciatoPAI.createCriteria().list() {
            //isNull ("dataCaricamento")
            ge "dataInvio", dataOdierna
            eq "cash", false
            eq "speciale", true
            eq "omaggio", false
            ne "tipo", TipoPai.PAGAMENTO
        }
        def data=new Date()
        if(traccPAI) {
            byte[] b = traccPAI.tracciatoPAI.join("\n").getBytes()
            response.contentType = "text/plain"
            response.addHeader "Content-disposition", "attachment; filename=flussiPAISPE_${data.format("yyyyMMdd")}.txt"
            response.outputStream << b
        } //else response.sendError(404)
        else{
            flash.error = "Non ci sono flussi PAI da mostrare"
            redirect action: "listFinanziate"
            return
        }
    }
    def elencoFlussiPAIOmaggio() {
        def dataOdierna=new Date()
        dataOdierna= dataOdierna.clearTime()
        def traccPAI = /*SqlLogger.log {*/TracciatoPAI.createCriteria().list() {
            isNull ("dataCaricamento")
            eq "omaggio", true
            ne "tipo", TipoPai.PAGAMENTO
        }
        def data=new Date()
        if(traccPAI) {
            byte[] b = traccPAI.tracciatoPAI.join("\n").getBytes()
            response.contentType = "text/plain"
            response.addHeader "Content-disposition", "attachment; filename=flussiPAIOmagg_${data.format("yyyyMMdd")}.txt"
            response.outputStream << b
        } //else response.sendError(404)
        else{
            flash.error = "Non ci sono flussi PAI da mostrare"
            redirect action: "listFinanziate"
            return
        }
    }
    def flussoDirectLine(long id) {
        if(Polizza.exists(id)) {
            def flussoDirectLine
            def polizza = Polizza.get(id)
            if(polizza.tracciatoCompagnia!=null) {
                flussoDirectLine = polizza.tracciatoCompagnia
            }else{
                flussoDirectLine = tracciatiService.generaTracciatoCompagnia(polizza)
                if (!flussoDirectLine.save(flush: true)) {
                    flash.errors = "Si è verificato un errore durante la generazione del flusso iAssicur"
                    log.error renderErrors(bean: flussoDirectLine)
                    redirect action: "lista"
                    return
                }
            }
            byte[] b = flussoDirectLine.tracciatoCompagnia.getBytes()
            response.contentType = "application/octet-stream"
            response.addHeader "Content-disposition", "attachment; filename=flussoDL_${polizza.noPolizza}.txt"
            response.outputStream << b
        } else response.sendError(404)
    }
    def flussoPAI(long id) {
        if(Polizza.exists(id)) {
            def flussoPAI
            def polizza = Polizza.get(id)
            if(polizza.tracciatoPAI!=null) {
                flussoPAI = polizza.tracciatoPAI
            }else{
                flussoPAI = tracciatiService.generaTracciatoPAI(polizza, false)
                if (!flussoPAI.save(flush: true)) {
                    flash.errors = "Si è verificato un errore durante la generazione del flusso PAI"
                    log.error renderErrors(bean: flussoPAI)
                    redirect action: "lista"
                    return
                }
            }
            byte[] b = flussoPAI.tracciatoPAI.getBytes()
            response.contentType = "text/plain"
            response.addHeader "Content-disposition", "attachment; filename=tracciato_PAI${polizza.noPolizza}.txt"
            response.outputStream << b
        } else response.sendError(404)
    }
    def flussoPAIPag(long id) {
        if(PolizzaPaiPag.exists(id)) {
            def flussoPAI
            def polizza = PolizzaPaiPag.get(id)
            if(polizza.tracciatoPAIPag != null) {
                flussoPAI = polizza.tracciatoPAIPag
            }else{
                flussoPAI = tracciatiService.generaTracciatoPAIPAgamento(polizza, false)
                if (!flussoPAI.save(flush: true)) {
                    flash.errors = "Si è verificato un errore durante la generazione del flusso PAI"
                    log.error renderErrors(bean: flussoPAI)
                    redirect action: "lista"
                    return
                }
            }
            byte[] b = flussoPAI.tracciatoPAIPaga.getBytes()
            response.contentType = "text/plain"
            response.addHeader "Content-disposition", "attachment; filename=tracciato_PAI${polizza.noPolizza}.txt"
            response.outputStream << b
        } else response.sendError(404)
    }
    def caricaPolizzaPAIOmaggio() {
        def polizze
        def logg
        def listPratiche=""
        def listFlussiPAI=""
        def listErrori=""
        def totaleErr=0
        def totale=0
        boolean speciale=false
        boolean rinnovi=false
        if (request.post) {
            def flussiPAI=[], errorePratica=[]
            def file =params.excelPratica
            def filename=""
            if(file.bytes) {
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                def risposta =[]
                Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$)/
                Pattern pattern = ~/(\w+)/
                String regex = "<(\\S+)[^>]+?mso-[^>]*>.*?</\\1>"
                def polizza
                try {
                    def wb = ExcelReader.readXlsx(myInputStream) {
                        sheet (0) {
                            rows(from: 1) {
                                def cellaNoPolizza = cell("F")?.value?:""
                                def dataDecorrenza=""
                                if((cellaNoPolizza.toString().equals("null") || cellaNoPolizza.toString()=='')){
                                    logg =new Log(parametri: "il numero di polizza non e' stato inserito nella cella", operazione: "caricamento polizze PAI omaggio tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    errorePratica.add("il numero di polizza non e' stato inserito nella cella,")
                                }else{
                                    //cellaNoPolizza=cellaNoPolizza.toString().replaceFirst ("^0*", "")
                                    if(cellaNoPolizza.toString().contains(".")){
                                        def punto=cellaNoPolizza.toString().indexOf(".")
                                        //cellaNoPolizza=Double.valueOf(cellaNoPolizza).longValue().toString()
                                        cellaNoPolizza=cellaNoPolizza.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                    }
                                    logg =new Log(parametri: "numero di polizza inseritop ${cellaNoPolizza}", operazione: "caricamento polizze PAI omaggio tramite csv", pagina: "POLIZZE FINANZIATE")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    polizza=Polizza.findByNoPolizza(cellaNoPolizza)
                                    if(!polizza){
                                        def resultwebS = IAssicurWebService.getPolizzeRinnoviperAnnulla(cellaNoPolizza)
                                        polizze=resultwebS.polizze
                                        if(polizze.size()>0){
                                            def celladatadecorrenza = cell("H")?.value?:""
                                            if((celladatadecorrenza.toString().equals("null") || celladatadecorrenza.toString()=='')) {
                                                logg = new Log(parametri: "la data di decorrenza non e' stata inserita", operazione: "caricamento polizze PAI omaggio tramite csv", pagina: "POLIZZE FINANZIATE")
                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                errorePratica.add("la data di decorrenza non e' stata inserita,")
                                            }else{
                                                if(celladatadecorrenza.getClass()==java.util.Date){
                                                    dataDecorrenza=celladatadecorrenza.format('dd/MM/yyyy')
                                                }
                                                def erroreiAssicur=resultwebS.iAssicurError
                                                def errore=resultwebS.error
                                                if(errore==null && erroreiAssicur==null){
                                                    rinnovi=true
                                                    def polizzaRIN = [:]
                                                    def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
                                                    String patternd = "#,##0.0#"
                                                    symbols.setGroupingSeparator((char) ',')
                                                    symbols.setDecimalSeparator((char) '.')
                                                    def  decimalFormat = new DecimalFormat(patternd, symbols)
                                                    decimalFormat.setParseBigDecimal(true)
                                                    polizze.each { polizzaWS ->
                                                        def noPolizza=cellaNoPolizza
                                                        polizzaRIN = [:]
                                                        polizzaRIN.codOperazione="0"
                                                        polizzaRIN.noPolizza=noPolizza
                                                        polizzaRIN.targa=polizzaWS.targa
                                                        polizzaRIN.telaio=polizzaWS.telaio
                                                        polizzaRIN.provincia=polizzaWS.provincia
                                                        polizzaRIN.cap=polizzaWS.cap
                                                        polizzaRIN.indirizzo=polizzaWS.indirizzo
                                                        polizzaRIN.localita=polizzaWS.localita
                                                        polizzaRIN.durata=polizzaWS.durata
                                                        polizzaRIN.onStar="N"
                                                        polizzaRIN.codPacchetto=polizzaWS.codPacchetto
                                                        polizzaRIN.dataDecorrenza=polizzaWS.dataDecorrenza
                                                        polizzaRIN.dataScadenza=polizzaWS.dataScadenza
                                                        polizzaRIN.modello=polizzaWS.modello
                                                        polizzaRIN.marca=polizzaWS.marca
                                                        polizzaRIN.cFiscale=polizzaWS.cFiscale
                                                        polizzaRIN.cliente=polizzaWS.cliente
                                                        polizzaRIN.email=polizzaWS.email
                                                        def coperturaR=polizzaWS.copertura
                                                        polizzaRIN.coperturaRichiesta=coperturaR
                                                        polizzaRIN.valoreAssicurato=polizzaWS.valoreAssicurato
                                                        polizzaRIN.dataDecorrenza= use(TimeCategory) {Date.parse("dd/MM/yyyy", dataDecorrenza)}
                                                        polizzaRIN.dataScadenza= use(TimeCategory) {Date.parse("dd/MM/yyyy", dataDecorrenza) +3.months}
                                                    }
                                                    logg =new Log(parametri: "contenuto array result IAssicur per flussi PAI Omaggio ${polizzaRIN} ", operazione: "caricamento polizze PAI omaggio tramite csv", pagina: "POLIZZE FINANZIATE")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    def tracciatoPAIPolizzaR = tracciatiService.generaTracciatoPAIOmaggioRinn(polizzaRIN)
                                                    if (!tracciatoPAIPolizzaR.save(flush: true)) {
                                                        logg = new Log(parametri: "Errore creazione tracciato PAI Omaggio: ${tracciatoPAIPolizzaR.errors}", operazione: "caricamento polizze PAI omaggio tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        risposta.add(noPratica: cellaNoPolizza, errore: "Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors}")
                                                        errorePratica.add(" pratica-->${cellaNoPolizza} Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors},")
                                                        totaleErr++
                                                    }else{
                                                        logg = new Log(parametri: "flusso pai generato correttamente per la polizza ${cellaNoPolizza}", operazione: "caricamento polizze PAI omaggio tramite csv", pagina: "POLIZZE FINANZIATE")
                                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        flussiPAI.add("tracciato generato per polizza ${cellaNoPolizza},")
                                                        totale++
                                                    }
                                                }else if (erroreiAssicur){
                                                    logg =new Log(parametri: "errore chiamata IAssicur  ${erroreiAssicur} ", operazione: "caricamento polizze PAI omaggio tramite csv", pagina: "POLIZZE FINANZIATE")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                }else if (errore){
                                                    logg =new Log(parametri: "errore chiamata  ${errore} ", operazione: "caricamento polizze PAI omaggio tramite csv", pagina: "POLIZZE FINANZIATE")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                }
                                            }

                                        }else{
                                            logg =new Log(parametri: "la polizza-->${cellaNoPolizza} non è stata caricata nel portale", operazione: "caricamento polizze PAI omaggio tramite csv", pagina: "POLIZZE FINANZIATE")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            errorePratica.add("la polizza-->${cellaNoPolizza} non \u00E8 stata caricata nel portale,")
                                        }
                                    }else{
                                        if(polizza.tipoPolizza==TipoPolizza.CASH){
                                            def celladatadecorrenza = cell("H")?.value?:""
                                            if((celladatadecorrenza.toString().equals("null") || celladatadecorrenza.toString()=='')) {
                                                logg = new Log(parametri: "la data di decorrenza non e' stata inserita", operazione: "caricamento polizze PAI omaggio tramite csv", pagina: "POLIZZE FINANZIATE")
                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                errorePratica.add("la data di decorrenza non e' stata inserita,")
                                            }else{
                                                if(celladatadecorrenza.getClass()==java.util.Date){
                                                    dataDecorrenza=celladatadecorrenza.format('dd/MM/yyyy')
                                                }
                                                def tracciatoPAIPolizzaR = tracciatiService.generaTracciatoPAIOMaggio(polizza, dataDecorrenza)
                                                if (!tracciatoPAIPolizzaR.save(flush: true)) {
                                                    logg = new Log(parametri: "Errore creazione tracciato PAI Omaggio: ${tracciatoPAIPolizzaR.errors}", operazione: "caricamento polizze PAI omaggio tramite csv", pagina: "POLIZZE FINANZIATE")
                                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    risposta.add(noPratica: cellaNoPolizza, errore: "Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors}")
                                                    errorePratica.add(" pratica-->${cellaNoPolizza} Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors},")
                                                    totaleErr++
                                                }else{
                                                    flussiPAI.add("tracciato generato per polizza ${cellaNoPolizza},")
                                                    totale++
                                                }
                                            }

                                        }else{
                                            dataDecorrenza=polizza.dataDecorrenza.format('dd/MM/yyyy')
                                            def tracciatoPAIPolizzaR = tracciatiService.generaTracciatoPAIOMaggio(polizza, dataDecorrenza)
                                            if (!tracciatoPAIPolizzaR.save(flush: true)) {
                                                logg = new Log(parametri: "Errore creazione tracciato PAI Omaggio: ${tracciatoPAIPolizzaR.errors}", operazione: "caricamento polizze PAI omaggio tramite csv", pagina: "POLIZZE FINANZIATE")
                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                risposta.add(noPratica: cellaNoPolizza, errore: "Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors}")
                                                errorePratica.add(" pratica-->${cellaNoPolizza} Errore creazione tracciato PAI: ${tracciatoPAIPolizzaR.errors},")
                                                totaleErr++
                                            }else{
                                                flussiPAI.add("flusso pai generato correttamente per la polizza ${cellaNoPolizza},")
                                                totale++
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }catch (e){
                    println "c'\u00E8 un problema con il file, ${e.toString()}"
                    risposta.add("c'\u00E8 un problema con il file, ${e.toString()}")
                }
                if(errorePratica.size()>0){
                    for (String s : errorePratica)
                    {
                        listErrori += s + "\r\n\r\n"
                    }
                }
                if(flussiPAI.size()>0){
                    for (String s : flussiPAI)
                    {
                        listFlussiPAI += s + "\r\n\r\n"
                    }
                }
                if(flussiPAI.size()>0){
                    flash.flussiPAI=listFlussiPAI+="totale flussi generati ${totale}"
                }
                if(errorePratica.size()>0){
                    flash.errorePratica=listErrori+="totale flussi non generati ${totaleErr}"
                }
            } else flash.error = "Specificare l'elenco xlsx"
            redirect action: "listFinanziate"
        }else response.sendError(404)
    }
    def generaCertFinLeasing(){
        def logg
        if (request.post) {
            if(params.dataGeneraCert !=""){
                def dataGeneraCert = Date.parse("dd-MM-yyyy", params.dataGeneraCert).format("dd.MM.yyyy")
                    try {
                        GenCertFinLeasJob.triggerNow([dataGeneraCert:dataGeneraCert])
                        flash.error = "generazione dei certificati iniziato, a breve arrivera' una mail con il riassunto"

                    } catch (e) {
                        logg = new Log(parametri: "errore try catch ${e.toString()}", operazione: "generazione certificati Fin/Leasing", pagina: "Polizza Fin/Leasing")
                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        println e.toString()
                    }
            }else{
                logg =new Log(parametri: "Selezionare una data per lanciare la generazione dei certificati", operazione: "generazione certificati Fin/Leasing generati", pagina: "Polizza Fin/Leasing")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                flash.certificati=["Selezionare una data per lanciare la generazione dei certificati"]
            }
            redirect action: "listFinanziate"
        }else response.sendError(404)

    }
    def invioCertMail(){
        def logg
        boolean cash=true
        try {
            if(params.isCash=="false"){
                cash=false
            }else{

            }
                InvioCertCashMailJob.triggerNow([cash:cash])
                //generaCertService.inviaMailDefinitivo(cash)
                flash.error = "invio dei certificati iniziato, a breve arrivera' una mail con il riassunto"

        } catch(e) { e.printStackTrace() }
        if(cash){
            redirect action: "lista"
        }else{
            redirect action: "listFinanziate"
        }


    }
    private def getRinnDb() {
        //println dataSource_rinnovi
        return new Sql(dataSource_rinnovi)
    }
    def generaFileGMFGEN(){
        def logg
        if (request.post) {
            if(params.mese !="" && params.anno !=""){
                try {
                    //println "questi son i parametri ${params}"
                    def mese=params.mese
                    def anno=params.anno
                    GenGMFGENJob.triggerNow([mese:mese,anno:anno])
                    flash.error = "generazione del report GMFGEN, a breve arrivera' una mail con il file"

                }catch(e) {
                    logg =new Log(parametri: "errore try catch ${e.toString()}", operazione: "generazione GMFGEN", pagina: "Polizza CASH")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println e.toString() }
            }else{
                logg =new Log(parametri: "Selezionare un mese per lanciare la generazione del report", operazione: "generazione GMFGEN", pagina: "Polizza CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                flash.certificati=["Selezionare un mese per lanciare la generazione del report"]
            }

            redirect action: "lista"
        }else response.sendError(404)


    }
    def recedePolizza(){
        def logg
        def tipoPoli=""
        boolean risposta=false
        boolean isRinnovo=false
        def strresponse=""
        def errore=[]
        def operazionePagina=""
        if(params.tipoPolizza){
            tipoPoli=params.tipoPolizza
        }
        if(tipoPoli=="RINN"){
            isRinnovo=true
            def codOperazione=params.codOperazione
            def noPolizza=params.noPolizza.toString().trim()
            def flussoIassicur, flussoDL
            def polizza = [:]
            if(noPolizza) {

                def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
                String pattern = "#,##0.0#"
                symbols.setGroupingSeparator((char) ',')
                symbols.setDecimalSeparator((char) '.')
                def  decimalFormat = new DecimalFormat(pattern, symbols)
                decimalFormat.setParseBigDecimal(true)
                if(codOperazione=="S"){
                    operazionePagina="recesso rinnovi"
                    polizza.codOperazione="S"
                    polizza.noPolizza=params.noPolizza.toString().trim()
                    polizza.targa=params.targa.toString().trim()
                    polizza.telaio=params.telaio.toString().trim()
                    polizza.provincia=params.provincia.toString().trim()
                    polizza.cap=params.cap.toString().trim()
                    polizza.indirizzo=params.indirizzo
                    polizza.localita=params.localita
                    polizza.durata=params.durata.toString().trim()
                    polizza.onStar=params.onStar
                    polizza.codPacchetto=params.codPacchetto
                    polizza.dataDecorrenza=params.dataDecorrenza
                    polizza.dataScadenza=params.dataScadenza
                    polizza.modello=params.modello.toString().trim()
                    polizza.marca=params.marca
                    polizza.cFiscale=params.cFiscale
                    polizza.cliente=params.cliente
                    polizza.dataStorno=params.dataStorno
                    polizza.valoreAssicurato=params.valoreAssicurato
                    polizza.ramo=params.ramo
                    def coperturaR=params.coperturaRichiesta
                    def polizzaCFL=""
                    def polizzaWS=""
                    def rispostawe=IAssicurWebService.chiamataWSRecessoAnnulla(polizzaCFL,polizzaWS,coperturaR,isRinnovo,polizza,codOperazione)
                    polizza.coperturaRichiesta=coperturaR
                    polizza.premioImponibile=Math.abs(rispostawe.premioImpo)
                    polizza.provvDealer=Math.abs(rispostawe.provvDealer)
                    polizza.provvGmfi=Math.abs(rispostawe.provvGMF)
                    polizza.provvMansutti=Math.abs(rispostawe.provvMansutti)
                    polizza.provvMach1=Math.abs(rispostawe.provvMach1)
                    polizza.provvVenditore=Math.abs(rispostawe.provvVendi)
                    polizza.rappel=Math.abs(rispostawe.rappel)
                    polizza.imposte=Math.abs(rispostawe.imposte)
                    polizza.premioLordo=Math.abs(rispostawe.premioLordo)
                    polizza.valoreAssicurato=(BigDecimal) decimalFormat.parse(params.valoreAssicurato)
                    polizza.dataAnnullamento= use(TimeCategory) {Date.parse("yyyy-MM-dd", params.dataStorno)}
                    polizza.dataDecorrenza= use(TimeCategory) {Date.parse("yyyy-MM-dd", params.dataDecorrenza)}
                    polizza.dataScadenza= use(TimeCategory) {Date.parse("yyyy-MM-dd", params.dataScadenza)}
                    flussoIassicur = tracciatiService.generaTracciatoIAssicurRinn(polizza)
                    if (!flussoIassicur.save(flush: true)) {
                        risposta=false
                        logg =new Log(parametri: "Si è verificato un errore durante la generazione del flusso iAssicur", operazione: "${operazionePagina}", pagina: "RECESSO RINNOVI")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flash.error = "Si è verificato un errore durante la generazione del flusso iAssicur"
                        log.error renderErrors(bean: flussoIassicur)
                    }else{
                        logg =new Log(parametri: "flusso IAssicur generato", operazione: "${operazionePagina}", pagina: "RECESSO RINNOVI")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flussoDL = tracciatiService.generaTracciatoCompagniaRinn(polizza)
                        if (!flussoDL.save(flush: true)) {
                            logg =new Log(parametri: "Si è verificato un errore durante la generazione del flusso Direct Line ${renderErrors(bean: flussoDL)}", operazione: "${operazionePagina}", pagina: "RECESSO RINNOVI")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            flash.error = "Si è verificato un errore durante la generazione del flusso Direct Line "
                            log.error renderErrors(bean: flussoDL)
                        }else{
                            logg =new Log(parametri: "flusso DL generato", operazione: "${operazionePagina}", pagina: "RECESSO RINNOVI")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            risposta=true
                            flash.message ="I flussi IAssicur e DL sono stati generati per la polizza ${noPolizza}"
                        }
                    }
                }else if(codOperazione=="E"){
                    operazionePagina="annullamento rinnovi"
                    polizza.annullata=true
                    polizza.codOperazione="E"
                    def premioImponibile=(BigDecimal) decimalFormat.parse(params.premioImponibile)
                    def premioLordo=(BigDecimal) decimalFormat.parse(params.premioLordo)
                    def provvDealer=(BigDecimal) decimalFormat.parse(params.provvDealer)
                    def provvGmfi=(BigDecimal) decimalFormat.parse(params.provvGmfi)
                    def provvMansutti=(BigDecimal) decimalFormat.parse(params.provvMansutti)
                    def provvMach1=(BigDecimal) decimalFormat.parse(params.provvMach1)
                    def provvVenditore=(BigDecimal) decimalFormat.parse(params.provvVenditore)
                    def rappel=(BigDecimal) decimalFormat.parse(params.rappel)
                    def imposte=(BigDecimal) decimalFormat.parse(params.imposte)
                    def valoreAssicurato=(BigDecimal) decimalFormat.parse(params.valoreAssicurato)
                    polizza.cliente=params.cliente
                    polizza.cFiscale=params.cFiscale
                    polizza.valoreAssicurato=valoreAssicurato
                    polizza.provincia=params.provincia
                    polizza.indirizzo=params.indirizzo
                    polizza.cap=params.cap
                    polizza.localita=params.localita
                    polizza.modello=params.modello
                    polizza.targa=params.targa
                    polizza.marca=params.marca
                    polizza.telaio=params.telaio
                    polizza.noPolizza=params.noPolizza
                    polizza.durata=params.durata
                    polizza.codPacchetto=params.codPacchetto
                    polizza.coperturaRichiesta=params.coperturaRichiesta
                    polizza.premioImponibile=Math.abs(premioImponibile)
                    polizza.provvDealer=Math.abs(provvDealer)
                    polizza.provvGmfi=Math.abs(provvGmfi)
                    polizza.provvMansutti=Math.abs(provvMansutti)
                    polizza.provvMach1=Math.abs(provvMach1)
                    polizza.provvVenditore=Math.abs(provvVenditore)
                    polizza.premioLordo=Math.abs(premioLordo)
                    polizza.rappel=Math.abs(rappel)
                    polizza.imposte=Math.abs(imposte)
                    polizza.ramo=params.ramo
                    polizza.dataAnnullamento= use(TimeCategory) {Date.parse("yyyy-MM-dd", params.dataStorno)}
                    polizza.dataDecorrenza= use(TimeCategory) {Date.parse("yyyy-MM-dd", params.dataDecorrenza)}
                    polizza.dataScadenza= use(TimeCategory) {Date.parse("yyyy-MM-dd", params.dataScadenza)}
                    flussoIassicur = tracciatiService.generaTracciatoIAssicurRinn(polizza)
                    if (!flussoIassicur.save(flush: true)) {
                        risposta=false
                        logg =new Log(parametri: "Si è verificato un errore durante la generazione del flusso iAssicur", operazione: "${operazionePagina}", pagina: "ANNULLAMENTI RINNOVI")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flash.error = "Si è verificato un errore durante la generazione del flusso iAssicur"
                        log.error renderErrors(bean: flussoIassicur)
                    }else{
                        logg =new Log(parametri: "flusso IAssicur generato", operazione: "${operazionePagina}", pagina: "ANNULLAMENTI RINNOVI")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flussoDL = tracciatiService.generaTracciatoCompagniaRinn(polizza)
                        if (!flussoDL.save(flush: true)) {
                            logg =new Log(parametri: "Si è verificato un errore durante la generazione del flusso Direct Line ${renderErrors(bean: flussoDL)}", operazione: "${operazionePagina}", pagina: "ANNULLAMENTI RINNOVI")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            flash.error = "Si è verificato un errore durante la generazione del flusso Direct Line "
                            log.error renderErrors(bean: flussoDL)
                        }else{
                            logg =new Log(parametri: "flussi generati per il rinnovo", operazione: "${operazionePagina}", pagina: "ANNULLAMENTI RINNOVI")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            risposta=true
                            flash.message ="I flussi IAssicur e DL sono stati generati per la polizza ${noPolizza}"
                        }
                    }
                }

            }
        }else{
            def id=Long.parseLong(params.idPolizza.toString().trim())
            def codOperazione=params.codOperazione
            if(Polizza.exists(id)) {
                def flussoIassicur, flussoDL
                def polizza = Polizza.get(id)
                if(codOperazione=="S"){
                    operazionePagina="recesso polizze CASH/FIN/LEASING"
                    logg =new Log(parametri: "parametri passati ${params}", operazione: "recedePolizza", pagina: "RECESSO")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    polizza.codOperazione="S"
                   // polizza.annullata=true
                    if(polizza.tracciatoId!=null){
                        def tracciatoId= polizza.tracciatoId
                        def tracciatoPolizza=Tracciato.get(tracciatoId)
                        if(tracciatoPolizza.dataCaricamento==null){tracciatoPolizza.dataCaricamento=new Date()}
                    }
                    if (!polizza.save(flush: true)) {
                        risposta=false
                        flash.error = "Si è verificato un errore durante l'annullamento della polizza"
                        log.error renderErrors(bean: polizza)
                        logg =new Log(parametri: "Si è verificato un errore durante l'annullamento della polizza ${renderErrors(bean: polizza)} ", operazione: "${operazionePagina}", pagina: "LISTA POLIZZE CASH")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }else{
                        flussoIassicur = tracciatiService.generaTracciatoIAssicur(polizza)
                        if (!flussoIassicur.save(flush: true)) {
                            risposta=false
                            flash.error = "Si è verificato un errore durante la generazione del flusso iAssicur"
                            log.error renderErrors(bean: flussoIassicur)
                            logg =new Log(parametri: "Si è verificato un errore durante la generazione del flusso iAssicur ${renderErrors(bean: flussoIassicur)} ", operazione: "${operazionePagina}", pagina: "LISTA POLIZZE CASH")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }else{
                            flussoDL = tracciatiService.generaTracciatoCompagnia(polizza)
                            if (!flussoDL.save(flush: true)) {
                                logg =new Log(parametri: "Si è verificato un errore durante la generazione del flusso Direct Line ${renderErrors(bean: flussoDL)} ", operazione: "${operazionePagina}", pagina: "LISTA POLIZZE CASH")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                flash.error = "Si è verificato un errore durante la generazione del flusso Direct Line "
                                log.error renderErrors(bean: flussoDL)
                                //redirect action: "lista"
                            }else{
                                polizza.targa=polizza.targa? "${polizza.targa}_R":null
                                polizza.telaio=polizza.telaio? "${polizza.telaio}_R":null
                                polizza.stato=StatoPolizza.RECEDUTA

                                if (!polizza.save(flush: true)) {
                                    logg =new Log(parametri: "Si è verificato un errore durante l'aggiornamento della targa o telaio della polizza ${renderErrors(bean: polizza)} ", operazione: "${operazionePagina}", pagina: "LISTA POLIZZE CASH")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    flash.error = "Si è verificato un errore durante l'aggiornamento della targa o telaio della  polizza "
                                    log.error renderErrors(bean: polizza)
                                }else{
                                    logg =new Log(parametri: "aggiornamento telaio:${polizza.telaio}/targa:${polizza.targa} della polizza ${polizza.noPolizza}", operazione: "${operazionePagina}", pagina: "RECESSO")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    risposta=true
                                    flash.message ="Polizza receduta"
                                }
                                //redirect action: "lista"
                            }
                        }
                    }
                }else{
                    operazionePagina="annullamento polizze CASH/FIN/LEASING"
                    logg =new Log(parametri: "parametri passati ${params}", operazione: "${operazionePagina}", pagina: "Annullamenti Polizze")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    polizza.annullata=true
                    polizza.codOperazione="E"
                    def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
                    String pattern = "#,##0.0#"
                    symbols.setGroupingSeparator((char) ',')
                    symbols.setDecimalSeparator((char) '.')
                    def  decimalFormat = new DecimalFormat(pattern, symbols)
                    decimalFormat.setParseBigDecimal(true)
                    def premioImponibile=(BigDecimal) decimalFormat.parse(params.premioImponibile)
                    def premioLordo=(BigDecimal) decimalFormat.parse(params.premioLordo)
                    def provvDealer=(BigDecimal) decimalFormat.parse(params.provvDealer)
                    def provvGmfi=(BigDecimal) decimalFormat.parse(params.provvGmfi)
                    def provvVenditore=(BigDecimal) decimalFormat.parse(params.provvVenditore)
                    def rappel=(BigDecimal) decimalFormat.parse(params.rappel)
                    def imposte=(BigDecimal) decimalFormat.parse(params.imposte)
                    polizza.premioImponibile=Math.abs(premioImponibile)
                    polizza.provvDealer=Math.abs(provvDealer)
                    polizza.provvGmfi=Math.abs(provvGmfi)
                    polizza.provvVenditore=Math.abs(provvVenditore)
                    polizza.premioLordo=Math.abs(premioLordo)
                    polizza.rappel=Math.abs(rappel)
                    polizza.imposte=Math.abs(imposte)
                    polizza.dataAnnullamento= use(TimeCategory) {Date.parse("yyyy-MM-dd", params.dataStorno)}
                    if(!polizza.save(flush: true)) {
                        risposta=false
                        flash.error = "Si è verificato un errore durante l'annullamento della polizza"
                        log.error renderErrors(bean: polizza)
                        logg =new Log(parametri: "Si è verificato un errore durante l'annullamento della polizza ${renderErrors(bean: polizza)} ", operazione: "${operazionePagina}", pagina: "ANNULLAMENTI")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    }else{
                        logg =new Log(parametri: "Polizza aggiornata ${polizza.noPolizza}", operazione: "${operazionePagina}", pagina: "ANNULLAMENTI")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flussoIassicur = tracciatiService.generaTracciatoIAssicur(polizza)
                        if (!flussoIassicur.save(flush: true)) {
                            risposta=false
                            logg =new Log(parametri: "Si è verificato un errore durante la generazione del flusso iAssicur ${renderErrors(bean: flussoIassicur)} ", operazione: "${operazionePagina}", pagina: "ANNULLAMENTI")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            flash.error = "Si è verificato un errore durante la generazione del flusso iAssicur"
                            log.error renderErrors(bean: flussoIassicur)
                            //redirect action: "lista"
                        }else{
                            logg =new Log(parametri: " generazione del flusso iAssicur", operazione: "${operazionePagina}", pagina: "ANNULLAMENTI")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            flussoDL = tracciatiService.generaTracciatoCompagnia(polizza)
                            if (!flussoDL.save(flush: true)) {
                                logg =new Log(parametri: "Si è verificato un errore durante la generazione del flusso Direct Line ${renderErrors(bean: flussoDL)}", operazione: "${operazionePagina}", pagina: "ANNULLAMENTI")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                flash.error = "Si è verificato un errore durante la generazione del flusso Direct Line "
                                log.error renderErrors(bean: flussoDL)
                                //redirect action: "lista"
                            }else{
                                logg =new Log(parametri: "generazione del flusso Direct Line", operazione: "${operazionePagina}", pagina: "ANNULLAMENTI")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                polizza.targa=polizza.targa? "${polizza.targa}_A":null
                                polizza.telaio=polizza.telaio? "${polizza.telaio}_A":null
                                polizza.stato=StatoPolizza.ANNULLATA
                                if (!polizza.save(flush: true)) {
                                    logg =new Log(parametri: "Si è verificato un errore durante l'aggiornamento della polizza ${renderErrors(bean: polizza)} ", operazione: "${operazionePagina}", pagina: "ANNULLAMENTI")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    flash.error = "Si è verificato un errore durante l'aggiornamento della polizza "
                                    log.error renderErrors(bean: polizza)
                                }else{
                                    logg =new Log(parametri: "aggiornamento telaio:${polizza.telaio}/targa:${polizza.targa} della polizza ${polizza.noPolizza}", operazione: "${operazionePagina}", pagina: "ANNULLAMENTI")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    risposta=true
                                    flash.message ="Polizza annullata"
                                }
                                //redirect action: "lista"
                            }
                        }
                    }
                }

            }
        }
        if(risposta) strresponse=[risposta:risposta]
        else strresponse=[risposta:risposta,errore:errore]
        errore=[]
        render strresponse as JSON
    }
    def recederePolizze(){
        def polizze
        def principal = SecurityUtils.principal
        def utente,admin
        boolean rinnovi=false
        if(request.post){
            def db = rinnDb
            if(SecurityUtils.hasRole("ADMIN")){
                admin=Admin.findById(principal.id)
                utente=admin
            }
            def polizza = [:]
            def cerca = params.searchNo.toString().trim() ? params.searchNo.toString().trim() : ""
            polizze = Polizza.createCriteria().list() {
                if(cerca) {
                    //ne ("codOperazione","E")
                    //eq ("annullata", false)
                    or {
                        ilike "noPolizza","%${cerca}%"
                    }
                }
                and {
                    order('noPolizza', 'desc')
                    order('dataDecorrenza', 'desc')

                }
            }
            if(!polizze){
                if(cerca!=''){
                    def resultwebS = IAssicurWebService.getPolizzeRinnoviperAnnulla(cerca)
                    polizze=resultwebS.polizze
                    //println "polizza trovata $polizze"
                    def erroreiAssicur=resultwebS.iAssicurError
                    def errore=resultwebS.error
                    if(errore==null && erroreiAssicur==null){
                        rinnovi=true
                    }
                }
            }
        }
        [polizze: polizze, utente:utente, rinnovi:rinnovi]
    }
    def caricaPolizzaPAIPagamentoOLD() {
        def logg
        def listFlussiPAI=""
        def listErrori=""
        def totaleErr=0
        def totale=0
        def erroPolizza=0
        if (request.post) {
            def flussiPAI=[], errorePratica=[]
            def file =params.excelPratica
            def filename=""
            if(file.bytes) {
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                def risposta =[]
                Pattern exprRegTarga = ~/(^[a-zA-Z]{2}[0-9]{3}[a-zA-Z]{2}$)/
                Pattern pattern = ~/(\w+)/
                String regex = "<(\\S+)[^>]+?mso-[^>]*>.*?</\\1>";
                def polizza
                try {
                    def wb = ExcelReader.readXlsx(myInputStream) {
                        sheet (0) {
                            def j=1
                            rows(from: 3) {
                                boolean sup75=false
                                def tipoClienteDef=TipoCliente.M
                                def cellnoPratica = cell("C")?.value?:""
                                def dataDecorrenza=""
                                if((cellnoPratica.toString().equals("null") || cellnoPratica.toString()=='')){
                                    logg =new Log(parametri: "il numero di polizza non e' stato inserito nella cella-- riga ${j}", operazione: "caricamento polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    errorePratica.add("il numero di polizza non e' stato inserito nella cella -- riga ${j} del file excel,")
                                }else{
                                    //cellaNoPolizza=cellaNoPolizza.toString().replaceFirst ("^0*", "")
                                    if(cellnoPratica.toString().contains(".")){
                                        def punto=cellnoPratica.toString().indexOf(".")
                                        //cellaNoPolizza=Double.valueOf(cellaNoPolizza).longValue().toString()
                                        cellnoPratica=cellnoPratica.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                    }
                                    logg =new Log(parametri: "numero di polizza inserito ${cellnoPratica}", operazione: "caricamento polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                                    polizza=PolizzaPaiPag.findByNoPolizza(cellnoPratica)
                                    if(polizza){
                                        logg = new Log(parametri: "la polizza con numero ${cellnoPratica} e' gia' stata caricata", operazione: "caricamento polizze PAI pagamento tramite xlsx", pagina: "POLIZZE FINANZIATE")
                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        errorePratica.add("la polizza con numero ${cellnoPratica}  e' gia' stata caricata,")
                                    }else{
                                        boolean denominazione=false
                                        boolean coobligato=false
                                        boolean cliente=false
                                        boolean nonTrovato=false
                                        def dealer
                                        def cognomeDef,nomeDef,provinciaDef,localitaDef,capDef,telDef,cellDef,cfDef,indirizzoDef,emailDef
                                        def celladatadecorrenza = cell("D")?.value?:""
                                        def cellaDealer =cell("E")?.value?:""
                                        def cellaBeneficiario=cell("BG")?.value?:""
                                        def cellaDenominazione= cell("AE")?.value?:""
                                        def cellaCoobligatoCognome= cell("S")?.value?:""
                                        def cellaCoobligatoNome= cell("T")?.value?:""
                                        def cellaCognome= cell("G")?.value?:""
                                        def cellaNome= cell("H")?.value?:""
                                        if((celladatadecorrenza.toString().equals("null") || celladatadecorrenza.toString()=='')) {
                                            logg = new Log(parametri: "la data di decorrenza non e' stata inserita", operazione: "caricamento polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                            if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            errorePratica.add("la data di decorrenza non e' stata inserita,")
                                            dataDecorrenza=null
                                        }else{
                                            //println "entro qui ${celladatadecorrenza} \n"
                                            if(celladatadecorrenza instanceof Date){
                                                dataDecorrenza=celladatadecorrenza
                                            }else {dataDecorrenza=Date.parse("dd/MM/yyyy",celladatadecorrenza)}

                                        }
                                        if(cellaDealer.toString().equals("null") || cellaDealer.toString().equals("")){
                                            cellaDealer=""
                                        }else{
                                            cellaDealer=cellaDealer.toString().replaceAll ("'", "")
                                            cellaDealer=cellaDealer.toString().replaceFirst ("^0*", "")
                                            if(cellaDealer.toString().contains(".")){
                                                cellaDealer=Double.valueOf(cellaDealer).longValue().toString()
                                                //cellaDealer=cellaDealer.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                            }
                                            dealer=Dealer.findByCodice(cellaDealer.toString())
                                        }
                                        if(cellaBeneficiario.toString()!=''){
                                            cellaBeneficiario=cellaBeneficiario.toUpperCase().trim()
                                            //println "benficiario ${cellaBeneficiario}"
                                            logg =new Log(parametri: "beneficiario-->${cellaBeneficiario}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            if(cellaDenominazione) {
                                                cellaDenominazione=cellaDenominazione.toUpperCase().trim()
                                                //println "cellaDenominazione ${cellaDenominazione}"
                                                if (cellaDenominazione.contains(cellaBeneficiario)) {
                                                    logg =new Log(parametri: "denominazione-->${cellaDenominazione}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    denominazione = true

                                                } else {
                                                    denominazione = false
                                                }
                                            }
                                            if(cellaCoobligatoNome){
                                                cellaCoobligatoNome=cellaCoobligatoNome.toUpperCase().trim()
                                                cellaCoobligatoCognome=cellaCoobligatoCognome.toUpperCase().trim()
                                                //def nomeCooC= cellaCoobligatoCognome+" "+cellaCoobligatoNome
                                                def nomeCooC= cellaCoobligatoNome+" "+cellaCoobligatoCognome
                                                //println "nomeCooC ${nomeCooC}"
                                                if(nomeCooC.trim().contains(cellaBeneficiario)){
                                                    logg =new Log(parametri: "nome Coobligato-->${nomeCooC}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    coobligato=true
                                                }else{
                                                    coobligato = false
                                                }
                                            }
                                            if(cellaNome){
                                                cellaNome=cellaNome.toUpperCase().trim()
                                                cellaCognome=cellaCognome.toUpperCase().trim()
                                                //def nomeCom=cellaCognome+" "+cellaNome
                                                def nomeCom=cellaNome+" "+cellaCognome
                                                //println "nomeCom ${nomeCom}"
                                                if(nomeCom.trim().contains(cellaBeneficiario)){
                                                    logg =new Log(parametri: "nome Cliente-->${nomeCom}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    cliente=true
                                                }else{
                                                    cliente = false
                                                }
                                            }
                                            if(coobligato){
                                                logg =new Log(parametri: "prendo i dati del Coobligato-->${cellaCoobligatoNome}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                def cellaCoobligatoPROV=cell("Z")?.value?:""
                                                if(cellaCoobligatoPROV.toString().contains("\'")) cellaCoobligatoPROV = cellaCoobligatoPROV.replaceAll("\'","")
                                                def cellaCoobligatoLOC=cell("X")?.value?:""
                                                if(cellaCoobligatoLOC.toString().contains("\'")) cellaCoobligatoLOC = cellaCoobligatoLOC.replaceAll("\'","")
                                                def cellaCoobligatoCAP=cell("AA")?.value?:""
                                                if(cellaCoobligatoCAP.toString().contains("\'")) cellaCoobligatoCAP = cellaCoobligatoCAP.replaceAll("\'","")
                                                def cellaCoobligatoTEL=cell("AB")?.value?:""
                                                if(cellaCoobligatoTEL.toString().contains("\'")) cellaCoobligatoTEL = cellaCoobligatoTEL.replaceAll("\'","")
                                                def cellaCoobligatoCEL=cell("AC")?.value?:""
                                                if(cellaCoobligatoCEL.toString().contains("\'")) cellaCoobligatoCEL = cellaCoobligatoCEL.replaceAll("\'","")
                                                def cellaCoobligatoCF=cell("V")?.value?:""
                                                if(cellaCoobligatoCF.toString().contains("\'")){
                                                    cellaCoobligatoCF = cellaCoobligatoCF.replaceAll("\'","")
                                                }
                                                def cellaCoobligatoIND=cell("Y")?.value?:""
                                                if(cellaCoobligatoIND.toString().contains("\'")) cellaCoobligatoIND = cellaCoobligatoIND.replaceAll("\'","")
                                                def cellaCoobligatoemail=cell("AD")?.value?:""
                                                if(cellaCoobligatoIND.toString().contains("\'")) cellaCoobligatoIND = cellaCoobligatoIND.replaceAll("\'","")
                                                cognomeDef=cellaCoobligatoCognome.toString().trim().toUpperCase()
                                                nomeDef=cellaCoobligatoNome.toString().trim().toUpperCase()
                                                provinciaDef=cellaCoobligatoPROV.toString().trim().toUpperCase()
                                                localitaDef=cellaCoobligatoLOC.toString().trim().toUpperCase()
                                                capDef=cellaCoobligatoCAP.toString().trim().toUpperCase()
                                                telDef=cellaCoobligatoTEL.toString().trim().toUpperCase()
                                                cellDef=cellaCoobligatoCEL.toString().trim().toUpperCase()
                                                if(cellaCoobligatoCF !='null'){
                                                    cfDef=cellaCoobligatoCF.toString().trim().toUpperCase()
                                                    if(cfDef){
                                                        def annoCF=cfDef.substring(6,8)
                                                        if(Integer.parseInt(annoCF) && Integer.parseInt(annoCF)<=40){
                                                            sup75=true
                                                        }
                                                        def sessC=cfDef.substring(9,11)
                                                        if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                                            tipoClienteDef=TipoCliente.F
                                                        }
                                                    }
                                                    /*if(!cfDef.toString().matches("^[a-z|A-Z]{6}[0-9]{2}[A-Z|a-z][0-9]{2}[A-Z|a-z][0-9]{3}[A-Z|a-z]\$") && !cfDef.matches("^0+\\d*(?:\\.\\d+)?\$")){
                                                        cfDef=Double.valueOf(cfDef).longValue().toString()
                                                    }
                                                    if(!cfDef.matches("[0-9]{11}")){
                                                    cfDef=cfDef.padLeft(11,"0")
                                                    }
                                                    */
                                                }
                                                if(cellDef.toString().contains(".")){
                                                    def punto=cellDef.toString().indexOf(".")
                                                    cellDef=cellDef.toString().replaceAll("[^0-9]", "")
                                                }
                                                if(telDef.toString().contains(".")){
                                                    def punto=telDef.toString().indexOf(".")
                                                    telDef=telDef.toString().replaceAll("[^0-9]", "")
                                                }
                                                indirizzoDef=cellaCoobligatoIND.toString().trim().toUpperCase()

                                                if(cellaCoobligatoemail.toString().trim().size()>4){
                                                    emailDef=cellaCoobligatoemail.toString().trim()
                                                }

                                            }else if(denominazione){
                                                logg =new Log(parametri: "prendo i dati della denominazione -->${cellaDenominazione}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                                                def cellaProvinciaBusiness =cell("AK")?.value?:""
                                                if(cellaProvinciaBusiness.toString().contains("\'")) cellaProvinciaBusiness = cellaProvinciaBusiness.replaceAll("\'","")
                                                def cellaLocalitaBusiness =cell("X")?.value?:""
                                                if(cellaLocalitaBusiness.toString().contains("\'")) cellaLocalitaBusiness = cellaLocalitaBusiness.replaceAll("\'","")
                                                def cellaCapBusiness =cell("AL")?.value?:""
                                                if(cellaCapBusiness.toString().contains("\'")) cellaCapBusiness = cellaCapBusiness.replaceAll("\'","")
                                                def cellaTelefonoBusiness =cell("AM")?.value?:""
                                                if(cellaTelefonoBusiness.toString().contains("\'")) cellaTelefonoBusiness = cellaTelefonoBusiness.replaceAll("\'","")
                                                def cellaCellulareBusiness =cell("AN")?.value?:""
                                                if(cellaCellulareBusiness.toString().contains("\'")) cellaCellulareBusiness = cellaCellulareBusiness.replaceAll("\'","")
                                                def cellaPIvaBusiness =cell("AH")?.value?:""
                                                if(cellaPIvaBusiness.toString().contains("\'")) cellaPIvaBusiness = cellaPIvaBusiness.replaceAll("\'","")
                                                def cellaIndirizzoBusiness =cell("AJ")?.value?:""
                                                if(cellaIndirizzoBusiness.toString().contains("\'")) cellaIndirizzoBusiness = cellaIndirizzoBusiness.replaceAll("\'","")
                                                def cellaEmailBusiness =cell("AO")?.value?:""
                                                if(cellaEmailBusiness.toString().contains("\'")) cellaEmailBusiness = cellaEmailBusiness.replaceAll("\'","")
                                                cognomeDef=cellaDenominazione.toString().trim().toUpperCase()
                                                nomeDef=""

                                                provinciaDef=cellaProvinciaBusiness.toString().trim().toUpperCase()
                                                localitaDef=cellaLocalitaBusiness.toString().trim().toUpperCase()
                                                capDef=cellaCapBusiness.toString().trim()
                                                telDef=cellaTelefonoBusiness.toString().trim()
                                                cellDef=cellaCellulareBusiness.toString().trim()
                                                if(cellDef.toString().contains(".")){
                                                    def punto=cellDef.toString().indexOf(".")
                                                    //cellaNoPolizza=Double.valueOf(cellaNoPolizza).longValue().toString()
                                                    //cellDef=cellDef.toString().replaceAll("[^0-9]", "")
                                                }
                                                if(telDef.toString().contains(".")){
                                                    def punto=telDef.toString().indexOf(".")
                                                    //cellaNoPolizza=Double.valueOf(cellaNoPolizza).longValue().toString()
                                                   // telDef=telDef.toString().replaceAll("[^0-9]", "")
                                                }
                                                if(cellaPIvaBusiness !='null'){
                                                    cfDef=cellaPIvaBusiness.toString().trim().toUpperCase()
                                                    /*if(!cfDef.toString().matches("^[a-z|A-Z]{6}[0-9]{2}[A-Z|a-z][0-9]{2}[A-Z|a-z][0-9]{3}[A-Z|a-z]\$") && !cfDef.matches("^0+\\d*(?:\\.\\d+)?\$")){
                                                        cfDef=Double.valueOf(cfDef).longValue().toString()
                                                    }
                                                    if(!cfDef.matches("[0-9]{11}")){
                                                    cfDef=cfDef.padLeft(11,"0")
                                                    }
                                                    */
                                                }

                                                indirizzoDef=cellaIndirizzoBusiness.toString().trim().toUpperCase()
                                                tipoClienteDef=TipoCliente.DITTA_INDIVIDUALE
                                                if(cellaEmailBusiness.toString().trim().size()>4){
                                                    emailDef=cellaEmailBusiness.toString().trim()
                                                }
                                            }else if(cliente){
                                                logg =new Log(parametri: "prendo i dati del cliente -->${cellaNome}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                def cellaProvincia=cell("N")?.value?:""
                                                if(cellaProvincia.toString().contains("\'")) cellaProvincia = cellaProvincia.replaceAll("\'","")
                                                def cellaLocalita=cell("L")?.value?:""
                                                if(cellaLocalita.toString().contains("\'")) cellaLocalita = cellaLocalita.replaceAll("\'","")
                                                def cellaCap=cell("O")?.value?:""
                                                if(cellaCap.toString().contains("\'")) cellaCap = cellaCap.replaceAll("\'","")
                                                def cellaTel=cell("P")?.value?:""
                                                if(cellaTel.toString().contains("\'")) cellaTel = cellaTel.replaceAll("\'","")
                                                def cellaCel=cell("Q")?.value?:""
                                                if(cellaCel.toString().contains("\'")) cellaCel = cellaCel.replaceAll("\'","")
                                                def cellaCF=cell("J")?.value?:""
                                                def cellaIndirizzo=cell("M")?.value?:""
                                                if(cellaIndirizzo.toString().contains("\'")) cellaIndirizzo = cellaIndirizzo.replaceAll("\'","")
                                                def cellaEmail=cell("R")?.value?:""
                                                if(cellaEmail.toString().contains("\'")) cellaEmail = cellaEmail.replaceAll("\'","")
                                                cognomeDef=cellaCognome.toString().trim().toUpperCase()
                                                nomeDef=cellaNome.toString().trim().toUpperCase()
                                                provinciaDef=cellaProvincia.toString().trim().toUpperCase()
                                                localitaDef=cellaLocalita.toString().trim().toUpperCase()
                                                capDef=cellaCap.toString().trim()
                                                telDef=cellaTel.toString().trim()
                                                cellDef=cellaCel.toString().trim()
                                                if(cellDef.toString().contains(".")){
                                                    //def punto=cellDef.toString().indexOf(".")
                                                    cellDef=cellDef.toString().replaceAll("[^0-9]", "")
                                                }
                                                if(telDef.toString().contains(".")){
                                                    def punto=telDef.toString().indexOf(".")
                                                    telDef=telDef.toString().replaceAll("[^0-9]", "")
                                                }
                                                if(cellaCF !='null'){
                                                    cfDef=cellaCF.toString().trim().toUpperCase()
                                                    if(cfDef){
                                                        def annoCF=cfDef.substring(6,8)
                                                        if(Integer.parseInt(annoCF) && Integer.parseInt(annoCF)<=40){
                                                            sup75=true
                                                        }
                                                        def sessC=cfDef.substring(9,11)
                                                        if(Integer.parseInt(sessC) && Integer.parseInt(sessC)>40){
                                                            tipoClienteDef=TipoCliente.F
                                                        }
                                                    }
                                                    /*if(!cfDef.toString().matches("^[a-z|A-Z]{6}[0-9]{2}[A-Z|a-z][0-9]{2}[A-Z|a-z][0-9]{3}[A-Z|a-z]\$") && !cfDef.matches("^0+\\d*(?:\\.\\d+)?\$")){
                                                        cfDef=Double.valueOf(cfDef).longValue().toString()
                                                    }

                                                    if(!cellaCF.matches("[0-9]{11}")){
                                                    cfDef=cfDef.padLeft(11,"0")
                                                    }
                                                    */
                                                }

                                                indirizzoDef=cellaIndirizzo.toString().trim().toUpperCase()
                                                if(cellaEmail.toString().trim().size()>4){
                                                    emailDef=cellaEmail.toString().trim()
                                                }

                                            }else{
                                                nonTrovato=true
                                            }

                                            if(nonTrovato){
                                                logg =new Log(parametri: "il beneficiario ${cellaBeneficiario} non appare nelle colonne dei dati coobligato/cliente/business, controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                errorePratica.add("per la pratica-->${cellnoPratica} il beneficiario non appare nelle colonne dei dati coobligato/cliente/business controllare file,")
                                                erroPolizza++
                                            }else{
                                                def cellaNomeDealer=cell("F")?.value?:null
                                                if(cellaNomeDealer.toString().contains("\'")) cellaNomeDealer = cellaNomeDealer.replaceAll("\'","")
                                                def cellaProdotto=cell("AZ")?.value?:null
                                                if(cellaProdotto.toString().contains("\'")) cellaProdotto = cellaProdotto.replaceAll("\'","")
                                                def cellaDataImmatr=cell("AQ")?.value?:null
                                                if(cellaDataImmatr.toString().contains("\'")) cellaDataImmatr = cellaDataImmatr.replaceAll("\'","")
                                                def cellaDurata=cell("BA")?.value?:null
                                                //println "cell(\"AZ\") ${cellaDurata}"
                                                if(cellaDurata.toString().contains("\'")) cellaDurata = cellaDurata.replaceAll("\'","")
                                                def cellaMarca=cell("AR")?.value?:null
                                                if(cellaMarca.toString().contains("\'")) cellaMarca = cellaMarca.replaceAll("\'","")
                                                def cellaModello=cell("AS")?.value?:null
                                                if(cellaModello.toString().contains("\'")) cellaModello = cellaModello.replaceAll("\'","")
                                                def cellaBeneFinanziato=cell("AP")?.value?:null
                                                if(cellaBeneFinanziato.toString().contains("\'")) cellaBeneFinanziato = cellaBeneFinanziato.replaceAll("\'","")
                                                def cellapremioImponibile=cell("BB")?.value?:null
                                                if(cellapremioImponibile.toString().contains("\'")) cellapremioImponibile = cellapremioImponibile.replaceAll("\'","")
                                                /*def cellapremioLordo=cell("BB")?.value?:null*/
                                                def cellaprovvVendi=cell("BE")?.value?:null
                                                if(cellaprovvVendi.toString().contains("\'")) cellaprovvVendi = cellaprovvVendi.replaceAll("\'","")
                                                def cellaprovvDealer=cell("BD")?.value?:null
                                                if(cellaprovvDealer.toString().contains("\'")) cellaprovvDealer = cellaprovvDealer.replaceAll("\'","")
                                                def cellaprovvGmfi=cell("BF")?.value?:null
                                                if(cellaprovvGmfi.toString().contains("\'")) cellaprovvGmfi = cellaprovvGmfi.replaceAll("\'","")
                                                def cellaTelaio
                                                if(cell("AV")?.value !=null){
                                                    cellaTelaio=cell("AV")?.value
                                                    if(cellaTelaio.toString().contains("\'")) cellaTelaio = cellaTelaio.replaceAll("\'","")
                                                }
                                                def cellaTarga
                                                if(cell("AW")?.value !=null){
                                                    cellaTarga=cell("AW")?.value
                                                    if( cellaTarga.toString().contains("\'")) cellaTarga = cellaTarga.replaceAll("\'","")
                                                }
                                                def cellaValoreAssi=cell("AX")?.value?:null
                                                if(cellaValoreAssi.toString().contains("\'")) cellaValoreAssi = cellaValoreAssi.replaceAll("\'","")
                                                def cellaPrezzo=cell("AX")?.value?:null
                                                if(cellaPrezzo.toString().contains("\'")) cellaPrezzo = cellaPrezzo.replaceAll("\'","")
                                                def cellaVenditore=cell("BH")?.value?:null
                                                if(cellaVenditore.toString().contains("\'")) cellaVenditore = cellaVenditore.replaceAll("\'","")
                                                def cellaTipoPol=cell("BK")?.value?:null
                                                if(cellaTipoPol.toString().contains("\'")) cellaTipoPol = cellaVenditore.replaceAll("\'","")
                                                def leasing=false
                                                def onStar=false
                                                def dataInserimento=new Date()
                                                dataInserimento=dataInserimento.clearTime()
                                                def dataScadenza
                                                def durata=null
                                                def premioImponibile=0.0
                                                def premioImponibileFoglio=0.0
                                                def provvDealerFoglio=0.0
                                                def provvVendFoglio=0.0
                                                def provvGMFFoglio=0.0
                                                def premioLordo=0.0
                                                def provvVendi=0.0
                                                def provvDealer=0.0
                                                def provvGmfi=0.0
                                                def premioFisso=126.83
                                                def imposte=0.0
                                                if(cellaTipoPol.toString().trim().toLowerCase().contains("lease")){leasing=true}
                                                if((cellaDurata.toString().trim()!=null && cellaDurata.toString().trim()!='') && cellaDurata.toString().trim().length()>0){
                                                    if(cellaDurata.toString().contains(".")){
                                                        def punto=cellaDurata.toString().indexOf(".")
                                                        cellaDurata=cellaDurata.toString().substring(0,punto).replaceAll("[^0-9]", "")
                                                    }
                                                    durata=Integer.parseInt(cellaDurata.toString().trim())
                                                    premioImponibile=premioFisso*durata
                                                    premioImponibile=Math.round(premioImponibile * 100) / 100
                                                    premioLordo = premioImponibile * (1.025)
                                                    premioLordo=Math.round(premioLordo * 100) / 100
                                                    provvGmfi=premioImponibile*0.38
                                                    provvGmfi=Math.round(provvGmfi * 100) / 100
                                                    if(dealer.percVenditore>0){
                                                        provvDealer= (premioImponibile*0.1867)
                                                        provvDealer=Math.round(provvDealer * 100) / 100
                                                        provvVendi= (premioImponibile*0.0933)
                                                        provvVendi=Math.round(provvVendi * 100) / 100
                                                    }else{
                                                        provvDealer= (premioImponibile*0.28)
                                                        provvDealer=Math.round(provvDealer * 100) / 100
                                                        provvVendi= (premioImponibile*0.0)
                                                        provvVendi=Math.round(provvVendi * 100) / 100
                                                    }
                                                    if(cellaprovvDealer !=null && cellaprovvDealer !=''){
                                                        if(cellaprovvDealer instanceof BigDecimal){
                                                            cellaprovvDealer=cellaprovvDealer.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                                            provvDealerFoglio = new BigDecimal(cellaprovvDealer)
                                                        }else if(cellaprovvDealer instanceof Double){
                                                            provvDealerFoglio = BigDecimal.valueOf(cellaprovvDealer)
                                                            provvDealerFoglio=Math.round(provvDealerFoglio * 100) / 100
                                                        }
                                                        else {provvDealerFoglio=0.0}
                                                        def diffProvvD=Math.abs(Math.round((provvDealer-provvDealerFoglio)*100)/100)
                                                        if(diffProvvD>=0.05 && diffProvvD!=""){
                                                            logg =new Log(parametri: "per la polizza ${cellnoPratica}  la provv. Dealer inserita nel foglio ${provvDealerFoglio} non corrisponde con la provv. Dealer calcolata : ${provvDealer} ", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            errorePratica.add("per la polizza ${cellnoPratica}  la provv. Dealer inserita nel foglio ${provvDealerFoglio} non corrisponde con la provv. Dealer calcolata : ${provvDealer},")
                                                            erroPolizza++
                                                        }
                                                    }else{
                                                        logg =new Log(parametri: "per la polizza ${cellnoPratica} il valore della provv. Dealer non e' stata inserita controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        errorePratica.add("per la polizza ${cellnoPratica} il valore della provv. Dealer non e' stata inserita controllare file,")
                                                        erroPolizza++
                                                        provvDealerFoglio=0.0
                                                    }
                                                    if(cellaprovvGmfi !=null && cellaprovvGmfi !=''){
                                                        if(cellaprovvGmfi instanceof BigDecimal){
                                                            cellaprovvGmfi=cellaprovvGmfi.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                                            provvGMFFoglio = new BigDecimal(cellaprovvGmfi)
                                                        }else if(cellaprovvGmfi instanceof Double){
                                                            provvGMFFoglio = BigDecimal.valueOf(cellaprovvGmfi)
                                                            provvGMFFoglio=Math.round(provvGMFFoglio * 100) / 100
                                                        }else {provvGMFFoglio=0.0}
                                                        def diffProvvGMF=Math.abs(Math.round((provvGmfi-provvGMFFoglio)*100)/100)
                                                        if(diffProvvGMF>=0.05 && diffProvvGMF!=""){
                                                            logg =new Log(parametri: "per la polizza ${cellnoPratica}  la provv. GMF inserita nel foglio ${provvGMFFoglio} non corrisponde con la provv. GMF calcolata : ${provvGmfi} ", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            errorePratica.add("per la polizza ${cellnoPratica}  la provv. GMF inserita nel foglio ${provvGMFFoglio} non corrisponde con la provv. GMF calcolata : ${provvGmfi},")
                                                            erroPolizza++
                                                        }

                                                    }else{
                                                        logg =new Log(parametri: "per la polizza ${cellnoPratica} il valore della provv. GMF non e' stata inserita controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        errorePratica.add("per la polizza ${cellnoPratica} il valore della provv. GMF non e' stata inserita controllare file,")
                                                        erroPolizza++
                                                        provvGMFFoglio=0.0
                                                    }
                                                    if(cellaprovvVendi !=null && cellaprovvVendi !=''){
                                                        if(cellaprovvVendi instanceof BigDecimal){
                                                            cellaprovvVendi=cellaprovvVendi.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                                            provvVendFoglio = new BigDecimal(cellaprovvVendi)
                                                        }else if(cellaprovvVendi instanceof Double){
                                                            provvVendFoglio = BigDecimal.valueOf(cellaprovvVendi)
                                                            provvVendFoglio=Math.round(provvVendFoglio * 100) / 100
                                                        }else {provvVendFoglio=0.0}
                                                        def diffProvvVend=Math.abs(Math.round((provvVendi-provvVendFoglio)*100)/100)
                                                        if(diffProvvVend>=0.05 && diffProvvVend!=""){
                                                            logg =new Log(parametri: "per la polizza ${cellnoPratica}  la provv. venditore inserita nel foglio ${provvVendFoglio} non corrisponde con la provv. venditore calcolata : ${provvVendi} ", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            errorePratica.add("per la polizza ${cellnoPratica}  la provv. venditore inserita nel foglio ${provvVendFoglio} non corrisponde con la provv. venditore calcolata : ${provvVendi},")
                                                            erroPolizza++
                                                        }

                                                    }else{
                                                        logg =new Log(parametri: "per la polizza ${cellnoPratica} il valore della provv. venditore non e' stata inserita controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        errorePratica.add("per la polizza ${cellnoPratica} il valore della provv. venditore non e' stata inserita controllare file,")
                                                        erroPolizza++
                                                        provvVendFoglio=0.0
                                                    }
                                                    imposte=premioImponibile *0.025
                                                    if( dataDecorrenza!=null){
                                                        if(durata==1){
                                                            dataScadenza= use(TimeCategory) {dataDecorrenza +12.months}
                                                        }else if( durata==2){
                                                            dataScadenza= use(TimeCategory) {dataDecorrenza +24.months}
                                                        }else if(durata==3){
                                                            dataScadenza=use(TimeCategory) {dataDecorrenza +36.months}
                                                        }else if(durata==4){
                                                            dataScadenza= use(TimeCategory) {dataDecorrenza +48.months}
                                                        }else if(durata==5){
                                                            dataScadenza=use(TimeCategory) {dataDecorrenza +60.months}
                                                        }else{
                                                            dataScadenza=null
                                                        }
                                                    }else{
                                                        logg =new Log(parametri: "la data decorrenza non e' stata inserita pertanto non e' possibile mettere una data scadenza, controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        errorePratica.add("la data decorrenza non e' stata inserita pertanto non e' possibile mettere una data scadenza controllare file,")
                                                        erroPolizza++
                                                        dataScadenza=null
                                                    }
                                                }else{
                                                    logg =new Log(parametri: "la durata non e' stata inserita, controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    errorePratica.add("la durata non e' stata inserita controllare file,")
                                                    erroPolizza++

                                                }
                                                def coperturaR
                                                if(cellaProdotto.toString().trim()!=null && cellaProdotto.toString().trim()!=''){
                                                    coperturaR=cellaProdotto

                                                }
                                                def dataImmatricolazione=null
                                                if(!(cellaDataImmatr.toString().trim().equals("null") || cellaDataImmatr.toString().trim()!='')){
                                                    def newDate = Date.parse( 'MM/yyyy', cellaDataImmatr )
                                                    dataImmatricolazione = newDate
                                                }else{
                                                    dataImmatricolazione = new Date()
                                                }
                                                if(cellaMarca.toString().equals("null") && cellaMarca.toString().trim()!=''){
                                                    cellaMarca=null;
                                                }else{
                                                    cellaMarca=cellaMarca.toString().trim().toUpperCase()
                                                }
                                                if(cellaModello.toString().equals("null")&& cellaModello.toString().equals("")){
                                                    cellaModello=null;
                                                }else{
                                                    cellaModello=cellaModello.toString().trim().toUpperCase()
                                                }
                                                def nuovo=false
                                                if(cellaBeneFinanziato.toString().trim()!=null && cellaBeneFinanziato.toString().trim()!=''){
                                                    if(cellaBeneFinanziato.toString().trim().toLowerCase().contains("seminuove")){
                                                        nuovo=false
                                                    }else{
                                                        nuovo=true
                                                    }
                                                }
                                                if(cellapremioImponibile !=null && cellapremioImponibile !=''){
                                                    if(cellapremioImponibile instanceof BigDecimal){
                                                        cellapremioImponibile=cellapremioImponibile.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                                        premioImponibileFoglio = new BigDecimal(cellapremioImponibile)
                                                    }else if(cellapremioImponibile instanceof Double){
                                                        premioImponibileFoglio = BigDecimal.valueOf(cellapremioImponibile)
                                                        premioImponibileFoglio=Math.round(premioImponibileFoglio * 100) / 100
                                                    }else {premioImponibileFoglio=0.0}
                                                    def diffPremioImpo=Math.abs(Math.round((premioImponibile-premioImponibileFoglio)*100)/100)
                                                    if(diffPremioImpo>=0.05 && diffPremioImpo!=""){
                                                        logg =new Log(parametri: "per la polizza ${cellnoPratica}  il premio imponibile inserito nel foglio ${premioImponibileFoglio} non corrisponde con il premio (imponibile: ${premioFisso} * la durata: ${durata}) -> ${premioImponibile}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                        errorePratica.add("per la polizza ${cellnoPratica} il premio imponibile inserito nel foglio ${premioImponibileFoglio} non corrisponde con il premio (imponibile: ${premioFisso} * la durata: ${durata})--> ${premioImponibile},")
                                                        erroPolizza++
                                                    }

                                                }else{
                                                    logg =new Log(parametri: "per la polizza ${cellnoPratica} il valore del premio imponibile non e' stato inserito controllare file", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    errorePratica.add("per la polizza ${cellnoPratica} il valore del premio imponibile non e' stato inserito controllare file,")
                                                    erroPolizza++
                                                    premioImponibileFoglio=0.0
                                                }

                                                def valoreAssicurato
                                                if((cellaValoreAssi.toString().trim()!=null && cellaValoreAssi.toString().trim()!='' )&& cellaValoreAssi.toString().length()>0){
                                                    cellaValoreAssi=cellaValoreAssi.toString().trim()
                                                    def counter = 0
                                                    for( int i=0; i<cellaValoreAssi.length(); i++ ) {
                                                        if( cellaValoreAssi.charAt(i) == '.' ) {
                                                            counter++
                                                        }
                                                    }
                                                    if(counter==1){
                                                        def punto=cellaValoreAssi.indexOf(".")
                                                        valoreAssicurato=cellaValoreAssi.substring(0,punto).replaceAll("[^0-9]", "")
                                                    }else if(counter>1){
                                                        def sdopto=cellaValoreAssi.indexOf(".", cellaValoreAssi.indexOf(".") + 1)
                                                        valoreAssicurato=cellaValoreAssi.substring(0,sdopto).replaceAll("[^0-9]", "")
                                                    }
                                                    valoreAssicurato = new BigDecimal(valoreAssicurato)
                                                }else{
                                                    valoreAssicurato=0.0
                                                }
                                                def prezzoVendita
                                                if((cellaPrezzo.toString().trim()!=null || cellaPrezzo.toString().trim()!='' ) && cellaPrezzo.toString().length()>0){
                                                    prezzoVendita=cellaPrezzo.toString().replaceAll("\\.","").replaceAll(",","\\.")
                                                    //prezzoVendita=prezzoVendita.toString().replaceAll(",","\\.")
                                                    prezzoVendita = new BigDecimal(prezzoVendita)
                                                }else{
                                                    prezzoVendita=0.0
                                                }
                                                if(cellaVenditore.toString()!=null || cellaVenditore.toString()!=''){
                                                    cellaVenditore=cellaVenditore.toString().trim()
                                                }else{
                                                    cellaVenditore=""
                                                }
                                                if(dealer && cellnoPratica && cellaTelaio && cellaBeneficiario.toString()!='' ){
                                                    if (!sup75){
                                                        def polizzaPAI = PolizzaPaiPag.findOrCreateWhere(
                                                                annullata:false,
                                                                cap: capDef.toString().trim().padLeft(5,"0"),
                                                                cellulare:  cellDef.toString().trim(),
                                                                cognome:  cognomeDef.toString().trim(),
                                                                coperturaRichiesta: coperturaR,
                                                                dataDecorrenza:dataDecorrenza,
                                                                dataImmatricolazione: dataImmatricolazione,
                                                                dataInserimento: dataInserimento,
                                                                dataScadenza: dataScadenza,
                                                                dealer: dealer,
                                                                durata:durata,
                                                                email: emailDef.toString().trim(),
                                                                indirizzo: indirizzoDef.toString().trim().toUpperCase(),
                                                                localita: localitaDef.toString().trim().toUpperCase(),
                                                                marca: cellaMarca.toString().trim(),
                                                                modello: cellaModello.toString().trim(),
                                                                noPolizza: cellnoPratica.toString().trim(),
                                                                nome: nomeDef.toString().trim().toUpperCase(),
                                                                nuovo:nuovo,
                                                                onStar: onStar,
                                                                partitaIva: cfDef.toString().trim().toUpperCase(),
                                                                premioImponibile:premioImponibile,
                                                                premioLordo:premioLordo,
                                                                provincia: provinciaDef.toString().trim().toUpperCase(),
                                                                provvDealer:provvDealer,
                                                                provvGmfi:provvGmfi,
                                                                provvVenditore:provvVendi,
                                                                stato:StatoPolizza.POLIZZA,
                                                                tipoCliente:tipoClienteDef,
                                                                tipoPolizza:TipoPolizza.PAIPAGAMENTO,
                                                                step:"0",
                                                                telaio:cellaTelaio.toString().trim(),
                                                                //targa:cellaTarga?:cellaTarga.toString().trim(),
                                                                telefono:telDef.toString().trim(),
                                                                valoreAssicurato:valoreAssicurato,
                                                                valoreAssicuratoconiva:prezzoVendita,
                                                                certificatoMail:false,
                                                                codiceZonaTerritoriale: "1",
                                                                rappel:0.0,
                                                                codOperazione:"0",
                                                                dSlip:"S",
                                                                venditore:cellaVenditore?cellaVenditore.toString().trim().toUpperCase():"",
                                                                imposte:imposte
                                                        )
                                                        if(cellaTarga){
                                                            polizzaPAI.targa=cellaTarga?:cellaTarga.toString().trim()
                                                        }
                                                        if(polizzaPAI.save(flush:true)) {
                                                            logg =new Log(parametri: "la polizza ${cellnoPratica} viene salvata nel DB", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            flussiPAI.add("pratica-->${cellnoPratica} salvata nel DB,")
                                                            def tracciatoPolizza= tracciatiService.generaTracciatoPAIPAgamento(polizzaPAI,leasing)
                                                            if(!tracciatoPolizza.save(flush: true)){
                                                                logg =new Log(parametri: "errore creazione tracciato PAI", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                                risposta.add(noPratica:cellnoPratica, errore:"Errore creazione tracciato PAI: ${tracciatoPolizza.errors}")
                                                                errorePratica.add(" pratica-->${cellnoPratica} Errore creazione tracciato PAI: ${tracciatoPolizza.errors},")
                                                                erroPolizza++
                                                            }else{
                                                                flussiPAI.add("per la pratica-->${cellnoPratica} tracciato PAI PAGAMENTO generato,")
                                                                totale++
                                                                logg =new Log(parametri: "per la pratica-->${cellnoPratica} tracciato PAI PAGAMENTO generato", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            }
                                                        }
                                                        else{
                                                            logg =new Log(parametri: "Errore creazione polizza:${cellnoPratica}--> ${polizzaPAI.errors}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                            risposta.add(noPratica:cellnoPratica, errore:"Errore creazione polizza: ${polizzaPAI.errors}")
                                                            errorePratica.add(" pratica-->${cellnoPratica} Errore creazione polizza: ${polizzaPAI.errors},")
                                                            erroPolizza++
                                                        }
                                                    }else{
                                                        errorePratica.add("il beneficiario della pratica-->${cellnoPratica}  supera la eta' di 75 anni,")
                                                        erroPolizza++
                                                    }

                                                }else if(!dealer){
                                                    logg =new Log(parametri: "Errore creazione polizza:${cellnoPratica} il dealer non e' presente", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    risposta.add("Errore creazione polizza:${cellnoPratica} il dealer non e' presente")
                                                    errorePratica.add("Errore creazione polizza:${cellnoPratica} il dealer ${cellaDealer}- ${cellaNomeDealer} non e' presente,")
                                                    erroPolizza++
                                                }else if(!cellnoPratica){
                                                    logg =new Log(parametri: "Errore creazione polizza: non e' presente un numero di pratica riga ${j}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    risposta.add("Errore creazione polizza: non e' presente un numero di pratica riga ${j},")
                                                    errorePratica.add("Errore creazione polizza: non e' presente un numero di polizza riga ${j},")
                                                    erroPolizza++
                                                }else if(!cellaTelaio){
                                                    logg =new Log(parametri: "Errore creazione polizza:${cellnoPratica} non e' presente un numero di telaio", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                    risposta.add("Errore creazione polizza:${cellnoPratica} non e' presente un numero di telaio,")
                                                    errorePratica.add("Errore creazione polizza:${cellnoPratica} non e' presente un numero di telaio,")
                                                    erroPolizza++
                                                }
                                            }
                                        }else{
                                            logg =new Log(parametri: "per la polizza ${cellnoPratica}-->i dati del beneficiaro non sono stati inseriti", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                            risposta.add("per la polizza ${cellnoPratica}-->i dati del beneficiaro non sono stati inseriti,")
                                            errorePratica.add("per la polizza ${cellnoPratica}-->i dati del beneficiaro non sono stati inseriti,")
                                            erroPolizza++
                                        }
                                    }
                                }
                                j++
                                if(erroPolizza>0){ totaleErr++}
                            }
                        }
                    }
                }catch (e){
                    logg =new Log(parametri: "c'e' un problema con il file, ${e.toString()}", operazione: "caricamento  polizze PAI pagamento tramite xlsx", pagina: "POLIZZE PAI pagamento")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println "c'e' un problema con il file, ${e.toString()}"
                    errorePratica.add("c'e' un problema con il file, ${e.toString()}")
                }
                if(errorePratica.size()>0){
                    for (String s : errorePratica)
                    {
                        listErrori += s + "\r\n\r\n"
                    }
                }
                if(flussiPAI.size()>0){
                    for (String s : flussiPAI)
                    {
                        listFlussiPAI += s + "\r\n\r\n"
                    }
                }
                if(flussiPAI.size()>0){
                    flash.flussiPAI=listFlussiPAI+="totale flussi generati ${totale}"
                }
                if(errorePratica.size()>0){
                    flash.errorePratica=listErrori+="totale flussi non generati ${totaleErr}"
                }
            } else flash.error = "Specificare l'elenco xlsx"
            redirect controller: "polizze", action: "listPaiPag"
        }else response.sendError(404)
    }
    def caricaPolizzaPAIPagamento() {
        def logg
        if (request.post) {
            def errorePratica=[]
            def fileReader = null
            def reader = null
            def file =params.excelPratica
            if(file.bytes) {
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                def risposta =[]
                try {
                    logg =new Log(parametri: "chiamo il job per caricare le polizze e chiamare il ws", operazione: "carica pratica PAI PAG", pagina: "Polizze PAI PAG")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    CaricaCSVPaipagJob.triggerNow([myInputStream:myInputStream])
                    // CaricaCSVPaipagJob.triggerNow([myInputStream:str])
                    flash.associazione = "A breve arrivera' la mail con il riassunto delle polizze inserite"

                }catch (e){
                    logg =new Log(parametri: "c\u00E8 un problema con il caricamento del file ${e.toString()}", operazione: "carica pratica PAI PAG", pagina: "POLIZZE PAI PAG")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    risposta.add("c\u00E8  un problema con il caricamento del file")
                    errorePratica.add("c\u00E8  un problema con il caricamento del file ")
                }

            } else {flash.error = "Specificare l'elenco excel"}

            redirect action: "listPaiPag"
        }else response.sendError(404)
    }
    def elencoFlussiPAIPagamento() {
        def dataOdierna=new Date()
        dataOdierna= dataOdierna.clearTime()
        def traccPAI = /*SqlLogger.log {*/TracciatoPAIPag.createCriteria().list() {
            isNull ("dataCaricamento")
            eq "tipo", TipoPai.PAGAMENTO
            eq "tipoPolizza", TipoPolizza.PAIPAGAMENTO
        }
        def data=new Date()
        if(traccPAI) {
            byte[] b = traccPAI.tracciatoPAIPaga.join("\n").getBytes()
            response.contentType = "text/plain"
            response.addHeader "Content-disposition", "attachment; filename=flussiPAIPaga_${data.format("yyyyMMdd")}.txt"
            response.outputStream << b
        } //else response.sendError(404)
        else{
            flash.error = "Non ci sono flussi PAI da mostrare"
            redirect action: "listPaiPag"
            return
        }
    }
    def generaFileGMFGENPAI(){
        def logg
        if (request.post) {
            if(params.mese !="" && params.anno !=""){
                try {
                    //println "questi son i parametri ${params}"
                    def mese=params.mese
                    def anno=params.anno
                    GenGMFGENPAIJob.triggerNow([mese:mese,anno:anno])
                    flash.error = "generazione del report GMFGEN, a breve arrivera' una mail con il file"

                }catch(e) {
                    logg =new Log(parametri: "errore try catch ${e.toString()}", operazione: "generazione GMFGEN", pagina: "Polizza CASH")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println e.toString() }
            }else{
                logg =new Log(parametri: "Selezionare un mese per lanciare la generazione del report", operazione: "generazione GMFGEN", pagina: "Polizza CASH")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                flash.certificati=["Selezionare un mese per lanciare la generazione del report"]
            }

            redirect action: "listPaiPag"
        }else response.sendError(404)


    }
    def caricacertAXA(){
        def logg
        def listcert=""
        def listError=""
        def totaleErr=0
        def totale=0
        if (request.post) {
            def certAXA=[], erroreCert=[]
            def file =params.pdfCert
            def filename=file.originalFilename
            if(file.bytes){
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                def reader = new PdfReader(myInputStream)
                def totalPages = reader.getNumberOfPages()
                logg =new Log(parametri: "totale certificati trovati ${totalPages} ", operazione: "CARICAMENTO CERT AXA", pagina: "POLIZZE PAI PAGAMENTO")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                for(def i = 1; i <= totalPages; i++) {
                    def pdf = new Document()
                    def output = new ByteArrayOutputStream()
                    def pdfWriter = PdfWriter.getInstance(pdf, output)
                    pdf.open()
                    def directContent = pdfWriter.getDirectContent()
                    def piva, telaio
                    for(def j = i; j <= i; j++) {
                        def StringBuilder sb = new StringBuilder()
                        def text = PdfTextExtractor.getTextFromPage(reader, j)
                        def input = []

                        if(text.contains("C.F./P.IVA")){
                            // println text
                            piva=text.substring(text.indexOf("C.F./P.IVA")+11,text.indexOf("C.F./P.IVA")+29)
                            piva=piva.toLowerCase().replaceAll("\\s+","").replaceAll("-","_").replaceAll(":","").toUpperCase()
                            piva=piva.trim()
                            logg =new Log(parametri: "partita iva trovata--> ${piva} ", operazione: "CARICAMENTO CERT AXA", pagina: "POLIZZE PAI PAGAMENTO")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }
                        if(text.contains("Telaio")){
                            // println text
                            telaio=text.substring(text.indexOf("Telaio")+7,text.indexOf("Telaio")+29)
                            telaio=telaio.toLowerCase().replaceAll("\\s+","").replaceAll("-","_").replaceAll(":","").toUpperCase()
                            telaio=telaio.trim()
                            logg =new Log(parametri: "telaio trovato--> ${telaio} ", operazione: "CARICAMENTO CERT AXA", pagina: "POLIZZE PAI PAGAMENTO")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }
                        pdf.newPage()
                        def page = pdfWriter.getImportedPage(reader, j)
                        directContent.addTemplate(page, 0, 0)
                        pdf.newPage()
                        pdfWriter.setPageEmpty(false)
                    }
                    output.flush()
                    pdf.close()
                    output.close()
                    def certificato= DocumentiSalesSpecialist.findByPIvaAndTelaio("${piva}","${telaio}")
                    if(certificato){
                        logg =new Log(parametri: "Esiste gia' un certificato caricato per questa piva ${piva} e questo telaio ${telaio}", operazione: "CARICAMENTO CERT AXA", pagina: "POLIZZE PAI PAGAMENTO")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        erroreCert.add("Esiste gia' un certificato caricato per questa piva ${piva} e questo telaio ${telaio},")
                        totaleErr++
                    }else{
                        def certificati=new DocumentiSalesSpecialist()
                        certificati.fileName = "${telaio?: piva?:"senzaTelaioPIVA"}.pdf"
                        certificati.fileContent =output.toByteArray()
                        certificati.pIva ="${piva?:"senza_piva"}"
                        certificati.tipo="CERTIFICATO"
                        certificati.telaio="${telaio?:"senza_telaio"}"
                        if (certificati.save(flush: true)) {
                            logg =new Log(parametri: "${certificati.fileName} generato correttamente", operazione: "CARICAMENTO CERT AXA", pagina: "POLIZZEPAI PAGAMENTO")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            certAXA.add( " ${certificati.fileName} generato correttamente,")
                            totale++
                        } else {
                            logg =new Log(parametri: "Errore generazione certificato ${certificati.errors}", operazione: "CARICAMENTO CERT AXA", pagina: "POLIZZE PAI PAGAMENTO")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            erroreCert.add(" Errore generazione certificato  ${certificati.errors},")
                            totaleErr++
                        }
                    }

                }
                if(erroreCert.size()>0){
                    for (String s : erroreCert)
                    {
                        listError += s + "\r\n\r\n"
                    }
                }
                if(certAXA.size()>0){
                    for (String s : certAXA)
                    {
                        listcert += s + "\r\n\r\n"
                    }
                }
                if(erroreCert.size()>0){
                    flash.errorePratica=listError+="totale certificati non caricati ${totaleErr}"
                }
                if(certAXA.size()>0){
                    flash.certPAI=listcert+="totale  certificati caricati ${totale}"
                }
            }
            redirect action: "listPaiPag"
        }else response.sendError(404)
    }
    def associapivacfPAI() {
        def logg
        def listAssocia=""
        def listErrori=""
        def totaleErr=0
        def totale=0
        if (request.post) {
            def associa=[], erroreTelaio=[]
            def file =params.telaiopiva
            if(file.bytes) {
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                def risposta =[]
                def certificati
                try {
                    def wb = ExcelReader.readXlsx(myInputStream) {
                        sheet (0) {
                            rows(from: 1) {
                                def cellaTelaio = cell("B")?.value?:""
                                def cellaPIva = cell("A")?.value?:""
                                def dataDecorrenza=""
                                if((cellaPIva.toString().equals("null") || cellaPIva.toString()=='')){
                                    logg =new Log(parametri: "il numero di partita iva non e' stato inserito nella cella", operazione: "associazione telaio-partita polizze PAI tramite xls", pagina: "POLIZZE PAI PAGAMENTO")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    erroreTelaio.add("il numero di partita iva non e' stato inserito nella cella,")
                                }else {
                                    if ((cellaTelaio.toString().equals("null") || cellaTelaio.toString() == '')) {
                                        logg = new Log(parametri: "il numero di telaio non e' stato inserito nella cella", operazione: "associazione telaio-partita polizze PAI tramite xls", pagina: "POLIZZE PAI PAGAMENTO")
                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        erroreTelaio.add("il numero di telaio non e' stato inserito nella cella,")
                                    } else {
                                    logg = new Log(parametri: "numero di telaio inserito ${cellaTelaio}", operazione: "associazione telaio-partita polizze PAI tramite xls", pagina: "POLIZZE PAI PAGAMENTO")
                                    if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    certificati = DocumentiSalesSpecialist.findByPIva(cellaPIva)
                                    if (!certificati) {
                                        logg = new Log(parametri: "non esiste un certificato associato a questa partita iva ${cellaPIva}", operazione: "associazione telaio-partita polizze PAI tramite xls", pagina: "POLIZZE PAI PAGAMENTO")
                                        if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                        erroreTelaio.add("non esiste un certificato associato a questa partita iva ${cellaPIva},")
                                        totaleErr++
                                    } else {
                                            certificati.telaio=cellaTelaio
                                            if (!certificati.save(flush: true)) {
                                                logg = new Log(parametri: "Errore aggiornamento certificato per pIva: ${cellaPIva} --> ${tracciatoPAIPolizzaR.errors}", operazione: "caricamento polizze PAI tramite xls", pagina: "POLIZZE PAI PAGAMENTO")
                                                if (!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                                erroreTelaio.add(" Errore aggiornamento certificato per pIva: ${cellaPIva} --> ${tracciatoPAIPolizzaR.errors},")
                                                totaleErr++
                                            } else {
                                                associa.add("associazione generata per file pdf -->polizza ${cellaPIva},")
                                                totale++
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                }catch (e){
                    println "c'e' un problema con il file, ${e.toString()}"
                    risposta.add("c'e' un problema con il file, ${e.toString()}")
                }
                if(erroreTelaio.size()>0){
                    for (String s : erroreTelaio)
                    {
                        listErrori += s + "\r\n\r\n"
                    }
                }
                if(associa.size()>0){
                    for (String s : associa)
                    {
                        listAssocia += s + "\r\n\r\n"
                    }
                }
                if(listAssocia.size()>0){
                    flash.certPAI=listAssocia+="totale associazioni generate ${totale}"
                }
                if(erroreTelaio.size()>0){
                    flash.errorePratica=listErrori+="totale associazioni non generate ${totaleErr}"
                }
            } else flash.error = "Specificare l'elenco xlsx"
            redirect action: "listPaiPag"
        }else response.sendError(404)
    }
    def caricaDealers() {
        def logg
        if (request.post) {
            def associa=[], erroreTelaio=[]
            def file =params.dealers
            if(file.bytes) {
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                def risposta =[]
                //try {
                    logg =new Log(parametri: "chiamo il job per caricare i dealers", operazione: "carica Dealers", pagina: "Polizze CASH")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    CaricaDealersJob.triggerNow([myInputStream:myInputStream])
                    flash.associazione = "A breve arrivera' la mail con il riassunto dei dealer caricati"
                /*}catch (e){
                    println "c'e' un problema con il file, ${e.toString()}"
                    risposta.add("c'e' un problema con il file, ${e.toString()}")
                    logg =new Log(parametri: "errore try catch: ${e.toString()}", operazione: "associazione telaio data tariffa", pagina: "Polizze FIN/LEASING")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }*/
            } else flash.error = "Specificare l'elenco xlsx"
            redirect action: "lista"
        }else response.sendError(404)
    }
    def correfinleasing() {
        def logg
        if (request.post) {
            def associa=[], erroreTelaio=[]
            def file =params.correPratica
            if(file.bytes) {
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                def risposta =[]
                //try {
                    logg =new Log(parametri: "chiamo il job per correggere i valori degli importi", operazione: "correzzione polizze fin/leasing", pagina: "Polizze FIN/LEASING")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    CorrezDataFinLeasJob.triggerNow([myInputStream:myInputStream])
                    flash.associazione = "A breve arrivera' la mail con il riassunto delle polizze aggiornate"
                /*}catch (e){
                    println "c'e' un problema con il file, ${e.toString()}"
                    risposta.add("c'e' un problema con il file, ${e.toString()}")
                    logg =new Log(parametri: "errore try catch: ${e.toString()}", operazione: "associazione telaio data tariffa", pagina: "Polizze FIN/LEASING")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }*/
            } else flash.error = "Specificare l'elenco csv"
            redirect action: "listFinanziate"
        }else response.sendError(404)
    }
    def generaflussicompagnia(){
        def logg
        def dataInizio, dataFine, nomeFile
        if (request.post) {
            //try {
                logg =new Log(parametri: "inizio a preparare la generazione dei tracciati solo per le polizze in quel periodo, parametri pasati datainizio: ${params.dataGenera1} data fine: ${params.dataGenera2}", operazione: "generazione tracciati compagnia", pagina: "polizze fin/leasing")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                def response =[]
                dataInizio = Date.parse('dd-MM-yyyy', params.dataGenera1).clearTime()
                dataFine = Date.parse('dd-MM-yyyy', params.dataGenera2).clearTime()

                def polizze= Polizza.createCriteria().list() {
                    ge "dateCreated", dataInizio
                    le "dateCreated", dataFine
                    eq "coperturaRichiesta", "PLATINUM"
                    eq "step","step 1"

                }
                if(polizze){
                    logg =new Log(parametri: "polizze trovate ${polizze.size()}",operazione:"generazione flussi compagnia", pagina: "Polizze FIN/LEASING")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    polizze.each(){polizza->
                        logg =new Log(parametri: "inizio a generare il flusso per la polizza ${polizza.noPolizza} ",operazione:"generazione flussi compagnia", pagina: "Polizze FIN/LEASING")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        def flussoIassicur = tracciatiService.generaTracciatoCompagnia(polizza)
                        if (!flussoIassicur.save(flush: true)) {
                            flash.errors = "Si è verificato un errore durante la generazione del flusso iAssicur polizza ${polizza.noPolizza}"
                            log.error renderErrors(bean: flussoIassicur)
                            logg =new Log(parametri: "errore gerenrazione flusso compagnia ${renderErrors(bean: flussoIassicur)}", operazione:"generazione flussi compagnia", pagina: "Polizze FIN/LEASING")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                        }
                    }
                    /*def traccDL = /*SqlLogger.log {*//*TracciatoCompagnia.createCriteria().list() {
                        isNull ("dataCaricamento")
                        le "dateCreated",dataFine
                        ge "dateCreated", dataInizio
                        eq "annullata", false
                    }

                    def data=new Date()
                    if(traccDL) {
                        byte[] b = traccDL.tracciatoCompagnia.join("\n").getBytes()
                        response.contentType = "text/plain"
                        response.addHeader "Content-disposition", "attachment; filename=flussiDL_${data.format("yyyyMMdd")}.txt"
                        response.outputStream << b
                    } //else response.sendError(404)
                    else{
                        flash.error = "Non ci sono flussi DL da mostrare"
                        redirect action: "listFinanziate"
                        return
                    }*/
                }
            flash.errors = "tracciati generati"
            redirect action: "listFinanziate"
            /*} catch(e) {
                logg =new Log(parametri: "errore try cash ${e.toString()}", operazione: "generazione tracciati compagnia", pagina: "POLIZZE FIN/LEASING")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                flash.error="errore generazione tracciati compagnia"
                redirect action: "listFinanziate"
            }*/
        }



    }
    def generaCertPaiPAG(){
        def logg
        if (request.post) {
            if(params.dataGeneraCert !=""){
                def dataGeneraCert = Date.parse("dd-MM-yyyy", params.dataGeneraCert).format("dd.MM.yyyy")
                try {
                    generaCertService.genCertPaiPAG(dataGeneraCert)
                    flash.error = "generazione dei certificati iniziato, a breve arrivera' una mail con il riassunto"
                }catch(e) {
                    logg =new Log(parametri: "errore try catch ${e.toString()}", operazione: "generazione certificati PAI PAGAMENTO", pagina: "Polizza PAI PAGAMENTO")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println e.toString()
                }

            }else{
                logg =new Log(parametri: "Selezionare una data per lanciare la generazione dei certificati", operazione: "generazione certificati Fin/Leasing generati", pagina: "Polizza Fin/Leasing")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                flash.certificati=["Selezionare una data per lanciare la generazione dei certificati"]
            }
            redirect action: "listPaiPag"
        }else response.sendError(404)


    }
    def inviaRiassuntoAUTOIMPORT(){
        def streamRiassunti = new ByteArrayOutputStream()
        def zipRiassunti = new ZipOutputStream(streamRiassunti)
        def logg, dataInizio, dataFine, nomeFile
        def fileNameAllegato
        def fileContentAllegato
        if (request.post) {
            try {
                logg =new Log(parametri: "inizio a preparare il file settimanale, parametri pasati datainizio: ${params.dataGenera1} data fine: ${params.dataGenera2}", operazione: "invio settimanale MACH1 delle polizze generate portale", pagina: "JOB ELENCO SETTIMANALE AUTOIMPORT")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                def response =[]
                dataInizio = Date.parse( 'dd-MM-yyyy', params.dataGenera1)
                dataFine = Date.parse( 'dd-MM-yyyy', params.dataGenera2)
                def dealer=Dealer.get(2)
                nomeFile="riepilogo_da_${dataInizio.format("ddMMyyyy")}_a_${dataFine.format("ddMMyyyy")}.zip"
                def polizze = /*SqlLogger.log {*/Polizza.createCriteria().list() {
                    ge "dataInserimento", dataInizio
                    le "dataInserimento", dataFine
                    eq "dealer", dealer
                    eq "tipoPolizza",TipoPolizza.CASH
                    eq "codOperazione","0"
                }
                if (polizze.size() > 0) {
                    estrazioneService.elencoPolizzeSettimanaleAUTOIMPORT(dataInizio,dataFine).each {fileName, fileContent ->
                        zipRiassunti.putNextEntry(new ZipEntry(fileName))
                        zipRiassunti.write(fileContent)
                    }

                    estrazioneService.generaFilePolizzeAutoimport(dataInizio,dataFine).each {fileName, fileContent ->
                        zipRiassunti.putNextEntry(new ZipEntry(fileName))
                        zipRiassunti.write(fileContent)
                    }
                    logg =new Log(parametri: "i file sono stati aggiunti al file zip", operazione: "invio settimanale per AUTOIMPORT delle polizze generate portale", pagina: "ELENCO  AUTOIMPORT")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    zipRiassunti.close()
                    streamRiassunti.close()
                }else{
                    logg =new Log(parametri: "Non ci sono polizze generate nel periodo scelto", operazione: "generazione riepilogo AUTOIMPORT delle polizze CASH", pagina: "ELENCO  AUTOIMPORT")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    flash.error="non ci sono polizze CASH generate nel periodo selezionato per AUTOIMPORT"
                    redirect action: "lista"
                }

            } catch(e) {
                logg =new Log(parametri: "errore try cash ${e.toString()}", operazione: "generazione riepilogo AUTOIMPORT delle polizze CASH", pagina: "ELENCO  AUTOIMPORT")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                flash.error="errore generazione riepilogo polizze CASH  per AUTOIMPORT"
                redirect action: "lista"
            }

        }
        if(streamRiassunti){
            response.contentType = "multipart/form-data"
            response.addHeader "Content-disposition", "attachment; filename=${nomeFile}"
            response.outputStream << streamRiassunti.toByteArray()
        }
    }
    def caricaPraticaCSV() {
        def logg
        if (request.post) {
            def errorePratica=[]
            def fileReader = null
            def reader = null
            def file =params.excelPratica
            if(file.bytes) {
                InputStream myInputStream = new ByteArrayInputStream(file.bytes)
                def str = new String(file.bytes)
                /*str.eachLine { String line ->
                    def splittato = line.split(";")
                    splittato.each { String token ->

                    }
                }*/
                def risposta =[]
                try {
                    logg =new Log(parametri: "chiamo il job per caricare le polizze e chiamare il ws", operazione: "carica pratica", pagina: "Polizze FIN/LEASING/RCA")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                   // CaricaCSVFinLeasJob.triggerNow([myInputStream:myInputStream])
                    CaricaCSVPolJob.triggerNow([myInputStream:str])
                    flash.associazione = "A breve arrivera' la mail con il riassunto delle polizze inserite"

                }catch (e){
                    logg =new Log(parametri: "c\u00E8 un problema con il caricamento del file ${e.toString()}", operazione: "carica pratica", pagina: "POLIZZE FINANZIATE")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    risposta.add("c\u00E8  un problema con il caricamento del file")
                    errorePratica.add("c\u00E8  un problema con il caricamento del file ")
                }

            } else {flash.error = "Specificare l'elenco csv"}

            redirect action: "listFinanziate"
        }else response.sendError(404)
    }

}
