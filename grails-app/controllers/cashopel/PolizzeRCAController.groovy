package cashopel

import cashopel.polizze.CausaleContatto
import cashopel.polizze.Contattare
import cashopel.polizze.MotivoAnnulla
import cashopel.polizze.Polizza
import cashopel.polizze.PolizzaRCA
import cashopel.polizze.StatoPolizza
import cashopel.polizze.TipoCliente
import cashopel.polizze.TipoPolizza
import cashopel.polizze.Tracciato
import cashopel.polizze.TracciatoIASSRCA
import cashopel.utenti.Admin
import cashopel.utenti.Dealer
import cashopel.utenti.Log
import com.presstoday.groovy.Stopwatch
import excel.builder.ExcelBuilder
import grails.converters.JSON
import grails.util.Environment
import groovy.time.TimeCategory
import org.grails.web.json.JSONObject
import security.SecurityUtils

import java.text.DecimalFormat
import java.text.DecimalFormatSymbols

class PolizzeRCAController {
    static scope = "prototype"
    def tracciatiService
    def DLWebService
    def caricamentiService
    def index() { }
    def listaRCA(){
        //CaricaPolizzeJob.triggerNow()
        //MailSeraleJob.triggerNow()
        //ScaricaPolizzeJob.triggerNow()
        //CaricaStatiPolJob.triggerNow()
        //SollecitoSettimanaleRCAMACH1Job.triggerNow()
        //ElencoSettimanaleRCAMACH1Job.triggerNow()

        def principal = SecurityUtils.principal
        def utente,dealer,admin
        if(SecurityUtils.hasRole("ADMIN")){
            admin=Admin.get(principal.id)
            utente=admin
        }

        else if(SecurityUtils.hasRole("DEALER")){
            dealer=Dealer.get(principal.id)
            utente=dealer
        }
        def max = 15, pagesPerPage = 7
        def page = (params.page ?: 1) as int
        def offset = (page - 1) * max
        def cerca = params.search
        def polizze = PolizzaRCA.createCriteria().list(max: max, offset: offset) {
            if(dealer) {
                eq "dealer", dealer
            }
            else {
                projections {
                    createAlias ('dealer', 'd')
                }
            }
            if(cerca) {
                or {
                    ilike "noPolizza", "%${cerca}%"
                    ilike "cognome", "%${cerca}%"
                    ilike "nome", "%${cerca}%"
                    ilike "partitaIva", "%${cerca}%"
                    ilike "telaio", "%${cerca}%"
                    ilike "targa", "%${cerca}%"
                    //ilike "tipoCliente",TipoCliente.DITTA_INDIVIDUALE.tipo
                    if(!dealer){
                        ilike "d.ragioneSocialeD", "%${cerca}%"
                    }
                }
            }
            and {
                ne('stato', StatoPolizza.DA_CHIAMARE)
                ne ('stato' ,StatoPolizza.ANNULLATA)
                ne ('stato' ,StatoPolizza.ANNULLATA_FURTO)
                order('stato', 'desc')
                if(!dealer){
                    order('d.ragioneSocialeD', 'asc')
                }

            }
        }

        def pages = Math.ceil(polizze.totalCount / max) as int
        def totalPages=pages
        if(pages <= 1) pages = []
        else if(pages <= pagesPerPage) pages = 1..pages
        else if(page < (pagesPerPage / 2))  pages = 1..pagesPerPage
        else {
            def minPage = page - ((pagesPerPage / 2) as int)
            def maxPage = page + ((pagesPerPage / 2) as int)
            if(maxPage > pages) pages = (maxPage - pagesPerPage)..pages
            else pages = minPage..maxPage
        }
        if(!pages && page > 1) {
            redirect action: "listaRCA", params: params.search ? [search: params.search] : null
        }
        else if(pages && page > pages.max()){
            redirect action: "listaRCA", params: [page: 1, search: params.search]
        }
        [polizze: polizze, pages: pages, page: page,utente:utente, totalPages:totalPages, url:request.queryString]
    }
    def dacontattare(){

        def principal = SecurityUtils.principal
        def utente,dealer,admin
        if(SecurityUtils.hasRole("ADMIN")){
            admin=Admin.get(principal.id)
            utente=admin
        }

        else if(SecurityUtils.hasRole("DEALER")){
            dealer=Dealer.get(principal.id)
            utente=dealer
        }
        def max = 15, pagesPerPage = 7
        def page = (params.page ?: 1) as int
        def offset = (page - 1) * max
        def cerca = params.search
        def polizze = PolizzaRCA.createCriteria().list(max: max, offset: offset) {
            if(dealer) {
                eq "dealer", dealer
            }
            else {
                projections {
                    createAlias ('dealer', 'd')
                }
            }
            if(cerca) {
                or {
                    ilike "noPolizza", "%${cerca}%"
                    ilike "cognome", "%${cerca}%"
                    ilike "partitaIva", "%${cerca}%"
                    ilike "telaio", "%${cerca}%"
                    //ilike "tipoCliente",TipoCliente.DITTA_INDIVIDUALE.tipo
                    if(!dealer){
                        ilike "d.ragioneSocialeD", "%${cerca}%"
                    }
                }
            }
            and {
                eq('stato', StatoPolizza.DA_CHIAMARE)
                order('stato', 'desc')
                if(!dealer){
                    order('d.ragioneSocialeD', 'asc')
                }

            }
        }

        def pages = Math.ceil(polizze.totalCount / max) as int
        def totalPages=pages
        if(pages <= 1) pages = []
        else if(pages <= pagesPerPage) pages = 1..pages
        else if(page < (pagesPerPage / 2))  pages = 1..pagesPerPage
        else {
            def minPage = page - ((pagesPerPage / 2) as int)
            def maxPage = page + ((pagesPerPage / 2) as int)
            if(maxPage > pages) pages = (maxPage - pagesPerPage)..pages
            else pages = minPage..maxPage
        }
        if(!pages && page > 1) {
            redirect action: "dacontattare", params: params.search ? [search: params.search] : null
        }
        else if(pages && page > pages.max()){
            redirect action: "dacontattare", params: [page: 1, search: params.search]
        }
        [polizze: polizze, pages: pages, page: page,utente:utente, totalPages:totalPages]
    }
    def annullate(){

        def principal = SecurityUtils.principal
        def utente,dealer,admin
        if(SecurityUtils.hasRole("ADMIN")){
            admin=Admin.get(principal.id)
            utente=admin
        }

        else if(SecurityUtils.hasRole("DEALER")){
            dealer=Dealer.get(principal.id)
            utente=dealer
        }
        def max = 15, pagesPerPage = 7
        def page = (params.page ?: 1) as int
        def offset = (page - 1) * max
        def cerca = params.search
        def polizze = PolizzaRCA.createCriteria().list(max: max, offset: offset) {
            if(dealer) {
                eq "dealer", dealer
            }
            else {
                projections {
                    createAlias ('dealer', 'd')
                }
            }
            if(cerca) {
                or {
                    ilike "noPolizza", "%${cerca}%"
                    ilike "cognome", "%${cerca}%"
                    ilike "partitaIva", "%${cerca}%"
                    ilike "telaio", "%${cerca}%"
                    //ilike "tipoCliente",TipoCliente.DITTA_INDIVIDUALE.tipo
                    if(!dealer){
                        ilike "d.ragioneSocialeD", "%${cerca}%"
                    }
                }
            }
            and {
                or{
                    eq('stato', StatoPolizza.ANNULLATA)
                    eq('stato', StatoPolizza.ANNULLATA_FURTO)
                }
                order('stato', 'desc')
                if(!dealer){
                    order('d.ragioneSocialeD', 'asc')
                }

            }
        }

        def pages = Math.ceil(polizze.totalCount / max) as int
        def totalPages=pages
        if(pages <= 1) pages = []
        else if(pages <= pagesPerPage) pages = 1..pages
        else if(page < (pagesPerPage / 2))  pages = 1..pagesPerPage
        else {
            def minPage = page - ((pagesPerPage / 2) as int)
            def maxPage = page + ((pagesPerPage / 2) as int)
            if(maxPage > pages) pages = (maxPage - pagesPerPage)..pages
            else pages = minPage..maxPage
        }
        if(!pages && page > 1) {
            redirect action: "annullate", params: params.search ? [search: params.search] : null
        }
        else if(pages && page > pages.max()){
            redirect action: "annullate", params: [page: 1, search: params.search]
        }
        [polizze: polizze, pages: pages, page: page,utente:utente, totalPages:totalPages]
    }
    def ofsgen(){


    }
    def lista_menu(){

        def principal = SecurityUtils.principal
        def utente,dealer,admin
        if(SecurityUtils.hasRole("ADMIN")){
            admin=Admin.get(principal.id)
            utente=admin
        }

        else if(SecurityUtils.hasRole("DEALER")){
            dealer=Dealer.get(principal.id)
            utente=dealer
        }
        def max = 15, pagesPerPage = 7
        def page = (params.page ?: 1) as int
        def offset = (page - 1) * max
        def cerca = params.search
        def polizze = PolizzaRCA.createCriteria().list(max: max, offset: offset) {
            if(dealer) {
                eq "dealer", dealer
            }
            else {
                projections {
                    createAlias ('dealer', 'd')
                }
            }
            if(cerca) {
                or {
                    ilike "noPolizza", "%${cerca}%"
                    ilike "cognome", "%${cerca}%"
                    ilike "partitaIva", "%${cerca}%"
                    ilike "telaio", "%${cerca}%"
                    //ilike "tipoCliente",TipoCliente.DITTA_INDIVIDUALE.tipo
                    if(!dealer){
                        ilike "d.ragioneSocialeD", "%${cerca}%"
                    }
                }
            }
            and {
                order('stato', 'desc')
                if(!dealer){
                    order('d.ragioneSocialeD', 'asc')
                }

            }
        }

        def pages = Math.ceil(polizze.totalCount / max) as int
        def totalPages=pages
        if(pages <= 1) pages = []
        else if(pages <= pagesPerPage) pages = 1..pages
        else if(page < (pagesPerPage / 2))  pages = 1..pagesPerPage
        else {
            def minPage = page - ((pagesPerPage / 2) as int)
            def maxPage = page + ((pagesPerPage / 2) as int)
            if(maxPage > pages) pages = (maxPage - pagesPerPage)..pages
            else pages = minPage..maxPage
        }
        if(!pages && page > 1) {
            redirect action: "lista_menu", params: params.search ? [search: params.search] : null
        }
        else if(pages && page > pages.max()){
            redirect action: "lista_menu", params: [page: 1, search: params.search]
        }
        [polizze: polizze, pages: pages, page: page,utente:utente, totalPages:totalPages]
    }
    def nuovaPolizza(){
        def logg, polizza, pagina
        //println "questi sono i parametri pda salvare--> $params"
        if(params.id && PolizzaRCA.exists(Long.parseLong(params.id))){
            polizza=PolizzaRCA.findById(Long.parseLong(params.id))
            println "statao-->${polizza.stato}"
            if(polizza.stato.toString()==StatoPolizza.ANNULLATA_FURTO.toString() || polizza.stato.toString()==StatoPolizza.ANNULLATA.toString()){
                pagina="ANNULLATA"
            }else if(polizza.stato.toString()==StatoPolizza.DA_CHIAMARE.toString()){
                pagina="DACONTTATARE"
            }else{
                pagina="RCA"
            }
        }else{
            polizza=new PolizzaRCA()
            pagina="RCA"
        }
        def province=[Agrigento:"AG",Alessandria:"AL",Ancona:"AN",Aosta:"AO",Arezzo:"AR",AscoliPiceno:"AP",Asti:"AT",Avellino:"AV",Bari:"BA",BarlettaAndria:"BT",Belluno:"BL",Benevento:"BN",Bergamo:"BG",Biella:"BI",Bologna:"BO",Bolzano:"BZ",Brescia:"BS",Brindisi:"BR",Cagliari:"CA",Caltanissetta:"CL",Campobasso:"CB",Carbonia_Iglesia:"CI",Caserta:"CE",Catania:"CT",Catanzaro:"CZ",Chieti:"CH",Como:"CO",Cosenza:"CS",Cremona:"CR",Crotone:"KR",Cuneo:"CN",Enna:"EN",Fermo:"FM",Ferrara:"FE",Firenze:"FI",Foggia:"FG",Forli_Cesena:"FC",Frosinone:"FR",Genova:"GE",Gorizia:"GO",Grosseto:"GR",Imperia:"IM",Isernia:"IS",La_Spezia:"SP",L_Aquila:"AQ",Latina:"LT",Lecce:"LE",Lecco:"LC",Livorno:"LI",Lodi:"LO",Lucca:"LU",Macerata:"MC",Mantova:"MN",Massa_Carrara:"MS",Matera:"MT",Messina:"ME",Milano:"MI",Modena:"MO",Monza_e_della_Brianza:"MB",Napoli:"NA",Novara:"NO",Nuoro:"NU",Olbia_Tempio:"OT",Oristano:"OR",Padova:"PD",Palermo:"PA",Parma:"PR",Pavia:"PV",Perugia:"PG",Pesaro_e_Urbino:"PU",Pescara:"PE",Piacenza:"PC",Pisa:"PI",Pistoia:"PT",Pordenone:"PN",Potenza:"PZ",Prato:"PO",Ragusa:"RG",Ravenna:"RA",Reggio_Calabria:"RC",Reggio_Emilia:"RE",Rieti:"RI",Rimini:"RN",Roma:"RM",Rovigo:"RO",Repubblica_di_San_Marino:"RSM",Salerno:"SA",Medio_Campidano:"VS",Sassari:"SS",Savona:"SV",Siena:"SI",Siracusa:"SR",Sondrio:"SO",Taranto:"TA",Teramo:"TE",Terni:"TR",Torino:"TO",Ogliastra:"OG",Trapani:"TP",Trento:"TN",Treviso:"TV",Trieste:"TS",Udine:"UD",Varese:"VA",Venezia:"VE",Verbano_Cusio_Ossola:"VB",Vercelli:"VC",Verona:"VR",Vibo_Valentia:"VV",Vicenza:"VI",Viterbo:"VT"]
        if(request.post) {
                def dealerId= params.codice.id
                def dealer =  Dealer.get(dealerId)
                params.nome=params.nome.toString().trim().toUpperCase()
                params.cognome=params.cognome.toString().trim().toUpperCase()
                params.partitaIva=params.partitaIva.toString().trim().toUpperCase()
                logg =new Log(parametri: "parametri inserimento:[dealer:${params.codice.id}, percVenditore:${params.percVenditore}, premioImponibile:${params.premioImponibile}, codiceZonaTerritoriale:${params.codiceZonaTerritoriale}, rappel:${params.rappel}, imposte:${params.imposte}, cognome:${params.cognome} , nome:${params.nome}, partitaIva:${params.partitaIva}, indirizzo:${params.indirizzo}, localita:${params.localita}, cap:${params.cap}, tipoCliente:${params.tipoCliente}, provincia:${params.provincia}, telefono:${params.telefono?params.telefono:""}, email:${params.email ? params.email :  ""}, certificatoMail: ${params.certificatoMail}, marca: ${params.marca}, modello: ${params.modello}, targa: ${params.targa? params.targa:""}, nuovo:${params.nuovo}, onStar:${params.onStar}, telaio: ${params.telaio? params.telaio:""}, valoreAssicuratoconiva:${params.valoreAssicuratoconiva}, iva:${params.iva}, valoreAssicurato:${params.valoreAssicurato}, dataImmatricolazione:${params.dataImmatricolazione}, step:${params.step}, coperturaRichiesta:${params.coperturaRichiesta}, durata:${params.durata}, dataDecorrenza:${params.dataDecorrenza}, dataInserimento:${params.dataInserimento}, premioLordo:${params.premioLordo}, provvDealer:${params.provvDealer}, provvGmfi:${params.provvGmfi}, provvVenditore:${params.provvVenditore}  ]", operazione: "creazione nuova polizza cash", pagina: "INSERISCE POLIZZA RCA")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                bindData(polizza, params, [exclude: [ 'categoriaveicolo','provImmatricolazione','durata','daChiamare','causaleContatto','contattare','primaImmatr','stato','quintali','cavalli','kw','tipoCliente','dealer','cilindrata','coperturaRichiesta','dataScadenza','valoreAssicurato','premioImponibile','premioLordo','provvDealer','provvGmfi','provvVenditore','tipoPolizza','telefono','targa','telaio','rappel','imposte','codCampagna','step']])
                def durata=12
                def self=params.nome.toString()
                def dataScadenza
                dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +12.months}:""

                //def copertura=params.coperturaRichiesta
                def coperturaRichiesta="F"

                def telefono= params.telefono.toString().trim().replaceAll("[^0-9]+","")
                def cellulare= params.cellulare.toString().trim().replaceAll("[^0-9]+","")
                //conversione valori decimali
                def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
                String pattern = "#,##0.0#"
                symbols.setGroupingSeparator((char) ',')
                symbols.setDecimalSeparator((char) '.')
                def  decimalFormat = new DecimalFormat(pattern, symbols)
                decimalFormat.setParseBigDecimal(true)
                polizza.tariffa=2
                polizza.dealer=dealer
                polizza.dataScadenza=dataScadenza
                polizza.coperturaRichiesta=coperturaRichiesta
                polizza.durata=12
                polizza.telefono=telefono
                polizza.cellulare=cellulare
                polizza.targa=params.targa?params.targa.toString().trim().toUpperCase():null
                polizza.telaio=params.telaio?params.telaio.toString().trim().toUpperCase():null
                polizza.codOperazione="0"
                polizza.quintali=params.quintali?Integer.parseInt(params.quintali):0
                polizza.kw=params.kw?Integer.parseInt(params.kw):0
                polizza.cilindrata=params.cilindrata?Integer.parseInt(params.cilindrata):0
                polizza.cavalli=params.cavalli?Integer.parseInt(params.cavalli):0
                if ((params.targa1a !='' && params.targa1a!=null )){
                    polizza.targa1a=params.targa1a.toString().trim().toUpperCase()
                }else{
                    polizza.targa1a=null
                }
                if ((params.primaImmatr !='' && params.primaImmatr )&& params.primaImmatr=="true"){
                    polizza.primaImmatr=true
                }else{
                    polizza.primaImmatr=false
                }
                def valoreAssicurato
                polizza.modello=params.modello?params.modello.toString().trim().toUpperCase():null
                def dealerid=Long.parseLong(params.codice.id)
                if(!polizza.getId() || params.tipoPolizza=="CASH"){
                    polizza.dacsv=false
                    def premioImponibile=params.premioImponibile ? (BigDecimal) decimalFormat.parse(params.premioImponibile):0.0
                    def premioLordo=params.premioLordo? (BigDecimal) decimalFormat.parse(params.premioLordo):0.0
                    valoreAssicurato = (BigDecimal) decimalFormat.parse(params.valoreAssicurato)
                    def provvDealer=params.provvDealer ? (BigDecimal) decimalFormat.parse(params.provvDealer):0.0
                    def provvGmfi=params.provvGmfi ? (BigDecimal) decimalFormat.parse(params.provvGmfi):0.0
                    def provvVenditore=params.provvVenditore ? (BigDecimal) decimalFormat.parse(params.provvVenditore):0.0
                    def rappel= params.rappel? (BigDecimal) decimalFormat.parse(params.rappel):0.0
                    def imposte=params.imposte ? (BigDecimal) decimalFormat.parse(params.imposte):0.0
                    polizza.codCampagna="0"
                    polizza.codiceZonaTerritoriale=params.codiceZonaTerritoriale
                    if(params.categoriaveicolo!=null && params.categoriaveicolo!=''){
                        polizza.categoriaveicolo=params.categoriaveicolo
                    }

                    polizza.step="step 1"
                    polizza.premioLordo=premioLordo
                    polizza.premioImponibile=premioImponibile
                    polizza.provvDealer=provvDealer
                    polizza.provvGmfi=provvGmfi
                    polizza.provvVenditore=provvVenditore
                    polizza.rappel=rappel
                    polizza.imposte=imposte

                    polizza.valoreAssicurato=valoreAssicurato
                    polizza.dataInserimento=new Date()
                    if(params.tipoPolizza=="CASH"){
                        polizza.tipoPolizza=TipoPolizza.CASH
                        polizza.dSlip=null
                    }else if(params.tipoPolizza=="LEASING") {
                        polizza.tipoPolizza=TipoPolizza.LEASING
                        polizza.dSlip="S"
                    }else if(params.tipoPolizza=="FINANZIATE") {
                        polizza.tipoPolizza=TipoPolizza.FINANZIATE
                        polizza.dSlip="S"
                    }
                    if(params.tipoCliente=="M"){
                        polizza.tipoCliente=TipoCliente.M
                    }else if(params.tipoCliente=="F") {
                        polizza.tipoCliente=TipoCliente.F
                    }else  {
                        polizza.tipoCliente=TipoCliente.M
                    }
                }
                println "categoria veicolo ${params.categoriaveicolo}"
                if(params.categoriaveicolo!=null && params.categoriaveicolo!=''){
                    //println "sono qui?"
                    polizza.categoriaveicolo=params.categoriaveicolo
                }
                polizza.provImmatricolazione=params.provImmatricolazione
                if ((params.daChiamare !='' && params.daChiamare )&& params.daChiamare=="true"){
                    polizza.stato=StatoPolizza.DA_CHIAMARE
                    polizza.daChiamare=true
                    if(params.causaleContatto==CausaleContatto.DATO_MANCANTE.toString()){
                        polizza.causaleContatto=CausaleContatto.DATO_MANCANTE
                    }else if(params.causaleContatto==CausaleContatto.ERRORE_ANIA.toString()){
                        polizza.causaleContatto=CausaleContatto.ERRORE_ANIA
                    }
                    if(params.contattare==Contattare.CLIENTE.toString()){
                        polizza.contattare=Contattare.CLIENTE
                    }else if(params.contattare==Contattare.CONCESSIONARIO.toString()){
                            polizza.contattare=Contattare.CONCESSIONARIO
                        }

                }else if(params.dataAnnullamento) {
                    if(params.motivoAnnulla=="ANNULLATA"){
                        polizza.stato=StatoPolizza.ANNULLATA
                        polizza.motivoAnnulla=MotivoAnnulla.ANNULLATA
                        polizza.dataAnnullamento=Date.parse("dd-MM-yyyy", params.dataAnnullamento)
                        polizza.daChiamare=false
                        polizza.causaleContatto=null
                        polizza.contattare=null

                    }else if (params.motivoAnnulla=="ANNULLATA_PER_FURTO"){
                        polizza.stato=StatoPolizza.ANNULLATA_FURTO
                        polizza.motivoAnnulla=MotivoAnnulla.ANNULLATA_PER_FURTO
                        polizza.dataAnnullamento=Date.parse("dd-MM-yyyy", params.dataAnnullamento)
                        polizza.daChiamare=false
                        polizza.causaleContatto=null
                        polizza.contattare=null
                    }
                }
                else{
                    polizza.stato=StatoPolizza.RICHIESTA_INVIATA
                    polizza.causaleContatto=null
                    polizza.contattare=null
                    polizza.daChiamare=false
                }
                logg =new Log(parametri: "CAMPI PASSATI A POLIZZA: iscash-->${polizza.isCash} bonus-> ${polizza.bonus} cavalli-> ${polizza.cavalli} codcampagna-> ${polizza.codCampagna} contattare-> ${polizza.contattare}" +
                        " causale contatto->${polizza.causaleContatto} cell-> ${polizza.cellulare} cilindrata ${polizza.cilindrata} cap ${polizza.cap} cognome->${polizza.cognome} copertura richiesta-> ${polizza.coperturaRichiesta}" +
                        " cod operazione->${polizza.codOperazione} cod zona territoriale->${polizza.codiceZonaTerritoriale} dealer-> ${polizza.dealer} desruso->${polizza.destUso} durata-> ${polizza.durata} datascadenza->${polizza.dataScadenza}" +
                        " data decorrenza->${polizza.dataDecorrenza} data immatricolazione-> ${polizza.dataImmatricolazione} email-->${polizza.email} emaildealer->${polizza.emailDealer} hascvt->${polizza.hasCVT}" +
                        " indirizzo ->${polizza.indirizzo} imposte-> ${polizza.imposte} kw ->${polizza.kw} localita-> ${polizza.localita} modello-> ${polizza.modello} marca->${polizza.marca} no polizza ${polizza.noPolizza}" +
                        " identificativo->${polizza.identificativo}  nome->${polizza.nome} nuovo-> ${polizza.nuovo} onstar-> ${polizza.onStar} prima imma ->${polizza.primaImmatr} provv->${polizza.provvVenditore}" +
                        " provv gmfi->${polizza.provvGmfi} provv dealer->${polizza.provvDealer} provincia-> ${polizza.provincia} premio imponibile-> ${polizza.premioImponibile}" +
                        " premio lordo->${polizza.premioLordo} partita iva ->${polizza.partitaIva}  quintlali ->${polizza.quintali} rappel->${polizza.rappel} stato-> ${polizza.stato} step->${polizza.step}" +
                        " targa1a ->${polizza.targa1a} targa->${polizza.targa} tariffa->${polizza.tariffa} tipo polizza->${polizza.tipoPolizza} tipoveicolo->${polizza.tipoVeicolo} tipocliente-> ${polizza.tipoCliente}" +
                        " dacsv-> ${polizza.dacsv}  " +
                        " telaio-> ${polizza.telaio} telefono -> ${polizza.telefono} versione ->${polizza.versione} valore->${polizza.valoreAssicurato} venditore->${polizza.venditore} " , operazione: "creazione nuova polizza RCA", pagina: "INSERISCE POLIZZA RCA")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(polizza.save(flush:true)) {
                    if( polizza.identificativo ==null){
                        def iden=caricamentiService.generaIdentificativo(polizza)
                        //println "la polizza viene salvata nel DB ${iden}"
                        polizza.identificativo=  iden
                        if (polizza.save(flush: true)) {
                            logg =new Log(parametri: "la polizza viene salvata nel DB iden ${iden} -->", operazione: "creazione identificativo polizza RCA CASH", pagina: "INSERISCE POLIZZA RCA CASH")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }
                    }
                    println "${polizza.stato}"
                    if(polizza.stato!=StatoPolizza.DA_CHIAMARE && polizza.stato!=StatoPolizza.ANNULLATA_FURTO && polizza.stato!=StatoPolizza.ANNULLATA && polizza.stato!=StatoPolizza.POLIZZA){

                        def tracciatoDLRCA= tracciatiService.generaFileDL(polizza)
                        if(!tracciatoDLRCA){
                            logg =new Log(parametri: "Si è verificato un errore durante l'invio del tracciato DL ", operazione: "creazione nuova polizza RCA", pagina: "INSERISCE POLIZZA RCA")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            flash.error= "Si è verificato un errore durante l'invio del tracciato DL"
                        } else{
                            logg =new Log(parametri: "Richiesta inviata ", operazione: "aggiornamento polizza RCA", pagina: "INSERISCE POLIZZA RCA")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        }
                        if(!polizza.isCash){
                            def polizzacvt=Polizza.findByNoPolizza(polizza.noPolizza)
                            if(polizzacvt){
                                if(!polizza.hasCVT){
                                    polizza.hasCVT=true
                                    if(!polizzacvt.save(flush:true)) {
                                        logg =new Log(parametri: "errore polizza RCA aggiornata con flag cvt uguale a true", operazione: "aggiornamento  polizza CVT con RCA", pagina: "INSERISCE POLIZZA RCA")
                                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    }
                                }
                                polizzacvt.nome=params.nome?.toString().toUpperCase()
                                polizzacvt.cognome=params.cognome?.toString().toUpperCase()
                                polizzacvt.partitaIva=params.partitaIva?.toString().toUpperCase()
                                polizzacvt.indirizzo=params.indirizzo?.toString().toUpperCase()
                                polizzacvt.localita=params.localita?.toString().toUpperCase()
                                polizzacvt.cap=params.cap?.toString().toUpperCase()
                                polizzacvt.provincia=params.provincia?.toString().toUpperCase()
                                polizzacvt.targa=params.targa?.toString().toUpperCase()
                                polizzacvt.dataDecorrenza=polizza.dataDecorrenza
                                polizzacvt.dataScadenza=use(TimeCategory) { polizza.dataDecorrenza + polizzacvt.durata.months}
                                if(polizzacvt.save(flush:true)) {
                                    logg =new Log(parametri: "polizza CVT aggiornata", operazione: "aggiornamento  polizza CVT con RCA", pagina: "INSERISCE POLIZZA RCA")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                    flash.message="Richiesta inviata"
                                    redirect action: "listaRCA", params: params.search ? [search: params.search] : [:]

                                }else{
                                    flash.error ="la polizza CVT non e' stata aggiornata ${renderErrors(bean: polizzacvt)}"
                                    logg =new Log(parametri: "Errore aggiornamento polizza cvt ${polizzacvt.noPolizza}-> ${renderErrors(bean: polizzacvt)}", operazione: "aggiorna polizza cvt", pagina: "INSERISCE POLIZZA RCA")
                                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                }
                            }else{
                                logg =new Log(parametri: "non c'e polizza cvt abbinata a questa polizza RCA ${polizza.noPolizza}", operazione: "aggiornamento polizza RCA ", pagina: "salva POLIZZA RCA")
                                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                                flash.message="Richiesta inviata"
                                redirect action: "listaRCA", params: params.search ? [search: params.search] : [:]
                            }
                        }else{
                            logg =new Log(parametri: "POLIZZA CASH RCA salvata", operazione: "aggiornamento polizza CASH RCA ", pagina: "salva POLIZZA RCA")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            flash.message="Richiesta inviata"
                            redirect action: "listaRCA", params: params.search ? [search: params.search] : [:]
                        }
                    }else{
                        if(polizza.stato==StatoPolizza.DA_CHIAMARE){
                            flash.message="polizza da chiamare"
                        }else if(polizza.stato==StatoPolizza.POLIZZA){
                            flash.message="polizza Attivata"
                        }else if(polizza.stato==StatoPolizza.ANNULLATA || polizza.stato==StatoPolizza.ANNULLATA_FURTO){
                            flash.message="polizza annullata"
                        }
                        else{
                            flash.message="Richiesta inviata"
                        }
                        redirect action: "listaRCA", params: params.search ? [search: params.search] : [:]
                    }
                } else {
                    flash.error = renderErrors(bean: polizza)
                    logg =new Log(parametri: "Errore creazione polizza-> ${renderErrors(bean: polizza)}", operazione: "creazione nuova polizza RCA", pagina: "INSERISCE POLIZZA RCA")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }
        }
         [polizza: polizza, province: province, pagina:pagina ]
    }
    def aggiornaPolizza( stato,  idPolizza,  dataDecorrenza,  targa,  premioImponibile,  premioLordo,  provvDealer, provvgmfi, provvVend, provvMansutti, provvMach1,rappel, imposte,  nome,  cognome,  indirizzo,  cap,  localita,  telefono,  cellulare,  provincia,  dealerId,  partitaIva,  durata,  valoreAssicurato, copertura,  telaio,  tipoCliente,  email,  marca,  modello,  bonus,  dataImmatricolazione){
        def logg
        def aggiornata=false
        if(PolizzaRCA.exists(idPolizza)) {
            def polizza=PolizzaRCA.get(idPolizza)
            /***data scadenza*/
            //Date.parse("dd-MM-yyyy", params.dataDecorrenza)
            //def dataScadenza= use(TC) { Date.parse("dd-MM-yyyy", params.dataDecorrenza) + 12.months}
            //def dataScadenza=params.dataDecorrenza ? use(TC) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +12.months}:""
            def dataScadenza=dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", dataDecorrenza) +12.months}:""
            if(dataScadenza!=""){
                GregorianCalendar calendar = new GregorianCalendar()
                def yearInt = Integer.parseInt(dataScadenza.format("yyyy"))
                def monthInt = Integer.parseInt(dataScadenza.format("MM"))
                monthInt = monthInt - 1
                calendar.set(yearInt, monthInt, 1)
                def dayInt = calendar.getActualMaximum(GregorianCalendar.DAY_OF_MONTH)
                dataScadenza=new GregorianCalendar(yearInt,monthInt,dayInt).format("dd-MM-yyyy")
                Date dataFormatata = Date.parse( "dd-MM-yyyy", dataScadenza )
                polizza.dataScadenza=dataFormatata
            }

            //polizza.premioImponibile=premioImponibile
            //polizza.premioLordo=premioLordo
            //polizza.provvDealer=provvDealer
            //polizza.provvGmfi=provvgmfi
            //polizza.provvVenditore=provvVend
            // bindData(polizza, params, [exclude: ['indirizzo', 'cap','localita','telefono','email','partitaIva','nome','cognome','provincia','tipoCliente']])
            polizza.targa=targa.toString().toUpperCase()
            polizza.dataDecorrenza=Date.parse("dd-MM-yyyy", dataDecorrenza)
            polizza.imposte=imposte
            polizza.rappel=rappel
            if(stato.toString().trim().contains("preventivo")){
                polizza.stato=StatoPolizza.PREVENTIVO
            }else if (stato.toString().trim().contains("richiesta inviata")){
                polizza.stato=StatoPolizza.RICHIESTA_INVIATA
            }else if (stato.toString().trim().contains("polizza")){
                polizza.stato=StatoPolizza.POLIZZA
            }else if (stato.toString().trim().contains("annullata")){
                polizza.stato=StatoPolizza.ANNULLATA
            }else if (stato.toString().trim().contains("da chiamare")){
                polizza.stato=StatoPolizza.DA_CHIAMARE
            }
            if(polizza.save(flush:true)){
                logg =new Log(parametri: "PolizzaRCA salvata", operazione: "aggiornamento polizza", pagina: "AGGIORNA POLIZZA")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    logg =new Log(parametri: "RICHIESTA_INVIATA", operazione: "aggiornamento polizza", pagina: "AGGIORNA POLIZZA")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    /*if(polizza.tracciato!=null){
                        polizza.tracciato.delete()
                        def tracciatoPolizza= tracciatiService.generaTracciatoIAssicurRCA(polizza)
                        if(!tracciatoPolizza.save(flush: true)){
                            logg =new Log(parametri: "errore generazione tracciato ${renderErrors(bean: tracciatoPolizza)}", operazione: "aggiornamento tracciato aggiornamento polizza", pagina: "AGGIORNA POLIZZA")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            aggiornata=false
                        } else{
                            logg =new Log(parametri: " generazione tracciato", operazione: "aggiornamento tracciato aggiornamento polizza", pagina: "AGGIORNA POLIZZA")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            aggiornata=true
                        }
                        aggiornata=true
                    }else{
                        def tracciatoPolizza= tracciatiService.generaTracciatoIAssicurRCA(polizza)
                        if(!tracciatoPolizza.save(flush: true)){
                            logg =new Log(parametri: "errore generazione tracciato ${renderErrors(bean: tracciatoPolizza)}", operazione: " tracciato aggiornamento polizza", pagina: "INSERISCE POLIZZA")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            aggiornata=false
                        } else{
                            logg =new Log(parametri: " generazione tracciato", operazione: "aggiornamento tracciato aggiornamento polizza", pagina: "INSERISCE POLIZZA")
                            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                            aggiornata=true
                        }
                        aggiornata=true
                    }*/
            }else {
                logg =new Log(parametri: "errore aggiornamento polizza ${renderErrors(bean: polizza)}", operazione: " aggiornamento polizza", pagina: "AGGIORNA POLIZZA")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                println "errori-->${renderErrors(bean: polizza)}"
                aggiornata=false
            }
            return aggiornata
            //redirect action: "listaRCA", params: params.search ? [search: params.search] : [:]
        }else {
            def polizza=new PolizzaRCA()
            //def dealerId=params.codice.id
            def dealer =  Dealer.get(dealerId)
            nome=nome.toString().toUpperCase()
            cognome=cognome.toString().toUpperCase()
            partitaIva=partitaIva.toString().toUpperCase()
            logg =new Log(parametri: "parametri inserimento:[dealer:${dealerId},  premioImponibile:${premioImponibile},  cognome:${cognome} , nome:${nome}, partitaIva:${partitaIva}, indirizzo:${indirizzo}, " +
                    "localita:${localita}, cap:${cap}, tipoCliente:${tipoCliente}, provincia:${provincia}, telefono:${telefono?telefono:""}, email:${email ? email :  ""}, " +
                    "marca: ${marca}, modello: ${modello}, targa: ${targa? targa:""},  bonus:${bonus}, telaio: ${telaio?telaio:""}, valoreAssicurato:${valoreAssicurato}, " +
                    "dataImmatricolazione:${dataImmatricolazione},  coperturaRichiesta:${copertura}, durata:${durata}, dataDecorrenza:${dataDecorrenza}, premioLordo:${premioLordo}, provvGmfi:${provvgmfi}, " +
                    "provvVenditore:${provvVend}  ]", operazione: "creazione nuova polizza RCA", pagina: "INSERISCE POLIZZA RCA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def self=nome.toString()
            def dataScadenza
            if(durata=="12"){
                dataScadenza=dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", dataDecorrenza) +12.months}:""
            }else if( durata=="18"){
                dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +18.months}:""
            }else if( durata=="24"){
                dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +24.months}:""
            }else if(durata=="30"){
                dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +30.months}:""
            }else if(durata=="36"){
                dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +36.months}:""
            }else if(durata=="42"){
                dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +42.months}:""
            }else if(durata=="48"){
                dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +48.months}:""
            }else if(durata=="54"){
                dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +54.months}:""
            }else if(durata=="60"){
                dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +60.months}:""
            }else if(durata=="66"){
                dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +66.months}:""
            }else if(durata=="72"){
                dataScadenza=params.dataDecorrenza ? use(TimeCategory) {Date.parse("dd-MM-yyyy", params.dataDecorrenza) +72.months}:""
            }
            def coperturaRichiesta=""
            if(copertura=='silver'){
                coperturaRichiesta="SILVER"
            }else if(copertura=='gold'){
                coperturaRichiesta="GOLD"
            }else if(copertura=='platinum'){
                coperturaRichiesta="PLATINUM"
            }else if(copertura=='platinumK'){
                coperturaRichiesta="PLATINUM KASKO"
            }else if(copertura=='platinumC'){
                coperturaRichiesta="PLATINUM COLLISIONE"
            }
             telefono= telefono.toString().replaceAll("[^0-9]+","")
            //conversione valori decimali
            def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
            String pattern = "#,##0.0#"
            symbols.setGroupingSeparator((char) ',')
            symbols.setDecimalSeparator((char) '.')
            def  decimalFormat = new DecimalFormat(pattern, symbols)
            decimalFormat.setParseBigDecimal(true)
             valoreAssicurato = (BigDecimal) decimalFormat.parse(valoreAssicurato)
             premioImponibile=(BigDecimal) decimalFormat.parse(premioImponibile)
             premioLordo=(BigDecimal) decimalFormat.parse(premioLordo)
             provvDealer=(BigDecimal) decimalFormat.parse(provvDealer)
            provvgmfi=(BigDecimal) decimalFormat.parse(provvgmfi)
            provvVend=(BigDecimal) decimalFormat.parse(provvVend)
            polizza.dealer=dealer
            polizza.nome=nome
            polizza.cognome=cognome
            polizza.provincia=provincia
            polizza.dataScadenza=dataScadenza
            polizza.coperturaRichiesta=coperturaRichiesta
            polizza.valoreAssicurato=valoreAssicurato
            polizza.premioImponibile=premioImponibile
            polizza.provvDealer=provvDealer
            polizza.provvGmfi=provvgmfi
            polizza.provvVenditore=provvVend
            polizza.premioLordo=premioLordo
            //polizza.tipoPolizza=TipoPolizza.RCA
            polizza.telefono=telefono
            polizza.targa=targa?targa.toString().toUpperCase():null
            polizza.telaio=telaio?telaio.toString().toUpperCase():null
            polizza.rappel=rappel
            polizza.imposte=imposte
            polizza.codOperazione="0"
            if(polizza.save(flush:true)) {
                /*def tracciatoIassicur= tracciatiService.generaTracciatoIAssicurRCA(polizza)
                if(!tracciatoIassicur.save(flush: true)){
                    logg =new Log(parametri: "Si è verificato un errore durante la creazione del tracciato IAssicur ${renderErrors(bean: tracciatoIassicur)} ", operazione: "creazione nuova polizza RCA", pagina: "INSERISCE POLIZZA RCA")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    flash.error = renderErrors(bean: tracciatoIassicur)
                    redirect action: "listaRCA"
                } else{
                    def tracciatoDLRCA= tracciatiService.generaTracciatoDLRCA(polizza)
                    if(!tracciatoDLRCA.save(flush: true)){
                        logg =new Log(parametri: "Si è verificato un errore durante la creazione del tracciato DL ${renderErrors(bean: tracciatoDLRCA)} ", operazione: "creazione nuova polizza RCA", pagina: "INSERISCE POLIZZA RCA")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        flash.error = renderErrors(bean: tracciatoDLRCA)
                        redirect action: "listaRCA"
                    } else{
                        /* def invioMail=mailService.invioMail(polizza)
                         if(invioMail){
                             flash.message="Richiesta inviata e mail inviata al dealer ${polizza.dealer.ragioneSocialeD} \n e al sales specialist ${polizza.dealer.emailSales}"
                             redirect action: "listaRCA", params: params.search ? [search: params.search] : [:]
                         }else{
                             flash.error = "c\u00E8 stato un errore con l\u00E8 invio della mail al dealer"
                             redirect action: "listaRCA", params: params.search ? [search: params.search] : [:]
                         }*/
                        logg =new Log(parametri: "Richiesta inviata ", operazione: "creazione nuova polizza RCA", pagina: "INSERISCE POLIZZA RCA")
                        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                        if(polizza.stato==StatoPolizza.POLIZZA){
                            flash.message="polizza Attivata"

                        }else{
                            flash.message="Richiesta inviata"

                        }
                        redirect action: "listaRCA", params: params.search ? [search: params.search] : [:]
                  //  }
                //}
            } else {
                flash.error = renderErrors(bean: polizza)
                logg =new Log(parametri: "Errore creazione polizza-> ${renderErrors(bean: polizza)}", operazione: "creazione nuova polizza RCA", pagina: "INSERISCE POLIZZA RCA")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            }
            logg =new Log(parametri: "errore aggironamento polizza, la polizza non esiste nel DB controllare, id polizza = $idPolizza ", operazione: " aggiornamento polizza", pagina: "AGGIORNA POLIZZA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            aggiornata=false
        }// response.sendError(404)
    }
    def chiamataWS(){
        def bottoneChiamata=params.bottone
        def logg
        def symbols = new DecimalFormatSymbols(locale: Locale.ITALY)
        String pattern = "#,##0.0#"
        symbols.setGroupingSeparator((char) ',')
        symbols.setDecimalSeparator((char) '.')
        def  decimalFormat = new DecimalFormat(pattern, symbols)
        decimalFormat.setParseBigDecimal(true)
        def localita=params.localita.toString().toUpperCase()
        def test="N"
        def pacchetto="RC000"
        def dealerid
        def idPolizza, percVenditore,valoreAssicurato,provincia,versione,targa, modello, dataEffetto, polizzaTipo,paymenttype

        if(params){
            if(params.dealerid){
                dealerid=Dealer.get(Long.parseLong(params.dealerid))
            }
            percVenditore=dealerid.percVenditore
           valoreAssicurato=(BigDecimal) decimalFormat.parse(params.valoreAssicurato)
            provincia=params.provincia
            localita=params.localita
            versione=params.versione
            targa=params.targa
            modello=params.modello
            polizzaTipo=params.polizzaTipo
            paymenttype="Leasing"
            if(polizzaTipo != TipoPolizza.LEASING.toString()){
                paymenttype="Funded"
            }
            if (modello.toString().trim().toUpperCase().contains("MOKKA X")){modello= modello.toString().replaceAll("X","").trim()}
            dataEffetto=new Date().parse("dd-MM-yyyy", params.dataDecorrenza).format("yyyy-MM-dd")
            if(params.idPolizza){
                idPolizza=Long.parseLong(params.idPolizza)
            }

        }

        /*if(Environment.current != Environment.DEVELOPMENT) {
            test="N"
        }else{
            test="S"
        }*/
        boolean risposta=true
        def stringaresponse=""
        def errore=[]
        stringaresponse=[risposta:risposta]
        //println "arrivo qui $bottoneChiamata --"
        def parametriaggiornamento=[]
        parametriaggiornamento=[
                modello:modello,
                percVenditore:dealerid,
                polizzaaggiornata:false,
                valoreAssicurato:valoreAssicurato,
                provincia:provincia,
                comune:localita,
                targa:targa,
        ]
        def parameters=[]
        parameters  =[
                codicepacchetto: "${pacchetto}",
                valoreassicurato: valoreAssicurato,
                provincia: provincia,
                comune: localita,
                dataeffettocopertura: dataEffetto,
                vehiclemodel: modello,
                vehicleversion: versione,
                test:test,
                durata:12,
                percentualeVenditore:percVenditore,
                paymenttype:paymenttype
        ]
        logg =new Log(parametri: "parametri inviate al webservice->${parameters}", operazione: "chiamata ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza RCA")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        println "parametri: ${parameters} \n"
        def url = "http://gmf.mach-1.it"

        def js = (parameters as JSON).toString()
        js = js.substring(1, js.length()-1)
        def path = "/calcprovvigioni/"
        /*if(Environment.current != Environment.PRODUCTION ){
            path = "/test/calcprovvigioni/"

        }else {*/
            path = "/calcprovvigioni/"
       // }
        logg =new Log(parametri: "il path che viene chiamato->${path}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza RCA")
        if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
        def query = "{"+js+"}"
        def esito, provvDealer,provvGmfi,provvVenditore,premioLordo,premioImponibile,zona,rappel,imposte,categoriaveicolo,provvMach1

        def response = DLWebService.postText(url, path, [p:query])
        JSONObject userJson = JSON.parse(response)
        userJson.each { id, data ->
            logg =new Log(parametri: "dati webservice->${data}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza RCA")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if(data.Errors?.descErrore){
                //println "entro qui negli errori ${data.Errors.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")}"
                //def desErrore=data.Errors?.desc_Errore?.toString().replaceAll("\\[","").replaceAll("]","")
                def desErrore=data.Errors?.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")
                logg =new Log(parametri: "errore riportato dal webservice->${desErrore}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizze")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                if(!((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con")))){
                    risposta=false
                    errore = data.Errors?.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")
                }else if (((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con"))) &&!( desErrore.contains("Provincia non valida"))){
                    risposta=true
                }
                else if (((desErrore.contains("Comune non identificato")) || (desErrore.contains("Data Effetto non coerente con"))) && desErrore.contains("Provincia non valida")){
                    risposta=false
                    errore = data.Errors?.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")
                }
                else{
                    risposta=false
                    errore = data.Errors?.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")
                    esito=data.Esito
                }
            }
            else{
               // println "${data.Esito}"
                if(data.Esito==0){
                    risposta=true
                }else{
                    risposta=false
                    errore=data.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")
                }
            }
            logg =new Log(parametri: "valore parametro risposta->${risposta}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            if(risposta){
                logg =new Log(parametri: "valori data --> ${data.toString().replaceAll("\\[","").replaceAll("]","").replaceAll("\\{","").replaceAll("}","")}", operazione: "chiamata  ws", pagina: "INSERIMENTO/MODIFICA/ANNULLAMENTI polizza")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"

                esito=data.Esito? 0:1
               // println "questo è data-->>>>>>${data}"
                premioLordo=(BigDecimal) decimalFormat.parse(data?.PremioLordo.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
                premioImponibile=(BigDecimal) decimalFormat.parse(data?.Premio.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
                provvDealer=(BigDecimal) decimalFormat.parse(data?.provvigioneDealer.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
                provvGmfi=(BigDecimal) decimalFormat.parse(data?.provvigioneGMF.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
                provvVenditore=(BigDecimal) decimalFormat.parse(data?.provvigioneVenditore.toString().replaceAll("\\[","").replaceAll("]","") )?:0.0
               //provvMansutti=/*data.Tariffazione?.IPRPremioCalcolato.provvigioneMansutti ?:*/0.0
               // provvMach1=/*data.Tariffazione?.IPRPremioCalcolato.provvigioneMach1 ?:*/0.0
                zona=data.zona.toString().replaceAll("\\[","").replaceAll("]","")?:""
                categoriaveicolo=data.categoriaveicolo.toString().replaceAll("\\[","").replaceAll("]","")?:""
                rappel=(BigDecimal) decimalFormat.parse(data?.rappel.toString().replaceAll("\\[","").replaceAll("]","")) ?:0.0
                imposte=(BigDecimal) decimalFormat.parse(data?.imposte.toString().replaceAll("\\[","").replaceAll("]","")) ?:0.0
                stringaresponse=[risposta:risposta,esito:esito,premioLordo:premioLordo,premioImponibile:premioImponibile, imposte:imposte, provvDealer:provvDealer,provvGmfi:provvGmfi,provvVenditore:provvVenditore, rappel:rappel,zona:zona, categoria:categoriaveicolo, bottone:"chiamata"]

            }else{
                stringaresponse=[risposta:risposta,esito:esito,errore:errore,premioLordo:premioLordo,premioImponibile:premioImponibile,provvDealer:provvDealer,provvgmfi:provvGmfi,provvVenditore:provvVenditore, rappel:rappel,zona:zona, categoria:categoriaveicolo,]
            }
        }

        errore=[]
        //println stringaresponse
        render stringaresponse as JSON
    }
    def generaFilePolizzeattivate(){

        def polizze=PolizzaRCA.createCriteria().list(){
             ne "stato", StatoPolizza.DA_CHIAMARE
        }

        def wb = Stopwatch.log { ExcelBuilder.create {
            style("header") {
                background bisque
                font {
                    bold(true)
                }
            }
            sheet("Polizze attivate") {
                row(style: "header") {
                    cell("Tipo Polizza")
                    cell("Dealer")
                    cell("Telefono Dealer")
                    cell("Mail Dealer")
                    cell("Numero Pratica")
                    cell("Nome Contraente")
                    cell("Cognome Contraente")
                    cell("PartitaIva Contraente")
                    cell("Marca")
                    cell("Modello")
                    cell("Targa")
                    cell("Telaio")
                    cell("Valore Assicurato")
                    cell("Data Immatricolazione")
                    cell("Data Decorrenza")
                    cell("Data Scadenza")
                    cell("Stato pratica")

                }
                if(polizze){
                    polizze.each { polizza ->
                        row {
                            cell(polizza.tipoPolizza)
                            cell(polizza.dealer.ragioneSocialeD)
                            cell(polizza.dealer.telefonoD)
                            cell(polizza.dealer.emailD?polizza.dealer.emailD.toString().trim():"")
                            cell(polizza.noPolizza)
                            cell(polizza.nome)
                            cell(polizza.cognome)
                            cell(polizza.partitaIva)
                            cell(polizza.marca)
                            cell(polizza.modello)
                            cell(polizza.targa!=null?polizza.targa.toString().toUpperCase():"")
                            cell(polizza.telaio!=null?polizza.telaio.toString().toUpperCase():"")
                            cell(polizza.valoreAssicurato)
                            cell(polizza.dataImmatricolazione)
                            cell(polizza.dataDecorrenza)
                            cell(polizza.dataScadenza)
                            cell(polizza.stato.toString().toUpperCase())
                        }
                        polizza.discard()
                    }
                    for (int i = 0; i < 17; i++) {
                        sheet.autoSizeColumn(i)
                    }
                }else{
                    row{
                        cell("non ci sono polizze attivate")
                    }
                }

            }

        } }

        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        response.addHeader "Content-disposition", "inline; filename=polizze_${new Date().format("ddMMyyyy")}.xlsx"
        response.outputStream << stream.toByteArray()
    }
    def generaFilePolizzedachiamare(){

        def polizze=PolizzaRCA.createCriteria().list(){
             eq "stato", StatoPolizza.DA_CHIAMARE
        }

        def wb = Stopwatch.log { ExcelBuilder.create {
            style("header") {
                background bisque
                font {
                    bold(true)
                }
            }
            sheet("Polizze da chiamare") {
                row(style: "header") {
                    cell("Tipo Polizza")
                    cell("Dealer")
                    cell("Telefono Dealer")
                    cell("Mail Dealer")
                    cell("Numero Pratica")
                    cell("Nome Contraente")
                    cell("Cognome Contraente")
                    cell("PartitaIva Contraente")
                    cell("Marca")
                    cell("Modello")
                    cell("Targa")
                    cell("Telaio")
                    cell("Valore Assicurato")
                    cell("Data Immatricolazione")
                    cell("Data Decorrenza")
                    cell("Data Scadenza")
                    cell("Stato pratica")

                }
                if(polizze){
                    polizze.each { polizza ->
                        row {
                            cell(polizza.tipoPolizza)
                            cell(polizza.dealer.ragioneSocialeD)
                            cell(polizza.dealer.telefonoD)
                            cell(polizza.dealer.emailD?polizza.dealer.emailD.toString().trim():"")
                            cell(polizza.noPolizza)
                            cell(polizza.nome)
                            cell(polizza.cognome)
                            cell(polizza.partitaIva)
                            cell(polizza.marca)
                            cell(polizza.modello)
                            cell(polizza.targa!=null?polizza.targa.toString().toUpperCase():"")
                            cell(polizza.telaio!=null?polizza.telaio.toString().toUpperCase():"")
                            cell(polizza.valoreAssicurato)
                            cell(polizza.dataImmatricolazione)
                            cell(polizza.dataDecorrenza)
                            cell(polizza.dataScadenza)
                            cell(polizza.stato.toString().toUpperCase())
                        }
                        polizza.discard()
                    }
                    for (int i = 0; i < 17; i++) {
                        sheet.autoSizeColumn(i)
                    }
                }else{
                    row{
                        cell("non ci sono polizze attivate")
                    }
                }

            }

        } }

        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        response.addHeader "Content-disposition", "inline; filename=polizze_${new Date().format("ddMMyyyy")}.xlsx"
        response.outputStream << stream.toByteArray()
    }
    def generaFilePolizzeannullate(){

        def polizze=PolizzaRCA.createCriteria().list(){
            or{
                eq "stato", StatoPolizza.ANNULLATA_FURTO
                eq "stato", StatoPolizza.ANNULLATA

            }
        }

        def wb = Stopwatch.log { ExcelBuilder.create {
            style("header") {
                background bisque
                font {
                    bold(true)
                }
            }
            sheet("Polizze annullate") {
                row(style: "header") {
                    cell("Tipo Polizza")
                    cell("Dealer")
                    cell("Telefono Dealer")
                    cell("Mail Dealer")
                    cell("Numero Pratica")
                    cell("Nome Contraente")
                    cell("Cognome Contraente")
                    cell("PartitaIva Contraente")
                    cell("Marca")
                    cell("Modello")
                    cell("Targa")
                    cell("Telaio")
                    cell("Valore Assicurato")
                    cell("Data Immatricolazione")
                    cell("Data Decorrenza")
                    cell("Data Scadenza")
                    cell("Stato pratica")
                }
                if(polizze){
                    polizze.each { polizza ->
                        row {
                            cell(polizza.tipoPolizza)
                            cell(polizza.dealer.ragioneSocialeD)
                            cell(polizza.dealer.telefonoD)
                            cell(polizza.dealer.emailD?polizza.dealer.emailD.toString().trim():"")
                            cell(polizza.noPolizza)
                            cell(polizza.nome)
                            cell(polizza.cognome)
                            cell(polizza.partitaIva)
                            cell(polizza.marca)
                            cell(polizza.modello)
                            cell(polizza.targa!=null?polizza.targa.toString().toUpperCase():"")
                            cell(polizza.telaio!=null?polizza.telaio.toString().toUpperCase():"")
                            cell(polizza.valoreAssicurato)
                            cell(polizza.dataImmatricolazione)
                            cell(polizza.dataDecorrenza)
                            cell(polizza.dataScadenza)
                            cell(polizza.stato.toString().toUpperCase())
                        }
                        polizza.discard()
                    }
                    for (int i = 0; i < 17; i++) {
                        sheet.autoSizeColumn(i)
                    }
                }else{
                    row{
                        cell("non ci sono polizze attivate")
                    }
                }

            }

        } }

        def stream = new ByteArrayOutputStream()
        stream.write(wb)
        stream.close()
        response.contentType = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"
        response.addHeader "Content-disposition", "inline; filename=polizze_${new Date().format("ddMMyyyy")}.xlsx"
        response.outputStream << stream.toByteArray()
    }
    def aggiornacontattato(){
        def logg
        def stringaresponse=[]
        if(params.idpol){
            logg =new Log(parametri: "polizza trovata, id polizza = $params.idpol ", operazione: " aggiornamento polizza", pagina: "AGGIORNA POLIZZA CONTATTATO")
            if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
            def idpol=Long.parseLong(params.idpol)
            def polizza=PolizzaRCA.findById(idpol)
            polizza.contattato=params.contattato
            if(polizza.save()){
                logg =new Log(parametri: "polizza aggiornata, id polizza = ${polizza.id} ", operazione: " aggiornamento polizza", pagina: "AGGIORNA POLIZZA CONTATTATO")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                stringaresponse=[risposta:true,errore:null]
            }else{
                logg =new Log(parametri: "polizza non aggiornata, errori polizza = ${polizza.errors} ", operazione: " aggiornamento polizza", pagina: "AGGIORNA POLIZZA CONTATTATO")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                stringaresponse=[risposta:false,errore:null]
            }
        }
        render stringaresponse as JSON
    }
    def elencoFlussiIassicur() {
        def traccIassicur = /*SqlLogger.log {*/TracciatoIASSRCA.createCriteria().list() {
            isNull ("dataCaricamento")
        }
        def data=new Date()
        if(traccIassicur) {
            byte[] b = traccIassicur.tracciato.join("\n").getBytes()
            response.contentType = "text/plain"
            response.addHeader "Content-disposition", "attachment; filename=flussi_RCA_${data.format("yyyyMMdd")}.txt"
            response.outputStream << b
        } //else response.sendError(404)
        else{
            flash.error = "Non ci sono flussi RCA  da mostrare"
            redirect action: "listaRCA"
            return
        }
    }
    def generazioneFlussiDL() {
        def logg
        def polizze = /*SqlLogger.log {*/PolizzaRCA.createCriteria().list() {
            eq "stato", StatoPolizza.RICHIESTA_INVIATA
        }
        def data=new Date()
        if(polizze){
            polizze.each { polizza ->
                def tracciatoDLRCA= tracciatiService.generaFileDL(polizza)
                if(!tracciatoDLRCA){
                    logg =new Log(parametri: "Si è verificato un errore durante l'invio del tracciato DL ", operazione: "rigenerazione file csv polizza RCA", pagina: "LISTA POLIZZA RCA")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    flash.error= "Si è verificato un errore durante l'invio del tracciato DL"
                } else{
                    logg =new Log(parametri: "Richiesta inviata ", operazione: "rigenerazione file csv polizza RCA", pagina: "LISTA POLIZZA RCA")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                }
            }
            flash.messagge = "generazione polizze"
            redirect action: "listaRCA"
            return
        }
        else{
            flash.error = "Non ci sono flussi RCA  da mostrare"
            redirect action: "listaRCA"
            return
        }
    }
    def generaofsgen(){
        def logg
        if (request.post) {
            if(params.mese !="" && params.anno !=""){
                try {
                    //println "questi son i parametri ${params}"
                    def mese=params.mese
                    def anno=params.anno
                    GenOFSGENRCAJob.triggerNow([mese:mese,anno:anno])
                    flash.error = "generazione del report OFSGEN, a breve arrivera' una mail con il file"

                }catch(e) {
                    logg =new Log(parametri: "errore try catch ${e.toString()}", operazione: "generazione OFSGEN", pagina: "Polizza RCA")
                    if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                    println e.toString() }
            }else{
                logg =new Log(parametri: "Selezionare un mese per lanciare la generazione del report", operazione: "generazione OFSGEN", pagina: "Polizza RCA")
                if(!logg.save(flush: true)) println "Errori salvataggio log reponse: ${logg.errors}"
                flash.certificati=["Selezionare un mese per lanciare la generazione del report"]
            }

            redirect action: "ofsgen"
        }else response.sendError(404)


    }
}
