package cashopel

class SecurityController {

    def securityService
    def passwordScaduta(String passwordAttuale, String nuovaPassword, String confermaNuovaPassword) {
        if(request.post) {
            def utente = securityService.utente
            if(securityService.passwordMatches(passwordAttuale)) {
                if(nuovaPassword == confermaNuovaPassword) {
                    utente.password = nuovaPassword
                    utente.passwordScaduta = false
                    if(utente.save(flush: true)) {
                        securityService.reauthenticate(utente)
                        redirect uri: "/"
                    }
                } else flash.error = "Le password non corrispondono"
            } else flash.error = "Le credenziali non corrispondono"
        }
        render view: "passwordScaduta"
    }
}
