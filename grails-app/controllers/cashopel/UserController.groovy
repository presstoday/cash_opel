package cashopel

import cashopel.utenti.Dealer
import grails.converters.JSON
import security.SecurityUtils

class UserController {

    def index() {}

    def datiDealer(){
        def principal = SecurityUtils.principal
        def utente
        utente=Dealer.get(principal.id)

        if(request.post) {
            if(SecurityUtils.hasRole("ADMIN")){
                redirect uri:  "/"
            }else if(SecurityUtils.hasRole("DEALER")){
                if((params.telefono.toString().trim()!='' && params.telefono.toString().trim() != utente.telefono )) utente.telefono=params.telefono.toString().trim()
                if((params.fax.toString().trim()!='' && params.fax.toString()!= utente.fax)) utente.fax=params.fax.toString().trim()
                if((params.email.toString().trim()!='' && params.email.toString().trim() != utente.email )) utente.email=params.email.toString().trim()
                /*if((params.telefono.toString().trim()!='' && params.telefono.toString().trim() != utente.telefono ) || (params.fax.toString().trim()!='' && params.fax.toString()!= utente.fax)||(params.email.toString().trim()!='' && params.email.toString().trim() != utente.email )) {
                    flash.message="Dealer aggiornato"
                }else{
                    flash.message=''
                }*/
                utente.attivo=true
                if(utente.save(flush: true)) {
                    //render (view:"nu/sceltaMenu", model: [utente: dealer] )
                    redirect uri:  "/", model: [utente: utente]
                }else{
                    println utente.errors
                }
            }
        }
        [utente:utente]
    }
    def modificaDealer(){
        /*def dealers = Dealer.withCriteria {

        }*/
        def province = Tasse.withCriteria {
            projections {
                property "provincia"
            }
        }
        if(request.post) {
           if(SecurityUtils.hasRole("ADMIN")){
               def idDealer=Long.parseLong(params.dealer.id)
               def dealer=Dealer.get(idDealer)
               if(dealer && (params.piva.toString().trim()!='' || params.localita.toString().trim()!='' || params.indirizzo.toString().trim() ||  params.cap.toString().trim())){
                   def localita=params.localita.toString().trim().toUpperCase()
                   def ragioneSociale=params.ragioneSociale.toString().trim().toUpperCase()
                   def indirizzo=params.indirizzo.toString().trim().toUpperCase()
                   def cap=params.cap.toString().trim()
                   if(cap.toString().contains(".")){
                       cap=Double.valueOf(cap).longValue().toString()
                       if (cap.toString().trim().length()<5){cap=cap.toString().trim().padLeft(5,"0")}
                   }
                   def provincia=params.provincia.toString().trim().toUpperCase()
                   bindData(dealer, params,[exclude: ['username','piva','telefono','fax','email']])
                   def piva
                   if(params.piva.toString().trim().length()<11){
                       piva=params.piva.toString().trim().padLeft(11,"0")
                   }else{
                       piva=params.piva.toString().trim()
                   }
                   dealer.ragioneSociale=ragioneSociale
                   dealer.password=piva
                   dealer.piva=piva
                   dealer.provincia=provincia
                   dealer.indirizzo=indirizzo
                   dealer.cap=cap
                   dealer.localita=localita
                   dealer.attivo=false
                   dealer.enabled=true
                   dealer.passwordScaduta=false
                   if((params.telefono.toString().trim()!='' )) dealer.telefono=params.telefono.toString().trim()
                   if((params.fax.toString().trim()!='')) dealer.fax=params.fax.toString().trim()
                   if((params.email.toString().trim()!='')) dealer.email=params.email.toString().trim()
                   if(dealer.save(flush:true)){
                       flash.message="DEALER AGGIORNATO"
                   }else{
                       println "errori-->${renderErrors(bean: dealer)}"
                       flash.error = renderErrors(bean: dealer)
                   }
               }
               else{
                   flash.error = "mancano i dati per aggiornare il dealer"
               }

            }else if(SecurityUtils.hasRole("DEALER")){
                   redirect uri:  "/"
           }
            redirect uri:  "/"
        }
    [province:province]
    }
    def loadDealer(long idDealer){
        def risposta
        if(Dealer.exists(idDealer)) {
            def dealer = Dealer.get(idDealer)
            risposta=[username:dealer.username,ragioneSociale:dealer.ragioneSociale,indirizzo:dealer.indirizzo,piva:dealer.piva,localita:dealer.localita,cap:dealer.cap,provincia:dealer.provincia,email:dealer.email?:"",telefono:dealer.telefono?:"",fax:dealer.fax?:"",risposta:true]
        } else{risposta=[risposta:false]}
        render risposta as JSON
    }
    def dealerCensiti(String username){
        username=username.replaceFirst("^0+(?!\\\$)", "")
        def dealerCensiti = Dealer.withCriteria {
            resultTransformer org.hibernate.criterion.CriteriaSpecification.ALIAS_TO_ENTITY_MAP
            ilike "username", "%${username}%"
            projections {
                property "id", "id"
                property "ragioneSociale", "ragioneSociale"
                property "username", "username"
            }
        }
        dealerCensiti.each { dealer->
           dealer.username=dealer.username.toString().padLeft(11,"0")
        }
            render dealerCensiti as JSON
    }
    def datiDealerT(String id) {
        def datiDealerT
        def dealer
        if(request.post) {
            if(id!=0){
                dealer= Dealer.findById(id)
                datiDealerT = [id:dealer.id, username:dealer.username,ragioneSociale:dealer.ragioneSociale,indirizzo:dealer.indirizzo,piva:dealer.piva,localita:dealer.localita,cap:dealer.cap,provincia:dealer.provincia,email:dealer.email?:"",telefono:dealer.telefono?:"",fax:dealer.fax?:""]
            }else{
                datiDealerT = [id:0,username:"",ragioneSociale:"",indirizzo:"",piva:"",localita:"",cap:"",provincia:"",email:"",telefono:"",fax:""]
            }
            render datiDealerT as JSON
        }
        else response.sendError(404)
    }
    def getDealer(long nodealer){
        def dealer=Dealer.get(nodealer)
        def risposta
        if(dealer){
            risposta=[id:dealer.id, username:dealer.username,ragioneSociale:dealer.ragioneSociale,indirizzo:dealer.indirizzo,piva:dealer.piva,localita:dealer.localita,cap:dealer.cap,provincia:dealer.provincia,email:dealer.email?:"",telefono:dealer.telefono?:"",fax:dealer.fax?:"",risposta:true]
        }else{risposta=[risposta:"nonEsistenessuna"]}
        render risposta as JSON
    }
}
