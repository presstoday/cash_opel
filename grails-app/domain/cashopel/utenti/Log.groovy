package cashopel.utenti

class Log {
    String parametri
    Date dateCreated
    Utente utente
    String operazione
    Double tempo = 0.0
    String pagina

    static mapping = {
        parametri type: "text"
        tempo defaultValue: "0.0"
        utente defaultvalue: ""
        pagina defaultvalue: ""
    }

    static constraints = {
        utente nullable: true
        pagina nullable: true
    }
}
