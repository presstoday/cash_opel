package cashopel.utenti

import cashopel.polizze.Polizza
import org.grails.databinding.BindingFormat

class Dealer {
    @BindingFormat("capitalize")String ragioneSocialeD
    @BindingFormat("capitalize")String dealerType
    @BindingFormat("uppercase") String pivaD
    @BindingFormat("capitalize") String indirizzoD
    @BindingFormat("capitalize") String localitaD
    @BindingFormat("capitalize") String provinciaD
    @BindingFormat("capitalize") String salesSpecialist
    @BindingFormat("capitalize") String codiceIASSICUR
    String capD,telefonoD,faxD, emailD, emailSales,codice,codProdCampagna
    int zonaCampagna
    BigDecimal percVenditore=0.0
    boolean attivo = false
    static constraints = {
        codice blank: false, unique: true
        codiceIASSICUR blank: false, unique: true
        ragioneSocialeD blank: false
        dealerType blank: false
        salesSpecialist blank: true
        indirizzoD nullable: true
        localitaD nullable: true
        pivaD nullable: true
        provinciaD nullable: true
        capD nullable: true
        telefonoD nullable: true
        faxD nullable: true
        emailD nullable: true
        emailSales nullable: true
        percVenditore min: 0.0
    }
    static hasMany =  [polizza: Polizza]
    static mapping = {
        table "dealers"
    }
}
