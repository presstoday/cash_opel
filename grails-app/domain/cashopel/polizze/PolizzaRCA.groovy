package cashopel.polizze

import cashopel.utenti.Dealer
import org.grails.databinding.BindingFormat

enum TipoVeicolo {
    AUTOVEICOLO, AUTOCARRO
}

enum CausaleContatto{
    DATO_MANCANTE, ERRORE_ANIA
}
enum Contattare{
    CLIENTE, CONCESSIONARIO
}
enum MotivoAnnulla{
    ANNULLATA, ANNULLATA_PER_FURTO
}
class PolizzaRCA {
    Date dateCreated = new Date()
    String noPolizza, telefono, cellulare, cap, email, codiceZonaTerritoriale, dSlip, codOperazione, identificativo, codCampagna, bonus,emailDealer, contattato, destUso, categoriaveicolo
    BigDecimal valoreAssicurato = 0.0, premioImponibile = 0.0, provvDealer = 0.0, provvGmfi = 0.0, provvVenditore = 0.0, premioLordo = 0.0, rappel = 0.0, imposte = 0.0
    int durata, tariffa, cilindrata, cavalli, quintali, kw
    @BindingFormat("capitalize")String nome
    @BindingFormat("capitalize")String cognome
    @BindingFormat("capitalize")String partitaIva
    @BindingFormat("capitalize")String indirizzo
    @BindingFormat("capitalize")String localita
    @BindingFormat("capitalize")String provincia
    @BindingFormat("capitalize")String provImmatricolazione
    @BindingFormat("capitalize")String marca
    @BindingFormat("capitalize")String modello
    @BindingFormat("capitalize")String versione
    @BindingFormat("capitalize")String targa
    @BindingFormat("capitalize")String targa1a
    @BindingFormat("capitalize")String telaio
    @BindingFormat("capitalize")String coperturaRichiesta
    @BindingFormat("capitalize")String step
    TipoPolizza tipoPolizza
    @BindingFormat("capitalize")String venditore
    StatoPolizza stato = StatoPolizza.PREVENTIVO
    TipoCliente tipoCliente = TipoCliente.M
    TipoVeicolo tipoVeicolo
    CausaleContatto causaleContatto
    MotivoAnnulla motivoAnnulla
    Contattare contattare
    Date dataDecorrenza
    Date dataScadenza
    Date dataImmatricolazione
    Date dataInserimento
    Date dataAnnullamento
    boolean annullata = false
    boolean onStar = false
    boolean isCash = false
    boolean nuovo = false
    boolean primaImmatr = false
    boolean hasCVT = false
    boolean daChiamare = false
    boolean dacsv = false
    Tracciato tracciato
    TracciatoDLRCA tracciatoDLRCA
    static belongsTo = [dealer: Dealer]
    static constraints = {
        tracciato nullable: true
        bonus nullable: true
        tracciatoDLRCA nullable: true
        noPolizza nullable: true
        motivoAnnulla nullable: true
        identificativo nullable: true
        telaio nullable: true, unique: true
        dataDecorrenza nullable: true
        dataScadenza nullable: true
        dataInserimento nullable: true
        dealer nullable: false
        dataImmatricolazione nullable: true
        dataAnnullamento nullable: true
        marca nullable: true
        indirizzo nullable: false
        telefono nullable: true
        cellulare nullable: true
        partitaIva nullable: false
        nome nullable: false
        cognome nullable: false
        tipoPolizza nullable: true
        cap nullable: false
        localita nullable: false
        provincia nullable: false
        email nullable: true
        targa nullable: true, unique: true
        targa1a nullable: true, unique: true
        modello nullable: true
        versione nullable: true
        valoreAssicurato min: 0.0
        coperturaRichiesta nullable: true
        premioImponibile min: 0.0
        premioLordo min: 0.0
        provvDealer min: 0.0
        provvGmfi min: 0.0
        provvVenditore min: 0.0
        rappel min: 0.0
        imposte min: 0.0
        durata nullable: true
        tariffa nullable: true
        quintali nullable: true
        cilindrata nullable: true
        cavalli nullable: true
        kw nullable: true
        codiceZonaTerritoriale nullable: true
        categoriaveicolo nullable: true
        dSlip nullable: true
        venditore nullable: true
        causaleContatto nullable: true
        identificativo blank: false, unique: true, matches: '[0-9]{6}'
        tipoVeicolo nullable: true
        destUso nullable: true
        causaleContatto nullable: true
        contattare nullable: true
        provImmatricolazione nullable: true
        emailDealer nullable: true
        contattato nullable: true
    }
    static mapping = {
        codOperazione defaultValue:"0"
        codCampagna defaultValue:"0"
        identificativo length: 6
        //destUso="PROPRIO"
    }
    def afterInsert(){
        if(isCash) noPolizza = "DLI" + id.toString().padLeft(7, "0")
        //else noPolizza = "DLI" + id.toString().padLeft(9, "0")
    }
    def afterUpdate() {
        if(isCash) noPolizza = "DLI" + id.toString().padLeft(7, "0")
    }
}