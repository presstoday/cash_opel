package cashopel.polizze

class Tracciato {

    Date dateCreated
    Date dataCaricamento
    Date dataInvio
    String tracciato
    TipoPolizza tipoPolizza=TipoPolizza.CASH
    boolean cash =false
    boolean annullata =false
    static belongsTo = [polizza: Polizza]

    static constraints = {
        tracciato size: 848..848
        polizza nullable: true, validator:{ polizza, tracciato1 ->
            if(tracciato1.tipoPolizza!=TipoPolizza.RINNOVI && !polizza) return "tracciato.polizza.nullable"
        }
        dataCaricamento nullable: true

    }

    static mapping = {
        table "tracciati_iassicur"
        //tracciatoCompagnia defaultvalue="IT"

    }

    String toString() { return tracciato }
}
