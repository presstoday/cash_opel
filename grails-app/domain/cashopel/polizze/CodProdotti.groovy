package cashopel.polizze

class CodProdotti {
    String codProdotto,pacchetto,copertura,step,codPacchetto,codProdottoflex2,codPacchettodlflex2,codPacchettoDLCampagna,codProdottoCampagna

    static constraints = {
        codProdotto nullable: true, unique: true
        codProdottoflex2 nullable: true, unique: true
        codPacchettoDLCampagna nullable: true, unique: true
        codProdottoCampagna nullable: true, unique: true
        step nullable: true
    }
}
