package cashopel.polizze

enum TipoPai {
    SPECIALE, OMAGGIO, PAGAMENTO
}
class TracciatoPAI {

    Date dateCreated
    Date dataCaricamento
    Date dataInvio
    String tracciatoPAI
    TipoPolizza tipoPolizza=TipoPolizza.CASH
    boolean cash =false
    boolean speciale =false
    boolean omaggio =false
    static belongsTo = [polizza: Polizza]
    TipoPai tipo=TipoPai.OMAGGIO
    static constraints = {
        tracciatoPAI  size: 848..848
        polizza nullable: true, validator:{ polizza, tracciato1 ->
            if(tracciato1.tipoPolizza!=TipoPolizza.RINNOVI && !polizza) return "tracciato.polizza.nullable"
        }
        dataCaricamento nullable: true

    }

    static mapping = {
        table "tracciati_pai"
        //tracciatoCompagnia defaultvalue="IT"

    }

    String toString() { return tracciatoPAI }
}
