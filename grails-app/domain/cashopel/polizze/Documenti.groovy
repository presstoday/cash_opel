package cashopel.polizze

enum TipoDoc {
    ELENCO_SETTIMANALE, FINANZIATE, LEASING, RINNOVI, CASH, ELENCO_SEMESTRALE_LUGLIO, ELENCO_SEMESTRALE_GENNAIO, ELENCO_MENSILE, PAIPAG
}
class Documenti {
    //String tipo
    TipoDoc tipo=TipoDoc.CASH
    Date dateCreated, lastUpdated
    String fileName, noPolizza,codIAssicur, cliente,indirizzo,cap,localita,provincia
    boolean inviatoMail =false
    boolean caricatoRotoMail =false
    boolean caricatosuIassicur =false
    byte[] fileContent
    static constraints = {
        fileName: unique: true
        fileContent nullable: false, size: 1..(1024 * 1024 * 5)
        tipo nullable: false
    }

    static mapping = {
        table "documenti_polizza"
    }
}
