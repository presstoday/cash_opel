package cashopel.polizze

class TracciatoIASSRCA {

    Date dateCreated
    Date dataCaricamento
    Date dataInvio
    String tracciato
    TipoPolizza tipoPolizza=TipoPolizza.CASH
    boolean rca =false
    boolean annullata =false
    static belongsTo = [polizzaRCA: PolizzaRCA]

    static constraints = {
        tracciato size: 848..848
        polizzaRCA nullable: true, validator:{ polizza, tracciato1 ->
            if(tracciato1.tipoPolizza!=TipoPolizza.RINNOVI && !polizza) return "tracciato.polizza.nullable"
        }
        dataCaricamento nullable: true

    }

    static mapping = {
        table "tracciati_iassicur_rca"
        //tracciatoCompagnia defaultvalue="IT"

    }

    String toString() { return tracciato }
}
