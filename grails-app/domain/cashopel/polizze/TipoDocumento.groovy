package cashopel.polizze

enum TipoDocumento {
    CERTIFICATO("certificato_polizza")


    final String value
    private TipoDocumento(String value) {
        this.value = value
    }
    String toString() { return value }
    static list() { return [CERTIFICATO] }
}
