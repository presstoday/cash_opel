package cashopel.polizze

class TracciatoDLRCA {

    Date dateCreated
    Date dataCaricamento
    Date dataInvio
    String tracciatoDL
    TipoPolizza tipoPolizza=TipoPolizza.CASH
    boolean cash =false
    boolean annullata =false
    static belongsTo = [polizza: PolizzaRCA]

    static constraints = {
        tracciatoDL nullable: false
        polizza nullable: true, validator:{ polizza, tracciato1 ->
            if(tracciato1.tipoPolizza!=TipoPolizza.RINNOVI && !polizza) return "tracciato.polizza.nullable"
        }
        dataCaricamento nullable: true
    }

    static mapping = {
        table "tracciati_DLRCA"
        tracciatoDL type: "text"
    }

    String toString() { return tracciatoDL }
}
