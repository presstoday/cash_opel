package cashopel.polizze
enum TipoDocSS {
    ELENCO_SETTIMANALE, FINANZIATE, LEASING, RINNOVI, CASH, ELENCO_SEMESTRALE_LUGLIO, ELENCO_SEMESTRALE_GENNAIO, ELENCO_MENSILE
}
class DocumentiSalesSpecialist {
    String tipo
    Date dateCreated, lastUpdated
    Date dataCaricamento
    String fileName
    String telaio
    String pIva
    byte[] fileContent
    static constraints = {
        fileName: unique:true
        fileContent nullable: false, size: 1..(1024 * 1024 * 5)
        tipo nullable: false
        dataCaricamento nullable: true
        telaio nullable: true
    }

    static mapping = {
        table "documenti_salesSpecialist"
    }
}
