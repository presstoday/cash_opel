package cashopel.polizze

import cashopel.utenti.Dealer
import org.grails.databinding.BindingFormat
enum TipoPolizza {
    CASH, FINANZIATE, LEASING, RINNOVI, PAIPAGAMENTO
}

class Polizza {
    Date dateCreated = new Date()
    String noPolizza, telefono,cellulare, cap, email,codiceZonaTerritoriale, dSlip,codOperazione, codCampagna
    BigDecimal valoreAssicuratoconiva=0.0, valoreAssicurato=0.0, premioImponibile=0.0, provvDealer=0.0, provvGmfi=0.0, provvVenditore=0.0, premioLordo=0.0, rappel=0.0, imposte=0.0, premioCliente=0.0, premioLordoCliente=0.0,premioTerzi=0.0, premioLordoTerzi=0.0
    int durata, tariffa
    @BindingFormat("uppercase")String nome
    @BindingFormat("uppercase")String cognome
    @BindingFormat("uppercase")String partitaIva
    @BindingFormat("uppercase")String indirizzo
    @BindingFormat("uppercase")String localita
    @BindingFormat("uppercase")String provincia
    @BindingFormat("uppercase")String marca
    @BindingFormat("uppercase")String modello
    @BindingFormat("uppercase")String targa
    @BindingFormat("uppercase")String telaio
    @BindingFormat("uppercase")String coperturaRichiesta
    @BindingFormat("uppercase")String step
    TipoPolizza tipoPolizza=TipoPolizza.CASH
    @BindingFormat("capitalize") String venditore
    StatoPolizza stato = StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE
    TipoCliente tipoCliente = TipoCliente.M
    Date dataDecorrenza
    Date dataScadenza
    Date dataImmatricolazione
    Date dataInserimento
    Date dataAnnullamento
    boolean annullata =false
    boolean onStar =false
    boolean nuovo =false
    boolean iva =false
    boolean certificatoMail=false
    boolean presaInCaricoMail=false
    boolean hasRCA = false
    Tracciato tracciato
    TracciatoCompagnia tracciatoCompagnia
    TracciatoPAI tracciatoPAI
    static belongsTo = [dealer: Dealer]
    static constraints = {
        tracciato nullable: true
        tracciatoCompagnia nullable: true
        tracciatoPAI nullable: true
        noPolizza nullable: true
        telaio nullable: true, unique: true
        dataDecorrenza nullable: true
        dataScadenza nullable: true
        dataInserimento nullable: true
        dealer nullable: false
        dataImmatricolazione nullable: true
        dataAnnullamento nullable: true
        marca nullable: true
        indirizzo nullable: false
        telefono nullable: true
        cellulare nullable: true
        partitaIva nullable: false
        nome nullable: false
        cognome nullable: false
        tipoPolizza nullable: false
        cap nullable: false
        localita nullable: false
        provincia nullable: false
        email nullable: true
        targa nullable: true, unique: true
        modello nullable: true
        valoreAssicurato min: 0.0
        valoreAssicuratoconiva min: 0.0
        coperturaRichiesta nullable: true
        premioImponibile min: 0.0
        premioLordo min: 0.0
        provvDealer min: 0.0
        provvGmfi min: 0.0
        provvVenditore min: 0.0
        rappel min: 0.0
        imposte min: 0.0
        durata nullable: true
        tariffa nullable: true
        codiceZonaTerritoriale nullable: true
        dSlip nullable: true
        venditore nullable: true
        premioCliente min: 0.0
        premioLordoCliente min: 0.0
        premioTerzi min: 0.0
        premioLordoTerzi min: 0.0

    }
    static mapping = {
        codOperazione defaultValue:"0"
        codCampagna defaultValue:"0"
    }
    def afterInsert(){
        if(tipoPolizza ==TipoPolizza.CASH) noPolizza = "DLI" + id.toString().padLeft(7, "0")
        //else noPolizza = "DLI" + id.toString().padLeft(9, "0")
    }
    def afterUpdate() {
        if(tipoPolizza ==TipoPolizza.CASH) noPolizza = "DLI" + id.toString().padLeft(7, "0")
    }
    /*void setDataDecorrenza(Date data) {
        if(data) {
            dataDecorrenza = data
            //stato = StatoPolizza.POLIZZA_IN_ATTESA_ATTIVAZIONE
        }
    }*/
}
