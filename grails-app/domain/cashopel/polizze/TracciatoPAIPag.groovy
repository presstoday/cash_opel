package cashopel.polizze

class TracciatoPAIPag {

    Date dateCreated
    Date dataCaricamento
    Date dataInvio
    String tracciatoPAIPaga
    TipoPolizza tipoPolizza=TipoPolizza.PAIPAGAMENTO
    boolean cash =false
    static belongsTo = [polizza: PolizzaPaiPag]
    TipoPai tipo=TipoPai.PAGAMENTO
    static constraints = {
        tracciatoPAIPaga  size: 848..848
        dataCaricamento nullable: true

    }
    static mapping = {
        table "tracciati_paiPag"
        //tracciatoCompagnia defaultvalue="IT"

    }

    String toString() { return tracciatoPAIPaga }
}
