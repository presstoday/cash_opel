package cashopel.polizze

enum StatoPolizza {
     PREVENTIVO("preventivo"), RICHIESTA_INVIATA("richiesta inviata"),  POLIZZA("polizza"),ANNULLATA("annullata"), POLIZZA_IN_ATTESA_ATTIVAZIONE("polizza in attesa di attivazione"), RECEDUTA ("receduta"),  DA_CHIAMARE("da chiamare"),ANNULLATA_FURTO("annullata per furto"),
    final String stato
    private StatoPolizza(String stato) { this.stato = stato }
    String toString() { return stato }
    static list() { return [PREVENTIVO,POLIZZA,ANNULLATA,POLIZZA_IN_ATTESA_ATTIVAZIONE, DA_CHIAMARE, RICHIESTA_INVIATA,ANNULLATA_FURTO] }
}

/*enum TipoPolizza{
    FINANZIATE ("finanziate"), LEASING("leasing"), CASH("cash")
    final String tipo
    private TipoPolizza(String tipo) { this.tipo = tipo }
    String toString() { return tipo }
    static list() { return [FINANZIATE,LEASING,CASH] }
}*/
enum TipoCliente {
    M("uomo", "M"), F("donna", "F"), DITTA_INDIVIDUALE("ditta individuale", "D"), SOCIETA("società", "S")
    final String tipo, value
    private TipoCliente(String tipo, String value) {
        this.tipo = tipo
        this.value = value
    }
    String toString() { return tipo }
    static list() { return [M, F, SOCIETA, DITTA_INDIVIDUALE] }
}