package cashopel.polizze

class TracciatoCompagnia {

    Date dateCreated
    Date dataCaricamento
    Date dataInvio
    String tracciatoCompagnia
    TipoPolizza tipoPolizza=TipoPolizza.CASH
    boolean cash =false
    boolean annullata =false
    static belongsTo = [polizza: Polizza]

    static constraints = {
        tracciatoCompagnia nullable: false
        polizza nullable: true, validator:{ polizza, tracciato1 ->
            if(tracciato1.tipoPolizza!=TipoPolizza.RINNOVI && !polizza) return "tracciato.polizza.nullable"
        }
        dataCaricamento nullable: true
    }

    static mapping = {
        table "tracciati_compagnia"
        tracciatoCompagnia type: "text"
    }

    String toString() { return tracciatoCompagnia }
}
