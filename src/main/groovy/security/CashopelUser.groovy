package security

import org.springframework.security.core.authority.AuthorityUtils
import org.springframework.security.core.userdetails.User
import cashopel.utenti.Utente

class CashopelUser extends User {

    private final def id
    private final String stringRepresentation

    CashopelUser(Utente utente) {
        super(utente.username, utente.password, utente.enabled, true, !utente.passwordScaduta, true, AuthorityUtils.commaSeparatedStringToAuthorityList(utente.ruoli.collect { ruolo -> ruolo.name() }.join(",")))
        this.id = utente.id
        this.stringRepresentation = utente.toString()
    }

    def getId() { return id }

    String toString() { return stringRepresentation }

}