package extensions

class StringStaticExtension {

    static String random(String target, int length = 10, List from = ["a".."z", "A".."Z", 0..9]) {
        def pool = from.flatten()
        def rand = new Random(System.currentTimeMillis())
        def str = (0..<length).collect { pool[rand.nextInt(pool.size())] }
        return str.join()
    }

}