package extensions

import groovy.transform.CompileStatic

import java.math.RoundingMode
import java.text.DecimalFormat

@CompileStatic
class NumberExtension {

    static String format(Number target, String format = "#,##0.00") { return new DecimalFormat(format).format(target) }

    static BigDecimal round(BigDecimal target, int precision = 0) { return target.setScale(precision, RoundingMode.HALF_UP) }

}
